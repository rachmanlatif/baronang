<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->modelClass; ?>', 'url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
	array('label'=>'Update <?php echo $this->modelClass; ?>', 'url'=>array('update', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'Delete <?php echo $this->modelClass; ?>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage <?php echo $this->modelClass; ?>', 'url'=>array('admin')),
);
?>

<div>
	<h1 class="left">View <?php echo $this->modelClass; ?></h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('index'); ?>"; ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('add'); ?>"; ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('update', array('id'=>\$model->". $this->tableSchema->primaryKey .")); ?>"; ?>">Update</a>
		<?php echo "<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>\$model->". $this->tableSchema->primaryKey ."),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>"; ?>
	</div>
</div>
<div class="clear"></div>
<hr />

<?php echo "<?php"; ?> $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
		CDetailViewHelper::getCreatedBy($model),
		CDetailViewHelper::getCreatedDate($model),
		CDetailViewHelper::getModifiedBy($model),
		CDetailViewHelper::getModifiedDate($model),
	),
)); ?>
