<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php $grid_id= $this->class2id($this->modelClass).'-grid'; ?>
<?php
echo "<?php\n";
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Manage',
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->modelClass; ?>', 'url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('<?php echo $this->class2id($this->modelClass); ?>-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div>
            <h1 class="left">Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?></h1>
            <div class="form-button-container">
                <a class="form-button btn btn-primary" href="<?php echo "<?php echo \$this->createUrl('add'); ?>"; ?>">Add</a>
                <?php echo "<?php echo CHtml::ajaxLink(\"Delete\",
				\$this->createUrl('deleteSelected'),
				array(
					'type' => 'post',
					'data' => 'js:{ajax:true, ids:$.fn.yiiGridView.getSelection(\'". $this->class2id($this->modelClass) ."-grid\')}',
					'success' => 'function(data) {
						$.fn.yiiGridView.update(\'". $this->class2id($this->modelClass) ."-grid\');
					}',
				),
				array(
					'class' => 'form-button btn btn-primary',
					'confirm' => 'Are you sure?',
				)
			);
		?>"; ?>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'<?php echo $grid_id; ?>',
            'itemsCssClass'=>'table table-striped table-bordered table-hover',
            'cssFile' => Yii::app()->theme->baseUrl.'/app/css/grid-view.css',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'selectableRows'=>2,
            'columns'=>array(
            array(
            'class'=>'CCheckBoxColumn',
            'id'=>'ids',
            ),
            <?php
            $count=0;
            foreach($this->tableSchema->columns as $column)
            {
                if(++$count==7)
                    echo "\t\t/*\n";
                echo "\t\t'".$column->name."',\n";
            }
            if($count>=7)
                echo "\t\t*/\n";
            ?>
            CGridViewHelper::getCreatedBy(),
            CGridViewHelper::getCreatedDate(),
            CGridViewHelper::getModifiedBy(),
            CGridViewHelper::getModifiedDate(),
            array(
            'class'=>'CButtonColumn',
            ),
            ),
            )); ?>



        </div>
    </div>
</div>