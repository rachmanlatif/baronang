<?php

class ServiceLayer {
	
	protected $model = null;
    protected $connection = NULL;
    protected $transaction = NULL;
	
	public function __construct($model=NULL) {
		if($model==NULL){
			$this->model =  NULL;
		}
		else{
			$this->model = $model;
		}
	}
	
}
