<?php

/**
 *	modules/operation/components/DataMapperBase.php
 *	**
 *	
 **/
class DataMapperBase {

	/**
	 *	return connection
	 **/
	protected function getConnection() {
		$con = Yii::app()->db;
		return $con;
	}
	
	/**
	 *	return void
	 **/
	protected function execSql($sql) {
		$con = $this->getConnection();
		$cmd = $con->createCommand($sql);
		$cmd->execute();
	}
	
	/**
	 *	return CDbDataReader
	 **/
	protected function execSqlQuery($sql) {
		$con = $this->getConnection();
		$cmd = $con->createCommand($sql);
		return $cmd->query();
	}
	
	/**
	 *	return CDbDataReader
	 **/
	protected function execSqlQueryAll($sql) {
		$con = $this->getConnection();
		$cmd = $con->createCommand($sql);
		return $cmd->queryAll();
	}

}

?>
