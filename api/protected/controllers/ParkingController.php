<?php

class ParkingController extends Controller {

    public function grepEmail($email){
        $arr = explode("@",$email);

        $e1 = $arr[0];
        $e2 = $arr[1];
        $count = strlen($arr[0]);
        if($count > 4){
            $e = str_replace(substr($e1, 2,2),'**',$e1).'@'.$e2;
        }
        else{
            $e = str_replace(substr($e1, -1,1),'*',$e1).'@'.$e2;
        }

        return $e;
    }

    public function actionResendEmail(){
        $aa = "select* from PaymentGateway.dbo.LogMail where Status = 0";
        $bb =  Yii::app()->db->createCommand($aa)->queryAll();
        foreach($bb as $b){
            $email = $b['Email'];
            $type = $b['Type'];
            $status = $b['Status'];

            $x = "select* from PaymentGateway.dbo.UserPaymentGateway where email = '".$email."'";
            $y =  Yii::app()->db->createCommand($x)->queryAll();
            if($y != null){
                $uid = $y[0]['KodeUser'];
                $nama = $y[0]['NamaUser'];
                $email = $y[0]['email'];
                $gac = $y[0]['Gac'];

                //generator
                $rand = rand(100000,999999);
                $newpass = md5($rand);

                $subject = '';
                $message = '';
                if($status == 0){
                    if($type == 0){
                        $link = 'https://baronang.com/api/parking/confirmation/id/'.$gac;

                        $subject = 'Sky Parking Register Confirmation';
                        $message = "<p>
                          Hi! ".$nama.",<br><br>
                          Terima kasih telah bergabung dengan Sky Parking.<br>
                          Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
                          <a href=".$link.">Konfirmasi</a>
                          <br>
                          <br>
                          <br>
                          <br>
                          Terima kasih.<br>
                          Salam,<br>
                          <br>
                          <br>
                          Sky Parking
                          <br>
                          <i>Powered by Baronang</i>
                        </p>";
                    }
                    else if($type == 1){
                        $link = 'https://baronang.com/api/parking/confirmationPassword/id/'.$gac.'/n/'.$newpass;

                        $subject = "Sky Parking Forgot Password";
                        $message = "<p>
                          Hi! ".$nama.",<br><br>
                          Anda telah melakukan permintaan lupa password.<br>
                          Kami perlu memastikan bahwa anda yang melakukan permintaan. Harap mengkonfirmasi dengan mengklik link dibawah ini.<br>
                          <a href='".$link."'>Konfirmasi</a><br>
                          Jika anda mengkonfirmasi, password baru anda adalah : ".$rand."<br><br>
                          Harap segera mengganti password anda.<br>
                          Abaikan pesan ini jika anda tidak melakukan permintaan lupa password.
                          <br>
                          <br>
                          <br>
                          <br>
                          Terima kasih.<br>
                          Salam,<br>
                          <br>
                          <br>
                          Sky Parking
                          <br>
                          <i>Powered by Baronang</i>
                        </p>";
                    }
                    else if($type == 2){
                        $link = 'https://baronang.com/api/parking/confirmationPin/id/'.$gac.'/n/'.$newpass;

                        $subject = "Sky Parking Forgot Pin";
                        $message = "<p>
                          Hi! ".$nama.",<br><br>
                          Anda telah melakukan permintaan lupa PIN.<br>
                          Kami perlu memastikan bahwa anda yang melakukan permintaan. Harap mengkonfirmasi dengan mengklik link dibawah ini.<br>
                          <a href='".$link."'>Konfirmasi</a><br>
                          Jika anda mengkonfirmasi, PIN baru anda adalah : ".$rand."<br><br>
                          Harap segera mengganti password anda.<br>
                          Abaikan pesan ini jika anda tidak melakukan permintaan lupa PIN.
                          <br>
                          <br>
                          <br>
                          <br>
                          Terima kasih.<br>
                          Salam,<br>
                          <br>
                          <br>
                          Sky Parking
                          <br>
                          <i>Powered by Baronang</i>
                        </p>";
                    }

                    $curl = curl_init();
                    $post = "toaddr=$email&subject=$subject&message=$message";
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://interzircon.com/phpmail.php",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $post,
                    ));
                    $output = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    $result = json_decode($output, true);
                    if($err) {
                        echo $email.' belum berhasil kirim';
                    }
                    else {
                        $qwe = "update PaymentGateway.dbo.LogMail set Status = 1 where Email='".$email."' and Type = '".$type."'";
                        $asd =  Yii::app()->db->createCommand($qwe);
                        if($asd->execute()){
                            echo $email.' berhasil kirim<br>';
                        }
                        else{
                            echo $email.' gagal update status<br>';
                        }
                    }
                }
            }
            else{
                echo $email.' tidak ditemukan di sistem<br>';
            }
        }
    }

    public function actionConfirmation($id){
        $param = $id;

        //save log api
        $id = '';
        $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
        $asd =  Yii::app()->db->createCommand($qwe);
        if($asd->execute()){
            $id = Yii::app()->db->getLastInsertID();
        }

        $msg = '';
        $a = "select* from PaymentGateway.dbo.UserPaymentGateway where Gac = '".$id."' and Status = 0";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $sql = "update PaymentGateway.dbo.UserPaymentGateway set Status = 1 where Gac = '".$id."'";
            $exec = Yii::app()->db->createCommand($sql);
            if($exec->execute()){
              $msg = '<h3>Berhasil melakukan konfirmasi</h3>';
            }
            else{
              $msg = '<h3>Gagal melakukan konfirmasi</h3>';
            }
        }
        else{
            $msg = '<h3>Anda sudah melakukan konfirmasi</h3>';
        }

        echo $msg;

        if($id != ''){
            //save log api
            $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }
    }

    public function actionConfirmationPassword($id, $n){
        $param = $id.','.$n;

        //save log api
        $id = '';
        $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
        $asd =  Yii::app()->db->createCommand($qwe);
        if($asd->execute()){
            $id = Yii::app()->db->getLastInsertID();
        }

        $msg = '';
        $a = "select* from PaymentGateway.dbo.UserPaymentGateway where Gac = '".$id."' and Status = 1";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $sql = "update PaymentGateway.dbo.UserPaymentGateway set Pass = '".$n."' where Gac = '".$id."'";
            $exec = Yii::app()->db->createCommand($sql);
            if($exec->execute()){
              $msg = '<h3>Berhasil melakukan ubah password</h3>';
            }
            else{
              $msg = '<h3>Gagal melakukan ubah password</h3>';
            }
        }
        else{
            $msg = '<h3>Anda belum melakukan konfirmasi</h3>';
        }

        echo $msg;

        if($id != ''){
            //save log api
            $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }
    }

    public function actionConfirmationPin($id, $n){
        $param = $id.','.$n;

        //save log api
        $id = '';
        $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
        $asd =  Yii::app()->db->createCommand($qwe);
        if($asd->execute()){
            $id = Yii::app()->db->getLastInsertID();
        }

        $msg = '';
        $a = "select* from PaymentGateway.dbo.UserPaymentGateway where Gac = '".$id."' and Status = 1";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $sql = "update PaymentGateway.dbo.UserPaymentGateway set Pin = '".$n."' where Gac = '".$id."'";
            $exec = Yii::app()->db->createCommand($sql);
            if($exec->execute()){
              $msg = '<h3>Berhasil melakukan ubah pin</h3>';
            }
            else{
              $msg = '<h3>Gagal melakukan ubah pin</h3>';
            }
        }
        else{
            $msg = '<h3>Anda belum melakukan konfirmasi</h3>';
        }

        echo $msg;

        if($id != ''){
            //save log api
            $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }
    }

    public function actionChangePassword(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['UserID']) and isset($_REQUEST['New']) and isset($_REQUEST['Retype']) and isset($_REQUEST['Pin'])){
            $uid = $_REQUEST['UserID'];
            $new = md5($_REQUEST['New']);
            $retype = md5($_REQUEST['Retype']);
            $pin = md5($_REQUEST['Pin']);

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $bb = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid'";
            $vv =  Yii::app()->db->createCommand($bb)->queryAll();
            if($vv != null){
                $acpin = $vv[0]['Pin'];

                if($pin == $acpin){
                    if($new == $retype){
                        $iu = "update [PaymentGateway].[dbo].[UserPaymentGateway] set Pass = '".$new."' where KodeUser = '".$uid."'";
                        $kj =  Yii::app()->db->createCommand($iu);
                        if($kj->execute()){
                            $response['status'] = true;
                            $response['message'] = 'Berhasil merubah password';
                        }
                        else{
                            $response['message'] = 'Gagal merubah password';
                        }
                    }
                    else{
                      $response['message'] = 'Pengulangan password tidak benar';
                    }
                }
                else{
                    $response['message'] = 'Pin tidak benar';
                }
            }
            else{
                $response['message'] = 'User not found';
            }

            if($id != ''){
                $msg = json_encode($response);

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionChangePin(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['UserID']) and isset($_REQUEST['New']) and isset($_REQUEST['Retype']) and isset($_REQUEST['Pin'])){
            $uid = $_REQUEST['UserID'];
            $new = md5($_REQUEST['New']);
            $retype = md5($_REQUEST['Retype']);
            $pin = md5($_REQUEST['Pin']);

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $bb = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid'";
            $vv =  Yii::app()->db->createCommand($bb)->queryAll();
            if($vv != null){
                $acpin = $vv[0]['Pin'];

                if($pin == $acpin){
                    if($new == $retype){
                        $iu = "update [PaymentGateway].[dbo].[UserPaymentGateway] set Pin = '".$new."' where KodeUser = '".$uid."'";
                        $kj =  Yii::app()->db->createCommand($iu);
                        if($kj->execute()){
                            $response['status'] = true;
                            $response['message'] = 'Berhasil merubah PIN';
                        }
                        else{
                            $response['message'] = 'Gagal merubah PIN';
                        }
                    }
                    else{
                      $response['message'] = 'Pengulangan PIN tidak benar';
                    }
                }
                else{
                    $response['message'] = 'PIN lama tidak benar';
                }
            }
            else{
                $response['message'] = 'User not found';
            }

            //save log api
            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetPopUp(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'urlImage'=>'',
            'link'=>'',
            'name'=>'',
            'description'=>'',
        );

        if(isset($_REQUEST['KID'])){
            $kid = $_REQUEST['KID'];
            
            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $aa = "select top 1 * from ".$zzz[0]['DatabaseName'].".dbo.PopUpMobile where Status = 1";
                $bb =  Yii::app()->db->createCommand($aa)->queryAll();
                if($bb != null){
                  $response['status'] = true;
                  $response['message'] = 'Success';
                  $response['urlImage'] = $bb[0]['Image'];
                  $response['link'] = $bb[0]['Link'];
                  $response['name'] = $bb[0]['Name'];
                  $response['description'] = $bb[0]['Description'];
                }
            }
            else{
                $response['message'] = 'Not found';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionLinkMember(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
            'member'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];

            //save log api
            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yaa = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zaa =  Yii::app()->db->createCommand($yaa)->queryAll();
            if($zaa != null){
                $bb = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid'";
                $vv =  Yii::app()->db->createCommand($bb)->queryAll();
                if($vv != null){
                    $namauser = $vv[0]['NamaUser'];
                    $address = $vv[0]['Alamat'];
                    $telp = $vv[0]['Telp'];
                    $email = $vv[0]['email'];
                    $ktp = $vv[0]['KTP'];

                    $bbb = "select* from PaymentGateway.dbo.UserMemberKoperasi where UserID = '$uid' and KID = '$kid'";
                    $vvv =  Yii::app()->db->createCommand($bbb)->queryAll();
                    if($vvv == null){
                        //getKodeMember
                        $aa = "select ".$zaa[0]['DatabaseName'].".[dbo].[getKodeMember]('$kid')";
                        $bb = Yii::app()->db->createCommand($aa)->queryAll();
                        if($bb != null){
                            $kodemember = $bb[0][''];

                            //MemberList
                            $iu = "exec ".$zaa[0]['DatabaseName'].".[dbo].[ProsesMemberList] '$kid', '$kodemember', '$namauser', '$address', '$telp', '$email', '1', '$ktp', '', '','$uid'";
                            $kj =  Yii::app()->db->createCommand($iu);
                            if($kj->execute()){
                                $response['status'] = true;
                                $response['member'] = $kodemember;
                                $response['message'] = 'Success linked member';
                            }
                            else{
                                $response['message'] = 'Failed linked member';
                            }

                        }
                        else{
                            $response['message'] = 'Failed linked member';
                        }
                    }
                    else{
                        $response['status'] = true;
                        $response['message'] = 'User already linked';
                    }
                }
                else{
                    $response['message'] = 'User not found';
                }
            }
            else{
                $response['message'] = 'KID not found';
            }

            //save log api
            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionLinkCardMember(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID']) and isset($_REQUEST['MemberID']) and isset($_REQUEST['LocationID']) and isset($_REQUEST['CardID'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];
            $mid = $_REQUEST['MemberID'];
            $cardid = $_REQUEST['CardID'];
            $locationid = $_REQUEST['LocationID'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $yy = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid' and Status = 1";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){

                    $yq = "select* from ".$zzz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '$locationid' and Status = 1";
                    $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                    if($zq != null){
                        $y = "select* from ".$zzz[0]['DatabaseName'].".dbo.MasterCard where Barcode = '$cardid' and Status = 1";
                        $z =  Yii::app()->db->createCommand($y)->queryAll();
                        if($z != null){
                            $cardno = $z[0]['CardNo'];

                            $yr = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardno' and Status = 1";
                            $zr =  Yii::app()->db->createCommand($yr)->queryAll();
                            if($zr != null){
                                $isLink = $zr[0]['isLink'];

                                $bvc = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberLocation where MemberID = '$mid' and UserIDBaronang = '$uid' and CardNo = '$cardno'";
                                $gfd =  Yii::app()->db->createCommand($bvc)->queryAll();
                                if($gfd != null){

                                    if($isLink == 0){
                                        $bvc = "exec ".$zzz[0]['DatabaseName'].".dbo.ProsesCardLink '$cardno','$mid'";
                                        $gfd =  Yii::app()->db->createCommand($bvc);
                                        if($gfd->execute()){
                                            $response['status'] = true;
                                            $response['message'] = 'Success link card';
                                        }
                                        else{
                                            $response['message'] = 'Failed link card member';
                                        }
                                    }
                                    else{
                                        $response['message'] = 'Card already link';
                                    }
                                }
                                else{
                                    $response['message'] = 'You are not registered as member in this location';
                                }
                            }
                            else{
                                $response['message'] = 'Card not active';
                            }
                        }
                        else{
                            $response['message'] = 'Card Not Found';
                        }
                    }
                    else{
                      $response['message'] = 'Location Not Found';
                    }
                }
                else{
                    $response['message'] = 'User Not Found';
                }
            }
            else{
                $response['message'] = 'KID Not Found';
            }

            //save log api
            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionTransferPoint(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID']) and isset($_REQUEST['MemberID']) and isset($_REQUEST['CardIDFrom']) and isset($_REQUEST['CardIDTo']) and isset($_REQUEST['Point'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];
            $mid = $_REQUEST['MemberID'];
            $cardidfrom = $_REQUEST['CardIDFrom'];
            $cardidto = $_REQUEST['CardIDTo'];
            $point = $_REQUEST['Point'];

            //save log api
            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $yy = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    $y = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardidfrom' and Status = 1 and MemberID = '$mid'";
                    $z =  Yii::app()->db->createCommand($y)->queryAll();
                    if($z != null){
                        $balance = $z[0]['Point'];

                        $yq = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardidto' and Status = 1 and MemberID = '$mid'";
                        $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                        if($zq != null){
                            if($balance >= $point){
                                $bvc = "exec ".$zzz[0]['DatabaseName'].".dbo.ProsesCardTransferPoint '$uid','$mid','$cardidfrom','$cardidto','$point'";
                                $gfd =  Yii::app()->db->createCommand($bvc);
                                if($gfd->execute()){
                                    $response['status'] = true;
                                    $response['message'] = 'Success transfer point';
                                }
                                else{
                                  $response['message'] = 'Failed transfer point';
                                }
                            }
                            else{
                                $response['message'] = 'Balance not enough';
                            }
                        }
                        else{
                            $response['message'] = 'Card Destination Not Found';
                        }
                    }
                    else{
                        $response['message'] = 'Card From Source Not Found';
                    }
                }
                else{
                    $response['message'] = 'User Not Found';
                }
            }
            else{
                $response['message'] = 'KID Not Found';
            }

            //save log api
            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionLinkCard(){
        $response = array(
            'Status'=>FALSE,
            'Message'=>'',
            'ErrorCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID']) and isset($_REQUEST['MemberID']) and isset($_REQUEST['CardID'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];
            $mid = $_REQUEST['MemberID'];
            $cardid = $_REQUEST['CardID'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $yy = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    $y = "select* from ".$zzz[0]['DatabaseName'].".dbo.MasterCard where Barcode = '$cardid' and Status = 1";
                    $z =  Yii::app()->db->createCommand($y)->queryAll();
                    if($z != null){
                        $cardno = $z[0]['CardNo'];

                        $yq = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardno' and Status = 1";
                        $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                        if($zq != null){
                            $isLink = $zq[0]['isLink'];
                            if($isLink == 0){
                                $bvc = "exec ".$zzz[0]['DatabaseName'].".dbo.ProsesCardLink '$cardno','$mid'";
                                $gfd =  Yii::app()->db->createCommand($bvc);
                                if($gfd->execute()){
                                    $response['Status'] = true;
                                    $response['Message'] = 'Success link card';
                                }
                                else{
                                  $response['Message'] = 'Failed link card';
                                }
                            }
                            else{
                                $response['Message'] = 'Card already link';
                            }
                        }
                        else{
                            $response['Message'] = 'Card not active';
                        }
                    }
                    else{
                        $response['Message'] = 'Card Not Found';
                    }
                }
                else{
                    $response['Message'] = 'User Not Found';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

            //save log api
            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionUnlinkCard(){
        $response = array(
            'Status'=>FALSE,
            'Message'=>'',
            'ErrorCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID']) and isset($_REQUEST['MemberID']) and isset($_REQUEST['AccNo'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];
            $mid = $_REQUEST['MemberID'];
            $accno = $_REQUEST['AccNo'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $yy = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    $y = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where MemberID = '$mid' and CardNo = '$accno' and Status = 1 and isLink = 1";
                    $z =  Yii::app()->db->createCommand($y)->queryAll();
                    if($z != null){
                        $cardno = $z[0]['CardNo'];

                        $bvc = "update ".$zzz[0]['DatabaseName'].".dbo.MemberCard set isLink = 0 where MemberID = '$mid' and CardNo = '$accno' and Status = 1 and isLink = 1";
                        $gfd =  Yii::app()->db->createCommand($bvc);
                        if($gfd->execute()){
                            $response['Status'] = true;
                            $response['Message'] = 'Success unlink card';
                        }
                        else{
                          $response['Message'] = 'Failed unlink card';
                        }
                    }
                    else{
                        $response['Message'] = 'Card Not Found';
                    }
                }
                else{
                    $response['Message'] = 'User Not Found';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

            //save log api
            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionCreateCard(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'member'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID']) and isset($_REQUEST['MemberID']) and isset($_REQUEST['PlatNo']) and isset($_REQUEST['Vehicle'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];
            $mid = $_REQUEST['MemberID'];
            $platno = $_REQUEST['PlatNo'];
            $vehicle = $_REQUEST['Vehicle'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){

                $yy = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    $nama = $zz[0]['NamaUser'];
                    $ktp = $zz[0]['KTP'];

                    $vehicletype = '';
                    if($vehicle == 'Motor'){
                        $vehicletype = 'C';
                    }
                    else if($vehicle == 'Mobil'){
                        $vehicletype = 'B';
                    }
                    else{
                        $vehicletype = '';
                    }

                    $yt = "select* from ".$zzz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vehicletype."'";
                    $zt =  Yii::app()->db->createCommand($yt)->queryAll();
                    if($zt != null){
                        $vehicleid = $zt[0]['VehicleID'];

                        $yr = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberData where PlateNumber = '".$platno."'";
                        $zr =  Yii::app()->db->createCommand($yr)->queryAll();
                        if($zr == null){
                            $aa = "select ".$zzz[0]['DatabaseName'].".[dbo].[getKodeCreateCard]()";
                            $bb = Yii::app()->db->createCommand($aa)->queryAll();
                            if($bb != null){
                                $cardno = $bb[0][''];

                                $barcode = $this->generateBarcode($cardno);
                                $tanggal = date('Y-m-d H:i:s');

                                $bvh = "insert into ".$zzz[0]['DatabaseName'].".dbo.MasterCard(CardNo, Barcode, Status, DateCreate, CardID) values ('$cardno','$barcode',1,'$tanggal','$cardno')";
                                $gfh =  Yii::app()->db->createCommand($bvh);
                                if($gfh->execute()){
                                    $bvc = "exec ".$zzz[0]['DatabaseName'].".dbo.ProsesCreateCard '$kid','$uid','$mid','$platno','$vehicleid','$cardno'";
                                    $gfd =  Yii::app()->db->createCommand($bvc);
                                    if($gfd->execute()){
                                        $response['status'] = true;

                                        $er = "select top 1 * from PaymentGateway.dbo.UserMemberKoperasi where KID = '".$kid."' and UserID = '".$uid."' and Status = 1";
                                        $wr =  Yii::app()->db->createCommand($er)->queryAll();
                                        if($wr != null){
                                            $response['member'] = $wr[0]['KodeMember'];
                                        }

                                        $response['message'] = 'Success create card';
                                    }
                                    else{
                                        $response['message'] = 'Failed create card';
                                    }
                                }
                                else{
                                    $response['message'] = 'Failed create master card, try again';
                                }
                            }
                            else{
                                $response['message'] = 'Failed get card number, try again';
                            }
                        }
                        else{
                            $response['message'] = 'Plat nomor '.$platno.' sudah terdaftar';
                        }
                    }
                    else{
                        $response['message'] = 'Try again';
                        $response['errorCode'] = 'ex947843';
                    }
                }
                else{
                    $response['message'] = 'User Not Found';
                }
            }
            else{
                $response['message'] = 'KID Not Found';
            }

            //save log api
            if($id != ''){
                $msg = json_encode($response);

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionSettleProduct(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        $response['message'] = 'Anda harus mengupdate aplikasi Sky Parking anda';
        $response['errorCode'] = 'ex54992';

        echo json_encode($response);
    }

    public function actionSettleProductMonthly(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        $response['message'] = 'Anda harus mengupdate aplikasi Sky Parking anda';
        $response['errorCode'] = 'ex54992';

        echo json_encode($response);
    }

    public function actionSettleHitProductMonthly(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID']) and isset($_REQUEST['MemberID']) and isset($_REQUEST['ProductID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['Month']) and isset($_REQUEST['LocationID'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];
            $mid = $_REQUEST['MemberID'];
            $productid = $_REQUEST['ProductID'];
            $cardid = $_REQUEST['CardID'];
            $month = $_REQUEST['Month'];
            $locationid = $_REQUEST['LocationID'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){

                $yy = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '$uid'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){

                    //lewat api ?
                    $yn = "select* from ".$zzz[0]['DatabaseName'].".dbo.MerchantHit where LocationID = '".$locationid."' and ActionHit = 'settleProduct' and Status = 1";
                    $zn =  Yii::app()->db->createCommand($yn)->queryAll();
                    if($zn != null){
                        $a = "select* from ".$zzz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '$cardid' and Status = 1";
                        $b =  Yii::app()->db->createCommand($a)->queryAll();
                        if($b != null){
                            $barcode = $b[0]['Barcode'];

                            $c = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardid' and Status = 1";
                            $d =  Yii::app()->db->createCommand($c)->queryAll();
                            if($d != null){
                                $balance = $d[0]['Point'];

                                $parking = new Parking();
                                $datas = $parking->getProduct($kid, $productid, $locationid);
                                if($datas != null){
                                    if(isset($datas['StatusCode']) and $datas['StatusCode'] == '000'){
                                        if($datas['Data'][0] != null){
                                            $data = $datas['Data'][0];

                                            $productname = $data['ProductName'];
                                            $jumlahpoint = $data['DeductPoint'];
                                            $vehicleid = $data['VehicleID'];
                                            $period = $data['Period'];

                                            $totalbulan = ($month+$period)-1;

                                            if($jumlahpoint <= $balance){
                                                $result = $parking->settleProductMonthly($kid, $productid, $locationid, $cardid, $month);
                                                if($result != null){
                                                    if(isset($result['StatusCode']) and $result['StatusCode'] == '000'){

                                                        $qwe = "exec ".$zzz[0]['DatabaseName'].".dbo.ProsesBuyMemberCardHit '".$uid."','".$mid."','".$productid."','".$productname."','".$vehicleid."','".$locationid."','$jumlahpoint',1,'$period','".$cardid."'";
                                                        $asd =  Yii::app()->db->createCommand($qwe);
                                                        if($asd->execute()){
                                                            //insert bulan member
                                                            for($a=$month;$a<=$totalbulan;$a++){
                                                                if($a > 12){
                                                                    $bln = $a - 12;
                                                                    $year = date('Y', strtotime('+1 year'));
                                                                }
                                                                else{
                                                                    $bln = $a;
                                                                    $year = date('Y');
                                                                }

                                                                $qwes = "exec ".$zzz[0]['DatabaseName'].".dbo.ProsesBuyMemberMonth '$cardid','$locationid','$vehicleid',$bln,'$year'";
                                                                $asds =  Yii::app()->db->createCommand($qwes);
                                                                $asds->execute();
                                                            }

                                                            $response['status'] = true;
                                                            $response['message'] = $result['Message'];
                                                        }
                                                        else{
                                                            $response['message'] = 'Gagal pembelian member. Point anda tidak terpotong';
                                                        }
                                                    }
                                                    else{
                                                        $response['message'] = $result['Message'];
                                                    }
                                                }
                                                else{
                                                    $response['message'] = 'Failed, please contact administrator';
                                                }
                                            }
                                            else{
                                                $response['message'] = 'Your point not enough';
                                            }
                                        }
                                        else{
                                            $response['message'] = 'Product not found';
                                        }
                                    }
                                    else{
                                        $response['message'] = $result['Message'];
                                    }
                                }
                                else{
                                    $response['message'] = 'Try again, please contact administrator';
                                }
                            }
                            else{
                                $response['message'] = 'Card Not Found';
                            }
                        }
                        else{
                            $response['message'] = 'Invalid Found';
                        }
                    }
                    else{
                        $yv = "select* from ".$zzz[0]['DatabaseName'].".dbo.LocationParkingProduct where ProductID = '$productid' and Status = 1";
                        $zv =  Yii::app()->db->createCommand($yv)->queryAll();
                        if($zv != null){
                            $locationid = $zv[0]['LocationID'];
                            $vehicleid = $zv[0]['VehicleID'];
                            $jumlah = $zv[0]['Jumlah'];

                            $yx = "select* from ".$zzz[0]['DatabaseName'].".dbo.LocationQuota where LocationID = '$locationid' and VehicleID = '$vehicleid' and Quota > 0";
                            $zx =  Yii::app()->db->createCommand($yx)->queryAll();
                            if($zx != null){
                                $y = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardid' and Status = 1";
                                $z =  Yii::app()->db->createCommand($y)->queryAll();
                                if($z != null){
                                    $cardno = $z[0]['CardNo'];

                                    $masa = date('Y-m-d');
                                    $reg = 0;
                                    $yt = "select top 1 * from ".$zzz[0]['DatabaseName'].".dbo.MemberLocation where CardNo = '$cardno' and LocationID = '$locationid' order by ValidDate desc";
                                    $zt =  Yii::app()->db->createCommand($yt)->queryAll();
                                    if($zt != null){
                                        $bln = date('m', strtotime($zt[0]['ValidDate']));
                                        $vehicleloc = $zt[0]['VehicleID'];

                                        if($month <= $bln){
                                            $masa = $zt[0]['ValidDate'];
                                        }
                                        else{
                                            $masa = date('Y').'-'.$month.'-01';
                                        }

                                        if($vehicleid <> $vehicleloc){
                                          $reg = 1;
                                        }
                                    }
                                    else{
                                        if($month < date('m')){
                                            $masa = date('Y', strtotime('+1 year')).'-'.$month.'-01';
                                        }
                                        else{
                                            $masa = date('Y').'-'.$month.'-01';
                                        }
                                    }

                                    $yearu = date('Y',strtotime($masa));
                                    $yearc = date('Y', strtotime('+1 year'));

                                    if($reg == 1){
                                        $response['message'] = 'Pembelian produk member tidak sesuai dengan jenis kartu';
                                    }
                                    else{
                                        $yi = "select top 1 * from ".$zzz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '$cardno' and LocationID = '$locationid' and VehicleID = '$vehicleid' and Year = '$yearu' and Bulan = '$month' and Status = 1";
                                        $zi =  Yii::app()->db->createCommand($yi)->queryAll();
                                        if($zi == null){
                                            $totalbulan = ($month+$jumlah)-1;

                                                    $bvc = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardno' and Status = 1";
                                                    $gfd =  Yii::app()->db->createCommand($bvc)->queryAll();
                                                    if($gfd != null){
                                                        $balance = $gfd[0]['Point'];

                                                        if($zv[0]['JumlahPoinDeduct'] <= $balance){
                                                            $qwe = "exec ".$zzz[0]['DatabaseName'].".dbo.ProsesBuyMemberCard '$uid','$mid','$productid','$cardno','$masa'";
                                                            $asd =  Yii::app()->db->createCommand($qwe);
                                                            if($asd->execute()){
                                                                //insert bulan member
                                                                for($a=$month;$a<=$totalbulan;$a++){
                                                                    if($a > 12){
                                                                        $bln = $a - 12;
                                                                        $year = date('Y', strtotime('+1 year'));
                                                                    }
                                                                    else{
                                                                        $bln = $a;
                                                                        $year = date('Y');
                                                                    }

                                                                    $qwes = "exec ".$zzz[0]['DatabaseName'].".dbo.ProsesBuyMemberMonth '$cardno','$locationid','$vehicleid',$bln,'$year'";
                                                                    $asds =  Yii::app()->db->createCommand($qwes);
                                                                    $asds->execute();
                                                                }

                                                                $response['status'] = true;
                                                                $response['message'] = 'Pembelian member berhasil dilakukan';
                                                            }
                                                            else{
                                                                $response['message'] = 'Gagal pembelian member. Point anda tidak terpotong';
                                                            }
                                                        }
                                                        else{
                                                            $response['message'] = 'Your point not enough';
                                                        }
                                                    }
                                                    else{
                                                        $response['message'] = 'Card not valid. Please use another card';
                                                    }
                                        }
                                        else{
                                            $response['message'] = 'Anda telah melakukan pembelian bulan '.$month.'. Silahkan pilih bulan lain';
                                        }
                                    }
                                }
                                else{
                                    $response['message'] = 'Card Not Found';
                                }
                            }
                            else{
                                $response['message'] = 'Slot Not Available. Please choose another product';
                            }
                        }
                        else{
                            $response['message'] = 'Product member Not Found';
                        }
                    }
                }
                else{
                    $response['message'] = 'User Not Found';
                }
            }
            else{
                $response['message'] = 'KID Not Found';
            }

            //save log api
            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetAllLocation(){
        $response = array();

        if(isset($_GET['KID'])){
            $kid = $_GET['KID'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){

                $yy = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationMerchant where Status = 1";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $data = array(
                            'LocationID'=>$z['LocationID'],
                            'Nama'=>$z['Nama'],
                            'Alamat'=>$z['Alamat'],
                            'Telepon'=>$z['Telepon'],
                            'Deskripsi'=>$z['Deskripsi'],
                            'Gambar'=>'https://baronang.com/'.$z['Gambar'],
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $response['Message'] = 'Empty';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetProductLocation(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['LocationID']) and isset($_GET['Type'])){
            $kid = $_GET['KID'];
            $locationid = $_GET['LocationID'];
            $type = $_GET['Type'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $satuan = '';
                if($type == 0){
                    $satuan = 'Hari';
                }

                if($type == 1){
                    $satuan = 'Bulan';
                }

                //cek url hit
                $abc = "select* from ".$zzz[0]['DatabaseName'].".dbo.URLHitProduct where LocationID = '$locationid' and Type = '".$type."' and Status = 1";
                $def =  Yii::app()->db->createCommand($abc)->queryAll();
                if($def != null){
                    $parking = new Parking();
                    if($type == 1){
                        $result = $parking->getProductMonthly($kid, $locationid);
                    }
                    else{
                        $result = $parking->getProductDaily($kid, $locationid);
                    }

                    if($result != null){
                        if($result['Status']){
                            if($result['Data'] != null){
                                foreach($result['Data'] as $data){
                                    $harga = $data['Harga'];
                                    $hargapoint = 0;
                                    $afg = "select top 1 * from ".$zzz[0]['DatabaseName'].".dbo.ParkingSetting";
                                    $zcv =  Yii::app()->db->createCommand($afg)->queryAll();
                                    if($zcv != null){
                                        $value = $zcv[0]['ValueConversion'];

                                        $hargapoint = round($harga/$value);
                                    }

                                    $d = array(
                                        'ProductID'=>$data['ProductID'],
                                        'Nama'=>$data['Nama'],
                                        'Jenis'=>$data['Jenis'],
                                        'Quota'=>$data['Quota'],
                                        'Deskripsi'=>$data['Deskripsi'],
                                        'Harga'=>$data['Harga'],
                                        'MasaBerlaku'=>$data['MasaBerlaku'],
                                        'JumlahPoint'=>$hargapoint,
                                        'Tipe'=>$satuan,
                                        'Gambar'=>$data['Gambar'],
                                    );

                                    array_push($response, $d);
                                }
                            }
                            else{
                                $data = array(
                                    'ProductID'=>'',
                                    'Nama'=>'Kosong',
                                    'Jenis'=>'',
                                    'Quota'=>'',
                                    'Deskripsi'=>'Silahkan coba lokasi lain',
                                    'Harga'=>'',
                                    'MasaBerlaku'=>'',
                                    'JumlahPoint'=>'',
                                    'Tipe'=>'',
                                    'Gambar'=>'',
                                );

                                array_push($response, $data);
                            }
                        }
                        else{
                            $data = array(
                                'ProductID'=>'',
                                'Nama'=>'Kosong',
                                'Jenis'=>'',
                                'Quota'=>'',
                                'Deskripsi'=>'Silahkan coba lokasi lain',
                                'Harga'=>'',
                                'MasaBerlaku'=>'',
                                'JumlahPoint'=>'',
                                'Tipe'=>'',
                                'Gambar'=>'',
                            );

                            array_push($response, $data);
                        }
                    }
                    else{
                        $data = array(
                            'ProductID'=>'',
                            'Nama'=>'Kosong',
                            'Jenis'=>'',
                            'Quota'=>'',
                            'Deskripsi'=>'Silahkan coba lokasi lain',
                            'Harga'=>'',
                            'MasaBerlaku'=>'',
                            'JumlahPoint'=>'',
                            'Tipe'=>'',
                            'Gambar'=>'',
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $yy = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationParkingProduct where LocationID = '$locationid' and Tipe = '$type' and Status = 1 order by VehicleID asc";
                    $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                    if($zz != null){
                        foreach ($zz as $z){
                            $vehicleid = $z['VehicleID'];
                            $productid = $z['ProductID'];

                            $jenis = '';
                            $quota = 0;
                            $yq = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.VehicleType where VehicleID = '$vehicleid' and Status = 1";
                            $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                            if($zq != null){
                                $jenis = $zq[0]['Name'];

                                $ye = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationQuota where LocationID = '$locationid' and VehicleID = '$vehicleid'";
                                $ze =  Yii::app()->db->createCommand($ye)->queryAll();
                                if($ze != null){
                                    $quota = $ze[0]['Quota'];
                                }
                            }

                            $diskon = 0;
                            $point = 0;
                            $tanggal = date('Y-m-d');
                            $yk = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationParkingProductDiscount where ProductID = '$productid' and convert(date, StartDate) <= '$tanggal' and convert(date, EndDate) >= '$tanggal' and Status = 1";
                            $zk =  Yii::app()->db->createCommand($yk)->queryAll();
                            if($zk != null){
                                $diskon = $zk[0]['Amount'];
                                $point = $zk[0]['Point'];
                            }

                            $harga = $z['Harga'] - $diskon;
                            $deductpoint = $z['JumlahPoinDeduct'] - $point;

                            $data = array(
                                'ProductID'=>$z['ProductID'],
                                'Nama'=>$z['Nama'],
                                'Jenis'=>$jenis,
                                'Quota'=>$quota,
                                'Deskripsi'=>$z['Deskripsi'],
                                'Harga'=>number_format($harga),
                                'MasaBerlaku'=>number_format($z['Jumlah']),
                                'JumlahPoint'=>number_format($deductpoint),
                                'Tipe'=>$satuan,
                                'Gambar'=>'https://baronang.com/'.$z['Gambar'],
                            );

                            array_push($response, $data);
                        }
                    }
                    else{
                        $data = array(
                            'ProductID'=>'',
                            'Nama'=>'Kosong',
                            'Jenis'=>'',
                            'Quota'=>'',
                            'Deskripsi'=>'Silahkan coba lokasi lain',
                            'Harga'=>'',
                            'MasaBerlaku'=>'',
                            'JumlahPoint'=>'',
                            'Tipe'=>'',
                            'Gambar'=>'https://baronang.com/',
                        );

                        array_push($response, $data);
                    }
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetAllProductLocation(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['LocationID'])){
            $kid = $_GET['KID'];
            $locationid = $_GET['LocationID'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $yy = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationParkingProduct where LocationID = '$locationid' and Status = 3 order by VehicleID asc";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $vehicleid = $z['VehicleID'];

                        $jenis = '';
                        $quota = 0;
                        $yq = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.VehicleType where VehicleID = '$vehicleid' and Status = 1";
                        $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                        if($zq != null){
                            $jenis = $zq[0]['Name'];

                            $ye = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationQuota where LocationID = '$locationid' and VehicleID = '$vehicleid'";
                            $ze =  Yii::app()->db->createCommand($ye)->queryAll();
                            if($ze != null){
                                $quota = $ze[0]['Quota'];
                            }
                        }

                        $data = array(
                            'ProductID'=>$z['ProductID'],
                            'Nama'=>$z['Nama'],
                            'Jenis'=>$jenis,
                            'Quota'=>$quota,
                            'Deskripsi'=>$z['Deskripsi'],
                            'Harga'=>number_format($z['Harga']),
                            'MasaBerlaku'=>number_format($z['Jumlah']),
                            'JumlahPoint'=>number_format($z['JumlahPoinDeduct']),
                            'Gambar'=>'https://baronang.com/'.$z['Gambar'],
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $response['Message'] = 'Empty';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function generateBarcode($cardno){
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($cardno, $cipher, '70007000', $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, '70007000', $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
        if($ciphertext != ''){
            return $ciphertext;
        }
    }

    public function actionGetMemberLocation(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['CardNo'])){
            $kid = $_GET['KID'];
            $cardno = $_GET['CardNo'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $yy = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.MemberLocation where CardNo = '$cardno' and Status = 1";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $vehicleid = $z['VehicleID'];
                        $locationid = $z['LocationID'];
                        $validdate = $z['ValidDate'];
                        $fromdate = $z['DateRegister'];

                        $jenis = '';
                        $yq = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.VehicleType where VehicleID = '$vehicleid' and Status = 1";
                        $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                        if($zq != null){
                            $jenis = $zq[0]['Name'];
                        }

                        $lokasi = '';
                        $yw = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '$locationid' and Status = 1";
                        $zw =  Yii::app()->db->createCommand($yw)->queryAll();
                        if($zw != null){
                            $lokasi = $zw[0]['Nama'];
                        }

                        $data = array(
                            'LocationID'=>$locationid,
                            'LocationName'=>$lokasi,
                            'CardNo'=>$cardno,
                            'VehicleName'=>$jenis,
                            'ValidDate'=>date('d-m-Y', strtotime($validdate)),
                            'FromDate'=>date('d-m-Y', strtotime($fromdate))
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $response['Message'] = 'Empty';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetMemberLocationMonth(){
        $response = array(
            'LocationID'=>'',
            'LocationName'=>'',
            'CardNo'=>'',
            'Status'=>false,
            'Message'=>'',
            '1'=>false,
            '2'=>false,
            '3'=>false,
            '4'=>false,
            '5'=>false,
            '6'=>false,
            '7'=>false,
            '8'=>false,
            '9'=>false,
            '10'=>false,
            '11'=>false,
            '12'=>false,
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['LocationID']) and isset($_REQUEST['CardNo'])){
            $kid = $_REQUEST['KID'];
            $locationid = $_REQUEST['LocationID'];
            $cardno = $_REQUEST['CardNo'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $yy = "select top 1 * FROM ".$zzz[0]['DatabaseName'].".dbo.MemberLocation where CardNo = '$cardno' and LocationID = '$locationid' and Status = 1";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    $lokasi = '';
                    $yw = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '$locationid' and Status = 1";
                    $zw =  Yii::app()->db->createCommand($yw)->queryAll();
                    if($zw != null){
                        $lokasi = $zw[0]['Nama'];
                    }

                    $year = date('Y');
                    $y = "select * FROM ".$zzz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '$cardno' and LocationID = '$locationid' and VehicleID = '".$zz[0]['VehicleID']."' and Year = '$year' and Status = 1";
                    $z =  Yii::app()->db->createCommand($y)->queryAll();
                    if($z != null){
                        $response['LocationID'] = $locationid;
                        $response['LocationName'] = $lokasi;
                        $response['CardNo'] = $cardno;
                        $response['Status'] = true;
                        $response['Message'] = 'Member';

                        foreach ($z as $zx){
                            $response[$zx['Bulan']] = true;
                        }
                    }
                    else{
                        $response['Message'] = 'Location Member Month Not Found';
                    }
                }
                else{
                    $response['Message'] = 'Empty';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetMemberLocationMonthByYear(){
        $response = array(
            'LocationID'=>'',
            'LocationName'=>'',
            'CardNo'=>'',
            'Status'=>false,
            'Message'=>'',
            '1'=>false,
            '2'=>false,
            '3'=>false,
            '4'=>false,
            '5'=>false,
            '6'=>false,
            '7'=>false,
            '8'=>false,
            '9'=>false,
            '10'=>false,
            '11'=>false,
            '12'=>false,
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['LocationID']) and isset($_REQUEST['CardNo']) and isset($_REQUEST['Year'])){
            $kid = $_REQUEST['KID'];
            $locationid = $_REQUEST['LocationID'];
            $cardno = $_REQUEST['CardNo'];
            $year = $_REQUEST['Year'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $yy = "select top 1 * FROM ".$zzz[0]['DatabaseName'].".dbo.MemberLocation where CardNo = '$cardno' and LocationID = '$locationid' and Status = 1";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    $lokasi = '';
                    $yw = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '$locationid' and Status = 1";
                    $zw =  Yii::app()->db->createCommand($yw)->queryAll();
                    if($zw != null){
                        $lokasi = $zw[0]['Nama'];
                    }

                    $y = "select * FROM ".$zzz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '$cardno' and LocationID = '$locationid' and VehicleID = '".$zz[0]['VehicleID']."' and Year = '$year' and Status = 1";
                    $z =  Yii::app()->db->createCommand($y)->queryAll();
                    if($z != null){
                        $response['LocationID'] = $locationid;
                        $response['LocationName'] = $lokasi;
                        $response['CardNo'] = $cardno;
                        $response['Status'] = true;
                        $response['Message'] = 'Member';

                        foreach ($z as $zx){
                            $response[$zx['Bulan']] = true;
                        }
                    }
                    else{
                        $response['Message'] = 'Card Not Member Year '.$year;
                    }
                }
                else{
                    $response['Message'] = 'Empty';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetProduct(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['ProductID'])){
            $kid = $_GET['KID'];
            $productid = $_GET['ProductID'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){

                $yy = "select top 1 * FROM ".$zzz[0]['DatabaseName'].".dbo.LocationParkingProduct where ProductID = '$productid' and Status = 1";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    $vehicleid = $zz[0]['VehicleID'];
                    $locationid = $zz[0]['LocationID'];
                    $type = $zz[0]['Tipe'];

                    $jenis = '';
                    $quota = 0;
                    $yq = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.VehicleType where VehicleID = '$vehicleid' and Status = 1";
                    $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                    if($zq != null){
                        $jenis = $zq[0]['Name'];

                        $ye = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationQuota where LocationID = '$locationid' and VehicleID = '$vehicleid'";
                        $ze =  Yii::app()->db->createCommand($ye)->queryAll();
                        if($ze != null){
                            $quota = $ze[0]['Quota'];
                        }
                    }

                    $diskon = 0;
                    $point = 0;
                    $tanggal = date('Y-m-d');
                    $yk = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationParkingProductDiscount where ProductID = '$productid' and convert(date, StartDate) <= '$tanggal' and convert(date, EndDate) >= '$tanggal' and Status = 1";
                    $zk =  Yii::app()->db->createCommand($yk)->queryAll();
                    if($zk != null){
                        $diskon = $zk[0]['Amount'];
                        $point = $zk[0]['Point'];
                    }

                    $harga = $zz[0]['Harga'] - $diskon;
                    $deductpoint = $zz[0]['JumlahPoinDeduct'] - $point;

                    $data = array(
                        'ProductID'=>$zz[0]['ProductID'],
                        'Nama'=>$zz[0]['Nama'],
                        'Jenis'=>$jenis,
                        'Quota'=>$quota,
                        'Deskripsi'=>$zz[0]['Deskripsi'],
                        'Harga'=>number_format($harga),
                        'MasaBerlaku'=>$zz[0]['Jumlah'],
                        'JumlahPoint'=>number_format($deductpoint),
                        'Gambar'=>'https://baronang.com/'.$zz[0]['Gambar'],
                    );

                    array_push($response, $data);
                }
                else{
                    $response['Message'] = 'Empty';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetSlot(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['LocationID'])){
            $kid = $_GET['KID'];
            $locationid = $_GET['LocationID'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){

                $yy = "select top 1 * FROM ".$zzz[0]['DatabaseName'].".dbo.LocationQuota where LocationID = '$locationid'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    $vehicleid = $zz[0]['VehicleID'];

                    $jenis = '';
                    $yq = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.VehicleType where VehicleID = '$vehicleid' and Status = 1";
                    $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                    if($zq != null){
                        $jenis = $zq[0]['Name'];
                    }

                    $locationname = '';
                    $yw = "select* FROM ".$zzz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '$locationid' and Status = 1";
                    $zw =  Yii::app()->db->createCommand($yw)->queryAll();
                    if($zw != null){
                        $locationname = $zw[0]['Nama'];
                    }

                    $data = array(
                        'LocationID'=>$zz[0]['LocationID'],
                        'LocationName'=>$locationname,
                        'Jenis'=>$jenis,
                        'Quota'=>$zz[0]['Quota']
                    );

                    array_push($response, $data);
                }
                else{
                    $response['Message'] = 'Empty';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionOpenEDC(){
        $response = array(
          'status'=>FALSE,
          'message'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID']) and isset($_REQUEST['MemberID'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];
            $mid = $_REQUEST['MemberID'];

            $yyy = "select* from Gateway.dbo.EDCList where KID = '$kid' and UserIDBaronang = '$uid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz == null){
                $sql = "exec dbo.ProsesEDCList '".$kid."','".$uid."','".$mid."','',''";
                $exec =  Yii::app()->db->createCommand($sql);
                if($exec->execute()){
                    $response['status'] = true;
                    $response['message'] = 'Berhasil membuat EDC';
                }
                else{
                    $response['message'] = 'Gagal membuat EDC';
                }
            }
            else{
                $response['message'] = 'Anda sudah mempunyai EDC. Hanya dapat membuat 1x';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionGetEDC(){
        $response = array();

        $edc = '';
        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];

            $yyy = "select TOP 1 * from Gateway.dbo.EDCList where KID = '$kid' and UserIDBaronang = '$uid' and Status = 1 order by SerialNumber desc";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $edc = $zzz[0]['SerialNumber'];
                $message = 'Found';
            }
            else{
                $message = 'Kosong';
            }
        }
        else{
            $message = 'Invalid require data';
        }

        $data = array(
            'edc'=>$edc,
            'message'=>$message
        );

        array_push($response, $data);

        echo json_encode($response);
    }

    public function actionCekLogin(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'uid'=>'',
            'mid'=>'',
            'email'=>'',
            'password'=>'',
            'konfirmasi'=>'',
            'nama'=>'',
            'telp'=>'',
            'alamat'=>'',
            'ktp'=>'',
            'balance'=>'',
            'tipe'=>'',
            'merchant'=>'',
            'edc'=>'',
        );

        if(isset($_REQUEST['kid']) and isset($_REQUEST['email']) and isset($_REQUEST['password'])){
            $kid = $_REQUEST['kid'];
            $email = $_REQUEST['email'];
            $password = $_REQUEST['password'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $s = "select* from PaymentGateway.dbo.UserPaymentGateway where email = '".$email."' and Pass = '".md5($password)."'";
            $e =  Yii::app()->db->createCommand($s)->queryAll();
            if($e != null){
                $status = $e[0]['Status'];
                $em = $e[0]['email'];
                $pass = $e[0]['Pass'];
                $uid = $e[0]['KodeUser'];
                $name = $e[0]['NamaUser'];
                $konfirmasi = $e[0]['Status'];
                $telp = $e[0]['Telp'];
                $address = $e[0]['Alamat'];
                $balance = number_format($e[0]['Balance']);
                $ktp = $e[0]['KTP'];
                $type = $e[0]['TipeAcc'];

                $mid = '';
                $ss = "select* from PaymentGateway.dbo.UserMemberKoperasi where KID = '".$kid."' and UserID = '".$uid."' and Status = 1";
                $ee =  Yii::app()->db->createCommand($ss)->queryAll();
                if($ee != null){
                    $mid = $ee[0]['KodeMember'];
                }

                $merchant = 0;
                $edc = '';
                $sss = "select* from PaymentGateway.dbo.MerchantParking where UserIDBaronang = '".$uid."' and Status = 1";
                $eee =  Yii::app()->db->createCommand($sss)->queryAll();
                if($eee != null){
                    $merchant = 1;

                    $sz = "select TOP 1 * from Gateway.dbo.EDCList where UserIDBaronang = '".$uid."' and Status = 1 and KID = '".$kid."' order by SerialNumber desc";
                    $ez =  Yii::app()->db->createCommand($sz)->queryAll();
                    if($ez != null){
                        $edc = $ez[0]['SerialNumber'];
                    }

                }

                $response['status'] = true;
                $response['uid'] = $uid;
                $response['mid'] = $mid;
                $response['email'] = $em;
                $response['password'] = $password;
                $response['nama'] = $name;
                $response['telp'] = $telp;
                $response['konfirmasi'] = $konfirmasi;
                $response['alamat'] = $address;
                $response['ktp'] = $ktp;
                $response['balance'] = $balance;
                $response['tipe'] = $type;
                $response['merchant'] = $merchant;
                $response['edc'] = $edc;
                $response['message'] = 'Berhasil masuk';
            }
            else{
                $response['message'] = 'Pengguna tidak ditemukan';
            }

            if($id != ''){
                $msg = $response['message'];
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }

        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionSaveRegister(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['kid']) and isset($_REQUEST['ktp']) and isset($_REQUEST['nama']) and isset($_REQUEST['telepon']) and isset($_REQUEST['email']) and isset($_REQUEST['alamat'])
            and isset($_REQUEST['password']) and isset($_REQUEST['repassword']) and isset($_REQUEST['pin']) and isset($_REQUEST['repin'])){
            $tipe = 0;
            $ktp = $_REQUEST['ktp'];
            $kid = $_REQUEST['kid'];
            $nama = $_REQUEST['nama'];
            $email = $_REQUEST['email'];
            $telp = $_REQUEST['telepon'];
            $alamat = $_REQUEST['alamat'];
            $password = md5($_REQUEST['password']);
            $pin = md5($_REQUEST['pin']);

            $param = $email.','.$telp;

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $a = "select* from PaymentGateway.dbo.UserPaymentGateway where Telp = '".$telp."' or email = '".$email."'";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b == null){

                if ($tipe == 0) {
                  $aa = "select PaymentGateway.dbo.getUserID()";
                  $bb = Yii::app()->db->createCommand($aa)->queryAll();
                }
                else {
                  $aa = "select PaymentGateway.dbo.getUserIDCorp()";
                  $bb = Yii::app()->db->createCommand($aa)->queryAll();
                }

                if($bb != null){
                    $codes = $bb[0][''];

                    $secretCode = '';
                    $ga = new GoogleAuthenticator();
                    $secret = $ga->createSecret();
                    if($secret != null){
                        $secretCode = $secret;
                    }
                    else{
                        $secretCode = md5('Y/m/d H:i:s');
                    }

                    $link = 'https://baronang.com/api/parking/confirmation/id/'.$secretCode;

                    $sql = "exec PaymentGateway.dbo.ProsesUserPaymentGateway '$codes', '$nama','$email','$telp','$alamat','$password','0','0','0','$ktp','$secretCode','$pin',$tipe";
                    $exec = Yii::app()->db->createCommand($sql);
                    if($exec->execute()){
                        $sql2 = "exec PaymentGateway.dbo.ProsesLogMail '$email', 0";
                        $exec2 = Yii::app()->db->createCommand($sql2);
                        $exec2->execute();

                        $subject = 'Sky Parking Register Confirmation';
                        $message = "<p>
                          Hi! ".$nama.",<br><br>
                          Terima kasih telah bergabung dengan Sky Parking.<br>
                          Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
                          <a href=".$link.">Konfirmasi</a>
                          <br>
                          <br>
                          <br>
                          <br>
                          Terima kasih.<br>
                          Salam,<br>
                          <br>
                          <br>
                          Sky Parking
                          <br>
                          <i>Powered by Baronang</i>
                        </p>";

                        $curl = curl_init();
                        $post = "toaddr=$email&subject=$subject&message=$message";
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://interzircon.com/phpmail.php",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => $post,
                        ));
                        $output = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);
                        $result = json_decode($output, true);
                        if($err) {
                            $response['status'] = true;
                            $response['message'] = 'Registrasi berhasil dilakukan. Email gagal dikirim ke email '.$email.' anda';
                        }
                        else {
                            $response['status'] = true;

                            $qwe = "update PaymentGateway.dbo.LogMail set Status = 1 where Email='".$email."' and Type = 0";
                            $asd =  Yii::app()->db->createCommand($qwe);
                            if($asd->execute()){
                                $response['message'] = 'Registrasi berhasil dilakukan. Harap segera konfirmasi email '.$email.' anda';
                            }
                            else{
                                $response['message'] = 'Registrasi berhasil dilakukan. Harap segera konfirmasi email '.$email.' anda';
                                $response['errorCode'] = '(eX0342)';
                            }
                        }
                    }
                    else{
                        $response['message'] = 'Registrasi gagal dilakukan';
                        $response['errorCode'] = '(eX0001)';
                    }
                }
                else{
                    $response['message'] = 'Registrasi gagal dilakukan';
                    $response['errorCode'] = '(eX0002)';
                }
            }

            if($b != null){
                $gac = $b[0]['Gac'];
                $email = $b[0]['email'];
                $nama = $b[0]['NamaUser'];

                $link = 'https://baronang.com/api/parking/confirmation/id/'.$gac;

                $sql2 = "exec PaymentGateway.dbo.ProsesLogMail '$email', 0";
                $exec2 = Yii::app()->db->createCommand($sql2);
                $exec2->execute();

                $subject = 'Sky Parking Register Confirmation';
                $message = "<p>
                  Hi! ".$nama.",<br><br>
                  Terima kasih telah bergabung dengan Sky Parking.<br>
                  Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
                  <a href=".$link.">Konfirmasi</a>
                  <br>
                  <br>
                  <br>
                  <br>
                  Terima kasih.<br>
                  Salam,<br>
                  <br>
                  <br>
                  Sky Parking
                  <br>
                  <i>Powered by Baronang</i>
                </p>";

                $curl = curl_init();
                $post = "toaddr=$email&subject=$subject&message=$message";
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://interzircon.com/phpmail.php",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $post,
                ));
                $output = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                $result = json_decode($output, true);
                if($err) {
                    $response['message'] = 'Registrasi gagal dilakukan. Email gagal dikirim ke email '.$email.' anda';
                }
                else {
                    $response['status'] = true;

                    $qwe = "update PaymentGateway.dbo.LogMail set Status = 1 where Email='".$email."' and Type = 0";
                    $asd =  Yii::app()->db->createCommand($qwe);
                    if($asd->execute()){
                        $response['message'] = 'Registrasi berhasil dilakukan. Harap segera konfirmasi email '.$email.' anda';
                    }
                    else{
                        $response['message'] = 'Registrasi berhasil dilakukan. Harap segera konfirmasi email '.$email.' anda';
                        $response['errorCode'] = '(eX0342)';
                    }
                }
            }

            if($id != ''){
                $msg = json_encode($response);

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX1111)';
        }

        echo json_encode($response);
    }

    public function actionCekForgotPassword(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['email'])){
            $param = $_REQUEST['email'];

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $a = "select* from PaymentGateway.dbo.UserPaymentGateway where email = '".$_REQUEST['email']."' or Telp = '".$_REQUEST['email']."'";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b != null){
                $email = $b[0]['email'];
                $nama = $b[0]['NamaUser'];
                $gac = $b[0]['Gac'];
                //generator
                $rand = rand(100000,999999);
                $newpass = md5($rand);

                $sql = "exec PaymentGateway.dbo.ProsesLogMail '$email', 1";
                $exec = Yii::app()->db->createCommand($sql);
                if($exec->execute()){
                    $link = 'https://baronang.com/api/parking/confirmationPassword/id/'.$gac.'/n/'.$newpass;

                    $subject = "Sky Parking Forgot Password";
                    $message = "<p>
                      Hi! ".$nama.",<br><br>
                      Anda telah melakukan permintaan lupa password.<br>
                      Kami perlu memastikan bahwa anda yang melakukan permintaan. Harap mengkonfirmasi dengan mengklik link dibawah ini.<br>
                      <a href='".$link."'>Konfirmasi</a><br>
                      Jika anda mengkonfirmasi, password baru anda adalah : ".$rand."<br><br>
                      Harap segera mengganti password anda.<br>
                      Abaikan pesan ini jika anda tidak melakukan permintaan lupa password.
                      <br>
                      <br>
                      <br>
                      <br>
                      Terima kasih.<br>
                      Salam,<br>
                      <br>
                      <br>
                      Sky Parking
                      <br>
                      <i>Powered by Baronang</i>
                    </p>";

                    $curl = curl_init();
                    $post = "toaddr=$email&subject=$subject&message=$message";
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://interzircon.com/phpmail.php",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $post,
                    ));
                    $output = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    $result = json_decode($output, true);
                    if($err) {
                        $response['message'] = 'Lupa password gagal dilakukan. Email gagal dikirim ke email '.$this->grepEmail($email).' anda';
                    }
                    else {
                        $response['status'] = true;

                        $qwe = "update PaymentGateway.dbo.LogMail set Status = 1 where Email='".$email."' and Type = 1";
                        $asd =  Yii::app()->db->createCommand($qwe);
                        if($asd->execute()){
                            $response['message'] = 'Email konfirmasi lupa password telah dikirim ke email '.$this->grepEmail($email).' anda';
                        }
                        else{
                            $response['message'] = 'Email konfirmasi lupa password telah dikirim ke email '.$this->grepEmail($email).' anda';
                            $response['errorCode'] = '(eX0332)';
                        }
                    }
                }
                else{
                  $response['message'] = 'Email konfirmasi gagal dikirim';
                }
            }
            else{
                $response['message'] = 'Email tidak ditemukan';
                $response['errorCode'] = '(eX0004)';
            }

            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX2222)';
        }

        echo json_encode($response);
    }

    public function actionCekForgotPIN(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['email']) and isset($_REQUEST['password'])){
            $password = md5($_REQUEST['password']);

            $param = $_REQUEST['email'].','.$password;

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $a = "select* from PaymentGateway.dbo.UserPaymentGateway where email = '".$_REQUEST['email']."' and Pass = '".$password."'";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b != null){
                $email = $b[0]['email'];
                $nama = $b[0]['NamaUser'];
                $gac = $b[0]['Gac'];
                //generator
                $rand = rand(100000,999999);
                $newpass = md5($rand);

                $sql = "exec PaymentGateway.dbo.ProsesLogMail '$email', 1";
                $exec = Yii::app()->db->createCommand($sql);
                if($exec->execute()){
                  $link = 'https://baronang.com/api/parking/confirmationPin/id/'.$gac.'/n/'.$newpass;

                  $subject = "Sky Parking Forgot Pin";
                  $message = "<p>
                    Hi! ".$nama.",<br><br>
                    Anda telah melakukan permintaan lupa PIN.<br>
                    Kami perlu memastikan bahwa anda yang melakukan permintaan. Harap mengkonfirmasi dengan mengklik link dibawah ini.<br>
                    <a href='".$link."'>Konfirmasi</a><br>
                    Jika anda mengkonfirmasi, PIN baru anda adalah : ".$rand."<br><br>
                    Harap segera mengganti password anda.<br>
                    Abaikan pesan ini jika anda tidak melakukan permintaan lupa PIN.
                    <br>
                    <br>
                    <br>
                    <br>
                    Terima kasih.<br>
                    Salam,<br>
                    <br>
                    <br>
                    Sky Parking
                    <br>
                    <i>Powered by Baronang</i>
                  </p>";

                  $curl = curl_init();
                  $post = "toaddr=$email&subject=$subject&message=$message";
                  curl_setopt_array($curl, array(
                      CURLOPT_URL => "http://interzircon.com/phpmail.php",
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 30,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => $post,
                  ));
                  $output = curl_exec($curl);
                  $err = curl_error($curl);
                  curl_close($curl);
                  $result = json_decode($output, true);
                  if($err) {
                      $response['message'] = 'Lupa pin gagal dilakukan. Email gagal dikirim ke email '.$email.' anda';
                  }
                  else {
                      $response['status'] = true;

                      $qwe = "update PaymentGateway.dbo.LogMail set Status = 1 where Email='".$email."' and Type = 2";
                      $asd =  Yii::app()->db->createCommand($qwe);
                      if($asd->execute()){
                          $response['message'] = 'Email konfirmasi lupa PIN telah dikirim ke email '.$email.' anda';
                      }
                      else{
                          $response['message'] = 'Email konfirmasi lupa PIN telah dikirim ke email '.$email.' anda';
                          $response['errorCode'] = '(eX0332)';
                      }
                  }
                }
                else{
                  $response['message'] = 'Email konfirmasi gagal dikirim';
                }
            }
            else{
                $response['message'] = 'Pengguna tidak ditemukan';
                $response['errorCode'] = '(eX0005)';
            }

            if($id != ''){
                $msg = json_encode($response);

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX3373)';
        }

        echo json_encode($response);
    }

    public function actionGetCardStatus(){
        $response = array(
            'Status'=>FALSE,
            'StatusCard'=>'',
            'Message'=>'Info kartu',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['Barcode'])){
            $kid = $_REQUEST['KID'];
            $barcode = str_replace(' ','+',$_REQUEST['Barcode']);

            $parking = new Parking();
            $cardNo = $parking->decrypt($barcode);

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $aa = "select* from ".$zzz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '$cardNo'";
                $ab =  Yii::app()->db->createCommand($aa)->queryAll();
                if($ab != null){
                    $bb = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardNo'";
                    $vv =  Yii::app()->db->createCommand($bb)->queryAll();
                    if($vv != null){
                        $status = $vv[0]['Status'];

                        $dd = "select* from ".$zzz[0]['DatabaseName'].".dbo.ParkingList where CardNo = '$cardNo' and Status = 0";
                        $ee =  Yii::app()->db->createCommand($dd)->queryAll();
                        if($ee != null){
                            $locationIn = $ee[0]['LocationIn'];

                            $locationname = '';
                            $ff = "select* from ".$zzz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '$locationIn'";
                            $gg =  Yii::app()->db->createCommand($ff)->queryAll();
                            if($gg != null){
                                $locationname = $gg[0]['Nama'];
                            }

                            $statusCard = 1;
                            $message = 'Kartu sedang digunakan di lokasi '.$locationname;
                        }
                        else{
                            $statusCard = 0;
                            $message = 'Kartu sedang tidak digunakan';
                        }

                        if($status == 2){
                            $statusCard = 2;
                            $message = 'Kartu diblokir';
                        }

                        $response['Status'] = TRUE;
                        $response['StatusCard'] = $statusCard;
                        $response['Message'] = $message;
                    }
                    else{
                        $response['Message'] = 'Card not found';
                    }
                }
                else{
                    $response['Message'] = 'Barcode not found';
                }
            }
            else{
                $response['Message'] = 'KID not found';
            }

        }
        else{
            $response['Message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function getDateAll(){
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );

        return $d->format("Y-m-d H:i:s.u");
    }

    public function actionGetEmoneyAcc(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['MemberID'])){
            $kid = $_GET['KID'];
            $member_id = $_GET['MemberID'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek balance
                $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where MemberID = '".$member_id."' and Status <> 0 and isLink = 1";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    foreach ($exec as $exe){
                        //get Barcode
                        $barcode = $exe['CardNo'];
                        $sql3 = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$exe['CardNo']."' and Status = 1";
                        $exec3 =  Yii::app()->db->createCommand($sql3)->queryAll();
                        if($exec3 != null){
                            $barcode = $exec3[0]['Barcode'];
                        }

                        //cek Status
                        $status  = '';
                        $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.ParkingList where CardNo = '".$exe['CardNo']."' and Status = 0";
                        $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                        if($exe['Status'] == 2){
                            $status = 'Block';
                        }
                        else if($exec2 != null){
                            $status = 'In';
                        }
                        else{
                            $status = '';
                        }

                        $data = array(
                            'AccNo'=>$exe['CardNo'],
                            'Barcode'=>$barcode,
                            'Balance'=>number_format($exe['Point']),
                            'Status'=>$status
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $response['Message'] = 'Invalid Require Data';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionGetMessage(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['UserID'])){
            $kid = $_GET['KID'];
            $uid = $_GET['UserID'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){

                $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.InboxMobile where Status = 1";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    foreach ($exec as $exe){
                        $id = $exe['ID'];
                        $tanggal = date('d F Y', strtotime($exe['CreatedDate']));
                        $judul = $exe['Judul'];

                        //cek Status
                        $status  = 0;
                        $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.InboxMobileRead where InboxMobileID = '".$id."' and UserID = '".$uid."'";
                        $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                        if($exec2 != null){
                            $status = 1;
                        }

                        $data = array(
                            'MessageID'=>$id,
                            'Tanggal'=>$tanggal,
                            'Judul'=>$judul,
                            'Status'=>$status
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $response['Message'] = 'Invalid Require Data';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionGetMessageDetail(){
        $response = array(
          'status'=>FALSE,
          'message'=>'',
          'errorCode'=>'',
          'judul'=>'',
          'tanggal'=>'',
          'isi'=>'',
          'image'=>''
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['UserID']) and isset($_REQUEST['MessageID'])){
            $kid = $_REQUEST['KID'];
            $uid = $_REQUEST['UserID'];
            $message = $_REQUEST['MessageID'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){

                $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.InboxMobile where Status = 1 and ID = '".$message."'";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    $tanggal = date('d F Y', strtotime($exec[0]['CreatedDate']));
                    $judul = $exec[0]['Judul'];
                    $isi = $exec[0]['Isi'];
                    $image = $exec[0]['Image'];

                    $date = date('Y-m-d H:i:s');
                    $sql2 = "insert into ".$zz[0]['DatabaseName'].".dbo.InboxMobileRead(InboxMobileID, UserID, ReadDate)values('".$message."','".$uid."','".$date."')";
                    $exec2 =  Yii::app()->db->createCommand($sql2);
                    if($exec2->execute()){
                        $response['status'] = TRUE;
                        $response['message'] = 'Found';
                        $response['judul'] = $judul;
                        $response['tanggal'] = $tanggal;
                        $response['isi'] = $isi;

                        if($image != null){
                            $response['image'] = $image;
                        }
                    }
                    else{
                        $response['message'] = 'Try again';
                    }
                }
                else{
                    $response['message'] = 'Not found';
                }
            }
            else{
                $response['message'] = 'Empty';
            }

        }
        else{
            $response['message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionGetEmoneyAccCountRow(){
        $response = array();

        $totalRow = 0;
        if(isset($_GET['KID']) and isset($_GET['MemberID'])){
            $kid = $_GET['KID'];
            $member_id = $_GET['MemberID'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                $yv = "select count(*) as TotalRow from ".$zz[0]['DatabaseName'].".dbo.MemberCard where MemberID = '".$member_id."' and Status <> 0 and isLink = 1";
                $zv =  Yii::app()->db->createCommand($yv)->queryAll();
                if($zv != null){
                    $totalRow = $zv[0]['TotalRow'];
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        $data = array(
            'TotalRow'=>$totalRow
        );

        array_push($response, $data);

        echo json_encode($response);
    }

    public function actionGetEmoneyAccByRow(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['MemberID']) and isset($_GET['From']) and isset($_GET['To'])){
            $kid = $_GET['KID'];
            $member_id = $_GET['MemberID'];
            $from = $_GET['From'];
            $to = $_GET['To'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek balance
                $sql = "select * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY CardNo asc) as row from ".$zz[0]['DatabaseName'].".dbo.MemberCard where MemberID = '".$member_id."' and Status not in (0) and isLink = 1) a WHERE row between '".$from."' and '".$to."'";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    foreach ($exec as $exe){
                        //get Barcode
                        $barcode = $exe['CardNo'];
                        $sql3 = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$exe['CardNo']."' and Status = 1";
                        $exec3 =  Yii::app()->db->createCommand($sql3)->queryAll();
                        if($exec3 != null){
                            $barcode = $exec3[0]['Barcode'];
                        }

                        //cek Status
                        $status  = '';
                        $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.ParkingList where CardNo = '".$exe['CardNo']."' and Status = 0";
                        $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                        if($exe['Status'] == 2){
                            $status = 'Block';
                        }
                        else if($exec2 != null){
                            $status = 'In';
                        }
                        else{
                            $status = '';
                        }

                        $data = array(
                            'AccNo'=>$exe['CardNo'],
                            'Barcode'=>$barcode,
                            'Balance'=>number_format($exe['Point']),
                            'Status'=>$status
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $response['Message'] = 'Invalid Require Data';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionGetEmoneyTrans(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['AccNo'])){
            $kid = $_GET['KID'];
            $accno = $_GET['AccNo'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek balance
                $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.ParkingTrans where AccNo = '".$accno."' order by TimeStam desc";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    foreach ($exec as $exe){
                        $amount = 0;
                        $type = '';
                        if($exe['Debet'] > 0){
                            $amount = $exe['Debet'];
                            $type = 'DB';
                        }
                        else{
                          $amount = $exe['Kredit'];
                          $type = 'CR';
                        }

                        $data = array(
                            'TransNumber'=>$exe['TransactionNumber'],
                            'AccNo'=>$exe['AccNo'],
                            'Date'=>date('Y-m-d H:i:s', strtotime($exe['TimeStam'])),
                            'Note'=>$exe['Note'],
                            'Type'=>$type,
                            'Point'=>number_format($amount)
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $data = array(
                        'TransNumber'=>'',
                        'AccNo'=>'',
                        'Date'=>'Kosong',
                        'Note'=>'',
                        'Type'=>'',
                        'Point'=>''
                    );

                    array_push($response, $data);
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionGetEmoneyTransByRow(){
        $response = array();

        if(isset($_GET['KID']) and isset($_GET['AccNo']) and isset($_GET['From']) and isset($_GET['To'])){
            $kid = $_GET['KID'];
            $accno = $_GET['AccNo'];
            $from = $_GET['From'];
            $to = $_GET['To'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek balance
                $sql = "select * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY TimeStam desc) as row FROM ".$zz[0]['DatabaseName'].".dbo.ParkingTrans where AccNo='".$accno."') a WHERE row between '".$from."' and '".$to."'";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    foreach ($exec as $exe){
                        $amount = 0;
                        $type = '';
                        if($exe['Debet'] > 0){
                            $amount = $exe['Debet'];
                            $type = 'DB';
                        }
                        else{
                          $amount = $exe['Kredit'];
                          $type = 'CR';
                        }

                        $data = array(
                            'TransNumber'=>$exe['TransactionNumber'],
                            'AccNo'=>$exe['AccNo'],
                            'Date'=>date('Y-m-d H:i:s', strtotime($exe['TimeStam'])),
                            'Note'=>$exe['Note'],
                            'Type'=>$type,
                            'Point'=>number_format($amount)
                        );

                        array_push($response, $data);
                    }
                }
                else{
                    $data = array(
                        'TransNumber'=>'',
                        'AccNo'=>'',
                        'Date'=>'Kosong',
                        'Note'=>'',
                        'Type'=>'',
                        'Point'=>''
                    );

                    array_push($response, $data);
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }

        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionGetEmoneyTransCountRow(){
        $response = array();

        $totalRow = 0;
        if(isset($_GET['KID']) and isset($_GET['AccNo'])){
            $kid = $_GET['KID'];
            $accno = $_GET['AccNo'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                $yv = "select count(*) as TotalRow from ".$zz[0]['DatabaseName'].".dbo.ParkingTrans where AccNo='".$accno."'";
                $zv =  Yii::app()->db->createCommand($yv)->queryAll();
                if($zv != null){
                    $totalRow = $zv[0]['TotalRow'];
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        $data = array(
            'TotalRow'=>$totalRow
        );

        array_push($response, $data);

        echo json_encode($response);
    }

    public function actionChangeStatus(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['AccNo']) and isset($_REQUEST['Status'])){
            $kid = $_REQUEST['KID'];
            $accno = $_REQUEST['AccNo'];
            $status = $_REQUEST['Status'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                $sql = "select * from  ".$zz[0]['DatabaseName'].".[dbo].[MemberCard] where CardNo ='".$accno."' and Status <> 0";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    $cardno = $exec[0]['CardNo'];

                    //change status
                    $message = '';
                    if($status == 1){
                        $message = 'Kartu berhasil di aktifkan';
                    }

                    if($status == 2){
                        $message = 'Kartu berhasil di blokir';
                    }

                    $arrStatus = array('1','2');
                    if(in_array($status, $arrStatus)){
                        $s = "update ".$zz[0]['DatabaseName'].".[dbo].[MemberCard] set Status = '$status' where CardNo = '".$accno."'";
                        $e =  Yii::app()->db->createCommand($s);
                        if($e->execute()){
                            $response['Status'] = true;
                            $response['Message'] = $message;
                        }
                        else{
                            $response['Message'] = 'Permintaan tidak dapat dilakukan';
                        }
                    }
                    else{
                        $response['Message'] = 'Permintaan tidak dapat dilakukan';
                    }
                }
                else{
                    $response['Message'] = 'Kartu Tidak Ditemukan';
                }
            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
            }

            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionDeductPoint(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'errorCode'=>'',
            'AccNo'=>'',
            'PaidDate'=>'',
            'Balance'=>'',
            'Point'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['AccNo']) and isset($_REQUEST['Point']) and isset($_REQUEST['UserID'])){
            $kid = $_REQUEST['KID'];
            $accno = $_REQUEST['AccNo'];
            $point = $_REQUEST['Point'];
            $userid = $_REQUEST['UserID'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek balance
                $sql = "select* from ".$zz[0]['DatabaseName'].".[dbo].[MemberCard] where CardNo = '".$accno."' and Status = 1";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    if($point <= $exec[0]['Point']){
                        //debet emoney
                        $s = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesCardDeduct] '".$accno."','".$point."','".$userid."'";
                        $e =  Yii::app()->db->createCommand($s);
                        if($e->execute()){
                            $response['Status'] = true;
                            $response['AccNo'] = $accno;
                            $response['PaidDate'] = date('Y-m-d H:i:s');
                            $response['Balance'] = number_format($exec[0]['Point'] - $point);
                            $response['Point'] = number_format($point);
                            $response['Message'] = 'Transaksi Berhasil Dilakukan';
                        }
                        else{
                            $response['Message'] = 'Transaksi Tidak Dapat Dilakukan';
                        }
                    }
                    else{
                        $response['Message'] = 'Saldo Tidak Cukup';
                    }
                }
                else{
                    $response['Message'] = 'Kartu Tidak Ditemukan';
                }
            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
            }

            if($id != ''){
                $msg = json_encode($response);

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionEmoneyBalance(){
        $response = array(
            'Status'=>false,
            'StatusCode'=>false,
            'Message'=>'',
            'CardID'=>'',
            'Point'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['CardID'])){
            $kid = $_REQUEST['KID'];
            $card_id = $_REQUEST['CardID'];

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek balance
                $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where Barcode = '".$card_id."' and Status = 1";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    $cardno = $exec[0]['CardNo'];

                    $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                    $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                    if($exec2 != null){
                        $response['Status'] = true;
                        $response['StatusCode'] = '000';
                        $response['CardID'] = $cardno;
                        $response['Point'] = number_format($exec2[0]['Point']);
                        $response['Message'] = 'Kartu Ditemukan';
                    }
                    else{
                      $response['Message'] = 'Saldo kartu Tidak Ditemukan';
                    }
                }
                else{
                    $response['Message'] = 'Kartu Tidak Ditemukan';
                }
            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
            }

        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionCekBayar(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'TicketNumber'=>'',
            'DateIn'=>'',
            'DateOut'=>'',
            'Point'=>'',
            'Duration'=>'',
            'VehicleType'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['TicketNumber'])){
            $kid = $_REQUEST['KID'];
            $ticket = $_REQUEST['TicketNumber'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $card = array('1000100010001001','1000100010001002','1000100010001003','1000100010001004','1000100010001006');
            if(in_array($ticket, $card)){
                  $response['Status'] = true;
                  $response['Point'] = rand(10,20);
                  $response['DateIn'] = date('Y-m-d H:i:s', strtotime('-3 hour'));
                  $response['DateOut'] = date('Y-m-d H:i:s');
                  $response['Duration'] = 3;
                  $response['VehicleType'] = 'Motor';
            }
            else{
                $response['Message'] = 'Ticket Number Not Found';
            }

            $response['TicketNumber'] = $ticket;

            if($id != ''){
                $msg = json_encode($response);

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }
}

?>
