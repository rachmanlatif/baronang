<?php

class MobileController extends Controller {

    public function actionIndex(){
        $this->render('index');
    }

    public function actionConfirmationPassword($id, $n, $v){
        $a = "select* from PaymentGateway.dbo.UserPaymentGateway where Gac = '".$id."' and Status = 1";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $sql = "update PaymentGateway.dbo.UserPaymentGateway set Pass = '".$n."' where Gac = '".$id."'";
            $exec = Yii::app()->db->createCommand($sql);
            if($exec->execute()){
              echo '<h3>Berhasil melakukan ubah password</h3>';
            }
            else{
              echo '<h3>Gagal melakukan ubah password</h3>';
            }
        }
        else{
            echo '<h3>Anda belum melakukan konfirmasi</h3>';
        }
    }

    public function actionConfirmationPin($id, $n, $v){
        $a = "select* from PaymentGateway.dbo.UserPaymentGateway where Gac = '".$id."' and Status = 1";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $sql = "update PaymentGateway.dbo.UserPaymentGateway set Pin = '".$n."' where Gac = '".$id."'";
            $exec = Yii::app()->db->createCommand($sql);
            if($exec->execute()){
              echo '<h3>Berhasil melakukan ubah pin</h3>';
            }
            else{
              echo '<h3>Gagal melakukan ubah pin</h3>';
            }
        }
        else{
            echo '<h3>Anda belum melakukan konfirmasi</h3>';
        }
    }

    public function actionGetBalance(){
        $response = array();

        if(isset($_GET['kid']) and isset($_GET['acc'])){
            $kid = $_GET['kid'];
            $acc = $_GET['acc'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){

                $yy = "exec dbo.ListAccMemberBelanja '".$kid."','".$acc."'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $data = array(
                            'balance'=>number_format($z['Balance']),
                        );

                        array_push($response, $data);
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function actionGetBalancePoint(){
        $response = array();

        if(isset($_GET['kid']) and isset($_GET['edc'])){
            $kid = $_GET['kid'];
            $edc = $_GET['edc'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){

                $yy = "select top 1 * from Gateway.dbo.EDCList where KID = '".$kid."' and SerialNumber = '".$edc."'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $data = array(
                            'balance'=>number_format($z['Balance']),
                        );

                        array_push($response, $data);
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function actionGetPoint(){
        $response = array();

        if(isset($_GET['kid'])){
            $kid = $_GET['kid'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){

                $yy = "select* from ".$zzz[0]['DatabaseName'].".[dbo].[LocationParkingProductTopUp] where Status = 1";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $data = array(
                            'productid'=>$z['ProductID'],
                            'nama'=>$z['Nama'],
                            'deskripsi'=>$z['Deskripsi'],
                            'harga'=>number_format($z['Harga']),
                            'poin'=>$z['JumlahPoinAdd'],
                            'bonus'=>$z['JumlahPoinBonus'],
                        );

                        array_push($response, $data);
                    }
                }
            }
        }

        echo json_encode($response);
    }

    public function actionGetHistoryPoint(){
        $response = array();

        if(isset($_GET['kid']) and isset($_GET['sn'])){
            $kid = $_GET['kid'];
            $sn = $_GET['sn'];

            $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
            if($zzz != null){
                $tanggal = date('Y-m-d');
                if(isset($_GET['tanggal']) and $_GET['tanggal'] != ''){
                    $tanggal = $_GET['tanggal'];
                }

                //conversi
                $conv = 1;
                $y = "select top 1 * from ".$zzz[0]['DatabaseName'].".[dbo].[ParkingSetting]";
                $z =  Yii::app()->db->createCommand($y)->queryAll();
                if($z != null){
                    $conv = round($z[0]['ValueConversion']);
                }

                $yy = "select* from ".$zzz[0]['DatabaseName'].".[dbo].[ParkingTrans] where convert(date, TimeStam) = '".$tanggal."' and UserID='".$sn."' and Kredit > 0 order by TimeStam desc";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $harga = $z['Kredit']*$conv;

                        $data = array(
                            'cardno'=>$z['AccNo'],
                            'tanggal'=>date('Y-m-d H:i:s', strtotime($z['TimeStam'])),
                            'harga'=>number_format($harga),
                        );

                        array_push($response, $data);
                    }
                }
                else{
                  $data = array(
                      'cardno'=>'Kosong',
                      'tanggal'=>'',
                      'harga'=>'',
                  );

                  array_push($response, $data);
                }
            }
        }

        echo json_encode($response);
    }

    public function actionCheckPIN(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
        );

        if(isset($_POST['uid']) and isset($_POST['pin'])){

            if(isset($_POST['kid'])){
                $pin = md5($_POST['pin']);
                $uid = $_POST['uid'];

                $s = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '".$uid."' and Pin = '".$pin."'";
                $e =  Yii::app()->db->createCommand($s)->queryAll();
                if($e != null){
                    $response['status'] = true;
                    $response['message'] = 'PIN benar';
                }
                else{
                    $response['message'] = 'PIN salah';
                }
            }
            else{
                $response['message'] = 'Anda harus melakukan update aplikasi di Play Store';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionSaveRegister(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['tipe']) and isset($_REQUEST['ktp']) and isset($_REQUEST['nama']) and isset($_REQUEST['telepon']) and isset($_REQUEST['email']) and isset($_REQUEST['alamat']) and isset($_REQUEST['password']) and isset($_REQUEST['repassword']) and isset($_REQUEST['pin']) and isset($_REQUEST['repin'])){

            $tipe = $_REQUEST['tipe'];
            $ktp = $_REQUEST['ktp'];
            $nama = $_REQUEST['nama'];
            $email = $_REQUEST['email'];
            $telp = $_REQUEST['telepon'];
            $alamat = $_REQUEST['alamat'];
            $password = md5($_REQUEST['password']);
            $pin = md5($_REQUEST['pin']);

            $a = "select* from PaymentGateway.dbo.UserPaymentGateway where KTP = '".$ktp."' or Telp = '".$telp."' or email = '".$email."'";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b == null){

                if ($tipe == 0) {
                  $aa = "select PaymentGateway.dbo.getUserID()";
                  $bb = Yii::app()->db->createCommand($aa)->queryAll();
              	}
              	else {
                  $aa = "select PaymentGateway.dbo.getUserIDCorp()";
                  $bb = Yii::app()->db->createCommand($aa)->queryAll();
              	}

                if($bb != null){
                    $codes = $bb[0][''];

                    $secretCode = '';
                    $ga = new GoogleAuthenticator();
                    $secret = $ga->createSecret();
                    if($secret != null){
                        $secretCode = $secret;
                    }
                    else{
                        $secretCode = md5('Y/m/d H:i:s');
                    }

                    $link = 'https://baronang.com/app/confregister.php?c='.$codes;

                    $subject = 'Verifikasi Baronang App';
                    $message = "<p>
                      Hi! ".$nama.",<br><br>
                      Terima kasih telah bergabung dengan Baronang.<br>
                      Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
                      <a href=".$link.">Konfirmasi</a>
                      <br>
                      <br>
                      <br>
                      <br>
                      Terima kasih.<br>
                      Salam,<br>
                      <br>
                      <br>
                      Baronang
                    </p>";

                    $sql = "exec PaymentGateway.dbo.ProsesUserPaymentGateway '$codes', '$nama','$email','$telp','$alamat','$password','0','0','0','$ktp','$secretCode','$pin',$tipe";
                    $exec = Yii::app()->db->createCommand($sql);
                    if($exec->execute()){
                        $curl = curl_init();
                        $post = "toaddr=$email&subject=$subject&message=$message";
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://interzircon.com/phpmail.php",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => $post,
                        ));
                        $output = curl_exec($curl);
                        $err = curl_error($curl);
                        curl_close($curl);
                        $result = json_decode($output, true);
                        if($err) {
                            $response['status'] = true;
                            $response['message'] = 'Registrasi berhasil dilakukan. Email gagal dikirim ke email '.$email.' anda';
                        }
                        else {
                            $response['status'] = true;
                            $response['message'] = 'Registrasi berhasil dilakukan. Harap segera konfirmasi email '.$email.' anda';
                        }
                    }
                    else{
                        $response['message'] = 'Registrasi gagal dilakukan';
                        $response['errorCode'] = '(eX0001)';
                    }
                }
                else{
                    $response['message'] = 'Registrasi gagal dilakukan';
                    $response['errorCode'] = '(eX0002)';
                }
            }

            if($b != null){
                $gac = $b[0]['Gac'];
                $email = $b[0]['email'];
                $nama = $b[0]['NamaUser'];

                $link = 'https://baronang.com/app/confregister.php?c='.$gac;

                $subject = 'Verifikasi Baronang App';
                $message = "<p>
                  Hi! ".$nama.",<br><br>
                  Terima kasih telah bergabung dengan Baronang.<br>
                  Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
                  <a href=".$link.">Konfirmasi</a>
                  <br>
                  <br>
                  <br>
                  <br>
                  Terima kasih.<br>
                  Salam,<br>
                  <br>
                  <br>
                  Baronang
                </p>";

                $curl = curl_init();
                $post = "toaddr=$email&subject=$subject&message=$message";
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://interzircon.com/phpmail.php",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $post,
                ));
                $output = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                $result = json_decode($output, true);
                if($err) {
                    $response['message'] = 'Email gagal dikirim ke email '.$email.' anda';
                }
                else {
                    $response['status'] = true;
                    $response['message'] = 'Email konfirmasi telah dikirim. Harap segera konfirmasi email '.$email.' anda';
                }
            }

        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX1111332)';
        }

        echo json_encode($response);
    }

    public function actionCekForgotPassword(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['email'])){
            $a = "select* from PaymentGateway.dbo.UserPaymentGateway where email = '".$_REQUEST['email']."' and Status = 1";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b != null){
                $nama = $b[0]['NamaUser'];
                $email = $b[0]['email'];
                $gac = $b[0]['Gac'];

                //password baru
                $rand = rand(100000,999999);
                $newpass = md5($rand);
                $options = [
                    'cost' => 11,
                    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
                ];
                $hash = password_hash($newpass, PASSWORD_BCRYPT, $options);

                $link = 'https://baronang.com/api/mobile/confirmationPassword/id/'.$gac.'/n/'.$newpass.'/v/'.$hash;

                $subject = 'Konfirmasi Lupa Password';
                $message = "<p>
                    Hi! ".$nama.",<br><br>
                    Anda telah melakukan permintaan lupa password.<br>
                    Kami perlu memastikan bahwa anda yang melakukan permintaan. Harap mengkonfirmasi dengan mengklik link dibawah ini.<br><br>
                    <a href='".$link."'><b>Konfirmasi</b></a><br><br>
                    Pastikan anda mengklik konfirmasi diatas jika ingin melakukan perubahan password<br><br>
                    Jika anda mengkonfirmasi, password baru anda adalah : ".$rand."<br><br>
                    Harap segera mengganti password anda.<br>
                    Abaikan pesan ini jika anda tidak melakukan permintaan lupa password.
                    <br>
                    <br>
                    <br>
                    <br>
                    Terima kasih.<br>
                    Salam,<br>
                    <br>
                    <br>
                    Baronang
                </p>";

                $curl = curl_init();
                $post = "toaddr=$email&subject=$subject&message=$message";
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://interzircon.com/phpmail.php",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $post,
                ));
                $output = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                $result = json_decode($output, true);
                if($err) {
                    $response['message'] = 'Email lupa password gagal dikirim ke email '.$email.' anda';
                }
                else {
                    $response['status'] = true;
                    $response['message'] = 'Email konfirmasi lupa password telah dikirim ke email '.$email.' anda';
                }
            }
            else{
                $response['message'] = 'Email tidak ditemukan';
                $response['errorCode'] = '(eX0004)';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX2222)';
        }

        echo json_encode($response);
    }

    public function actionCekForgotPIN(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['email']) and isset($_REQUEST['password'])){
            $password = md5($_REQUEST['password']);
            $a = "select* from PaymentGateway.dbo.UserPaymentGateway where email = '".$_REQUEST['email']."' and Pass = '".$password."' and Status = 1";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b != null){
                $nama = $b[0]['NamaUser'];
                $email = $b[0]['email'];
                $gac = $b[0]['Gac'];

                //password baru
                $rand = rand(100000,999999);
                $newpass = md5($rand);
                $options = [
                    'cost' => 11,
                    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
                ];
                $hash = password_hash($newpass, PASSWORD_BCRYPT, $options);

                $link = 'https://baronang.com/api/mobile/confirmationPin/id/'.$gac.'/n/'.$newpass.'/v/'.$hash;

                $subject = 'Konfirmasi Lupa PIN';
                $message = "<p>
                    Hi! ".$nama.",<br><br>
                    Anda telah melakukan permintaan lupa PIN.<br>
                    Kami perlu memastikan bahwa anda yang melakukan permintaan. Harap mengkonfirmasi dengan mengklik link dibawah ini.<br><br>
                    <a href='".$link."'><b>Konfirmasi</b></a><br><br>
                    Pastikan anda mengklik konfirmasi diatas jika ingin melakukan perubahan PIN<br><br>
                    Jika anda mengkonfirmasi, PIN baru anda adalah : ".$rand."<br><br>
                    Harap segera mengganti PIN anda.<br>
                    Abaikan pesan ini jika anda tidak melakukan permintaan lupa PIN.
                    <br>
                    <br>
                    <br>
                    <br>
                    Terima kasih.<br>
                    Salam,<br>
                    <br>
                    <br>
                    Baronang
                </p>";

                $curl = curl_init();
                $post = "toaddr=$email&subject=$subject&message=$message";
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://interzircon.com/phpmail.php",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $post,
                ));
                $output = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                $result = json_decode($output, true);
                if($err) {
                    $response['message'] = 'Email lupa PIN gagal dikirim ke email '.$email.' anda';
                }
                else {
                    $response['status'] = true;
                    $response['message'] = 'Email konfirmasi lupa PIN telah dikirim ke email '.$email.' anda';
                }
            }
            else{
                $response['message'] = 'Pengguna tidak ditemukan';
                $response['errorCode'] = '(eX0005)';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX3333)';
        }

        echo json_encode($response);
    }

    public function actionCekLogin(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'kid'=>'',
            'uid'=>'',
            'email'=>'',
            'password'=>'',
            'nama'=>'',
            'telp'=>'',
            'alamat'=>'',
            'ktp'=>'',
            'balance'=>'',
            'tipe'=>'',
        );

        if(isset($_POST['email']) and isset($_POST['password'])){
          $email = $_POST['email'];
          $password = $_POST['password'];
          $s = "select* from PaymentGateway.dbo.UserPaymentGateway where email = '".$email."' and Pass = '".md5($password)."'";
          $e =  Yii::app()->db->createCommand($s)->queryAll();
          if($e != null){
              $status = $e[0]['Status'];
              $em = $e[0]['email'];
              $pass = $e[0]['Pass'];
              $uid = $e[0]['KodeUser'];
              $name = $e[0]['NamaUser'];
              $telp = $e[0]['Telp'];
              $address = $e[0]['Alamat'];
              $balance = number_format($e[0]['Balance']);
              $ktp = $e[0]['KTP'];
              $type = $e[0]['TipeAcc'];
              if($status == 1){
                  $response['status'] = true;
                  $response['uid'] = $uid;
                  $response['email'] = $em;
                  $response['password'] = $password;
                  $response['nama'] = $name;
                  $response['telp'] = $telp;
                  $response['alamat'] = $address;
                  $response['ktp'] = $ktp;
                  $response['balance'] = $balance;
                  $response['tipe'] = $type;
                  $response['message'] = 'Berhasil masuk';
              }
              else{
                  $response['message'] = 'Pengguna belum melakukan konfirmasi pendaftaran';
              }
          }
          else{
              $response['message'] = 'Pengguna tidak ditemukan';
          }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionCekEdc(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
        );

        if(isset($_POST['sn'])){
            $sql = "select* from Gateway.dbo.EDCList where SerialNumber = '".$_POST['sn']."'";
            // execute the sql command
            $exec =  Yii::app()->db->createCommand($sql)->queryAll();
            if($exec != null){
                $response['status'] = true;
                $response['message'] = 'Data found';
                $response['kid'] = $exec[0]['KID'];
                $response['sn'] = $exec[0]['SerialNumber'];
                $response['uid'] = $exec[0]['UserIDBaronang'];
                $response['mid'] = $exec[0]['MemberID'];
                $response['acc'] = $exec[0]['RegSavAcc'];

                $s = "select top 1 * from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '".$exec[0]['UserIDBaronang']."'";
                // execute the sql command
                $e =  Yii::app()->db->createCommand($s)->queryAll();
                if($e != null){
                    $response['name'] = $e[0]['NamaUser'];
                    $response['type'] = $e[0]['TipeAcc'];
                }
                else{
                    $response['name'] = '';
                    $response['type'] = '';
                }

                $sa = "select top 1 * from PaymentGateway.dbo.MerchantParking where UserIDBaronang = '".$exec[0]['UserIDBaronang']."'";
                // execute the sql command
                $ea =  Yii::app()->db->createCommand($sa)->queryAll();
                if($ea != null){
                    $response['merchant'] = 1;
                }
                else{
                    $response['merchant'] = 0;
                }
            }
            else{
                $response['message'] = 'Serial Number tidak ditemukan';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
        }

        echo json_encode($response);
    }

    public function actionCekAcc(){
        $response = array(
            'status'=>FALSE,
            'tipe'=>FALSE, //selain emoney FALSE
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_POST['kid']) and isset($_POST['acc'])){
            //cek balance
            $sql = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['acc']."'";
            // execute the sql command
            $exec =  Yii::app()->db->createCommand($sql)->queryAll();
            if($exec != null){
                $s = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '".$exec[0]['UserIDBaronang']."'";
                $e =  Yii::app()->db->createCommand($s)->queryAll();
                if($e != null){
                    $response['status'] = true;
                    $response['acc'] = $exec[0]['KodeAcc'];
                    $response['message'] = 'Akun ditemukan';
                    if($exec[0]['Keterangan'] == 'e-money'){
                        $response['tipe'] = true;
                    }
                    else{
                        $response['tipe'] = false;
                    }
                }
                else{
                    $response['message'] = 'Akun tidak ditemukan';
                    $response['errorCode'] = '(eX0006)';
                }
            }
            else{
                $response['message'] = 'Akun tidak ditemukan';
                $response['errorCode'] = '(eX0007)';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX0000)';
        }

        echo json_encode($response);
    }

    public function actionCekPINEDC(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
        );

        if(isset($_POST['uid']) and isset($_POST['pin'])){
            $uid = $_POST['uid'];
            $pin = md5($_POST['pin']);

            $s = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '".$uid."' and Pin = '".$pin."'";
            $e =  Yii::app()->db->createCommand($s)->queryAll();
            if($e != null){
                $response['status'] = true;
                $response['message'] = 'PIN benar';
            }
            else{
                $response['message'] = 'PIN salah';
            }
        }
        else{
            $response['message'] = 'Invalid require datas';
        }

        echo json_encode($response);
    }

    public function actionCekPIN(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
        );

        if(isset($_POST['kid']) and isset($_POST['acc']) and isset($_POST['pin'])){
            //cek balance
            $sql = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['acc']."'";
            // execute the sql command
            $exec =  Yii::app()->db->createCommand($sql)->queryAll();
            if($exec != null){
                $pin = md5($_POST['pin']);

                $s = "select* from PaymentGateway.dbo.UserPaymentGateway where KodeUser = '".$exec[0]['UserIDBaronang']."' and Pin = '".$pin."'";
                $e =  Yii::app()->db->createCommand($s)->queryAll();
                if($e != null){
                    $response['status'] = true;
                    $response['message'] = 'PIN benar';
                }
                else{
                    $response['message'] = 'PIN salah';
                }
            }
            else{
                $response['message'] = 'Akun tidak ditemukan';
            }
        }
        else{
            $response['message'] = 'Invalid require datas';
        }

        echo json_encode($response);
    }

    public function actionCekCard(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['kid']) and isset($_REQUEST['sn']) and isset($_REQUEST['barcode'])){
          $kid = $_REQUEST['kid'];
          $sn = $_REQUEST['sn'];
          $barcode = $_REQUEST['barcode'];

          $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
          $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
          if($zzz != null){
              $yy = "select* from Gateway.dbo.EDCList where SerialNumber = '$sn' and Status = 1";
              $zz =  Yii::app()->db->createCommand($yy)->queryAll();
              if($zz != null){
                  $kid2 = $zz[0]['KID'];

                  $yq = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid2' and Status = 1";
                  $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                  if($zq != null){
                      $yw = "select* from ".$zzz[0]['DatabaseName'].".dbo.MasterCard where Barcode = '$barcode' and Status = 1";
                      $zw =  Yii::app()->db->createCommand($yw)->queryAll();
                      if($zw != null){
                          $cardno = $zw[0]['CardNo'];

                          $yb = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardno' and Status = 1";
                          $zb =  Yii::app()->db->createCommand($yb)->queryAll();
                          if($zb != null){
                              $response['status'] = true;
                              $response['cardNo'] = $cardno;
                              $response['message'] = 'Kartu ditemukan';
                          }
                          else{
                              $response['message'] = 'Card not active';
                              $response['errorCode'] = '(eX52367)';
                          }
                      }
                      else{
                          $response['message'] = 'Card not found';
                          $response['errorCode'] = '(eX52462)';
                      }
                  }
                  else{
                    $response['message'] = 'KID not found';
                    $response['errorCode'] = '(eX1137)';
                  }
              }
              else{
                  $response['message'] = 'EDC not found';
                  $response['errorCode'] = '(eX5535)';
              }
          }
          else{
              $response['message'] = 'KID not found';
              $response['errorCode'] = '(eX5555)';
          }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX5555)';
        }

        echo json_encode($response);
    }

    public function actionSettlePoint(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_REQUEST['kid']) and isset($_REQUEST['sn']) and isset($_REQUEST['barcode']) and isset($_REQUEST['productid'])){
          $kid = $_REQUEST['kid'];
          $sn = $_REQUEST['sn'];
          $barcode = $_REQUEST['barcode'];
          $productid = $_REQUEST['productid'];

          $yyy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
          $zzz =  Yii::app()->db->createCommand($yyy)->queryAll();
          if($zzz != null){
              $yy = "select* from Gateway.dbo.EDCList where SerialNumber = '$sn' and Status = 1";
              $zz =  Yii::app()->db->createCommand($yy)->queryAll();
              if($zz != null){
                  $uid = $zz[0]['UserIDBaronang'];
                  $balance = $zz[0]['Balance'];
                  $acc = $zz[0]['RegSavAcc'];
                  $kid2 = $zz[0]['KID'];

                  $yq = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid2' and Status = 1";
                  $zq =  Yii::app()->db->createCommand($yq)->queryAll();
                  if($zq != null){
                      $yw = "select* from ".$zzz[0]['DatabaseName'].".dbo.MasterCard where Barcode = '$barcode' and Status = 1";
                      $zw =  Yii::app()->db->createCommand($yw)->queryAll();
                      if($zw != null){
                          $cardno = $zw[0]['CardNo'];

                          $yb = "select* from ".$zzz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '$cardno' and Status = 1";
                          $zb =  Yii::app()->db->createCommand($yb)->queryAll();
                          if($zb != null){

                              $y = "select* from ".$zzz[0]['DatabaseName'].".dbo.LocationParkingProductTopUp where ProductID = '$productid' and Status = 1";
                              $z =  Yii::app()->db->createCommand($y)->queryAll();
                              if($z != null){
                                  $harga = $z[0]['Harga'];
                                  $poin = $z[0]['JumlahPoinAdd']+$z[0]['JumlahPoinBonus'];

                                  if($balance >= $harga){

                                      $ss = "exec ".$zzz[0]['DatabaseName'].".[dbo].[ProsesCardTopUp] '".$cardno."','".$poin."','".$sn."'";
                                      $ee =  Yii::app()->db->createCommand($ss);
                                      if($ee->execute()){
                                          //kurangin saldo edc
                                          $ket = 'Pengeluaran top up Sky Point';
                                          $ss2 = "exec ".$zq[0]['DatabaseName'].".[dbo].[ProsesRegularSavingTransFullKeterangan] '".$acc."',1,'".$harga."','".$uid."','".$ket."','PARK'";
                                          $ee2 =  Yii::app()->db->createCommand($ss2);
                                          $ee2->execute();

                                          $response['status'] = true;
                                          $response['message'] = 'Berhasil melakukan pembelian';
                                      }
                                      else{
                                          $response['message'] = 'Transaksi gagal dilakukan';
                                          $response['errorCode'] = '(eX2234)';
                                      }
                                  }
                                  else{
                                      $response['message'] = 'Balance EDC not enough';
                                      $response['errorCode'] = '(eX2237)';
                                  }

                              }
                              else{
                                  $response['message'] = 'Product not found';
                                  $response['errorCode'] = '(eX5235)';
                              }
                          }
                          else{
                            $response['message'] = 'Card not active';
                            $response['errorCode'] = '(eX52347)';
                          }
                      }
                      else{
                          $response['message'] = 'Card not found';
                          $response['errorCode'] = '(eX52347)';
                      }
                  }
                  else{
                    $response['message'] = 'KID not found';
                    $response['errorCode'] = '(eX1137)';
                  }
              }
              else{
                  $response['message'] = 'EDC not found';
                  $response['errorCode'] = '(eX5535)';
              }
          }
          else{
              $response['message'] = 'KID not found';
              $response['errorCode'] = '(eX5555)';
          }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX5555)';
        }

        echo json_encode($response);
    }

    public function actionSettleEMoney(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_POST['kid']) and isset($_POST['barcode']) and isset($_POST['amount']) and isset($_POST['acc']) and isset($_POST['account'])){
            /*
            barcode = akun e-money
            acc = akun tabungan
            account = akun edc
            */
            if($_POST['amount'] > 0){
                //cek balance edc
                $sql = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['account']."'";
                // execute the sql command
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    $uid = $exec[0]['UserIDBaronang'];
                    $mid = $exec[0]['MemberID'];
                    if($_POST['amount'] <= $exec[0]['Balance']){
                        //cek e-Money
                        $sql2 = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['barcode']."'";
                        // execute the sql command
                        $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                        if($exec2 != null){
                            //cek akun tabungan debet
                            $sql3 = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['acc']."'";
                            // execute the sql command
                            $exec3 =  Yii::app()->db->createCommand($sql3)->queryAll();
                            if($exec3 != null){
                                if($_POST['amount'] <= $exec3[0]['Balance']){
                                    //exec
                                    $sss = "exec [PaymentGateway].[dbo].[Prosesemoneytopup] '".$_POST['kid']."', '".$_POST['barcode']."',".$_POST['amount'].",'".$uid."','".$_POST['acc']."'";
                                    $eee =  Yii::app()->db->createCommand($sss);
                                    if($eee->execute()){
                                        $response['status'] = true;
                                        $response['message'] = 'Transaksi berhasil dilakukan';
                                    }
                                    else{
                                        $response['message'] = 'Transaksi gagal dilakukan';
                                        $response['errorCode'] = '(eX0008)';
                                    }
                                }
                                else{
                                    $response['message'] = 'Saldo tabungan tidak mencukupi';
                                    $response['errorCode'] = '(eX0009)';
                                }
                            }
                            else{
                                $response['message'] = 'Transaksi tidak dapat dilakukan';
                                $response['errorCode'] = '(eX0010)';
                            }
                        }
                        else{
                            $response['message'] = 'Transaksi tidak dapat dilakukan';
                            $response['errorCode'] = '(eX0011)';
                        }
                    }
                    else{
                        $response['message'] = 'Saldo EDC tidak mencukupi';
                        $response['errorCode'] = '(eX0012)';
                    }
                }
                else{
                    $response['message'] = 'Transaksi tidak dapat dilakukan';
                    $response['errorCode'] = '(eX0013)';
                }
            }
            else{
              $response['message'] = 'Jumlah debet harus lebih dari 0';
              $response['errorCode'] = '(eX0014)';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX5555)';
        }

        echo json_encode($response);
    }

    public function actionSettleBalance(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        if(isset($_POST['kid']) and isset($_POST['barcode']) and isset($_POST['amount']) and isset($_POST['acc']) and isset($_POST['method'])){
            /*
              barcode = akun yg di scan
              acc = akun edc nya
              method
              0 = pembayaran
              1 = top up
              2 = setoran
              3 = penarikan
            */
            //filter tidak boleh nol
            $acc = $_POST['barcode'];
            $amount = $_POST['amount'];
            $kid = $_POST['kid'];
            if($_POST['amount'] > 0){
                $method = $_POST['method'];
                //pembayaran
                if($method == 0){
                    //cek balance
                    $sql = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['barcode']."'";
                    // execute the sql command
                    $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                    if($exec != null){
                        if($_POST['amount'] <= $exec[0]['Balance']){
                            //dapetin sn
                            $sn = null;
                            $a = "select * from Gateway.dbo.EDCList where KID = '".$_POST['kid']."' and RegSavAcc = '".$_POST['acc']."'";
                            $b =  Yii::app()->db->createCommand($a)->queryAll();
                            if($b != null){
                                $sn = $b[0]['SerialNumber'];
                            }

                            //dapetin code
                            $code = null;
                            $aa = "select dbo.getKodeTrx()";
                            $bb =  Yii::app()->db->createCommand($aa)->queryAll();
                            if($bb != null){
                                $code = $bb[0][''];
                            }

                            $uid = $exec[0]['UserIDBaronang'];
                            $mid = $exec[0]['MemberID'];

                            if($sn != null and $code != null and $uid != null and $acc != null and $amount >= 0){
                                $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
                                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                                if($zz != null){
                                    $ss = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesTransaksiPos] '".$code."','".$mid."','".$sn."','".$amount."','".$uid."'";
                                    $ee =  Yii::app()->db->createCommand($ss);
                                    $ee->execute();

                                    //debet emoney
                                    $ssss = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesemoneyDebet] '".$kid."','".$uid."','".$acc."','".$amount."','".$sn."'";
                                    $eeee =  Yii::app()->db->createCommand($ssss);
                                    $eeee->execute();
                                }

                                // execute the sql command
                                $sss = "exec dbo.ProsesTransaksiGateway '".$code."','".$sn."','".$acc."','".$amount."','".$uid."'";
                                $eee =  Yii::app()->db->createCommand($sss);
                                if($eee->execute()){
                                    $response['status'] = true;
                                    $response['message'] = 'Transaksi berhasil dilakukan';
                                }
                                else{
                                    $response['message'] = 'Transaksi gagal dilakukan';
                                    $response['errorCode'] = '(eX0015)';
                                }
                            }
                            else{
                                $response['message'] = 'Transaksi tidak dapat dilakukan';
                                $response['errorCode'] = '(eX0016)';
                            }
                        }
                        else{
                            $response['message'] = 'Saldo tidak mencukupi';
                            $response['errorCode'] = '(eX0017)';
                        }
                    }
                    else{
                        $response['message'] = 'Akun tidak ditemukan';
                        $response['errorCode'] = '(eX0018)';
                    }
                }
                //top up e-money
                else if($method == 1){
                    $account = '';
                    //cek balance edc
                    $sql = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['acc']."'";
                    // execute the sql command
                    $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                    if($exec != null){
                        $uid = $exec[0]['UserIDBaronang'];
                        $mid = $exec[0]['MemberID'];
                        if($_POST['amount'] <= $exec[0]['Balance']){
                            //cek emoney
                            $sql2 = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['barcode']."'";
                            // execute the sql command
                            $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                            if($exec2 != null){
                                if($exec2[0]['Keterangan'] == 'e-money'){
                                    //cari regular saving acc
                                    $s = "exec dbo.ListAcceMoney '".$_POST['kid']."','".$_POST['barcode']."'";
                                    // execute the sql command
                                    $e =  Yii::app()->db->createCommand($s)->queryAll();
                                    if($e != null){
                                        $account = $e[0]['AccNo'];
                                    }
                                    else{
                                        $account = $_POST['barcode'];
                                    }

                                    //exec
                                    $sss = "exec [dbo].[ProseseMoneyTopUp] '".$_POST['kid']."', '".$account."',".$_POST['amount'].",'".$uid."','".$_POST['acc']."'";
                                    $eee =  Yii::app()->db->createCommand($sss);
                                    if($eee->execute()){
                                        $response['status'] = true;
                                        $response['message'] = 'Transaksi berhasil dilakukan';
                                    }
                                    else{
                                        $response['message'] = 'Transaksi gagal dilakukan';
                                        $response['errorCode'] = '(eX0008)';
                                    }
                                }
                                else{
                                    $response['message'] = 'Invalid card';
                                    $response['errorCode'] = '(eX0033)';
                                }
                            }
                            else{
                                $response['message'] = 'Transaksi tidak dapat dilakukan';
                                $response['errorCode'] = '(eX0032)';
                            }
                        }
                        else{
                            $response['message'] = 'Saldo EDC tidak mencukupi';
                            $response['errorCode'] = '(eX0032)';
                        }
                    }
                    else{
                        $response['message'] = 'Transaksi tidak dapat dilakukan';
                        $response['errorCode'] = '(eX0031)';
                    }
                }
                //setoran
                else if($method == 2){
                    //cek balance edc
                    $sql = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['acc']."'";
                    // execute the sql command
                    $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                    if($exec != null){
                        $uid = $exec[0]['UserIDBaronang'];
                        $mid = $exec[0]['MemberID'];
                        if($_POST['amount'] <= $exec[0]['Balance']){
                            //cek akun user
                            $sql2 = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['barcode']."'";
                            // execute the sql command
                            $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                            if($exec2 != null){
                                $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
                                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                                if($zz != null){
                                    //kurangin saldo edc
                                    $ss2 = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesRegularSavingTransFull] '".$_POST['acc']."',1,'".$amount."','".$uid."'";
                                    $ee2 =  Yii::app()->db->createCommand($ss2);
                                    $ee2->execute();

                                    //tambahin saldo user
                                    $ss = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesRegularSavingTransFull] '".$_POST['barcode']."',0,'".$amount."','".$uid."'";
                                    $ee =  Yii::app()->db->createCommand($ss);
                                    if($ee->execute()){
                                        $response['status'] = true;
                                        $response['message'] = 'Transaksi berhasil dilakukan';
                                    }
                                    else{
                                        $response['message'] = 'Transaksi gagal dilakukan';
                                        $response['errorCode'] = '(eX0019)';
                                    }
                                }
                                else{
                                      $response['message'] = 'Transaksi tidak dapat dilakukan';
                                      $response['errorCode'] = '(eX0020)';
                                }
                            }
                            else{
                                $response['message'] = 'Transaksi tidak dapat dilakukan';
                                $response['errorCode'] = '(eX0021)';
                            }
                        }
                        else{
                            $response['message'] = 'Saldo EDC tidak mencukupi';
                            $response['errorCode'] = '(eX0022)';
                        }
                    }
                    else{
                        $response['message'] = 'Transaksi tidak dapat dilakukan';
                        $response['errorCode'] = '(eX0029)';
                    }
                }
                //penarikan
                else if($method == 3){
                    //cek balance edc
                    $sql = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['acc']."'";
                    // execute the sql command
                    $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                    if($exec != null){
                        $uid = $exec[0]['UserIDBaronang'];
                        $mid = $exec[0]['MemberID'];
                        if($_POST['amount'] <= $exec[0]['Balance']){
                            //cek akun user
                            $sql2 = "exec dbo.ListAccMemberBelanja '".$_POST['kid']."','".$_POST['barcode']."'";
                            // execute the sql command
                            $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                            if($exec2 != null){
                                $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
                                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                                if($zz != null){
                                    //kurangin saldo edc
                                    $ss2 = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesRegularSavingTransFull] '".$_POST['acc']."',0,'".$amount."','".$uid."'";
                                    $ee2 =  Yii::app()->db->createCommand($ss2);
                                    $ee2->execute();

                                    //tambahin saldo user
                                    $ss = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesRegularSavingTransFull] '".$_POST['barcode']."',1,'".$amount."','".$uid."'";
                                    $ee =  Yii::app()->db->createCommand($ss);
                                    if($ee->execute()){
                                        $response['status'] = true;
                                        $response['message'] = 'Transaksi berhasil dilakukan';
                                    }
                                    else{
                                        $response['message'] = 'Transaksi gagal dilakukan';
                                        $response['errorCode'] = '(eX0023)';
                                    }
                                }
                                else{
                                      $response['message'] = 'Transaksi tidak dapat dilakukan';
                                      $response['errorCode'] = '(eX0024)';
                                }
                            }
                            else{
                                $response['message'] = 'Transaksi tidak dapat dilakukan';
                                $response['errorCode'] = '(eX0025)';
                            }
                        }
                        else{
                            $response['message'] = 'Saldo EDC tidak mencukupi';
                            $response['errorCode'] = '(eX0026)';
                        }
                    }
                    else{
                        $response['message'] = 'Transaksi tidak dapat dilakukan';
                        $response['errorCode'] = '(eX0030)';
                    }
                }
                else{
                    $response['message'] = 'Transaksi tidak dapat dilakukan';
                    $response['errorCode'] = '(eX0027)';
                }
            }
            else{
              $response['message'] = 'Jumlah debet harus lebih dari 0';
              $response['errorCode'] = '(eX0028)';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['errorCode'] = '(eX7777)';
        }

        echo json_encode($response);
    }
}

?>
