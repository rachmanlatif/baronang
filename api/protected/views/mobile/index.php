<div class="error-page">
    <h1 class="headline text-yellow">404</h1>

    <div class="error-content">
        <h2><i class="fa fa-warning text-yellow"></i> Oops! Halaman Tidak Ditemukan.</h2>

        <p>
            Tidak bisa mengurai request "mobile"
            <br>
            <br>
            <button onclick="window.history.back()" class="btn btn-success btn-sm">Kembali</button>
        </p>
    </div>
    <!-- /.error-content -->
</div>
