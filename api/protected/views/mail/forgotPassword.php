<p>
Hi! <?php echo $nama; ?>,<br><br>
    Anda telah melakukan permintaan lupa password.<br>
    Kami perlu memastikan bahwa anda yang melakukan permintaan. Harap mengkonfirmasi dengan mengklik link dibawah ini.<br>
    <a href="<?php echo $link; ?>">Konfirmasi</a><br><br>
    Jika anda mengkonfirmasi, password baru anda adalah : <?php echo $password; ?><br><br>
    Harap segera mengganti password anda.<br>
    Abaikan pesan ini jika anda tidak melakukan permintaan lupa password.
    <br>
    <br>
    <br>
    <br>
    Terima kasih.<br>
    Salam,<br>
    <br>
    <br>
    Baronang
</p>
