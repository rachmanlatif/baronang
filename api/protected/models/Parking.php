<?php

class Parking extends CFormModel
{
    public function settleProductMonthly($kid, $productid, $locationid, $cardno, $month){

        $response = array(
            'Status'=>false,
            'StatusCode'=>'200',
            'Message'=>''
        );

        $a = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $c = "select* from ".$b[0]['DatabaseName'].".dbo.MerchantHit where ActionHit = 'settleProduct' and LocationID = '$locationid' and Status = 1";
            $d =  Yii::app()->db->createCommand($c)->queryAll();
            if($d != null){
                $url = $d[0]['URL'];
                $methodhit = $d[0]['HitMethod'];

                $id = $d[0]['ID'];
                $mid = $d[0]['MerchantID'];
                $isHeader = $d[0]['IsHeader'];

                $barcode = '';
                $e = "select* from ".$b[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$cardno."' and Status = 1";
                $f =  Yii::app()->db->createCommand($e)->queryAll();
                if($f != null){
                    $barcode = $f[0]['Barcode'];
                    $rfid = $f[0]['RFIDNo'];

                    $headers = array();
                    if($isHeader == 1){
                        $ee = "select* from ".$b[0]['DatabaseName'].".dbo.MerchantHeader where MerchantHitID = '".$id."'";
                        $ff =  Yii::app()->db->createCommand($ee)->queryAll();
                        foreach ($ff as $f) {
                            $hid = $f['MerchantCredentialID'];
                            $hname = $f['Name'];

                            $value = '';
                            $g2 = "select Value from ".$b[0]['DatabaseName'].".dbo.MerchantCredential where ID = '".$hid."'";
                            $h2 =  Yii::app()->db->createCommand($g2)->queryAll();
                            if($h2 != null){
                                $value = $h2[0]['Value'];

                                array_push($headers, $hname.': '.$value);
                            }
                        }
                    }

                    $requests = array(
                      'ProductId'=>$productid,
                      'CardNo'=>$cardno,
                      'Month'=>$month,
                      'Date'=>'',
                      'Barcode'=>$barcode,
                      'RFID'=>$rfid
                    );

                    $jsonPost = json_encode($requests);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Ignore Verify SSL Certificate
                    curl_setopt_array($ch, array(
                      CURLOPT_POST => TRUE,
                      CURLOPT_RETURNTRANSFER => TRUE,
                      CURLOPT_HTTPHEADER => $headers,
                      CURLOPT_POSTFIELDS => $jsonPost,
                    ));
                    $output = curl_exec($ch); // This is API Response
                    $err = curl_error($ch);
                    curl_close($ch);
                    $result = json_decode($output, true);

                    if($result != null){
                        if($result['StatusCode'] == '000'){

                            $transno = '';
                            if(isset($result['TransNo'])){
                                $transno = $result['TransNo'];
                            }

                            $response['Status'] = true;
                            $response['TransNo'] = $transno;
                            $response['CardNo'] = $cardno;
                            $response['Tanggal'] = date('Y-m-d H:i:s');
                            $response['StatusCode'] = '000';
                            $response['Message'] = 'Success buy product location';

                            return $response;
                        }
                        else{
                            $message = '';
                            if(isset($result['Message'])){
                                $message = $result['Message'];
                            }

                            $response['Message'] = $message;

                            return $response;
                        }
                    }
                    else{
                        $response['Message'] = 'Failed get response';

                        return $response;
                    }
                }
                else{
                    $response['Message'] = 'Failed get card';

                    return $response;
                }
            }
            else{
                $response['Message'] = 'Failed';

                return $response;
            }
        }
        else{
            $response['Message'] = 'Connection Failed';

            return $response;
        }
    }

    public function getProduct($kid, $productid, $locationid){

        $response = array(
            'Status'=>false,
            'StatusCode'=>'200',
            'Message'=>'',
            'Data'=>array()
        );

        $a = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $c = "select* from ".$b[0]['DatabaseName'].".dbo.MerchantHit where ActionHit = 'product' and LocationID = '$locationid' and Status = 1";
            $d =  Yii::app()->db->createCommand($c)->queryAll();
            if($d != null){
                $url = $d[0]['URL'];
                $methodhit = $d[0]['HitMethod'];

                $id = $d[0]['ID'];
                $mid = $d[0]['MerchantID'];
                $isHeader = $d[0]['IsHeader'];

                $headers = array();
                if($isHeader == 1){
                    $ee = "select* from ".$b[0]['DatabaseName'].".dbo.MerchantHeader where MerchantHitID = '".$id."'";
                    $ff =  Yii::app()->db->createCommand($ee)->queryAll();
                    foreach ($ff as $f) {
                        $hid = $f['MerchantCredentialID'];
                        $hname = $f['Name'];

                        $value = '';
                        $g2 = "select Value from ".$b[0]['DatabaseName'].".dbo.MerchantCredential where ID = '".$hid."'";
                        $h2 =  Yii::app()->db->createCommand($g2)->queryAll();
                        if($h2 != null){
                            $value = $h2[0]['Value'];

                            array_push($headers, $hname.': '.$value);
                        }
                    }
                }

                $requests = array(
                  'ProductID'=>$productid
                );

                $jsonPost = json_encode($requests);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Ignore Verify SSL Certificate
                curl_setopt_array($ch, array(
                  CURLOPT_POST => TRUE,
                  CURLOPT_RETURNTRANSFER => TRUE,
                  CURLOPT_HTTPHEADER => $headers,
                  CURLOPT_POSTFIELDS => $jsonPost,
                ));
                $output = curl_exec($ch); // This is API Response
                $err = curl_error($ch);
                curl_close($ch);
                $result = json_decode($output, true);
                if($result != null){
                    if(isset($result['StatusCode']) and $result['StatusCode'] != null and $result['StatusCode'] != ''){
                        if($result['StatusCode'] == '000'){
                            $response['Status'] = true;
                            $response['StatusCode'] = '000';
                            $response['Message'] = 'Success get product '.$productid;

                            if($result['Data'] != null){
                                foreach($result['Data'] as $data){
                                    if(isset($data['ProductID']) and isset($data['Nama']) and isset($data['Jenis']) and isset($data['Deskripsi']) and isset($data['Quota']) and isset($data['Harga']) and isset($data['MasaBerlaku']) and isset($data['TipeKendaraan'])){

                                        $uu = "select * from ".$b[0]['DatabaseName'].".dbo.VehicleType where Code = '".$data['TipeKendaraan']."'";
                                        $ii =  Yii::app()->db->createCommand($uu)->queryAll();
                                        if($ii != null){
                                            $jumlahpoint = 0;
                                            $nn = "select top 1 * from ".$b[0]['DatabaseName'].".dbo.ParkingSetting";
                                            $kk =  Yii::app()->db->createCommand($nn)->queryAll();
                                            if($kk != null){
                                                $jumlahpoint = $data['Harga']/round($kk[0]['ValueConversion']);
                                            }
                                            else{
                                                $jumlahpoint = $data['Harga']/1000;
                                            }

                                            $data = array(
                                                'ProductID'=>$data['ProductID'],
                                                'ProductName'=>$data['Nama'],
                                                'VehicleID'=>$ii[0]['VehicleID'],
                                                'Quota'=>$data['Quota'],
                                                'Price'=>$data['Harga'],
                                                'Period'=>$data['MasaBerlaku'],
                                                'DeductPoint'=>$jumlahpoint
                                            );

                                            array_push($response['Data'], $data);
                                        }
                                        else{
                                            $response['Message'] = 'Invalid vehicle type';

                                            return $response;
                                        }
                                    }
                                    else{
                                        $response['Message'] = 'Invalid require data';

                                        return $response;
                                    }
                                }

                                return $response;
                            }
                            else{
                                $response['Message'] = 'Invalid require data';

                                return $response;
                            }
                        }
                        else{
                            $response['Message'] = 'Failed';

                            return $response;
                        }
                    }
                    else{
                        $response['Message'] = 'Invalid Status Code';

                        return $response;
                    }
                }
                else{
                    $response['Message'] = 'Failed get response';

                    return $response;
                }
            }
            else{
                $response['Message'] = 'Failed';

                return $response;
            }
        }
        else{
            $response['Message'] = 'Connection Failed';

            return $response;
        }
    }

    public function getProductMonthly($kid, $locationid){
        $response = array(
            'Status'=>false,
            'StatusCode'=>'200',
            'Message'=>'',
            'Data'=>array()
        );

        $do = 'productMonthly';

        $a = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $c = "select top 1 * from ".$b[0]['DatabaseName'].".dbo.MerchantHit where LocationID = '$locationid' and ActionHit = '".$do."' and Status = 1";
            $d =  Yii::app()->db->createCommand($c)->queryAll();
            if($d != null){
                $id = $d[0]['ID'];
                $mid = $d[0]['MerchantID'];
                $url = $d[0]['URL'];
                $methodhit = $d[0]['HitMethod'];
                $isHeader = $d[0]['IsHeader'];

                $headers = array();
                if($isHeader == 1){
                    $ee = "select* from ".$b[0]['DatabaseName'].".dbo.MerchantHeader where MerchantHitID = '".$id."'";
                    $ff =  Yii::app()->db->createCommand($ee)->queryAll();
                    foreach ($ff as $f) {
                        $hid = $f['MerchantCredentialID'];
                        $hname = $f['Name'];

                        $value = '';
                        $g2 = "select Value from ".$b[0]['DatabaseName'].".dbo.MerchantCredential where ID = '".$hid."'";
                        $h2 =  Yii::app()->db->createCommand($g2)->queryAll();
                        if($h2 != null){
                            $value = $h2[0]['Value'];

                            array_push($headers, $hname.': '.$value);
                        }
                    }
                }

                $requests = array(
                  'LocationID'=>$locationid
                );

                $jsonPost = json_encode($requests);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Ignore Verify SSL Certificate
                curl_setopt_array($ch, array(
                  CURLOPT_POST => TRUE,
                  CURLOPT_RETURNTRANSFER => TRUE,
                  CURLOPT_HTTPHEADER => $headers,
                  CURLOPT_POSTFIELDS => $jsonPost,
                ));
                $output = curl_exec($ch); // This is API Response
                $err = curl_error($ch);
                curl_close($ch);
                $result = json_decode($output, true);
                if($result != null){
                    if(isset($result['StatusCode']) and $result['StatusCode'] != null and $result['StatusCode'] != ''){
                        if($result['StatusCode'] == '000'){
                            $response['Status'] = true;
                            $response['StatusCode'] = '000';
                            $response['Message'] = 'Success get product location '.$locationid;

                            if($result['Data'] != null){
                                foreach($result['Data'] as $data){
                                    if(isset($data['ProductID']) and isset($data['Nama']) and isset($data['Jenis']) and isset($data['Deskripsi']) and isset($data['Quota']) and isset($data['Harga']) and isset($data['MasaBerlaku'])){

                                        $jumlahpoint = 0;
                                        $nn = "select top 1 * from ".$b[0]['DatabaseName'].".dbo.ParkingSetting";
                                        $kk =  Yii::app()->db->createCommand($nn)->queryAll();
                                        if($kk != null){
                                            $jumlahpoint = $data['Harga']/round($kk[0]['ValueConversion']);
                                        }
                                        else{
                                            $jumlahpoint = $data['Harga']/1000;
                                        }

                                        $data = array(
                                            'ProductID'=>$data['ProductID'],
                                            'Nama'=>$data['Nama'],
                                            'Jenis'=>$data['Jenis'],
                                            'Quota'=>$data['Quota'],
                                            'Deskripsi'=>$data['Deskripsi'],
                                            'Harga'=>$data['Harga'],
                                            'MasaBerlaku'=>$data['MasaBerlaku'],
                                            'JumlahPoint'=>$jumlahpoint,
                                            'Gambar'=>$data['Gambar']
                                        );

                                        array_push($response['Data'], $data);
                                    }
                                    else{
                                        $response['Message'] = 'Invalid require data';

                                        return $response;
                                    }
                                }

                                return $response;
                            }
                            else{
                                $response['Message'] = 'Invalid require data';

                                return $response;
                            }
                        }
                        else{
                            $response['Message'] = 'Failed';

                            return $response;
                        }
                    }
                    else{
                        $response['Message'] = 'Invalid Status Code';

                        return $response;
                    }
                }
                else{
                    $response['Message'] = 'Failed get response';

                    return $response;
                }
            }
            else{
                $response['Message'] = 'Not accessible';

                return $response;
            }
        }
        else{
            $response['Message'] = 'Connection refused';

            return $response;
        }
    }

    public function getProductDaily($kid, $locationid){
        $response = array(
            'Status'=>false,
            'StatusCode'=>'200',
            'Message'=>''
        );

        $do = 'productDaily';

        $a = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
        $b =  Yii::app()->db->createCommand($a)->queryAll();
        if($b != null){
            $c = "select top 1 * from ".$b[0]['DatabaseName'].".dbo.MerchantHit where LocationID = '$locationid' and ActionHit = '".$do."' and Status = 1";
            $d =  Yii::app()->db->createCommand($c)->queryAll();
            if($d != null){
                $id = $d[0]['ID'];
                $mid = $d[0]['MerchantID'];
                $url = $d[0]['URL'];
                $methodhit = $d[0]['HitMethod'];
                $isHeader = $d[0]['IsHeader'];

                $headers = array();
                if($isHeader == 1){
                    $ee = "select* from ".$b[0]['DatabaseName'].".dbo.MerchantHeader where MerchantHitID = '".$id."'";
                    $ff =  Yii::app()->db->createCommand($ee)->queryAll();
                    foreach ($ff as $f) {
                        $hid = $f['MerchantCredentialID'];
                        $hname = $f['Name'];

                        $value = '';
                        $g2 = "select Value from ".$b[0]['DatabaseName'].".dbo.MerchantCredential where ID = '".$hid."'";
                        $h2 =  Yii::app()->db->createCommand($g2)->queryAll();
                        if($h2 != null){
                            $value = $h2[0]['Value'];

                            array_push($headers, $hname.': '.$value);
                        }
                    }
                }

                $requests = array(
                  'LocationID'=>$locationid
                );

                $jsonPost = json_encode($requests);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Ignore Verify SSL Certificate
                curl_setopt_array($ch, array(
                  CURLOPT_POST => TRUE,
                  CURLOPT_RETURNTRANSFER => TRUE,
                  CURLOPT_HTTPHEADER => $headers,
                  CURLOPT_POSTFIELDS => $jsonPost,
                ));
                $output = curl_exec($ch); // This is API Response
                $err = curl_error($ch);
                curl_close($ch);
                $result = json_decode($output, true);
                if($result != null){
                    if(isset($result['StatusCode']) and $result['StatusCode'] != null and $result['StatusCode'] != ''){
                        if($result['StatusCode'] == '000'){
                            $response['Status'] = true;
                            $response['StatusCode'] = '000';
                            $response['Message'] = 'Success get product location '.$locationid;

                            if($result['Data'] != null){
                                foreach($result['Data'] as $data){
                                    if(isset($data['ProductID']) and isset($data['Nama']) and isset($data['Jenis']) and isset($data['Deskripsi']) and isset($data['Quota']) and isset($data['Harga']) and isset($data['MasaBerlaku']) and isset($data['JumlahPoint'])){

                                        $data = array(
                                            'ProductID'=>$data['ProductID'],
                                            'Nama'=>$data['Nama'],
                                            'Jenis'=>$data['Jenis'],
                                            'Quota'=>$data['Quota'],
                                            'Deskripsi'=>$data['Deskripsi'],
                                            'Harga'=>$data['Harga'],
                                            'MasaBerlaku'=>$data['MasaBerlaku'],
                                            'JumlahPoint'=>$data['JumlahPoint'],
                                            'Gambar'=>$data['Gambar']
                                        );

                                        array_push($response['Data'], $data);
                                    }
                                    else{
                                        $response['Message'] = 'Invalid require data';

                                        return $response;
                                    }
                                }

                                return $response;
                            }
                            else{
                                $response['Message'] = 'Invalid require data';

                                return $response;
                            }
                        }
                        else{
                            $response['Message'] = 'Failed';

                            return $response;
                        }
                    }
                    else{
                        $response['Message'] = 'Invalid Status Code';

                        return $response;
                    }
                }
                else{
                    $response['Message'] = 'Failed get response';

                    return $response;
                }
            }
            else{
                $response['Message'] = 'Not accessible';

                return $response;
            }
        }
        else{
            $response['Message'] = 'Connection refused';

            return $response;
        }
    }

    public function decrypt($barcode){
        $keyorigin = "iMijz5XlckKwsQqcHedfLbf66muVMzfI6rl9KBxTScu8BbjoJU3y2t9rFmSHcdfMhQrakszK7F1cHMbQ9w72hQ==";
        $key = $this->getKey($keyorigin);

        $card_id = str_replace(' ','+',$barcode);

        if(strlen($card_id) >= 108){
            $c = base64_decode($card_id);
            $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
            $iv = substr($c, 0, $ivlen);
            $hmac = substr($c, $ivlen, $sha2len=32);
            $ciphertext_raw = substr($c, $ivlen+$sha2len);
            $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
            $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
            if (hash_equals($hmac, $calcmac))
            {
                return $original_plaintext;
            }
            else{
                return $card_id;
            }
        }
        else{
            return $card_id;
        }
    }

    public function getKey($key){
        $c = base64_decode($key);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, 'c8f6dae75c5e2010d46924649e0c0357', $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, 'c8f6dae75c5e2010d46924649e0c0357', $as_binary=true);
        if (hash_equals($hmac, $calcmac))
        {
            return $original_plaintext;
        }
        else{
            return $key;
        }
    }
}
