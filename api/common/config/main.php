<?php
$root = dirname(dirname(dirname(__FILE__)));
Yii::setPathOfAlias('site', $root);

$local = include dirname(__FILE__).'/local.php';
$global = include dirname(__FILE__).'/global.php';
$main = CMap::mergeArray($global, $local);

$httpRequest = new CHttpRequest();
$baseUrl = rtrim($httpRequest->getScriptUrl(),'\\/');

return $main;