<?php

class Root extends CApplicationComponent
{
    /**
     * @var string
     */
    private $_url;

    /**
     * @var string
     */
    private $_path;

    /**
     * Get the level of the root path with document root as level 0
     * @return int
     */
    public function getLevel()
    {
        $root = $this->getPath();
        $rootArray = explode(DIRECTORY_SEPARATOR, $root);
        $basePath = dirname(Yii::app()->basePath);
        $basePathArray = explode(DIRECTORY_SEPARATOR, $basePath);

        $basePathArrayCount = count($basePathArray);
        for ($i=0; $i<$basePathArrayCount; $i++) {
            if (count($rootArray) > $i && $rootArray[$i] == $basePathArray[$i]) {
                unset($basePathArray[$i]);
            }
        }

        return count($basePathArray);
    }

    public function setUrl($url)
    {
        $this->_url = $url;
    }

    public function getUrl()
    {
        if ($this->_url === null) {
            $this->_url = Yii::app()->baseUrl;

            $level = $this->getLevel();
            for ($i=0; $i<$level; $i++) {
                $this->_url = rtrim(dirname($this->_url),'\\/');
            }
        }

        return $this->_url;
    }

    public function setPath($path)
    {
        $this->_path = $path;
    }

    public function getPath()
    {
        if ($this->_path === null) {
            $this->_path = Yii::getPathOfAlias('root');
        }

        return Yii::getPathOfAlias('root');
    }
}