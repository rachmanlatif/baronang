<?php

abstract class MyEnum
{
    /**
     * @var array
     */
    public static $labels = array();

    /**
     * Get new instance statically
     * @return mixed
     */
    public static function instance()
    {
        $className = get_called_class();
        return new $className();
    }

    /**
     * @param string $translationContext
     * @return array
     */
    public static function getList($translationContext='string')
    {
        $class = self::instance();
        $reflection = new ReflectionClass( get_class( $class ) );

        $list = array();
        $constants = $reflection->getConstants();

        foreach ($constants as $constant) {
            $list[$constant] = Yii::t($translationContext,$constant);
        }

        return $list;
    }
    
    public static function getLabel($id)
    {
            $list = static::getList();
            if (isset($list[$id])) {
                    return $list[$id];
            }

            return '-';
    }
}