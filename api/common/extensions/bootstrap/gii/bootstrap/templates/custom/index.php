<?php echo "<?php\n"; ?>

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#<?php echo $this->class2id($this->modelClass); ?>-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?></h1>
<hr />

<?php echo "<?php\n"; ?>$this->widget('ext.widgets.form.FormLinkButtons', array(
	'linkButtons'=>array(
		array('label'=>'Create', 'url'=>array('create'), 'access'=>array('<?php echo $this->modelClass; ?>Create')),
	),
)); ?>

<!--<?php echo "<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>"; ?>-->

<div class="search-form" style="display:none">
<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";
	echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array(
				'view'=>array(
					'visible'=>'Yii::app()->user->checkAccess("<?php echo $this->modelClass; ?>View") && MyHelper::checkOptionalAccesses(array("<?php echo $this->modelClass; ?>Own"), array("model"=>$data))',
				),
				'update'=>array(
					'visible'=>'Yii::app()->user->checkAccess("<?php echo $this->modelClass; ?>Update") && MyHelper::checkOptionalAccesses(array("<?php echo $this->modelClass; ?>Own"), array("model"=>$data))',
				),
				'delete'=>array(
					'visible'=>'Yii::app()->user->checkAccess("<?php echo $this->modelClass; ?>Delete") && MyHelper::checkOptionalAccesses(array("<?php echo $this->modelClass; ?>Own"), array("model"=>$data))',
				),
			),
		),
	),
)); ?>