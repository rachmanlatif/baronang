<?php

class ARException extends MyException
{
	public function __construct($message='')
	{
		if ($message == '')
			$message = 'Data validation error';

		FlashMessageHelper::setError($message);
		parent::__construct($message);
	}
}