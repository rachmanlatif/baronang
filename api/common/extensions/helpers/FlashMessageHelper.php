<?php

class FlashMessageHelper
{
	/**
	 * @param string $key
	 * @param string $value
	 */
	public static function setFlash($key, $value)
	{
		if ($key != '' && $value != '') {
			Yii::app()->user->setFlash($key, $value);
		}
	}

	/**
	 * @param string $key
	 * @return string
	 */
	public static function getFlash($key)
	{
		return Yii::app()->user->getFlash($key);
	}

	/**
	 * @param string $message
	 */
	public static function setSuccess($message)
	{
		self::setFlash('success', $message);
	}

	/**
	 * @return string
	 */
	public static function getSuccess()
	{
		return self::getFlash('success');
	}

	/**
	 * @param string $message
	 */
	public static function setError($message)
	{
		self::setFlash('error', $message);
	}

	/**
	 * @return string
	 */
	public static function getError()
	{
		return self::getFlash('error');
	}
}