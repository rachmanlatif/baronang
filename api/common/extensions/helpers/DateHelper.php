<?php

class DateHelper
{

	/**
	*	format date to IsoSql
	*/
	public static function formatIsoSql($date) {
		return self::formatDate($date, 'Y-m-d H:i:s');
	}

	/**
	*	get current date (just date part)
	*/	
	public static function currentDate($format='Y-m-d') {
		return date($format);
	}

    /**
     * Get current timestamp (date + time)
     * @param string $format
     * @return string
     */
    public static function now($format='Y-m-d H:i:s')
    {
        return date($format);
    }

    /**
     * Format a date
     * @param string $date
     * @param null|string $format
     * @return string
     */
    public static function formatDate($date, $format=null)
    {
        if ($date == null)
            return $date;

        if ($format == null) {
            $format='d-M-Y';
        }

        return date($format, strtotime($date));
    }

    /**
     * Format a Date Time
     * @param string $date
     * @param null|string $format
     * @return string
     */
    public static function formatDateTime($date, $format=null)
    {
        if ($format == null) {
            $format='d-M-Y H:i:s';
        }

        return self::formatDate($date, $format);
    }

	/**
	return date formatted as sql date
	*/
	public static function formatSqlDate($date) {
		return self::formatDate($date, 'Y-m-d');
	}

	/**
	@param strDate without time element
	@param strStartTime without date element
	@param strDuration HH:MM
	@return DateTime
	*/
	public static function addTime($strDate, $strStartTime, $strDuration) {
		$dt = new DateTime($strDate . " " . $strStartTime);
		$expls = explode($strDuration, ':');
		$sIntervalSpec = 'PT' . $expls[0] . 'H'. $expls[1] . 'M';
		$di = new DateInterval($sIntervalSpec);
		return $dt.add($di);
	}

	/**
	@param strDate without time element
	@param strStartTime without date element
	@param strMinutes in minutes
	@return DateTime
	*/
	public static function addMinute($strDate, $strStartTime, $strMinutes) {
		$dt = new DateTime($strDate . " " . $strStartTime);
		$sIntervalSpec = 'PT' . $strMinutes .  'M';
		$di = new DateInterval($sIntervalSpec);
		return $dt->add($di);
	}

    /**
     * Validate time format 24 hours in HH:MM
     * @param string $time
     * @return boolean
     */
    public static function validateTimeFormat($time){
        return  preg_match("/(2[0-3]|[01][0-9]):[0-5][0-9]/", $time);
    }

    /**
     * Convert integer into month name in bahasa
     * @param $month integer
     * @return mixed return string if success and return FALSE if failed
     */
    public static function getMonthNameInBahasa($month){
        $months = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni','Juli', 'Agustus','September', 'Oktober', 'November','Desember');
        if($month>=1 && $month<= 12){
            return $months[$month-1];
        }
        else{
            return FALSE;
        }

    }

    /**
     * Get List of Month in Bahasa
     * @return array
     */
    public static function getMonthListInBahasa(){
        $months = array(
            1=> 'Januari',
            2=> 'Februari',
            3=> 'Maret',
            4=> 'April',
            5=> 'Mei',
            6=> 'Juni',
            7=> 'Juli',
            8=> 'Agustus',
            9=> 'September',
            10=> 'Oktober',
            11=> 'November',
            12=> 'Desember'
        );
        return $months;

    }

    /**
     * Get List of Year
     * @return array
     */
    public static function getYearList($start = NULL, $end = NULL){
        if($start == NULL){
            $start = date('Y');
        }

        if($end == NULL){
            $end = date('Y') + 5;
        }
        $data = array();
        for($i = $start; $i<= $end; $i++){
            $data[$i] = $i;
        }
        return $data;

    }

    /**
     * Get total days in 1 month specific year
     * @param $month integer
     * @param $year integer
     * @return integer
     */
    public static function getTotalDaysInMonth($month, $year){
        $totalDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        return $totalDays;
    }

}
