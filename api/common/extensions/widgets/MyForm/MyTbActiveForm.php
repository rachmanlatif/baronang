<?php

Yii::import('bootstrap.widgets.TbActiveForm');

class MyTbActiveForm extends TbActiveForm
{
	public $renderForm = true;

	public function init()
	{
		if (!isset($this->htmlOptions['class'])) {
			$this->htmlOptions['class'] = 'form-' . $this->type;
		} else {
			$this->htmlOptions['class'] .= ' form-' . $this->type;
		}

		if (!isset($this->inlineErrors)) {
			$this->inlineErrors = $this->type === self::TYPE_HORIZONTAL;
		}

		if ($this->inlineErrors) {
			$this->errorMessageCssClass = 'help-inline error';
		} else {
			$this->errorMessageCssClass = 'help-block error';
		}

		if(!isset($this->htmlOptions['id']))
			$this->htmlOptions['id']=$this->id;
		else
			$this->id=$this->htmlOptions['id'];

		if ($this->renderForm) {
			if($this->stateful)
				echo CHtml::statefulForm($this->action, $this->method, $this->htmlOptions);
			else
				echo CHtml::beginForm($this->action, $this->method, $this->htmlOptions);
		}
	}
}