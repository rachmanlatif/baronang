<?php

class ColorPicker extends CWidget
{
    public $baseScriptUrl;
	public $id='colorPicker';
	public $eventName = 'click';
    public $color = 'ff0000';

    public function init()
    {
        if ($this->baseScriptUrl === null)
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
    }
		
	public function addConfig($configs)
	{
		foreach ($configs as $key=>$config) {
			$this->_gridview_config[$key] = $config;
		}
	}
	
	protected function registerClientScript()
	{
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($this->baseScriptUrl .'/css/colorpicker.css');
        $cs->registerScriptFile($this->baseScriptUrl . '/js/colorpicker.js');

		$originalScriptId = $scriptId = 'colorPicker';
		$num = 0;
		while($cs->isScriptRegistered($scriptId)) {
			$num++;
			$scriptId = $originalScriptId . $num;
		}

		
		$cs->registerScript($scriptId, "
		    $('#".$this->id."').ColorPicker({
		        eventName: '".$this->eventName."',
		        color: '".$this->color."',
		        onSubmit: function(hsb, hex, rgb, el) {
                    $(el).val(hex);
                    $(el).ColorPickerHide();
                },
		    });
		");
	}

	public function run()
	{
		$this->registerClientScript();
	}
}