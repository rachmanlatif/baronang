<div class="popup-search-container" id="<?php echo $id; ?>">
	<div class="popup-search-box">
		<div class="popup-search-close"><a href="javascript:void(0)" class="popup-search-close-button"></a></div>
		<div class="popup-search">
			<?php echo $content; ?>
		</div>
	</div>
</div>