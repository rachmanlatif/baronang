// VERTICALLY ALIGN FUNCTION
$.fn.vAlign = function() {
	return this.each(function() {
		var top = ($(window).height() - $(this).outerHeight()) / 2;
		var left = ($(window).width() - $(this).outerWidth()) / 2;
		var margin_top = ($(this).outerHeight() / 2) * -1;
		var margin_left = ($(this).outerWidth() / 2) * -1;

        if (top >= 0) {
            $(this).css({
                'position'		: 'fixed',
                'margin'		: 0,
                'top'			: '50%',
                'left'			: '50%',
                'margin-top'	: (top > 0 ? margin_top : 0)+'px',
                'margin-left'	: (left > 0 ? margin_left : 0)+'px'
            });
        } else {
            $(this).css({
                'position'		: 'fixed',
                'margin'		: 0,
                'top'			: 0,
                'left'			: '50%',
                'margin-top'	: 0,
                'margin-left'	: (left > 0 ? margin_left : 0)+'px'
            });
        }
	});
};