<?php if ($taskList != null): ?>
    <ul id="task-list">
    <?php foreach($taskList as $task): ?>
        <li>
            <div class="task-list-header">
                <div class="task-list-title"><?php echo $task->task; ?></div>
                <div class="task-list-date">exp: <?php echo DateHelper::formatDate($task->due_date, 'd M Y'); ?></div>
                <div class="clear"></div>
            </div>
            <div class="task-list-body">
                <div class="task-list-remark"><?php echo $task->remark; ?></div>
                <div class="task-list-action">
                    <a href="#">DONE</a>
                </div>
                <div class="clear"></div>
            </div>
        </li>
    <?php endforeach; ?>
    </ul>
<?php endif; ?>