<?php

class DatePicker extends CWidget
{
	public $id = 'datePicker';
	public $dateFormat = 'yy-mm-dd';
    public $changeMonth = true;
    public $changeYear = true;
    public $baseScriptUrl;
    public $theme = 'smoothness';
    public $jqueryUi = true;
    public $yearRange = null;
    public $minDate = NULL;
    public $maxDate = NULL;
    
    public function init()
    {
        if ($this->baseScriptUrl === null)
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
        if($this->yearRange==null){
            $this->yearRange = '\'1900:'.  date('Y').'\'';
        }
    }
		
	public function addConfig($configs)
	{
		foreach ($configs as $key=>$config) {
			$this->_gridview_config[$key] = $config; 
		}
	}
	
	protected function registerClientScript()
	{
            $cs = Yii::app()->clientScript;
            if ($this->jqueryUi) {
                $cs->registerCssFile($this->baseScriptUrl . '/jquery-ui/'. $this->theme .'/jquery-ui-1.8.23.custom.css');
                $cs->registerScriptFile($this->baseScriptUrl . '/jquery-ui/jquery-ui-1.10.3.custom.js');
            }
		
            $originalScriptId = $scriptId = 'datePicker';
            $num = 0;
            while($cs->isScriptRegistered($scriptId)) {
                    $num++;
                    $scriptId = $originalScriptId . $num;
            }
            $maxDateString = '';
            if($this->maxDate != NULL){
                $maxDateString = 'maxDate: '.$this->maxDate.',';
            }

            $minDateString = '';
            if($this->minDate != NULL){
                $minDateString = 'minDate: "'.$this->minDate.'",';
            }
			
            $cs->registerScript($scriptId, "
                    date = $('#". $this->id ."').val();
                    $('#". $this->id ."').datepicker({
                        changeMonth: ". $this->changeMonth .",
                        changeYear: ". $this->changeYear .",
                        ".$minDateString."
                        ".$maxDateString."    
                    });
                    $('#". $this->id ."').datepicker('option', 'dateFormat', '". $this->dateFormat ."');
                    $('#". $this->id ."').val(date);
            ");
		//add year range if need, code : yearRange: ". $this->yearRange .",
	}

	public function run()
	{
		$this->registerClientScript();
	}
}