<?php

class HasCreatedByBehavior extends CActiveRecordBehavior
{
    public function attach($owner)
    {
        parent::attach($owner);
        $this->owner->getMetaData()->addRelation('userCreate', array(CActiveRecord::BELONGS_TO, 'Pengguna', 'dibuat_oleh'));
    }

    public function beforeValidate($event)
    {
        if ($this->owner->isNewRecord) {
	        if (isset(Yii::app()->user->isMemberShipLogin)) {
		        $this->owner->dibuat_oleh = 0;
	        }
            else if (Yii::app()->user->isGuest){
                $this->owner->dibuat_oleh = 0;
            }
            else {
	            if($this->owner->dibuat_oleh===NULL || $this->owner->dibuat_oleh=== '')
		            $this->owner->dibuat_oleh = Yii::app()->user->id;
	        }
        }

        return parent::beforeValidate($event);
    }
}