<?php

class HasModifiedByBehavior extends CActiveRecordBehavior
{
    public function attach($owner)
    {
        parent::attach($owner);
        $this->owner->getMetaData()->addRelation('userModify', array(CActiveRecord::BELONGS_TO, 'Pengguna', 'diubah_oleh'));
    }

    public function beforeValidate($event)
    {
        if (!$this->owner->isNewRecord) {
	        if (isset(Yii::app()->user->isMemberShipLogin)) {
		        $this->owner->diubah_oleh = 0;
	        }
            else if (Yii::app()->user->isGuest){
                $this->owner->diubah_oleh = 0;
            }
            else if ($this->owner->diubah_oleh === null || $this->owner->diubah_oleh === '') {
                $this->owner->diubah_oleh = Yii::app()->user->id;
	        }
        }

        return parent::beforeValidate($event);
    }
}