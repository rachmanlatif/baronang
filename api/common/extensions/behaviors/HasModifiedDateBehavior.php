<?php

class HasModifiedDateBehavior extends CActiveRecordBehavior
{
    public function beforeValidate($event)
    {
        if (!$this->owner->isNewRecord) {
            $this->owner->tanggal_diubah = DateHelper::now();
        }

        return parent::beforeValidate($event);
    }
}