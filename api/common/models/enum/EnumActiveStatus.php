<?php

class EnumActiveStatus extends MyEnum
{
    const ACTIVE= 'Aktif';
    const NON_ACTIVE = 'Tidak Aktif';
}