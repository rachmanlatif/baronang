<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/images/icon.png">

    <!-- Core CSS - Include with every page -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/assets/css/admin.css" rel="stylesheet" type="text/css" />

    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- date-picker -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl;?>/plugins/datepicker/datepicker3.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.2 -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/assets/js/jquery-ui.min.js" type="text/javascript"></script>

    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- FastClick -->
    <script src='<?php echo Yii::app()->theme->baseUrl;?>/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="<?php echo Yii::app()->theme->baseUrl;?>/dist/js/app.min.js" type="text/javascript"></script>

    <script src="<?php echo Yii::app()->theme->baseUrl;?>/res/igomad/igomad.js" type="text/javascript"></script>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="hold-transition skin-blue layout-top-nav">
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Full Width Column -->
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content">
                <?php echo $content; ?>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
</div><!-- ./wrapper -->

</body>

</html>
