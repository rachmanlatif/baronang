<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Saldo Deposito");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'No');
    $objWorkSheet->SetCellValue('B1', 'Nomor Akun');
    $objWorkSheet->SetCellValue('C1', 'Nama');
    $objWorkSheet->SetCellValue('D1', 'Tanggal Buka');
    $objWorkSheet->SetCellValue('E1', 'Tanggal Tutup');
    $objWorkSheet->SetCellValue('F1', 'Produk');
    $objWorkSheet->SetCellValue('G1', 'Suku Bunga%');
    $objWorkSheet->SetCellValue('H1', 'Saldo');
    $objWorkSheet->SetCellValue('I1', 'Status');




    $no = 1;
    $row = 2;
    $x =  "select * from dbo.TimeDepositReport ORDER by MemberID Asc";
    $y = sqlsrv_query ($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        $bunga = $z[5].'%';
        
        //push untuk drop down list
        array_push($listCombo, $z[1]);

        $open = $z[7]->format('Y-m-d H:i:s');
                    $close = '';
                    if($z[9] != ''){
                        $close = $z[9]->format('Y-m-d H:i:s');
                    }

        $objWorkSheet->SetCellValue("A".$row, $no);
        $objWorkSheet->SetCellValueExplicit("B".$row, $z[2], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("C".$row, $z[1]);
        $objWorkSheet->SetCellValue("D".$row, $open);
        $objWorkSheet->SetCellValue("E".$row, $close);
        $objWorkSheet->SetCellValue("F".$row, $z[4]);
        $objWorkSheet->SetCellValueExplicit("G".$row, $bunga, PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("H".$row, $z[6], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("I".$row, $z[10]);
    
        $no++;
        $row++;
    }

    $objWorkSheet->setTitle('Saldo Deposito');

    $fileName = 'Saldodeposito'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
