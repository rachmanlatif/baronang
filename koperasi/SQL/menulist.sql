/****** Script for SelectTopNRows command from SSMS  ******/
insert into [KoperasiNew].[dbo].[MenuDetail] values(1,1,'General Setting','mgeneralsetting.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(1,2,'User Level','muserlevel.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(1,3,'User Account','museraccount.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(1,4,'Bank List','bank_list.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(1,5,'Bank Account','bank_acc.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(1,6,'Position','mposition.php',1,0);

insert into [KoperasiNew].[dbo].[MenuDetail] values(2,1,'Basic Saving Type','bas_sav_type.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(2,2,'Regular Saving Type','reg_sav_type.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(2,3,'Time Deposit Type','time_dep_type.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(2,4,'Loan Type','loan_type.php',1,0);

insert into [KoperasiNew].[dbo].[MenuDetail] values(3,1,'Open Account','#',1,1);
insert into [KoperasiNew].[dbo].[MenuChild] values(1,1,'Member','csoamemberlist.php',1);
insert into [KoperasiNew].[dbo].[MenuChild] values(1,2,'Regular Saving','csoaregsaving.php',1);
insert into [KoperasiNew].[dbo].[MenuChild] values(1,3,'Time Deposit','csoatimedeposit.php',1);
insert into [KoperasiNew].[dbo].[MenuChild] values(1,4,'Loan Application','csoaloanapp.php',1);

insert into [KoperasiNew].[dbo].[MenuDetail] values(3,2,'Close Account','#',1,1);
insert into [KoperasiNew].[dbo].[MenuChild] values(2,1,'Member','cscamember.php',1);
insert into [KoperasiNew].[dbo].[MenuChild] values(2,2,'Regular Saving','cscaregsaving.php',1);
insert into [KoperasiNew].[dbo].[MenuChild] values(2,3,'Time Deposit','cscatimedeposit.php',1);
insert into [KoperasiNew].[dbo].[MenuChild] values(2,4,'Loan','csloanclose.php',1);

insert into [KoperasiNew].[dbo].[MenuDetail] values(4,1,'Deposit','#',1,1);
insert into [KoperasiNew].[dbo].[MenuChild] values(1,1,'Basic Saving','dep_bas_sav.php',1);
insert into [KoperasiNew].[dbo].[MenuChild] values(1,2,'Regular Saving','dep_reg_sav.php',1);
insert into [KoperasiNew].[dbo].[MenuChild] values(1,3,'Loan Payment','dep_loan_pay.php',1);

insert into [KoperasiNew].[dbo].[MenuDetail] values(4,2,'Withdrawal','#',1,1);
insert into [KoperasiNew].[dbo].[MenuChild] values(2,1,'Regular Saving','dep_bas_sav.php',1);

insert into [KoperasiNew].[dbo].[MenuDetail] values(5,1,'Loan Chat','loan_chat.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(5,2,'Loan Approval','loan_app.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(5,3,'Loan Release','loan_rel.php',1,0);

insert into [KoperasiNew].[dbo].[MenuDetail] values(6,1,'Report Member List','rep_list.php',1,0);
insert into [KoperasiNew].[dbo].[MenuDetail] values(6,2,'Report Basic Saving','rep_bas.php',1,0);
