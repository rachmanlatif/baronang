<?php 
$servername = "35.240.183.132";
$username = "dev";
$password = "C0b@t3b@k";
$databaseName = "rsvp";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $databaseName);
?>
 
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RSVP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <!-- Collect the nav links, forms, and other content for toggling -->
       
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="rsvp.php" class="navbar-brand"><b>RSVP</b></a>
        </div>
        
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form>
        </div>
        
		</div>
        
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>RSVP</h1>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
			
		<?php
		$query_tablecount = "Select Salutation, Name, TableNo from NameList where HostTable='1' order by TableNo Asc";
		$result = mysqli_query($conn, $query_tablecount);
		while($row = mysqli_fetch_assoc($result)) {
			$TableNum = $row["TableNo"];
        ?>
        <div class="col-md-3">
          <div class="box box-success box-solid collapsed-box">
            <div class="box-header with-border bg-aqua-active">
              <h3 class="box-title">
				  <table>
				  <tr>
					  <td><?php echo $row["Name"]; ?></td>
					  <td style="text-align:right">&nbsp;</td>
					  <td style="text-align:right">&nbsp;</td>
					  <td style="text-align:right">#</td>
					  <td style="text-align:right"><?php echo $row["TableNo"]; ?></td>
				  </tr>
				</table></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table>
				  <?php
		$query_tablename = "Select Salutation, Name from NameList where TableNo = $TableNum and HostTable='0'";
		$result1 = mysqli_query($conn, $query_tablename);
		while($row1 = mysqli_fetch_assoc($result1)) {
        ?>
              <tr><td><?php echo $row1["Salutation"]." ".$row1["Name"]; ?></td></tr>
              
              <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
         
          <!-- /.box -->
        </div>
         <?php } ?>
        <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
 
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
