<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Neraca Akun");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->SetCellValue('A3', 'No Transaksi');
    $objWorkSheet->SetCellValue('B3', 'Tanggal Transaksi');
    $objWorkSheet->SetCellValue('C3', 'Tipe Transaksi');
    $objWorkSheet->SetCellValue('D3', 'Deskripsi');
    $objWorkSheet->SetCellValue('E3', 'Debit');
    $objWorkSheet->SetCellValue('F3', 'Kredit'); 
    $objWorkSheet->SetCellValue('G3', 'Total');    



    if($_GET['st'] == 3 ){
    $no = 1;
    $row = 4;

    $objWorkSheet->SetCellValueExplicit("A1",$_GET['acc1']);

    $aaa = "SELECT [TransactionNumber],[AccNumber],[TimeStam],[KodeTransactionType],[Descriptions],[Note],[Debet],[Kredit],[UserID] FROM [KoperasiNew].[dbo].[RegularSavingTrans] where AccNumber = '$_GET[acc1]' ORDER by TimeStam asc";
    $bbb = sqlsrv_query($conn, $aaa);
    $total=0;
    $atotal=$total;
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
    //var_dump($ccc).die;
                               
        if($ccc[6] > 0 ){
            $total-=$ccc[6];
            $atotal=$total;
        }
        else{
            $total+=$ccc[7];
            $atotal=$total;
        }
        
        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
        $bbbb = sqlsrv_query($conn, $aaaa);
        $abc = 0;
        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
        //  var_dump($cccc) ;
            if($cccc != null){
                    $abc = $cccc[1];
                }
                    else{
                    $abc = $ccc[3];
                }  


            $objWorkSheet->SetCellValue("A".$row,$ccc[0]);
            $objWorkSheet->SetCellValue("B".$row, $ccc[2]->format('Y-m-d H:i:s'));
            $objWorkSheet->SetCellValue("C".$row,$abc);
            $objWorkSheet->SetCellValue("D".$row,$ccc[5]);
            $objWorkSheet->SetCellValue("E".$row, number_format($ccc[6]), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->SetCellValue("F".$row, number_format($ccc[7]), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->SetCellValue("G".$row, number_format($total), PHPExcel_Cell_DataType::TYPE_STRING);            
           
                        

           
    
                $no++;
                $row++;
            
    }
    }
//exit;
    $objWorkSheet->setTitle('Drep Neraca Akun');

    $fileName = 'NeracaAK'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
