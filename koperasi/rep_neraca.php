<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Neraca'); ?></h3>
    </div>
    
    <div class="box-body">
    <?php if(!isset($_GET['acc']) and !isset($_GET['acc1']) and !isset($_GET['acc2']) and !isset($_GET['acc3'])){ ?>
      <div class="row">
            <form action="" method="post">
                <div class="form-group">
                    <label class="col-sm-1 control-label" style="text-align: left;"><?php echo lang('Tanggal'); ?></label>
                    <div class="col-sm-2">
                        <?php
                        if(isset($_POST['to'])){
                            $to = $_POST['to'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>
                        <input type="text" name="to" value="<?php echo $to; ?>" class="form-control datepicker">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('Pilih'); ?></button>
                        <a href="rep_neraca1.php"><button type="button" class="btn btn-sm btn-primary">Refresh</button></a>
                    </div>
                </div>
            </form>
        </div>        
    <?php } ?>


        <?php if($_GET['st'] == 5 ){ ?>

            <div class="table-responsive">
                <a href="drep_neracaHIS.php?acc3=<?php echo $_GET['acc3']; ?>&st=5"><button type="button" class="btn btn-primary">Download to excel</button></a>
                <a class="btn btn-primary" onclick="printDiv()" href="#">Print</a>


                <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">
                

                <div class="box-header with-border" align="center">
                    <tr>
                        <h3 class="box-title" align="center"><?php echo lang('Laporan Neraca All'); ?></h3>
                    </tr>
                </div>
                <table class="table table-bordered">
                                <tr>
                                    <th><?php echo 'Kode Transaksi'; ?></th>
                                    <th><?php echo 'Kode Akun'; ?></th>
                                    <th><?php echo 'Tanggal Transaksi'; ?></th>
                                    <th><?php echo 'Deskripsi'; ?></th>
                                    <th><?php echo 'Debit'; ?></th>
                                    <th><?php echo 'Kredit'; ?></th>
                                </tr>
                    <?php
                    $a = "exec dbo.TampilDetailReportCOA '0','$_GET[acc3]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                    ?>
                        <?php
                        $aa = "exec dbo.TampilDetailReportCOA '1','$c[0]'";
                        $bb = sqlsrv_query($conn, $aa);
                        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
                        ?>
                            <?php
                            $aaa = "exec dbo.TampilDetailReportCOA '2','$cc[0]'";
                            $bbb = sqlsrv_query($conn, $aaa);
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                            ?>
                               
                                <tr>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[1]; ?></td>
                                    <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                    <td>
                                        <?php
                                        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                        $bbbb = sqlsrv_query($conn, $aaaa);
                                        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                        if($cccc != null){
                                            echo $cccc[1];
                                        }
                                        else{
                                            echo $ccc[3];
                                        }
                                        ?>
                                    </td>
                                    <td align="right"><?php echo number_format($ccc[6],3); ?></td>
                                    <td align="right"><?php echo number_format($ccc[7],3); ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </table>
                </div>
            </div>
         <?php } ?>



        <?php if($_GET['st'] == 4 ){ ?>

            <div class="table-responsive">
                <a href="drep_neracaHISAK.php?acc2=<?php echo $_GET['acc2']; ?>&st=4"><button type="button" class="btn btn-primary">Download to excel</button></a>
                <a class="btn btn-primary" onclick="printDiv()" href="#">Print</a>

                <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">

                <div class="box-header with-border" align="center">
                    <tr>
                        <h3 class="box-title" align="center"><?php echo lang('Detail History'); ?></h3>
                    </tr>
                </div>

                <table class="table table-bordered">
                  
                        <thead>
                            <tr>
                                <th>No Transaksi : <?php echo $_GET['acc2']; ?></th>
                            </tr>
                            <tr>
                                <th>No</th>
                                <th><?php echo lang('Kode Jurnal'); ?></th>
                                <th><?php echo lang('Kode Akun'); ?></th>
                                <th><?php echo lang('Nama Akun'); ?></th>
                                <th><?php echo lang('Debit'); ?></th>
                                <th><?php echo lang('Kredit'); ?></th>
                                <th><?php echo lang('Deskripsi'); ?></th>
                                <th></th>
                                
                            </tr>
                        </thead>

                        <?php
                            $aaa = "SELECT * FROM ( SELECT a.KodeJurnal,a.KodeAccount,a.Debet,a.Kredit,a.Keterangan, ROW_NUMBER() OVER (ORDER BY b.Tanggal asc) as row FROM [dbo].[JurnalDetail] a inner join [dbo].[JurnalHeader] b on a.KodeJurnal = b.KodeJurnal where a.KodeJurnal= '$_GET[acc2]' ) a ORDER BY KodeJurnal asc";
                            //echo $aaa;

                            $bbb = sqlsrv_query($conn, $aaa);
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                            
                                
        
                                $noacc = '';
                                $acc = '';
                                $sql   = "select * from dbo.Account where KodeAccount='$ccc[1]'";
                                $stmt  = sqlsrv_query($conn, $sql);
                                $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                                if($row != null){
                                    $noacc = $row[0];
                                    $acc = $row[1];
                                }
                                ?>

                                <tr>
                                    <td><?php echo $ccc[5]; ?></td>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $noacc; ?></td>
                                    <td><?php echo $acc; ?></td>
                                    <td><?php echo number_format($ccc[2]); ?></td>
                                    <td><?php echo number_format($ccc[3]); ?></td>
                                    <td><?php echo $ccc[4]; ?></td>
                                    <td></td>
                                </tr>



                            
                        <?php } ?>
                    
                </table>
                </div>
            </div>
        <?php } ?>

         <?php if($_GET['st'] == 3 ){ ?>

            <div class="table-responsive">
                <a href="drep_neracaAK.php?acc1=<?php echo $_GET['acc1']; ?>&st=3"><button type="button" class="btn btn-primary">Download to excel</button></a>
                <a class="btn btn-primary" onclick="printDiv()" href="#">Print</a>

                <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">

                <div class="box-header with-border" align="center">
                    <tr>
                        <h3 class="box-title" align="center"><?php echo lang('History Per Akun'); ?></h3>
                    </tr>
                </div>
                
                <table class="table table-bordered">
                  
                        <thead>
                            <tr>
                                <th>No Akun : <?php echo $_GET['acc1']; ?></th>
                            </tr>
                            <tr>
                                <th>No Transaksi</th>
                                <th>Tanggal Transaksi</th>
                                <th><?php echo lang('Tipe Transaksi'); ?></th>
                                <th>Deskripsi</th>
                                <th><?php echo lang('Debit'); ?></th>
                                <th><?php echo lang('Kredit'); ?></th>
                                <th><?php echo lang('Total'); ?></th>
                                <th></th>
                                
                            </tr>
                        </thead>

                        <?php
                            $aaa = "SELECT [TransactionNumber],[AccNumber],[TimeStam],[KodeTransactionType],[Descriptions],[Note],[Debet],[Kredit],[UserID] FROM [KoperasiNew].[dbo].[RegularSavingTrans] where AccNumber = '$_GET[acc1]' ORDER by TimeStam asc";
                            $bbb = sqlsrv_query($conn, $aaa);
                            $total=0;
                            $atotal=$total;
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                                //var_dump($ccc).die;
                               
                                        if($ccc[6] > 0 ){
                                            $total-=$ccc[6];
                                            $atotal=$total;
                                        }
                                        else{
                                            $total+=$ccc[7];
                                            $atotal=$total;
                                        }
                                ?>

                                <tr>
                                    <td><a href="?acc2=<?php echo $ccc[0]; ?>&st=4"><?php echo $ccc[0]; ?></a></td>
                                    <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                    <td>
                                        <?php
                                        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                        $bbbb = sqlsrv_query($conn, $aaaa);
                                        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                        if($cccc != null){
                                            echo $cccc[1];
                                        }
                                        else{
                                            echo $ccc[3];
                                        }

                                        ?>
                                    </td>
                                    <td><?php echo $ccc[5]; ?></td>
                                    <td align="right"><?php echo number_format($ccc[6],3); ?></td>
                                    <td align="right"><?php echo number_format($ccc[7],3); ?></td>
                                    <td align="right"><?php echo number_format($total,3); ?></td>
                                    <td>

                                    </td>
                                </tr>

                            
                        <?php } ?>


                        <tr>
                                <th colspan="6"><?php echo lang('Total Saldo Akhir'); ?></th>
                                <th><span class="pull-right"><?php echo number_format($atotal,3); ?></span></th>
                                <th></th>
                        </tr>
                </table>
                </div>
            </div>
         <?php } ?>




        <?php if($_GET['acc']){ ?>

            <div class="table-responsive">
                <a href="drep_neraca.php?acc=<?php echo $_GET['acc']; ?>"><button type="button" class="btn btn-primary">Download to excel</button></a>
                <a href="rep_neraca.php"><button type="button" class="btn btn-primary">Back</button></a>
                <!--<a class="btn btn-primary" target="_blank" href="print_neraca1.php?acc=<?php echo $_GET['acc']; ?>">Print</a>-->
                <a class="btn btn-primary" onclick="printDiv()" href="#">Print</a>
                <tr >
                    <th colspan="6" class="pull-right"><a href="?acc3=<?php echo $_GET['acc']; ?>&st=5"><?php echo 'History All' ?></a></th>
                </tr> 

                <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">
                

                <div class="box-header with-border" align="center">
                    <tr>
                        <h3 class="box-title" align="center"><?php echo lang('Laporan Neraca Berdasarkan Akun'); ?></h3>
                    </tr>
                </div>


                <table class="table table-bordered">
                    <?php
                    $a = "exec dbo.TampilDetailReportCOA '0','$_GET[acc]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                    ?>
                        <tr>
                            <th width="30%"><?php echo $c[1]; ?></th>
                            <th></th>
                            <th width="30%"><?php echo 'No Akun'; ?></th>
                            <th width="30%"><?php echo 'Nama Akun'; ?></th>
                            <th width="30%"><?php echo 'Nilai'; ?></th>
                            <th colspan="4"></th>
                        </tr>
                        <?php
                        $aa = "exec dbo.TampilDetailReportCOA '1','$c[0]'";
                        $bb = sqlsrv_query($conn, $aa);
                        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td></td>
                            <td><td><a href="?acc1=<?php echo $cc[0]; ?>&st=3"><?php echo $cc[0]; ?></a></td>
                            <td><?php echo $cc[1]; ?></td>
                            <td align="right"><?php echo number_format($cc[2]); ?></td>
                            <td colspan="4"></td>
                        </tr>
                            <!--<?php
                            $aaa = "exec dbo.TampilDetailReportCOA '2','$cc[0]'";
                            $bbb = sqlsrv_query($conn, $aaa);
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                            ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[1]; ?></td>
                                    <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                    <td>
                                        <?php
                                        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                        $bbbb = sqlsrv_query($conn, $aaaa);
                                        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                        if($cccc != null){
                                            echo $cccc[1];
                                        }
                                        else{
                                            echo $ccc[3];
                                        }
                                        ?>
                                    </td>
                                    <td align="right"><?php echo number_format($ccc[6]); ?></td>
                                    <td align="right"><?php echo number_format($ccc[7]); ?></td>
                                </tr>
                            <?php } ?>-->
                        <?php } ?>
                    <?php } ?>
                </table>
                </div>
            </div>

        <?php } ?>

        <?php if(isset($_POST['to']) and !isset($_POST['acc'])){
            $from = date('Y-m-d', strtotime($_POST['from']));
            $to = date('Y-m-d', strtotime($_POST['to']));
            $a = "exec dbo.ProsesGenerateLaporanNeraca '$to','$_SESSION[Name]'";
            //ech;
            $b = sqlsrv_query($conn, $a);
            ?>
            <div class="table-responsive ">
            <table class="table table-bordered">
                    <td colspan="2">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <ul id="treeview">
                                    <?php
                                        $atotal = 1;
                                        $x = "select* from dbo.GroupAcc where KodeGroup in('1.0.00.00.000')";
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <li data-icon-cls="fa fa-folder"><?php echo $z[0].' - '.$z[1].' - '.$z[1]; ?>
                                            <span class="pull-right"></i> 0 </span>
                                            <ul id="treeview">
                                            <?php
                                            $atotal1 = 0;
                                            $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                            //echo $xx;
                                            $yy = sqlsrv_query($conn, $xx);
                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                            <li data-icon-cls="fa fa-folder"><?php echo $zz[0].' - '.$zz[1].' - '.$zz[4]; ?>
                                                <span class="pull-right"></i> 0 </span>
                                                <ul id="treeview">
                                                    <?php
                                                    $xxx = "select * from dbo.AccountView where KodeTipe = '$zz[0]' and header = 0 ORDER BY KodeAccount Asc"; 
                                                    $yyy = sqlsrv_query($conn, $xxx);
                                                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                    ?>
                                                    <li data-icon-cls="fa fa-folder"><?php echo $zzz[0].' - '.$zzz[1].' - '.$zzz[4]; ?>
                                                    <span class="pull-right"></i> 0 </span>
                                                    <ul id="ttreeview">
                                                        <?php
                                                        $xxxx = "select * from dbo.AccountView where KodeTipe = '$zzz[2]' and header = 1 ORDER BY KodeAccount Asc";
                                                        $yyyy = sqlsrv_query($conn, $xxxx);
                                                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                        ?>
                                                        <li data-icon-cls="fa fa-folder"><?php echo $zzzz[0].' - '.$zzzz[1].' - '.$zzzz[4]; ?>
                                                            <span class="pull-right"></i> 0 </span>
                                                            <ul class="treesh">
                                                                <?php
                                                                $xxxxx = "select * from dbo.AccountView where KodeTipe = '$zzzz[2]' and header =2 ORDER BY KodeAccount Asc";
                                                                $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){
                                                                ?>
                                                                <li><a href="?acc=<?php echo $zzzzz[0]; ?>&st=2"><?php echo $zzzzz[0].' - '.$zzzzz[1].' - '.$zzzzz[4]; ?></a>
                                                                    <span class="pull-right"></i> 0 </span> 
                                                                </li>  
                                                            <?php } ?>  
                                                            </ul>
                                                        </li>
                                                    <?php } ?>
                                                    </ul>
                                                    </li>
                                                <?php } ?>
                                                </ul>
                                                </li>
                                            <?php $atotal1++; } ?>        
                                            </ul>
                                        </li>

                                 <?php $atotal++; } ?>
                                </ul>
                            
                            </tbody>
                        </table>

                    </td>
                    <td colspan="2">
                        <table class="table table-bordered table-striped">
                            <tbody>
                             <ul id="treeview1">
                                    <?php
                                        $atotal = 1;
                                        $x = "select* from dbo.GroupAcc where KodeGroup in('2.0.00.00.000','3.0.00.00.000')";
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <li data-icon-cls="fa fa-folder"><?php echo $z[0].' - '.$z[1].' - '.$z[4]; ?>
                                            <span class="pull-right"></i> 0 </span>
                                            <ul id="treeview1">
                                            <?php
                                            $atotal1 = 0;
                                            $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                            //echo $xx;
                                            $yy = sqlsrv_query($conn, $xx);
                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                            <li data-icon-cls="fa fa-folder"><?php echo $zz[0].' - '.$zz[1].' - '.$zz[4]; ?>
                                                <span class="pull-right"></i> 0 </span>
                                                <ul id="treeview">
                                                    <?php
                                                    $xxx = "select * from dbo.AccountView where KodeTipe = '$zz[0]' and header = 0 ORDER BY KodeAccount Asc"; 
                                                    $yyy = sqlsrv_query($conn, $xxx);
                                                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                    ?>
                                                    <li data-icon-cls="fa fa-folder"><?php echo $zzz[0].' - '.$zzz[1].' - '.$zzz[4]; ?>
                                                    <span class="pull-right"></i> 0 </span>
                                                    <ul id="ttreeview1">
                                                        <?php
                                                        $xxxx = "select * from dbo.AccountView where KodeTipe = '$zzz[2]' and header = 1 ORDER BY KodeAccount Asc";
                                                        $yyyy = sqlsrv_query($conn, $xxxx);
                                                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                        ?>
                                                        <li data-icon-cls="fa fa-folder"><?php echo $zzzz[0].' - '.$zzzz[1].' - '.$zzzz[4]; ?>
                                                            <span class="pull-right"></i> 0 </span>
                                                            <ul class="treesh">
                                                                <?php
                                                                $xxxxx = "select * from dbo.AccountView where KodeTipe = '$zzzz[2]' and header =2 ORDER BY KodeAccount Asc";
                                                                $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){
                                                                ?>
                                                                <li><a href="?acc=<?php echo $zzzzz[0]; ?>&st=2"><?php echo $zzzzz[0].' - '.$zzzzz[1].' - '.$zzzzz[4]; ?></a>
                                                                    <span class="pull-right"></i> 0 </span> 
                                                                </li>  
                                                            <?php } ?>  
                                                            </ul>
                                                        </li>
                                                    <?php } ?>
                                                    </ul>
                                                    </li>
                                                <?php } ?>
                                                </ul>
                                                </li>
                                            <?php $atotal1++; } ?>        
                                            </ul>
                                        </li>

                                 <?php $atotal++; } ?>
                                </ul>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th><?php echo lang('Total Aktiva'); ?></th>
                    <th><span class="pull-right"><?php echo number_format($atotal); ?></span></th>
                    <th><?php echo lang('Total Pasiva'); ?></th>
                    <th><span class="pull-right"><?php echo number_format($btotal); ?></span></th>
                </tr>
            </table>
        </div>
        <?php } ?>
    </div>
</div>

 <!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview1").shieldTreeView();
    });
</script>

<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
