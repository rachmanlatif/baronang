<?php require('blank-header.php');?>

<?php
session_start();
include("connectinti.php");
?>

<div class="register-box">
    <div class="register-logo">
        <a href="" class="logo">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src = "static/images/Logo_Black.png"></span>
        </a>
    </div><!-- /.login-logo -->

    <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
        <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
            <?php echo $_SESSION['error-message']; ?>
        </div>
    <?php } ?>

    <div class="register-box-body">
        <p class="login-box-msg">Register</p>
        <form action="procregister.php" method="post" enctype="multipart/form-data">
            <div class="form-group has-feedback">
                <input type="number" id="uid" name="uid" class="form-control" placeholder="<?php echo lang('User ID Baronang App'); ?>" required="">
                <span class="fa fa-user form-control-feedback" aria-hidden="true"></span>
                <a href="https://play.google.com/store/apps/details?id=com.baronang.koperasibaronang"><?php echo lang('Cara mendapatkannya'); ?> ?</a>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="name" class="form-control" placeholder="<?php echo lang('Nama koperasi'); ?>" required="">
                <span class="fa fa-address-card-o form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="<?php echo lang('Email'); ?>" required="">
                <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="number" name="telp" class="form-control" placeholder="<?php echo lang('Telepon'); ?>" required="">
                <span class="fa fa-phone form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="address" class="form-control" placeholder="<?php echo lang('Alamat'); ?>" required="">
                <span class="fa fa-globe form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <select name="province" class="form-control select2" id="province" required="">
                    <option value="">- <?php echo lang('Pilih provinsi'); ?> -</option>
                    <?php
                    $a = "select * from [dbo].[Provinsi]";
                    $b = sqlsrv_query($conns, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group has-feedback" id="city">
                <select name="city" class="form-control select2" required="">
                    <option value="">- <?php echo lang('Pilih kota'); ?> -</option>
                </select>
            </div>
            <div class="form-group has-feedback">
                <a href="login.php"><?php echo lang('Masuk'); ?></a>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-flat btn-primary btn-block"><?php echo lang('Daftar'); ?></button>
            </div>
            <section id="qrcode">
                <div class="form-group has-feedback text-center">
                    <?php echo lang('Scan QR Code dibawah ini untuk mendownload Baronang App'); ?>

                    <img src="static/images/qr_code.jpg" class="img-responsive">
                </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('#uid').keyup(function(){
        var uid = $('#uid').val();

        if(uid.length > 16){
            alert('<?php echo lang('User ID Baronang App Harus 16 Digit'); ?>');
            return false;
        }
    });

    $('#province').change(function(){
        var province = $('#province :selected').val();

        $.ajax({
            url : "ajax_register.php",
            type : 'POST',
            data: { province: province},
            success : function(data) {
                $("#city").html(data);
            },
            error : function(){
                alert('<?php echo lang('Silahkan coba lagi'); ?>');
                return false;
            }
        });
    });
</script>

<?php require('blank-footer.php');?>