<?php
include 'connect.php';
include 'connectuser.php';

if(isset($_POST['uid']) and isset($_POST['mid'])){
    $a = "select * from [dbo].[UserPaymentGateway] where KodeUser='$_POST[uid]'";
    $b = sqlsrv_query($connuser, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);

    $aa = "select * from [dbo].[MemberList] where MemberID='$_POST[mid]'";
    $bb = sqlsrv_query($conn, $aa);
    $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);

    $aaa = "select * from [dbo].[UserMemberKoperasi] where UserID='$_POST[uid]' and KID = '$_SESSION[KID]'";
    $bbb = sqlsrv_query($connuser, $aaa);
    $ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC);
    ?>
    <?php if($c != null and $cc != null){ ?>
        <form action="proc_csoamemberlink.php" method="post">
            <input type="hidden" name="userid" value="<?php echo $_POST['uid']; ?>" readonly>
            <input type="hidden" name="memberid" value="<?php echo $_POST['mid']; ?>" readonly>

        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning ?>"></i> Info</h4>
            Pastikan data member dan data user Baronang Pay sama
        </div>

        <table class="table table-striped">
            <tr>
                <td colspan="2"><h4>User Baronang Pay</h4></td>
                <td colspan="2"><h4>Member List</h4></td>
            </tr>
            <tr>
                <th>User ID</th>
                <td><?php echo $c[0]; ?></td>
                <th>NIP</th>
                <td><?php echo $cc[8]; ?></td>
            </tr>
            <tr>
                <th>KTP</th>
                <td><?php echo $c[9]; ?></td>
                <th>KTP</th>
                <td><?php echo $cc[7]; ?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?php echo $c[1]; ?></td>
                <th>Name</th>
                <td><?php echo $cc[2]; ?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $c[2]; ?></td>
                <th>Email</th>
                <td><?php echo $cc[5]; ?></td>
            </tr>
            <tr>
                <th>Address</th>
                <td><?php echo $c[4]; ?></td>
                <th>Address</th>
                <td><?php echo $cc[3]; ?></td>
            </tr>
            <tr>
                <th>Phone</th>
                <td><?php echo $c[3]; ?></td>
                <th>Phone</th>
                <td><?php echo $cc[4]; ?></td>
            </tr>
        </table>

            <?php if($ccc != null){ ?>
                <div class="alert alert-warning alert-dismissible">
                    <h4><i class="icon fa fa-warning ?>"></i> Warning</h4>
                    User Baronang Pay is connected!
                </div>
            <?php } else { ?>
                <button type="submit" class="btn btn-success pull-right btn-connect">Connect</button>
            <?php } ?>
        <?php } ?>
    </form>
<?php
} else{
    echo '<h3>Data tidak ditemukan</h3>';
}
?>
