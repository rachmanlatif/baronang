<?php
session_start();
error_reporting(0);

include "connect.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$delete			= $_GET['delete'];

$des	= $_POST['des'];
$amo	= $_POST['amo'];
$bank	= $_POST['bank'];
$kba	= $_POST['kba'];

$a = "select [dbo].[getKodeBasicSavingType]('$_SESSION[KID]')";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
$codes = $c[0];

if(!empty($delete)){
		
	$dpultsql = "delete from [dbo].[BasicSavingType] where KodeBasicSavingType='$delete'";
	$dpulstmt = sqlsrv_query($conn, $dpultsql);
	if($dpulstmt){
        messageAlert(lang('Berhasil menghapus data dari database'),'success');
        header('Location: bas_sav_type.php');
    }
	else{
        messageAlert(lang('Gagal menghapus data dari database'),'danger');
        header('Location: bas_sav_type.php');
	}
}
else{
	if($des == "" || $amo == "" || $kba == "" || $bank == ""){
        messageAlert(lang('Harap isi seluruh kolom'),'info');
        header('Location: bas_sav_type.php');
	}
	else{
        $uasqll = "select * from [dbo].[BasicSavingType] where KodeBasicSavingType='$codes'";
		$uastmtt = sqlsrv_query($conn, $uasqll);
		$uarow = sqlsrv_fetch_array( $uastmtt, SQLSRV_FETCH_NUMERIC);

		if(!empty($edit)){
			if(count($ulrow[0] > 0)){
				$upultsqll = "update [dbo].[BasicSavingType] set Descript='$des', Ammount='$amo',  Status='$bank', KBA='$kba' where KodeBasicSavingType='$edit'";  
				$upulstmtt = sqlsrv_query($conn, $upultsqll);
				if($upulstmtt){
                    messageAlert(lang('Berhasil memperbaharui data ke database'),'success');
                    header('Location: bas_sav_type.php');
				}
				else{
                    messageAlert(lang('Gagal memperbaharui data ke database'),'danger');
                    header('Location: bas_sav_type.php');
				}
			}
			else{
                header('Location: bas_sav_type.php');
			}
		}
        else{
            $blsqll = "select * from [dbo].[BasicSavingType]  where KodeBasicSavingType='$codes'";
            $blstmtt = sqlsrv_query($conn, $blsqll);
            $blroww = sqlsrv_fetch_array( $blstmtt, SQLSRV_FETCH_NUMERIC);
            if(empty($blroww[0])){
                $spbltsql = "exec [dbo].[ProsesBasicSavingType] '$codes','$des','$amo','$bank','$kba'";
                $spblstmt = sqlsrv_query($conn, $spbltsql);
                if($spblstmt){
                    messageAlert(lang('Berhasil menyimpan ke database'),'success');
                    header('Location: bas_sav_type.php');
                }
                else{
                    messageAlert(lang('Gagal menyimpan ke database'),'danger');
                    header('Location: bas_sav_type.php');
                }
            }
            else{
                messageAlert(lang('Kode Bank sudah ada'),'warning');
                header('Location: bas_sav_type.php');
            }
        }
    }
}
?>