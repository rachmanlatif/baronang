<?php
session_start();
include("connect.php");

$tsql = "select * from [dbo].[LoginList] where UserID='$_SESSION[UserID]'";
$stmt = sqlsrv_query($conn, $tsql);
$row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC);

$oldpass	= md5($_POST['oldpass']);
$pass		= md5($_POST['pass']);
$repass		= md5($_POST['repass']);

if($oldpass=="" || $pass=="" || $repass==""){
    messageAlert('Harap isi seluruh kolom','info');
    header('Location: change_pass.php');
}
else{
	if($oldpass == $row[2]){
		if($pass == $repass){
			$usql	= "update [dbo].[LoginList] set Passwd='$repass' where UserID='$_SESSION[UserID]'";
			$ustmt = sqlsrv_query($conn, $usql);
			if($ustmt){
                messageAlert('Password berhasil diubah','success');
                header('Location: change_pass.php');
			}
			else{
                messageAlert('Gagal merubah password','gagal');
                header('Location: change_pass.php');
			}
		}
		else{
            messageAlert('Password Baru dan Konfirmasi Password berbeda','warning');
            header('Location: change_pass.php');
		}
	}
	else{
        messageAlert('Password lama tidak sesuai','warning');
        header('Location: change_pass.php');
	}
}

?>