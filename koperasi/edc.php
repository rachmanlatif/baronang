<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah EDC</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="procedc.php" method="post">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="text-align: left;">KBA</label>
                                <div class="col-sm-6">
                                    <select name="kba" class="form-control">
                                        <?php
                                        $sql = "select* from [dbo].[BankAccount]";
                                        $exec = sqlsrv_query($conn, $sql);
                                        while($row = sqlsrv_fetch_array($exec, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                            <option value="<?php echo $row[1]; ?>"><?php echo $row[3].' - '.$row[4]; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <span class="lead">Daftar EDC</span>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Serial Number</th>
                                <th>KBA</th>
                                <th>Private Key</th>
                            </tr>
                            <?php
                            $xx = "select* from [Gateway].[dbo].[EDCList] where KID = '$_SESSION[KID]'";
                            $yy = sqlsrv_query($conn, $xx);
                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                            ?>
                                <tr>
                                    <td><?php echo $zz[0]; ?></td>
                                    <td>
                                        <?php
                                        $s = "select* from [dbo].[BankAccount] where KodeKoperasiBankAccount = '$zz[2]'";
                                        $e = sqlsrv_query($conn, $s);
                                        $r = sqlsrv_fetch_array($e, SQLSRV_FETCH_NUMERIC);
                                        if($r != null){
                                            echo $r[3].' - '.$r[4];
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $zz[4]; ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('content-footer.php');?>

<?php require('footer.php');?>