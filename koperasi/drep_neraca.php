<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Neraca");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->SetCellValue('A3', 'No');
    $objWorkSheet->SetCellValue('B3', 'No Akun');
    $objWorkSheet->SetCellValue('C3', 'Nama Akun');
    $objWorkSheet->SetCellValue('D3', 'Nilai');



    if($_GET['acc']){
    $no = 1;
    $row = 4;
    $x = "exec dbo.TampilDetailReportCOA '0','$_GET[acc]'";
   
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
    //push untuk drop down list
        array_push($listCombo, $z[1]);

        $objWorkSheet->SetCellValue("A".$no,$z[1]);

        $aa = "exec dbo.TampilDetailReportCOA '1','$z[0]'";
        $bb = sqlsrv_query($conn, $aa);
            while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
            //var_dump($cc).die;
                 //push untuk drop down list
                    array_push($listCombo, $cc[1]);


            $objWorkSheet->SetCellValue("A".$row,$no);
            $objWorkSheet->SetCellValueExplicit("B".$row, $cc[0]);
            $objWorkSheet->SetCellValue("C".$row, $cc[1]);
            $objWorkSheet->SetCellValueExplicit("D".$row, number_format($cc[2]), PHPExcel_Cell_DataType::TYPE_STRING);
    
            $no++;
            $row++;
        }
    }
}
//exit;
    $objWorkSheet->setTitle('Drep Neraca');

    $fileName = 'Neraca'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
