<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Neraca History Akun");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->SetCellValue('A3', 'No');
    $objWorkSheet->SetCellValue('B3', 'Kode Jurnal');
    $objWorkSheet->SetCellValue('C3', 'Kode Akun');
    $objWorkSheet->SetCellValue('D3', 'Nama Akun');
    $objWorkSheet->SetCellValue('E3', 'Debit');
    $objWorkSheet->SetCellValue('F3', 'Kredit'); 
    $objWorkSheet->SetCellValue('G3', 'Deskripsi');    



    if($_GET['st'] == 4 ){
    $no = 1;
    $row = 4;

    $objWorkSheet->SetCellValue("A1",$_GET['acc2']);


   $aaa = "SELECT * FROM ( SELECT a.KodeJurnal,a.KodeAccount,a.Debet,a.Kredit,a.Keterangan, ROW_NUMBER() OVER (ORDER BY b.Tanggal asc) as row FROM [dbo].[JurnalDetail] a inner join [dbo].[JurnalHeader] b on a.KodeJurnal = b.KodeJurnal where a.KodeJurnal= '$_GET[acc2]' ) a ORDER BY KodeJurnal asc";
    //echo($aaa);
    $bbb = sqlsrv_query($conn, $aaa);
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        //var_dump($ccc);

        $noacc = '';
        $acc = '';
        $sql   = "select * from dbo.Account where KodeAccount='$ccc[1]'";
        //echo $sql;
        $stmt  = sqlsrv_query($conn, $sql);
        $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
        if($row != null){
            $noacc = $row[0];
            $acc = $row[1];

        }


                $no++;
                $row++;
            
    }
    }
//exit;
    $objWorkSheet->setTitle('Drep Neraca History Akun');

    $fileName = 'NeracaHISAK'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
