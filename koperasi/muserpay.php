<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
$x = "select * from [dbo].[Profil]";
$y = sqlsrv_query($conn, $x);
$z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC);
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary col-md-6">
            <div class="box-header with-border">
                <h3 class="box-title">Profile User Baronang Pay</h3>
            </div>
            <form class="form-horizontal" action="proc_mprofile.php" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Email</label>
                                <div class="col-sm-4">
                                    <input type="email" name="email" class="form-control" placeholder="" value="<?php echo $z[0]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" name="name" class="form-control" placeholder="" value="<?php echo $z[1]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Phone</label>
                                <div class="col-sm-2">
                                    <input type="text" name="phone" class="form-control" placeholder="" value="<?php echo $z[2]; ?>">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Koperasi Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="kname" class="form-control" placeholder="" value="<?php echo $z[3]; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Koperasi Nick Name</label>
                                <div class="col-sm-2">
                                    <input type="text" name="knick" class="form-control" placeholder="" value="<?php echo $z[4]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Logo</label>
                                <div class="col-sm-4">
                                    Pilih logo jika ingin mengganti logo terbaru
                                    <input type="file" name="filename" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"></label>
                                <div class="col-sm-4">
                                    <img src="<?php echo $z[8]; ?>" width="220" height="150">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Koperasi Phone</label>
                                <div class="col-sm-6">
                                    <input type="text" name="kphone" class="form-control" placeholder="" value="<?php echo $z[9]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Address</label>
                                <div class="col-sm-6">
                                    <textarea name="address" class="form-control"><?php echo $z[5]; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Province</label>
                                <div class="col-sm-2">
                                    <input type="text" name="province" class="form-control" placeholder=""  value="<?php echo $z[6]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">City</label>
                                <div class="col-sm-2">
                                    <input type="text" name="city" class="form-control" placeholder="" value="<?php echo $z[7]; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>