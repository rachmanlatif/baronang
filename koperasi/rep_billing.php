<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Tagihan Bulanan'); ?></h3>
    </div>
    <div class="box-body">
        <a href="drep_billing.php"><button type="button" class="btn btn-primary">Download to excel</button></a>
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Member ID</th>
                    <th><?php echo lang('Nama'); ?></th>
                    <th><?php echo lang('Hutang Simpanan Pokok'); ?></th>
                    <th><?php echo lang('Hutang Simpanan Wajib'); ?></th>
                    <th><?php echo lang('Hutang Simpanan Sukarela'); ?></th>
                    <th><?php echo lang('Pokok Pinjaman'); ?></th>
                    <th><?php echo lang('Bunga Pinjaman'); ?></th>
                    <th><?php echo lang('Belanja Toko'); ?></th>
                    <th><?php echo lang('Toko'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.TagihanBulanan";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[TagihanBulanan]) a WHERE row between '$posisi' and '$batas'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                ?>
                    <tr>
                        <td><?php echo $ulrow[12]; ?></td>
                        <td><?php echo $ulrow[0]; ?></td>
                        <td><?php echo $ulrow[1]; ?></td>
                        <td><?php echo number_format($ulrow[3]); ?></td>
                        <td><?php echo number_format($ulrow[5]); ?></td>
                        <td><?php echo number_format($ulrow[7]); ?></td>
                        <td><?php echo number_format($ulrow[8]); ?></td>
                        <td><?php echo number_format($ulrow[9]); ?></td>
                        <td><?php echo number_format($ulrow[10]); ?></td>
                        <td><?php echo number_format($ulrow[11]); ?></td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div>

            <div class="box-footer clearfix pull-right">
                <div><label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?></label></div>
                    <?php
                    $reload = "rep_billing.php?";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages, $adjacents)."</div>";
            
            /*
            <div class="box-footer clearfix">
                <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
                <?=$posisi." - ".$batas?>
                <ul class="pagination pagination-sm no-margin pull-right">
                    <li class="paginate_button"><a href="rep_billing.php">&laquo;</a></li>
                    <?php
                    for($ul = 1; $ul <= $jmlhalaman; $ul++){
                        if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                        echo '<li class="paginate_button '.$ulpageactive.'"><a href="rep_billing.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                    }
                    ?>
                    <li class="paginate_button"><a href="rep_billing.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
                </ul>
                */
                ?>
            </div>
        </div>
    </div>
</div>

<?php
function paginate($reload, $page, $tpages, $adjacents) {

    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";

    $out = "<nav><ul class=\"pagination\">\n";

    // previous
    if($page==1) {
        $out.= "<li class=\"disabled\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // first
    if($page>($adjacents+1)) {
        $out.= "<li><a href=\"" . $reload . "\">1</a></li>\n";
    }

    // interval
    if($page>($adjacents+2)) {
        $out.= "<li class=\"disabled\"><a href=\"#\">...</a></li>\n";
    }

    // pages
    $pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
    $pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
    for($i=$pmin; $i<=$pmax; $i++) {
        if($i==$page) {
            //$out.= "<li class=\"active\"><a href=\"#\">" . $i . "</a></li>\n";
            $out.= "<li class=\"active\"><span class=\"current\">Page " . $page . " of " . $tpages ."</span></li>";
        }
        elseif($i==1) {
            $out.= "<li><a href=\"" . $reload . "\">" . $i . "</a></li>\n";
        }
        else {
            $out.= "<li><a href=\"" . $reload . "&amp;page=" . $i . "\">" . $i . "</a></li>\n";
        }
    }

    // interval
    if($page<($tpages-$adjacents-1)) {
        $out.= "<li class=\"disabled\"><a href=\"#\">...</a></li>\n";
    }

    // last
    if($page<($tpages-$adjacents)) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $tpages . "</a></li>\n";
    }

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabled\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    $out.= "</ul></nav>";

    return $out;
}
?>
<?php require('content-footer.php');?>

<?php require('footer.php');?>
