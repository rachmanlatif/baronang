<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
include "connectinti.php";

//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ulprocedit = "";
$uldisabled = "";
$uldisabled2 = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.BankAccount where KodeKoperasiBankAccount='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = $eulrow[4];
        $ul5    = substr($eulrow[5],0,-5);
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul1;
        $uldisabled = "readonly";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='bank_acc.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='bank_acc.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Akun bank'); ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" action="procbank_acc.php<?=$ulprocedit?>" method = "POST">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="AccountName" class="col-sm-2 control-label"><?php echo lang('Nama bank'); ?></label>
                        <div class="col-sm-3">
                            <select name="kode" class="form-control select2">
                                <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                                <?php
                                $julsql   = "select * from [dbo].[BankList] order by NamaBank";
                                $julstmt = sqlsrv_query($conns, $julsql);
                                while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    $selected = '';
                                    if($rjulrow[0] == $ul2){
                                        $selected = 'selected';
                                    }
                                    ?>
                                    <option value="<?=$rjulrow[0];?>" <?php echo $selected; ?>><?=$rjulrow[1];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cooperativebankaccount" class="col-sm-2 control-label"><?php echo lang('Nomor akun'); ?></label>
                        <div class="col-sm-3">
                            <input type="number" name="acc" class="form-control" id="cooperativebankaccount" placeholder="" value="<?=$ul3;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cooperativebankaccount" class="col-sm-2 control-label"><?php echo lang('Nama Akun'); ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="accn" class="form-control" id="cooperativebankaccount" placeholder="" value="<?=$ul4;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cooperativebankaccount" class="col-sm-2 control-label"><?php echo lang('Saldo'); ?></label>
                        <div class="col-sm-3">
                            <input type="text" name="bal" class="form-control price" id="cooperativebankaccount" placeholder="" value="<?=$ul5;?>" <?=$uldisabled2?>>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-4">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <button type="submit" class="btn btn-flat btn-block btn-success pull-left"><?php echo lang('Perbaharui'); ?></button>
                    <?php }else{ ?>
                        <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                    <?php } ?>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <a href="bank_acc.php" class="btn btn-flat btn-block btn-default"><?php echo lang('Batal'); ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- /.box-body -->
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Daftar akun bank'); ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo lang('No'); ?></th>
                    <th><?php echo lang('Kode akun bank'); ?></th>
                    <th><?php echo lang('Nama bank'); ?></th>
                    <th><?php echo lang('Nomor akun'); ?></th>
                    <th><?php echo lang('Nama akun'); ?></th>
                    <th><?php echo lang('Saldo'); ?></th>
                    <th><?php echo lang('Aksi'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.BankAccountView";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KID asc) as row FROM [dbo].[BankAccountView]) a WHERE KID='$_SESSION[KID]' and row between '$posisi' and '$batas'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    ?>
                    <tr>
                        <td><?=$ulrow[7];?></td>
                        <td><?=$ulrow[1];?></td>
                        <td><?=$ulrow[2];?></td>
                        <td><?=$ulrow[4];?></td>
                        <td><?=$ulrow[5];?></td>
                        <td><?=number_format($ulrow[6]);?></td>
                        <td width="20%" style="padding: 3px">
                            <div class="btn-group" style="padding-right: 15px">
                                <a href="bank_acc.php?page=<?=$halaman?>&edit=<?=$ulrow[1]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
        <div class="box-footer clearfix">
            <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="bank_acc.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="bank_acc.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="bank_acc.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
