<?php include "connect.php"; ?>

<style type="text/css">
    @charset "utf-8";
/* CSS Document */

table, th, td {
    border-collapse:collapse;
    border:1px solid #999;
    font-family:Tahoma, Geneva, sans-serif;
    font-size:12px;
}

.head {
    background: rgb(206,220,231); /* Old browsers */
    background: -moz-linear-gradient(top,  rgba(206,220,231,1) 0%, rgba(89,106,114,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(206,220,231,1)), color-stop(100%,rgba(89,106,114,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cedce7', endColorstr='#596a72',GradientType=0 ); /* IE6-9 */
}

.head th {
    padding:10px;
    color:#333;
    text-shadow:1px 1px 0px #CCC;
    font-size:14px;
}

.satu {
    background-color:#CECECE;
}

.dua {
    background-color:#E0E0E0;
}

.satu:hover, .dua:hover {
    background-color:#BADFFE;
    font-weight:bold;
    cursor:pointer;
}
</style>

<div>
    <button onclick="window.print()">Print</button>
</div>
<table class="table table-bordered" width="100%">
                    <?php
                    $a = "exec dbo.TampilDetailReportCOA '0','$_GET[acc]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                    ?>
                        <tr class="head">
                            <th><?php echo $c[1]; ?></th>
                            <th></th>
                            <th><?php echo 'No Akun'; ?></th>
                            <th><?php echo 'Nama Akun'; ?></th>
                            <th><?php echo 'Nilai'; ?></th>
                            <th colspan="4"></th>
                        </tr>
                        <?php
                        $aa = "exec dbo.TampilDetailReportCOA '1','$c[0]'";
                        $bb = sqlsrv_query($conn, $aa);
                        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr class="satu">
                            <td></td>
                            <td><td><a href="?acc1=<?php echo $cc[0]; ?>&st=3"><?php echo $cc[0]; ?></a></td>
                            <td><?php echo $cc[1]; ?></td>
                            <td align="right"><?php echo number_format($cc[2]); ?></td>
                            <td colspan="4"></td>
                        </tr>
                            <!--<?php
                            $aaa = "exec dbo.TampilDetailReportCOA '2','$cc[0]'";
                            $bbb = sqlsrv_query($conn, $aaa);
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                            ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[1]; ?></td>
                                    <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                    <td>
                                        <?php
                                        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                        $bbbb = sqlsrv_query($conn, $aaaa);
                                        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                        if($cccc != null){
                                            echo $cccc[1];
                                        }
                                        else{
                                            echo $ccc[3];
                                        }
                                        ?>
                                    </td>
                                    <td align="right"><?php echo number_format($ccc[6]); ?></td>
                                    <td align="right"><?php echo number_format($ccc[7]); ?></td>
                                </tr>
                            <?php } ?>-->
                        <?php } ?>
                    <?php } ?>
                </table>