<?php require('blank-header.php');?>

  <?php
 // session_start();

 //if(!empty($_SESSION['KID']) && !empty($_SESSION['NamaKoperasi']) && !empty($_SESSION['Server']) && !empty($_SESSION['DatabaseName']) && !empty($_SESSION['UserDB']) && !empty($_SESSION['PassDB']) &&  !empty($_SESSION['UserID']) && !empty($_SESSION['Name']) && !empty($_SESSION['KodeJabatan'])){
   // unset($_SESSION['KID']);
 //  unset($_SESSION['NamaKoperasi']);
  //  unset($_SESSION['Server']);
  //  unset($_SESSION['DatabaseName']);
  //  unset($_SESSION['UserDB']);
  //  unset($_SESSION['PassDB']);
  //  unset($_SESSION['Status']);
  //  unset($_SESSION['UserID']);
 //   unset($_SESSION['Name']);
  //  unset($_SESSION['KodeJabatan']);
  //  exit;
 // }
  ?>   

    <div class="login-box">
      <div class="login-logo">
        <a href="" class="logo">
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src = "static/images/Logo_Black.png"></img></span>
        </a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="cek_loginuser.php" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="name" class="form-control" placeholder="Name">
            <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password">
            <span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-flat btn-primary btn-block">Sign In</button>
          </div>
        </form>

        <a href="#">I forgot my password</a><br>
		<a href="register.php">Register</a><br>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

<?php require('blank-footer.php');?>   
