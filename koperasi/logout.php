<?php 
	session_start();
    session_destroy();
	unset($_SESSION['KID']);
	unset($_SESSION['NamaKoperasi']);
	unset($_SESSION['Server']);
	unset($_SESSION['DatabaseName']);
	unset($_SESSION['UserDB']);
	unset($_SESSION['PassDB']);
	unset($_SESSION['Status']);
	unset($_SESSION['UserID']);
	unset($_SESSION['Name']);
	unset($_SESSION['KodeJabatan']);
	echo "<script language='javascript'>document.location='login.php';</script>";
	exit;  
?>