<?php require('header.php');?>
<?php require('sidebar-left.php');?>
<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ul6    = "";
$ul7    = "";
$ulprocedit = "";
$uldisabled = "";
$uldisabled2 = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.EmoneyType where KodeemoneyType='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = $eulrow[4];
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='EmoneyType.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='EmoneyType.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Tipe e-money'); ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" action="proc_type_emoney.php<?=$ulprocedit?>" method = "POST">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Deskripsi'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="dsc" class="form-control" id="regularsavingdescription" placeholder="" value="<?=$ul1?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Max Limit'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="inter" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul2?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Fee Topup'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="cp" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul3?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Price'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="ma" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul4?>">
                        </div>
                    </div>
                </div>
                    </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-4">
                        <?php if(count($eulrow[0]) > 0){ ?>
                            <button type="submit" class="btn btn-flat btn-block btn-success pull-left"><?php echo lang('Perbaharui'); ?></button>
                        <?php } else { ?>
                            <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                        <?php } ?>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4">
                        <?php if(count($eulrow[0]) > 0){ ?>
                            <a href="emoneytype.php" class="btn btn-flat btn-block btn-default">Batal</a>
                        <?php } ?>
                    </div>
                </div>
				</div>
    </form>
	</div>
	<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Daftar tipe e-money'); ?></h3>
    </div>
    <div class="box-body">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo lang('No'); ?></th>
                    <th><?php echo lang('Kode tipe e-money'); ?></th>
                    <th><?php echo lang('Deskripsi'); ?></th>
                    <th><?php echo lang('Max Limit'); ?></th>
                    <th><?php echo lang('Fee Top Up'); ?></th>
                    <th><?php echo lang('Price e-money'); ?></th>
                    <th><?php echo lang('Aksi'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.EmoneyType";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeemoneyType asc) as row FROM [dbo].[EmoneyType]) a WHERE row between '$posisi' and '$batas'";

                //$ulsql = "select * from [dbo].[TimeDepositType]";

                $ulstmt = sqlsrv_query($conn, $ulsql);

                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    ?>
                    <tr>
                        <td><?=$ulrow[6];?></td>
                        <td><?=$ulrow[0];?>
                        <td><?=$ulrow[1];?></td>
                        <td><?=number_format($ulrow[2],0,',','.');?></td>
                        <td><?=number_format($ulrow[3],0,',','.');?></td>
                        <td><?=number_format($ulrow[4],0,',','.');?></td>
                        <td width="20%" style="padding: 3px">
                            <div class="btn-group" style="padding-right: 15px">
                                <a href="emoneytype.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="emoneytype.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="emoneytype.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="emoneytype.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>
<?php require('content-footer.php');?>

<?php require('footer.php');?>
