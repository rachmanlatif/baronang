<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Jurnal'); ?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <form action="" method="GET" name="filtering1">
                <div class="form-group">
                    <label class="col-sm-1 control-label" style="text-align: left;"><?php echo lang('Dari'); ?></label>
                    <div class="col-sm-2">
                        <input type="text" name="from" id="from" value="<?php echo $_GET['from']; ?>" class="form-control datepicker">
                    </div>
                    <label class="col-sm-1 control-label" style="text-align: left;"><?php echo lang('Sampai'); ?></label>
                    <div class="col-sm-2">
                        <input type="text" name="to" id="to" value="<?php echo $_GET['to']; ?>" class="form-control datepicker">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('Pilih'); ?></button>
                        <a href="rep_jurnal.php"><button type="button" class="btn btn-sm btn-primary">Refresh</button></a>
                    </div>
                    <div class="col-sm-12">

                    </div>
                    <div class="col-sm-10">
                        <a href="drep_jurnal.php?from=<?php echo $_GET['from']; ?>&to=<?php echo $_GET['to']; ?>"><button type="button" class="btn btn-primary">Download to excel</button></a>
                    </div>
                    <div class="col-sm-12">

                    </div>

                </div>
            </form>
        </div>



        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th><?php echo lang('Kode Jurnal'); ?></th>
                    <th><?php echo lang('Kode Akun'); ?></th>
                    <th><?php echo lang('Nama Akun'); ?></th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th><?php echo lang('Deskripsi'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //tanggal
                $tglawal    = $_GET['from'];
                $tglakhir   = $_GET['to'];
                //count
                $jmlulsql   = "select count(*) from dbo.JurnalDetail a inner join dbo.JurnalHeader b on a.KodeJurnal = b.KodeJurnal where a.KodeJurnal is not null and b.Tanggal between '$tglawal' and '$tglakhir'";
                //var_dump($jmlulsql).die;
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];

                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT a.KodeJurnal,a.KodeAccount,a.Debet,a.Kredit,a.Keterangan, ROW_NUMBER() OVER (ORDER BY b.Tanggal asc) as row FROM [dbo].[JurnalDetail] a inner join [dbo].[JurnalHeader] b on a.KodeJurnal = b.KodeJurnal where a.KodeJurnal is not null and b.Tanggal between '$tglawal' and '$tglakhir') a WHERE row between '$posisi' and '$batas' ORDER BY KodeJurnal asc";
                echo $ulsql;
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    $noacc = '';
                    $acc = '';
                    $sql   = "select * from dbo.Account where KodeAccount='$ulrow[1]'";
                    $stmt  = sqlsrv_query($conn, $sql);
                    $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                    if($row != null){
                        $noacc = $row[0];
                        $acc = $row[1];
                    }
                    ?>
                    <tr>
                        <td><?php echo $ulrow[5]; ?></td>
                        <td><?php echo $ulrow[0]; ?></td>
                        <td><?php echo $noacc; ?></td>
                        <td><?php echo $acc; ?></td>
                        <td><?php echo number_format($ulrow[2]); ?></td>
                        <td><?php echo number_format($ulrow[3]); ?></td>
                        <td><?php echo $ulrow[4]; ?></td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div>
            <div class="box-footer clearfix pull-right">
                <div style="text-align: center;"><label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?></label></div>
                    <?php
                    $tglaw = $tglawal;
                    $tglak   = $tglakhir;
                    $reload = "rep_jurnal.php?from=$tglaw&to=$tglak";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages, $adjacents)."</div>";

                    /*
                    <ul class="pagination pagination-sm no-margin pull-right">
                    <li class="paginate_button"><a href="rep_jurnal.php">&laquo;</a></li>
                    <?php
                    for($ul = 1; $ul <= $jmlhalaman; $ul++){
                        if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                        echo '<li class="paginate_button '.$ulpageactive.'"><a href="rep_jurnal.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                    }
                    ?>
                    <li class="paginate_button"><a href="rep_jurnal.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
                </ul>
                */
                ?>
            </div>
        </div>
    </div>
</div>

<?php
function paginate($reload, $page, $tpages, $adjacents) {

    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";

    $out = "<nav><ul class=\"pagination\">\n";

    // previous
    if($page==1) {
        $out.= "<li class=\"disabled\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // first
    if($page>($adjacents+1)) {
        $out.= "<li><a href=\"" . $reload . "\">1</a></li>\n";
    }

    // interval
    if($page>($adjacents+2)) {
        $out.= "<li class=\"disabled\"><a href=\"#\">...</a></li>\n";
    }

    // pages
    $pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
    $pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
    for($i=$pmin; $i<=$pmax; $i++) {
        if($i==$page) {
            //$out.= "<li class=\"active\"><a href=\"#\">" . $i . "</a></li>\n";
            $out.= "<li class=\"active\"><span class=\"current\">Page " . $page . " of " . $tpages ."</span></li>";
        }
        elseif($i==1) {
            $out.= "<li><a href=\"" . $reload . "&amp;page=" . $i . "\">" . $i . "</a></li>\n";
        }
        else {
            $out.= "<li><a href=\"" . $reload . "&amp;page=" . $i . "\">" . $i . "</a></li>\n";
        }
    }

    // interval
    if($page<($tpages-$adjacents-1)) {
        $out.= "<li class=\"disabled\"><a href=\"#\">...</a></li>\n";
    }

    // last
    if($page<($tpages-$adjacents)) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $tpages . "</a></li>\n";
    }

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabled\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    $out.= "</ul></nav>";

    return $out;
}
?>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
