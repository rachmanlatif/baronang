<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

    <script language="javascript">
        function load_page(acc){
            var mid = $('#csoaregsavmember').val();

            window.location.href = "dep_time_dep.php?mid=" + mid + "&acc=" + acc;
        }
    </script>

<?php
$mid = $_GET['mid'];
$acc = $_GET['acc'];
$dis = '';
$ul0 = '';
$ul1 = '';
$ul2 = '';
$ul3 = '';
$ul4 = '';
if(isset($mid) and isset($acc)){
    $a = "select * from [dbo].[MemberList] where MemberID='$mid' and StatusMember = 1";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $aa = "select * from [dbo].[TimeDepositAccount] where MemberID='$mid' and AccountNumber='$acc' and Status = 2";
        $bb = sqlsrv_query($conn, $aa);
        $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
        if($cc != null){
            $ul0 = $c[1];
            $ul1 = $c[2];
            $ul2 = $cc[1];

            $aaa = "select * from [dbo].[TimeDepositType] where KodeTimeDepositType='$cc[2]'";
            $bbb = sqlsrv_query($conn, $aaa);
            $ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC);

            $ul3 = $ccc[1];
            $ul4 = $cc[7];
        }
        else{
            $dis = 'disabled';

            $_SESSION['error-message'] = 'Account number tidak ditemukan';
            $_SESSION['error-type'] = 'warning';
            $_SESSION['error-time'] = time()+5;
        }
    }
    else{
        $dis = 'disabled';

        $_SESSION['error-message'] = 'Member ID tidak aktif';
        $_SESSION['error-type'] = 'warning';
        $_SESSION['error-time'] = time()+5;
    }
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Time Deposit</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="procdep_time_dep.php" method = "POST">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="csoaregsavmember" class="col-sm-2 control-label" style="text-align: left;">Member ID</label>
                                <div class="col-sm-6">
                                    <input type="text" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?=$ul0?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regularsavingdescription" class="col-sm-2 control-label" style="text-align: left;">Account Number</label>
                                <div class="col-sm-6">
                                    <input type="text" name="acc" class="form-control" id="" placeholder="" onblur="load_page(this.value);" value="<?=$ul2?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="nama" class="col-sm-2 control-label" style="text-align: left;">Name</label>
                                <div class="col-sm-6">
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?=$ul1?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regularsavingdescription" class="col-sm-2 control-label" style="text-align: left;">Time Deposit</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" placeholder="" disabled value="<?=$ul3?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regularsavingdescription" class="col-sm-2 control-label" style="text-align: left;">Amount</label>
                                <div class="col-sm-6">
                                    <input type="text"  name="amount" class="form-control price" id="amount" placeholder="" readonly value="<?=$ul4?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info" <?php echo $dis; ?>>Save</button>
                </div>
            </div>
        </form>
    </div>

<?php  require('content-footer.php');?>

<?php  require('footer.php');?>