<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

    <div class="row">
    <div class="col-md-5">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo lang('Pengguna'); ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="proc_emoneylink.php" method="POST">
                <div class="box-body">
                    <div class="form-group">
                        <label for="UserName" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Akun e-Money'); ?></label>
                        <div class="col-sm-8">
                            <input type="number" name="acc" class="form-control" id="result-code" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="UserName" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Nomor Kartu'); ?></label>
                        <div class="col-sm-8">
                            <input type="number" name="card" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" id="ocam" class="btn btn-info"><i class="fa fa-camera"></i> Scan QR Code</button>
                            </div>
                            <div class="col-sm-4">
                                <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </form>
        </div>
    </div>
    <div class="col-md-7">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Daftar master kartu'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class=""><?php echo lang('No'); ?></th>
                        <th class=""><?php echo lang('Nomor Kartu'); ?></th>
                        <th class=""><?php echo lang('Image'); ?></th>
                        <th class=""><?php echo lang('Barcode'); ?></th>
                        <th class=""><?php echo lang('Logo'); ?></th>
                        <th class=""><?php echo lang('Status'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from [PaymentGateway].[dbo].[MasterCard]";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY CardNo asc) as row FROM [PaymentGateway].[dbo].[MasterCard]) a WHERE row between '$posisi' and '$batas'";
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><?=$ulrow[6];?></td>
                            <td><?=$ulrow[1];?></td>
                            <td><?=$ulrow[2];?></td>
                            <td><?=$ulrow[2];?></td>
                            <td><?=$ulrow[4];?></td>
                            <td>
                                <?php
                                if($ulrow[5] == 1){
                                    echo 'Aktif';
                                }
                                else{
                                    echo 'Tidak Aktif';
                                }
                                ?>
                            </td>
                        </tr>
                    <?php
                    }

                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
                <?=$posisi." - ".$batas?>
                <ul class="pagination pagination-sm no-margin pull-right">
                    <li class="paginate_button"><a href="cscachangecard.php">&laquo;</a></li>
                    <?php
                    for($ul = 1; $ul <= $jmlhalaman; $ul++){
                        if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                        echo '<li class="paginate_button '.$ulpageactive.'"><a href="cscachangecard.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                    }
                    ?>
                    <li class="paginate_button"><a href="cscachangecard.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="box hide" id="form-camera">
            <div class="box-header">
                <h3 class="box-title">Scan QR Code/Barcode</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2">Pilih kamera</label>
                    <div class="col-sm-7">
                        <select class="form-control" id="camera-select"></select>
                    </div>
                    <div class="col-sm-3">
                        <button title="Decode Image" class="btn btn-default" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                        <button title="Image shoot" class="btn btn-info disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                        <button title="Start Scan QR Barcode" class="btn btn-success" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-camera"></span></button>
                        <button title="Pause" class="btn btn-warning" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                        <button title="Stop streams" class="btn btn-danger" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="well" style="position: relative;display: inline-block;">
                            <canvas id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="thumbnail" id="result">
                            <img width="320" height="240" id="scanned-img" src="">
                            <div class="caption">
                                <p class="text-center" id="scanned-QR"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script type="text/javascript">
        $('#ocam').click(function(){
            $('#result-code').val();
            $('#form-camera').removeClass('hide');
        });
    </script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
