<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0        = "";
$ul1        = "";
$ulprocedit = "";
$uldisabled = "";
if(!empty($uledit)){
    $eulsql       = "select * from dbo.UserLevel where KodeUserLevel='$uledit'";
    $eulstmt      = sqlsrv_query($conn, $eulsql);
    $eulrow       = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0        = $eulrow[0];
        $ul1        = $eulrow[1];

        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "readonly";

    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='muserlevel.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='muserlevel.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

    <div class="row">
    <div class="col-md-12">
        <div class="box box-primary col-md-6">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo lang('Level Pengguna'); ?></h3>
            </div>
            <form class="form-horizontal" action="proc_muserlevel.php<?=$ulprocedit?>" method="POST">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="levelid" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Kode Level'); ?></label>
                                <div class="col-sm-8">
                                    <input type="text" name="id" class="form-control" id="levelid" placeholder="" value="<?=$ul0?>" <?=$uldisabled?>>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="levelname" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Nama Level'); ?></label>
                                <div class="col-sm-8">
                                    <input type="text" name="levelname" class="form-control" id="levelname" placeholder="" value="<?=$ul1?>">
                                </div>
                            </div>

                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <?php
                                    $a = "select * from dbo.MenuHeader where Status=1 order by HeaderNo asc";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        $active = '';
                                        if($c[0] == 1){
                                            $active = 'active';
                                        }
                                        ?>
                                        <li class="<?php echo $active; ?>">
                                            <a href="#<?php echo $c[0]; ?>" data-toggle="tab"><label><?php echo lang($c[1]); ?></label></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php
                                    $a = "select * from dbo.MenuHeader where Status=1 order by HeaderNo asc";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        $active = '';
                                        if($c[0] == 1){
                                            $active = 'active';
                                        }
                                        ?>
                                        <div class="tab-pane <?php echo $active; ?>" id="<?php echo lang($c[0]); ?>">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <?php
                                                            $aa = "select * from dbo.MenuDetail where HeaderNo='$c[0]' and Status=1 order by DetailNo asc";
                                                            $bb = sqlsrv_query($conn, $aa);
                                                            while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
                                                            ?>
                                                            <?php if($cc[5] == 0){ ?>
                                                                <?php
                                                                if(!empty($uledit)){
                                                                    $x = "select * from dbo.UserLevelMenu where KodeUserLevel='$uledit' and MenuName='$cc[3]'";
                                                                    $y = sqlsrv_query($conn, $x);
                                                                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                                                                    $le = '';
                                                                    $ap = '';
                                                                    if($z != NULL){
                                                                        $le = 'checked';

                                                                        if($z[1] == 1){
                                                                            $ap = 'checked';
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                                <td>
                                                                    <div class="btn btn-default btn-xs" title="Need Approve SPV ?">
                                                                        <input type="checkbox" name="level[]" class="minimal" value="1" <?php echo $ap; ?>> <i class="fa fa-user"></i>
                                                                    </div>
                                                                    <input type="checkbox" name="menu[]" class="minimal" value="<?php echo $cc[3]; ?>" <?php echo $le; ?>> <?php echo lang($cc[2]); ?>
                                                                </td>
                                                            <?php } else { ?>
                                                                <tr>
                                                                    <td><label><?php echo lang($cc[2]); ?></label></td>
                                                                    <td>
                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <?php
                                                                                $aaa = "select * from dbo.MenuChild where HeaderNo='$c[0]' and DetailNo='$cc[1]' and Status=1 order by ChildNo asc";
                                                                                $bbb = sqlsrv_query($conn, $aaa);
                                                                                while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                                                                                    ?>
                                                                                    <?php
                                                                                    if(!empty($uledit)){
                                                                                        $xx = "select * from dbo.UserLevelMenu where KodeUserLevel='$uledit' and MenuName='$ccc[4]'";
                                                                                        $yy = sqlsrv_query($conn, $xx);
                                                                                        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                                                                                        $lle = '';
                                                                                        $aap = '';
                                                                                        if($zz != NULL){
                                                                                            $lle = 'checked';

                                                                                            if($zz[1] == 1){
                                                                                                $aap = 'checked';
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <td>
                                                                                        <div class="btn btn-default btn-xs" title="Need Approve SPV ?">
                                                                                            <input type="checkbox" name="level[]" class="minimal" value="1" <?php echo $aap; ?>> <i class="fa fa-user"></i>
                                                                                        </div>
                                                                                        <input type="checkbox" name="menu[]" class="minimal" value="<?php echo $ccc[4]; ?>" <?php echo $lle; ?>> <?php echo lang($ccc[3]); ?>
                                                                                    </td>
                                                                                <?php } ?>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php if(count($eulrow[0]) > 0){ ?>
                                <a href="muserlevel.php" class="btn btn-flat btn-default"><?php echo lang('Batal'); ?></a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-4">
                            <?php if(count($eulrow[0]) > 0){ ?>
                                <button type="submit" class="btn btn-flat btn-block btn-success pull-right"><?php echo lang('Perbaharui'); ?></button>
                            <?php }else{ ?>
                                <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Data Pengguna'); ?></h3>
            </div>
            <div class="box-body pad table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class=""><?php echo lang('No'); ?></th>
                        <th class="-"><?php echo lang('Kode Level'); ?></th>
                        <th class=""><?php echo lang('Nama Level'); ?></th>
                        <th class=""><?php echo lang('Aksi'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from dbo.UserLevel";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeUserLevel asc) as row FROM [dbo].[UserLevel]) a WHERE row between '$posisi' and '$batas'";
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><?=$ulrow[2];?></td>
                            <td><?=$ulrow[0];?></td>
                            <td><?=$ulrow[1];?></td>
                            <td width="20%" style="padding: 3px">
                                <div class="btn-group" style="padding-right: 15px">
                                    <a href="muserlevel.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="#" class="btn btn-default btn-flat btn-sm btn-detail<?php echo $ulrow[0]; ?>" title="show detail"><i class="fa fa-arrows-v"></i> </a>
                                    <a href="#" class="hide btn btn-default btn-flat btn-sm btn-close<?php echo $ulrow[0]; ?>" title="close detail"><i class="fa fa-arrows"></i> </a>
                                </div>
                            </td>
                        </tr>
                        <tr class="t<?php echo $ulrow[0]; ?> hide">
                            <td colspan="4" class="d<?php echo $ulrow[0]; ?>">

                            </td>
                        </tr>
                        <script type="text/javascript">
                            $('.btn-detail<?php echo $ulrow[0]; ?>').click(function(){
                                $('.t<?php echo $ulrow[0]; ?>').removeClass('hide');
                                $.ajax({
                                    url : "ajax_userlevel.php",
                                    type : 'POST',
                                    data: { param: '<?php echo $ulrow[0]; ?>' },
                                    success : function(data) {
                                        $(".d<?php echo $ulrow[0]; ?>").html(data);
                                    },
                                    error : function(){
                                        alert('<?php echo lang('Silahkan coba lagi'); ?>';
                                        return false;
                                    }
                                });

                                $('.btn-close<?php echo $ulrow[0]; ?>').removeClass('hide');
                                $('.btn-detail<?php echo $ulrow[0]; ?>').addClass('hide');
                            });

                            $('.btn-close<?php echo $ulrow[0]; ?>').click(function(){
                                $('.t<?php echo $ulrow[0]; ?>').addClass('hide');
                                $(".t<?php echo $ulrow[0]; ?>").html('');
                                $('.btn-close<?php echo $ulrow[0]; ?>').addClass('hide');
                                $('.btn-detail<?php echo $ulrow[0]; ?>').removeClass('hide');
                            });
                        </script>
                    <?php
                    }

                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
                <?=$posisi." - ".$batas?>
                <ul class="pagination pagination-sm no-margin pull-right">
                    <li class="paginate_button"><a href="muserlevel.php">&laquo;</a></li>
                    <?php
                    for($ul = 1; $ul <= $jmlhalaman; $ul++){
                        if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                        echo '<li class="paginate_button '.$ulpageactive.'"><a href="muserlevel.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                    }
                    ?>
                    <li class="paginate_button"><a href="muserlevel.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>
    </div>
<?php require('content-footer.php');?>

<?php require('footer.php');?>