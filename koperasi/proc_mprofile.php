<?php
session_start();
error_reporting(0);

include "connect.php";
include "connectinti.php";

$email = $_POST['email'];
$kname = $_POST['kname'];
$knick = $_POST['knick'];
$kphone = $_POST['kphone'];
$address = $_POST['address'];
$province = $_POST['province'];
$city = $_POST['city'];

$design = $_POST['design'];
$color = $_POST['color'];

if($email == "" || $kname == "" || $knick == "" || $kphone == "" || $address == "" || $province == "" || $city == ""){
    messageAlert(lang('Harap isi seluruh kolom'),'info');
    header('Location: mprofile.php');
}
else{
    $uploads_dir = 'uploads/';
    foreach ($_FILES["filename"]["error"] as $key => $error) {
        $doc = $_POST['jenisdoc'][$key];
        if($_FILES['filename']['name'][$key] != ''){
            $tmp_name = $_FILES["filename"]["tmp_name"][$key];
            $path = $uploads_dir . basename($_FILES["filename"]["name"][$key]);
            $extension = $_FILES["filename"]["type"][$key];
            $allowed_extensions = array('image/png', 'image/jpg', 'image/jpeg', 'application/pdf');
            if(!in_array($extension, $allowed_extensions)) {
                messageAlert(lang('Gagal unggah file, format file tidak benar'),'danger');
                header('Location: mprofile.php');
            }
            else{
                if(move_uploaded_file($tmp_name, $path)){
                    $sql = "exec dbo.ProsesUserRegisterDoc '$_SESSION[UserID]','$doc','$path'";
                    sqlsrv_query($conns, $sql);
                }
            }
        }
    }

    if($_FILES['path']['name'] != ''){
        foreach ($_FILES["path"] as $design) {
            $tmp_name = $_FILES["path"]["tmp_name"];
            $filename = basename($_FILES['path']['name']);
            $file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));
            //convert only to png
            $path = 'card/'.$_SESSION['KID'].'_card.png';

            $extension = $_FILES["path"]["type"];
            $allowed_extensions = array('image/png', 'image/jpg', 'image/jpeg');
            if(!in_array($extension, $allowed_extensions)) {
                messageAlert(lang('Gagal unggah design kartu member, format file tidak benar. Format gambar hanya berlaku PNG/JPG'),'danger');
                header('Location: mprofile.php');
            }
            else{
                if(move_uploaded_file($tmp_name, $path)){
                    messageAlert(lang('Berhasil upload design kartu member'),'success');
                    header('Location: mprofile.php');
                }
            }

            $design = $path;
        }
    }

    $path = '';
    $x = "select top 1 * from [dbo].[Profil]";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        $path = $z[6];
    }

    if($_FILES['logo']['name'] != ''){
        foreach ($_FILES["logo"] as $file) {
            $tmp_name = $_FILES["logo"]["tmp_name"];
            $path = $uploads_dir . basename($_FILES["logo"]["name"]);
            if(move_uploaded_file($tmp_name, $path)){
                messageAlert(lang('Profil berhasil diperbaharui'),'success');
                header('Location: mprofile.php');
            }
            else{
                messageAlert(lang('Gagal upload logo koperasi'),'danger');
                header('Location: mprofile.php');
            }
        }
    }

    $sql = "exec dbo.ProsesProfil '$email','$kname','$knick','$path','$kphone','$address','$province','$city'";
    $stmt = sqlsrv_query($conn, $sql);
    if($stmt){
        $sqlq = "exec dbo.ProsesCardDesign '$design','$color'";
        $stmtq = sqlsrv_query($conn, $sqlq);

        messageAlert(lang('Profil berhasil diperbaharui'),'success');
        header('Location: mprofile.php');
    }
    else{
        messageAlert(lang('Gagal perbaharui profil'),'danger');
        header('Location: mprofile.php');
    }
}
?>