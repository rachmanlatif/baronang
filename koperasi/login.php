<?php require('blank-header.php');?>

<?php
include "connectinti.php";

if(!empty($_SESSION['KID']) && !empty($_SESSION['NamaKoperasi']) && !empty($_SESSION['Server']) && !empty($_SESSION['DatabaseName']) && !empty($_SESSION['UserDB']) && !empty($_SESSION['PassDB']) &&  !empty($_SESSION['UserID']) && !empty($_SESSION['Name']) && !empty($_SESSION['KodeJabatan'])){
    unset($_SESSION['KID']);
    unset($_SESSION['NamaKoperasi']);
    unset($_SESSION['Server']);
    unset($_SESSION['DatabaseName']);
    unset($_SESSION['UserDB']);
    unset($_SESSION['PassDB']);
    unset($_SESSION['Status']);
    unset($_SESSION['UserID']);
    unset($_SESSION['Name']);
    unset($_SESSION['KodeJabatan']);
}
?>

<div class="login-box">
    <div class="login-logo">
        <a href="" class="logo">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src = "static/images/Logo_Black.png"></span>
        </a>
    </div><!-- /.login-logo -->

    <?php if(isset($_SESSION['error-type']) and isset($_SESSION['error-message']) and isset($_SESSION['error-time'])){ ?>
        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>
    <?php } ?>

    <div class="login-box-body">
        <p class="login-box-msg"><?php echo lang('Masuk'); ?></p>
        <form action="cek_login.php" method="post">
            <div class="form-group has-feedback">
                <input type="number" name="kid" class="form-control" placeholder="<?php echo lang('ID Koperasi'); ?>">
                <span class="fa fa-address-card-o form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="name" class="form-control" placeholder="<?php echo lang('Username'); ?>">
                <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="<?php echo lang('Password'); ?>">
                <span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <a href="register.php"><?php echo lang('Daftar'); ?></a>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-flat btn-primary btn-block"><?php echo lang('Masuk'); ?></button>

                <span class="pull-right">
                <?php
                $y = "select * from [dbo].[LanguageList]";
                $n = sqlsrv_query($conns, $y);
                while($m = sqlsrv_fetch_array( $n, SQLSRV_FETCH_NUMERIC)){
                    if($_SESSION['Language'] == $m[0]){
                        echo $m[0].' /';
                    } else { ?>
                        <a href="?lang=<?php echo $m[0]; ?>" title="<?php echo $m[1]; ?>"><?php echo $m[0]; ?></a> /
                    <?php } ?>
                <?php } ?>
                </span>
            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<?php require('blank-footer.php');?>
