<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];

$ul0 = "";
$ul1 = "";
$ul2 = 0;
$ul3 = 0;
$ul4 = "";
$ul5 = "";
$ul6 = "";
$ul7 = "";
$ul8 = 0;
$ulprocedit = "";
$uldisabled = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.JabatanView where KodeJabatan='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if($eulrow != null){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul4    = $eulrow[7];
        $ul5    = $eulrow[9];
        $ul6    = $eulrow[11];
        $ul7    = $eulrow[2];

        $qw = "select top 1 StatusLimit from dbo.GeneralSetting";
        $as = sqlsrv_query($conn, $qw);
        $zx = sqlsrv_fetch_array($as, SQLSRV_FETCH_NUMERIC);
        if($zx != null){
            if($zx[0] == 1){
                $ul2 = $eulrow[3];
                $ul3 = $eulrow[5];
            }
            else{
                $t = $eulrow[3]+$eulrow[5];
                $ul8 = $t;
            }
        }

        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='mposition.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='mposition.php?page=".$_GET['page']."';</script>";
        }
    }
}

$mpbspksql = "select * from [dbo].[BasicSavingTypeView] where Status='1'";  //simpanan pokok
$mpbswjsql = "select * from [dbo].[BasicSavingTypeView] where Status='0'";  //simpanan wajib
$mpbssksql = "select * from [dbo].[BasicSavingTypeView] where Status='2'";  //simpanan sukarela
$mpbsstmt = sqlsrv_query($conn, $mpbspksql);
$mpbswstmt = sqlsrv_query($conn, $mpbswjsql);
$mpbssstmt = sqlsrv_query($conn, $mpbssksql);
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<form action="proc_mposition.php<?=$ulprocedit?>" method="POST" class="form-horizontal">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo lang('Jabatan'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="positionname" class="col-sm-5 control-label" style="text-align: left;"><?php echo lang('Nama jabatan'); ?></label>
                        <div class="col-sm-7">
                            <input type="text" name="name" class="form-control" id="positionname" placeholder="" value="<?=$ul1?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Description" class="col-sm-5 control-label" style="text-align: left;"><?php echo lang('Keterangan'); ?></label>
                        <div class="col-sm-7">
                            <input type="text" name="description" class="form-control" id="Description" placeholder="" value="<?=$ul7?>">
                        </div>
                    </div>
                    <?php
                    $qw = "select top 1 StatusLimit from dbo.GeneralSetting";
                    $as = sqlsrv_query($conn, $qw);
                    $zx = sqlsrv_fetch_array($as, SQLSRV_FETCH_NUMERIC);
                    if($zx != null){
                        if($zx[0] == 1){
                    ?>
                        <div class="form-group">
                            <label for="LimitBelanja" class="col-sm-5 control-label" style="text-align: left;"><?php echo lang('Limit belanja'); ?></label>
                            <div class="col-sm-7">
                                <input type="text" name="belanja" class="form-control price" id="LimitBelanja" placeholder="" value="<?=$ul2?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="LimitPinjaman" class="col-sm-5 control-label" style="text-align: left;"><?php echo lang('Limit pinjaman'); ?></label>
                            <div class="col-sm-7">
                                <input type="text" name="pinjaman" class="form-control price" id="LimitPinjaman" placeholder="" value="<?=$ul3?>">
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="form-group">
                            <label for="LimitPinjaman" class="col-sm-5 control-label" style="text-align: left;"><?php echo lang('Limit gabungan'); ?></label>
                            <div class="col-sm-7">
                                <input type="text" name="gabungan" class="form-control price" placeholder="" value="<?=$ul8?>">
                                <?php echo lang('(Belanja+pinjaman)'); ?>
                            </div>
                        </div>
                    <?php }
                    }?>
                    *Limit dapat diganti melalui menu Pengaturan Umum
                </div>
                <!-- /.col -->
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Simpanan pokok'); ?></label>
                        <div class="col-sm-9">
                            <select name="pokok" class="form-control">
                                <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                                <?php
                                while ($mpbsrow = sqlsrv_fetch_array( $mpbsstmt, SQLSRV_FETCH_NUMERIC)) {
                                    ?>
                                    <option value="<?=$mpbsrow[0];?>" <?php if($mpbsrow[0]==$ul5){echo "selected";} ?>><?=$mpbsrow[1];?> - <?=$mpbsrow[3];?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Simpanan wajib'); ?></label>
                        <div class="col-sm-9">
                            <select name="wajib" class="form-control">
                                <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                                <?php
                                while ($mpbswrow = sqlsrv_fetch_array( $mpbswstmt, SQLSRV_FETCH_NUMERIC)) {
                                    ?>
                                    <option value="<?=$mpbswrow[0];?>" <?php if($mpbswrow[0]==$ul4){echo "selected";} ?>><?=$mpbswrow[1];?> - <?=$mpbswrow[3];?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Simpanan sukarela'); ?></label>
                        <div class="col-sm-9">
                            <select name="sukarela" class="form-control">
                                <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                                <?php
                                while ($mpbsswrow = sqlsrv_fetch_array( $mpbssstmt, SQLSRV_FETCH_NUMERIC)) {
                                    ?>
                                    <option value="<?=$mpbsswrow[0];?>" <?php if($mpbsswrow[0]==$ul6){echo "selected";} ?>><?=$mpbsswrow[1];?> - <?=$mpbsswrow[3];?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-4">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <button type="submit" class="btn btn-flat btn-block btn-success pull-left"><?php echo lang('Perbaharui'); ?></button>
                    <?php } else{ ?>
                        <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                    <?php } ?>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <a href="mposition.php" class="btn btn-flat btn-block btn-default"><?php echo lang('Batal'); ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Daftar jabatan'); ?></h3>
    </div>
    <div class="box-body pad table-responsive">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="text-center"><?php echo lang('No'); ?></th>
                    <th class="text-center"><?php echo lang('Kode jabatan'); ?></th>
                    <th class="text-center"><?php echo lang('Nama jabatan'); ?></th>
                    <th class="text-center"><?php echo lang('Keterangan'); ?></th>
                    <th class="text-center"><?php echo lang('Limit belanja'); ?></th>
                    <th class="text-center"><?php echo lang('Limit pinjaman'); ?></th>
                    <th class="text-center"><?php echo lang('Simpanan pokok'); ?></th>
                    <th class="text-center"><?php echo lang('Simpanan wajib'); ?></th>
                    <th class="text-center"><?php echo lang('Simpanan sukarela'); ?></th>
                    <th class="text-center"><?php echo lang('Aksi'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.JabatanView";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeJabatan asc) as row FROM [dbo].[JabatanView]) a WHERE row between '$posisi' and '$batas'";

                //$ulsql = "select * from [dbo].[UserLevel]";

                $ulstmt = sqlsrv_query($conn, $ulsql);

                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    $tmpbspksql = "select * from [dbo].[BasicSavingTypeView] where KodeBasicSavingType='$ulrow[9]'";  //simpanan pokok
                    $tmpbswjsql = "select * from [dbo].[BasicSavingTypeView] where KodeBasicSavingType='$ulrow[7]'";  //simpanan wajib
                    $tmpbssksql = "select * from [dbo].[BasicSavingTypeView] where KodeBasicSavingType='$ulrow[11]'";  //simpanan sukarela
                    $tmpbsstmt = sqlsrv_query($conn, $tmpbspksql);    //simpanan pokok
                    $tmpbswstmt = sqlsrv_query($conn, $tmpbswjsql);   //simpanan wajib
                    $tmpbssstmt = sqlsrv_query($conn, $tmpbssksql);   //simpanan sukarela
                    $tpkmprow = sqlsrv_fetch_array( $tmpbsstmt, SQLSRV_FETCH_NUMERIC);   //simpanan pokok
                    $twjmprow = sqlsrv_fetch_array( $tmpbswstmt, SQLSRV_FETCH_NUMERIC);     //simpanan wajib
                    $tskmprow = sqlsrv_fetch_array( $tmpbssstmt, SQLSRV_FETCH_NUMERIC);     //simpanan sukarela
                    ?>
                    <tr>
                        <td><?=$ulrow[13];?></td>
                        <td><?=$ulrow[0];?></td>
                        <td><?=$ulrow[1];?></td>
                        <td><?=$ulrow[2];?></td>
                        <td><?=$ulrow[4];?></td>
                        <td><?=$ulrow[6];?></td>
                        <td><?=$twjmprow[3];?></td>
                        <td><?=$tpkmprow[3];?></td>
                        <td><?=$tskmprow[3];?></td>
                        <td width="100" style="padding: 3px">
                            <div class="btn-group" style="padding-right: 15px">
                                <a href="mposition.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                            </div>
                            <!--
                    <div class="btn-group">
                      <a href="<?php /* proc_mposition.php?page=<?=$halaman?>&delete=<?=$ulrow[0]?> */?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
                    </div>
					-->
                        </td>
                    </tr>
                <?php
                }

                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="mposition.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="mposition.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="mposition.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>