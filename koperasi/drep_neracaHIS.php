<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Neraca History");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->SetCellValue('A1', 'Kode Transaksi');
    $objWorkSheet->SetCellValue('B1', 'Kode Akun');
    $objWorkSheet->SetCellValue('C1', 'Tanggal Transaksi');
    $objWorkSheet->SetCellValue('D1', 'Deskripsi');
    $objWorkSheet->SetCellValue('E1', 'Debit');
    $objWorkSheet->SetCellValue('F1', 'Kredit');    



    if($_GET['st'] == 5 ){

    $no = 1;
    $row = 2;
    $x = "exec dbo.TampilDetailReportCOA '0','$_GET[acc3]'";
    
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
    //var_dump($z) ;

        $aa = "exec dbo.TampilDetailReportCOA '1','$z[0]'";

        $bb = sqlsrv_query($conn, $aa);
        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
        //var_dump($cc) ;
            
            $aaa = "exec dbo.TampilDetailReportCOA '2','$cc[0]'";

            $bbb = sqlsrv_query($conn, $aaa);
            $abc = 0;
            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
            //var_dump($ccc) ;
                    
               
                $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                $bbbb = sqlsrv_query($conn, $aaaa);
                $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                //var_dump($cccc) ;
                if($cccc != null){
                    $abc = $cccc[1];
                }
                    else{
                    $abc = $ccc[3];
                }

                $objWorkSheet->SetCellValue("A".$row,$ccc[0]);
                $objWorkSheet->SetCellValueExplicit("B".$row,$ccc[1]);
                $objWorkSheet->SetCellValue("C".$row, $ccc[2]->format('Y-m-d H:i:s'));
                $objWorkSheet->SetCellValue("D".$row,$abc);
                $objWorkSheet->SetCellValue("E".$row, number_format($ccc[6]), PHPExcel_Cell_DataType::TYPE_STRING);
                $objWorkSheet->SetCellValue("F".$row, number_format($ccc[7]), PHPExcel_Cell_DataType::TYPE_STRING);
            
                $no++;
                $row++;
            }
        }
    }
    }
//exit;
    $objWorkSheet->setTitle('Drep Neraca History');

    $fileName = 'NeracaHIS'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
