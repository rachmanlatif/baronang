<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<?php
$accno = $_GET['accno'];
$mid = $_GET['mid'];
$bal = "0";
$inte = "0";
$intepin = "0";
$fixpin = "0";
$totint = "0";
$tot = "0";
$kdmem="";
$namax="";
$dis = '';
if ($accno != '' and $mid != '') {
    $eulsql = "select * from dbo.HitungDeposito where MemberID='$mid' and AccountNumber='$accno'";
    $eulstmt = sqlsrv_query($conn, $eulsql);
    $eulrow = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if($eulrow != null){
        $a = "Select * from [dbo].[TimeDepositClose] where AccountNumber = '$eulrow[1]'";
        $b = sqlsrv_query($conn, $a);
        $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
        if($c != null){
            $_SESSION['error-message'] = 'Account number closed';
            $_SESSION['error-type'] = 'warning';
            $_SESSION['error-time'] = time()+5;

            $dis = 'disabled';
        }
        else{
            $bal = $eulrow[10];
            $inte = $eulrow[4];
            $intepin = $eulrow[5];
            $fixpin = $eulrow[3];
            $totint = $eulrow[12];
            $tot = $eulrow[16];
            $namax = $_GET['name'];
            $kdmem = $eulrow[0];
        }
    }
    else{
        $_SESSION['error-message'] = 'Not found';
        $_SESSION['error-type'] = 'warning';
        $_SESSION['error-time'] = time()+5;

        $dis = 'disabled';
    }
}
?>
<script language="javascript">
    function load_page(accno){
        window.location.href = "cscatimedeposit.php?accno=" + accno +"&mid=" + document.getElementById("csoaregsavmember").value;
    };
</script>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Time Deposit Close</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="proccscatimedeposit.php" method="POST" class="form-horizontal">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="csoaregsavmember" class="col-sm-4 control-label" style="text-align: left;">Member ID</label>
                                <div class="col-sm-8">
                                    <input type="number" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?php echo $_SESSION['RequestMember']; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-sm-4 control-label" style="text-align: left;">Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="" value ="<?php echo $_SESSION['RequestMemberName']; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;">Account Number</label>
                                <div class="col-sm-8">
                                    <input type="number" name="acc" class="form-control" id="balance" value ="<?=$accno?>" onblur="load_page(this.value);" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="balance" class="col-sm-4 control-label" style="text-align: left;">Balance</label>
                                <div class="col-sm-8">
                                    <input type="text" name="balance" class="form-control price" id="balance" value ="<?=$bal?>" placeholder="" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="interest" class="col-sm-4 control-label" style="text-align: left;">Interest</label>
                                <div class="col-sm-8">
                                    <input type="number" name="kode" class="form-control" id="interest" value ="<?=$inte?>" placeholder="" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="penalty" class="col-sm-4 control-label" style="text-align: left;">Interest Penalty</label>
                                <div class="col-sm-8">
                                    <input type="text" name="penalty" class="form-control price" id="penalty" value ="<?=$intepin?>" placeholder="" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fixed" class="col-sm-4 control-label" style="text-align: left;">Fixed Penalty</label>
                                <div class="col-sm-8">
                                    <input type="text" name="fixed" class="form-control price" id="fixed" value ="<?=$fixpin?>" placeholder="" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="transfer" class="col-sm-4 control-label" style="text-align: left;">Total Interest Transfered</label>
                                <div class="col-sm-8">
                                    <input type="text" name="transfer" class="form-control price" id="transfer" value ="<?=$totint?>" placeholder="" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="total" class="col-sm-4 control-label" style="text-align: left;">Total</label>
                                <div class="col-sm-8">
                                    <input type="text" name="total" class="form-control price" id="total" value ="<?=$tot?>" placeholder="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-flat btn-block btn-danger" <?php echo $dis; ?>>Close</button>
                        </div>
                        <div class="col-sm-4">
                            <?php if ($accno != '' or $mid != ''){ ?>
                                <a href="cscatimedeposit.php"><button type="button" class="btn btn-flat btn-block btn-default">Cancel</button></a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-4">
                            <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
