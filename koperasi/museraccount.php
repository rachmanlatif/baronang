<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ulprocedit = "";
$uldisabled = "";
$uledit = $_GET['edit'];
if(!empty($uledit)){
    $eulsql   = "select * from dbo.LoginList where UserID='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = $eulrow[4];
        $ul5    = $eulrow[5];
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='muserlevel.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='muserlevel.php?page=".$_GET['page']."';</script>";
        }
    }
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-5">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo lang('Pengguna'); ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="proc_museraccount.php<?=$ulprocedit?>" method="POST">
                <div class="box-body">
                    <div class="form-group">
                        <label for="UserName" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('User ID Baronang App'); ?></label>
                        <div class="col-sm-8">
                            <input type="number" name="username" class="form-control" id="result-code" placeholder="" value="<?=$ul0?>" <?=$uldisabled?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Position" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Level pengguna'); ?></label>
                        <div class="col-sm-8">
                            <select name="level" class="form-control">
                                <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                                <?php
                                $julsql   = "select * from [dbo].[UserLevel]";
                                $julstmt = sqlsrv_query($conn, $julsql);
                                while($julrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <option value="<?=$julrow[0];?>" <?php if($julrow[0] == $ul3){echo "selected";}?>><?=$julrow[1];?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Interest Text" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Status'); ?></label>
                        <div class="col-sm-4">
                            <div class="radio">
                                <label style="padding-left: 10px">
                                    <input type="radio" name="status" class="minimal" id="status0" value="1" <?php if($ul5 == 1){ echo "checked";} ?>>
                                    <?php echo lang('Aktif'); ?>
                                </label>
                                <label style="padding-left: 10px">
                                    <input type="radio" name="status" class="minimal" id="status1" value="0" <?php if($ul5 ==  0){ echo "checked";} ?>>
                                    <?php echo lang('Tidak aktif'); ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Interest Text" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Persetujuan'); ?></label>
                        <div class="col-sm-4">
                            <div class="radio">
                                <label style="padding-left: 10px">
                                    <input type="radio" name="approval" class="minimal" id="approval0" value="1" <?php if($ul4 == 1){ echo "checked";} ?>>
                                    SPV
                                </label>
                                <label style="padding-left: 10px">
                                    <input type="radio" name="approval" class="minimal" id="approval1" value="0" <?php if($ul4 ==  0){ echo "checked";} ?>>
                                    Staff
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4">
                                <?php if(count($eulrow[0]) > 0){ ?>
                                    <a href="museraccount.php" class="btn btn-flat btn-default"><?php echo lang('Batal'); ?></a>
                                <?php } ?>
                            </div>
                            <div class="col-sm-4">
                                <?php if(count($eulrow[0]) > 0){ ?>
                                    <button type="submit" class="btn btn-flat btn-block btn-success pull-right"><?php echo lang('Perbaharui'); ?></button>
                                <?php }else{ ?>
                                    <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                                <?php } ?>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" id="ocam" class="btn btn-info"><i class="fa fa-camera"></i> Scan QR Code</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </form>
        </div>
    </div>
    <div class="col-md-7">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Daftar pengguna'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class=""><?php echo lang('No'); ?></th>
                        <th class=""><?php echo lang('User ID Baronang App'); ?></th>
                        <th class=""><?php echo lang('Nama'); ?></th>
                        <th class=""><?php echo lang('Level Pengguna'); ?></th>
                        <th class=""><?php echo lang('Status'); ?></th>
                        <th class=""><?php echo lang('Persetujuan'); ?></th>
                        <th class="text-center"><?php echo lang('Aksi'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from dbo.LoginList";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY UserID asc) as row FROM [dbo].[LoginList]) a WHERE row between '$posisi' and '$batas'";
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        //userlevel
                        $rulsql   = "select * from dbo.UserLevel where KodeUserLevel='$ulrow[3]'";
                        $rulstmt  = sqlsrv_query($conn, $rulsql);
                        $rulrow   = sqlsrv_fetch_array( $rulstmt, SQLSRV_FETCH_NUMERIC);
                        ?>
                        <tr>
                            <td><?=$ulrow[6];?></td>
                            <td><?=$ulrow[0];?></td>
                            <td><?=$ulrow[1];?></td>
                            <td><?=$rulrow[1];?></td>
                            <td>
                                <?php
                                if($ulrow[5] == 1){
                                    echo 'Aktif';
                                }
                                else{
                                    echo 'Tidak Aktif';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if($ulrow[4] == 1){
                                    echo 'SPV';
                                }
                                else{
                                    echo 'Staff';
                                }
                                ?>
                            </td>
                            <td width="20%" style="padding: 3px">
                                <div class="btn-group" style="padding-right: 15px">
                                    <a href="museraccount.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                                </div>
                                <!--
                      <div class="btn-group">
                        <a href="<?php /*proc_museraccount.php?page=<?=$halaman?>&delete=<?=$ulrow[0]?> */?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
                      </div>
					  -->
                            </td>
                        </tr>
                    <?php
                    }

                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
                <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
                <?=$posisi." - ".$batas?>
                <ul class="pagination pagination-sm no-margin pull-right">
                    <li class="paginate_button"><a href="museraccount.php">&laquo;</a></li>
                    <?php
                    for($ul = 1; $ul <= $jmlhalaman; $ul++){
                        if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                        echo '<li class="paginate_button '.$ulpageactive.'"><a href="museraccount.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                    }
                    ?>
                    <li class="paginate_button"><a href="museraccount.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="box hide" id="form-camera">
            <div class="box-header">
                <h3 class="box-title">Scan QR Code/Barcode</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2">Pilih kamera</label>
                    <div class="col-sm-7">
                        <select class="form-control" id="camera-select"></select>
                    </div>
                    <div class="col-sm-3">
                        <button title="Decode Image" class="btn btn-default" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                        <button title="Image shoot" class="btn btn-info disabled" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                        <button title="Start Scan QR Barcode" class="btn btn-success" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-camera"></span></button>
                        <button title="Pause" class="btn btn-warning" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                        <button title="Stop streams" class="btn btn-danger" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="well" style="position: relative;display: inline-block;">
                            <canvas id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="thumbnail" id="result">
                            <img width="320" height="240" id="scanned-img" src="">
                            <div class="caption">
                                <p class="text-center" id="scanned-QR"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#ocam').click(function(){
        $('#result-code').val();
        $('#form-camera').removeClass('hide');
    });
</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>