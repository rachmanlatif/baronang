<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<?php
function buatrp($angka)
{
    $jadi = number_format($angka,2,',','.');
    return $jadi;
}
$accno = $_GET['accno'];
if ($accno !='') {
    $eulsql   = "select * from dbo.LoanSummarySisa where LoanNumber='$accno'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    $bal = buatrp($eulrow[2]);
    $inte = buatrp($eulrow[3]);
    $namax = $_GET['name'];
    $kdmem = $eulrow[0];
}else {
    $bal = "0";
    $inte = "0";
    $kdmem="";
    $namax="";
}
?>
    <script language="javascript">
        function load_page(accno){
            window.location.href = "csloanclose.php?accno=" + accno +"&name=" + document.getElementById("nama").value;
        }
    </script>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-sm-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Loan Close</h3>
            </div>
            <form action="procloanclose.php" method="POST" class="form-horizontal">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="csoaregsavmember" class="col-sm-5 control-label" style="text-align: left;">Member ID</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="member" class="form-control" id="csoaregsavmember" value="<?php echo $_SESSION['RequestMember']; ?>" placeholder="" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nama" class="col-sm-5 control-label" style="text-align: left;">Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo $_SESSION['RequestMemberName']; ?>" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="text-align: left;">Account Number</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="acc" class="form-control" id="belanja" placeholder="" onblur="load_page(this.value);" value ="<?=$accno?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="belanja" class="col-sm-5 control-label" style="text-align: left;">Balance Bill Pokok</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="balancebillpokok" class="form-control" id="balancebillpokok" value ="<?=$bal?>" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="text-align: left;">Balance Bill Bunga</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="BBB" class="form-control" id="BBB" value ="<?=$inte?>" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="belanja" class="col-sm-5 control-label" style="text-align: left;">Balance Payment Pokok</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="BPP" class="form-control" id="BPP" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="text-align: left;">Balance Payment Bunga</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="BPB" class="form-control" id="BPB" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="belanja" class="col-sm-5 control-label" style="text-align: left;">Keterangan</label>
                                    <div class="col-sm-7">
                                        <textarea name="keterangan" rows="3" cols="30" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="text-align: left;">Pinalty</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="pinalty" class="form-control" id="pinalty" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-flat btn-block btn-danger pull-right">Close</button>
                            </div>
                            <div class="col-sm-4">

                            </div>
                            <div class="col-sm-4">
                                <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>