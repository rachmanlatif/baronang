<?php
session_start();
error_reporting(0);

include "connect.php";
include "connectuser.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$bln			= $_GET['balance'];
$delete			= $_GET['delete'];

$memberPost	= $_POST['member'];
$regPost	= $_POST['reg'];
$amount	= $_POST['amount'];

if($memberPost == "" || $regPost == "" || $amount == ""){
    messageAlert('Harap isi seluruh kolom','info');
    header('Location: csoaregsaving.php');
}
else{
    $x = "select * from [dbo].[RegularSavingType] where RegularSavingType='$regPost'";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        if($amount < $z[4]){
            messageAlert('Minimum amount is '.number_format($z[4]),'warning');
            header('Location: identity_cs.php');
        }
    }

    $a = "Select [dbo].[getKodeRegularSavingAcc]('$z[0]','$z[7]','$memberPost')";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    $reg_sav = $c[0];

    $a = "select * from [dbo].[MemberList] where MemberID='$memberPost' and StatusMember = 1";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $ppsql = "exec [dbo].[ProsesRegularSavingAcc] '$_SESSION[KID]', '$memberPost', '$regPost', '$amount','',0";
        $ppstmt = sqlsrv_query($conn, $ppsql);

        if($ppstmt){
            messageAlert('Success open Regular Saving Account and create ticketing cashier','success');
            header('Location: identity_cs.php');
        }
        else{
            messageAlert('Failed create Regular Saving','danger');
            header('Location: csoaregsaving.php');
        }
    }
    else{
        messageAlert('Member ID Not Active','warning');
        header('Location: csoaregsaving.php');
    }
}
?>
