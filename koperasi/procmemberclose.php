<?php
session_start();
error_reporting(0);
include "connect.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$bln			= $_GET['balance'];
$delete			= $_GET['delete'];

$member		= $_POST['member'];
$nama		= $_POST['nama'];

if($member == ""){
    messageAlert('Harap isi seluruh kolom','info');
    header('Location: cscamember.php');
}
else{
    $a = "select * from [dbo].[MemberList] where MemberID='$member' and StatusMember = 1";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        //close time deposit
        $aa = "select * from [dbo].[TimeDepositAccount] where MemberID='$member' and AccountNumber not in(select AccountNumber from [dbo].[TimeDepositClose])";
        $bb = sqlsrv_query($conn, $aa);
        while($cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC)){
            $ppsql1 = "exec [dbo].[ProsesTimeDepositClose] '$cc[1]', '$_SESSION[UserID]'";
            $ppstmt1 = sqlsrv_query($conn, $ppsql1);
        }

        //close regular saving
        $aaa = "select * from [dbo].[RegularSavingAcc] where MemberID='$member' and AccountNo not in(select AccNo from [dbo].[RegularSavingClose])";
        $bbb = sqlsrv_query($conn, $aaa);
        while($ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC)){
            $ppsql2 = "exec [dbo].[ProsesRegularSavingClose] '$ccc[2]',0, '$_SESSION[UserID]'";
            $ppstmt2 = sqlsrv_query($conn, $ppsql2);
        }

        //close basic saving
        $date = date('Y-m-d H:i:s');
        $aaaa = "select * from [dbo].[BasicSavingBalance] where MemberID='$member' and AccNo not in(select AccNo from [dbo].[BasicSavingCloseTransaction])";
        $bbbb = sqlsrv_query($conn, $aaaa);
        while($cccc = sqlsrv_fetch_array( $bbbb, SQLSRV_FETCH_NUMERIC)){
            $amount = $cccc[4] - $cccc[5];
            $ppsql3 = "exec [dbo].[ProsesBasicSavingCloseTransaction] '$_SESSION[KID]','$date','$cccc[3]','$cccc[2]','$amount'";
            $ppstmt3 = sqlsrv_query($conn, $ppsql3);
        }

        //not active member
        $ppsql = "update MemberList set StatusMember =0 where MemberID = '$member'";
        $ppstmt = sqlsrv_query($conn, $ppsql);
        if($ppstmt){
            messageAlert('Success closed Member and all account','success');
            header('Location: identity_cs.php');
        }
        else{
            messageAlert('Failed close Member','danger');
            header('Location: cscamember.php');
        }
    }
    else{
        messageAlert('Member ID not active','warning');
        header('Location: cscamember.php');
    }
}
?>