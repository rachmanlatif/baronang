<?php
include "connect.php";
if(isset($_GET['step'])){
    if(is_numeric($_GET['step'])){
        $aa = "select * from KoneksiKoperasiBaronang.[dbo].[WizSeting] where Step = '$_GET[step]'";
        $bb = sqlsrv_query($conn, $aa);
        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
        if($cc != null){
            if($cc[5] == 1){
                $a = "select top 1 * from [dbo].[Wiz]";
                $b = sqlsrv_query($conn, $a);
                $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                if($c != null){
                    if($_GET['step'] > $c[1]){
                        $sql = "exec [dbo].[ProsesWiz] '$_SESSION[KID]', '$cc[0]','1'";
                        $stmt = sqlsrv_query($conn, $sql);
                        if($stmt){
                            $next = $_GET['step'] +1;
                            $x = "select * from KoneksiKoperasiBaronang.[dbo].[WizSeting] where Step = '$next'";
                            $y = sqlsrv_query($conn, $x);
                            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                            if($z != null){
                                messageAlert('Berhasil ke step selanjutnya. Harap lengkapi halaman ini.','info');
                                header('Location: '.$z[3]);
                            }
                            else{
                                messageAlert('Berhasil ke step selanjutnya','info');
                                echo '<script>history.go(-1);</script>';
                            }
                        }
                        else{
                            messageAlert('Gagal ke step selanjutnya','danger');
                            echo '<script>history.go(-1);</script>';
                        }
                    }
                    else{
                        messageAlert('Step tidak valid','warning');
                        echo '<script>history.go(-1);</script>';
                    }
                }
                else{
                    messageAlert('Belum ada step yang telah dilalui','warning');
                    echo '<script>history.go(-1);</script>';
                }
            }
            else{
                messageAlert('Halaman ini tidak dapat dilewati. Harap lengkapi proses ini.','warning');
                echo '<script>history.go(-1);</script>';
            }
        }
        else{
            messageAlert('Gagal ke step selanjutnya','warning');
            echo '<script>history.go(-1);</script>';
        }
    }
    else{
        messageAlert('Invalid retrieve data','warning');
        echo '<script>history.go(-1);</script>';
    }
}
else{
    messageAlert('Invalid request data','warning');
    echo '<script>history.go(-1);</script>';
}
?>