<?php
if(!isset($_POST['loanappnum'])){
    echo 'Data not found!';
}
else{
    include "connect.php";

    if(isset($_POST['message']) and isset($_POST['loanappnum'])){
        //save chat
        $kode = $_POST['loanappnum'];
        $message = $_POST['message'];
        if($message != ''){
            $sql = "exec dbo.ProsesChatApproval '$kode','$_SESSION[UserID]','$message','1'";
            $stmt = sqlsrv_query($conn, $sql);
        }
    }
    ?>

    <input type="hidden" id="kode" value="<?php echo $_POST['loanappnum']; ?>" readonly>
    <!-- Conversations are loaded here -->
    <div class="direct-chat-messages">
        <?php
        $a = "select * from [dbo].[ChatApprovalView] where KodeAppNum = '".$_POST['loanappnum']."' order by Tanggal asc";
        $b = sqlsrv_query($conn, $a);
        while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
            $right = '';
            $p1 = 'pull-left';
            $p2 = 'pull-right';
            if($_SESSION['UserID'] == $c[4]){
                $right = 'right';
                $p1 = 'pull-right';
                $p2 = 'pull-left';
            }
            ?>

            <!-- Message. Default to the left -->
            <div class="direct-chat-msg <?php echo $right; ?>">
                <div class="direct-chat-info clearfix">
                    <span class="direct-chat-name <?php echo $p1; ?>"><?php echo $c[5]; ?></span>
                    <span class="direct-chat-timestamp <?php echo $p2; ?>"><?php echo $c[1]->format('Y-m-d H:i:s'); ?></span>
                </div>
                <!-- /.direct-chat-info -->
                <img class="direct-chat-img" src="static/dist/img/user-icon.png" alt="message user image">
                <!-- /.direct-chat-img -->
                <div class="direct-chat-text">
                    <?php echo $c[2]; ?>
                </div>
                <!-- /.direct-chat-text -->
            </div>
            <!-- /.direct-chat-msg -->

        <?php } ?>
    </div>
    <!--/.direct-chat-messages-->
<?php } ?>