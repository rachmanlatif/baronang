<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
include "connectinti.php";

$abc = "select top 1 * from [dbo].[Profil]";
$def = sqlsrv_query($conn, $abc);
$ghi = sqlsrv_fetch_array( $def, SQLSRV_FETCH_NUMERIC);

$qwe = "select top 1 * from [dbo].[CardDesign]";
$asd = sqlsrv_query($conn, $qwe);
$zxc = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC);
if($zxc != null){
    $_SESSION['FontColor'] = $zxc[1];
}
else{
    $_SESSION['FontColor'] = '#000000';
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Profil koperasi'); ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

    <div class="row">
        <div class="col-sm-12">
            <form class="form-horizontal" action="proc_mprofile.php" method="POST" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab"><?php echo lang('Profil'); ?></a></li>
                        <li><a href="#tab_2" data-toggle="tab"><?php echo lang('Dokumen'); ?></a></li>
                        <li><a href="#tab_3" data-toggle="tab"><?php echo lang('Design Kartu Member'); ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Nama Koperasi'); ?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="kname" class="form-control" placeholder="" value="<?php echo $ghi[1]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Nama Singkat Koperasi'); ?></label>
                                <div class="col-sm-2">
                                    <input type="text" name="knick" class="form-control" placeholder="" value="<?php echo $ghi[2]; ?>">
                                </div>
                                <div class="col-sm-4">
                                    *Nama singkat koperasi digunakan untuk nama di Kartu Anggota Koperasi
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Email'); ?></label>
                                <div class="col-sm-4">
                                    <input type="email" name="email" class="form-control" placeholder="" value="<?php echo $ghi[0]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Logo'); ?></label>
                                <div class="col-sm-4">
                                    <?php echo lang('Pilih kembali untuk mengganti file'); ?>
                                    <input type="file" name="logo" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"></label>
                                <div class="col-sm-4 bg-gray-light">
                                    <img src="<?php echo $ghi[6]; ?>" class="img-responsive">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Telepon'); ?></label>
                                <div class="col-sm-6">
                                    <input type="number" name="kphone" class="form-control" placeholder="" value="<?php echo $ghi[7]; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Alamat'); ?></label>
                                <div class="col-sm-6">
                                    <textarea name="address" class="form-control"><?php echo $ghi[3]; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Provinsi'); ?></label>
                                <div class="col-sm-4">
                                    <select name="province" class="form-control select2" id="province" required="">
                                        <option value="">- Select -</option>
                                        <?php
                                        $a = "select * from [dbo].[Provinsi]";
                                        $b = sqlsrv_query($conns, $a);
                                        while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                            if($ghi[4] == $c[0]){
                                                ?>
                                                <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                            <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Kota'); ?></label>
                                <div class="col-sm-4" id="city">
                                    <select name="city" class="form-control select2" required="">
                                        <option value="">- Select -</option>
                                        <?php
                                        $a = "select * from [dbo].[Kota] where KodeProvinsi = '$ghi[4]'";
                                        $b = sqlsrv_query($conns, $a);
                                        while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                            if($ghi[5] == $c[0]){
                                                ?>
                                                <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                            <?php } } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Ekstensi file'); ?></label>
                                <div class="col-sm-6">
                                    .jpg, .png, .pdf, .jpeg
                                </div>
                            </div>
                            <?php
                            $count = 0;
                            $a = "select * from [dbo].[JenisDoc]";
                            $b = sqlsrv_query($conns, $a);
                            while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                            ?>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: left;"><?php echo $c[1]; ?></label>
                                    <div class="col-sm-6">
                                        <?php
                                        $aa = "select * from [dbo].[UserRegisterDoc] where KodeJenisDoc='$c[0]'";
                                        $bb = sqlsrv_query($conns, $aa);
                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                        if($cc != null){
                                            ?>
                                            <a href="<?php echo $cc[2]; ?>" target="_blank"><?php echo lang('Klik untuk melihat file'); ?></a>
                                            <br>
                                            <?php echo lang('Pilih kembali untuk mengganti file'); ?>
                                        <?php } ?>
                                        <input type="hidden" name="jenisdoc[]" class="form-control" value="<?php echo $c[0]; ?>" readonly>
                                        <input type="file" name="filename[]" class="form-control" accept=".jpg,.png,.pdf,.jpeg">
                                    </div>
                                </div>
                            <?php $count++; } ?>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Pilih Design'); ?></label>
                                <div class="col-sm-6">
                                    <?php
                                    $img='';
                                    $img1='';
                                    $img2='';
                                    $img3='';
                                    $img4='';
                                    $img5='';
                                    $img6='';

                                    $cimg='';
                                    $cimg1='';
                                    $cimg2='';
                                    $cimg3='';
                                    $cimg4='';
                                    $cimg5='';
                                    $cimg6='';
                                    if($zxc[0] == 'card/card0.png'){
                                        $img='active';
                                        $cimg='checked';
                                    }
                                    else if($zxc[0] == 'card/card1.png'){
                                        $img1='active';
                                        $cimg1='checked';
                                    }
                                    else if($zxc[0] == 'card/card2.png'){
                                        $img2='active';
                                        $cimg2='checked';
                                    }
                                    else if($zxc[0] == 'card/card3.png'){
                                        $img3='active';
                                        $cimg3='checked';
                                    }
                                    else if($zxc[0] == 'card/card4.png'){
                                        $img4='active';
                                        $cimg4='checked';
                                    }
                                    else if($zxc[0] == 'card/card5.png'){
                                        $img5='active';
                                        $cimg5='checked';
                                    }
                                    else if($zxc[0] == 'card/'.$_SESSION['KID'].'_card.png'){
                                        $img6='active';
                                        $cimg6='checked';
                                    }
                                    ?>
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="<?php echo $img; ?>"><a href="#img_0" data-toggle="tab"><?php echo lang('Default'); ?></a></li>
                                            <li class="<?php echo $img1; ?>"><a href="#img_1" data-toggle="tab"><?php echo lang('1'); ?></a></li>
                                            <li class="<?php echo $img2; ?>"><a href="#img_2" data-toggle="tab"><?php echo lang('2'); ?></a></li>
                                            <li class="<?php echo $img3; ?>"><a href="#img_3" data-toggle="tab"><?php echo lang('3'); ?></a></li>
                                            <li class="<?php echo $img4; ?>"><a href="#img_4" data-toggle="tab"><?php echo lang('4'); ?></a></li>
                                            <li class="<?php echo $img5; ?>"><a href="#img_5" data-toggle="tab"><?php echo lang('5'); ?></a></li>
                                            <li class="<?php echo $img6; ?>"><a href="#img_6" data-toggle="tab"><?php echo lang('Upload Design'); ?></a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane <?php echo $img; ?>" id="img_0">
                                                <input type="radio" name="design" value="card/card0.png" <?php echo $cimg; ?>> Pilih default
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <img src="card/card0.png" class="img-responsive">
                                                        <?php if($_SESSION['Logo'] != ''){ ?>
                                                            <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                                        <?php } ?>
                                                        <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                                        <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane <?php echo $img1; ?>" id="img_1">
                                                <input type="radio" name="design" value="card/card1.png" <?php echo $cimg1; ?>> Pilih design 1
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <img src="card/card1.png" class="img-responsive">
                                                        <?php if($_SESSION['Logo'] != ''){ ?>
                                                            <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                                        <?php } ?>
                                                        <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                                        <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane <?php echo $img2; ?>" id="img_2">
                                                <input type="radio" name="design" value="card/card2.png" <?php echo $cimg2; ?>> Pilih design 2
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <img src="card/card2.png" class="img-responsive">
                                                        <?php if($_SESSION['Logo'] != ''){ ?>
                                                            <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                                        <?php } ?>
                                                        <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                                        <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane <?php echo $img3; ?>" id="img_3">
                                                <input type="radio" name="design" value="card/card3.png" <?php echo $cimg3; ?>> Pilih design 3
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <img src="card/card3.png" class="img-responsive">
                                                        <?php if($_SESSION['Logo'] != ''){ ?>
                                                            <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                                        <?php } ?>
                                                        <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                                        <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane <?php echo $img4; ?>" id="img_4">
                                                <input type="radio" name="design" value="card/card4.png" <?php echo $cimg4; ?>> Pilih design 4
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <img src="card/card4.png" class="img-responsive">
                                                        <?php if($_SESSION['Logo'] != ''){ ?>
                                                            <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                                        <?php } ?>
                                                        <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                                        <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane <?php echo $img5; ?>" id="img_5">
                                                <input type="radio" name="design" value="card/card5.png" <?php echo $cimg5; ?>> Pilih design 5
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <img src="card/card5.png" class="img-responsive">
                                                        <?php if($_SESSION['Logo'] != ''){ ?>
                                                            <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                                        <?php } ?>
                                                        <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                                        <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                                        <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane <?php echo $img6; ?>" id="img_6">
                                                <?php
                                                //Image Locations
                                                $image_card = 'card/'.$_SESSION['KID'].'_card.png';
                                                $image_card2 = 'card/'.$_SESSION['KID'].'_card.jpg';
                                                if(file_exists($image_card)){
                                                    $link = $image_card;
                                                }
                                                else if(file_exists($image_card2)){
                                                    $link = $image_card2;
                                                }
                                                else{
                                                    $link = '';
                                                }
                                                ?>
                                                <?php if($link != ''){ ?>
                                                    <input type="radio" name="design" value="<?php echo $link; ?>" <?php echo $cimg6; ?>> Pilih upload design
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <img src="<?php echo $link; ?>" class="img-responsive">
                                                            <?php if($_SESSION['Logo'] != ''){ ?>
                                                                <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                                            <?php } ?>
                                                            <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                                            <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                                            <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                                            <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                                        </div>
                                                    </div>
                                                    <br>
                                                <?php } ?>

                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Pilih gambar'); ?></label>
                                                    <div class="col-sm-8">
                                                        <input type="file" name="path" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Warna Huruf'); ?></label>
                                <div class="col-sm-4">
                                    <div class="input-group colorpicker colorpicker-element">
                                        <input type="text" name="color" class="form-control" value="<?php echo $_SESSION['FontColor']; ?>">
                                        <div class="input-group-addon">
                                            <i style="background-color: rgb(0, 0, 0);"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="box-footer">
                    <div class="row">
                        <button type="submit" class="btn btn-primary"><?php echo lang('Simpan'); ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

    <script type="text/javascript">
        $('#province').change(function(){
            var province = $('#province :selected').val();

            $.ajax({
                url : "ajax_profil.php",
                type : 'POST',
                data: { province: province},
                success : function(data) {
                    $("#city").html(data);
                },
                error : function(){
                    alert('<?php echo lang('Silahkan coba lagi'); ?>';
                    return false;
                }
            });
        });
    </script>
<?php require('content-footer.php');?>

<?php require('footer.php');?>