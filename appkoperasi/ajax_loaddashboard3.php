<?php
include "connect.php";

$json = array(
    'status'=>0,
    'hari'=>'',
    'tanggal'=>'',
    'point_bal'=>'',
    'rp_bal'=>'',
    'point'=>'',
    'rp'=>'',
    'point2'=>'',
    'rp2'=>'',
    'current_point'=>'',
    'current_rp'=>'',
    'current_mem_sales'=>'',
    'current_park'=>'',
    'countloc'=>'',
    'namaloc'=>'',
    'membersales'=>'',
    'membersales_rp'=>'',
    'park'=>'',
    'park_rp'=>'',
    'in'=>'',
    'out'=>''

);

$membersales = array();
$membersales_rp = array();
$park = array();
$park_rp = array();
$namaloc = array();
//$namaloc2 = array();
$in = array();
$out = array();

$dmn = date('t M');
$month = date('m');
$yn = date('Y');
$day = date('d');
$dtf = date('Y-m-d');
$hari = date ('l');
$tanggal = date('F jS, Y');

//today point sales Rp
$sql_line1 = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='TOPP' and [Date] between '$dtf 00:00:00' and '$dtf 23:59:59'";
$query1 = sqlsrv_query($conn, $sql_line1);
while($data1 = sqlsrv_fetch_array($query1)) {
 if ($data1[0] == null) {
   $rp = 0; }
 else {$rp =  $data1[0];}
}
$rp2 = $rp;

//membership sales up to yesterday
$sql_line2 = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='MBRS' and [Date] not between '$dtf 00:00:00' and '$dtf 23:59:59'";
$query2 = sqlsrv_query($conn, $sql_line2);
while($data2 = sqlsrv_fetch_array($query2)) {
 if ($data2[0] == null) {
   $rp_mem_sales = 0; }
 else {$rp_mem_sales =  $data2[0];}
}

//parking up to yesterday
$sql_line5 = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='PARK' and [Date] not between '$dtf 00:00:00' and '$dtf 23:59:59'";
$query5 = sqlsrv_query($conn, $sql_line5);
while($data5 = sqlsrv_fetch_array($query5)) {
 if ($data5[0] == null) {
   $rp_park = 0; }
 else {$rp_park =  $data5[0];}
}

//point sales up to yesterday
$sql_line3 = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='TOPP' and [Date] not between '$dtf 00:00:00' and '$dtf 23:59:59'";
$query3 = sqlsrv_query($conn, $sql_line3);
while($data3 = sqlsrv_fetch_array($query3)) {
 if ($data3[0] == null) {
   $rp_point_sales = 0; }
 else {$rp_point_sales =  $data3[0];}
}
$rp_bal = $rp_point_sales - $rp_mem_sales - $rp_park;

$sql_line4 = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='MBRS' and [Date] between '$dtf 00:00:00' and '$dtf 23:59:59'";
$query4 = sqlsrv_query($conn, $sql_line4);
while($data4 = sqlsrv_fetch_array($query4)) {
 if ($data4[0] == null) {
   $rp_current_mem_sales = 0; }
 else {$rp_current_mem_sales =  $data4[0];}
}

$sql_line5 = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='PARK' and [Date] between '$dtf 00:00:00' and '$dtf 23:59:59'";
$query5 = sqlsrv_query($conn, $sql_line5);
while($data5 = sqlsrv_fetch_array($query5)) {
 if ($data5[0] == null) {
   $rp_current_park = 0; }
 else {$rp_current_park =  $data5[0];}
}

$current_rp = $rp_bal + $rp - $rp_current_mem_sales - $rp_current_park;

$sql_rp = "SELECT ValueConversion from [dbo].[ParkingSetting]";
$query_rp = sqlsrv_query($conn, $sql_rp);
$data_rp = sqlsrv_fetch_array($query_rp);
$conv = $data_rp[0];
$point = $rp / $conv;
$point2 = $point;
$point_bal = $rp_bal / $conv;
$current_point = $current_rp / $conv;
$current_park = $rp_current_park / $conv;
$current_mem_sales = $rp_current_mem_sales / $conv;

$sql_lokasi = "Select LocationID,Nama from [dbo].[LocationMerchant] where Status='1' order by LocationID";
$queryLoc = sqlsrv_query($conn, $sql_lokasi);

$sql_loc = "Select Count(*) from [dbo].[LocationMerchant] where Status='1'";
			$queryloc = sqlsrv_query($conn, $sql_loc);
			$i = sqlsrv_fetch_array($queryloc, SQLSRV_FETCH_NUMERIC);

while($dataLoc = sqlsrv_fetch_array($queryLoc)) {

	$linkLoc = '<a href="chart_loc_detail.php?chart=bulan&bulan='.$dmn.'&tahun='.$yn.'&loc='.$dataLoc[0].'" target="_self">'.$dataLoc[1].'</a>';

	array_push($namaloc, $linkLoc);
	//array_push($namaloc2, $linkLoc);

	$sql_membersales = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='MBRS' and AccountKredit='$dataLoc[0]' and [Date] between '$dtf 00:00:00' and '$dtf 23:59:59'";
	$query_membersales = sqlsrv_query($conn, $sql_membersales);
	while($data_membersales = sqlsrv_fetch_array($query_membersales)) {
		if ($data_membersales[0] == null) {
			array_push($membersales, 0);
			array_push($membersales_rp, 0);}
		else {
			array_push($membersales_rp, number_format($data_membersales[0],0,",","."));
			$memsalespoint = $data_membersales[0] / $conv;
			array_push($membersales, number_format($memsalespoint,0,",","."));}
	}

	$sql_park = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='PARK' and AccountKredit='$dataLoc[0]' and [Date] between '$dtf 00:00:00' and '$dtf 23:59:59'";
	$query_park = sqlsrv_query($conn, $sql_park);
	while($data_park = sqlsrv_fetch_array($query_park)) {
		if ($data_park[0] == null) {
			array_push($park, 0);
			array_push($park_rp, 0);}
		else {
			array_push($park_rp, number_format($data_park[0],0,",","."));
			$parkpoint = $data_park[0] / $conv;
			array_push($park, number_format($parkpoint,0,",","."));}
	}

	$sql_in = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$dataLoc[0]' and TimeIn between '$dtf 00:00:00' and '$dtf 23:59:59'";
	$query_in = sqlsrv_query($conn, $sql_in);
	$data_in = sqlsrv_fetch_array($query_in, SQLSRV_FETCH_NUMERIC);
	if ($data_in[0] == null)
		{array_push($in, 0) ; }
	else
		{array_push($in,number_format($data_in[0],0,",",".")) ; }

	$sql_out = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$dataLoc[0]' and TimeOut between '$dtf 00:00:00' and '$dtf 23:59:59'";
	$query_out = sqlsrv_query($conn, $sql_out);
	$data_out = sqlsrv_fetch_array($query_out, SQLSRV_FETCH_NUMERIC);
	if ($data_out[0] == null)
		{array_push($out, 0) ; }
	else
		{array_push($out, number_format($data_out[0],0,",",".")) ; }

}

$json['status'] = 1;
$json['hari'] = $hari;
$json['tanggal'] = $tanggal;
$json['point_bal'] = number_format($point_bal,0,",",".");
$json['rp_bal'] = 'Rp. '.number_format($rp_bal,0,",",".");
$json['point'] = number_format($point,0,",",".");
$json['rp'] = 'Rp. '.number_format($rp,0,",",".");
$json['point2'] = number_format($point,0,",",".");
$json['rp2'] = 'Rp. '.number_format($rp,0,",",".");
$json['current_point'] = number_format($current_point,0,",",".");
$json['current_rp'] = 'Rp. '.number_format($current_rp,0,",",".");
$json['current_park'] = number_format($current_park,0,",",".");
$json['rp_current_park'] = 'Rp. '.number_format($rp_current_park,0,",",".");
$json['current_mem_sales'] = number_format($current_mem_sales,0,",",".");
$json['rp_current_mem_sales'] = 'Rp. '.number_format($rp_current_mem_sales,0,",",".");
$json['countloc'] = $i[0];
$json['namaloc'] = $namaloc;
//$json['namaloc2'] = $namaloc2;
$json['membersales'] = $membersales;
$json['membersales_rp'] = $membersales_rp;
$json['park'] = $park;
$json['park_rp'] = $park_rp;
$json['in'] = $in;
$json['out'] = $out;

echo json_encode($json);
?>
