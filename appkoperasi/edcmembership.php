<?php
@session_start();
error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

// edc list
// where by TOPP
// Select AccountDebet, Sum(Amount),  from Translist > where by TransactionType=TOPP
// 

if( !isset($_GET['tgl']) ) {
	$today = date('Y-m-d');
}
else {
	$today = @$_GET['tgl'];
}

$yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today) ) );
$tomorrow = date('Y-m-d', strtotime('+1 day', strtotime($today) ) );
?>

	<div class="animated fadeinup delay-1">
        <div class="page-content txt-black">

        	

        	<?php
        	if( isset($_GET['det']) ) { 
        		$sdet = "SELECT SerialNumber, UserIDBaronang from [Gateway].[dbo].[EDCList] where KID='700097' and UserIDBaronang='".@$_GET['det']."'";
		        $qdet = sqlsrv_query($conn, $sdet);
		        $ddet = sqlsrv_fetch_array($qdet);

		        $s1 = "SELECT KodeUser, NamaUser from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='".$ddet[1]."'";
		        $q1 = sqlsrv_query($conn, $s1);
		        $d1 = sqlsrv_fetch_array($q1);
        		?>

        		<div class="">
	        		<?php echo '<b>'.date('d F Y', strtotime($today) ).'</b> : '.$d1[1]; ?>
	        	</div>

	        	<div class="table-responsive m-t-30">
	        		<table>

	        			<thead>
	        				<tr>
	        					<th>No. Transaksi</th>
	        					<th>Jam</th>
	        					<th>No. Kartu</th>
	        					<th style="text-align: center;">Amount</th>
	        				</tr>
	        			</thead>

	        			<tbody>
	        				<?php
	        				$total = 0;
	        				$s2 = "SELECT TransNo, Date, AccountDebet, Amount from [dbo].[TransList] where TransactionType = 'Topp' and UserID='".$ddet[0]."' and Date between '$today 00:00:00.000' and '$today 23:59:59.999' order by Date ASC";
	        				$q2 = sqlsrv_query($conn, $s2);
	        				while($d2 = sqlsrv_fetch_array($q2)) { 
	        					$total = $total + $d2[3];
	        					?>
	        					<tr>
	        						<td><?php echo $d1[0]; ?></td>
	        						<td>
	        							<?php
	        							if( $d2[1] == '' || is_null($d2[1]) || empty($d2[1]) ) {
	        								echo '00:00:00';
	        							}
	        							else {
	        								echo $d2[1]->format('h:m:s'); 
	        							}
	        							?>
	        						</td>
	        						<td><?php echo $d2[2]; ?></td>
	        						<td style="text-align: right;"><?php echo number_format($d2[3], 2,".",","); ?></td>
	        					</tr>
	        					<?php
	        				}
	        				?>
	        			</tbody>

	        			<tfoot>
	        				<tr>
	        					<th style="text-align: right;" colspan="3" >Total Rp. </th>
	        					<th style="text-align: right;"><?php echo number_format($total, 2,".",","); ?></th>
	        				</tr>
	        			</tfoot>

	        		</table>
	        	</div>

        		<?php
        	}
        	else { ?>

        		<div class="">
	        		<b><?php echo date('d F Y', strtotime($today) ); ?></b>
	        		<div class="right hide">Receipt Number : xxx</div>
	        	</div>

	        	<div class="table-responsive m-t-30">
	        		<table>
	        			<thead>
	        				<tr class="m-t-30">
	        					<td colspan="0"><h3 class="uppercase" style="color: #000;"> <span id="trans"></span> <br> Transactions</h3></td>
	        					<td colspan="0"><h3 class="uppercase" style="color: #000;"> <span id="tc"></span> <br> Total Collected</h3></td>
	        					<td colspan="0"><h3 class="uppercase" style="color: #000;"> <span id="ns"></span> <br> Net Sales</h3></td>
	        				</tr>
	        				<tr>
	        					<th style="text-align: center;">Cashier</th>
	        					<th style="text-align: center;">Payment Method</th>
	        					<th style="text-align: center;">Total Receivable</th>
	        				</tr>
	        			</thead>

	        			<tbody>
	        				<?php
	        				$rowstrans = 0;
	        				$total = 0;
	        				//echo $today;
	        				$sql = "SELECT SerialNumber, UserIDBaronang from [Gateway].[dbo].[EDCList] where KID='700097'";
	        				$query = sqlsrv_query($conn, $sql);
		        			while($data = sqlsrv_fetch_array($query)) { 
		        				$s1 = "SELECT KodeUser, NamaUser from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='".$data[1]."'";
		        				$q1 = sqlsrv_query($conn, $s1);
		        				$d1 = sqlsrv_fetch_array($q1);

		        				$s2 = 	"SELECT (SELECT SUM(Amount) from [dbo].[TransList] where TransactionType = 'Topp' and UserID='".$data[0]."' and Date between '$today 00:00:00.000' and '$today 23:59:59.999')
		        						 from [dbo].[TransList] where TransactionType = 'Topp' and UserID='".$data[0]."' and Date between '$today 00:00:00.000' and '$today 23:59:59.999'";
		        				$q2 = sqlsrv_query($conn, $s2);
		        				$d2 = sqlsrv_fetch_array($q2);

		        				$total = $total + $d2[0];
		        				?>
		        				<tr>
		        					<td><?php echo $d1[1]; ?></td>
		        					<td>CASH</td>
		        					<td style="text-align: right;"><a href="?tgl=<?php echo $today; ?>&det=<?php echo $data[1]; ?>"> <?php echo number_format($d2[0], 2,".",","); ?> </a></td>
		        				</tr>
		        				<?php
		        				$rowstrans++;
		        			}
	        				?>
	        				
	        			</tbody>

	        			<tfoot>
	        				<tr>
	        					<td colspan="3" style="text-align: left; margin-top: 50px;">
	        						<a href="?tgl=<?php echo $yesterday; ?>" class="btn btn-large waves-effect waves-light primary-color">
	        							<i class="ion-ios-arrow-back"></i>
	        						</a>
	        					</td>
	        					<td colspan="3" style="text-align: right; margin-top: 50px;">
	        						<?php
	        						if( isset($_GET['tgl']) ) { ?>
	        							<a href="?tgl=<?php echo $tomorrow; ?>" class="btn btn-large waves-effect waves-light primary-color">
		        							<i class="ion-ios-arrow-forward"></i>
		        						</a>
	        							<?php
	        						}
	        						?>
	        					</td>
	        				</tr>
	        			</tfoot>
	        		</table>
	        	</div>

	        	<script type="text/javascript">
	        		$('#trans').html('<?php echo $rowstrans; ?>');
	        		$('#tc').html('<?php echo 'Rp. '.number_format($total, 2,".",","); ?>');
	        		$('#ns').html('<?php echo 'Rp. '.number_format($total, 2,".",","); ?>');
	        	</script>

        		<?php
        	}
        	?>
			
		</div>
	</div>

<?php require('footer.php'); ?>