<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ulprocedit = "";
$uldisabled = "";
$uledit = $_GET['edit'];
if(!empty($uledit)){
    $eulsql   = "select * from dbo.LoginList where UserID='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = $eulrow[4];
        $ul5    = $eulrow[5];
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='muserlevel.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='muserlevel.php?page=".$_GET['page']."';</script>";
        }
    }
}
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Pengguna'); ?></h2>

            <form class="form-horizontal" action="proc_museraccount.php<?=$ulprocedit?>" method="POST">

                <div class="input-field">
                    <label for="result-code" class="active"><?php echo lang('User ID Baronang App'); ?></label>
                    <input type="number" name="username" class="validate" id="result-code" value="<?=$ul0?>" <?=$uldisabled?>>
                </div><br>

                <label><?php echo lang('Level pengguna'); ?></label>
                <select name="level" class="browser-default">
                    <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    $julsql   = "select * from [dbo].[UserLevel]";
                    $julstmt = sqlsrv_query($conn, $julsql);
                    while($julrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <option value="<?=$julrow[0];?>" <?php if($julrow[0] == $ul3){echo "selected";}?>><?=$julrow[1];?></option>
                        <?php
                    }
                    ?>
                </select><br>

                <div class="input-field">
                    <?php echo lang('Status'); ?><br>
                    <input type="radio" name="status" class="minimal" id="status0" value="1" <?php if($ul5 == 1){ echo "checked";} ?>>
                    <label for="status0"> <?php echo lang('Aktif'); ?> </label>

                    <input type="radio" name="status" class="minimal" id="status1" value="0" <?php if($ul5 ==  0){ echo "checked";} ?>>
                    <label for="status1"> <?php echo lang('Tidak aktif'); ?> </label>
                </div><br>

                <div class="input-field">
                    <?php echo lang('Persetujuan'); ?><br>
                    <input type="radio" name="approval" class="minimal" id="approval0" value="1" <?php if($ul4 == 1){ echo "checked";} ?>>
                    <label for="approval0"> SPV </label>

                    <input type="radio" name="approval" class="minimal" id="approval1" value="0" <?php if($ul4 ==  0){ echo "checked";} ?>>
                    <label for="approval1"> Staff </label>
                </div>
                <br>
                <div>
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <a href="museraccount.php" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Batal'); ?></a>
                    <?php } ?>
                </div>

                <div class="row">
                    <div class="col">
                      <?php if(count($eulrow[0]) > 0){ ?>
                          <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                      <?php }else{ ?>
                          <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                      <?php } ?>
                    </div>
                    <div class="col">
                      <button type="button" id="ocam" class="waves-effect waves-light btn-large primary-color width-100"><i class="fa fa-camera"></i> Scan QR Code</button>
                    </div>
                </div>
            </form>

            <div class="box hide" id="form-camera">
                <h3 class="uppercase">Scan QR Code/Barcode</h3>

                <div class=""><b>Pilih Kamera</b></div>
                <select class="browser-default" id="camera-select"></select>
                <br>
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <button title="Start Scan QR Barcode" class="waves-effect waves-light btn-large primary-color width-100" id="play" type="button" data-toggle="tooltip">Start</button>
                        </div>

                        <div class="col">
                            <button title="Stop streams" class="waves-effect waves-light btn-large primary-color width-100" id="stop" type="button" data-toggle="tooltip">Stop</button>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="well" style="position: relative;display: inline-block;">
                                <canvas id="webcodecam-canvas"></canvas>
                                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <div class="thumbnail" id="result">
                                <img style="width: 100%;" id="scanned-img" src="">
                                <div class="caption">
                                    <p class="text-center" id="scanned-QR"></p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <?php if($_SESSION['device'] == 0){ ?>
                <script type="text/javascript">
                    $('#ocam').click(function(){
                        Intent.openActivity("QRActivity","museraccount.php");
                      });
                </script>
            <?php } ?>

            <?php if($_SESSION['device'] == 1){ ?>
              <script type="text/javascript">
                  $('#ocam').click(function(){
                      $('#result-code').val();
                      $('#form-camera').removeClass('hide');
                  });

                  $('#stop').click(function(){
                      $('#form-camera').addClass('hide');
                  });
              </script>
            <?php } ?>

        <div class="box box-primary">
            <h3 class="uppercase"><?php echo lang('Daftar pengguna'); ?></h3>
            <!-- /.box-header -->
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class=""><?php echo lang('No'); ?></th>
                        <th class=""><?php echo lang('User ID Baronang App'); ?></th>
                        <th class=""><?php echo lang('Nama'); ?></th>
                        <th class=""><?php echo lang('Level Pengguna'); ?></th>
                        <th class=""><?php echo lang('Status'); ?></th>
                        <th class=""><?php echo lang('Persetujuan'); ?></th>
                        <th class="text-center"><?php echo lang('Aksi'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from dbo.LoginList";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY UserID asc) as row FROM [dbo].[LoginList]) a WHERE row between '$posisi' and '$batas'";
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        //userlevel
                        $rulsql   = "select * from dbo.UserLevel where KodeUserLevel='$ulrow[3]'";
                        $rulstmt  = sqlsrv_query($conn, $rulsql);
                        $rulrow   = sqlsrv_fetch_array( $rulstmt, SQLSRV_FETCH_NUMERIC);
                        ?>
                        <tr>
                            <td><?=$ulrow[6];?></td>
                            <td><?=$ulrow[0];?></td>
                            <td><?=$ulrow[1];?></td>
                            <td><?=$rulrow[1];?></td>
                            <td>
                                <?php
                                if($ulrow[5] == 1){
                                    echo 'Aktif';
                                }
                                else{
                                    echo 'Tidak Aktif';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if($ulrow[4] == 1){
                                    echo 'SPV';
                                }
                                else{
                                    echo 'Staff';
                                }
                                ?>
                            </td>
                            <td width="20%" style="padding: 3px">
                                <div class="btn-group" style="padding-right: 15px">
                                    <a href="museraccount.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                </div>
                                <!--
                              <div class="btn-group">
                                <a href="<?php /*proc_museraccount.php?page=<?=$halaman?>&delete=<?=$ulrow[0]?> */?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
                              </div>
                              -->
                            </td>
                        </tr>
                        <?php
                    }

                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix right">
                <div style="text-align: center;">
                    <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>
                    <?php
                    $reload = "museraccount.php?";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages)."</div>";
                    ?>
                </div>
            </div>
    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

include('footer.php');
?>
