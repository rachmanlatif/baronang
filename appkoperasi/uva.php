<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
require_once 'lib/phpExcel/Classes/PHPExcel/IOFactory.php';
include "connect.php";

if(isset($_FILES['filename'])) {
    $uploads_dir = 'uploads/excel/';
    $bank = $_POST['bank'];
    $date = $_POST['date'];
    $tmp_name = $_FILES["filename"]["tmp_name"];
    $path = $uploads_dir . basename($_FILES["filename"]["name"]);

    if(move_uploaded_file($tmp_name, $path)){
        //upload data from excel
        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getSheet(0);
        } catch(Exception $e) {
            messageAlert('Failed reading excel','warning');
            header('Location: topup_va.php');
        }

        $highestRow = $data->getHighestRow();
        $highestColumn = $data->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        if(!($highestRow -1) >0)
        {
            messageAlert('No data found in Sheet 0 or Sheet not available.','warning');
            header('Location: upmemberlist.php');
        }

        try {
            $vatampung = '';
            if($bank == 'B0080'){
                //cek header
                $rowHeader = $data->rangeToArray('A3:'.$highestColumn.'3',NULL,TRUE,FALSE);

                $transno = $rowHeader[0][0];
                $transdate = $rowHeader[0][1];
                $description = $rowHeader[0][2];
                $transcode = $rowHeader[0][3];
                $detail = $rowHeader[0][4];
                $debet = $rowHeader[0][5];
                $credit = $rowHeader[0][6];

                //filter jika format tidak sesuai
                if($transno != 'TransactionReferenceNumber' or $transdate != 'TransactionDate' or $description != 'Description' or $transcode != 'Transaction Code' or $detail != 'Detail' or $debet != 'DebitAmount' or $credit != 'CreditAmount'){
                    messageAlert('Format template tidak sesuai Bank Sinarmas','info');
                    header('Location: topup_va.php');
                }

                $no = 1;
                //baca dari row 4
                for($row=4; $row<=$highestRow; ++$row) {

                    $rowData = $data->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
                    $transno2 = $rowData[0][0];
                    $transdate2 = $rowData[0][1];
                    $description2 = $rowData[0][2];
                    $transcode2 = $rowData[0][3];
                    $detail2 = $rowData[0][4];
                    $va = '8308'.$detail2;
                    $debet2 = $rowData[0][5];
                    $credit2 = $rowData[0][6];

                    $a = "select * from dbo.VaMember where VaNumber='$va'";
                    $b = sqlsrv_query($conn, $a);
                    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
                    if($c != null){
                        $aa = "select * from dbo.VaUpload where TransactionNumber='$transno2'";
                        $bb = sqlsrv_query($conn, $aa);
                        $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
                        if($cc == null){
                            if($debet2 == '' and $credit2 >= 0 or $debet2 <= 0 and $credit2 >= 0){
                                $sql = "exec dbo.ProsesUploadVa '$transno2','$date','$bank','$description2','$va','$credit2'";
                                $exec = sqlsrv_query($conn, $sql);
                                if($exec){
                                    $vatampung.='Kode transaksi '.$transno2.' dengan nomor kartu 700097'.$detail2.' berhasil upload<br>';
                                }
                                else{
                                    $vatampung.='<span style="color:red;">Terjadi kesalahan upload kode transaksi '.$transno2.' dengan nomor kartu 700097'.$detail2.'</span><br>';
                                }
                            }
                            else{
                                $vatampung.='<span style="color:red;">Terjadi kesalahan format excel dengan nomor kartu 700097'.$detail2.'</span><br>';
                            }
                        }
                        else{
                            $vatampung.='<span style="color:red;">Kode transaksi '.$transno2.' dengan nomor kartu 700097'.$detail2.' sudah di upload</span><br>';
                        }
                    }
                    else{
                        $vatampung.='<span style="color:red;">Nomor kartu 700097'.$detail2.' belum terdaftar virtual account</span><br>';
                    }
                }
            }
            else{
              messageAlert('Hanya dapat menerima Bank Sinarmas saja','info');
              header('Location: topup_va.php');
            }

            messageAlert('Berhasil upload data: <br>'.$vatampung,'success');
            header('Location: topup_va.php');

        } catch(Exception $e) {
            messageAlert('Gagal upload excel','danger');
            header('Location: topup_va.php');
        }
    }
    else{
        messageAlert('Gagal upload file','danger');
        header('Location: topup_va.php');
    }
}

?>
