<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
include "connectinti.php";

//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ulprocedit = "";
$uldisabled = "";
$uldisabled2 = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.BankAccount where KodeKoperasiBankAccount='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = $eulrow[4];
        $ul5    = substr($eulrow[5],0,-5);
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul1;
        $uldisabled = "readonly";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='bank_acc.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='bank_acc.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Akun bank'); ?></h2>

        <!-- /.box-header -->
            <form class="form-horizontal" action="procbank_acc.php<?=$ulprocedit?>" method = "POST">

                <label><?php echo lang('Nama bank'); ?></label>
                <select name="kode" class="browser-default select2">
                    <option disabled>- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    $julsql   = "select * from [dbo].[BankList] order by NamaBank";
                    $julstmt = sqlsrv_query($conns, $julsql);
                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                        $selected = '';
                        if($rjulrow[0] == $ul2){
                            $selected = 'selected';
                        }
                        ?>
                        <option value="<?=$rjulrow[0];?>" <?php echo $selected; ?>><?=$rjulrow[1];?></option>
                    <?php } ?>
                </select>

                <div class="input-field">
                    <input type="number" name="acc" class="validate" id="cooperativebankaccount" value="<?=$ul3;?>">
                    <label for="cooperativebankaccount"><?php echo lang('Nomor akun'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="accn" class="validate" id="cooperativebankaccount" value="<?=$ul4;?>">
                    <label for="cooperativebankaccount"><?php echo lang('Nama Akun'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="bal" class="validate price" id="cooperativebankaccount" value="<?=$ul5;?>" <?=$uldisabled2?>>
                    <label for="cooperativebankaccount"><?php echo lang('Saldo'); ?></label>
                </div>
                <br>
                <div>
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                    <?php }else{ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                    <?php } ?>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <a href="bank_acc.php" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Batal'); ?></a>
                    <?php } ?>
                </div>

            </form>

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Daftar akun bank'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table border="1" class="table table-striped">
                        <thead>
                        <tr>
                            <th><?php echo lang('No'); ?></th>
                            <th><?php echo lang('Kode akun bank'); ?></th>
                            <th><?php echo lang('Nama bank'); ?></th>
                            <th><?php echo lang('Nomor akun'); ?></th>
                            <th><?php echo lang('Nama akun'); ?></th>
                            <th><?php echo lang('Saldo'); ?></th>
                            <th><?php echo lang('Aksi'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //count
                        $jmlulsql   = "select count(*) from dbo.BankAccountView";
                        $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        }
                        else{
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KID asc) as row FROM [dbo].[BankAccountView]) a WHERE KID='$_SESSION[KID]' and row between '$posisi' and '$batas'";
                        $ulstmt = sqlsrv_query($conn, $ulsql);
                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?=$ulrow[7];?></td>
                                <td><?=$ulrow[1];?></td>
                                <td><?=$ulrow[2];?></td>
                                <td><?=$ulrow[4];?></td>
                                <td><?=$ulrow[5];?></td>
                                <td><?=number_format($ulrow[6]);?></td>
                                <td width="20%" style="padding: 3px">
                                    <div class="btn-group" style="padding-right: 15px">
                                        <a href="bank_acc.php?page=<?=$halaman?>&edit=<?=$ulrow[1]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
                <div class="box-footer clearfix right">
                    <div style="text-align: center;">
                        <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>
                        <?php
                        $reload = "bank_acc.php?";
                        $page = intval($_GET["page"]);
                        $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                        if( $page == 0 ) $page = 1;

                        $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                        echo "<div>".paginate($reload, $page, $tpages)."</div>";
                        ?>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

include('footer.php');
?>
