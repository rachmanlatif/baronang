<!-- Left Sidebar -->
<ul id="slide-out-left" class="side-nav collapsible">
    <li>
        <div class="sidenav-header primary-color">
            <?php
            $Logo = $_SESSION['Logo'];
            if($Logo == ''){
                $Logo = 'static/images/yourlogohere.png';
            }
            ?>

            <div class="nav-avatar">
                <img class="circle avatar" src="<?php echo $Logo; ?>" alt="">
                <div class="avatar-body">
                    <b style="color: white;"><?php echo ucfirst($NameUser); ?></b>
                    <p>Online</p>
                </div>
            </div>

        </div>
    </li>
    <li><a href="dashboard.php" class="no-child">Dashboard</a></li>

    <?php
    //hak akses
    $w = array();
    $aaa = "select * from [dbo].[UserLevelMenu] where KodeUserLevel = '$JabatanUser'";
    $bbb = sqlsrv_query($conn, $aaa);
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        array_push($w, $ccc[2]);
    }

    //table menu header
    if($_SESSION['device'] == 0){
        $x = "select * from [dbo].[MenuHeader] where Status = 1 and Nama not in('Import','Laporan') order by HeaderNo ASC";
    }
    else{
      $x = "select * from [dbo].[MenuHeader] where Status = 1 order by HeaderNo ASC";
    }
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
        ?>
        <li class="clcolor">
            <div class="collapsible-header clcolor">
                <?php echo $z[1]; ?>
            </div>

            <div class="collapsible-body clcolor">
                <ul class="collapsible clcolor">
                    <?php
                    //table menu detail
                    $xx = "select * from [dbo].[MenuDetail] where HeaderNo = '$z[0]' and Status = 1 and isVisible = 1 order by DetailNo ASC";
                    $yy = sqlsrv_query($conn, $xx);
                    while($zz = sqlsrv_fetch_array( $yy, SQLSRV_FETCH_NUMERIC)) {
                        //cek hak akses
                        if (in_array($zz[3], $w) or $zz[5] == 1) {
                            ?>
                            <li>
                                <a href="<?php echo $zz[3]; ?>"><i class="<?php echo $zz[6]; ?>"></i> <?php echo lang($zz[2]); ?> </a>
                            </li>
                        <?php
                        }
                    } ?>
                </ul>
            </div>
        </li>
        <?php
    } ?>
</ul>
<!-- End of Sidebars -->

<!-- Page Content -->
<div id="content" class="page">

    <!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <div class="open-left" id="open-left" data-activates="slide-out-left">
            <i class="ion-android-menu"></i>
        </div>
        <span class="title"> <?php echo $_SESSION['NamaKoperasi']; ?> </span>
        <div class="open-right" id="open-right" data-activates="slide-out">
            <i class="ion-android-person"></i>
        </div>
    </div>
