<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php
include "connectinti.php";

$abc = "select top 1 * from [dbo].[Profil]";
$def = sqlsrv_query($conn, $abc);
$ghi = sqlsrv_fetch_array( $def, SQLSRV_FETCH_NUMERIC);

$qwe = "select top 1 * from [dbo].[CardDesign]";
$asd = sqlsrv_query($conn, $qwe);
$zxc = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC);
if($zxc != null){
    $_SESSION['FontColor'] = $zxc[1];
}
else{
    $_SESSION['FontColor'] = '#000000';
}
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Profil koperasi'); ?></h2><br>
      </div>
            <form class="form-horizontal" action="proc_mprofile.php" method="POST" enctype="multipart/form-data">
                <ul class="faq collapsible animated fadeinright delay-1" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Profil'); ?> </div>
                        <div class="collapsible-body">

                            <div style="padding: 10px;">

                                <div class="input-field">
                                    <input type="text" name="kname" class="validate" id="kname" value="<?php echo $ghi[1]; ?>">
                                    <label for="kname"><?php echo lang('Nama Koperasi'); ?></label>
                                </div>

                                <div class="input-field">
                                    <input type="text" name="knick" class="validate" id="knick" value="<?php echo $ghi[2]; ?>">
                                    <label for="knick"><?php echo lang('Nama Singkat Koperasi'); ?></label>
                                    *Nama singkat koperasi digunakan untuk nama di Kartu Anggota Koperasi
                                </div>

                                <br>

                                <div class="input-field">
                                    <input type="email" name="email" class="validate" id="email" value="<?php echo $ghi[0]; ?>">
                                    <label for="email"><?php echo lang('Email'); ?></label>
                                </div>

                                <div class="input-field">
                                    <?php echo lang('Pilih kembali untuk mengganti file'); ?><br>

                                    <label></label><?php echo lang('Logo'); ?></label>
                                    <input type="file" name="logo" class="validate" id="logo">
                                </div>

                                <br>

                                <div class="input-field">
                                    <label></label>
                                    <img src="<?php echo $ghi[6]; ?>" class="img-responsive">
                                </div>

                                <div class="input-field">
                                    <input type="number" name="kphone" class="validate" id="kphone" value="<?php echo $ghi[7]; ?>">
                                    <label for="kphone"><?php echo lang('Telepon'); ?></label>
                                </div>

                                <div class="input-field">
                                    <label><?php echo lang('Alamat'); ?></label><br>
                                    <textarea name="address" class="validate" id="address"><?php echo $ghi[3]; ?></textarea>
                                </div><br>

                                <label><?php echo lang('Provinsi'); ?></label>
                                <select name="province" class="browser-default select2" id="province" required="">
                                    <option value="">- Select -</option>
                                    <?php
                                    $a = "select * from [dbo].[Provinsi]";
                                    $b = sqlsrv_query($conns, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        if($ghi[4] == $c[0]){
                                            ?>
                                            <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                        <?php } } ?>
                                </select><br>

                                <label> <?php echo lang('Kota'); ?> </label>
                                <select name="city" class="browser-default select2" required="">
                                    <option value="">- Select -</option>
                                    <?php
                                    $a = "select * from [dbo].[Kota] where KodeProvinsi = '$ghi[4]'";
                                    $b = sqlsrv_query($conns, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        if($ghi[5] == $c[0]){
                                            ?>
                                            <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                        <?php } } ?>
                                </select>

                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header"><i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Dokumen'); ?> </div>
                        <div class="collapsible-body">

                            <div class="input-field" style="margin-bottom: 10px; padding: 10px;">
                                <?php echo lang('Ekstensi file'); ?> <br>
                                .jpg, .png, .pdf, .jpeg
                            </div>

                            <?php
                            $count = 0;
                            $a = "select * from [dbo].[JenisDoc]";
                            $b = sqlsrv_query($conns, $a);
                            while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                ?>

                                <div style="padding: 10px;">
                                    <?php echo $c[1]; ?></label> <br>
                                    <?php
                                    $aa = "select * from [dbo].[UserRegisterDoc] where KodeJenisDoc='$c[0]'";
                                    $bb = sqlsrv_query($conns, $aa);
                                    $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                    if($cc != null){
                                        ?>
                                        <a href="<?php echo $cc[2]; ?>" target="_blank"><?php echo lang('Klik untuk melihat file'); ?></a>
                                        <br>
                                        <?php echo lang('Pilih kembali untuk mengganti file'); ?>
                                    <?php } ?>

                                    <input type="hidden" name="jenisdoc[]" class="validate" value="<?php echo $c[0]; ?>" readonly>
                                    <input type="file" name="filename[]" class="validate" accept=".jpg,.png,.pdf,.jpeg"><br>
                                </div>

                                <?php $count++; } ?>

                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header"><i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Design Kartu Member'); ?> </div>
                        <div class="collapsible-body" style="padding: 10px;">

                            <div style="margin-bottom: 10px;">
                                <?php echo lang('Pilih Design'); ?>
                            </div>
                            <?php
                            $img='';
                            $img1='';
                            $img2='';
                            $img3='';
                            $img4='';
                            $img5='';
                            $img6='';

                            $cimg='';
                            $cimg1='';
                            $cimg2='';
                            $cimg3='';
                            $cimg4='';
                            $cimg5='';
                            $cimg6='';
                            if($zxc[0] == 'card/card0.png'){
                                $img='active';
                                $cimg='checked';
                            }
                            else if($zxc[0] == 'card/card1.png'){
                                $img1='active';
                                $cimg1='checked';
                            }
                            else if($zxc[0] == 'card/card2.png'){
                                $img2='active';
                                $cimg2='checked';
                            }
                            else if($zxc[0] == 'card/card3.png'){
                                $img3='active';
                                $cimg3='checked';
                            }
                            else if($zxc[0] == 'card/card4.png'){
                                $img4='active';
                                $cimg4='checked';
                            }
                            else if($zxc[0] == 'card/card5.png'){
                                $img5='active';
                                $cimg5='checked';
                            }
                            else if($zxc[0] == 'card/'.$_SESSION['KID'].'_card.png'){
                                $img6='active';
                                $cimg6='checked';
                            }
                            ?>

                            <ul class="faq collapsible animated fadeinright delay-1" data-collapsible="accordion">
                                <li>
                                    <div class="collapsible-header">
                                        <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Default'); ?>
                                    </div>
                                    <div class="collapsible-body">

                                        <input type="radio" name="design" id="design0" value="card/card0.png" <?php echo $cimg; ?>>
                                        <label for="design0">Pilih default</label>

                                        <div class="input-field">
                                            <img src="card/card0.png" class="img-responsive">

                                            <?php if($_SESSION['Logo'] != ''){ ?>
                                                <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                            <?php } ?>

                                            <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                            <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                        </div>

                                    </div>
                                </li>

                                <li>
                                    <div class="collapsible-header">
                                        <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('1'); ?>
                                    </div>
                                    <div class="collapsible-body">

                                        <input type="radio" name="design" id="design1" value="card/card1.png" <?php echo $cimg1; ?>>
                                        <label for="design1">Pilih design 1</label>

                                        <div class="input-field">
                                            <img src="card/card1.png" class="img-responsive">

                                            <?php if($_SESSION['Logo'] != ''){ ?>
                                                <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                            <?php } ?>

                                            <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                            <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                        </div>

                                    </div>
                                </li>

                                <li>
                                    <div class="collapsible-header">
                                        <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('2'); ?>
                                    </div>
                                    <div class="collapsible-body">

                                        <input type="radio" name="design" id="design2" value="card/card2.png" <?php echo $cimg2; ?>>
                                        <label for="design2">Pilih design 2</label>

                                        <div class="input-field">
                                            <img src="card/card2.png" class="img-responsive">

                                            <?php if($_SESSION['Logo'] != ''){ ?>
                                                <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                            <?php } ?>

                                            <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                            <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                        </div>

                                    </div>
                                </li>

                                <li>
                                    <div class="collapsible-header">
                                        <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('3'); ?>
                                    </div>
                                    <div class="collapsible-body">

                                        <input type="radio" name="design" id="design3" value="card/card3.png" <?php echo $cimg3; ?>>
                                        <label for="design3">Pilih design 3</label>

                                        <div class="input-field">
                                            <img src="card/card3.png" class="img-responsive">

                                            <?php if($_SESSION['Logo'] != ''){ ?>
                                                <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                            <?php } ?>

                                            <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                            <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                        </div>

                                    </div>
                                </li>

                                <li>
                                    <div class="collapsible-header">
                                        <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('4'); ?>
                                    </div>
                                    <div class="collapsible-body">

                                        <input type="radio" name="design" id="design4" value="card/card4.png" <?php echo $cimg4; ?>>
                                        <label for="design4">Pilih design 4</label>

                                        <div class="input-field">
                                            <img src="card/card4.png" class="img-responsive">

                                            <?php if($_SESSION['Logo'] != ''){ ?>
                                                <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                            <?php } ?>

                                            <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                            <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                        </div>

                                    </div>
                                </li>

                                <li>
                                    <div class="collapsible-header">
                                        <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('5'); ?>
                                    </div>
                                    <div class="collapsible-body">

                                        <input type="radio" name="design" id="design5" value="card/card5.png" <?php echo $cimg5; ?>>
                                        <label for="design5">Pilih design 5</label>

                                        <div class="input-field">
                                            <img src="card/card5.png" class="img-responsive">

                                            <?php if($_SESSION['Logo'] != ''){ ?>
                                                <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                            <?php } ?>

                                            <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                            <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                            <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                        </div>

                                    </div>
                                </li>

                                <li>
                                    <div class="collapsible-header">
                                        <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Upload Design'); ?>
                                    </div>
                                    <div class="collapsible-body">

                                        <?php
                                        //Image Locations
                                        $image_card = 'card/'.$_SESSION['KID'].'_card.png';
                                        $image_card2 = 'card/'.$_SESSION['KID'].'_card.jpg';
                                        if(file_exists($image_card)){
                                            $link = $image_card;
                                        }
                                        else if(file_exists($image_card2)){
                                            $link = $image_card2;
                                        }
                                        else{
                                            $link = '';
                                        }

                                        if($link != ''){ ?>

                                            <input type="radio" name="design" id="design6" value="<?php echo $link; ?>" <?php echo $cimg6; ?>>
                                            <label for="design6">Pilih upload design</label>

                                            <div class="input-field">
                                                <img src="<?php echo $link; ?>" class="img-responsive">

                                                <?php if($_SESSION['Logo'] != ''){ ?>
                                                    <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                                                <?php } ?>

                                                <span class="lead" style="top: 5%;right: 8%;position: absolute;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['NamaKoperasi']; ?></span>
                                                <span class="lead" style="position: absolute;bottom: 0;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['KID'].' 1234'; ?></span>
                                                <span class="lead" style="position: absolute;bottom: 8%;left: 8%;color: <?php echo $_SESSION['FontColor']; ?>;"><?php echo $_SESSION['Name']; ?></span>
                                                <img src="card/qrcode.jpg" style="width: 15%;bottom: 8%;right: 8%;position: absolute;">
                                            </div>

                                            <br>
                                        <?php } ?>

                                        <div style="margin-left: 10px;">
                                            <div class="input-field">
                                                <?php echo lang('Pilih gambar'); ?><br>
                                                <input type="file" name="path" class="validate">
                                            </div>
                                            <br>
                                            <div class="input-field">
                                                <input type="text" name="color" class="validate" id="color" value="<?php echo $_SESSION['FontColor']; ?>">
                                                <label for="color"><?php echo lang('Warna Huruf'); ?></label>

                                                <div class="input-group colorpicker colorpicker-element">
                                                    <div class="input-group-addon">
                                                        <i style="background-color: rgb(0, 0, 0);"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </li>
                            </ul>

                        </div>
                    </li>
                </ul>

                <div class="page-content">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                  </div>

            </form>
</div>

<script type="text/javascript">
    $('#province').change(function(){
        var province = $('#province :selected').val();

        $.ajax({
            url : "ajax_profil.php",
            type : 'POST',
            data: { province: province},
            success : function(data) {
                $("#city").html(data);
            },
            error : function(){
                alert('<?php echo lang('Silahkan coba lagi'); ?>');
                return false;
            }
        });
    });
</script>

<?php require('footer_new.php');?>
