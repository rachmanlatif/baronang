<?php require('header.php');?>      

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if(isset($_GET['memberid'])){
    $memberid=$_GET['memberid'];

    $xxxs  = "select * from [dbo].[BasicSavReport] where MemberID='$memberid'";
    $xxxxs  = sqlsrv_query($conn, $xxxs);
    while($xxs   = sqlsrv_fetch_array( $xxxxs, SQLSRV_FETCH_NUMERIC)){
        $names = $xxs[1];
        $_SESSION ['Namex']	= $names;
    }

}

if(isset($_GET['loannum'])){
    $loannum=$_GET['loannum'];

    $a = "select * from [dbo].[LoanReleaseView] where LoanAppNum='$loannum'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        //proses loan service
        $sql = "exec dbo.ProsesLoanService '".$_SESSION['UserID']."','LNBL','$c[2]'";
        $stmt = sqlsrv_query($conn, $sql);
        sqlsrv_execute($stmt);

        $pp = 0;
        $ip = 0;
        $aa = "select * from [dbo].[LoanTransaction] where LoanNumber='$loannum' and KodeTransactionType='LNBL'";
        $bb = sqlsrv_query($conn, $aa);
        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
            $ttp = $cc[6] - $cc[8];
            $pp+=$ttp;

            $iip = $cc[7] - $cc[9];
            $ip+=$iip;
        }
    }
}
?>
    <script language="javascript">
        function load_page(){
            var e = document.getElementById("csoaregsavmember").value;
            var a = document.getElementById("loan").value;

            window.location.href = "dep_loan_pay.php?memberid=" + e + "&loannum=" + a;
        };
    </script>

		<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Loan Payment Cashier</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="procdep_loan_pay.php" method = "POST">
              <div class="box-body">
				<div class="row">
					<div class="col-sm-6">
							<div class="form-group">
								<label for="csoaregsavmember" class="col-sm-2 control-label" style="text-align: left;">Member ID</label>
								<div class="col-sm-6">
								<input type="text" name="member" class="form-control" onblur="load_page();" id="csoaregsavmember" placeholder="" value="<?=$memberid?>" required="">
								</div>
							</div>
						  <div class="form-group">
							<label for="nama" class="col-sm-2 control-label" style="text-align: left;">Name</label>
							<div class="col-sm-6">
							  <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?=$names?>" disabled>
							</div>
						  </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align: left;">Loan Number</label>
							<div class="col-sm-6">
							<input type="text" name="loan" class="form-control" onblur="load_page();" id="loan" placeholder="" value="<?=$loannum?>" required="">
							</div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align: left;">Principal Payment</label>
							<div class="col-sm-6">
							<input type="text" name="pp" class="form-control" placeholder="" required="" value="<?= $pp; ?>">
							</div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label" style="text-align: left;">Interest Payment</label>
							<div class="col-sm-6">
							<input type="text" name="ip" class="form-control" placeholder="" required="" value="<?= $ip; ?>">
							</div>
                      </div>
						<div class="col-md-12">
						<br>
						<button type="submit" class="btn btn-info pull-left">Save</button>
				</div>
              </div>
			  </div>
			  </div>
<?php  require('content-footer.php');?>
<?php  require('footer.php');?>