<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('sidebar-right.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ulprocedit = "";
$uldisabled = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.SaldoAwalView where KodeAccount='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul3    = $eulrow[3];
        $ul4    = $eulrow[4];
        $ul1    = $eulrow[2];
        $ulprocedit = "?page=".$_GET['page']."&edit=".$uledit;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='saldoawal.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='saldoawal.php?page=".$_GET['page']."';</script>";
        }
    }
}
?>

<script type="text/javascript">
    var totdebet = 0;
    var totkredit = 0;

    function sum(idx) {
        var num1 = $(idx).val();//document.getElementById(idx).val;
        totdebet = parseInt(totdebet) + parseInt(num1);
        //document.getElementById('htotdebet').html = totdebet;
        $('#htotdebet').html(totdebet);
        if (!isNaN(totdebet)) {
            //document.getElementById('htotdebet').html = totdebet;
            //totdebet = parseInt(totdebet) - parseInt(num1);
            $('#htotdebet').html(totdebet);
        }
    }

    function addCommas(nStr) {
      nStr += '';
      var comma = /,/g;
      nStr = nStr.replace(comma,'');
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
    }
</script>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <div class="table-responsive p-10">
            <!--<form class="form-horizontal" action="procsaldoawal.php<?=$ulprocedit?>" method="POST">-->
            <form class="form-horizontal" action="" method="POST">
                <div class="row">
                    <div class="col s6">
                        <?php
                            if(isset($_POST['bulan'])){
                                $from = $_POST['bulan'];
                            }
                            else{
                                $from = date('Y/m/d');
                            }
                            ?>
                            <select id="bulan" name="bulan" class="browser-default">
                                <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['bulan']; ?>">- <?php echo lang('Pilih Bulan'); ?> -</option>
                                <?php
                                $bln=array(01=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
                                echo $bln;
                                $from='';
                                for ($bulan=01; $bulan<=12; $bulan++) { 
                                     if ($_POST['bulan'] == $bulan){
                                        $ck="selected";
                                     }   else {
                                        $ck="" ;
                                     }
                                    echo "<option value='$bulan' $ck>$bln[$bulan]</option>";
                                }
                                ?>
                            </select>
                    </div>
                    <div class="col s6">
                        <?php
                            if(isset($_POST['tahun'])){
                                $to = $_POST['tahun'];
                            }
                            else{
                                $to = date('Y/m/d');
                            }
                            ?>
                            <select id="tahun" name="tahun" class="browser-default">
                                <option type="text" name="tahun" id="tahun" value="<?php echo $_POST['tahun']; ?>">- <?php echo lang('Pilih Tahun'); ?> -</option>
                                <?php
                                $now=date("Y");
                                for ($tahun=2017; $tahun<=$now; $tahun++) { 
                                     if ($_POST['tahun'] == $tahun){
                                        $ck="selected";
                                     }   else {
                                        $ck="" ;
                                     }
                                    echo "<option value='$tahun' $ck>$tahun</option>"; 
                                }

                                ?>
                            </select>
                    </div>
                    <!-- <div class="col s6">
                        <?php
                        $BUL = array("FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");
                        $BULlen = count($BUL);
                        $BULAN = array("Februari", "Maret", "April", "May", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                        $BULANlen = count($BULAN);
                        ?>
                        <span>Bulan</span> : <br>
                        <select class="browser-default" id="bulan" name="bulan">
                            <option value="<?php echo date('t M', strtotime("NOV+1 Month")); ?>">Januari</option>
                            <?php
                            $ibul = 1;
                            for($x = 0; $x < $BULlen; $x++) { ?>
                                <option value="<?php echo date('t M', strtotime($BUL[$x].date("Y")."-1 Month")); ?>"><?php echo $BULAN[$x]; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="col s6">
                        <span>Tahun</span> : <br>
                        <select class="browser-default" id="tahun" name="tahun">
                            <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                        </select>
                    </div> -->
                </div>
                

                <br> <br>
                <table>
                    <tr>
                        <th class="center">No. Rekening</th>
                        <th class="center">Nama Rekening</th>
                        <th class="center">Debet</th>
                        <th class="center">Kredit</th>
                    </tr>

                    <?php
                    $x = 0;
                    //$sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,3) not in('1.1.01') order by KodeAccount ASC";
                    $sql = "SELECT * FROM dbo.AccountMax where substring(KodeAccount,1,1) != 4 and substring(KodeAccount,1,1) != 5 and substring(KodeAccount,1,1) != 6 order by KodeAccount ASC";
                    $query = sqlsrv_query($conn, $sql);
                    while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                        <tr>
                            <td>
                                <input type="text" name="kba[]" id="kba1<?php echo $x; ?>" value="<?php echo $row[0] ;?>" readonly>
                            </td>
                            <td>
                                <input type="text" name="narek[]" id="narek-<?php echo $x; ?>" value="<?php echo $row[1]; ?>" readonly>
                            </td>
                            <td>
                                <input type="hidden" name="debet[]" id="hdebit<?php echo $x; ?>" value="0" readonly>
                                <input type="text" id="debit<?php echo $x; ?>" style="text-align: right;" placeholder="0">
                            </td>
                            <td>
                                <input type="hidden" name="kredit[]" id="hkredit<?php echo $x; ?>" value="0" readonly>
                                <input type="text" id="kredit<?php echo $x; ?>" style="text-align: right;" placeholder="0">
                            </td>
                        </tr>

                        <script type="text/javascript">
                            $('#debit<?php echo $x; ?>').keyup(function(){
                                var total = 0;

                                //$('#debit<?php echo $x; ?>').val($('#debit<?php echo $x; ?>').toLocaleString());
                                var amount = $('#debit<?php echo $x; ?>').val();
                                //$('#debit<?php echo $x; ?>').val(price(amount));
                                $("#debit<?php echo $x; ?>").val(amount.toLocaleString());

                                var amounth = $('#hdebit<?php echo $x; ?>').val();
                                var totdebit = $('#totdebit').val();
                                if(amount > 0){
                                    var amounth2 = $('#hdebit<?php echo $x; ?>').val();

                                    total = (parseFloat(totdebit) - parseFloat(amounth2)) + parseFloat(amount);
                                    $('#totdebit').val(total);

                                    $('#hdebit<?php echo $x; ?>').val(amount);
                                    $('#kredit<?php echo $x; ?>').prop('readonly', 'disabled');
                                    $('#kredit<?php echo $x; ?>').val('0');
                                }

                                if(amount == 0){
                                    var amounth = $('#hdebit<?php echo $x; ?>').val();
                                    var totdebit = $('#totdebit').val();

                                    total = parseFloat(totdebit) - parseFloat(amounth);
                                    $('#totdebit').val(total);

                                    $('#hdebit<?php echo $x; ?>').val(amount);
                                    $('#kredit<?php echo $x; ?>').prop('readonly', false);
                                }

                                if(amount.length == 0){
                                    $('#hdebit<?php echo $x; ?>').val(0);
                                    $('#kredit<?php echo $x; ?>').prop('readonly', false);
                                }

                                selisih();
                            });

                            $('#kredit<?php echo $x; ?>').keyup(function(){
                                var total = 0;
                                var amount = $('#kredit<?php echo $x; ?>').val();

                                var amounth = $('#hkredit<?php echo $x; ?>').val();
                                var totkredit = $('#totkredit').val();
                                if(amount > 0){
                                    var amounth2 = $('#hkredit<?php echo $x; ?>').val();

                                    total = (parseFloat(totkredit) - parseFloat(amounth2)) + parseFloat(amount);
                                    $('#totkredit').val(total);

                                    $('#hkredit<?php echo $x; ?>').val(amount);

                                    $('#debit<?php echo $x; ?>').prop('readonly', 'disabled');
                                    $('#debit<?php echo $x; ?>').val('0');
                                }

                                if(amount == 0){
                                    var amounth = $('#hkredit<?php echo $x; ?>').val();
                                    var totkredit = $('#totkredit').val();

                                    total = parseFloat(totkredit) - parseFloat(amounth);
                                    $('#totkredit').val(total.toLocaleString());

                                    $('#hkredit<?php echo $x; ?>').val(amount);

                                    $('#debit<?php echo $x; ?>').prop('readonly', false);
                                }

                                if(amount.length == 0){
                                    $('#hkredit<?php echo $x; ?>').val(0);

                                    $('#kredit<?php echo $x; ?>').prop('readonly', false);
                                }

                                selisih();
                            });

                            function selisih(){
                                var a = $('#totdebit').val();
                                var b = $('#totkredit').val();

                                var selisih = 0;
                                sisa = parseFloat(a) - parseFloat(b);
                                if(sisa < 0){
                                    selisih = sisa * -1;
                                }
                                else{
                                    selisih = sisa;
                                }

                                //var kata = '';
                                if(selisih == 0){
                                    //kata = 'Simpan';
                                    $('#btnok').prop('disabled', false);
                                }
                                else{
                                    //kata = 'Selisih '+selisih;
                                    $('#btnok').prop('disabled', true);
                                }

                                //$('#kata').html(kata);
                            }
                        </script>
                        <?php
                        $x++;
                    }
                    ?>

                    <tr>
                        <td colspan="2"></td>
                        <td>
                            Total Debet:<br>
                            <input type="text" name="totdebit" id="totdebit" class="validate" value="0">
                        </td>
                        <td>
                            Total Kredit:<br>
                            <input type="text" name="totkredit" id="totkredit" class="validate" value="0">
                        </td>
                    </tr>

                </table>

                <button type="submit" name="ok" id="btnok" class="btn btn-large primary-color width-100 waves-effect waves-light" disabled>Simpan</button>
            </form>
        
            <?php
            $ok = @$_POST['ok'];
            if(isset($ok)) {
                $x = 0;
                foreach (@$_POST['debet'] as $key) {
                    $kba = @$_POST['kba'][$x];
                    $narek = @$_POST['narek'][$x];
                    $debet = @$_POST['debet'][$x];
                    $kredit = @$_POST['kredit'][$x];
                    $from1 = @$_POST['tahun'].'/'.@$_POST['bulan'].'/01';
                    $from = date('Y-m-01', strtotime($from1));

                    if( $debet > 0 || $kredit > 0 ){

                        $sqlh = "SELECT * from Account where KodeAccount='$kba'";
                        $queryh = sqlsrv_query($conn, $sqlh);
                        $header = 0;
                        while ($datah = sqlsrv_fetch_array($queryh, SQLSRV_FETCH_NUMERIC)) {
                            $kt = $datah[2];
                            $group = "select * from dbo.typeacc where KodeTipe = '$datah[2]'";
                            $pgroup = sqlsrv_query($conn, $group);
                            $hgroup = sqlsrv_fetch_array($pgroup, SQLSRV_FETCH_NUMERIC);
                            $hsgroup = $hgroup[2];

                            if ($datah[3] == '0'){
                               $header = 3; 
                            } else if ($datah[3] == '1'){
                                $header = 4;
                            } else {
                                $header = 5;
                            }
                        }

                       

                        $sql = "INSERT into AccountBalance(KodeAccount, NamaAccount, KodeGroup, KodeTipe, Header, Debet, Kredit, Tanggal, Keterangan, Balance) values('$kba','$narek', '$hsgroup', '$kt','$header','$debet','$kredit','$from','Saldo Awal',0)";
                        //echo '#no-'.$x.':'.$sql.'<br><br>'; 
                        $query = sqlsrv_query($conn, $sql);
                    }
                    $x++;
                }
            }
            
            ?>
        </div>

    </div>
</div>




<?php require('footer.php');?>
