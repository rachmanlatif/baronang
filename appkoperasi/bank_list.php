      <?php require('header.php');?>      

      <?php require('sidebar-left.php');?>

      <?php require('content-header.php');?>

	  <?php
      include "connectinti.php";

        //edit
      $uledit = $_GET['edit'];
      if(!empty($uledit)){
        $eulsql   = "select * from dbo.BankList where KodeBank='$uledit'";  
        $eulstmt  = sqlsrv_query($conns, $eulsql);
        $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
        if(count($eulrow[0]) > 0){
          $ul0    = $eulrow[0];
          $ul1    = $eulrow[1];
          $ul2    = $eulrow[2];
          $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
          $uldisabled = "disabled";
        }
        else{
          if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='bank_list.php';</script>";
          }
          else{
            echo "<script language='javascript'>document.location='bank_list.php?page=".$_GET['page']."';</script>";
          }
        }
      }
      else{
        $ul0    = "";
        $ul1    = "";
        $ul2    = "";
        $ulprocedit = "";
        $uldisabled = "";
      }

      ?>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Bank List</h3>
            </div>
            <form class="form-horizontal" action="proc_mbanklist.php<?=$ulprocedit?>"  method="POST">
              <div class="box-body">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="id" class="col-sm-3 control-label" style="text-align: left;">Bank ID</label>
                    <div class="col-sm-9">
                      <input type="text" name="id" class="form-control" id="id" placeholder="" value="<?=$ul0?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="name" class="col-sm-3 control-label" style="text-align: left;">Bank Name</label>
                    <div class="col-sm-9">
                      <input type="text" name="name" class="form-control" id="name" placeholder=""  value="<?=$ul1?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="desc" class="col-sm-3 control-label" style="text-align: left;">Description</label>
                    <div class="col-sm-9">
                      <textarea name="desc" class="form-control" id="desc" rows="3" placeholder=""  style="max-width: 353px;"><?=$ul2?></textarea>
                    </div>
                  </div>
                </div>
              </div>
                     <div class="box-footer">
						<div class="row">
							<div class="col-sm-4">
							<?php
								if(count($eulrow[0]) > 0){
								  echo '<a href="bank_list.php" class="btn btn-flat btn-default">Cancel</a>';
								}
							  ?>  
							</div>
								<div class="col-sm-4">
									<?php
										if(count($eulrow[0]) > 0){
										echo '<button type="submit" class="btn btn-flat btn-block btn-success pull-right">Update</button>'; 
										}
										else{
										echo '<button type="submit" class="btn btn-flat btn-block btn-primary pull-right">Save</button>';
										}
									?>
								</div>
									<div class="col-sm-4">
									</div>
						</div>
					</div>
				  </div>
              <!-- /.box-body -->
            </form>
          </div>      
        </div>

      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Bank List Data</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Bank ID</th>
                  <th>Bank Name</th>
                  <th>Description</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                    //count
                    $jmlulsql   = "select count(*) from dbo.BankList";  
                    $jmlulstmt  = sqlsrv_query($conns, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page']; 
                    if(empty($halaman)){ 
                       $posisi  = 0;
                       $batas   = $perpages; 
                       $halaman = 1; 
                    } 
                    else{ 
                       $posisi  = (($perpages * $halaman) - 10) + 1;
                       $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeBank asc) as row FROM [dbo].[BankList]) a WHERE row between '$posisi' and '$batas'";

                    //$ulsql = "select * from [dbo].[BankList]";  

                    $ulstmt = sqlsrv_query($conns, $ulsql);

                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                  ?>
                <tr>
                  <td><?=$ulrow[3];?></td>
                  <td><?=$ulrow[0];?></td>
                  <td><?=$ulrow[1];?></td>
                  <td><?=$ulrow[2];?></td>
				   <td width="20%" style="padding: 3px">
                      <div class="btn-group" style="padding-right: 15px">
                        <a href="bank_list.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                      </div>
                      <!--
					  <div class="btn-group">
                        <a href="proc_mbanklist.php?page=<?=$halaman?>&delete=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
                      </div>
					  -->
                    </td>
                  </tr>
                <?php
				}
				$jmlhalaman = ceil($jmlulrow[0]/$perpages);
				?>
              </tbody>
            </table>
          </div>
          <!-- /.col -->
        </div>
       <div class="box-footer clearfix">
              <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
              <?=$posisi." - ".$batas?>
              <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="bank_list.php">&laquo;</a></li>
                <?php
                  for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="bank_list.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                  }
                ?>
                <li class="paginate_button"><a href="bank_list.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
              </ul>
            </div>
          </div>
        </div>

      <?php require('content-footer.php');?>

      <?php require('footer.php');?>