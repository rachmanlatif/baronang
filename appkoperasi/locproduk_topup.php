<?php
// locproduk_topup   
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

$kid = $_SESSION['KID'];

// GET LAST ID LocationMerchant
$sqlloc = 'SELECT TOP 1 ProductID FROM [dbo].[LocationParkingProductTopUp] ORDER BY ProductID DESC';
$query = sqlsrv_query($conn, $sqlloc);
$dataloc = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
//echo $dataloc[0].'<br>';
$intaidloc = 0;
$graidloc = substr($dataloc[0], -3);
$gploc = (int)$graidloc + 1;
$intaidloc = str_pad($gploc, 3, '0', STR_PAD_LEFT);

if(@$_GET['prodid']) {
	$btn_text = 'Update';
	$btn_name_id = 'btnupdate';

	$sql = "SELECT * from [dbo].[LocationParkingProductTopUp] where ProductID='".@$_GET['prodid']."'";
	$query = sqlsrv_query($conn, $sql);
	$data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
}
else {
	$btn_text = 'Simpan';
	$btn_name_id = 'btnok';
}
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<h2 class="uppercase txt-black">Produk EDC</h2>

			<div class="form-inputs m-t-30">
				<form action="" method="post" class="m-t-30" enctype="multipart/form-data">
					<input type="hidden" name="prodid" id="prodid" value="<?php echo $kid.'PRODTOP'.$intaidloc; ?>">
					<input type="hidden" name="prodid_ed" id="prodid_ed" value="<?php echo @$_GET['prodid']; ?>">

					<div class="input-field">
						<input type="text" name="nama" id="nama" class="validate" value="<?php if(@$_GET['prodid']) echo $data[1]; ?>" >
						<label for="nama">Nama</label>
					</div>

					<div class="input-field">
						<input type="text" name="desk" id="desk" class="validate" value="<?php if(@$_GET['prodid']) echo $data[2]; ?>" >
						<label for="desk">Deskripsi</label>
					</div>

					<div class="input-field">
						<span class="txt-black">Status</span> :
						<select name="status" id="status" class="browser-default">
							<option value="0" <?php if(@$_GET['prodid']) { if($data[3] == '0') echo 'selected'; } ?> >Tidak Aktif</option>
							<option value="1" <?php if(@$_GET['prodid']) { if($data[3] == '1') echo 'selected'; } ?> >Aktif</option>
						</select>
					</div>

					<div class="input-field">
						<input type="number" name="poinadd" id="poinadd" class="validate" value="<?php if(@$_GET['prodid']) echo $data[4]; ?>" >
						<label for="poinadd">Jumlah Poin Add</label>
					</div>

					<div class="input-field">
						<input type="number" name="poinbonus" id="poinbonus" class="validate" value="<?php if(@$_GET['prodid']) echo $data[5]; ?>" >
						<label for="poinbonus">Jumlah Poin Bonus</label>
					</div>

					<div class="input-field">
						<input type="number" name="harga" id="harga" class="validate" value="<?php if(@$_GET['prodid']) echo $data[6]; ?>" >
						<label for="harga">Harga</label>
					</div>

					<button type="submit" name="<?php echo $btn_name_id; ?>" id="<?php echo $btn_name_id; ?>" class="btn btn-large primary-color width-100 waves-effect waves-light m-t-30"><?php echo $btn_text; ?></button>

				</form>

				<?php
				$ok = @$_POST['btnok'];
				if(isset($ok)) {
					$prodid 	= @$_POST['prodid'];
					$nama 		= @$_POST['nama'];
					$desk 		= @$_POST['desk'];
					$status 	= @$_POST['status'];
					$add 		= @$_POST['poinadd'];
					$bonus 		= @$_POST['poinbonus'];
					$harga 		= @$_POST['harga'];

					$sql = "INSERT into [dbo].[LocationParkingProductTopUp] values('$prodid','$nama','$desk','$status','$add','$bonus','$harga')";
					$query = sqlsrv_query($conn, $sql);
					?>
					<script type="text/javascript">
						alert('Simpan Berhasil');
						window.location.href = "locproduk_topup.php";
					</script>
					<?php
				}

				$update = @$_POST['btnupdate'];
				if(isset($update)) {
					$prodid 	= @$_POST['prodid_ed'];
					$nama 		= @$_POST['nama'];
					$desk 		= @$_POST['desk'];
					$status 	= @$_POST['status'];
					$add 		= @$_POST['poinadd'];
					$bonus 		= @$_POST['poinbonus'];
					$harga 		= @$_POST['harga'];

					$sql = "UPDATE [dbo].[LocationParkingProductTopUp] set 
								Nama = '$nama',
								Deskripsi = '$desk',
								Status = '$status',
								JumlahPoinAdd = '$add',
								JumlahPoinBonus = '$bonus',
								Harga = '$harga'
							where ProductID='$prodid'";
					$query = sqlsrv_query($conn, $sql);
					?>
					<script type="text/javascript">
						alert('Update Berhasil');
						window.location.href = "locproduk_topup.php";
					</script>
					<?php
				}
				?>
			</div>

			<div class="table-responsive m-t-30">
                <table class="table">
					<thead>
						<tr>
							<th>Product ID</th>
							<th>Nama</th>
							<th>Deskripsi</th>
							<th>Status</th>
							<th>Jumlah Poin Tambah</th>
							<th>Jumlah Poin Bonus</th>
							<th>Harga (Rp.)</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<?php
						$sql = "SELECT * from [dbo].[LocationParkingProductTopUp]";
						$query = sqlsrv_query($conn, $sql);
						while($data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) { 
							if($data[3] == '1') $status = 'Aktif';
							else $status = 'Tidak Aktif';
							?>
							<tr>
								<td><?php echo $data[0]; ?></td>
								<td><?php echo $data[1]; ?></td>
								<td><?php echo $data[2]; ?></td>
								<td><?php echo $status;  ?></td>
								<td><?php echo $data[4]; ?></td>
								<td><?php echo $data[5]; ?></td>
								<td style="text-align: right;"><?php echo number_format($data[6],2,',','.'); ?></td>
								<td>
									<a href="?prodid=<?php echo $data[0]; ?>" class="btn btn-large primary-color waves-light waves-effect"><i class="ion-android-create"></i></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
			
		</div>
	</div>

<?php require('footer.php'); ?>