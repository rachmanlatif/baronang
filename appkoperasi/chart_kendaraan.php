<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>



<div class="animated fadeinup delay-1">
    <div class="page-content txt-black">
        <form action="" method="GET">
            <div class="input-field" >
                <?php
                if(isset($_GET['template'])){
                $to = $_GET['template'];
                }
                else{
                $to = date('Y/m/d');
                }
                ?>


                <select id="akun" name="akun" class="browser-default">
                	<option value=''>- <?php echo lang('Pilih Lokasi'); ?> -</option>
                    	<?php
                        $julsql   = "select * from [dbo].[LocationMerchant  ] where status ='1' order by LocationID asc";
                        // $julsql1   = "select * from [dbo].[LaporanRugiLabaViewNew] where Header ='2' and Username = '$nameuser'";
                        echo $julsq;
                        $julstmt = sqlsrv_query($conn, $julsql);
                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                               ?>
                    <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_GET['lok']){echo "selected";} ?>>&nbsp;<?=$rjulrow[0];?>&nbsp;<?=$rjulrow[2];?></option>
                            <?php } ?>
                </select>
            </div> 
               
            <div style="margin-top: 30px;">
                <button type="submit" name="ok" id="ok" class="btn btn-large primary-color width-100 waves-effect waves-light">OK</button>
            </div>
        </form>
            
        <?php
        $ok = @$_GET['ok'];
        if(isset($ok)) {        
            $lok = $_GET['akun'];
            $dte = date('d-m-Y');
            //echo $dte;
            $bulan_int = date('m', strtotime($dte));
            $tahun_int = date('Y', strtotime($dte));
			//echo $bulan.' '.$tahun.' / '.$bulan_int.' '.$tahun_int;
			?>
                <script type="text/javascript">
                    window.location.href = "chart_kendaraan.php?chart=bulan&lok=<?php echo $lok; ?>&bulan=<?php echo $bulan_int; ?>&tahun=<?php echo $tahun_int; ?>";
                </script>
       	<?php } ?>

       	<?php
       	if($_GET['lok']) {
       		$blnsblm = '01'.'/'.$_GET['bulan'].'/'.$_GET['tahun'];
       		$hasilbln = date('d-m-Y', strtotime('-1 days', strtotime($blnsblm)));
       		$blnhasil = date('m', strtotime($hasilbln));
       		$tahunhasil = date('Y', strtotime($hasilbln));
       		?>
       		<div class="row" style="margin-top: 30px;">
                    <div class="col">
                        <a href="chart_kendaraan.php?chart=bulan&lok=<?php echo $_GET['lok']; ?>&bulan=<?php echo $blnhasil; ?>&tahun=<?php echo $tahunhasil; ?>"><button type="submit" class="btn btn-large primary-color width-100 waves-effect waves-light"> Bulan Sebelumnya </button></a>
                    </div>

            <?php 
                    $hr = date('d-m-Y');
                    $ln = date('m', strtotime($hr));
                    $tn = date('Y', strtotime($hr));
                    if ($_GET['bulan'] != $ln ){ ?>
                    <div class="col">
                        <a href="chart_kendaraan.php?chart=bulan&lok=<?php echo $_GET['lok']; ?>&bulan=<?php echo $ln; ?>&tahun=<?php echo $tn; ?>"><button type="submit" class="btn btn-large primary-color width-100 waves-effect waves-light">Bulan Selanjutnya</button></a>
                    </div>
                  <?php } ?>
            </div>
       	<?php } ?>
        <?php
            $chart = $_GET['chart'];
            if($_GET['chart'] and $_GET['lok']) {
                $json_label = array();
                $json_data1 = array();
                $json_data2 = array();
                $json_data3 = array();
                $json_data4 = array();
                $json_data5 = array();
                $json_data6 = array();


                $dte = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
				$dte1 = date('Y-m-01', strtotime($dte));

                //echo $dte1;
                $Lok = $_GET['lok'];
                $bulan_int = date('t', strtotime($dte));
                $tgl_int = date('m', strtotime($dte));
                //echo $tgl_int;
                $tahun_int = date('Y', strtotime($dte));


                //echo $bulan.' '.$tahun.' / '.$bulan_int.' '.$tahun_int;
                ?>
                <div id="bar_chart" class="card animated fadeinup delay-3">
                    <div class="legend txt-black">
                    	<?php
                    	if ($_GET['chart'] == 'hari'){ ?>
                        	<h5 class="uppercase txt-black"> <?php echo 'Tanggal : '.$_GET['tgl'].' / '.$_GET['bulan'].' / '.$_GET['tahun']; ?></h5>
                    	<?php } else { ?>
	                    	<h5 class="uppercase txt-black"> <?php echo '01/'.$tgl_int.'/'.$tahun_int.' - '.$bulan_int.'/'.$tgl_int.'/'.$tahun_int; ?></h5>
                    	<?php } ?>

                        <?php
                        if ($_GET['chart'] == 'hari'){
                        	echo 'Waktu dari Jam 00.00 sampai dengan 24.00';
                        } 
                        ?>
                        <p><span class="data-color red "></span>Kendaraan Motor</p>
                        <p><span class="data-color blue "></span>Kendaraan Mobil</p>
                        <p><span class="data-color orange "></span>Kendaraan Truck</p>
                        <p><span class="data-color green "></span>Kendaraan VIP</p>
                        <p><span class="data-color purple "></span>Kendaraan Valet</p>
                        <p><span class="data-color teal"></span>Total Kendaraan</p>
                    </div>

                    <div class="barChartCont">
                        <canvas id="barChart"></canvas>
                    </div>
                
                <?php

                if(@$_GET['chart'] == 'bulan') {
                    for($l = 1; $l <= $bulan_int; $l++) {
                        $ln1 = 0;
                        $ln2 = 0;
                        $ln3 = 0;
                        $ln4 = 0;
                        $ln5 = 0;
                        $ln6 = 0;
                       

                        $dtf = $tahun_int."-".$tgl_int."-".$l;
                        $dtt = $tahun_int."-".$tgl_int."-".$bulan_int;

                        if($l > 0 && $l < 10) { array_push($json_label, '0'.$l); }
                        else { array_push($json_label, $l); }
                        

                        $sql_line1 = "SELECT sum(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'C' and TimeOut between '$dtf 00:00:00.000' and '$dtf 23:59:59.999'";
                        //echo $sql_line1;
                        $query1 = sqlsrv_query($conn, $sql_line1);
                        while($data1 = sqlsrv_fetch_array($query1)) {
                            array_push($json_data1, $data1[0]);
                        }

                        $sql_line2 = "SELECT sum(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'B' and TimeOut between '$dtf 00:00:00.000' and '$dtf 23:59:59.999'";
                        $query2 = sqlsrv_query($conn, $sql_line2);
                        while($data2 = sqlsrv_fetch_array($query2)) {
                            array_push($json_data2, $data2[0]);
                        }

                        $sql_line3 = "SELECT sum(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'T' and TimeOut between '$dtf 00:00:00.000' and '$dtf 23:59:59.999'";
                        $query3 = sqlsrv_query($conn, $sql_line3);
                        while($data3 = sqlsrv_fetch_array($query3)) {
                            array_push($json_data3, $data3[0]);
                        }

                        $sql_line4 = "SELECT sum(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'V' and TimeOut between '$dtf 00:00:00.000' and '$dtf 23:59:59.999'";
                        $query4 = sqlsrv_query($conn, $sql_line4);
                        while($data4 = sqlsrv_fetch_array($query4)) {
                            array_push($json_data4, $data4[0]);
                        }

                        $sql_line5 = "SELECT sum(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'P' and TimeOut between '$dtf 00:00:00.000' and '$dtf 23:59:59.999'";
                        $query5 = sqlsrv_query($conn, $sql_line5);
                        while($data5 = sqlsrv_fetch_array($query5)) {
                            array_push($json_data5, $data5[0]);
                        }

                        $sql_line6 = "SELECT sum(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and TimeOut between '$dtf 00:00:00.000' and '$dtf 23:59:59.999'";
                        $query6 = sqlsrv_query($conn, $sql_line6);
                        while($data6 = sqlsrv_fetch_array($query6)) {
                            array_push($json_data6, $data6[0]);
                        }

                        
                    }
                    ?>
                    <div class="m-t-30">
                        <div class="row">
                            
                            <?php
                            for($tgl = 1; $tgl <= $bulan_int; $tgl++) { ?>
                                <div class="col s1">
                                    <?php 
                                    if($tgl > 0 && $tgl < 10) { 
                                        echo '<a href="chart_kendaraan.php?chart=hari&lok='.$Lok.'&tgl='.$tgl.'&bulan='.$tgl_int.'&tahun='.$tahun_int.'" style="color: #0000ff;">0'.$tgl.'</a>'; 
                                    }
                                    else { 
                                        echo '<a href="chart_kendaraan.php?chart=hari&lok='.$Lok.'&tgl='.$tgl.'&bulan='.$tgl_int.'&tahun='.$tahun_int.'" style="color: #0000ff;">'.$tgl.'</a>'; ; 
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>

                        </div>
                    </div>
 
                    <?php
                }
                else if(@$_GET['chart'] == 'hari') {
                    for($j = 0; $j < 24; $j++) {
                        $ln1 = 0;
                        $ln2 = 0;
                       

                        $dtf = $tahun_int."-".$tgl_int."-".@$_GET['tgl'];
                        $jf = $j.':00:00.000';
                        $jt = $j.':59:59.999';

                        //echo $dtf.' '.$jf.' - '.$dtf.' '.$jt.'<br>';

                        array_push($json_label, $j.':00 - '.$j.':59');

                        $sql_line1 = "SELECT SUM(ismember) from [dbo].[ParkingList]  where LocationIn='$_GET[lok]' and Vehicletype = 'C' and TimeOut between '$dtf $jf' and '$dtf $jt'";
                        //echo $sql_line1;
                        $query1 = sqlsrv_query($conn, $sql_line1);
                        while($data1 = sqlsrv_fetch_array($query1)) {
                            array_push($json_data1, $data1[0]);
                        }

                        $sql_line2 = "SELECT SUM(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'B' and TimeOut between '$dtf $jf' and '$dtf $jt'";
                        $query2 = sqlsrv_query($conn, $sql_line2);
                        while($data2 = sqlsrv_fetch_array($query2)) {
                            array_push($json_data2, $data2[0]);
                        }

                        $sql_line3 = "SELECT SUM(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'T' and TimeOut between '$dtf $jf' and '$dtf $jt'";
                        $query3 = sqlsrv_query($conn, $sql_line3);
                        while($data3 = sqlsrv_fetch_array($query3)) {
                            array_push($json_data3, $data3[0]);
                        }

                        $sql_line4 = "SELECT SUM(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'V' and TimeOut between '$dtf $jf' and '$dtf $jt'";
                        $query4 = sqlsrv_query($conn, $sql_line4);
                        while($data4 = sqlsrv_fetch_array($query4)) {
                            array_push($json_data4, $data4[0]);
                        }

                        $sql_line5 = "SELECT SUM(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and Vehicletype = 'P' and TimeOut between '$dtf $jf' and '$dtf $jt'";
                        $query5 = sqlsrv_query($conn, $sql_line5);
                        while($data5 = sqlsrv_fetch_array($query5)) {
                            array_push($json_data5, $data5[0]);
                        }

                        $sql_line6 = "SELECT SUM(ismember) from [dbo].[ParkingList] where LocationIn='$_GET[lok]' and TimeOut between '$dtf $jf' and '$dtf $jt'";
                        $query6 = sqlsrv_query($conn, $sql_line6);
                        while($data6 = sqlsrv_fetch_array($query6)) {
                            array_push($json_data6, $data6[0]);
                        }          

                        //echo '<br>';
                    }
                }
            }
            ?>

    </div>
</div>

    <script src="js/chart.min.js"></script>
    <script type="text/javascript">
    var barChartData = {
        //labels: ["1", "2", "3", "4", "5"],
        labels: <?php echo json_encode($json_label); ?>,
        datasets: [{
            fillColor: "#ff0000",
            strokeColor: "#ff0000",
            highlightFill: "rgba(255, 100, 100, 0.9)",
            highlightStroke: "rgba(255, 100, 100, 0)",
            //data: [65, 59, 90, 56, 40]
            data: <?php echo json_encode($json_data1); ?>
        }, {
            fillColor: "#0000FF",
            strokeColor: "#0000FF",
            highlightFill: "rgba(100, 100, 255, 0.9)",
            highlightStroke: "rgba(100, 100, 255, 0)",
            //data: [28, 48, 19, 27, 90]
            data: <?php echo json_encode($json_data2); ?>
        }, {
            fillColor: "#FFD700",
            strokeColor: "#FFD700",
            highlightFill: "rgba(100, 100, 255, 0.9)",
            highlightStroke: "rgba(100, 100, 255, 0)",
            //data: [28, 48, 19, 27, 90]
            data: <?php echo json_encode($json_data3); ?>
        }, {
            fillColor: "#008000",
            strokeColor: "#008000",
            highlightFill: "rgba(100, 100, 255, 0.9)",
            highlightStroke: "rgba(100, 100, 255, 0)",
            //data: [28, 48, 19, 27, 90]
            data: <?php echo json_encode($json_data4); ?>
        }, {
            fillColor: "#C71585",
            strokeColor: "#C71585",
            highlightFill: "rgba(100, 100, 255, 0.9)",
            highlightStroke: "rgba(100, 100, 255, 0)",
            //data: [28, 48, 19, 27, 90]
            data: <?php echo json_encode($json_data5); ?>
        }, {
            fillColor: "#008080",
            strokeColor: "#008080",
            highlightFill: "rgba(100, 100, 255, 0.9)",
            highlightStroke: "rgba(100, 100, 255, 0)",
            //data: [28, 48, 19, 27, 90]
            data: <?php echo json_encode($json_data6); ?>
        }]
    }

    var ctx3 = document.getElementById("barChart").getContext("2d");
    window.myBar = new Chart(ctx3).Bar(barChartData, {
        responsive: true
    });
    </script>


<?php require('footer_new.php'); ?>