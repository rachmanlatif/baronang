<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');

if($_GET['actv'] == 'b2b') {
    $title = "B2B";
}
else if($_GET['actv'] == 'compl') {
    $title = "Compliment";
}
else{
  echo "<script>document.location='parking_membership.php';</script>";
}

$selected = '';
$barcode = '';
$disabled = 'disabled';
$readonly = 'readonly';
$hide = 'hide';
$scan = 'Scan QR';
if(isset($_REQUEST['barcode'])){
    $barcode = $_REQUEST['barcode'];
    $selected = 'checked';

    $cardno = '';
    $card_id = str_replace(' ','+',$barcode);
    $xx = "select* from dbo.MasterCard where Barcode='$card_id'";
    $yy = sqlsrv_query($conn, $xx);
    $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
    if($zz != null){
        $cardno = $zz[0];
    }
    else{
        $readonly = '';
    }

    $disabled = '';
    $hide = '';
    $scan = 'Scan Ulang QR';
}
?>

	<div class="animated fadeinup delay-1">
    	<div class="page-content">

            <h1 class="uppercase">Aktivasi <?php echo $title; ?></h1>

                    <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                        <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                            <?php echo $_SESSION['error-message']; ?>
                        </div>
                    <?php } ?>

    		<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
          <div class="m-t-30">
              <button type="button" id="ocam" class="btn btn-large width-100 primary-color waves-effect waves-light"><?php echo $scan; ?></button>
          </div>

          <div id="form-input" class="<?php echo $hide; ?>">
        			<div class="input-field">
        				<span>Pilih ID</span> : <br>
        				<select class="browser-default" name="vhlid">
                  <option value="" selected></option>
        					<?php
                  if($_GET['actv'] == 'b2b') {
                      $sql2 = "SELECT * from [dbo].[RegistrasiB2B] where Quantity > 0 order by RegistrasiB2BID ASC";
                  }
                  else {
                      $sql2 = "SELECT * from [dbo].[RegistrasiCompliment] where Quantity > 0 order by RegistrasiComplimentID ASC";
                  }
                  $query2 = sqlsrv_query($conn, $sql2);
                  while($data2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC)) {
                    $locationname = '';
                    $sql = "SELECT * from [dbo].[LocationMerchant] where LocationID = '$data2[1]'";
                    $query = sqlsrv_query($conn, $sql);
                    $data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
                    if($data != null){
                      $locationname = $data[2];
                    }
                    ?>
                      <option value="<?php echo $data2[0]; ?>" <?php if($_GET['actvid']){ if($data2[0] == $data[3]) echo 'selected'; } ?> ><?php echo $data2[0].' - '.$locationname; ?></option>
                  <?php } ?>
    	          </select>
        			</div>

              <div class="input-field">
                <span>Qr Code</span> <br>
                <input type="text" name="result-code" id="result-code" value="<?php echo $card_id; ?>" readonly>
              </div>

              <div class="input-field">
                <span>Card No</span> <br>
                <input type="number" name="result-number" value="<?php echo $cardno; ?>" <?php echo $readonly; ?>>
                <?php if($cardno == ''){ echo 'Masukan kembali nomor kartu'; } ?>
              </div>

        			<div class="input-field">
                  <div class="box box-solid hide" id="form-camera">
                      <div class=""><b>Pilih Kamera</b></div>
                      <select class="browser-default" id="camera-select"></select>
                      <br>
                      <div class="box-body">
                          <div class="row">
                              <div class="col">
                                  <button title="Start Scan QR Barcode" class="waves-effect waves-light btn-large primary-color width-100" id="play" type="button">Start</button>
                              </div>
                              <div class="col">
                                  <button title="Stop streams" class="waves-effect waves-light btn-large primary-color width-100" id="stop" type="button">Stop</button>
                              </div>
                          </div>

                          <div class="row">
                              <div class="col-xs-12 col-sm-6">
                                  <div class="well" style="position: relative;display: inline-block;">
                                      <canvas id="webcodecam-canvas"></canvas>
                                      <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                                      <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                                      <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                                      <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                                  </div>
                              </div>

                              <div class="col-xs-12 col-sm-6">
                                  <div class="thumbnail" id="result">
                                      <img style="width: 100%;" id="scanned-img" src="">
                                      <div class="caption">
                                          <p class="text-center" id="scanned-QR"></p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
        			</div>

              <?php if($_SESSION['device'] == 0){ ?>
                  <script type="text/javascript">
                      $('#ocam').click(function(){
                          Intent.openActivity("QRActivity","activated_mb2b_compl.php?actv=<?php echo $_GET['actv']; ?>");
                      });
                  </script>
              <?php } ?>

              <?php if($_SESSION['device'] == 1){ ?>
                  <script type="text/javascript">
                      $('#ocam').click(function(){
                          $('#result-code').val();
                          $('#form-camera').removeClass('hide');
                      });

                      $('#stop').click(function(){
                          $('#form-camera').addClass('hide');
                      });
                  </script>
              <?php } ?>

              <button type="submit" name="btnok" id="btnok" class="btn btn-large primary-color width-100 waves-effect waves-light m-t-30" <?php echo $disabled; ?>>Submit</button>
            </div>
    		</form>

            <?php
            if(isset($_POST['btnok'])) {
                if($_GET['actv'] == 'b2b') {
                    $sqlloc = "SELECT TOP 1 AktivasiB2BID FROM [dbo].[AktivasiB2B] ORDER BY AktivasiB2BID DESC";
                    $query = sqlsrv_query($conn, $sqlloc);
                    $dataloc = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
                }
                else{
                    $sqlloc = "SELECT TOP 1 AktivasiComplimentID FROM [dbo].[AktivasiCompliment] ORDER BY AktivasiComplimentID DESC";
                    $query = sqlsrv_query($conn, $sqlloc);
                    $dataloc = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
                }

                $intaidloc = 0;
                $graidloc = substr($dataloc[0], -6);
                $gploc = (int)$graidloc + 1;
                $intaidloc = str_pad($gploc, 6, '0', STR_PAD_LEFT);
                if($_GET['actv'] == 'b2b') {
                  $id = $KID.'B2B'.date('ym').$intaidloc;
                }
                else {
                  $id = $KID.'CMT'.date('ym').$intaidloc;
                }

                $vhlid          = $_POST['vhlid'];
                $sqr            = $_POST['result-code'];
                $sqrnumber      = $_POST['result-number'];

                if($id == ''){
                    messageAlert('Silahkan coba lagi', 'info');
                    echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
                }
                else if($vhlid == ''){
                    messageAlert('Harap pilih lokasi', 'info');
                    echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
                }
                else if($sqrnumber == ''){
                  messageAlert('Harap masukan nomor kartu qr', 'info');
                  echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
                }
                else{
                    $xu = "select* from dbo.MasterCard where CardNo='$sqrnumber'";
                    $yu = sqlsrv_query($conn, $xu);
                    $zu = sqlsrv_fetch_array($yu, SQLSRV_FETCH_NUMERIC);
                    if($zu == null){
                      messageAlert('Kartu QR tidak ditemukan', 'info');
                      echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
                    }
                    else{
                      //cek
                      $status = 0;
                      if($_GET['actv'] == 'b2b') {
                          $x = "select* from dbo.AktivasiB2B where RegistrasiB2BID='$vhlid' and TipeQR='$sqrnumber'";
                          $y = sqlsrv_query($conn, $x);
                          $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                          if($z != null){
                              $status = 1;
                          }
                      }
                      else{
                        $x = "select* from dbo.AktivasiCompliment where RegistrasiComplimentID='$vhlid' and TipeQR='$sqrnumber'";
                        $y = sqlsrv_query($conn, $x);
                        $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                        if($z != null){
                            $status = 1;
                        }
                      }

                      if($status == 1){
                        messageAlert('QR Code sudah didaftarkan', 'info');
                        echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
                      }
                      else{
                        $xx = "select* from dbo.MasterCard where Barcode='$sqr'";
                        $yy = sqlsrv_query($conn, $xx);
                        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                        if($zz == null){
                            $sql2 = "update dbo.MasterCard set Barcode = '$sqr' where CardNo = '$sqrnumber'";
                            $query2 = sqlsrv_query($conn, $sql2);
                        }

                        if($_GET['actv'] == 'b2b') {
                            $sql = "insert into dbo.AktivasiB2b(AktivasiB2BID, RegistrasiB2BID, TipeQR) values('$id','$vhlid','$sqrnumber')";
                        }
                        else {
                            $sql = "insert into dbo.AktivasiCompliment(AktivasiComplimentID, RegistrasiComplimentID, TipeQR) values('$id','$vhlid','$sqrnumber')";
                        }
                        $query = sqlsrv_query($conn, $sql);
                        if($query){
                            if($_GET['actv'] == 'b2b') {
                                $sql2 = "update dbo.RegistrasiB2B set Quantity = Quantity - 1 where RegistrasiB2BID='$vhlid'";
                                $query2 = sqlsrv_query($conn, $sql2);

                                $x = "select* from dbo.RegistrasiB2B where RegistrasiB2BID='$vhlid'";
                                $y = sqlsrv_query($conn, $x);
                                $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                                if($z != null){
                                    $location = $z[1];
                                    $vehicle = $z[2];
                                    $validdate = $z[3]->format('Y-m-d');

                                    $year = date('Y');
                                    $startmonth = date('m');
                                    $endmonth = $z[3]->format('m');

                                    $register = date('Y-m-d H:i:s');
                                    $sql3 = "insert into dbo.MemberLocation(LocationID, Status, DateRegister, ValidDate, VehicleID, CardNo)values('$location',1,'$register','$validdate','$vehicle','$sqrnumber')";
                                    $query3 = sqlsrv_query($conn, $sql3);

                                    for($a=$startmonth;$a<=$endmonth;$a++){
                                        $qwes = "insert into dbo.MemberLocationMonth(CardNo, LocationID, VehicleID, Bulan, Year, Status) values('$sqrnumber','$location','$vehicle',$a,'$year',1)";
                                        $asds =  sqlsrv_query($conn, $qwes);
                                    }

                                }

                                messageAlert('Berhasil melakukan aktivasi B2B', 'success');
                                echo "<script>document.location='activated_mb2b_compl.php?actv=b2b';</script>";
                            }
                            else {
                                $sql2 = "update dbo.RegistrasiCompliment set Quantity = Quantity - 1 where RegistrasiComplimentID='$vhlid'";
                                $query2 = sqlsrv_query($conn, $sql2);

                                $x = "select* from dbo.RegistrasiCompliment where RegistrasiComplimentID='$vhlid'";
                                $y = sqlsrv_query($conn, $x);
                                $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                                if($z != null){
                                    $location = $z[1];
                                    $vehicle = $z[2];
                                    $validdate = $z[3]->format('Y-m-d');

                                    $year = date('Y');
                                    $startmonth = date('m');
                                    $endmonth = $z[3]->format('m');

                                    $register = date('Y-m-d H:i:s');
                                    $sql3 = "insert into dbo.MemberLocation(LocationID, Status, DateRegister, ValidDate, VehicleID, CardNo)values('$location',1,'$register','$validdate','$vehicle','$sqrnumber')";
                                    $query3 = sqlsrv_query($conn, $sql3);

                                    for($a=$startmonth;$a<=$endmonth;$a++){
                                        $qwes = "insert into dbo.MemberLocationMonth(CardNo, LocationID, VehicleID, Bulan, Year, Status) values('$sqrnumber','$location','$vehicle',$a,'$year',1)";
                                        $asds =  sqlsrv_query($conn, $qwes);
                                    }
                                }

                                messageAlert('Berhasil melakukan aktivasi Compliment', 'success');
                                echo "<script>document.location='activated_mb2b_compl.php?actv=compl';</script>";
                            }
                        }
                        else{
                            messageAlert('Gagal melakukan aktivasi B2B. Silahkan coba lagi', 'danger');
                            echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
                        }
                      }
                    }
                }
            }
            ?>
            <br>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Aktivasi ID</th>
                            <th>Register ID</th>
                            <th>QR Code</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if($_GET['actv'] == 'b2b') {
                            $sql = "SELECT * from [dbo].[AktivasiB2b] Order by AktivasiB2BID ASC";
                        }
                        else {
                            $sql = "SELECT * from [dbo].[AktivasiCompliment] Order by AktivasiComplimentID ASC";
                        }

                        $query = sqlsrv_query($conn, $sql);
                        while($data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) {
                          ?>
                            <tr>
                                <td><?php echo $data[0]; ?></td>
                                <td><?php echo $data[3]; ?></td>
                                <td><?php echo $data[1]; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>

		</div>
	</div>

<?php require('footer.php'); ?>
