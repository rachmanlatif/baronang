<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
        <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

        <h2 class="uppercase"><?php echo lang('Pengguna'); ?></h2>

            <form class="form-horizontal" action="proc_emoneylink.php" method="POST">

                <div class="input-field">
                    <input type="number" name="acc" class="validate" id="result-code-acc">
                    <label for="result-code-acc"><?php echo lang('Akun e-Money'); ?></label>
                </div>

                <div class="input-field">
                    <input type="number" name="card" class="validate" id="result-code-card">
                    <label for="result-code-card"><?php echo lang('Nomor Kartu'); ?></label>
                </div>

                <div class="row m-l-0" style="margin-top: 30px; margin-bottom: 30px;">
                    <div class="col">
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4"><?php echo lang('Simpan'); ?></button>
                    </div>
                    <div class="col">
                        <button type="button" id="ocam" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4"> Scan QR Code</button>
                    </div>
                    <div class="col">
                        <a href="identity_cs.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4">Back</button></a>
                    </div>
                </div>

            </form>

            <div class="box box-solid hide" id="form-camera">
              <h3 class="uppercase">Scan QR Code/Barcode</h3>

                <b>Pilih kamera</b>
                <select class="browser-default" id="camera-select"></select>
                <br>
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <button title="Start Scan QR Barcode" class="waves-effect waves-light btn-large primary-color width-100" id="play" type="button">Start</button>
                        </div>
                        <div class="col">
                            <button title="Stop streams" class="waves-effect waves-light btn-large primary-color width-100" id="stop" type="button">Stop</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="well" style="position: relative;display: inline-block;">
                                <canvas id="webcodecam-canvas"></canvas>
                                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="thumbnail" id="result">
                                <img id="scanned-img" style="width: 100%;" src="">
                                <div class="caption">
                                    <p class="text-center" id="scanned-QR"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            
        <div class="box box-solid">

            <h3 class="uppercase"><?php echo lang('Daftar master kartu'); ?></h3>

            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class=""><?php echo lang('No'); ?></th>
                            <th class=""><?php echo lang('Nomor Kartu'); ?></th>
                            <th class=""><?php echo lang('Image'); ?></th>
                            <th class=""><?php echo lang('Barcode'); ?></th>
                            <th class=""><?php echo lang('Logo'); ?></th>
                            <th class=""><?php echo lang('Status'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //count
                        $jmlulsql   = "select count(*) from [PaymentGateway].[dbo].[MasterCard]";
                        $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        }
                        else{
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY CardNo asc) as row FROM [PaymentGateway].[dbo].[MasterCard]) a WHERE row between '$posisi' and '$batas'";
                        $ulstmt = sqlsrv_query($conn, $ulsql);
                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?=$ulrow[6];?></td>
                                <td><?=$ulrow[1];?></td>
                                <td><?=$ulrow[2];?></td>
                                <td><?=$ulrow[2];?></td>
                                <td><?=$ulrow[4];?></td>
                                <td>
                                    <?php
                                    if($ulrow[5] == 1){
                                        echo 'Aktif';
                                    }
                                    else{
                                        echo 'Tidak Aktif';
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }

                        $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                        ?>
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix right">
                <div style="text-align: center;">
                    <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>

                    <?php
                    $reload = "cscachangecard.php?";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages)."</div>";
                    ?>
                </div>

            </div>

        </div>

    </div>
</div>

<?php if($_SESSION['device'] == 0){ ?>
    <script type="text/javascript">
        $('#ocam').click(function(){
            Intent.openActivity("QRActivity","identity_cs.php");
          });
    </script>
<?php } ?>

<?php if($_SESSION['device'] == 1){ ?>
    <script type="text/javascript">
        $('#ocam').click(function(){
            $('#result-code').val();
            $('#form-camera').removeClass('hide');
        });

        $('#stop').click(function(){
            $('#form-camera').addClass('hide');
        });
    </script>
<?php } ?>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer.php');
?>
