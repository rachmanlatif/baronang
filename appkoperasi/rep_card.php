<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Laporan Kartu'); ?></h2>                     
                         
            <form class="form-horizontal" action="" method = "POST">
                <div class="input-field">
                    <input type="text" name="tgl1" id="tgl1" class="datepicker" value="<?php echo "$_POST[tgl1]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Awal'); ?></label>
                </div>
                
                <div class="input-field">
                    <input type="text" name="tgl2" id="tgl2" class="datepicker" value="<?php echo "$_POST[tgl2]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Akhir'); ?></label>
                </div>

                <div class="input-field">
                    <?php
                    if(isset($_POST['template'])){
                    $to = $_POST['template'];
                    }
                    else{
                    $to = date('Y/m/d');
                    }
                    ?>


                    <select id="akun" name="akun" class="browser-default">
                        <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                            <?php
                            //$julsql   = "select * from [gateway].[dbo].[EDCList] where SUBSTRING(UserIDBaronang,0,6) = SUBSTRING('700080',0,6)";
                            $julsql   = "SELECT distinct (CardNo) FROM ( SELECT a.CardNo,c.TransactionType, ROW_NUMBER() OVER (ORDER BY c.Date asc) as row FROM [dbo].[MasterCard] a inner join[dbo].[TransList] c on CardNo = AccountDebet  or CardNo = AccountDebet where c.TransNo is not null and c.TransactionType = 'MBRS' or c.TransactionType = 'TOPP' ) a";
                            //echo $julsql1;
                            $julstmt = sqlsrv_query($conn, $julsql);

                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                $carmemb = "select * from dbo.MasterCard where CardNo = '$rjulrow[0]' and status = '1'";
                                $procmemb = sqlsrv_query($conn, $carmemb);
                                $hasilmemb  = sqlsrv_fetch_array( $procmemb, SQLSRV_FETCH_NUMERIC);

                               ?>
                            <option value="<?=$hasilmemb[0];?>" <?php if($hasilmemb[0]==$_POST['akun']){echo "selected";} ?>>&nbsp;<?=$hasilmemb[0];?>&nbsp;<?=$hasilmemb[6];?></option>
                            <?php } ?>
                    </select>
                </div>


                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_card.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
            </form>

        <?php if($_POST['akun']){
            $tgl1 = $_POST['tgl1'];
            $tanggaldari = date('Y-m-d', strtotime($tgl1));
            //echo $tgl1;
            $tgl2 = $_POST['tgl2'];
            //echo $tgl2;
            $tanggalsampai = date('Y-m-d', strtotime($tgl2));
            $hanyatanggal = date('d', strtotime($tgl1));
            $hanyatanggal1 = date('d', strtotime($tgl2));
            //echo $akun;
            ?>
            <!-- <div>
                <a href="drep_neracaHIS.php?from=<?php echo $tanggaldari; ?>&to=<?php echo $tanggalsampai; ?>&acc3=<?php echo $_POST['akun']; ?>&st=5"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
            </div>
 -->
            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Laporan Kartu'); ?></h3>
                </tr>
            </div>
            <div class="box-header " align="Center">
                <tr>
                    <h3 class="box-title" align="center"><?php echo $_SESSION['NamaKoperasi']; ?></h3>
                </tr>
                </div>   


            <div class="box-header" align="Center">
                    <tr>
                        <?php
                            $bulan = date('m', strtotime($_POST['tgl1']));
                            $tahun = date('Y', strtotime($_POST['tgl1']));
                            $bulan2 = date('m', strtotime($_POST['tgl2']));
                            $tahun2 = date('Y', strtotime($_POST['tgl2']));
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        $bulan = 'Desember';
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
                        <?php
                        if ($bulan2 != 1) {
                                if ($bulan2 != 2 ) {
                                   if ($bulan2 != 3) {
                                       if ($bulan2 != 4) {
                                            if ($bulan2 !=5) {
                                                if ($bulan2 !=6) {
                                                    if ($bulan2 !=7) {
                                                        if ($bulan2 !=8) {
                                                            if ($bulan2 !=9) {
                                                                if ($bulan2 !=10) {
                                                                    if ($bulan2 !=11) {
                                                                        $bulan2 = 'Desember';
                                                                    } else {
                                                                        $bulan2 = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan2 = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan2 = 'September';
                                                            }
                                                        } else {
                                                            $bulan2 = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan2 = 'Juli';
                                                    }
                                                } else {
                                                    $bulan2 = 'Juni';
                                                }
                                            } else {
                                              $bulan2 = 'Mei';  
                                            }
                                       } else {
                                            $bulan2 = 'April';       
                                       }
                                   } else {
                                    $bulan2 = 'Maret';
                                   }
                                } else {
                                    $bulan2 = 'Februari';    
                                }                               
                            } else {
                                $bulan2 = 'Januari';
                            }
                        ?>
                        <h3 class="box-title"><?php echo "Periode : ",$hanyatanggal," ",$bulan," ",$tahun, " - ",$hanyatanggal1," ",$bulan2," ",$tahun2; ?></h3>
                    </tr>

                <?php 
                $nm = 1;
                $x = "select * from dbo.MasterCard where CardNo = '$_POST[akun]' and Status = '1'";
                //echo $x;
                $y = sqlsrv_query($conn, $x);
                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                    $subsedc = substr($z[6],0,8);
                    $cekedc  = "select * from dbo.SetingJurnal where Kode = '$subsedc'";
                    $proedc = sqlsrv_query($conn, $cekedc);
                    $hasedc = sqlsrv_fetch_array($proedc, SQLSRV_FETCH_NUMERIC);
                    $niledc = $hasedc[2];

                ?>
                <div class="box-header " align="left">
                <tr>
                    <h5 class="box-title" align="left"><?php echo ('No. Kartu : '); ?> <?php echo $z[0]; ?></h5  >
                </tr>
                </div>    
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo 'Kode Transaksi'; ?></th>
                                    <!-- <th><?php echo 'No. Rekening'; ?></th> -->
                                    <th><?php echo 'Tanggal Transaksi'; ?></th>
                                    <th><?php echo 'Deskripsi'; ?></th>
                                    <th><?php echo 'Debit'; ?></th>
                                    <th><?php echo 'Kredit'; ?></th>
                                    <th><?php echo 'Total'; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $from= date('Y/01/d', strtotime('-50 years', strtotime($_POST['tgl1'])));
                            //echo $from;
                            $from1= date('Y/m/d', strtotime('-1 month', strtotime($_POST['tgl1'])));
                            $to = date('Y/m/t', strtotime($from1));
                            $from2= date('Y/m/d', strtotime('-1 days', strtotime($_POST['tgl1'])));
                            //echo $to;
                           
                            //$a = "SELECT * FROM ( SELECT a.KodeJurnal,a.KodeAccount,a.Debet,a.Kredit,a.Keterangan,b.Tanggal, ROW_NUMBER() OVER (ORDER BY b.Tanggal asc) as row FROM [dbo].[JurnalDetail] a inner join [dbo].[JurnalHeader] b on a.KodeJurnal = b.KodeJurnal where a.KodeAccount = '$z[0]' and b.tanggal between '$from' and '$to') a ";
                            
                            //$a = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where Debet = '$z[0]' and CONVERT(date,Date) between '$from' and '$to' or Kredit = '$z[0]' and CONVERT(date,Date) between '$from' and '$to') a";
                           
                            
                            $a = "select * from dbo.AccountBalance where KodeAccount ='$niledc' and Header = '5'and Tanggal = '$from2'";
                            //echo $a;
                            $b = sqlsrv_query($conn, $a);
                            
                            //$btotal = 0;
                            $ck = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                                //var_dump($ck);



                            if($ck == null){
                                $nilai = 0;
                            } else {
                                $nilai = $ck[10];
                            }
                                
                                
                             // //var_dump($c);
                             //        if($c[3] == $z[0] ){
                             //            $total3-=$c[5];
                             //            $ctotal2=$total3;               
                             //        }else{
                             //            $total3+=$c[5];
                             //            $ctotal2=$total3;
                             //        }
                             //        echo $total3;
                            ?>       
                             <tr>
                                <th colspan="5"><?php echo lang('Saldo Awal'); ?></th>
                                <th style="text-align: right;"><span><?php echo number_format($nilai); ?></span></th>
                            </tr>
                            


            
                    <?php
                    $from= $_POST['tgl1'];
                    $akun = $_POST['akun'];
                    $to= $_POST['tgl2'];
                    $tglb = date('Y-m-d H:i:s', strtotime($from));
                    $tglh = date('Y-m-d H:i:s', strtotime('+1 days', strtotime($to)));
                    $a = "select * from dbo.TransList where AccountDebet = '$z[0]' and date between '$tglb' and '$tglh' and TransactionType between 'MBRS' and 'TOPP' or AccountKredit = '$z[0]' and Date between '$tglb' and '$tglh' and TransactionType between 'MBRS' and 'TOPP' order by TransNo Asc";
                    //echo $a;
                    $b = sqlsrv_query($conn, $a);
                    //echo $b;
                    $total=$nilai;
                    //echo $total;
                    $ctotal=$total;
                    //echo $ctotal;
                    //echo $total;
                    //$btotal = 0;
                    
                    //var_dump($c);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                            $cf = $c[5]/1000;
                        //var_dump($c);
                            if($c[1] == $z[0] ){
                                $total-=$cf;
                                $ctotal=$total;               
                            }else{
                                $total+=$cf;
                                $ctotal=$total;
                                
                            }
                

                            ?>                 
                    <tr>
                        <td><a href="buku_besarlap.php?trx=<?php echo $c[0]; ?>&tgl1=<?php echo $from; ?>&tgl2=<?php echo $to; ?>" target="_blank"><?php echo $c[0]; ?></a></td>
                        <!-- <td><?php  if ($z[0] == $c[1]){
                                    $akunr = $c[1];
                                    } else {
                                    $akunr = $c[2];
                                        } 
                                    echo $akunr; ?></td> -->
                        <td><?php echo $c[6]->format('Y-m-d H:i:s'); ?></td>
                        <td><?php echo $c[9]; ?> - <?php echo $c[7]; ?></td>
                        <td style="text-align: right;"><?php if ($z[0] == $c[1]){
                                    $amountr = $c[5]/1000;
                                    } else {
                                    $amountr = 0;
                                        }
                                    echo number_format($amountr); ; ?></td>
                        <td style="text-align: right;"><?php if ($z[0] == $c[2]){
                                    $amountr = $c[5]/1000;
                                    } else {
                                    $amountr = 0;
                                        }
                                    echo number_format($amountr); ; ?></td>
                        <td style="text-align: right;"><?php echo number_format($total); ?></td>

                    </tr>
                     <?php } ?>
                      <?php
                        $from= $_POST['tgl1'];
                        $to= $_POST['tgl2'];
                        $tglh = date('Y/m/d', strtotime('+1 days', strtotime($to)));
                        //$a = "select * from dbo.Translist where date between '$from' and '$to'";
                        $a = "select * from dbo.TransList where AccountDebet = '$z[0]' and date between '$tglb' and '$tglh' and TransactionType between 'MBRS' and 'TOPP' or AccountKredit = '$z[0]' and Date between '$tglb' and '$tglh' and TransactionType between 'MBRS' and 'TOPP' order by TransNo Asc";
                          //echo $a;
                        $b = sqlsrv_query($conn, $a);
                        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                            
                                $amountdebit = "select sum(amount) from dbo.TransList where AccountDebet = '$z[0]' and date between '$tglb' and '$tglh' and TransactionType between 'MBRS' and 'TOPP'";
                                //echo $amountdebit;
                                $prosesdebit = sqlsrv_query($conn, $amountdebit);
                                $hasilDebit1 = sqlsrv_fetch_array($prosesdebit, SQLSRV_FETCH_NUMERIC); 
                                $hasilDebit = $hasilDebit1[0]/1000;

                                $amountKredit = "select sum(amount) from dbo.TransList where AccountKredit = '$z[0]' and Date between '$tglb' and '$tglh' and TransactionType between 'MBRS' and 'TOPP'";
                                //echo $amountKredit;
                                $prosesKredit = sqlsrv_query($conn, $amountKredit);
                                $hasilKredit1 = sqlsrv_fetch_array($prosesKredit, SQLSRV_FETCH_NUMERIC); 
                                $hasilKredit = $hasilKredit1[0]/1000;

                        ?>
                             <tr>
                                <th colspan="3"><?php echo lang('Total Saldo Akhir'); ?></th>
                                <th style="text-align: right;"><?php echo number_format($hasilDebit); ?></th>
                                <th style="text-align: right;"><?php echo number_format($hasilKredit); ?></th>
                                <th style="text-align: right;"><span><?php echo number_format($ctotal); ?></span></th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>    
                              
                <?php } ?>
                    

                    
            <!-- div dekat tanggal -->
            </div>
            </div>
        <?php } ?>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>


<script type="text/javascript">
    var tampung = [];

     $('.btn-addu').click(function(){
       var index = $('#detailUser tr').length;

        var user = $('#user option:selected').val();
        var amount = $('#amount').val();
        var date = $('#date').val();

        if(date == ''){
          alert('Assign date tidak boleh kosong');
          return false;
        }
        else if(user == '' || amount == '' || amount <= 0){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(user, tampung) !== -1){
            alert('Tidak dapat memilih user yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addteller.php",
                type : 'POST',
                dataType : 'json',
                data: { index: index, user: user, amount: amount, date: date},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.id);
                        $("#detailUser").append("<tr>" +
                            "<td><input type='hidden' name='user["+data.index+"]' value="+ data.id +">" + data.nama + "</td>" +
                            "<td><input type='hidden' name='amount["+data.index+"]' value="+ data.amount +">" + data.amount +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='ion-android-delete'></i></a></td>" +
                            "</tr>");

                            $('#user').val('');
                            $('#amount').val('');
                    }
                    else{
                        alert(data.message);
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });
</script>

<?php require('footer.php'); ?>
