<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Transaksi Total Parkir");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->getStyle('C1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('C1', 'Laporan Transaksi Parkir');

    $objWorkSheet->getStyle('A3')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A3', 'Lokasi :');

    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A4', 'Total Point : ');

    


    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A6', 'No');
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B6', 'No. Transaksi');
    $objWorkSheet->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('C6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C6', 'Tanggal Transaksi');
    $objWorkSheet->getStyle('D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D6', 'No Kartu');
    $objWorkSheet->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E6', 'Type Kartu');
    $objWorkSheet->getStyle('F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('F6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('F6', 'Point');
 


    if($_GET['lok']){

    $akun = $_GET['lok'];
    $tgl1 = $_GET['tgl1'];
    $tgl2 = $_GET['tgl2'];
    $tglh = date('Y/m/d', strtotime('+1 days', strtotime($tgl2)));
    
    $k = 3;
    $l = 4;
    $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
        $asum = "SELECT sum(amount) FROM [dbo].[Translist] where AccountKredit = '$_GET[lok]' and TransactionType between 'MBRS' and 'PARK' and Date between '$tgl1' and '$tglh'";
        $prosum = sqlsrv_query($conn,$asum);
        $hassum = sqlsrv_fetch_array($prosum, SQLSRV_FETCH_NUMERIC);
        $sumn = $hassum[0] / 1000;

           
        $objWorkSheet->getStyle('B'.$k)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$k,$z[0] .' - '. $z[2]);
        $objWorkSheet->getStyle('B' .$l)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT); 
        $objWorkSheet->getStyle('B'.$l)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$l, number_format($sumn) .' PTS');           

    }

    $row = 7;
    $rw = 7;
    
    $xy = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] where AccountKredit = '$_GET[lok]' and TransactionType between 'MBRS' and 'PARK' and Date between '$tgl1' and '$tglh') a";
    //echo $x;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
         $point = $za[5] / 1000;

               
        $objWorkSheet->SetCellValueExplicit("A".$row,$za[15]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
        $objWorkSheet->SetCellValueExplicit("C".$row,$za[6]->format('Y-m-d H:i:s'));
        $objWorkSheet->SetCellValueExplicit("D".$row,$za[1]);
        $objWorkSheet->SetCellValueExplicit("E".$row,$za[7]);
        $objWorkSheet->getStyle('F' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("F".$row, number_format($point), PHPExcel_Cell_DataType::TYPE_STRING);
        $row++;
        $rw++;
        }

    

 }
//exit;
    $objWorkSheet->setTitle('Drep Laporan Transaksi Parkir');

    $fileName = 'LapTransPar'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
