<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Template COA");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    
    $objWorkSheet->getStyle('C1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('C1', 'Laporan Laba Rugi');
    $objWorkSheet->getStyle('B2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B2', 'Periode :');


    $objWorkSheet->getStyle('C4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C4', 'Laba Rugi');
    


    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'Kode Akun');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'Nama');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Nilai');





if($_GET['bulan'] and $_GET['tahun'] ){
    $no = 1;
    $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
    $from = date('Y-m-01', strtotime($from1));
    $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
    $to = date('Y-m-t', strtotime($to3));
    $to1 = date('m', strtotime($to3));
    $to2 = date('Y', strtotime($to3));
    $a = "exec dbo.ProsesGenerateLaporanRugiLaba '$from','$to','$_SESSION[UserID]'";
    //echo $a;
    $b = sqlsrv_query($conn, $a);    

    $bln = 2 ;
    $bulan = date('m', strtotime($to));
    //echo $bulan;
    $tahun = date('Y', strtotime($to));
        if ($bulan != 01) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

        $objWorkSheet->getStyle('C')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("C".$bln,$bulan .'-'. $tahun);   



        $H1 = 6;
           
        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
        $from = date('Y-m-01', strtotime($from1));
        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
        $to = date('Y-m-t', strtotime($to3));
        $to1 = date('m', strtotime($to3));
        $to2 = date('Y', strtotime($to3));
        $name = $_SESSION[Name];
        $x = "select* from dbo.GroupAcc where KodeGroup in('4.0.00.00.000','5.0.00.00.000','6.0.00.00.000')";
        $atotal = 0;
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            //var_dump($z);
             
        $objWorkSheet->SetCellValue("B".$H1,$z[0]);
        $objWorkSheet->SetCellValue("C".$H1,$z[1]);

        $H1++;

                $H2 = 6;
                $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                //echo $atotal1; 
                $yy = sqlsrv_query($conn, $xx);
                $total2=0;
                $total3=0;
                while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                    //var_dump($zz);    

                $abc = "select SUM (NilaiHide) from dbo.LaporanRugiLabaViewNew where GroupID = '$z[0]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name'";
                //echo $abc;
                $bcd = sqlsrv_query($conn, $abc);
                $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                $dtotal1=$b[0];
                $atotal= $atotal1+$dtotal1;

                $H2=$H1++;
                $dtotal1+=$b[0];

                $objWorkSheet->SetCellValue("B".$H2,$zz[0]);    
                $objWorkSheet->SetCellValue("C".$H2,$zz[1]);    


                    $h3 = 6 ;
                    $xxx = "select * from dbo.LaporanRugiLabaViewNew where TypeID = '$zz[0]' and header = 0 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' ORDER BY KodeAccount Asc"; 
                    //echo $xxx;
                    $yyy = sqlsrv_query($conn, $xxx);
                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                        //var_dump($zzz);


                        $abc = "select SUM (NilaiHide) from dbo.LaporanRugiLabaViewNew where TypeID = '$zzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%'";
                        //echo $abc;
                        $bcd = sqlsrv_query($conn, $abc);
                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                        $c=$b[0];

                    
                    $h3=$H1++;

                    $objWorkSheet->SetCellValue("B".$h3,$zzz[0]);    
                    $objWorkSheet->SetCellValue("C".$h3,$zzz[1]);  
                    $objWorkSheet->getStyle('D')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objWorkSheet->SetCellValue("D".$h3, number_format($c), PHPExcel_Cell_DataType::TYPE_STRING);  


                        $h4 = 6 ;
                        $xxxx = "select * from dbo.LaporanRugiLabaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                        
                        $yyyy = sqlsrv_query($conn, $xxxx);
                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                        $abc = "select SUM (NilaiHide) from dbo.LaporanRugiLabaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%'";
                        //echo $abc;
                        $bcd = sqlsrv_query($conn, $abc);
                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                        $a=$b[0];


                        $h4=$H1++;

                        $objWorkSheet->SetCellValue("B".$h4,$zzzz[0]);    
                        $objWorkSheet->SetCellValue("C".$h4,$zzzz[1]);  
                        $objWorkSheet->SetCellValue("D".$h4, number_format($a), PHPExcel_Cell_DataType::TYPE_STRING);  


                            $h5 = 6;
                            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $from = date('Y-m-01', strtotime($from1));
                            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $to = date('Y-m-t', strtotime($to3));
                            $to1 = date('m', strtotime($to3));
                            $to2 = date('Y', strtotime($to3));
                            $name = $_SESSION[Name];
                            $xxxxx ="select * from dbo.LaporanRugiLabaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                            //echo $xxxxx;
                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  


                            $h5=$H1++;

                            $objWorkSheet->SetCellValue("B".$h5,$zzzzz[0]);    
                            $objWorkSheet->SetCellValue("C".$h5,$zzzzz[1]);  
                            $objWorkSheet->SetCellValue("D".$h5, number_format($zzzzz[7]), PHPExcel_Cell_DataType::TYPE_STRING);  


                        }
                    }
                }

            }


          }
            $objWorkSheet->getStyle('B5:D' .$H1)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objWorkSheet->getStyle('B'.$H1,'Saldo Akhir')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue('B'.$H1,'Saldo Akhir');
            $objWorkSheet->getStyle('D' .$H1, number_format($atotal))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("D".$H1, number_format($atotal), PHPExcel_Cell_DataType::TYPE_STRING);

    }




//exit;
    $objWorkSheet->setTitle('Template COA');

    $fileName = 'TemplateCOA'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
