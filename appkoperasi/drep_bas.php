<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Laporan Anggota");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->getStyle('A1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('A1', 'Laporan Anggota');
    $objWorkSheet->getStyle('A2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A2', 'Member ID :');
    $objWorkSheet->getStyle('A3')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A3')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A3', 'Nama :');
    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A4')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A4', 'Alamat :');
    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('A6')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A6', 'Simpanan');
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E6')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('E6', 'Saldo Tabungan');
    $objWorkSheet->getStyle('M6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('M6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('M6')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('M6', 'Saldo Deposito');
    $objWorkSheet->getStyle('V6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('V6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('V6')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('V6', 'Saldo Pinjaman');
   
    

    //simpanan
    $objWorkSheet->getStyle('A7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A7', 'Jenis Simpanan');
    $objWorkSheet->getStyle('B7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B7', 'Kode Akun');
    $objWorkSheet->getStyle('C7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C7', 'Total');


    //Tabungan
    $objWorkSheet->getStyle('E7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E7', 'No.');
    $objWorkSheet->getStyle('F7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F7', 'No. Akun');
    $objWorkSheet->getStyle('G7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G7', 'Tanggal Buka');
    $objWorkSheet->getStyle('H7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('H7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H7', 'Tanggal Tutup');
    $objWorkSheet->getStyle('I7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('I7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('I7', 'Produk');
    $objWorkSheet->getStyle('J7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('J7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('J7', 'Saldo');
    $objWorkSheet->getStyle('K7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('K7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('K7', 'Status');    
    

    //Deposito
    $objWorkSheet->getStyle('M7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('M7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('M7', 'No.');
    $objWorkSheet->getStyle('N7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('N7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('N7', 'No. Akun');
    $objWorkSheet->getStyle('O7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('O7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('O7', 'Tanggal Buka');
    $objWorkSheet->getStyle('P7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('P7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('P7', 'Tanggal Tutup');
    $objWorkSheet->getStyle('Q7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('Q7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('Q7', 'Produk');
    $objWorkSheet->getStyle('R7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('R7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('R7', 'Suku Bunga');
    $objWorkSheet->getStyle('S7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('S7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('S7', 'Saldo');
    $objWorkSheet->getStyle('T7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('T7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('T7', 'Status'); 

     //Pinjaman
    $objWorkSheet->getStyle('V7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('V7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('V7', 'No.');
    $objWorkSheet->getStyle('W7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('W7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('W7', 'No. Pinjaman');
    $objWorkSheet->getStyle('X7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('X7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('X7', 'Pinjaman Awal');
    $objWorkSheet->getStyle('Y7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('Y7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('Y7', 'Pinjaman disetujui');
    $objWorkSheet->getStyle('Z7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('Z7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('Z7', 'Periode Awal');
    $objWorkSheet->getStyle('AA7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('AA7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('AA7', 'Periode Akhir');
    $objWorkSheet->getStyle('AB7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('AB7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('AB7', 'Produk');
    $objWorkSheet->getStyle('AC7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('AC7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('AC7', 'Sisa Pinjaman');
    $objWorkSheet->getStyle('AD7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('AD7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('AD7', 'Status');



if(isset($_GET['acc'])){
    $no = 1;
    $cs = $_GET['acc'];
    

    $sel = "SELECT * FROM [dbo].[MemberListView] WHERE KID='$_SESSION[KID]' and MemberID='$cs'";
    //echo $sel;
    $cek = sqlsrv_query($conn, $sel);
    
    while($hasil = sqlsrv_fetch_array( $cek, SQLSRV_FETCH_NUMERIC)){
    $tes = $hasil[8];
    $jab = "select * from dbo.JabatanDetail where KodeJabatan = '$tes'";
    //echo $jab;
    $jab1 = sqlsrv_query($conn, $jab);
    $jab2 = sqlsrv_fetch_array( $jab1, SQLSRV_FETCH_NUMERIC);



    $objWorkSheet->getStyle('B2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("B2",$hasil[1]);
    $objWorkSheet->getStyle('B3')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("B3",$hasil[2]);
    $objWorkSheet->getStyle('B4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("B4",$hasil[3]);


                            
    }
    

    
    $row = 8;
    $rw = 8;

    $sql3   = "SELECT sum(a.PaymentBalance) FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$cs') a";
    //echo $sql3;
    $stmt3  = sqlsrv_query($conn, $sql3);
    $row3  = sqlsrv_fetch_array($stmt3, SQLSRV_FETCH_NUMERIC);  
    $jml = $row3[0];

    $ulsql = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$cs') a";
    //echo $ulsql;
    $ulstmt = sqlsrv_query($conn, $ulsql);
    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
        if ($ulrow[6] == '1'){
            $jns = 'Simpanan Pokok';
        } else if ($ulrow[6] == '0'){
            $jns = 'Simpanan Wajib';
        } else {
            $jns = 'Simpanan Sukarela';
        } 
               
        $objWorkSheet->SetCellValueExplicit("A".$row,$jns);
        $objWorkSheet->SetCellValueExplicit("B".$row,$ulrow[3]);
        $objWorkSheet->SetCellValue("C".$row, number_format($ulrow[5],2));
        
        $row++;
        $rw++;
        }        

    $objWorkSheet->getStyle('A8:C' .$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objWorkSheet->getStyle('A'.$row,'Saldo Akhir')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A'.$row,'Saldo Akhir');
    $objWorkSheet->getStyle('C'.$row, number_format($row3[0]))->getFont()->setBold(true);
    $objWorkSheet->SetCellValue("C".$row, number_format($row3[0],2), PHPExcel_Cell_DataType::TYPE_STRING);



    // tabungan
    $ros = 8;
    $tabungan = "SELECT * FROM (select *, ROW_NUMBER() OVER (Order by a.MemberID asc) as row from [dbo].[RegularSavingAcc] a WHERE KID='$_SESSION[KID]' and MemberID='$cs' and status != '0') a";
    //echo $ulsql;
    $ptabungan = sqlsrv_query($conn, $tabungan);
    while($htabungan = sqlsrv_fetch_array( $ptabungan, SQLSRV_FETCH_NUMERIC)){
        $open = '';
        $close = '';
        if ($htabungan[6] == '0') {
            $sts = 'Close';
        } else if($htabungan[6] == '9') {
            $sts = 'Default';
        } else {
            $sts = 'Open';
        }

    $open1 = "select top 1 * from dbo.RegularSavingTrans where AccNumber = '$htabungan[2]' order by TimeStam asc";
    $openproses = sqlsrv_query($conn, $open1);
    $openhasil = sqlsrv_fetch_array($openproses, SQLSRV_FETCH_NUMERIC);
    $opentanggal = $openhasil[3]->format('Y-m-d H:i:s');

    $close = "select * from dbo.RegularSavingClose where AccNo = '$ulrow[2]'";
    //echo $close;
    $closeproses = sqlsrv_query($conn, $close);
    $closehasil = sqlsrv_fetch_array($closeproses, SQLSRV_FETCH_NUMERIC);
    //echo $closehasil;
    if ($closehasil != null ){
        $closetanggal = $closehasil[1]->format('Y-m-d H:i:s');        
    } else {
        $closetanggal = '-';
    }

    $status = "select * from dbo.RegularSavingtype where RegularSavingtype = '$htabungan[3]'";
    $statusproses = sqlsrv_query($conn, $status);
    while ($statushasil = sqlsrv_fetch_array($statusproses, SQLSRV_FETCH_NUMERIC)){
        $type = $statushasil[1];
    }                            



    $objWorkSheet->SetCellValueExplicit("E".$ros,$htabungan[7]);
    $objWorkSheet->SetCellValueExplicit("F".$ros,$htabungan[2]);
    $objWorkSheet->SetCellValueExplicit("G".$ros,$opentanggal);
    $objWorkSheet->SetCellValueExplicit("H".$ros,$closetanggal);
    $objWorkSheet->SetCellValueExplicit("I".$ros,$type);
    $objWorkSheet->SetCellValue("J".$ros, number_format($htabungan[4],2), PHPExcel_Cell_DataType::TYPE_STRING);
    $objWorkSheet->SetCellValueExplicit("K".$ros,$sts);
   

    $ros++;

    }

    $objWorkSheet->getStyle('E8:K' .$ros)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    // Deposito
    $rod = 8;

    $Deposito = "SELECT * FROM (select *, ROW_NUMBER() OVER (Order by a.MemberID asc) as row from [dbo].[TimeDepositReport] a where MemberID='$cs') a";
    //echo $Deposito;
    $tanggalbaku = '1900-01-01 00:00:00.0000000';
    //echo $tanggalbaku;
    $pdeposito = sqlsrv_query($conn, $Deposito);
    while($hdeposito = sqlsrv_fetch_array( $pdeposito, SQLSRV_FETCH_NUMERIC)){
    $open = $hdeposito[7]->format('Y-m-d H:i:s');
    $tglcls = $hdeposito[9]->format('Y-m-d H:i:s');
    $close = '';
    $tanggalbaku = '1900-01-01 00:00:00';
    //echo $tglcls;
    //echo $tanggalbaku;
                                    

    if ($tglcls !== $tanggalbaku ){
        $close = $hdeposito[9]->format('Y-m-d H:i:s');                                                   
    } else {
        $close = '-'; 
    }



    $objWorkSheet->SetCellValueExplicit("M".$rod,$hdeposito[11]);
    $objWorkSheet->SetCellValueExplicit("N".$rod,$hdeposito[2]);
    $objWorkSheet->SetCellValueExplicit("O".$rod,$open);
    $objWorkSheet->SetCellValueExplicit("P".$rod,$close);
    $objWorkSheet->SetCellValueExplicit("Q".$rod,$hdeposito[4]);
    $objWorkSheet->getStyle('R' .$rod)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objWorkSheet->SetCellValue("R".$rod,$hdeposito[5], PHPExcel_Cell_DataType::TYPE_STRING);
    $objWorkSheet->getStyle('S' .$rod)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objWorkSheet->SetCellValue("S".$rod,$hdeposito[6], PHPExcel_Cell_DataType::TYPE_STRING);
    $objWorkSheet->SetCellValueExplicit("T".$rod,$hdeposito[10]);

    $rod++;
    }

    $objWorkSheet->getStyle('M8:T' .$rod)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


    //Pinjaman
    $rop = 8;

    $cuser = "select * from [PaymentGateway].[dbo].[UserMemberKoperasi] where KodeMember = '$cs' and KID = '$_SESSION[KID]'";
    //echo $cuser;
    $puser = sqlsrv_query($conn, $cuser);
    $huser = sqlsrv_fetch_array( $puser, SQLSRV_FETCH_NUMERIC);

    $jmlpn = "SELECT sum(LoanAmmount) FROM [dbo].[LoanList] WHERE UserID = '$huser[1]'";
    //echo $jmlpn;
    $pjmlpn = sqlsrv_query($conn, $jmlpn);
    $hjmlpn = sqlsrv_fetch_array( $pjmlpn, SQLSRV_FETCH_NUMERIC);

    $Pinjaman = "SELECT * from (SELECT *, ROW_NUMBER() OVER (order by a.LoanNumber asc) as row FROM [dbo].[LoanReportBalance] a WHERE MemberID='$cs') a";
    //echo $Pinjaman;
    $ppinjaman = sqlsrv_query($conn, $Pinjaman);
    while($hpinjaman = sqlsrv_fetch_array( $ppinjaman, SQLSRV_FETCH_NUMERIC)){
        $ammoun = "select * from dbo.loanrelease where KodeAppNum = '$hpinjaman[0]'";
        $pammoun = sqlsrv_query($conn, $ammoun);
        $hammoun  = sqlsrv_fetch_array( $pammoun, SQLSRV_FETCH_NUMERIC);

        $opentgl = $hpinjaman[2]->format('Y-m-d H:i:s');
            $closetgl = '';
        if($hpinjaman[3] != ''){
            $closetgl = $hpinjaman[3];
        }



    $objWorkSheet->SetCellValueExplicit("V".$rop,$hpinjaman[9]);
    $objWorkSheet->SetCellValueExplicit("W".$rop,$hpinjaman[0]);
    $objWorkSheet->SetCellValueExplicit("X".$rop,$hpinjaman[1]);
    $objWorkSheet->getStyle('Y' .$rop)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);    
    $objWorkSheet->SetCellValue("Y".$rop, number_format($hammoun[3],2), PHPExcel_Cell_DataType::TYPE_STRING);
    $objWorkSheet->SetCellValueExplicit("Z".$rop,$opentgl);
    $objWorkSheet->SetCellValueExplicit("AA".$rop,$closstgl);
    $objWorkSheet->SetCellValueExplicit("AB".$rop,$hpinjaman[4]);
    $objWorkSheet->getStyle('AC' .$rop)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objWorkSheet->SetCellValue("AC".$rop,$hpinjaman[5], PHPExcel_Cell_DataType::TYPE_STRING);
    $objWorkSheet->SetCellValueExplicit("AD".$rop,$hpinjaman[6]);
    
    $rop++;
    }

    $objWorkSheet->getStyle('V8:AD' .$rop)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objWorkSheet->getStyle('V'.$rop,'Saldo Akhir')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('V'.$rop,'Saldo Akhir');
    $objWorkSheet->getStyle('AC' .$rop, $hjmlpn)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objWorkSheet->getStyle('AC'.$rop, number_format($hjmlpn[0]))->getFont()->setBold(true);
    $objWorkSheet->SetCellValue("AC".$rop, number_format($hjmlpn[0],2), PHPExcel_Cell_DataType::TYPE_STRING);
 }
//exit;
    $objWorkSheet->setTitle('Drep Laporan Anggota');

    $fileName = 'Lapanggota'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
