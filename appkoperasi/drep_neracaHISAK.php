<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Neraca History Akun");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    
    $objWorkSheet->getStyle('D1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D1', 'Detail History Transaksi');
    $objWorkSheet->getStyle('D2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D2', 'No Transaksi :');
    $objWorkSheet->getStyle('D3')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D3')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D3', 'Periode :');

    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No');
    $objWorkSheet->getStyle('B5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('B5', 'Kode Jurnal');
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C5', 'Kode Akun');
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D5', 'Nama Akun');
    $objWorkSheet->getStyle('E5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E5', 'Debit');
    $objWorkSheet->getStyle('F5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('F5', 'Kredit'); 
    $objWorkSheet->getStyle('G5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('G5', 'Deskripsi');    



    if($_GET['st'] == 4 ){

    $objWorkSheet->getStyle('E2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue("E2",$_GET['acc2']);

    $bln = 3;
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
    if ($bulan != 1) {
        if ($bulan != 2 ) {
            if ($bulan != 3) {
                if ($bulan != 4) {
                    if ($bulan !=5) {
                    if ($bulan !=6) {
                        if ($bulan !=7) {
                            if ($bulan !=8) {
                                if ($bulan !=9) {
                                    if ($bulan !=10) {
                                        if ($bulan !=11) {
                                            $bulan = 'Desember';
                                            } else {
                                            $bulan = 'Nopember';
                                            }
                                            } else {
                                        $bulan = 'Oktober';
                                        }
                                        } else {
                                    $bulan = 'September';
                                    }
                                    } else {
                                $bulan = 'Agustus';
                                }
                                } else {
                            $bulan = 'Juli';
                            }
                            } else {
                        $bulan = 'Juni';
                        }
                        } else {
                    $bulan = 'Mei';  
                    }
                    } else {
                $bulan = 'April';       
                }
                } else {
            $bulan = 'Maret';
            }
            } else {
        $bulan = 'Februari';    
        }                               
        } else {
    $bulan = 'Januari';
    }
    
    if ($bulan2 != 1) {
        if ($bulan2 != 2 ) {
            if ($bulan2 != 3) {
                if ($bulan2 != 4) {
                    if ($bulan2 !=5) {
                        if ($bulan2 !=6) {
                            if ($bulan2 !=7) {
                                if ($bulan2 !=8) {
                                    if ($bulan2 !=9) {
                                        if ($bulan2 !=10) {
                                            if ($bulan2 !=11) {
                                                $bulan2 = 'Desember';
                                            } else {
                                            $bulan2 = 'Nopember';
                                            }
                                        } else {
                                         $bulan2 = 'Oktober';
                                        }
                                    } else {
                                    $bulan2 = 'September';
                                    }
                                } else {
                                $bulan2 = 'Agustus';
                                }
                            } else {
                            $bulan2 = 'Juli';
                            }
                        } else {
                        $bulan2 = 'Juni';
                        }
                    } else {
                    $bulan2 = 'Mei';  
                    }
                } else {
                $bulan2 = 'April';       
                }
            } else {
            $bulan2 = 'Maret';
            }
        } else {
        $bulan2 = 'Februari';    
        }                               
    } else {
    $bulan2 = 'Januari';
    }
                        
        $objWorkSheet->getStyle('E'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("E".$bln,$bulan .'-'. $tahun  .' Sampai '. $bulan2 .'-'. $tahun2);

    $no = 1;
    $row = 6;
   

    $from= $_GET['from'];
    $to= $_GET['to']; 
    $aaa = "SELECT * FROM ( SELECT a.KodeJurnal,a.KodeAccount,a.Debet,a.Kredit,a.Keterangan, ROW_NUMBER() OVER (ORDER BY b.Tanggal asc) as row FROM [dbo].[JurnalDetail] a inner join [dbo].[JurnalHeader] b on a.KodeJurnal = b.KodeJurnal where a.KodeJurnal= '$_GET[acc2]' ) a ORDER BY KodeJurnal asc";
    //echo($aaa);
    $bbb = sqlsrv_query($conn, $aaa);
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
    //var_dump($ccc);

        $noacc = '';
        $acc = '';
        $sql   = "select * from dbo.Account where KodeAccount='$ccc[1]'";
        //echo $sql;
        $stmt  = sqlsrv_query($conn, $sql);
        $row1   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
        //var_dump($row1);
        if($row1 != null){
            $noacc = $row1[0];
            $acc = $row1[1];

        }
                $objWorkSheet->SetCellValue("A".$row,$ccc[5]);
                $objWorkSheet->SetCellValue("B".$row,$ccc[0]);
                $objWorkSheet->SetCellValue("C".$row, $noacc);
                $objWorkSheet->SetCellValue("D".$row,$acc);
                $objWorkSheet->getStyle('E' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objWorkSheet->SetCellValue("E".$row, number_format($ccc[2]));
                $objWorkSheet->getStyle('F' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objWorkSheet->SetCellValue("F".$row, number_format($ccc[3]));
                $objWorkSheet->SetCellValue("G".$row, $ccc[4]);

                $objWorkSheet->getStyle('A5:G' .$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
                $no++;
                $row++;
            
    }

    }
//exit;
    $objWorkSheet->setTitle('Drep Neraca History Akun');

    $fileName = 'NeracaHISAK'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
