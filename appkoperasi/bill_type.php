<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
$new = 'hide';
?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ulprocedit = "";
$uldisabled = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.BillType where KodeTipe='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[4];
        $ul3    = $eulrow[7];
        $ul4    = $eulrow[2];
        $ul5    = $eulrow[6];
        $ul6    = $eulrow[5];
        $ul7    = $eulrow[3];
        $ul8    = $eulrow[8];
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='bill_type.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='bill_type.php?page=".$_GET['page']."';</script>";
        }
    }
}
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>
        <script type="text/javascript">
            function demoDisplay() {
                $("#mytext").removeClass('hide');
            }

            function demoVisibility() {
                $("#mytext").addClass('hide');
            }
        </script>

        <h2 class="uppercase"><?php echo lang('Tipe Billing'); ?></h2>

            <form class="form-horizontal" action="procbill_type.php<?=$ulprocedit?>" method="POST">

                <div class="input-field">
                    <input type="text" name="des" class="validate" id="AccountNumber" value="<?=$ul1?>">
                    <label for="AccountNumber"><?php echo lang('Deskripsi'); ?></label>
                </div>

                <div class="input-field">
                    <input type="number" name="cutoff" class="validate price" id="cutoff" value="<?=$ul3?>">
                    <label for="AccountNumber"><?php echo lang('Tanggal Cut Off'); ?></label>
                </div>

                <div class="input-field">
                    <input type="number" name="tempoharibill" class="validate price" id="tempoharibill" value="<?=$ul3?>">
                    <label for="AccountNumber"><?php echo lang('Tempo Hari Bill'); ?></label>
                </div><br>

                <b><?php echo lang('Skema Diskon'); ?></b>
                <table class="table table-bordered table-striped" id="tskema">
                    <thead>
                    <tr>
                        <th><?php echo lang('No'); ?></th>
                        <th><?php echo lang('Jumlah Pelunasan'); ?></th>
                        <th><?php echo lang('Nominal Diskon'); ?></th>
                        <th></th>
                    </tr>
                    <?php
                    $jum = 1;
                    $qwe = "select * from [dbo].[DiskonSkemaBill] where KodeTipe = '$uledit' order by JumlahPelunasan asc";
                    $asd = sqlsrv_query($conn, $qwe);
                    while($zxc = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><?php echo $jum; ?></td>
                            <td><?php echo $zxc[1]; ?></td>
                            <td><?php echo number_format($zxc[2]); ?></td>
                            <td>
                                <a href="procbill_type.php?del=<?php echo $zxc[0]; ?>&u=<?php echo $zxc[1]; ?>"><button type="button" class="btn btn-sm btn-danger btn-delete"><i class="ion-android-close"></i> </button></a>
                            </td>
                        </tr>
                        <?php
                        $jum++;
                    }
                    ?>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                    <tr>
                        <td><?php echo $jum; ?></td>
                        <td><input type="number" id="jumlah" class="validate" value="0"></td>
                        <td><input type="text" id="nominal" class="validate price" value="0"></td>
                        <td><button type="button" class="btn btn-sm btn-success btn-add"><?php echo lang('Tambah'); ?></button></td>
                    </tr>
                    </tfoot>
                </table>

                <div class="input-field">
                    <input type="text" name="amo" class="validate price" id="AccountName" value="<?=$ul4?>">
                    <label for="AccountName"><?php echo lang('Nominal'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="dendabill" class="validate price" id="dendabill" value="<?=$ul5?>">
                    <label for="dendabill"><?php echo lang('Denda Bill'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="adminbill" class="validate price" id="adminbill" value="<?=$ul6?>">
                    <label for="adminbill"><?php echo lang('Admin Bill'); ?></label>
                </div>

                <div style="border: 1px solid #d2d6de;border-radius:15px; margin-bottom: 20px;">
                    <?php
                    $s1 = '';
                    $s0 = '';
                    $s2 = '';
                    if($ul7 == 1) {
                        $s1 = 'checked';
                    }
                    else if($ul7 == 0){
                        $s0 = 'checked';
                    }
                    else{
                        $s2 = 'checked';
                    }
                    ?>
                    <div style="margin-bottom: 30px;">
                        <div class="input-field">
                            <input type="radio" onChange="demoDisplay();" checked name="bank" id="optionsRadios1" value="1" <?php echo $s1; ?>>
                            <label for="optionsRadios1" class="validate"><?php echo lang('Period'); ?><label>
                        </div>
                        <div class="input-field">
                            <input type="radio" onClick="demoVisibility();" name="bank" id="optionsRadios2" value="0" <?php echo $s0; ?>>
                            <label for="optionsRadios2" class="validate"><?php echo lang('Sekali Bill'); ?></label>
                        </div>
                        <div class="input-field">
                            <input type="radio" onClick="demoVisibility();" name="bank"  id="optionsRadios3" value="2" <?php echo $s2; ?>>
                            <label for="optionsRadios3" class="validate"><?php echo lang('Unlimited'); ?></label>
                        </div>
                    </div>
                </div><br>

                <?php
                $h = 'hide';
                if(isset($_GET['edit']) and $ul7 == 1){
                    $h = '';
                }
                ?>
                <div id="mytext" class="<?php echo $h; ?>">
                    <div class="input-field">
                        <input type="number" name="period" class="form-control" id="period" value="<?=$ul2?>">
                        <label for="period" class="validate control-label" style="text-align: left;"><?php echo lang('Periode'); ?></label>
                    </div>
                </div>

                <!-- /.box-body -->
                <?php if($ul0 != '') { ?>
                    <br><h2 class="uppercase"><?php echo lang('Billing'); ?></h2><br>

                    <ul class="faq collapsible animated fadeinright delay-1" data-collapsible="accordion">
                        <?php
                        $no = 1; $n = 1; $nochkbox1 = 0;
                        $qwe = "select * from [dbo].[JabatanView] order by KodeJabatan asc";
                        $asd = sqlsrv_query($conn, $qwe);
                        while($zxc = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                            if($no == 1){
                                $stat = 'active';
                            }
                            else{
                                $stat = '';
                            }
                            ?>
                            <li>
                                <div class="collapsible-header">
                                    <i class="ion-android-arrow-dropdown right"></i> <?php echo ucfirst($zxc[1]); ?>
                                </div>

                                <div class="collapsible-body">
                                    <div class="tab-content">
                                        <?php

                                            ?>
                                            <div class="table-responsive" style="padding: 10px;">
                                                <b>Belum ditambahkan</b>
                                                <table class="table table-hover table-striped table-bordered">
                                                    <tr>
                                                        <th><?php echo lang('Member'); ?></th>
                                                    </tr>
                                                    <?php
                                                    $nochkbox2 = 0;
                                                    $julsql   = "select * from [dbo].[MemberList] where KodeJabatan = '".$zxc[0]."'";
                                                    $julstmt = sqlsrv_query($conn, $julsql);
                                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" name="KodeMember[]" id="cb<?php echo $nochkbox1.$nochkbox2; ?>" value="<?php echo $rjulrow[1]; ?>">
                                                                <label for="cb<?php echo $nochkbox1.$nochkbox2; ?>"><?php echo ' '.$rjulrow[1].' - '.$rjulrow[2]; ?></label>
                                                            </td>
                                                        </tr>
                                                    <?php $nochkbox2++; } ?>
                                                </table>

                                                <br><br>

                                                <b>Sudah ditambahkan</b>
                                                <table class="table table-hover table-striped table-bordered">
                                                    <tr>
                                                        <th><?php echo lang('Member'); ?></th>
                                                        <th><?php echo lang('Jumlah Billing'); ?></th>
                                                    </tr>
                                                    <?php
                                                    $julsql   = "select a.MemberID,
                                                                                    a.Name,
                                                                                    (Select count(*) from dbo.PersonBill b where b.KodeMember = a.MemberID and KodeTipe = '".$_GET[edit]."') as Jumlah from [dbo].[MemberList] a where a.KodeJabatan = '".$zxc[0]."' and a.MemberID in(select KodeMember from dbo.PersonBill where KodeTipe = '".$_GET[edit]."')";
                                                    $julstmt = sqlsrv_query($conn, $julsql);
                                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $rjulrow[0].' - '.$rjulrow[1]; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $rjulrow[2]; ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                            </div>
                                            <?php
                                            $n++;
                                         ?>
                                    </div>
                                </div>

                            </li>
                            <?php $no++; $nochkbox1++; } ?>
                    </ul>x

                    <!-- /.box-body -->
                <?php } ?>

                <div style="margin-top: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                    <?php } else { ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                    <?php } ?>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <a href="bill_type.php" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Batal'); ?></a>
                    <?php } ?>
                </div>

            </form>

        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Daftar Tipe Bill'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><?php echo lang('No'); ?></th>
                            <th><?php echo lang('Kode Tipe Bill'); ?></th>
                            <th><?php echo lang('Deskripsi'); ?></th>
                            <th><?php echo lang('Nominal'); ?></th>
                            <th><?php echo lang('Jenis'); ?></th>
                            <th><?php echo lang('Tanggal Cut Off'); ?></th>
                            <th><?php echo lang('Aksi'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //count
                        $jmlulsql   = "select count(*) from dbo.BillType";
                        $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        }
                        else{
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeTipe asc) as row FROM [dbo].[BillType]) a WHERE row between '$posisi' and '$batas'";

                        //$ulsql = "select * from [dbo].[BasicSavingType]";

                        $ulstmt = sqlsrv_query($conn, $ulsql);

                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                            $status = '';
                            if($ulrow[3] == 1){
                                $status = lang('Period');
                            }
                            else if($ulrow[3] == 0){
                                $status = lang('Sekali Bill');
                            }
                            else{
                                $status = lang('Unlimited');
                            }
                            ?>
                            <tr>
                                <td><?=$ulrow[9];?></td>
                                <td><?=$ulrow[0];?></td>
                                <td><?=$ulrow[1];?></td>
                                <td><?=number_format($ulrow[2],0,',','.');?></td>
                                <td><?=$status;?></td>
                                <td><?=$ulrow[7];?></td>
                                <td width="20%" style="padding: 3px">
                                    <div class="btn-group" style="padding-right: 15px">
                                        <a href="bill_type.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }

                        $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix right">
                    <div style="text-align: center;">
                        <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>
                        <?php
                        $reload = "bill_type.php?";
                        $page = intval($_GET["page"]);
                        $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                        if( $page == 0 ) $page = 1;

                        $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                        echo "<div>".paginate($reload, $page, $tpages)."</div>";
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
var tampung = [];

$('.btn-delete').click(function(){
   if(confirm('<?php echo lang('Urutan dibawah kolom ini akan dihapus juga. Apakah anda yakin'); ?> ?') == false){
   return false;
   }
});

$('.btn-add').click(function(){
    var jumlah = $('#jumlah').val();
    var nominal = $('#nominal').val();

    if(jumlah == '' || nominal == ''){
    alert('<?php echo lang('Harap isi inputan dengan benar'); ?>');
    return false;
    }
    else{
    $.ajax({
           url : "ajax_addskemabill.php",
           type : 'POST',
           dataType : 'json',
           data: { jumlah: jumlah, nominal: nominal},   // data POST yang akan dikirim
           success : function(data) {



           $("#tskema tbody").append("<tr>" +
                                     "<td></td>" +
                                     "<td><input type='hidden' name='jumlah[]' value="+ data.jumlah +">" + data.jumlah + "</td>" +
                                     "<td><input type='hidden' name='nominal[]' value="+ data.nominal +">" + data.nominal + "</td>" +
                                     "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='fa fa-trash-o'></i></a></td>" +
                                     "</tr>");
           },
           error : function() {
           alert('<?php echo lang('Telah terjadi error'); ?>');
           return false;
           }
           });
    }
});
</script>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer_new.php');
?>
