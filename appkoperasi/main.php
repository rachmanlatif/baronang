      <?php require('header.php');     
			require('sidebar-left.php');
			require('content-header.php');?>
      
          <form class="form-horizontal">
          <!-- Left col -->
          <section class="col-lg-6 connectedSortable">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Setting</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="PaymentPriority" class="col-sm-4 control-label">Payment Priority</label>
                </div>
                <div class="form-group">
                  <label for="BasicSaving" class="col-sm-5 control-label">Basic Saving</label>

                  <div class="col-sm-2">
                    <input type="email" class="form-control" id="BasicSaving" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="Loan" class="col-sm-5 control-label">Loan</label>
                  <div class="col-sm-2">
                    <input type="email" class="form-control" id="Loan" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="StoreCredit" class="col-sm-5 control-label">Store Credit</label>
                  <div class="col-sm-2">
                    <input type="email" class="form-control" id="StoreCredit" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="MonthlyCutOfDate" class="col-sm-4 control-label">Monthly Cut Of Date</label>
                </div>
                <div class="form-group">
                  <label for="BasicSaving" class="col-sm-5 control-label">Basic Saving</label>
                  <div class="col-sm-2">
                    <input type="email" class="form-control" id="BasicSaving" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="RegulerSavingsInterest" class="col-sm-5 control-label">Reguler Savings Interest</label>
                  <div class="col-sm-2">
                    <input type="email" class="form-control" id="RegulerSavingsInterest" placeholder="">
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section class="col-md-6 connectedSortable">
            <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="cooperativeID" class="col-sm-4 control-label" style="text-align: left;">Daily Cut Of Hour</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" id="cooperativeID" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="PaymentPriority" class="col-sm-4 control-label" style="text-align: left;">Credit days (Yearly)</label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="Days" placeholder="Days">
                  </div>
                  <div class="col-sm-2">
                    <label for="PaymentPriority" class="control-label" style="text-align: left;">Real Days</label>
                  </div>
                  <div class="col-sm-4">
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" class="minimal" id="optionsRadios1" value="option1" checked>
                        Yes
                      </label>
                      <label>
                        <input type="radio" name="optionsRadios" class="minimal" id="optionsRadios2" value="option2">
                        No
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="BasicSaving" class="col-sm-4 control-label"></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" id="Weeks" placeholder="Weeks">
                  </div>
                </div>
                <div class="form-group">
                  <label for="Loan" class="col-sm-4 control-label"></label>
                  <div class="col-sm-2">
                    <input type="Text" class="form-control" id="Months" placeholder="Months">
                  </div>
                </div>
                <div class="form-group">
                  <label for="StoreCredit" class="col-sm-4 control-label" style="text-align: left;">Credit days (Monthly)</label>
                  <div class="col-sm-2">
                    <input type="Text" class="form-control" id="CreditDays" placeholder="">
                  </div>
                  <label for="PaymentPriority" class="col-sm-2 control-label" style="text-align: left;">Real Days</label>
                    <div class="col-sm-4">
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" class="minimal" id="optionsRadios1" value="option1" checked>
                          Yes
                        </label>
                        <label>
                          <input type="radio" name="optionsRadios" class="minimal" id="optionsRadios2" value="option2">
                          No
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="Interest Text" class="col-sm-4 control-label" style="text-align: left;">Interest Text</label>
                    <div class="col-sm-4">
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" class="minimal" id="optionsRadios1" value="option1" checked>
                          Yes
                        </label>
                        <label>
                          <input type="radio" name="optionsRadios" class="minimal" id="optionsRadios2" value="option2">
                          No
                        </label>
                      </div>
                    </div>
                    <label for="Interest Text" class="col-sm-2 control-label" style="text-align: center;padding-top:0;">Tax Rate(%)</label>
                    <div class="col-sm-2">
                      <input type="Text" class="form-control" id="CreditDays" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="MinimumInterest" class="col-sm-4 control-label" style="text-align: left;padding-top:0;">Minimum Interest Amount To Be Taxed</label>
                    <div class="col-sm-4">
                      <input type="email" class="form-control" id="MinimumInterest" placeholder="">
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="reset" class="btn btn-default">Erase</button>
                  <button type="submit" class="btn btn-info pull-right">Save</button>
                </div>
                <!-- /.box-footer -->
              </div>
            </section> 
          </form>

      <?php require('content-footer.php');?>

      <?php require('footer.php');?>
