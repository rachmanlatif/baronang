<?php
require("lib/PHPMailer_5.2.0/class.phpmailer.php");

include("connect.php");

$kba = $_POST['kba'];

if($kba == ""){
    messageAlert('Harap isi seluruh kolom','info');
    header('Location: edc.php');
}
else{
    $x = "select* from [KoneksiKoperasiBaronang].[dbo].[ListKoperasi] where KID = '$_SESSION[KID]' and Status = 1";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        $param = $z[0].date('ymdHisms');
        $key = getKey($param);

        $sql = "exec Gateway.[dbo].[ProsesEDCList] '$z[0]', '$kba','$key'";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            messageAlert('Berhasil menambahkan EDC','success');
            header('Location: edc.php');
        }
        else{
            messageAlert('Gagal menambahkan EDC','danger');
            header('Location: edc.php');
        }
    }
    else{
        messageAlert('Koperasi tidak ditemukan','warning');
        header('Location: edc.php');
    }
}

function getKey($param){
    $options = [
        'cost' => 11,
        'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
    ];
    $key = password_hash($param, PASSWORD_BCRYPT, $options);

    return $key;
}
?>