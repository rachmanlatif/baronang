<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Tagihan Bulanan'); ?></h2><br>

        <div class="box-body">
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Member ID</th>
                        <th><?php echo lang('Nama'); ?></th>
                        <th><?php echo lang('Hutang Simpanan Pokok'); ?></th>
                        <th><?php echo lang('Hutang Simpanan Wajib'); ?></th>
                        <th><?php echo lang('Hutang Simpanan Sukarela'); ?></th>
                        <th><?php echo lang('Pokok Pinjaman'); ?></th>
                        <th><?php echo lang('Bunga Pinjaman'); ?></th>
                        <th><?php echo lang('Belanja Toko'); ?></th>
                        <th><?php echo lang('Toko'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from dbo.TagihanBulanan";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[TagihanBulanan]) a WHERE row between '$posisi' and '$batas'";
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    ?>
                        <tr>
                            <td><?php echo $ulrow[12]; ?></td>
                            <td><?php echo $ulrow[0]; ?></td>
                            <td><?php echo $ulrow[1]; ?></td>
                            <td><?php echo number_format($ulrow[3]); ?></td>
                            <td><?php echo number_format($ulrow[5]); ?></td>
                            <td><?php echo number_format($ulrow[7]); ?></td>
                            <td><?php echo number_format($ulrow[8]); ?></td>
                            <td><?php echo number_format($ulrow[9]); ?></td>
                            <td><?php echo number_format($ulrow[10]); ?></td>
                            <td><?php echo number_format($ulrow[11]); ?></td>
                        </tr>
                    <?php
                    }
                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix right">
                <div style="text-align: center;">
                    Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                    <?php
                    $reload = "rep_billing.php?";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages, $adjacents)."</div>";
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer_new.php');
?>
