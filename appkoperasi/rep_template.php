<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">

<style>
    .tengah{
        text-align:center;
        }
    .kiri{
        text-align:left;
        }
    .kanan{
        text-align:right;
        }

    .tab4 { 
       margin-left: 40px; 
}

</style>




    <div class="page-content">

        <h3 class="box-title"><?php echo lang('Perubahan Template'); ?></h3>

        <div class="form-inputs">
            <form action="proc_template.php" target="_blank" method="POST">
                <h5 class="box-title"><?php echo lang('Template'); ?></h5>
                <div class="input-field">
                    <input type="text" name="nama" class="nama" id="nama" value="">
                    <label for=""><?php echo lang('Nama'); ?></label>
                </div>
                <div class="input-field">
                    <select id="lap" name="lap" class="browser-default">
                        <option type="text" name="lap" id="lap" value="<?php echo $_GET['set']; ?>">- <?php echo lang('Pilih Laporan'); ?> -</option>
                        <?php
                        $bln=array(00=>"Neraca","Laba-Rugi");
                        //echo $bln;
                        $from='';
                        for ($bulan=00; $bulan<=1; $bulan++) { 
                            if ($_GET['set'] == $bulan){
                                $ck="selected";
                            } 
                            else {
                                $ck="";
                            }
                             echo "<option value='$bulan' $ck>$bln[$bulan]</option>";
                        }
                        ?>
                    </select>
                </div>
                
                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                </div>

                <div style="margin-top: 30px;"> 
                    <a href="list_template.php?&st=<?php echo @$_GET['st']; ?>&set=00" target="_blank"><button type="button" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('List Edit Template'); ?></button></a>
                </div>

                <?php if($_GET['st'] == 5 ){ ?>
                <input type="hidden" name="st" id="st" value="5">
                <table class="table table-bordered m-t-30">
                        <tr>
                            <th colspan="2" width="50%"><?php echo lang('Aktiva'); ?></th>
                            <th colspan="2" width="50%"><?php echo lang('Pasiva'); ?></th>
                        </tr>
                        <tr>
                            <td colspan="2" width="50%">
                                <table class="table table-bordered table-striped" width="50%">
                                    <!-- <td><input type="checkbox" name="cek[]" id="1" value = "1"> -->
                                    <!-- <label for="1" class="custom-unchecked"><?php echo 'Pilih Semua'; ?></label></td> -->
                                        
                                    <tbody>
                                        <?php
                                        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                        $from = date('Y-m-01', strtotime($from1));
                                        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                        $to = date('Y-m-d', strtotime('-1 days'));
                                        //echo $to;
                                        $to1 = date('m', strtotime($to));
                                        $to2 = date('Y', strtotime($to));
                                        $to4 = date('d', strtotime($to));

                                        $name = $_SESSION[Name];
                                        $x = "select* from dbo.GroupAcc where KodeGroup in('1.0.00.00.000')";
                                        $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('1.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2'";
                                            //echo $abc;
                                            $bcd = sqlsrv_query($conn, $abc);
                                            while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                $total = $def[0];
                                            }
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                            $m=$z[0];
                                            $m1 = str_replace(".","",$m);
                                            ?>
                                           
                                            <tr>
                                                <td><input type="checkbox" name="cek[]" id="<?php echo $m1; ?>" value = "<?php echo $m; ?>">
                                                <label for="<?php echo $m1; ?>" class="custom-unchecked"><?php echo ($z[0]); ?></label></td>
                                                <td><?php echo ($z[1]); ?></td>
                                                <td style="text-align: right;"><?php if ($z[0] != null){
                                                    $amn = "select * from dbo.AccountBalance where KodeAccount = '$z[0]' and Tanggal = '$to'";
                                                    $nma = sqlsrv_query($conn, $amn);
                                                    $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                    $man1 = $man[10];
                                                    } else {
                                                        $man1 = 0;
                                                        } echo number_format($man1,2); ?>
                                                </td>
                                            </tr>

                                            <?php
                                            $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                            $yy = sqlsrv_query($conn, $xx);
                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                                $n=$zz[0];
                                                $n1 = str_replace(".","",$n);
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" name="cek[]" id="<?php echo $n1; ?>" value = "<?php echo $n; ?>">
                                                    <label for="<?php echo $n1; ?>" class="custom-unchecked">&emsp;<?php echo ($zz[0]); ?></label></td>
                                                    <td><?php echo ($zz[1]); ?><!-- <?php echo $n; ?></td> -->
                                                    <td style="text-align: right;"><?php if ($z[0] != null){
                                                        $amn = "select * from dbo.AccountBalance where KodeAccount = '$zz[0]' and Tanggal = '$to'";
                                                        $nma = sqlsrv_query($conn, $amn);
                                                        $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                        $man1 = $man[10];
                                                        } else {
                                                            $man1 = 0;
                                                            } echo number_format($man1,2); ?>
                                                    </td>
                                                </tr>

                                                <?php
                                                $xxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and userName = '$name' ORDER BY KodeAccount Asc"; 
                                                //echo $xxx;    
                                                $yyy = sqlsrv_query($conn, $xxx);           
                                                while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                    $o=$zzz[0];
                                                    $o1 = str_replace(".","",$o);
                                                        //echo $o1;
                                                    ?>
                                                    <tr>
                                                        <td><input type="checkbox" name="cek[]" id="<?php echo $o1; ?>" value = "<?php echo $o; ?>">
                                                        <label for="<?php echo $o1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></label></td>
                                                        <td><?php echo ($zzz[1]); ?><!-- <?php echo $o1; ?></td> -->
                                                        <td style="text-align: right;"><?php if ($z[0] != null){
                                                            $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzz[0]' and Tanggal = '$to'";
                                                            $nma = sqlsrv_query($conn, $amn);
                                                            $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                            $man1 = $man[10];
                                                            } else {
                                                                $man1 = 0;
                                                                } echo number_format($man1,2); ?>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    $xxxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                    //echo $xxxx;
                                                    $yyyy = sqlsrv_query($conn, $xxxx);
                                                    while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                        $p = $zzzz[0];
                                                        $p1 = str_replace(".","",$p);
                                                        ?>
                                                        <tr>        
                                                            <td><input type="checkbox" name="cek[]" id="<?php echo $p1; ?>" value = "<?php echo $p; ?>"><label for="<?php echo $p1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></label></td>
                                                            <td><?php echo ($zzzz[1]); ?></td>
                                                            <td style="text-align: right;"><?php if ($z[0] != null){
                                                                $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzz[0]' and Tanggal = '$to'";
                                                                $nma = sqlsrv_query($conn, $amn);
                                                                $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                $man1 = $man[10];
                                                                } else {
                                                                    $man1 = 0;
                                                                    } echo number_format($man1,2); ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                        $from = date('Y-m-01', strtotime($from1));
                                                        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                        $to = date('Y-m-d', strtotime('-1 days'));
                                                        $to1 = date('m', strtotime($to));
                                                        $to2 = date('Y', strtotime($to));
                                                        $name = $_SESSION[Name];
                                                        $xxxxx ="select * from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                            //echo $xxxxx;
                                                        $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                        while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  
                                                            $q=$zzzzz[0];
                                                            $q1 = str_replace(".","",$q);    
                                                            ?>
                                                            <tr>
                                                                <td><input type="checkbox" name="cek[]" id="<?php echo $q1; ?>" value = "<?php echo $q; ?>">
                                                                <label for="<?php echo $q1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></label></td>
                                                                <td><?php echo ($zzzzz[1]); ?></td>
                                                                <td style="text-align: right;"><?php if ($z[0] != null){
                                                                    $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzzz[0]' and Tanggal = '$to'";
                                                                    $nma = sqlsrv_query($conn, $amn);
                                                                    $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                    $man1 = $man[10];
                                                                    } else {
                                                                        $man1 = 0;
                                                                        } echo number_format($man1,2); ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php  } ?>           
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </td>

                            <td colspan="2" width="50%">
                                <table class="table table-bordered table-striped" width="50%">
                                    <tbody>
                                        <?php
                                        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                        $from = date('Y-m-01', strtotime($from1));
                                        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                        $to = date('Y-m-d', strtotime('-1 days'));
                                        //echo $to;
                                        $to1 = date('m', strtotime($to));
                                        $to2 = date('Y', strtotime($to));
                                        $to4 = date('d', strtotime($to));
                                        $name = $_SESSION[Name];

                                        $x = "select* from dbo.GroupAcc where KodeGroup in('2.0.00.00.000','3.0.00.00.000')";
                                        //echo $x;
                                        $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('2.0.00.00.000','3.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2'";
                                            //echo $abc;
                                            $bcd = sqlsrv_query($conn, $abc);
                                            while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                $total1 = $def[0];
                                            }
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                            $r=$z[0];
                                            $r1 = str_replace(".","",$r);
                                            ?>
                                            <tr>
                                                <td><input type="checkbox" name="cek[]" id="<?php echo $r1; ?>" value="<?php echo $r; ?>">
                                                <label for="<?php echo $r1; ?>" class="custom-unchecked"><?php echo ($z[0]); ?></td>
                                                <td><?php echo ($z[1]); ?><?php echo $r; ?></td>
                                                <td style="text-align: right;"><?php if ($z[0] != null){
                                                    $amn = "select * from dbo.AccountBalance where KodeAccount = '$z[0]' and Tanggal = '$to'";
                                                    $nma = sqlsrv_query($conn, $amn);
                                                    $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                    $man1 = $man[10];
                                                    } else {
                                                        $man1 = 0;
                                                        } echo number_format($man1,2); ?>
                                                </td>
                                            </tr>

                                            <?php
                                            $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                            $yy = sqlsrv_query($conn, $xx);
                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                                $s=$zz[0];
                                                $s1 = str_replace(".","",$s);
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" name="cek[]" id="<?php echo $s1; ?>" value="<?php echo $s; ?>"><label for="<?php echo $s1; ?>" class="custom-unchecked">&emsp;<?php echo ($zz[0]); ?></td>
                                                    <td><?php echo ($zz[1]); ?></td>
                                                    <td style="text-align: right;"><?php if ($z[0] != null){
                                                    $amn = "select * from dbo.AccountBalance where KodeAccount = '$zz[0]' and Tanggal = '$to'";
                                                    $nma = sqlsrv_query($conn, $amn);
                                                    $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                    $man1 = $man[10];
                                                    } else {
                                                        $man1 = 0;
                                                        } echo number_format($man1,2); ?>
                                                    </td>
                                                </tr>

                                                <?php
                                                $xxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and userName = '$name' ORDER BY KodeAccount Asc"; 
                                                //echo $xxx;    
                                                $yyy = sqlsrv_query($conn, $xxx);           
                                                while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                    $o=$zzz[0];
                                                    $o1 = str_replace(".","",$o);
                                                        //echo $o1;
                                                    ?>
                                                    <tr>
                                                        <td><input type="checkbox" name="cek[]" id="<?php echo $o1; ?>" value = "<?php echo $o; ?>">
                                                        <label for="<?php echo $o1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></label></td>
                                                        <td><?php echo ($zzz[1]); ?><!-- <?php echo $o1; ?></td> -->
                                                        <td style="text-align: right;"><?php if ($z[0] != null){
                                                            $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzz[0]' and Tanggal = '$to'";
                                                            $nma = sqlsrv_query($conn, $amn);
                                                            $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                            $man1 = $man[10];
                                                            } else {
                                                                $man1 = 0;
                                                                } echo number_format($man1,2); ?>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    $xxxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                        //echo $xxxx;
                                                    $yyyy = sqlsrv_query($conn, $xxxx);
                                                    while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                        $p = $zzzz[0];
                                                        $p1 = str_replace(".","",$p);  
                                                        ?>
                                                        <tr>        
                                                            <td><input type="checkbox" name="cek[]" id="<?php echo $p1; ?>" value = "<?php echo $p; ?>"><label for="<?php echo $p1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></label></td>
                                                            <td><?php echo ($zzzz[1]); ?></td>
                                                            <td style="text-align: right;"><?php if ($z[0] != null){
                                                                $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzz[0]' and Tanggal = '$to'";
                                                                $nma = sqlsrv_query($conn, $amn);
                                                                $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                $man1 = $man[10];
                                                                } else {
                                                                    $man1 = 0;
                                                                    } echo number_format($man1,2); ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                        $from = date('Y-m-01', strtotime($from1));
                                                        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                        $to = date('Y-m-d', strtotime('-1 days'));
                                                        $to1 = date('m', strtotime($to));
                                                        $to2 = date('Y', strtotime($to));
                                                        $name = $_SESSION[Name];
                                                        $xxxxx ="select * from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                            //echo $xxxxx;
                                                        $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                        while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  
                                                            $q=$zzzzz[0];
                                                            $q1 = str_replace(".","",$q);    
                                                            ?>
                                                            <tr>
                                                                <td><input type="checkbox" name="cek[]" id="<?php echo $q1; ?>" value = "<?php echo $q; ?>">
                                                                <label for="<?php echo $q1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></label></td>
                                                                <td><?php echo ($zzzzz[1]); ?></td>
                                                                <td style="text-align: right;"><?php if ($z[0] != null){
                                                                    $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzzz[0]' and Tanggal = '$to'";
                                                                    $nma = sqlsrv_query($conn, $amn);
                                                                    $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                    $man1 = $man[10];
                                                                    } else {
                                                                        $man1 = 0;
                                                                        } echo number_format($man1,2); ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>      
                                            <?php } ?>    
                                            <!-- sub 1 -->
                                        <?php $atotal1=$b[0]; } ?>
                                    </tbody>
                                </table>
                            </td>
                            <tr>
                                <th><?php echo lang('Total Aktiva'); ?></th>
                                <th><span class="right"><?php echo number_format($total,2); ?></span></th>
                                <th><?php echo lang('Total Pasiva'); ?></th>
                                <th><span class="right"><?php echo number_format($total1,2); ?></span></th>
                            </tr>
                        </tr>
                </table>
                <?php } ?>

                <?php if($_GET['st'] == 4 ){ ?>
                    <input type="hidden" name="st" id="st" value="4">
                    <div class="table-responsive m-t-30">
                        <?php
                        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                        $from = date('Y-m-01', strtotime($from1));
                        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                        $to = date('Y-m-t', strtotime($to3));
                        $to1 = date('m', strtotime($to3));
                        $to2 = date('Y', strtotime($to3));
                        $a = "exec dbo.ProsesGenerateLaporanRugiLaba '$from','$to','$_SESSION[UserID]''";
                        //echo $a;
                        $b = sqlsrv_query($conn, $a);
                        ?>

                        <table class="table table-bordered">
                            <tr>
                                <th colspan="2" width="50%"><?php echo lang('Laba Rugi'); ?></th>
                            </tr>
                            <tr>
                                <td colspan="2" width="50%">
                                    <table class="table table-bordered table-striped" width="50%">
                                        <tbody>
                                            <?php
                                            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                            $from = date('Y-m-01', strtotime($from1));
                                            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                            $to = date('Y-m-d', strtotime('-1 days'));
                                            //echo $to;
                                            $to1 = date('m', strtotime($to));
                                            $to2 = date('Y', strtotime($to));
                                            $to4 = date('d', strtotime($to));
                                            $name = $_SESSION[Name];

                                            $x = "select * from dbo.GroupAcc where KodeGroup in('4.0.00.00.000', '5.0.00.00.000', '6.0.00.00.000') and KodeGroup in (select account from templatedetail )";
                                                                                    
                                            $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('4.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2'";
                                            //echo $abc;
                                            $bcd = sqlsrv_query($conn, $abc);
                                            while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                $total = $def[0];
                                            }
                                                
                                            $limaenam = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('5.0.00.00.000','6.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2'";
                                            //echo $abc;
                                            $sum = sqlsrv_query($conn, $limaenam);
                                            while($hasilsum = sqlsrv_fetch_array($sum, SQLSRV_FETCH_NUMERIC)){
                                                $total1 = $hasilsum[0];
                                            }

                                            $totalfix1 = $total - $total1;
                                            //echo $totalfix1;

                                            $y = sqlsrv_query($conn, $x);
                                            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                                $m=$z[0];
                                                $m1 = str_replace(".","",$m);
                                                ?>
                                                <tr>
                                                    <td><input type="checkbox" name="cek[]" id="<?php echo $m1; ?>" value ="<?php echo $m; ?>">
                                                    <label for="<?php echo $m1; ?>" class="custom-unchecked"><?php echo ($z[0]); ?></label></td>
                                                    <td><?php echo ($z[1]); ?></td>
                                                    <td style="text-align: right;"><?php if ($z[0] != null){
                                                        $amn = "select * from dbo.AccountBalance where KodeAccount = '$z[0]' and Tanggal = '$to'";
                                                        $nma = sqlsrv_query($conn, $amn);
                                                        $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                        $man1 = $man[10];
                                                        } else {
                                                        $man1 = 0;
                                                        } echo number_format($man1,2); ?>
                                                    </td>
                                                </tr>
                                                    
                                                <?php
                                                $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                                //echo $xx;
                                                $bcd = sqlsrv_query($conn, $abc);
                                                $b = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                $dtotal1=$b[0];
                                                $atotal=$atotal+$dtotal1;
                                                $yy = sqlsrv_query($conn, $xx);
                                                while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                                    $n=$zz[0];
                                                    $n1 = str_replace(".","",$n);
                                                    ?>
                                                    <tr>
                                                        <td><input type="checkbox" name="cek[]" id="<?php echo $n1; ?>" value="<?php echo $n; ?>">
                                                        <label for="<?php echo $n1; ?>" class="custom-unchecked">&emsp;<?php echo ($zz[0]); ?></label></td>
                                                        <td><?php echo ($zz[1]); ?></td>
                                                        <td style="text-align: right;"><?php if ($z[0] != null){
                                                            $amn = "select * from dbo.AccountBalance where KodeAccount = '$zz[0]' and Tanggal = '$to'";
                                                            $nma = sqlsrv_query($conn, $amn);
                                                            $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                            $man1 = $man[10];
                                                            } else {
                                                            $man1 = 0;
                                                            } echo number_format($man1,2); ?>
                                                        </td>
                                                    </tr>


                                                    <?php
                                                    $xxx = "select * from dbo.LaporanRugiLabaViewNew where TypeID = '$zz[0]' and header = 0 and userName = '$name' ORDER BY KodeAccount Asc"; 
                                                    //echo $xxx;    
                                                    $yyy = sqlsrv_query($conn, $xxx);           
                                                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                        $o=$zzz[0];
                                                        $o1 = str_replace(".","",$o);
                                                        ?>
                                                        <tr>

                                                            <td><input type="checkbox" name="cek[]" id="<?php echo $o1; ?>" value = "<?php echo $o; ?>">
                                                            <label for="<?php echo $o1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></label></td>
                                                            <td><?php echo ($zzz[1]); ?></td>
                                                            <td style="text-align: right;"><?php if ($z[0] != null){
                                                                $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzz[0]' and Tanggal = '$to'";
                                                                $nma = sqlsrv_query($conn, $amn);
                                                                $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                $man1 = $man[10];
                                                                } else {
                                                                $man1 = 0;
                                                                } echo number_format($man1,2); ?>
                                                            </td>
                                                        </tr>
                            
                                                        <?php
                                                        $xxxx = "select * from dbo.LaporanRugiLabaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                            //echo $xxxx;
                                                        $yyyy = sqlsrv_query($conn, $xxxx);
                                                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                            $p = $zzzz[0];
                                                            $p1 = str_replace(".","",$p);
                                                            ?>
                                                            <tr>        
                                                                <td><input type="checkbox" name="cek[]" id="<?php echo $p1; ?>" value = "<?php echo $p; ?>"><label for="<?php echo $p1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></label></td>
                                                                <td><?php echo ($zzzz[1]); ?></td>
                                                                <td style="text-align: right;"><?php if ($z[0] != null){
                                                                    $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzz[0]' and Tanggal = '$to'";
                                                                    $nma = sqlsrv_query($conn, $amn);
                                                                    $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                    $man1 = $man[10];
                                                                    } else {
                                                                        $man1 = 0;
                                                                        } echo number_format($man1,2); ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                            $from = date('Y-m-01', strtotime($from1));
                                                            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                            $to = date('Y-m-d', strtotime('-1 days'));
                                                            $to1 = date('m', strtotime($to));
                                                            $to2 = date('Y', strtotime($to));
                                                            $name = $_SESSION[Name];
                                                            $xxxxx ="select * from dbo.LaporanRugiLabaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                                //echo $xxxxx;
                                                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  
                                                                $q=$zzzzz[0];
                                                                $q1 = str_replace(".","",$q);    
                                                                ?>
                                                                <tr>
                                                                    <td><input type="checkbox" name="cek[]" id="<?php echo $q1; ?>" value = "<?php echo $q; ?>">
                                                                    <label for="<?php echo $q1; ?>" class="custom-unchecked">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></label></td>
                                                                    <td><?php echo ($zzzzz[1]); ?></td>
                                                                    <td style="text-align: right;"><?php if ($z[0] != null){
                                                                        $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzzz[0]' and Tanggal = '$to'";
                                                                        $nma = sqlsrv_query($conn, $amn);
                                                                        $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                        $man1 = $man[10];
                                                                        } else {
                                                                            $man1 = 0;
                                                                            } echo number_format($man1,2); ?>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php $dtotal1+=$b[0];} ?>
                                        </tbody>
                                    </table>
                                </td>
                                <tr>
                                    <th><?php echo ('Total Laba Rugi'); ?></th>
                                    <th><span class="right"><?php echo number_format($totalfix1,2); ?></span></th>
                                </tr>
                            </tr>
                        </table>
                    </div>
                <?php } ?>

            </form>
        </div>

    </div>


<!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview1").shieldTreeView();
    });
</script>
<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview3").TreeView();
        collapsed: true,
        unique: true,
        persist: “location”
    });
</script>

<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

    $("#button1").click(function(){
        var level = $("#level").val();
        window.open("level_neraca.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&level="+level);
    }); 
    
    function arHide(setID) {
            if( $(setID).hasClass('hide') ) $(setID).removeClass('hide');
            else $(setID).addClass('hide');
        }


$(function() {

  $('input[type="checkbox"]').change(checkboxChanged);

  function checkboxChanged() {
    var $this = $(this),
        checked = $this.prop("checked"),
        container = $this.parent(),
        siblings = container.siblings();

    container.find('input[type="checkbox"]')
    .prop({
        indeterminate: false,
        checked: checked
    })
    .siblings('label')
    .removeClass('custom-checked custom-unchecked custom-indeterminate')
    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

    checkSiblings(container, checked);
  }

  function checkSiblings($el, checked) {
    var parent = $el.parent().parent(),
        all = true,
        indeterminate = false;

    $el.siblings().each(function() {
      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
    });

    if (all && checked) {
      parent.children('input[type="checkbox"]')
      .prop({
          indeterminate: false,
          checked: checked
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(checked ? 'custom-checked' : 'custom-unchecked');

      checkSiblings(parent, checked);
    } 
    else if (all && !checked) {
      indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

      parent.children('input[type="checkbox"]')
      .prop("checked", checked)
      .prop("indeterminate", indeterminate)
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

      checkSiblings(parent, checked);
    } 
    else {
      $el.parents("li").children('input[type="checkbox"]')
      .prop({
          indeterminate: true,
          checked: false
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass('custom-indeterminate');
    }
  }
});


</script>




<?php require('footer_new.php');?>
