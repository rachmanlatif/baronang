<?php
@session_start();
error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content txt-black">

            <div class="row">
                <div class="col s4">
                    <button id="btn_inoutqty" class="btn btn-large width-100 primary-color waves-efeect waves-light">In Out</button>
                </div>

                <div class="col s4">
                    <button id="btn_aktifasi" class="btn btn-large width-100 primary-color waves-efeect waves-light">Aktifasi</button>
                </div>

                <div class="col s4">
                    <button id="btn_member" class="btn btn-large width-100 primary-color waves-efeect waves-light">Member</button>
                </div>
            </div>

            <div class="m-t-30 hide" id="inoutqty">
                <form action="" method="post">
                    <div class="row">
                        <div class="col s4">
                            <b>Lokasi</b> : 
                            <select class="browser-default" id="loc_ioqty" name="loc_ioqty">
                                <?php
                                $sq_ioqty = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
                                $qr_ioqty = sqlsrv_query($conn, $sq_ioqty);
                                while($dr_ioqty = sqlsrv_fetch_array($qr_ioqty)) { ?>
                                    <option value="<?php echo $dr_ioqty[0]; ?>"><?php echo $dr_ioqty[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s4">
                            <b>Kendaraan</b> : <br>
                            <select class="browser-default" id="vhcl_ioqty" name="vhcl_ioqty">
                                <?php
                                $sq_vhcl = "SELECT Code, Name from [dbo].[VehicleType] where Status='1' order by Name ASC";
                                $qr_vhcl = sqlsrv_query($conn, $sq_vhcl);
                                while($dr_vhcl = sqlsrv_fetch_array($qr_vhcl)) { ?>
                                    <option value="<?php echo $dr_vhcl[0]; ?>"><?php echo $dr_vhcl[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s4">
                            <b>Tanggal</b> : <br>
                            <input type="date" name="tgl_ioqty" id="tgl_ioqty">
                        </div>
                    </div>

                    <button type="submit" name="fbtn_ioqty" id="fbtn_ioqty" class="btn btn-large width-100 primary-color waves-efeect waves-light m-t-10">OK</button>
                </form>
            </div>

            <div class="m-t-30 hide" id="aktifasi">
                <form action="" method="post">
                    <?php /*
                    <div class="row">
                         <div class="col s6">
                            <b>Lokasi</b> : 
                            <select class="browser-default" id="loc_aktifasi" name="loc_akaktifasi">
                                <?php
                                $sq_ioqty = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
                                $qr_ioqty = sqlsrv_query($conn, $sq_ioqty);
                                while($dr_ioqty = sqlsrv_fetch_array($qr_ioqty)) { ?>
                                    <option value="<?php echo $dr_ioqty[0]; ?>"><?php echo $dr_ioqty[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <b>Kendaraan</b> : <br>
                            <select class="browser-default" id="vhcl_aktifasi" name="vhcl_akaktifasi">
                                <?php
                                $sq_vhcl = "SELECT VehicleID, Name from [dbo].[VehicleType] where Status='1' order by Name ASC";
                                $qr_vhcl = sqlsrv_query($conn, $sq_vhcl);
                                while($dr_vhcl = sqlsrv_fetch_array($qr_vhcl)) { ?>
                                    <option value="<?php echo $dr_vhcl[0]; ?>"><?php echo $dr_vhcl[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    */ ?>

                    <div class="row">
                        <div class="col s6">
                            <b>Bulan</b> : <br>
                            <select class="browser-default" id="bulan_aktifasi" name="bulan_aktifasi">
                                <?php
                                for($x = 1; $x < 13; $x++) { 
                                    $dx = '1-'.$x.'-'.date('Y');
                                    ?>
                                    <option value="<?php echo date('m-t', strtotime($dx)); ?>"><?php echo date('F', strtotime($dx)); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <b>Tahun</b> : <br>
                            <select class="browser-default" id="tahun_aktifasi" name="tahun_aktifasi">
                                <?php
                                for($x = (int)date('Y'); $x > 2014; $x--) { ?>
                                    <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <button type="submit" name="fbtn_aktifasi" id="fbtn_aktifasi" class="btn btn-large width-100 primary-color waves-efeect waves-light m-t-10">OK</button>
                </form>
            </div>

            <div class="m-t-30 hide" id="member">
                <form action="" method="post">
                    <div class="row">
                         <div class="col s6">
                            <b>Lokasi</b> : 
                            <select class="browser-default" id="loc_member" name="loc_member">
                                <?php
                                $sq_ioqty = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
                                $qr_ioqty = sqlsrv_query($conn, $sq_ioqty);
                                while($dr_ioqty = sqlsrv_fetch_array($qr_ioqty)) { ?>
                                    <option value="<?php echo $dr_ioqty[0]; ?>"><?php echo $dr_ioqty[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <b>Kendaraan</b> : <br>
                            <select class="browser-default" id="vhcl_member" name="vhcl_member">
                                <?php
                                $sq_vhcl = "SELECT VehicleID, Name from [dbo].[VehicleType] where Status='1' order by Name ASC";
                                $qr_vhcl = sqlsrv_query($conn, $sq_vhcl);
                                while($dr_vhcl = sqlsrv_fetch_array($qr_vhcl)) { ?>
                                    <option value="<?php echo $dr_vhcl[0]; ?>"><?php echo $dr_vhcl[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s6">
                            <b>Bulan</b> : <br>
                            <select class="browser-default" id="bulan_member" name="bulan_member">
                                <?php
                                for($x = 1; $x < 13; $x++) { 
                                    $dx = '1-'.$x.'-'.date('Y');
                                    ?>
                                    <option value="<?php echo $x; ?>"><?php echo date('F', strtotime($dx)); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <b>Tahun</b> : <br>
                            <select class="browser-default" id="tahun_member" name="tahun_member">
                                <?php
                                for($x = (int)date('Y'); $x > 2014; $x--) { ?>
                                    <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <button type="submit" name="fbtn_member" id="fbtn_member" class="btn btn-large width-100 primary-color waves-efeect waves-light m-t-10">OK</button>
                </form>
            </div>

            <?php
            $fbtn_ioqty = @$_POST['fbtn_ioqty'];
            $fbtn_aktifasi = @$_POST['fbtn_aktifasi'];
            $fbtn_member = @$_POST['fbtn_member'];
            ?>

                <div class="table-responsive">
                    <table class="table txt-black" width="100%">
                        <thead>
                            <tr>
                                <?php
                                if(isset($fbtn_ioqty)) { ?>
                                    <th>Tanggal</th>
                                    <th>Lokasi</th>
                                    <th>Kendaraan</th>
                                    <th>Jumlah Masuk</th>
                                    <th>Jumlah Keluar</th>
                                    <th>Pendapatan</th>
                                <?php } 
                                else if(isset($fbtn_aktifasi)) { ?>
                                    <th>Nama</th>
                                    <th>Telp</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Nomor Kartu</th>
                                    <th>Jenis Kendaraan</th>
                                    <th>Tanggal Daftar</th>
                                    <?php
                                }
                                else if(isset($fbtn_member)) { ?>
                                    <th>Nama</th>
                                    <th>Telp</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Nomor Kartu</th>
                                    <th>Bulan</th>
                                    <th>Tahun</th>
                                    <th>Jenis Kendaraan</th>
                                    <th>Lokasi</th>
                                    <?php
                                }
                                ?>
                            </tr>
                        </thead>

                        <tbody>
                            
                                <?php
                                if(isset($fbtn_ioqty)) {
                                    $tgl    = @$_POST['tgl_ioqty'];
                                    $lokasi = @$_POST['loc_ioqty'];
                                    $vhcl   = @$_POST['vhcl_ioqty'];

                                    
                                    $sql1 = "SELECT a.TimeIn,
                                                    b.Nama,
                                                    c.Name, 
                                                    (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn = '$lokasi' and VehicleType = '$vhcl' and TimeIn between '$tgl 00:00:00.000' and '$tgl 23:59:59.999' and MONTH(TimeIn) = 1) as Masuk, 
                                                    (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn = '$lokasi' and VehicleType = '$vhcl' and TimeOut between '$tgl 00:00:00.000' and '$tgl 23:59:59.999') as Keluar, 
                                                    (SELECT Count(TotalDeduct) from [dbo].[ParkingList] where LocationIn = '$lokasi' and VehicleType = '$vhcl') as Profit
                                            from [dbo].[ParkingList] as a 
                                            inner join [dbo].[LocationMerchant] as b
                                                on a.LocationIn = b.LocationID
                                            inner join [dbo].[VehicleType] as c
                                                on a.VehicleType = c.Code
                                            where LocationIn='$lokasi' and VehicleType = '$vhcl' and TimeIn between '$tgl 00:00:00.000' and '$tgl 23:59:59.999'";
                                    /*
                                    SELECT  Convert(date, a.TimeIn), 
                                            b.Nama, 
                                            c.Name, 
                                            (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn = a.LocationIn and Convert(date,TimeIn) = convert(date, a.TimeIn)) as Masuk, 
                                            (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn = a.LocationIn and Convert(date,TimeOut) = convert(date, a.TimeOut)) as Keluar, 
                                            (SELECT SUM(TotalDeduct) from [dbo].[ParkingList] where LocationIn = a.LocationIn and VehicleType = c.Code) as Profit 
                                    from [dbo].[ParkingList] as a 
                                    inner join [dbo].[LocationMerchant] as b 
                                        on a.LocationIn = b.LocationID 
                                    inner join [dbo].[VehicleType] as c 
                                        on a.VehicleType = c.Code 
                                    where b.LocationID='700097LOC0000002' and c.Code = 'C' and Convert(date, a.TimeIn) = '2019-01-09'
                                    group by 
                                        Convert(date, a.TimeIn), 
                                        b.Nama, 
                                        c.Name, 
                                        a.LocationIn
    
                                    $sql =  "SELECT  a.CardNo,
                                                    (Select max(TimeIn) from dbo.ParkingList where CardNo = a.CardNo) as LastIn,
                                                    (Select count(TimeIn) from dbo.ParkingList where CardNo = a.CardNo and MONTH(TimeIn) = 1),
                                                    (Select count(TimeOut) from dbo.ParkingList where CardNo = a.CardNo and MONTH(TimeIn) = 2),
                                                    b.isLink,
                                                    c.Name,
                                                    c.Phone,
                                                    c.KTP,
                                                    c.email
                                            from dbo.MemberLocation a
                                            inner join dbo.MemberCard b
                                                on a.CardNo = b.CardNo
                                            left join dbo.MemberList c
                                                on b.MemberID = c.MemberID
                                            where a.LocationID='$lokasi' and a.CardNo not in (select TipeQR from dbo.AktivasiCompliment) and VehicleType = '$vhcl' and TimeIn between '$tgl 00:00:00.000' and '$tgl 23:59:59.999'";
                                    */
                                    echo $sql1.'<br><br>';
                                    $query1 = sqlsrv_query($conn, $sql1);
                                    while($data1 = sqlsrv_fetch_array($query1)) { ?>
                                        <tr>
                                            <td><?php echo $data1[1]->format('Y-m-d'); ?></td>
                                            <td><?php echo $data1[1]; ?></td>
                                            <td><?php echo $data1[2]; ?></td>
                                            <td><?php echo $data1[3]; ?></td>
                                            <td><?php echo $data1[4]; ?></td>
                                            <td><?php echo $data1[5]; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                else if(isset($fbtn_aktifasi)) {
                                    //$lokasi = @$_POST['loc_aktifasi'];
                                    //$vhcl   = @$_POST['vhcl_aktifasi'];
                                    $bulan  = date('Y').'-'.@$_POST['bulan_aktifasi'];
                                    $tahun  = @$_POST['tahun_aktifasi'];

                                    $tgl_awal = $tahun.'-'.date('m', strtotime($bulan)).'-1';
                                    $tgl_akhir = $bulan;
                                    //echo $tgl_awal.' : '.$tgl_akhir;

                                    $sql =  "SELECT b.Name, b.Phone, b.Email, b.Addr, a.CardNo, c.Name, a.DateRegister
                                            from [KOPX700097].[dbo].[MemberLocation] as a
                                            inner join [KOPX700097].[dbo].[MemberList] as b
                                            on a.MemberID = b.MemberID
                                            inner join [KOPX700097].[dbo].[VehicleType] as c
                                            on a.VehicleID = c.VehicleID
                                            where a.DateRegister between '$tgl_awal' and '$tgl_akhir'";

                                    //echo $sql;
                                    $query = sqlsrv_query($conn, $sql);
                                    while( $data = sqlsrv_fetch_array($query) ) { ?>
                                        <tr>
                                            <td><?php echo $data[0]; ?></td>
                                            <td><?php echo $data[1]; ?></td>
                                            <td><?php echo $data[2]; ?></td>
                                            <td><?php echo $data[3]; ?></td>
                                            <td><?php echo $data[4]; ?></td>
                                            <td><?php echo $data[5]; ?></td>
                                            <td><?php echo $data[6]->format('Y-m-d'); ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                else if(isset($fbtn_member)) {
                                    $lokasi = @$_POST['loc_member'];
                                    $vhcl   = @$_POST['vhcl_member'];
                                    $bulan  = @$_POST['bulan_member'];
                                    $tahun  = @$_POST['tahun_member'];

                                    $sql =  "SELECT c.Name, c.Phone, c.Email, c.Addr, a.CardNo, b.Bulan, b.Year, d.Name, e.Nama, b.Status from [dbo].[MemberLocation]  as a
                                            inner join [dbo].[MemberLocationMonth] as b
                                            on a.CardNo = b.CardNo
                                            inner join [dbo].[MemberList] as c
                                            on a.MemberID = c.MemberID
                                            inner join [dbo].[VehicleType] as d
                                            on a.VehicleID = d.VehicleID
                                            inner join [dbo].[LocationMerchant] as e
                                            on a.LocationID = e.LocationID
                                            where b.Bulan='$bulan' and b.Year='$tahun' and e.LocationID = '$lokasi' and d.VehicleID = '$vhcl' and b.Status='1'
                                            order by b.Bulan ASC, b.Year ASC, c.Name ASC, a.CardNo ASC";

                                    $query = sqlsrv_query($conn, $sql);
                                    //echo $sql;
                                    while( $data = sqlsrv_fetch_array($query) ) { ?>
                                        <tr>
                                            <td><?php echo $data[0]; ?></td>
                                            <td><?php echo $data[1]; ?></td>
                                            <td><?php echo $data[2]; ?></td>
                                            <td><?php echo $data[3]; ?></td>
                                            <td><?php echo $data[4]; ?></td>
                                            <td><?php echo $data[5]; ?></td>
                                            <td><?php echo $data[6]; ?></td>
                                            <td><?php echo $data[7]; ?></td>
                                            <td><?php echo $data[8]; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                
                            
                        </tbody>
                    </table>
                </div>

        </div>
    </div>

    <script type="text/javascript">
        $('#btn_inoutqty').click(function() {
            if( $('#inoutqty').hasClass('hide') ) {
                $('#inoutqty').removeClass('hide');
                $('#aktifasi').addClass('hide');
                $('#member').addClass('hide');
            }
            else {
                $('#inoutqty').addClass('hide');
            }
        });

        $('#btn_aktifasi').click(function() {
            if( $('#aktifasi').hasClass('hide') ) {
                $('#aktifasi').removeClass('hide');
                $('#inoutqty').addClass('hide');
                $('#member').addClass('hide');
            }
            else {
                $('#aktifasi').addClass('hide');
            }
        });

        $('#btn_member').click(function() {
            if( $('#member').hasClass('hide') ) {
                $('#member').removeClass('hide');
                $('#inoutqty').addClass('hide');
                $('#aktifasi').addClass('hide');
            }
            else {
                $('#member').addClass('hide');
            }
        });
    </script>

<?php require('footer.php'); ?>