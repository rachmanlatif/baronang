<?php
session_start();
error_reporting(0);
include "connect.php";
include "connectinti.php";

$tanggal = date('Y-m-d');
//kasir
$q = "SELECT top 1 a.AssignDate, b.UserID, b.Amount, a.CashierID, b.IDCashierAssignUser FROM dbo.CashierAssign a inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where a.Type = 1 and b.Status = 0 and b.UserID='$_SESSION[UserID]' and a.AssignDate='$tanggal' order by a.TimeStamp asc";
$w = sqlsrv_query($conn, $q);
$e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);
if($e != null){
  $transNumber = '';
  $a = "SELECT dbo.getKodeSessionTrans('$_SESSION[KID]','OSESC')";
  $b = sqlsrv_query($conn, $a);
  $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
  if($c != null){
    $transNumber = $c[0];

    $tanggal = date('Y-m-d H:i:s');
    $sql = "exec dbo.ProsesBukaSesi '$transNumber','$e[3]','$_SESSION[UserID]','$e[2]','$e[4]',1";
    $stmt = sqlsrv_query($conn, $sql);
    if($stmt){
        $sql3 = "update dbo.CashierAssignUser set Status = 1 where IDCashierAssignUser = '$e[4]'";
        $stmt3 = sqlsrv_query($conn, $sql3);

        $_SESSION['sesikasir'] = $transNumber;
    }
  }
}

//teller
$q = "SELECT top 1 a.AssignDate, b.UserID, b.Amount, a.CashierID, b.IDCashierAssignUser FROM dbo.CashierAssign a inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where a.Type = 0 and b.Status = 0 and b.UserID='$_SESSION[UserID]' and a.AssignDate='$tanggal' order by a.TimeStamp asc";
$w = sqlsrv_query($conn, $q);
$e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);
if($e != null){
  $transNumber = '';
  $a = "SELECT dbo.getKodeSessionTrans('$_SESSION[KID]','OSEST')";
  $b = sqlsrv_query($conn, $a);
  $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
  if($c != null){
    $transNumber = $c[0];

    $tanggal = date('Y-m-d H:i:s');
    $sql2 = "exec dbo.ProsesBukaSesi '$transNumber','$e[3]','$_SESSION[UserID]','$e[2]','$e[4]',0";
    $stmt2 = sqlsrv_query($conn, $sql2);
    if($stmt2){
        $sql4 = "update dbo.CashierAssignUser set Status = 1 where IDCashierAssignUser = '$e[4]'";
        $stmt4 = sqlsrv_query($conn, $sql4);

        $_SESSION['sesiteller'] = $transNumber;
    }
  }
}

if($stmt or $stmt2){
  messageAlert(lang('Berhasil buka sesi'),'success');
  header('Location: dashboard.php');
}
else{
  messageAlert('Gagal buka sesi','danger');
  header('Location: buka_sesi.php');
}

?>
