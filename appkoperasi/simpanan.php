<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<script language="javascript" type="text/javascript">
    function showCompany(ind) {
        document.frm.submit();
    }
</script>


<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Daftar Simpanan'); ?></h2> <br>
            <div class="box-body">
                <form class="form-horizontal" action="" method = "get" name="frm" id="frm">
                <div class="row">
                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                <label ><?php echo lang('Pilih Simpanan'); ?></label>
                <select id="ind" name="ind" class="browser-default" value="" onChange="showCompany(this.value);">
                    <option value=''> <?php echo ('Semua Produk Simpanan'); ?> </option>
                        <?php
                        $julsql   = "select * from [dbo].[BasicSavingTypeView]";
                        //echo $julsql;
                        $julstmt = sqlsrv_query($conn, $julsql);
                        while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_GET['ind']){echo "selected";} ?>><?=$rjulrow[1];?></option>
                        <?php } ?>
                </select>


                <?php
                if ($_GET['ind']){
                $a = "select * from [dbo].[BasicSavingTypeView] where KodeBasicSavingType='$_GET[ind]'";
                //echo $a;
                $b =  sqlsrv_query($conn, $a);
                $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
                    ?>
                    <div class="Active" style="padding-top: 30px">
                        <label><h4><?php echo ('Tipe Produk Simpanan'); ?></h4></label>
                        <input type="hidden" name="" id="" value="<?php echo $c[1]; ?>" readonly>
                        <input type="text" name="" class="validate" id="regularsavingdescription" value="<?php echo $c[1]; ?>" readonly>
                        
                    </div>

                    <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                            <a href="drep_simpanan.php?id=<?php echo $_GET['ind']; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
                    </div>

                        <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo ('No'); ?></th>    
                                <th><?php echo ('Kode Akun'); ?></th>
                                <!-- <th><?php echo ('Member ID'); ?></th> -->
                                <th><?php echo ('Nama'); ?></th>
                                <th><?php echo ('Saldo'); ?></th> 
                            </tr>
                        </thead>
                        <tbody>
                         <?php
                             //count
                            $ind = $_GET['ind'];
                            $jmlulsql   = "select count(*) from dbo.BasicSavingBalance where KodeBasicSavingType = '$ind'";
                            //echo $jmlulsql;
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }

                            //cek
                            $no1 = 1;
                            $ind = $_GET['ind'];

                            //$ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[RegSavingAccView]) a WHERE KID='$_SESSION[KID]' and RegularSavingType = '$ind' and row between '$posisi' and '$batas'";
                            $ulsql = "SELECT * FROM ( SELECT a.kid, a.MemberID,a.KodeBasicSavingType, a.AccNo, a.BillingBalance,a.PaymentBalance, b.UserID, ROW_NUMBER() OVER (ORDER BY a.MemberID asc) as row FROM [dbo].[BasicSavingBalance] a inner join [PaymentGateway].[dbo].[UserMemberKoperasi] b on a.MemberID = b.KodeMember and a.kid = b.kid where a.KodeBasicSavingType = '$ind') a  where row between '$posisi' and '$batas'";        
                            //echo $ulsql;
                            $ulstmt = sqlsrv_query($conn, $ulsql);
                            while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                                    $sql   = "select * from dbo.MemberListView where MemberID='$ulrow[1]'";
                                    //echo $sql;
                                    $stmt  = sqlsrv_query($conn, $sql);
                                    $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                            ?>
                           <tr>
                                <td><?php echo $ulrow[7]; ?></td>
                                <td align="right"><a href="rep_reg.php?acc=<?php echo $ulrow[3]; ?>&st=1&ind=<?php echo $_GET['ind']; ?>&user=<?php echo $ulrow[6]; ?>"><?php echo $ulrow[3]; ?></a></td>
                                <!-- <td><?php echo $ulrow[1]; ?></td> -->
                                <td><?php echo $row[2]; ?></td>
                                <td style="text-align: right"><?php echo number_format($ulrow[5],2); ?></td>
                            </tr>

                            <?php $no1++;  $jmlhalaman = ceil($jmlulrow[0]/$perpages); } ?>
                        </tbody>
                        </table>
                   
                </div>          
                </div>
                <div class="box-footer clearfix right">
                    <div style="text-align: center;">
                        Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                        <?php
                        $ind = $_GET['ind'];
                        $reload = "simpanan.php?ind=$ind";
                        $page = intval($_GET["page"]);
                        $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                        if( $page == 0 ) $page = 1;

                        $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                        echo "<div>".paginate($reload, $page, $tpages)."</div>";
                        ?>
                    </div>
                </div>
                <?php } ?>
                </form>

                <!-- All  -->
                <div class="box-body" style="padding-top: 30px;">
                    <?php
                    if (empty($_GET['ind'] || $_GET['ind'] = '')){
                    ?>
                    <h3> Semua Tipe Simpanan </h3>
                    <?php 
                        $nm = 1;
                        $sim = "select * from dbo.BasicSavingTypeView ";
                        //echo $sim;
                        $pan = sqlsrv_query($conn, $sim);
                        while($nan = sqlsrv_fetch_array( $pan, SQLSRV_FETCH_NUMERIC)){
                            $no1 = 1;

                        ?>
                        <div>
                            <h4><?php echo $nm; ?>. <?php echo $nan[1]; ?></h4>
                        </div>
                        <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                            <a href="drep_simpanan.php?id=<?php echo $nan[0]; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
                        </div>
                    
                    <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo ('No'); ?></th>    
                                    <th><?php echo ('Kode Akun'); ?></th>
                                    <!-- <th><?php echo ('Member ID'); ?></th> -->
                                    <th><?php echo ('Nama'); ?></th>
                                    <th><?php echo ('Saldo'); ?></th> 
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php
                                //count
                                $jmlulsql   = "select count(*) from dbo.BasicSavingBalance where KodeBasicSavingType='$nan[0]'";
                                //echo $jmlulsql;
                                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                                //pagging
                                $perpages   = 10;
                                $halaman    = $_GET['page'];
                                if(empty($halaman)){
                                    $posisi  = 0;
                                    $batas   = $perpages;
                                    $halaman = 1;
                                }
                                else{
                                    $posisi  = (($perpages * $halaman) - 10) + 1;
                                    $batas   = ($perpages * $halaman);
                                }

                                //cek
                                
                                // $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[BasicSavingBalance] where KodeBasicSavingType = '$nan[0]') a";
                                $ulsql = "SELECT * FROM ( SELECT a.kid, a.MemberID,a.KodeBasicSavingType, a.AccNo, a.BillingBalance,a.PaymentBalance, b.UserID, ROW_NUMBER() OVER (ORDER BY a.MemberID asc) as row FROM [dbo].[BasicSavingBalance] a inner join [PaymentGateway].[dbo].[UserMemberKoperasi] b on a.MemberID = b.KodeMember and a.kid = b.kid where a.KodeBasicSavingType = '$nan[0]') a";
                                //echo $ulsql;
                                $ulstmt = sqlsrv_query($conn, $ulsql);
                                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                                    //var_dump($ulrow);
                                    if (!empty($nan[0])){
                                    

                                    $sql2 = "SELECT * FROM[dbo].[MemberListView] WHERE MemberID='$ulrow[1]'";
                                    //  echo $sql2;
                                    $stmt2 = sqlsrv_query($conn, $sql2);
                                    $row2  = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_NUMERIC); 
                                        } else {
                                        ?>
                                        <tr>
                                            <th><?php echo ('No'); ?></th>    
                                            <th><?php echo ('Kode Akun'); ?></th>
                                            <!-- <th><?php echo ('Member ID'); ?></th> -->
                                            <th><?php echo ('Nama'); ?></th>
                                            <th><?php echo ('Saldo'); ?></th>
                                        </tr>
                                    <?php } ?>                   
                                <tr>
                                    <td><?php echo $ulrow[7]; ?></td>
                                    <td align="right"><a href="rep_reg.php?acc=<?php echo $ulrow[3]; ?>&st=1&ind=<?php echo $_GET['ind']; ?>&user=<?php echo $ulrow[6]; ?>"><?php echo $ulrow[3]; ?></a></td>
                                    <!-- <td><?php echo $ulrow[1]; ?></td> -->
                                    <td><?php echo $row2[2]; ?></td>
                                    <td style="text-align: right"><?php echo number_format($ulrow[5],2); ?></td>
                                </tr>
                                <?php $no1++; $jmlhalaman = ceil($jmlulrow[0]/$perpages); } ?>
                            </tbody>
                        </table>
                        <div class="box-footer clearfix right">
                            <div style="text-align: center;">
                                Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                                <?php
                                $ind2 = $nan[0];
                                $reload = "simpanan.php?";
                                $page = intval($_GET["page"]);
                                $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                                if( $page == 0 ) $page = 1;
                                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                    echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                    ?>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    <?php $nm++;} ?>
                    <?php } ?>
                </div>
            </div>
    </div>
</div>

<script type="text/javascript">
    // untuk mendapatkan jwpopup
var jwpopup = document.getElementById('jwpopupBox');

// untuk mendapatkan link untuk membuka jwpopup
var mpLink = document.getElementById("jwpopupLink");

// untuk mendapatkan aksi elemen close
var close = document.getElementsByClassName("close")[0];

// membuka jwpopup ketika link di klik
mpLink.onclick = function() {
    jwpopup.style.display = "block";
}

// membuka jwpopup ketika elemen di klik
close.onclick = function() {
    jwpopup.style.display = "none";
}

// membuka jwpopup ketika user melakukan klik diluar area popup
window.onclick = function(event) {
    if (event.target == jwpopup) {
        jwpopup.style.display = "none";
    }
}
</script>

<?php 
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer_new.php'); ?>
