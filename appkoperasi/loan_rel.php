<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Pencairan Pinjaman'); ?></h2>

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <div class="box box-primary">
            <div class="table-responsive">
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th><?php echo lang('No App Pinjaman'); ?></th>
                            <th><?php echo lang('Tanggal di Setujui'); ?></th>
                            <th><?php echo lang('Masa Pinjaman'); ?></th>
                            <th><?php echo lang('Suku Bunga'); ?></th>
                            <th><?php echo lang('Denda Utama'); ?></th>
                            <th><?php echo lang('Denda Bunga'); ?></th>
                            <th><?php echo lang('Jumlah'); ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $aa   = "select * from [dbo].[LoanList] where LoanAppNumber in(select LoanAppNum from [dbo].[LoanApplicationList] where StatusComplete = 4) order by TimeStamp desc";
                        $bb  = sqlsrv_query($conn, $aa);
                        while($cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= $cc[0] ?></td>
                                <td><?= $cc[1]->format('Y-m-d H:i:s'); ?></td>
                                <td><?= $cc[8] ?></td>
                                <td><?= $cc[7] ?></td>
                                <td><?= $cc[9] ?></td>
                                <td><?= $cc[10] ?></td>
                                <td><?= number_format($cc[3]) ?></td>
                                <td>
                                    <a href="proccsoaloanrel.php?t=<?= md5('y') ?>&lan=<?= $cc[0] ?>"><button class="btn btn-success btn-sm" type="button"><i class="fa fa-check"></i> Release</button></a>
                                </td>
                            </tr>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <button type="button" class="btn btn-primary hide" id="modal" data-toggle="modal" data-target="#myModal"></button>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php require('footer_new.php');?>
