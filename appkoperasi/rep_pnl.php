<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<div class="animated fadeinup delay-1">

<style>
    .tengah{
        text-align:center;
    }
    .kiri{
        text-align:left;
    }
    .kanan{
        text-align:right;
    }   

    .eshop.rand span {
        display: none;
    }

    ul, #treeview {
        list-style-type: none;
        border-bottom: #3px solid #000;
    }

    #treeview {
        margin: 0;
        padding: 0;
    }

    .caret {
        border-bottom: #1px solid #000;
        cursor: pointer;
        -webkit-user-select: none; /* Safari 3.1+ */
        -moz-user-select: none; /* Firefox 2+ */
        -ms-user-select: none; /* IE 10+ */
        user-select: none;
    }

    .caret::before {
        content: "\25B6";
        color: black;
        display: inline-block;
        margin-right: 6px;
    }

    .caret-down::before {
        -ms-transform: rotate(90deg); /* IE 9 */
        -webkit-transform: rotate(90deg); /* Safari */'
        transform: rotate(90deg);  
    }

    .nested {
      display: none;
      margin-left: 5%;
      margin-top: 5px;
    }

    .active {
      display: block;
    }
</style>


    <div class="page-content">

        <h3 class="box-title"><?php echo lang('Laba Rugi'); ?></h3>

        <div class="form-inputs">
         <?php if(!isset($_GET['acc']) and !isset($_GET['acc1']) and !isset($_GET['acc2']) and !isset($_GET['acc3']) and !isset($_GET['acc4']) and !isset($_GET['acc5']) and !isset($_GET['acc6'])){ ?>
            <form action="" method="POST">
                <form>
                <h5 class="box-title"><?php echo lang('Periode dan Template'); ?></h5>
                <div class="input-field">

                    <?php
                        if(isset($_POST['bulan'])){
                            $from = $_POST['bulan'];
                        }
                        else{
                            $from = date('Y/m/d');
                        }
                        ?>


                        <select id="bulan" name="bulan" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['bulan']; ?>">- <?php echo lang('Pilih Bulan'); ?> -</option>
                            <?php
                            $bln=array(01=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
                            echo $bln;
                            $from='';
                            for ($bulan=01; $bulan<=12; $bulan++) { 
                                 if ($_POST['bulan'] == $bulan){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$bulan' $ck>$bln[$bulan]</option>";


                            }
                            ?>
                        </select>

                </div>
                <div class="input-field">
                    <?php
                        if(isset($_POST['tahun'])){
                            $to = $_POST['tahun'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>

                        <select id="tahun" name="tahun" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['tahun']; ?>">- <?php echo lang('Pilih Tahun'); ?> -</option>
                            <?php
                            $now=date("Y");
                            for ($tahun=2017; $tahun<=$now; $tahun++) { 
                                 if ($_POST['tahun'] == $tahun){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$tahun' $ck>$tahun</option>"; 
                            }

                            ?>
                        </select>
                </div>

                <div class="input-field">
                    <?php
                        if(isset($_POST['template'])){
                            $to = $_POST['template'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>

                        <select id="template" name="template" class="browser-default">
                            <option value=''>- <?php echo lang('Pilih Template'); ?> -</option>
                            <?php
                            $julsql   = "select * from [dbo].[Template] where Tipe = 1";
                            echo $julsql;
                            $julstmt = sqlsrv_query($conn, $julsql);
                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                               ?>
                            <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_POST['template']){echo "selected";} ?>><?=$rjulrow[1];?></option>
                    <?php } ?>
                        </select>

                </div>


                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_neraca.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
                </form>
            </form>
        </div>
        <?php } ?>


        <?php if(isset($_POST['bulan']) and isset($_POST['tahun']) and isset($_POST['template']) and !isset($_POST['acc'])) {
                    $from1 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                    $from = date('Y-m-01', strtotime($from1));
                    $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                    $to = date('Y-m-t', strtotime($to3));
                    $to1 = date('m', strtotime($to3));
                    $to2 = date('Y', strtotime($to3));
                    $a = "exec dbo.ProsesGenerateLaporanRugiLaba '$from','$to','$_SESSION[UserID]'";
                    //echo $a;
                    $b = sqlsrv_query($conn, $a);
                    ?>
                    <div class="row m-l-0">
                        <!--<div class="col">
                             <a onclick="printDiv()" href="#"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-example" onclick="arHide('#modal-example')"> <?php echo lang('Print'); ?></button></a>
                        </div> -->
                        <div class="col">
                            <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-confirm" onclick="arHide('#modal-confirm')"> <?php echo lang('Download Template'); ?></button>
                        </div>
                        <div class="col">
                            <a href="rep_template.php?&st=4&set=01" target="_blank"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="delete"> <?php echo lang('Template'); ?></button>
                        </div>
                    </div>


                    <div class="hide" id="modal-confirm">
                        <h3 class="uppercase"><?php echo lang('Konfirmasi'); ?></h3>
                        <?php echo lang('Sudah Yakin Download Template ?'); ?><br><br>
                        <div class="row m-l-0">
                            <div class="col">
                                <a href="dpnl.php?bulan=<?php echo $_POST['bulan']; ?>&tahun=<?php echo $_POST['tahun']; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" target="_blank"><?php echo lang('Ya'); ?></button></a>
                            </div>
                           
                            <div class="col">
                                <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" onclick="arHide('#modal-confirm')"><?php echo lang('Tidak'); ?></button>
                            </div>
                        </div>
                    </div>

                    
                    <!--<div method="POST" action="">
                    <select id="level" name="level" class="browser-default" style="margin-top: 30px;">
                        <option value="">-Pilih Level-</option>
                        <option value="0">-0-</option>
                        <option value="1">-1-</option>
                        <option value="2">-2-</option>
                    </select>
                    <?php
                    if(isset($_GET['submit'])){
                        if(empty($_GET['level']))
                        {
                            $level= "Eror";
                        } else {
                            $level=$_GET['level'];
                        }
                    } 
                    ?>


                        <div style="margin-top: 20px;">
                            <button type="submit" class="waves-effect waves-light btn-large primary-color width-100" name="submit1" id="button1" value="submit"><?php echo lang('Pilih'); ?></button>
                        </div>
                    </div>-->

            <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">

                <div class="box-header" align="center">
                    <tr>
                        <h3 class="" style="margin-top: 30px;"><?php echo ('Laporan Laba Rugi'); ?></h3>
                    </tr>
                    <tr>
                        <h3 class=""><?php echo $_SESSION['NamaKoperasi']; ?></h3>
                    <tr>
                </div>

                <div class="box-header with-border" align="center">
                    <tr>
                        <?php
                            $bulan = $_POST['bulan'];
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        if($bulan !=12){
                                                                            Eror;
                                                                        } else {
                                                                           $bulan = 'Desember';
                                                                        }
                                                                    } else {
                                                                        $bulan = 'November';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
                        <h3 class="box-title" style="margin-bottom: 20px;"><?php echo "Periode : "; ?></h3>
                        <h3 class="box-title" style="margin-bottom: 20px;"><?php echo $bulan," - "; ?></h3>
                        <h3 class="box-title" style="margin-bottom: 20px;"><?php echo $_POST['tahun']; ?></h3>

                    </tr> 
                </div>

                <?php
                $term = $_POST['template'];
                $ar = array("");
                $c = " select * from dbo.TemplateDetail_2 where IDTemplate = $term";
                //echo $c;
                $d = sqlsrv_query($conn, $c);
                while($zz = sqlsrv_fetch_array($d, SQLSRV_FETCH_NUMERIC)){
                    $t = $zz[2];
                    array_push($ar, $t);
                    
                 } //var_dump($ar);
            
                ?>
                <?php
                function getIsActive($x, $c) {
                    $sql1 = "SELECT Status from [dbo].[TemplateDetail_2] where Account='$x'";
                    $query1 = sqlsrv_query($c, $sql1);
                    $data1 = sqlsrv_fetch_array($query1);

                    return $data1[0];
                }
                ?>

                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" width="50%"><?php echo lang('Laba Rugi'); ?></th>
                        <tr>
                         
                        <tr>
                            <td colspan="2" width="50%">
                                <table class="table table-bordered table-striped" width="50%">
                                    <tr>
                                        <ul id="treeview">
                                            <?php
                                                if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                $to = date('Y-m-d', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('d', strtotime($to));    
                                            } else {
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                $to = date('Y-m-t', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('t', strtotime($to));
                                            }

                                            $name = $_SESSION[Name];
                                            $x = "select * from dbo.AccountBalance where KodeAccount in ('4.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('4.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            //echo $x;
                                            $bcd = sqlsrv_query($conn, $abc);
                                            while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                $total = $def[0];
                                            }
                                        
                                            $y = sqlsrv_query($conn, $x);
                                            $ga1 = 1;
                                            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                                $a=$z[1];
                                                $a1 = str_replace(".","",$a);
                                                $sact1 = getIsActive($z[1], $conn);
                                                ?>

                                                <!-- sub 1 -->
                                                <li><span class="caret <?php if($sact1 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-a-'.$ga1; ?>"><?php echo $z[1].' - '.$z[2]; ?>
                                                        <div class="right" id="<?php echo 'tch-a-lbl-'.$ga1; ?>"><?php echo number_format($z[10],2);?></div>
                                                    </span> 
                                                    <!--<script type="text/javascript">
                                                    $("#testtree li").click(function(e) {
                                                        e.stopPropagation();
                                                        alert("test");
                                                    });
                                                    $(document).ready(function(){
                                                        $(".1").click(function(){
                                                            $("span").toggle();
                                                        });
                                                    });
                                                    </script>
                                                    <script>
                                                        function myFunction() {
                                                            var x = document.getElementById('demo');
                                                            if (x.style.display === 'none') {
                                                                x.style.display = 'block';
                                                            } else {
                                                                x.style.display = 'none';
                                                            }
                                                        }
                                                    </script> -->

                                                    <ul class="nested" id="<?php echo 'tch-ta-'.$ga1; ?>">
                                                        <?php
                                                        $xx = "select * from dbo.AccountBalance where KodeGroup ='$z[1]' and Header = '2' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                       // echo $xx;
                                                        $yy = sqlsrv_query($conn, $xx);
                                                        $ga2 = 2;
                                                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                                            $sact2 = getIsActive($zz[1], $conn);
                                                        ?>
                                                            <!-- sub 2 -->
                                                            <li><span class="caret <?php if($sact2 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-a-'.$ga1.'-'.$ga2; ?>"><?php echo $zz[1].' - '.$zz[2]; ?>
                                                                    <div class="right" id="<?php echo 'tch-a-lbl-'.$ga1.'-'.$ga2; ?>"><?php echo ' '; ?><?php echo number_format($zz[10],2);?></div>
                                                                </span>

                                                                <ul class="nested" id="<?php echo 'tch-ta-'.$ga1.'-'.$ga2; ?>">
                                                                    <?php
                                                                    $xxx = "select * from dbo.AccountBalance where KodeTipe ='$zz[1]' and Header = '3' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                    //echo $xxx;
                                                                    $yyy = sqlsrv_query($conn, $xxx);
                                                                    $ga3 = 3;
                                                                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                                        $sact3 = getIsActive($zzz[1], $conn);
                                                                    ?>
                                                                        <li><span class="caret <?php if($sact3 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-a-'.$ga1.'-'.$ga2.'-'.$ga3; ?>"><?php echo $zzz[1].' - '.$zzz[2]; ?>
                                                                                <div class="right" id="<?php echo 'tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3; ?>"><?php echo ' '; ?><?php echo number_format($zzz[10],2);?></div>
                                                                            </span> 
                                                                
                                                                            <ul class="nested" id="<?php echo 'tch-ta-'.$ga1.'-'.$ga2.'-'.$ga3; ?>">
                                                                                <?php
                                                                                $xxxx = "select * from dbo.AccountBalance where KodeTipe ='$zzz[4]' and Header = '4' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzz[1],0,6)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                                //echo $xxxx;
                                                                                $yyyy = sqlsrv_query($conn, $xxxx);
                                                                                $ga4 = 4;
                                                                                while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)) {
                                                                                    $sact4 = getIsActive($zzzz[1], $conn);
                                                                                ?>
                                                                                    <li><span class="caret <?php if($sact4 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-a-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>"><?php echo $zzzz[1].' - '.$zzzz[2]; ?>
                                                                                            <div class="right" id="<?php echo 'tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>"><?php echo number_format($zzzz[10],2);?></div>
                                                                                        </span> 
                                                                                        
                                                                                        <ul class="nested" id="<?php echo 'tch-ta-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>">
                                                                                            <?php
                                                                                            if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                                                                $to = date('Y-m-d', strtotime($to3));
                                                                                                $to1 = date('m', strtotime($to3));
                                                                                                $to2 = date('Y', strtotime($to3));
                                                                                                $to4 = date('d', strtotime($to));    
                                                                                            } else {
                                                                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                                                                $to = date('Y-m-t', strtotime($to3));
                                                                                                $to1 = date('m', strtotime($to3));
                                                                                                $to2 = date('Y', strtotime($to3));
                                                                                                $to4 = date('t', strtotime($to));
                                                                                            }
                                                                                            $name = $_SESSION[Name];
                                                                                            $abcde = 11111;
                                                                                            $xxxxx ="select * from dbo.AccountBalance where KodeTipe ='$zzzz[4]' and Header = '5'and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzzz[1],0,9)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                                            //echo $xxxxx;
                                                                                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                                                <li><a href="rep_pnllap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[1]; ?>&st=5" target="_blank"><?php echo $zzzzz[1].' - '.$zzzzz[2]; ?></a>
                                                                                                    <span class="right"><?php echo number_format($zzzzz[10],2) ;?></span>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                        </ul>
                                                                                    </li>

                                                                                    <script type="text/javascript">
                                                                                        // Level 4
                                                                                        $('ul#treeview li <?php echo 'span#tch-a-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>').click(function(){
                                                                                            if(  $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>').hasClass('hide') ) {
                                                                                                $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>').removeClass('hide');
                                                                                            }
                                                                                            else {
                                                                                                $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>').addClass('hide');
                                                                                            }
                                                                                        });

                                                                                        if( $('ul#treeview li <?php echo 'span#tch-a-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>').hasClass('caret-down') ) {
                                                                                            $('ul#treeview ul#tch-ta-<?php echo $ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>').addClass('active');
                                                                                            $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3.'-'.$ga4; ?>').addClass('hide');
                                                                                        }
                                                                                    </script>
                                                                                <?php $ga4++; } ?>
                                                                            </ul>
                                                                        </li>

                                                                        <script type="text/javascript">
                                                                            // Level 3
                                                                            $('ul#treeview li <?php echo 'span#tch-a-'.$ga1.'-'.$ga2.'-'.$ga3; ?>').click(function(){
                                                                                if(  $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3; ?>').hasClass('hide') ) {
                                                                                    $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3; ?>').removeClass('hide');
                                                                                }
                                                                                else {
                                                                                    $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3; ?>').addClass('hide');
                                                                                }
                                                                            });

                                                                            if( $('ul#treeview li <?php echo 'span#tch-a-'.$ga1.'-'.$ga2.'-'.$ga3; ?>').hasClass('caret-down') ) {
                                                                                $('ul#treeview ul#tch-ta-<?php echo $ga1.'-'.$ga2.'-'.$ga3; ?>').addClass('active');
                                                                                $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2.'-'.$ga3; ?>').addClass('hide');
                                                                            }
                                                                        </script>
                                                                    <?php $ga3++; } ?>
                                                                </ul>
                                                            </li>

                                                            <script type="text/javascript">
                                                                // Level 2
                                                                $('ul#treeview li <?php echo 'span#tch-a-'.$ga1.'-'.$ga2; ?>').click(function(){
                                                                    if(  $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2; ?>').hasClass('hide') ) {
                                                                        $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2; ?>').removeClass('hide');
                                                                    }
                                                                    else {
                                                                        $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2; ?>').addClass('hide');
                                                                    }
                                                                });

                                                                if( $('ul#treeview li <?php echo 'span#tch-a-'.$ga1.'-'.$ga2; ?>').hasClass('caret-down') ) {
                                                                    $('ul#treeview ul#tch-ta-<?php echo $ga1.'-'.$ga2; ?>').addClass('active');
                                                                    $('<?php echo '#tch-a-lbl-'.$ga1.'-'.$ga2; ?>').addClass('hide');
                                                                }
                                                            </script>
                                                        <?php $ga2++; } ?>        
                                                    </ul>
                                                </li>

                                                <script type="text/javascript">
                                                    // Level 1
                                                    $('ul#treeview li <?php echo 'span#tch-a-'.$ga1; ?>').click(function(){
                                                        if(  $('<?php echo '#tch-a-lbl-'.$ga1; ?>').hasClass('hide') ) {
                                                            $('<?php echo '#tch-a-lbl-'.$ga1; ?>').removeClass('hide');
                                                        }
                                                        else {
                                                            $('<?php echo '#tch-a-lbl-'.$ga1; ?>').addClass('hide');
                                                        }
                                                    });

                                                    if( $('ul#treeview li <?php echo 'span#tch-a-'.$ga1; ?>').hasClass('caret-down') ) {
                                                        $('ul#treeview ul#tch-ta-<?php echo $ga1; ?>').addClass('active');
                                                        $('<?php echo '#tch-a-lbl-'.$ga1; ?>').addClass('hide');
                                                    }
                                                </script>
                                            <?php $ga1++; } ?>
                                        </ul>
                                    </tr>

                                    <tr>
                                        <th><?php echo ('Total Pendapatan'); ?></th>
                                        <th><span class="right"><?php echo number_format($total,2); ?></span></th>
                                    </tr>
                                </table>

                                <div class="m-t-20"></div>

                                <table class="table table-bordered table-striped" width="50%">
                                    <tbody>
                                        <ul id="treeview">
                                            <?php
                                                if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                $to = date('Y-m-d', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('d', strtotime($to));    
                                            } else {
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                $to = date('Y-m-t', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('t', strtotime($to));
                                            }

                                            $name = $_SESSION[Name];
                                            $x = "select * from dbo.AccountBalance where KodeAccount in ('5.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('5.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            //echo $x;
                                            $bcd = sqlsrv_query($conn, $abc);
                                            while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                $total = $def[0];
                                            }
                                            $lima = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('4.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            //echo $abc;
                                            //echo $limaenam;
                                            $sum = sqlsrv_query($conn, $lima);
                                            while($hasilsum = sqlsrv_fetch_array($sum, SQLSRV_FETCH_NUMERIC)){
                                                $total1 = $hasilsum[0];
                                            }

                                            $totalfix = $total1 - $total;
                                            $y = sqlsrv_query($conn, $x);
                                            $gb1 = 1;
                                            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                                $sact1 = getIsActive($z[1], $conn);
                                            ?>
                                                <!-- sub 1 -->
                                                <li><span class="caret <?php if($sact1 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-b-'.$gb1; ?>"> <?php echo $z[1].' - '.$z[2]; ?>
                                                        <div class="right" id="<?php echo 'tch-b-lbl-'.$gb1; ?>"><?php echo number_format($z[10],2);?></div>
                                                    </span>
            
                                                    <ul class="nested" id="<?php echo 'tch-tb-'.$gb1; ?>">
                                                        <?php
                                                        $xx = "select * from dbo.AccountBalance where KodeGroup ='$z[1]' and Header = '2' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                       // echo $xx;
                                                        $yy = sqlsrv_query($conn, $xx);
                                                        $gb2 = 2;
                                                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){ 
                                                            $sact2 = getIsActive($zz[1], $conn);
                                                        ?>
                                                            <!-- sub 2 -->
                                                            <li><span class="caret <?php if($sact2 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-b-'.$gb1.'-'.$gb2; ?>"><?php echo $zz[1].' - '.$zz[2]; ?>
                                                                    <div class="right" id="<?php echo 'tch-b-lbl-'.$gb1.'-'.$gb2; ?>"><?php echo '  ' ?><?php echo number_format($zz[10],2);?></div>
                                                                </span>

                                                                <ul class="nested" id="<?php echo 'tch-tb-'.$gb1.'-'.$gb2; ?>">
                                                                    <?php
                                                                    $xxx = "select * from dbo.AccountBalance where KodeTipe ='$zz[1]' and Header = '3' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                    //echo $xxx;
                                                                    $yyy = sqlsrv_query($conn, $xxx);
                                                                    $gb3 = 3;
                                                                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){ 
                                                                        $sact3 = getIsActive($zzz[1], $conn);
                                                                    ?>
                                                                        <li><span class="caret <?php if($sact3 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-b-'.$gb1.'-'.$gb2.'-'.$gb3; ?>"><?php echo $zzz[1].' - '.$zzz[2]; ?>
                                                                                <div class="right" id="<?php echo 'tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3; ?>"><?php echo '  ' ?><?php echo number_format($zzz[10],2);?></div>
                                                                            </span> 

                                                                            <ul class="nested" id="<?php echo 'tch-tb-'.$gb1.'-'.$gb2.'-'.$gb3; ?>">
                                                                                <?php
                                                                                $xxxx = "select * from dbo.AccountBalance where KodeTipe ='$zzz[4]' and Header = '4' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzz[1],0,6)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                                //echo $xxxx;
                                                                                $yyyy = sqlsrv_query($conn, $xxxx);
                                                                                $gb4 = 4;
                                                                                while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){ 
                                                                                    $sact4 = getIsActive($zzzz[1], $conn);
                                                                                ?>
                                                                                    <li><span class="caret <?php if($sact4 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-b-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>"> <?php echo $zzzz[1].' - '.$zzzz[2]; ?>
                                                                                            <div class="right" id="<?php echo 'tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>"><?php echo number_format($zzzz[10],2);?></div>
                                                                                        </span>
                                                                                        <ul class="nested" id="<?php echo 'tch-tb-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>">
                                                                                            <?php
                                                                                            if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                                                                $to = date('Y-m-d', strtotime($to3));
                                                                                                $to1 = date('m', strtotime($to3));
                                                                                                $to2 = date('Y', strtotime($to3));
                                                                                                $to4 = date('d', strtotime($to));    
                                                                                            } else {
                                                                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                                                                $to = date('Y-m-t', strtotime($to3));
                                                                                                $to1 = date('m', strtotime($to3));
                                                                                                $to2 = date('Y', strtotime($to3));
                                                                                                $to4 = date('t', strtotime($to));
                                                                                            }
                                                                                            $name = $_SESSION[Name];
                                                                                            $abcde = 11111;
                                                                                            $xxxxx ="select * from dbo.AccountBalance where KodeTipe ='$zzzz[4]' and Header = '5'and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzzz[1],0,9)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                                            //echo $xxxxx;
                                                                                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                                                <li><a href="rep_pnllap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[1]; ?>&st=5" target="_blank"><?php echo $zzzzz[1].' - '.$zzzzz[2]; ?></a>
                                                                                                    <div class="right"><?php echo number_format($zzzzz[10],2) ;?></div>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                        </ul>
                                                                                    </li>

                                                                                    <script type="text/javascript">
                                                                                        // Level 4
                                                                                        $('ul#treeview li <?php echo 'span#tch-b-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>').click(function(){
                                                                                            if(  $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>').hasClass('hide') ) {
                                                                                                $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>').removeClass('hide');
                                                                                            }
                                                                                            else {
                                                                                                $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>').addClass('hide');
                                                                                            }
                                                                                        });

                                                                                        if( $('ul#treeview li <?php echo 'span#tch-b-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>').hasClass('caret-down') ) {
                                                                                            $('ul#treeview ul#tch-tb-<?php echo $gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>').addClass('active');
                                                                                            $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3.'-'.$gb4; ?>').addClass('hide');
                                                                                        }
                                                                                    </script>
                                                                                <?php $gb4++; } ?>
                                                                            </ul>
                                                                        </li>

                                                                        <script type="text/javascript">
                                                                            // Level 3
                                                                            $('ul#treeview li <?php echo 'span#tch-b-'.$gb1.'-'.$gb2.'-'.$gb3; ?>').click(function(){
                                                                                if(  $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3; ?>').hasClass('hide') ) {
                                                                                    $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3; ?>').removeClass('hide');
                                                                                }
                                                                                else {
                                                                                    $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3; ?>').addClass('hide');
                                                                                }
                                                                            });

                                                                            if( $('ul#treeview li <?php echo 'span#tch-b-'.$gb1.'-'.$gb2.'-'.$gb3; ?>').hasClass('caret-down') ) {
                                                                                $('ul#treeview ul#tch-tb-<?php echo $gb1.'-'.$gb2.'-'.$gb3; ?>').addClass('active');
                                                                                $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2.'-'.$gb3; ?>').addClass('hide');
                                                                            }
                                                                        </script>
                                                                    <?php $gb3++; } ?>
                                                                </ul>
                                                            </li>

                                                            <script type="text/javascript">
                                                                // Level 2
                                                                $('ul#treeview li <?php echo 'span#tch-b-'.$gb1.'-'.$gb2; ?>').click(function(){
                                                                    if(  $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2; ?>').hasClass('hide') ) {
                                                                        $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2; ?>').removeClass('hide');
                                                                    }
                                                                    else {
                                                                        $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2; ?>').addClass('hide');
                                                                    }
                                                                });

                                                                if( $('ul#treeview li <?php echo 'span#tch-b-'.$gb1.'-'.$gb2; ?>').hasClass('caret-down') ) {
                                                                    $('ul#treeview ul#tch-tb-<?php echo $gb1.'-'.$gb2; ?>').addClass('active');
                                                                    $('<?php echo '#tch-b-lbl-'.$gb1.'-'.$gb2; ?>').addClass('hide');
                                                                }
                                                            </script>
                                                        <?php $gb2++; } ?>        
                                                    </ul>
                                                </li>

                                                <script type="text/javascript">
                                                    // Level 1
                                                    $('ul#treeview li <?php echo 'span#tch-b-'.$gb1; ?>').click(function(){
                                                        if(  $('<?php echo '#tch-b-lbl-'.$gb1; ?>').hasClass('hide') ) {
                                                            $('<?php echo '#tch-b-lbl-'.$gb1; ?>').removeClass('hide');
                                                        }
                                                        else {
                                                            $('<?php echo '#tch-b-lbl-'.$gb1; ?>').addClass('hide');
                                                        }
                                                    });

                                                    if( $('ul#treeview li <?php echo 'span#tch-b-'.$gb1; ?>').hasClass('caret-down') ) {
                                                        $('ul#treeview ul#tch-tb-<?php echo $gb1; ?>').addClass('active');
                                                        $('<?php echo '#tch-b-lbl-'.$gb1; ?>').addClass('hide');
                                                    }
                                                </script>
                                            <?php $gb1++; } ?>
                                        </ul>
                                    </tbody>
                           
                                    <tr>
                                        <th><?php echo ('Total Harga Pokok'); ?></th>
                                        <th><span class="right"><?php echo number_format($total,2); ?></span></th>
                                    </tr>

                                    <tr>
                                        <th><?php echo ('Total Laba/Rugi Kotor  '); ?></th>
                                        <th><span class="right"><?php echo number_format($totalfix,2); ?></span></th>
                                    </tr>
                                </table>

                                <div class="m-t-20"></div>

                                <table class="table table-bordered table-striped" width="50%">
                                    <tbody>
                                        <ul id="treeview">
                                            <?php
                                                if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                $to = date('Y-m-d', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('d', strtotime($to));    
                                            } else {
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                $to = date('Y-m-t', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('t', strtotime($to));
                                            }

                                            $name = $_SESSION[Name];
                                            $x = "select * from dbo.AccountBalance where KodeAccount in ('6.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('6.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            //echo $x;
                                            $bcd = sqlsrv_query($conn, $abc);
                                            while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                $total6 = $def[0];
                                            }

                                            $totalakhir = $totalfix - $total6;
                                            $y = sqlsrv_query($conn, $x);
                                            $gc1 = 1;
                                            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){ 
                                                $sact1 = getIsActive($z[1], $conn);
                                                //echo 'Sub 1 : '.$x; 
                                                ?>
                                                <!-- sub 1 -->
                                                <li><span class="caret <?php if($sact1 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-c-'.$gc1; ?>"><?php echo $z[1].' - '.$z[2]; ?>
                                                        <div class="right" id="<?php echo 'tch-c-lbl-'.$gc1; ?>"><?php echo number_format($z[10],2);?></div>
                                                    </span>
                                                    <ul class="nested" id="<?php echo 'tch-tc-'.$gc1; ?>">
                                                        <?php
                                                        $xx = "select * from dbo.AccountBalance where KodeGroup ='$z[1]' and Header = '2' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                        // echo $xx;
                                                        $yy = sqlsrv_query($conn, $xx);
                                                        $gc2 = 2;
                                                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){ 
                                                            $sact2 = getIsActive($zz[1], $conn);
                                                            ?>
                                                            <!-- sub 2 -->
                                                            <li><span class="caret <?php if($sact2 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-c-'.$gc1.'-'.$gc2; ?>"><?php echo $zz[1].' - '.$zz[2]; ?>
                                                                    <div class="right" id="<?php echo 'tch-c-lbl-'.$gc1.'-'.$gc2; ?>"><?php echo number_format($zz[10],2);?></div>
                                                                </span>
                                                                <ul class="nested" id="<?php echo 'tch-tc-'.$gc1.'-'.$gc2; ?>">
                                                                    <?php
                                                                    $xxx = "select * from dbo.AccountBalance where KodeTipe ='$zz[1]' and Header = '3' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                    //echo $xxx;
                                                                    $yyy = sqlsrv_query($conn, $xxx);
                                                                    $gc3 = 3;
                                                                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){ 
                                                                        $sact3 = getIsActive($zzz[1], $conn);
                                                                        ?>
                                                                        <li><span class="caret <?php if($sact3 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-c-'.$gc1.'-'.$gc2.'-'.$gc3; ?>"><?php echo $zzz[1].' - '.$zzz[2]; ?>
                                                                                <div class="right" id="<?php echo 'tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3; ?>"><?php echo number_format($zzz[10],2);?></div>
                                                                            </span>

                                                                            <ul class="nested" id="<?php echo 'tch-tc-'.$gc1.'-'.$gc2.'-'.$gc3; ?>">
                                                                                <?php
                                                                                $xxxx = "select * from dbo.AccountBalance where KodeTipe ='$zzz[4]' and Header = '4' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzz[1],0,6)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                                //echo $xxxx;
                                                                                $yyyy = sqlsrv_query($conn, $xxxx);
                                                                                $gc4 = 4;
                                                                                while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){ 
                                                                                    $sact4 = getIsActive($zzzz[1], $conn);
                                                                                    ?>
                                                                                    <li><span class="caret <?php if($sact4 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'tch-c-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>"><?php echo $zzzz[1].' - '.$zzzz[2]; ?>
                                                                                            <div class="right" id="<?php echo 'tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>"><?php echo number_format($zzzz[10],2);?></div>
                                                                                        </span>

                                                                                        <ul class="nested" id="<?php echo 'tch-tc-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>">
                                                                                            <?php
                                                                                            if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                                                                $to = date('Y-m-d', strtotime($to3));
                                                                                                $to1 = date('m', strtotime($to3));
                                                                                                $to2 = date('Y', strtotime($to3));
                                                                                                $to4 = date('d', strtotime($to));    
                                                                                            } else {
                                                                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                                                                $to = date('Y-m-t', strtotime($to3));
                                                                                                $to1 = date('m', strtotime($to3));
                                                                                                $to2 = date('Y', strtotime($to3));
                                                                                                $to4 = date('t', strtotime($to));
                                                                                            }
                                                                                            $name = $_SESSION[Name];
                                                                                            $abcde = 11111;
                                                                                            $xxxxx ="select * from dbo.AccountBalance where KodeTipe ='$zzzz[4]' and Header = '5'and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzzz[1],0,9)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                                            //echo $xxxxx;
                                                                                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                                                <li  data-icon-cls="fa fa-folder"><a href="rep_pnllap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[1]; ?>&st=5" target="_blank"><?php echo $zzzzz[1].' - '.$zzzzz[2]; ?></a>
                                                                                                    <span class="right"><?php echo number_format($zzzzz[10],2) ;?></span>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                        </ul>
                                                                                    </li>

                                                                                    <script type="text/javascript">
                                                                                        // Level 4
                                                                                        $('ul#treeview li <?php echo 'span#tch-c-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>').click(function(){
                                                                                            if(  $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>').hasClass('hide') ) {
                                                                                                $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>').removeClass('hide');
                                                                                            }
                                                                                            else {
                                                                                                $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>').addClass('hide');
                                                                                            }
                                                                                        });
                                                                                        
                                                                                        if( $('ul#treeview li <?php echo 'span#tch-c-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>').hasClass('caret-down') ) {
                                                                                            $('ul#treeview ul#tch-tc-<?php echo $gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>').addClass('active');
                                                                                            $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3.'-'.$gc4; ?>').addClass('hide');
                                                                                        }
                                                                                    </script>
                                                                                <?php $gc4++; } ?>
                                                                            </ul>
                                                                        </li>

                                                                        <script type="text/javascript">
                                                                            // Level 3
                                                                            $('ul#treeview li <?php echo 'span#tch-c-'.$gc1.'-'.$gc2.'-'.$gc3; ?>').click(function(){
                                                                                if(  $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3; ?>').hasClass('hide') ) {
                                                                                    $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3; ?>').removeClass('hide');
                                                                                }
                                                                                else {
                                                                                    $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3; ?>').addClass('hide');
                                                                                }
                                                                            });
                                                                            
                                                                            if( $('ul#treeview li <?php echo 'span#tch-c-'.$gc1.'-'.$gc2.'-'.$gc3; ?>').hasClass('caret-down') ) {
                                                                                $('ul#treeview ul#tch-tc-<?php echo $gc1.'-'.$gc2.'-'.$gc3; ?>').addClass('active');
                                                                                $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2.'-'.$gc3; ?>').addClass('hide');
                                                                            }
                                                                        </script>
                                                                    <?php $gc3++; } ?>
                                                                </ul>
                                                            </li>

                                                            <script type="text/javascript">
                                                                // Level 2
                                                                $('ul#treeview li <?php echo 'span#tch-c-'.$gc1.'-'.$gc2; ?>').click(function(){
                                                                    if(  $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2; ?>').hasClass('hide') ) {
                                                                        $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2; ?>').removeClass('hide');
                                                                    }
                                                                    else {
                                                                        $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2; ?>').addClass('hide');
                                                                    }
                                                                });
                                                                
                                                                if( $('ul#treeview li <?php echo 'span#tch-c-'.$gc1.'-'.$gc2; ?>').hasClass('caret-down') ) {
                                                                    $('ul#treeview ul#tch-tc-<?php echo $gc1.'-'.$gc2; ?>').addClass('active');
                                                                    $('<?php echo '#tch-c-lbl-'.$gc1.'-'.$gc2; ?>').addClass('hide');
                                                                }
                                                            </script>
                                                        <?php $gc2++; } ?>        
                                                    </ul>
                                                </li>

                                                <script type="text/javascript">
                                                    // Level 1
                                                    $('ul#treeview li <?php echo 'span#tch-c-'.$gc1; ?>').click(function(){
                                                        if(  $('<?php echo '#tch-c-lbl-'.$gc1; ?>').hasClass('hide') ) {
                                                            $('<?php echo '#tch-c-lbl-'.$gc1; ?>').removeClass('hide');
                                                        }
                                                        else {
                                                            $('<?php echo '#tch-c-lbl-'.$gc1; ?>').addClass('hide');
                                                        }
                                                    });
                                                    
                                                    if( $('ul#treeview li <?php echo 'span#tch-c-'.$gc1; ?>').hasClass('caret-down') ) {
                                                        $('ul#treeview ul#tch-tc-<?php echo $gc1; ?>').addClass('active');
                                                        $('<?php echo '#tch-c-lbl-'.$gc1; ?>').addClass('hide');
                                                    }
                                                </script>
                                            <?php $gc1++; } ?>
                                        </ul>
                                    </tbody>
                           
                                    <tr>
                                        <th><?php echo ('Total Biaya'); ?></th>
                                        <th><span class="right"><?php echo number_format($total6,2); ?></span></th>
                                    </tr>
                                </table>
                            </td>

                            <tr>
                                <th><?php echo ('Total Laba/Rugi Bersih'); ?></th>
                                <th><span class="right"><?php echo number_format($totalakhir,2); ?></span></th>
                            </tr>
                        </tr> 
                    </table>
            </div>    
        <?php } ?>
            
    </div>


<!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <?php /*
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>
    */ ?>


    <script type="text/javascript">
        var toggler = document.getElementsByClassName("caret");
        var i;

        for (i = 0; i < toggler.length; i++) {
          toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
          });
        }

        /*
        jQuery(function ($) {
            //$("#treeview").shieldTreeView();
        });

        jQuery(function ($) {
            //$("#treeview1").shieldTreeView();
        });

        
        jQuery(function ($) {
            $("#treeview2").TreeView();
            collapsed: true,
            unique: true,
            persist: “location”
        });
        

        jQuery(function ($) {
            //$("#treeview3").shieldTreeView();
        });

        jQuery(function ($) {
            //$("#treeview4").shieldTreeView();
        });

        /*
        jQuery(function ($) {
            $("#treeview5").TreeView();
            collapsed: true,
            unique: true,
            persist: “location”
        });

        jQuery(function ($) {
            //$("#treeview6").shieldTreeView();
        });

        jQuery(function ($) {
            //$("#treeview7").shieldTreeView();
        });

        jQuery(function ($) {
            //$("#treeview8").shieldTreeView();
        });

        jQuery(function ($) {
            //$("#treeview9").shieldTreeView();
        });

        jQuery(function ($) {
            //$("#treeview10").shieldTreeView();
        });
        */
    </script>




<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

    $("#button1").click(function(){
        var level = $("#level").val();
        window.open("level_neraca.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&level="+level);
    }); 
    
    function arHide(setID) {
            if( $(setID).hasClass('hide') ) $(setID).removeClass('hide');
            else $(setID).addClass('hide');
        }


$(function() {

  $('input[type="checkbox"]').change(checkboxChanged);

  function checkboxChanged() {
    var $this = $(this),
        checked = $this.prop("checked"),
        container = $this.parent(),
        siblings = container.siblings();

    container.find('input[type="checkbox"]')
    .prop({
        indeterminate: false,
        checked: checked
    })
    .siblings('label')
    .removeClass('custom-checked custom-unchecked custom-indeterminate')
    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

    checkSiblings(container, checked);
  }

  function checkSiblings($el, checked) {
    var parent = $el.parent().parent(),
        all = true,
        indeterminate = false;

    $el.siblings().each(function() {
      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
    });

    if (all && checked) {
      parent.children('input[type="checkbox"]')
      .prop({
          indeterminate: false,
          checked: checked
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(checked ? 'custom-checked' : 'custom-unchecked');

      checkSiblings(parent, checked);
    } 
    else if (all && !checked) {
      indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

      parent.children('input[type="checkbox"]')
      .prop("checked", checked)
      .prop("indeterminate", indeterminate)
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

      checkSiblings(parent, checked);
    } 
    else {
      $el.parents("li").children('input[type="checkbox"]')
      .prop({
          indeterminate: true,
          checked: false
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass('custom-indeterminate');
    }
  }
});




</script>

<script type="text/javascript">
    function viewInput() {
        // alert("test <?php echo $arrayTree[0]; ?>" );
        $('.tombol').removeClass('hide');
        
    }
    </script>

<script type="text/javascript">
    function viewInput1() {
         // alert("test <?php echo $arrayTree[0]; ?>" );
          $('.tombol ').addClass('hide');
            
          
        // $('.tombol').addClass('hide');
        // <?php 
        //     for($x = 0; $x < $arrayTree; $x++) {
        // ?>
            // $('.<?php echo $arrayTree[$x]?>').addClass('hide');

        // <?php
        //     }
        // ?>
    
    }
    </script>





<?php require('footer_new.php');?>
