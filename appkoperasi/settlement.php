<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php include "connectinti.php"; ?>


<!-- Main content -->
<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h3 class="uppercase"><?php echo lang('Settlement'); ?></h3>

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

            <div>
                <label><?php echo lang('Tanggal'); ?></label>
                <input type="text" name="tgl" class="validate" id="tgl" value="<?php echo date('d F Y');?>" readonly>
            </div>

            <label ><?php echo lang('User ID'); ?></label>
            <select id="ind" name="ind" class="browser-default" onChange="showCompany(this.value);">
                <option value=''>- <?php echo ('Pilih User'); ?> -</option>
                    <?php
                    $julsql   = "select * from [dbo].[LoginList] where Status = 1";
                    //echo $julsql;
                    $julstmt = sqlsrv_query($conn, $julsql);
                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                    ?>
                    <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_GET['userid']){echo "selected";} ?>><?=$rjulrow[1];?></option>
                    <?php } ?>
            </select>

            <?php
            if ($_GET['userid']){
                ?>

                <div class="table-responsive" style="margin-top: 10px;">
                    <table class="table table-bordered table-striped" id="assign">
                        <thead>
                            <tr>
                            <th><?php echo lang('Session Number'); ?></th>
                            <th><?php echo lang('Time In/Out'); ?></th>
                            <th><?php echo lang('Begining Balance'); ?></th>
                            <th><?php echo lang('Total Debit'); ?></th>
                            <th><?php echo lang('Total Credit'); ?></th>
                            <th><?php echo lang('End Balance'); ?></th>
                            <th><?php echo lang('Recived Amount'); ?></th>
                            <th><?php echo lang('Settle'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
                            $totala = 0;
                            $totalb = 0;
                            $totalc = 0;
                            $totald = 0;
                            $a = "select * from [dbo].[CashierSession] where UserID='$_GET[userid]' and Status = 1";
                            $b =  sqlsrv_query($conn, $a);
                            while($c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC)){

                                $totala+=$c[3];
                                $totalb+=$c[4];
                                $totalc+=$c[5];
                                $totald+=$c[6];
                            ?>
                            <form action="procsettlement.php" method="post">
                                <tr>
                                    <td><?php echo $c[0]; ?></td>
                                    <td><?php echo $c[12]->format('Y-m-d H:i:s').' / '.$c[13]->format('Y-m-d H:i:s'); ?></td>
                                    <td><?php echo number_format($c[3]); ?></td>
                                    <td><?php echo number_format($c[4]); ?></td>
                                    <td><?php echo number_format($c[5]); ?></td>
                                    <td><?php echo number_format($c[6]); ?></td>
                                    <td align="right">
                                      <input type="hidden" name="idsession" class="validate" value="<?php echo $c[0]; ?>">
                                      <input type="hidden" name="userid" class="validate" value="<?php echo $c[2]; ?>">
                                      <input type="number" name="amount" class="validate" value="0">
                                    </td>
                                    <td><button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button></td>
                                </tr>
                            </form>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">Total</td>
                                <td><?php echo number_format($totala); ?></td>
                                <td><?php echo number_format($totalb); ?></td>
                                <td><?php echo number_format($totalc); ?></td>
                                <td><?php echo number_format($totald); ?></td>
                                <td colspan="2"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            <?php } ?>
    </div>
</div>


<script language="javascript" type="text/javascript">
    function showCompany(ind) {
        window.location.href="settlement.php?userid="+ind;
    }
</script>

<?php require('footer_new.php'); ?>
