<?php 
require('header.php'); 
require('sidebar-left.php'); 
require('sidebar-right.php'); 

// GET LAST ID LocationMerchant
$sqlloc = 'SELECT TOP 1 ProductID FROM [dbo].[LocationParkingProductTopUp] ORDER BY ProductID DESC';
$query = sqlsrv_query($conn, $sqlloc);
$dataloc = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
//echo $dataloc[0].'<br>';
$intaidloc = 0;
$graidloc = substr($dataloc[0], -3);

$gploc = (int)$graidloc + 1;
$intaidloc = str_pad($gploc, 3, '0', STR_PAD_LEFT);
//echo $graidloc.' -- '.$intaidloc.'<br>';
?>

	<div class="animated fadeinup delay-1">
    	<div class="page-content">

    		<h1 class="uppercase">Parking Point</h1>

    		<form class="form-horizontal" action="" method="POST">
    			<input type="hidden" name="prodid" id="prodid" value="<?php echo '700097PRODTOP'.$intaidloc; ?>">

    			<div class="input-field">
    				<input type="text" name="nama" id="nama">
    				<label for="nama">Nama</label>
    			</div>

    			<div class="input-field">
    				<input type="text" name="desk" id="desk">
    				<label for="deskripsi">Deskripsi</label>
    			</div>

    			<div class="input-field">
					<span>Status :</span> <br>
					<select name="status" id="status" class="browser-default">
						<option value="0">Tidak Aktif</option>
						<option value="1">Aktif</option>
					</select>
				</div>

    			<?php
    			$sqlp 	= "SELECT * FROM [dbo].[ParkingSetting]";
    			$query 	= sqlsrv_query($conn, $sqlp);
    			$data	= sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
    			?>
    			<input type="hidden" name="poin" id="poin" value="<?php echo $data[1]; ?>">

    			<div class="input-field">
    				<input type="number" name="jmlpoin" id="jmlpoin">
    				<label for="jmlpoin">Jumlah Poin</label>
    			</div>
    			<div class="input-field">
    				<span>Harga (Rp.)</span><br>
    				<input type="number" name="harga" id="harga" value="0" readonly>
    			</div>

    			<button type="submit" name="ok" id="ok" class="btn btn-large primary-color width-100 waves-effect waves-light">Simpan</button>
    		</form>

    		<?php
    		$ok = @$_POST['ok'];
    		if(isset($ok)) {
    			$id 	= @$_POST['prodid'];
    			$nama 	= @$_POST['nama'];
    			$desk 	= @$_POST['desk'];
    			$poin 	= @$_POST['jmlpoin'];
    			$sts 	= @$_POST['status'];
    			$harga 	= @$_POST['harga']; 

    			$sql = "INSERT into [dbo].[LocationParkingProductTopUp](ProductID, Nama, Deskripsi, Status, JumlahPoinAdd, Harga)
    					values('$id', '$nama', '$desk', '$sts', '$poin', '$harga')";
    			
    			//echo $sql;
    			$query = sqlsrv_query($conn, $sql);
    			?>
				<script type="text/javascript">
					alert('Simpan Gagal');
					window.location.href = "parkingpoint.php";
				</script>
				<?php
    		}
    		?>

    		<div class="table-responsive m-t-30">
                <table class="table">
                	<thead>
                		<tr>
                			<th>Product ID</th>
                			<th>Nama</th>
                			<th>Deskripsi</th>
                			<th>Status</th>
                			<th>Jumlah Poin</th>
                			<th>Jumlah Poin Bonus</th>
                			<th>Harga (Rp.)</th>
                		</tr>
                	</thead>

                	<tbody>
                		<?php
                		$sql = "SELECT * from [dbo].[LocationParkingProductTopUp] ORDER BY ProductID ASC";
                		$query = sqlsrv_query($conn, $sql);
                		while($data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) { ?>
                			<tr>
	                			<td><?php echo $data[0]; ?></td>
	                			<td><?php echo $data[1]; ?></td>
	                			<td><?php echo $data[2]; ?></td>

	                			<?php 
	                			if($data[3] == 0) $status = 'Tidak Aktif';
	                			else $status = 'Aktif';
	                			?>
	                			<td><?php echo $status; ?></td>

	                			<td><?php echo $data[4]; ?></td>
	                			<td><?php echo $data[5]; ?></td>
	                			<td style="text-align: right;"><?php echo number_format($data[6],2,',','.'); ?></td>
                			</tr>
                			<?php
                		}
                		?>
                	</tbody>
                </table>
            </div>
			
		</div>
	</div>

	<script type="text/javascript">
		$('#jmlpoin').keyup(function() {
			var jmlpoin = $('#jmlpoin').val();
			var poin 	= $('#poin').val();
			var harga 	= jmlpoin * poin;

			if(jmlpoin == 0 || jmlpoin == '') $('#harga').val('0');
			else $('#harga').val(harga);

			$('#harga').val(harga);
		});
		
	</script>

<?php require('footer.php'); ?>