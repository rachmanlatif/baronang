<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

    <!-- Main Content -->
    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <!-- Mulai coding disini -->
            <h2 class="uppercase">Dashboard</h2>

            <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                    <?php echo $_SESSION['error-message']; ?>
                </div>
            <?php } ?>

            <?php if(isset($_SESSION['sesiteller'])){ ?>
            <div class="c-widget">
                <div class="c-widget-figure primary-color">
                    <i class="ion-android-mail"></i>
                </div>
                <div class="c-widget-body">
                    <p class="m-0">Sesi Teller Aktif</p>
                    <p class="small m-0"><?php echo $_SESSION['sesiteller']; ?></p>
                </div>
            </div>
          <?php } ?>

          <?php if(isset($_SESSION['sesikasir'])){ ?>
            <div class="c-widget">
                <div class="c-widget-figure primary-color">
                    <i class="ion-android-mail"></i>
                </div>
                <div class="c-widget-body">
                    <p class="m-0">Sesi Kasir Aktif</p>
                    <p class="small m-0"><?php echo $_SESSION['sesikasir']; ?></p>
                </div>
            </div>
          <?php } ?>

            <!-- Akhir baris coding -->
        </div>
    </div> <!-- End of Main Contents -->

<?php require('footer.php');?>
