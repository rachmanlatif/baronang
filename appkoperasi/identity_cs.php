<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php include "connectuser.php"; ?>

<?php include "connectinti.php"; ?>

<?php

$uid = '';
if(isset($_SESSION['RequestID'])){
    $uid = $_SESSION['UID'];
}

if(isset($_POST['barcode'])){
    $uid = $_POST['barcode'];  
}

if(isset($_GET['new'])){
    unset($_SESSION['RequestID']);
    unset($_SESSION['UID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Session closed';
    $_SESSION['error-time'] = time() + 5;
}
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h3 class="uppercase"><?php echo lang('Pelayanan Pelanggan'); ?></h3>

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

            <form id="siden" action="proc_identitycs.php" method="post">
                <div class="input-field">
                    <label for="result-code" class="active">Nomor ID Baronang App</label>
                    <input type="password" class="validate" name="uid" id="result-code" value="<?php echo $uid; ?>" >
                </div>
                <br>
                <div class="row">
                    <div class="col">
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100" id="btn-go"> Go!</button>
                    </div>

                    <div class="col">
                        <button type="button" id="ocam" class="waves-effect waves-light btn-large primary-color width-100"> Scan QR Code </button>
                    </div>

                    <?php if(isset($_SESSION['RequestID'])){ ?>
                    <div class="col">
                        <a href="identity_cs.php?new"><button type="button" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Sesi Baru'); ?></button></a>
                    </div>
                  <?php } ?>
                </div>

            </form>

        <div id="menu">

        </div>

        <div class="box box-solid hide" id="form-camera">
            <h3 class="uppercase">Scan QR Code/Barcode</h3><br>

            <b>Pilih Kamera</b>
            <select class="browser-default" id="camera-select"></select>
            <br>
            <div class="box-body">
                <div class="row">
                    <div class="col">
                        <button title="Start Scan QR Barcode" class="waves-effect waves-light btn-large primary-color width-100" id="play" type="button">Start</button>
                    </div>
                    <div class="col">
                        <button title="Stop streams" class="waves-effect waves-light btn-large primary-color width-100" id="stop" type="button">Stop</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="well" style="position: relative;display: inline-block;">
                            <canvas id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="thumbnail" id="result">
                            <img id="scanned-img" style="width: 100%;" src="">
                            <div class="caption">
                                <p class="text-center" id="scanned-QR"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if($_SESSION['device'] == 0){ ?>
            <script type="text/javascript">
                $('#ocam').click(function(){
                    Intent.openActivity("QRActivity","identity_cs.php");
                  });
            </script>
        <?php } ?>

        <?php if($_SESSION['device'] == 1){ ?>
            <script type="text/javascript">
                $('#ocam').click(function(){
                    $('#result-code').val();
                    $('#form-camera').removeClass('hide');
                });

                $('#stop').click(function(){
                    $('#form-camera').addClass('hide');
                });
            </script>
        <?php } ?>

        <script type="text/javascript">
            var uid = $('#result-code').val();
            $.ajax({
                url : "identity_menu.php",
                type : 'POST',
                data: { uid: uid},
                success : function(data) {
                    $("#menu").html(data);
                },
                error : function(){
                    alert('Try again.');
                    return false;
                }
            });

            $('#result-code').keyup(function(){
                var uid = $('#result-code').val();
                if(uid.length == 16){
                    $('#siden').submit();
                }
            });
        </script>

    </div>
</div>

<?php require('footer.php');?>
