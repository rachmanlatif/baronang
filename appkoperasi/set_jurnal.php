<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Pengaturan Jurnal'); ?></h2>

        <div class="form-inputs">
            <form action="procsetjurnal.php" method = "POST">

                <ul class="faq collapsible animated fadeinright delay-1" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"> <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Akun Bank'); ?> </div>
                        <div class="collapsible-body" style="padding: 10px;">

                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th>KBA</th>
                                        <th>COA</th>
                                    </tr>
                                    <?php /*
                                    $index = 0;
                                    $sql = "select * from dbo.BankAccountView";
                                    $stmt = sqlsrv_query($conn, $sql);
                                    while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="kode0[]" value="<?php echo $row[1]; ?>" readonly>
                                                <input type="hidden" name="status0[]" value="0" readonly>
                                                <?php echo $row[4].' - '.$row[5]; ?>
                                            </td>
                                            <td>
                                                <select name="coa0[]" class="form-control select2" style="width: 100%;" required="">
                                                    <option value=""><?php echo lang('--- Pilih Satu ---'); ?></option>
                                                    <?php
                                                    $a = "select * from dbo.AccountMax";
                                                    $b = sqlsrv_query($conn, $a);
                                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                                        $aa = "select * from dbo.SetingJurnal where Status = 0 and Kode = '$row[1]' and KodeAcc = '$c[0]'";
                                                        $bb = sqlsrv_query($conn, $aa);
                                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                                        if($cc != null){
                                                            ?>
                                                            <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                    <?php $index++; } */ ?>
                                </table>
                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header"> <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Simpanan Pokok'); ?> </div>
                        <div class="collapsible-body" style="padding: 10px;">

                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><?php echo lang('Produk'); ?></th>
                                        <th>COA</th>
                                    </tr>
                                    <?php /*
                                    $index2 = $index;
                                    $sql = "select * from dbo.BasicSavingTypeView";
                                    $stmt = sqlsrv_query($conn, $sql);
                                    while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="kode1[]" value="<?php echo $row[0]; ?>" readonly>
                                                <input type="hidden" name="status1[]" value="1" readonly>
                                                <?php echo $row[0].' - '.$row[1]; ?>
                                            </td>
                                            <td>
                                                <select name="coa1[]" class="form-control select2" style="width: 100%;" required="">
                                                    <option value=""><?php echo lang('--- Pilih Satu ---'); ?></option>
                                                    <?php
                                                    $a = "select * from dbo.AccountMax";
                                                    $b = sqlsrv_query($conn, $a);
                                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                                        $aa = "select * from dbo.SetingJurnal where Status = 1 and Kode = '$row[0]' and KodeAcc = '$c[0]'";
                                                        $bb = sqlsrv_query($conn, $aa);
                                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                                        if($cc != null){
                                                            ?>
                                                            <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                    <?php $index2++; } */ ?>
                                </table>
                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header"> <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Tabungan'); ?> </div>
                        <div class="collapsible-body" style="padding: 10px;">

                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><?php echo lang('Produk'); ?></th>
                                        <th>COA</th>
                                    </tr>
                                    <?php /*
                                    $index3 = $index2;
                                    $sql = "select * from dbo.RegularSavingTypeView";
                                    $stmt = sqlsrv_query($conn, $sql);
                                    while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="kode2[]" value="<?php echo $row[0]; ?>" readonly>
                                                <input type="hidden" name="status2[]" value="2" readonly>
                                                <?php echo $row[0].' - '.$row[1]; ?>
                                            </td>
                                            <td>
                                                <select name="coa2[]" class="form-control select2" style="width: 100%;" required="">
                                                    <option value=""><?php echo lang('--- Pilih Satu ---'); ?></option>
                                                    <?php
                                                    $a = "select * from dbo.AccountMax";
                                                    $b = sqlsrv_query($conn, $a);
                                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                                        $aa = "select * from dbo.SetingJurnal where Status = 2 and Kode = '$row[0]' and KodeAcc = '$c[0]'";
                                                        $bb = sqlsrv_query($conn, $aa);
                                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                                        if($cc != null){
                                                            ?>
                                                            <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                    <?php $index3++; } */ ?>
                                </table>
                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header"> <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Deposito'); ?> </div>
                        <div class="collapsible-body" style="padding: 10px;">

                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><?php echo lang('Produk'); ?></th>
                                        <th>COA</th>
                                    </tr>
                                    <?php /*
                                    $index4 = $index3;
                                    $sql = "select * from dbo.TimedepositTypeView";
                                    $stmt = sqlsrv_query($conn, $sql);
                                    while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="kode3[]" value="<?php echo $row[0]; ?>" readonly>
                                                <input type="hidden" name="status3[]" value="3" readonly>
                                                <?php echo $row[0].' - '.$row[1]; ?>
                                            </td>
                                            <td>
                                                <select name="coa3[]" class="form-control select2" style="width: 100%;" required="">
                                                    <option value=""><?php echo lang('--- Pilih Satu ---'); ?></option>
                                                    <?php
                                                    $a = "select * from dbo.AccountMax";
                                                    $b = sqlsrv_query($conn, $a);
                                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                                        $aa = "select * from dbo.SetingJurnal where Status = 3 and Kode = '$row[0]' and KodeAcc = '$c[0]'";
                                                        $bb = sqlsrv_query($conn, $aa);
                                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                                        if($cc != null){
                                                            ?>
                                                            <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php $index4++; } */ ?>
                                </table>
                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header"> <i class="ion-android-arrow-dropdown right"></i> <?php echo lang('Pinjaman'); ?> </div>
                        <div class="collapsible-body" style="padding: 10px;">

                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th><?php echo lang('Produk'); ?></th>
                                        <th>COA</th>
                                    </tr>
                                    <?php /*
                                    $index5 = $index4;
                                    $sql = "select * from dbo.LoanTypeView";
                                    $stmt = sqlsrv_query($conn, $sql);
                                    while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="kode4[]" value="<?php echo $row[0]; ?>" readonly>
                                                <input type="hidden" name="status4[]" value="4" readonly>
                                                <?php echo $row[0].' - '.$row[1]; ?>
                                            </td>
                                            <td>
                                                <select name="coa4[]" class="form-control select2" style="width: 100%;" required="">
                                                    <option><?php echo lang('--- Pilih Satu ---'); ?></option>
                                                    <?php
                                                    $a = "select * from dbo.AccountMax";
                                                    $b = sqlsrv_query($conn, $a);
                                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                                        $aa = "select * from dbo.SetingJurnal where Status = 4 and Kode = '$row[0]' and KodeAcc = '$c[0]'";
                                                        $bb = sqlsrv_query($conn, $aa);
                                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                                        if($cc != null){
                                                            ?>
                                                            <option value="<?php echo $c[0]; ?>" selected><?php echo $c[1]; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php $index5++; } */ ?>
                                </table>
                            </div>

                        </div>
                    </li>
                </ul>

                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                </div>

            </form>
        </div>

    </div>
</div>

<?php require('footer_new.php');?>