<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Daftar Simpanan");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    

    $objWorkSheet->getStyle('B1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B1', 'Daftar Simpanan');


    $objWorkSheet->getStyle('B2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B2', 'Jenis Simpanan');
    
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No');
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'Kode Akun');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', ' Nama');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Saldo');
    



    if(isset($_GET['id'])){

    $a = "select * from [dbo].[BasicSavingTypeView] where KodeBasicSavingType='$_GET[id]'";
    //echo $a;
    $b =  sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);


    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("C2",$c[1]);
    

    $row = 6;
    $ind = $_GET['id'];
    $aaa = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[BasicSavingBalance] where KodeBasicSavingType = '$ind') a";
    //echo $aaa;
    $bbb = sqlsrv_query($conn, $aaa);
    $total= $awal;
    $ctotal = $total;
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        $sql   = "select * from dbo.MemberListView where MemberID='$ccc[1]'";
        //echo $sql;
        $stmt  = sqlsrv_query($conn, $sql);
        $coba   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
        $hcoba = $coba[2];
        
                                               

            $objWorkSheet->SetCellValue("A".$row,$ccc[6]);
            $objWorkSheet->SetCellValue("B".$row, $ccc[3]);
            $objWorkSheet->SetCellValue("C".$row,$hcoba);
            $objWorkSheet->getStyle('D' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("D".$row, number_format($ccc[5],2), PHPExcel_Cell_DataType::TYPE_STRING);
                

        $no++;
        $row++;
        }              
        $objWorkSheet->getStyle('A5:D' .$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            
    }
//exit;
    $objWorkSheet->setTitle('Drep Daftar Simpanan');

    $fileName = 'DafSimpanan'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
