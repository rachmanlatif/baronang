<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">

<style>
    .tengah{
        text-align:center;
        }
    .kiri{
        text-align:left;
        }
    .kanan{
        text-align:right;
        }

</style>




    <div class="page-content">

        <h3 class="box-title"><?php echo lang('Neraca'); ?></h3>

        <div class="form-inputs">
         <?php if(!isset($_GET['acc']) and !isset($_GET['acc1']) and !isset($_GET['acc2']) and !isset($_GET['acc3']) and !isset($_GET['acc4']) and !isset($_GET['acc5']) and !isset($_GET['acc6'])){ ?>
            <form action="" method="POST">
                <form>
                <h5 class="box-title"><?php echo lang('Periode dan Template'); ?></h5>
                <div class="input-field">

                    <?php
                        if(isset($_POST['bulan'])){
                            $from = $_POST['bulan'];
                        }
                        else{
                            $from = date('Y/m/d');
                        }
                        ?>


                        <select id="bulan" name="bulan" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['bulan']; ?>">- <?php echo lang('Pilih Bulan'); ?> -</option>
                            <?php
                            $bln=array(01=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
                            echo $bln;
                            $from='';
                            for ($bulan=01; $bulan<=12; $bulan++) { 
                                 if ($_POST['bulan'] == $bulan){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$bulan' $ck>$bln[$bulan]</option>";


                            }
                            ?>
                        </select>

                </div>
                <div class="input-field">
                    <?php
                        if(isset($_POST['tahun'])){
                            $to = $_POST['tahun'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>

                        <select id="tahun" name="tahun" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['tahun']; ?>">- <?php echo lang('Pilih Tahun'); ?> -</option>
                            <?php
                            $now=date("Y");
                            for ($tahun=2017; $tahun<=$now; $tahun++) { 
                                 if ($_POST['tahun'] == $tahun){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$tahun' $ck>$tahun</option>"; 
                            }

                            ?>
                        </select>
                </div>

                <div class="input-field">
                    <?php
                        if(isset($_POST['template'])){
                            $to = $_POST['template'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>

                        <select id="template" name="template" class="browser-default">
                            <option value=''>- <?php echo lang('Pilih Template'); ?> -</option>
                            <?php
                            $julsql   = "select * from [dbo].[Template] where Tipe = 0";
                            echo $julsql;
                            $julstmt = sqlsrv_query($conn, $julsql);
                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                               ?>
                            <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_POST['template']){echo "selected";} ?>><?=$rjulrow[1];?></option>
                    <?php } ?>
                        </select>

                </div>


                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_neraca.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
                </form>
            </form>
        </div>
        <?php } ?>




        <?php if(isset($_POST['bulan']) and isset($_POST['tahun']) and isset($_POST['template']) and !isset($_POST['acc'])){
                    $from1 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                    $from = date('Y-m-01', strtotime($from1));
                    $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                    $to = date('Y-m-t', strtotime($to3));
                    $to1 = date('m', strtotime($to3));
                    $to2 = date('Y', strtotime($to3));
                    $a = "exec dbo.ProsesGenerateLaporanNeraca '$to','$_SESSION[Name]'";
                    //echo $a;
                    $b = sqlsrv_query($conn, $a);
                    ?>

                    <div class="row m-l-0">
                        <!--<div class="col">
                             <a onclick="printDiv()" href="#"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-example" onclick="arHide('#modal-example')"> <?php echo lang('Print'); ?></button></a>
                        </div> -->
                        <div class="col">
                            <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-confirm" onclick="arHide('#modal-confirm')"> <?php echo lang('Download Template'); ?></button>
                        </div>
                        <div class="col">
                            <a href="rep_template.php?&st=5&set=00" target="_blank"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="delete"> <?php echo lang('Template'); ?></button>
                        </div>
                    </div>


                     <div class="hide" id="modal-confirm">
                        <h3 class="uppercase"><?php echo lang('Konfirmasi'); ?></h3>
                        <?php echo lang('Sudah Yakin Download Template ?'); ?><br><br>
                        <div class="row m-l-0">
                            <div class="col">
                                <a href="dneraca.php?bulan=<?php echo $_POST['bulan']; ?>&tahun=<?php echo $_POST['tahun']; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" target="_blank"><?php echo lang('Ya'); ?></button></a>
                            </div>
                           
                            <div class="col">
                                <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" onclick="arHide('#modal-confirm')"><?php echo lang('Tidak'); ?></button>
                            </div>
                        </div>
                    </div>

                    
                    <!--<div method="POST" action="">
                    <select id="level" name="level" class="browser-default" style="margin-top: 30px;">
                        <option value="">-Pilih Level-</option>
                        <option value="0">-0-</option>
                        <option value="1">-1-</option>
                        <option value="2">-2-</option>
                    </select>
                    <?php
                    if(isset($_GET['submit'])){
                        if(empty($_GET['level']))
                        {
                            $level= "Eror";
                        } else {
                            $level=$_GET['level'];
                        }
                    } 
                    ?>


                        <div style="margin-top: 20px;">
                            <button type="submit" class="waves-effect waves-light btn-large primary-color width-100" name="submit1" id="button1" value="submit"><?php echo lang('Pilih'); ?></button>
                        </div>
                    </div>-->

                 <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">

                <div class="box-header" align="center">
                    <tr>
                        <h3 class="" style="margin-top: 30px;"><?php echo ('Laporan Neraca'); ?></h3>
                    </tr>
                </div>
                <div class="box-header with-border" align="center">
                    <tr>
                        <?php
                            $bulan = $_POST['bulan'];
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        if($bulan !=12){
                                                                            Eror;
                                                                        } else {
                                                                           $bulan = 'Desember';
                                                                        }
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'OKtober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
                        <h3 class="box-title" style="margin-bottom: 30px;"><?php echo "Periode : "; ?></h3>
                        <h3 class="box-title" style="margin-bottom: 30px;"><?php echo $bulan," - "; ?></h3>
                        <h3 class="box-title" style="margin-bottom: 30px;"><?php echo $_POST['tahun']; ?></h3>
                    </tr>
                    
                </div>

                <?php
                $term = $_POST['template'];
                $ar = array("");
                $c = " select * from dbo.TemplateDetail where IDTemplate = $term";
                //echo $c;
                $d = sqlsrv_query($conn, $c);
                while($zz = sqlsrv_fetch_array($d, SQLSRV_FETCH_NUMERIC)){
                    $t = $zz[2];
                    array_push($ar, $t);
                    
                 } //var_dump($ar);
            
                ?>
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" width="50%"><?php echo lang('Aktiva'); ?></th>
                            <th colspan="2" width="50%"><?php echo lang('Pasiva'); ?></th>
                        </tr>
                        <tr>
                            <div>
                            <td colspan="2" width="50%">
                                <table class="table table-bordered table-striped" width="50%">
                                <tbody>
                                <button onclick="viewInput()" width="100px">Show</button>
                                <button onclick="viewInput1()" width="100px">Hide</button>
                                
                                    <?php
                                        $from1 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                        $from = date('Y-m-01', strtotime($from1));
                                        $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                        $to = date('Y-m-t', strtotime($to3));
                                        $to1 = date('m', strtotime($to3));
                                        $to2 = date('Y', strtotime($to3));
                                        $name = $_SESSION[Name];
                                        $x = "select* from dbo.GroupAcc where KodeGroup in('1.0.00.00.000') and KodeGroup in (select Account from TemplateDetail)";
                                        $atotal = 0;
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                        	$hide = '';
                                            if (in_array($z[0], $ar)) {
                                            ?>
                                            <tr>
                                            	<td><?php echo ($z[0]); ?></label></td>
                                            	<td><?php echo ($z[1]); ?></td>
                                            	<td></td>
                                                <td><button id = "button2" onclick = "displayLoginBox()"></td>
                                        	</tr>
                                        	<?php } else {
                                            $hide = "hide";
                                            //echo "$z[0]";
                                        	?>
                                    	    <tr class="tombol <?php echo  $hide; ?>">
                                        	    <td><?php echo ($z[0]); ?></label></td>
                                            	<td><?php echo ($z[1]); ?></td>
                                            	<td></td>
                                                <td></td>
                                        	</tr>
                                      		<?php } ?>
                                            
                                            <?php
                                            $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' and KodeTipe in (select Account from TemplateDetail) ORDER BY KodeTipe Asc";
                                            $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where GroupID = '$z[0]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name'";
                                            $bcd = sqlsrv_query($conn, $abc);
                                            $b = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                            $dtotal1=$b[0];
                                            $atotal=$atotal+$dtotal1;
                                            $yy = sqlsrv_query($conn, $xx);
                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                                $hide = '';
                                                $hide1 = '';
                                             	if (in_array($zz[0], $ar)) {
                                             	?> 
                                        		<tr>
                                            		<td>&emsp;&emsp;<?php echo ($zz[0]); ?></label></td>
                                            		<td><?php echo ($zz[1]); ?></td>
                                            		<td></td>
                                                    <td><button id = "button3" onclick = "displayLoginBox()"></button></td>
                                        		</tr>
                                        		<!-- sub 1 -->
                                         		<?php } else { 

                                            	$hide = "hide";
                                                $hide1 = "hide";
                                        		?>
                                         		<tr class="tombol2">
                                            		<td>&emsp;&emsp;<?php echo ($zz[0]); ?></label></td>
                                            		<td><?php echo ($zz[1]); ?></td>
                                            		<td></td>
                                                    <td></td>
                                        		</tr>
                                        		<?php } ?>

                                                <?php
                                                $xxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount in (select Account from TemplateDetail) ORDER BY KodeAccount Asc"; 
                                                    //echo $xxx;
                                                $yyy = sqlsrv_query($conn, $xxx);
                                                while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                	 $hide = '';
                                             		 if (in_array($zzz[0], $ar)) {
                                                        $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%'";
                                                        //echo $abc;
                                                        $bcd = sqlsrv_query($conn, $abc);
                                                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                        $c=$b[0];
                                                        //echo $total2; 
                                                       
                                             			?>
                                             			<tr>
                                                        	<td>&emsp;&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></td>
                                                        	<td><?php echo ($zzz[1]); ?></td>
                                                        	<td class="kanan"><?php echo number_format($c,2); ?></td>
                                                            <td></td>
                                                    	</tr>
                                             			<?php } else {		
                                               			$hide = "hide";
                                                    	?>
                                                    
                                                    	<tr class="tombol3">
                                                        	<td>&emsp;&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></td>
                                                        	<td><?php echo ($zzz[1]); ?></td>
                                                        	<td class="kanan"><?php echo number_format($c,2); ?></td>
                                                            <td></td>
                                                    	</tr>
                                                    	<?php } ?>
                                                        
                                                        <?php
                                                        $xxxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount in (select Account from TemplateDetail) and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                        //echo $xxxx;
                                                        $yyyy = sqlsrv_query($conn, $xxxx);
                                                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                            
                                                            $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[0]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%'";
                                                            //echo $abc;
                                                            $bcd = sqlsrv_query($conn, $abc);
                                                            $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                            $a=$b[0];
                                                            //echo $total3;
                                                            $hide = '';
                                             				if (in_array($zzzz[0], $ar)) {
                                             				?>
                                             				<tr>
                                                            	<td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></label></td>
                                                            	<td><?php echo ($zzzz[1]); ?></td>
                                                            	<td class="kanan"><?php echo number_format($a,2); ?></td>
                                                                <td></td>
                                                        	</tr>
                                             				<?php } else {
                                                			$hide = "hide";
                                                        	?>
                                                        	<tr class="tombol <?php echo  $hide; ?> ">
	                                                            <td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></td>
                                                            	<td><?php echo ($zzzz[1]); ?></td>
                                                            	<td class="kanan"><?php echo number_format($a,2); ?></td>
                                                                <td></td>
                                                        	</tr>
                                                        	<?php } ?>
                                                        

                                                            <?php
                                                            $from1 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                            $from = date('Y-m-01', strtotime($from1));
                                                            $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                            $to = date('Y-m-t', strtotime($to3));
                                                            $to1 = date('m', strtotime($to3));
                                                            $to2 = date('Y', strtotime($to3));
                                                            $name = $_SESSION[Name];
                                                            $xxxxx ="select * from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and UserName = '$name' and KodeAccount in (select Account from TemplateDetail) and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                                //echo $xxxxx;
                                                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  
                                                            $hide = '';
                                             					if (in_array($zzzzz[0], $ar)) {
                                             					?>
                                             					<tr>
                                                                    <td><a href="rep_neracalap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[0]; ?>&st=5" target="_blank">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></a></td>
                                                                    <td><?php echo ($zzzzz[1]); ?></td>
                                                                    <td class="kanan"><?php echo ($zzzzz[8]); ?></td>
                                                                    <td></td>
                                                                </tr>
                                                                <?php } else {
																	$hide = "hide";
                                                                ?>
                                                                <tr class="tombol <?php echo  $hide; ?> ">
                                                                    <td><a href="rep_neracalap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[0]; ?>&st=5" target="_blank">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></a></td>
                                                                    <td><?php echo ($zzzzz[1]); ?></td>
                                                                    <td class="kanan"><?php echo ($zzzzz[8]); ?></td>
                                                                    <td></td>
                                                                </tr>
                                                                <?php  } ?>
                                                                    <?php  } ?>
                                                        <?php } ?>
                                                <?php } ?>
                                            <?php } ?>        
                                 <?php $dtotal1+=$b[0];} ?>
                            </tbody>
                            </table>
                            </td>
                            </div>


                            <td colspan="2">
                                <table class="table table-bordered table-striped" width="50%">
                                    <tbody>
                                    <button onclick="viewInput2()" width="100px">Show</button>
	                                <button onclick="viewInput3()" width="100px">Hide</button>
                      
                                    <?php
                                        $from1 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                        $from = date('Y-m-01', strtotime($from1));
                                        $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                        $to = date('Y-m-t', strtotime($to3));
                                        $to1 = date('m', strtotime($to3));
                                        $to2 = date('Y', strtotime($to3));
                                        $name = $_SESSION[Name];
                                        $btotal = 1;
                                        $x = "select* from dbo.GroupAcc where KodeGroup in('2.0.00.00.000','3.0.00.00.000') and KodeGroup in (select Account from TemplateDetail)";
                                        //echo $x;
                                        $total1=0;
                                        $m=1;
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                            $hide = '';
                                            if (in_array($z[0], $ar)) {
                                                //echo "$z[0]";
                                        ?> 
                                        <tr>
                                            <td><?php echo ($z[0]); ?></td>
                                            <td><?php echo ($z[1]); ?></td>
                                            <td></td>
                                        </tr>
                                        <?php } else {
                                         	$hide = "hide";
                                         	?>
                                         <tr class="tombol1 <?php echo  $hide; ?>">
                                            <td><?php echo ($z[0]); ?></td>
                                            <td><?php echo ($z[1]); ?></td>
                                            <td></td>
                                        </tr>
                                        <?php } ?>
                                       
                                            <?php
                                           
                                            $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' and KodeTipe in (select Account from TemplateDetail) ORDER BY KodeTipe Asc";
                                            //echo $xx;
                                            $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where GroupID = '$z[0]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name'";
                                            //echo $abc;
                                            $bcd = sqlsrv_query($conn, $abc);
                                            $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                            $atotal1=$b[0];
                                            $total1= $total1+$atotal1;
                                            //echo $atotal1; 
                                            $yy = sqlsrv_query($conn, $xx);
                                            $total2=0;
                                            $total3=0;
                                            $ab = 11;
                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                                $hide = '';
                                            	if (in_array($zz[0], $ar)) {
                                            	?>
                                            	<tr>
                                                	<td>&emsp;&emsp;<?php echo ($zz[0]); ?></td>
                                                	<td><?php echo ($zz[1]); ?></td>
                                                	<td></td>
                                            	</tr>
                                            	<?php } else {
                                        	   	$hide = "hide";
                                        		?>
	                                         	<tr class="tombol1 <?php echo  $hide; ?>">
	                                                <td>&emsp;&emsp;<?php echo ($zz[0]); ?></td>
	                                                <td><?php echo ($zz[1]); ?></td>
	                                                <td></td>
	                                            </tr>
	                                            <?php } ?>
                                           
                                                <?php
                                           	    $xxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and UserName = '$name' and KodeAccount in (select Account from TemplateDetail)ORDER BY KodeAccount Asc";
                                                //echo $xxx;
                                                   
                               		            $yyy = sqlsrv_query($conn, $xxx);
                                                $abc = 111;
                                                while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                	$hide = '';
                                             		if (in_array($zzz[0], $ar)) {
                                                	
                                                    	$abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name'  and KodeAccount in (select Account from TemplateDetail)  and KodeAccount like '%".substr($zzz[0],0,6)."%'";
                                                        //echo $abc;
                                                        $bcd = sqlsrv_query($conn, $abc);
                                                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                        $c=$b[0];
                                                        //echo $total2; 
                                                     	?>
	                                                    <tr>
	                                                        <td>&emsp;&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></td>
	                                                        <td><?php echo ($zzz[1]); ?></td>
	                                                        <td class="kanan"><?php echo number_format($c,2); ?></td>
	                                                    </tr>
	                                                    <?php } else {
	                                                    $hide = "hide";
	                                                    ?>  
	                                                    <tr class="tombol1 <?php echo  $hide; ?> ">
	                                                        <td>&emsp;&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></td>
	                                                        <td><?php echo ($zzz[1]); ?></td>
	                                                        <td class="kanan"><?php echo number_format($c,2); ?></td>
	                                                    </tr>
                                                    	<?php } ?>
                                                    
                                                        <?php
                                                        $xxxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name'  and KodeAccount in (select Account from TemplateDetail)  and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                        //echo $xxxx;
                                                        $abcd = 1111;
                                                        $yyyy = sqlsrv_query($conn, $xxxx);
                                                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                            $hide = '';
                                            				if (in_array($zzzz[0], $ar)) {
                                                			//echo "$z[0]";
                                                            $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%'";
                                                            //echo $abc;
                                                            $bcd = sqlsrv_query($conn, $abc);
                                                            $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                            $a=$b[0];
                                                            //echo $total3;
	                                                        ?>
	                                                        <tr>
	                                                    
	                                                            <td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></td>
	                                                            <td><?php echo ($zzzz[1]); ?></td>
	                                                            <td class="kanan"><?php echo number_format($a,2); ?></td>
	                                                        </tr>
	                                                        <?php } else {
	                                                        $hide = "hide";
	                                                        ?>
	                                                        <tr class="tombol1 <?php echo  $hide; ?>">
	                                                    
	                                                            <td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></td>
	                                                            <td><?php echo ($zzzz[1]); ?></td>
	                                                            <td class="kanan"><?php echo number_format($a,2); ?></td>
	                                                        </tr>
	                                                       	<?php } ?>

                                                            <?php
                                                            $from1 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                            $from = date('Y-m-01', strtotime($from1));
                                                            $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                            $to = date('Y-m-t', strtotime($to3));
                                                            $to1 = date('m', strtotime($to3));
                                                            $to2 = date('Y', strtotime($to3));
                                                            $name = $_SESSION[Name];
                                                            $abcde = 11111;
                                                            $xxxxx ="select * from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name'  and KodeAccount in (select Account from TemplateDetail)  and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                                //echo $xxxxx;
                                                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  
                                                                $hide = '';
                                            					if (in_array($zzzzz[0], $ar)) {
                                        	    				//echo "$z[0]";
                                                                ?>
                                                                <tr>
                                                                    <td><a href="rep_neracalap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[0]; ?>&st=5" target="_blank">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></a></td>
                                                                    <td><?php echo ($zzzzz[1]); ?></td>
                                                                    <td class="kanan"><?php echo ($zzzzz[8]); ?></td>
                                                                </tr>
                                                                <?php } else {
                                                                $hide = "hide";
                                                                ?>
                                                                <tr class="tombol1 <?php echo  $hide; ?>">
                                                                    <td><a href="rep_neracalap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[0]; ?>&st=5" target="_blank">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></a></td>
                                                                    <td><?php echo ($zzzzz[1]); ?></td>
                                                                    <td class="kanan"><?php echo ($zzzzz[8]); ?></td>
                                                                </tr>
                                                                <?php  } ?>                    
                                                        <?php   } ?>
                                                <?php  } ?>
                                                 <?php } ?>
                                             <?php } ?>
                                 <?php $m++; $atotal1=$b[0];  } ?>
                            </tbody>
                    </table>
                    </td>

                            <tr>
                                <th><?php echo lang('Total Aktiva'); ?></th>
                                <th><span class="right"><?php echo number_format($atotal,2); ?></span></th>
                                <th><?php echo lang('Total Pasiva'); ?></th>
                                <th><span class="right"><?php echo number_format($total1,2); ?></span></th>
                            </tr>
                </div> 

       
        <?php } ?>
    </div>

   
</div>


<!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview1").shieldTreeView();
    });
</script>
<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview3").TreeView();
        collapsed: true,
        unique: true,
        persist: “location”
    });
</script>

<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

    $("#button1").click(function(){
        var level = $("#level").val();
        window.open("level_neraca.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&level="+level);
    }); 
    
    function arHide(setID) {
            if( $(setID).hasClass('hide') ) $(setID).removeClass('hide');
            else $(setID).addClass('hide');
        }


$(function() {

  $('input[type="checkbox"]').change(checkboxChanged);

  function checkboxChanged() {
    var $this = $(this),
        checked = $this.prop("checked"),
        container = $this.parent(),
        siblings = container.siblings();

    container.find('input[type="checkbox"]')
    .prop({
        indeterminate: false,
        checked: checked
    })
    .siblings('label')
    .removeClass('custom-checked custom-unchecked custom-indeterminate')
    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

    checkSiblings(container, checked);
  }

  function checkSiblings($el, checked) {
    var parent = $el.parent().parent(),
        all = true,
        indeterminate = false;

    $el.siblings().each(function() {
      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
    });

    if (all && checked) {
      parent.children('input[type="checkbox"]')
      .prop({
          indeterminate: false,
          checked: checked
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(checked ? 'custom-checked' : 'custom-unchecked');

      checkSiblings(parent, checked);
    } 
    else if (all && !checked) {
      indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

      parent.children('input[type="checkbox"]')
      .prop("checked", checked)
      .prop("indeterminate", indeterminate)
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

      checkSiblings(parent, checked);
    } 
    else {
      $el.parents("li").children('input[type="checkbox"]')
      .prop({
          indeterminate: true,
          checked: false
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass('custom-indeterminate');
    }
  }
});




</script>

<script type="text/javascript">
    function viewInput() {
        // alert("test <?php echo $arrayTree[0]; ?>" );
        $('.tombol').removeClass('hide');
        
    }
    </script>

<script type="text/javascript">
    function viewInput1() {
         // alert("test <?php echo $arrayTree[0]; ?>" );
          $('.tombol').addClass('hide');
            
          
        // $('.tombol').addClass('hide');
        // <?php 
        //     for($x = 0; $x < $arrayTree; $x++) {
        // ?>
            // $('.<?php echo $arrayTree[$x]?>').addClass('hide');

        // <?php
        //     }
        // ?>
    
    }
    </script>

<script type="text/javascript">
    function viewInput2() {
        // alert("test <?php echo $arrayTree[0]; ?>" );
        $('.tombol1').removeClass('hide');
        
    }
    </script>

<script type="text/javascript">
    function viewInput3() {
         // alert("test <?php echo $arrayTree[0]; ?>" );
          $('.tombol1').addClass('hide');
            
    }
    </script>

<script type="text/javascript">
    function viewInput4() {
        // alert("test <?php echo $arrayTree[0]; ?>" );
        $('.tombol2').removeClass('hide');
        $('.tombol2').addClass('hide');  
        
    }

    $('#button2').on('click', function(e){

    $(".tombol2").toggle();
    $(this).toggleClass('')
});

     $('#button3').on('click', function(e){

    $(".tombol3").toggle();
    $(this).toggleClass('')
});
    </script>



<?php require('footer_new.php');?>
