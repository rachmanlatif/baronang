<?php
include 'connect.php';
include 'connectuser.php';

$qr = $_POST['qr'];
$barcode = str_replace(' ','+',$qr);

$sql1 	= "SELECT * from [dbo].[MasterCard] where Barcode='$barcode'";
$query1 = sqlsrv_query($conn, $sql1);
$data1 	= sqlsrv_fetch_array($query1, SQLSRV_FETCH_NUMERIC);

$sql2 	= "SELECT * from [dbo].[MemberCard] where CardNo='".$data1[0]."'";
$query2 = sqlsrv_query($conn, $sql2);
$data2 	= sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC);

$sql3 	= "SELECT * from [dbo].[MemberList] where MemberID='".$data2[0]."'";
$query3 = sqlsrv_query($conn, $sql3);
$data3 	= sqlsrv_fetch_array($query3, SQLSRV_FETCH_NUMERIC);

$lokasi = '';
$jam = '';
$sql8 	= "SELECT top 1 * from [dbo].[ParkingList] where CardNo='".$data1[0]."' and Status = 0 order by TimeIn desc";
$query8 = sqlsrv_query($conn, $sql8);
$data8 	= sqlsrv_fetch_array($query8, SQLSRV_FETCH_NUMERIC);
if($data8 != null){
		$sql55 = "SELECT * from [dbo].[LocationMerchant] where LocationID='".$data8[1]."'";
		$query55 = sqlsrv_query($conn, $sql55);
		$data55 = sqlsrv_fetch_array($query55, SQLSRV_FETCH_NUMERIC);
		if($data55 != null){
			$lokasi = $data55[2];
		}
		$jam = $data8[3]->format('Y/m/d H:i:s');
}
?>

<div class="form-input">
	<div class="input-field">
		<span style="font-weight: bold;">Nomor Kartu</span>
		<input type="text" name="nokartu" id="nokartu" value="<?php echo $data1[0]; ?>" readonly>
	</div>

	<div class="input-field">
		<span style="font-weight: bold;">Nama</span>
		<input type="text" name="nama" id="nama" value="<?php echo $data3[2]; ?>" readonly>
	</div>

	<div class="input-field">
		<span style="font-weight: bold;">Saldo Poin</span>
		<input type="text" name="saldopoin" id="saldopoin" value="<?php echo $data2[2].' Poin'; ?>" readonly>
	</div>

	<?php if($jam != ''){ ?>
	<div class="input-field">
		<span style="font-weight: bold;">Lokasi Masuk</span>
		<input type="text" name="lokasi" id="nama" value="<?php echo $lokasi; ?>" readonly>
	</div>

	<div class="input-field">
		<span style="font-weight: bold;">Jam Masuk</span>
		<input type="text" name="jam" id="nama" value="<?php echo $jam; ?>" readonly>
		<b>Kartu sedang masuk</b>
	</div>
	<?php } ?>

	<div class="input-field">
		<span style="font-weight: bold;">Member di</span>: <br>
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>Lokasi</th>
						<th>Kendaraan</th>
						<th>Bulan Member</th>
						<th>Tahun</th>
					</tr>
				</thead>

				<tbody>
					<?php
					$year = date('Y');
					$sql4 = "SELECT * from [dbo].[MemberLocation] where MemberID='".$data2[0]."'";
					$query4 = sqlsrv_query($conn, $sql4);
					while($data4 = sqlsrv_fetch_array($query4, SQLSRV_FETCH_NUMERIC)) {
						$sql5 = "SELECT * from [dbo].[LocationMerchant] where LocationID='".$data4[1]."'";
						$query5 = sqlsrv_query($conn, $sql5);
						$data5 = sqlsrv_fetch_array($query5, SQLSRV_FETCH_NUMERIC);

						$sql6 = "SELECT * from [dbo].[VehicleType] where VehicleID='".$data4[6]."'";
						$query6 = sqlsrv_query($conn, $sql6);
						$data6 = sqlsrv_fetch_array($query6, SQLSRV_FETCH_NUMERIC);

						$arr = array();
						$bulan = '';
						$sql7 = "SELECT Bulan from [dbo].[MemberLocationMonth] where Year='".$year."' and CardNo='".$data4[7]."' and LocationID='".$data4[1]."' and VehicleID='".$data4[6]."'";
						$query7 = sqlsrv_query($conn, $sql7);
						while($data7 = sqlsrv_fetch_array($query7, SQLSRV_FETCH_NUMERIC)){
								array_push($arr, $data7[0]);
						}

						$bulan = implode(", ",$arr);
						?>
						<tr>
							<td><?php echo $data5[2]; ?></td>
							<td><?php echo $data6[1]; ?></td>
							<td><?php echo $bulan; ?></td>
							<td><?php echo $year; ?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
