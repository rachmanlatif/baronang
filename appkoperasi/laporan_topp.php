<?php
if(isset($_GET['download'])) {

}
else {
	require('header.php');
    require('sidebar-right.php');
    require('sidebar-left.php');

    if(isset($_GET['bulan'])) { $bulan = @$_GET['bulan']; } else { $bulan = date('m'); }
	if(isset($_GET['tahun'])) { $tahun = @$_GET['tahun']; } else { $tahun = date('Y'); }
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<h2 class="center uppercase txt-black">
				<?php
					if(isset($_GET['ch'])) {
						if( @$_GET['ch'] == 1 ) { $title = 'EDC'; }
						else if( $_GET['ch'] == 2 ) { $title = 'VA Sinar Mas'; }
						else $title = 'VA BCA';

						echo date('F Y', strtotime($tahun.'-'.$bulan.'-1')).'<br>'.$title;
					}
					else if(isset($_GET['ch']) && isset($_GET['det'])) {
						if( @$_GET['ch'] == 1 ) { $title = 'EDC'; }
						else if( $_GET['ch'] == 2 ) { $title = 'VA Sinar Mas'; }
						else $title = 'VA BCA';

						echo date('F Y', strtotime($tahun.'-'.$bulan.'-1')).'<br>'.$title;
					}
					else {
						echo date('F Y', strtotime($tahun.'-'.$bulan.'-1') );
					}	
				?>
			</h2><br>

			<?php
			if( isset($_GET['ch']) ) { 
				if( !isset($_GET['det']) ) { ?>
					<br><br><div class="right">
						<a href="<?php echo '?det=1&ch='.@$_GET['ch'].'&bulan='.@$_GET['bulan'].'&tahun='.@$_GET['tahun']; ?>" class="btn primary-color waves-effect waves-light">Detail</a>
					</div><br>
					<?php
				}
			}
			else { ?>
				<div class="form-inputs">
					<div class="row">
						<div class="col s4">
							<span>Bulan</span> : <br>
							<select class="browser-default" id="pilbul">
								<?php
								for($x = 1; $x < 13; $x++) { ?>
									<option value="<?php echo date('m', strtotime('2014-'.$x.'-1') ); ?>"><?php echo date('F', strtotime('2015-'.$x.'-1') ); ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<div class="col s4">
							<span>Tahun</span> : <br>
							<select class="browser-default" id="piltah">
								<?php
								for($x = date('Y'); $x > 2015; $x--) { ?>
									<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<div class="col s4">
							<button class="btn btn-large width-100 primary-color waves-light waves-effect m-t-10" id="btnpilok">OK</button>
						</div>
					</div>
				</div>

				<script type="text/javascript">
					$('#btnpilok').click(function(){
						var bulan = $('#pilbul :selected').val();
						var tahun = $('#piltah :selected').val();
						window.location.href = "?bulan=" + bulan + "&tahun=" + tahun
					});
				</script>
				<?php
			}
			?>
			<div class="table-responsive m-t-30">
				<table class="table">
					<thead>
						<tr>
							<?php
							if(isset($_GET['ch'])) { ?>
								<th>Tanggal</th>
								<?php
								if(isset($_GET['det'])) { ?>
									<th>Card No.</th>
									<th>Nama</th>
									<?php
								}
							}
							else { ?>
								<th>Channel</th>
								<?php
							}
							?>
							<th style="text-align: center;">Top Up Point</th>
							<th style="text-align: center;">Fee</th>
							<th style="text-align: center;">Total Point</th>
							<th style="text-align: center;">Total (Rp)</th>
						</tr>
					</thead>

					<tbody>
						<?php
						$sps = "SELECT ValueConversion from [dbo].[ParkingSetting]";
						$qps = sqlsrv_query($conn, $sps);
						$dps = sqlsrv_fetch_array($qps);

						if(isset($_GET['ch'])) {

							if( @$_GET['ch'] == 1 ) { 
								$where1 = "KodeTransactionType='TOPP'"; 
								$where2 = "KodeTransactionType='FEEVA'";
							}
							else if( $_GET['ch'] == 2 ) { 
								$where1 = "UserID='B0080'";
								$where2 = "KodeTransactionType='FEEVA'";

							}
							else {
								$where1 = "UserID='B0008'";
								$where2 = "KodeTransactionType='FEEVA'";
							}

							$lastdate = date('t', strtotime($tahun.'-'.$bulan.'-1'));
							
							if(isset($_GET['det'])) {
								/*
								for($x = 1; $x <= $lastdate; $x++) {
									$ts1 = $tahun.'-'.$bulan.'-'.$x.' 00:00:00';
									$ts2 = $tahun.'-'.$bulan.'-'.$x.' 23:59:59';
									$sch = "SELECT TOP 1 	a.AccNo,
															c.Name,
															(SELECT Sum(Kredit) from [dbo].[ParkingTrans] where $where1 and TimeStam between '$ts1' and '$ts2' and Kredit != 0),
															(SELECT Sum(Kredit) from [dbo].[ParkingTrans] where $where2 and TimeStam between '$ts1' and '$ts2' and Kredit != 0)
											from [dbo].[ParkingTrans] as a 
											inner join [dbo].[MemberCard] as b 
												on a.AccNo = b.CardNo 
											inner join [dbo].[MemberList] as c 
												on b.MemberID = c.MemberID
											where a.".$where." and a.TimeStam between '$ts1' and '$ts2' and a.AccNo IS NOT NULL";
									//echo $sch.'<br><br>';
									$qch = sqlsrv_query($conn, $sch);

									$dch = sqlsrv_fetch_array($qch);
									$ch_tot = $dch[2] - $dch[3];
									?>
									<tr>
										<td><?php echo $x; ?></td>
										<td><?php echo $dch[0]; ?></td>
										<td><?php echo $dch[1]; ?></td>
										<td style="text-align: right;"><?php echo number_format($dch[2], 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($dch[3], 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($ch_tot, 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($ch_tot * $dps[0], 2, ",", "."); ?></td>
									</tr>
									<?php
								}
								*/
								$ts1 = @$_GET['tahun'].'-'.@$_GET['bulan'].'-'.@$_GET['t'].' 00:00:00';
								$ts2 = @$_GET['tahun'].'-'.@$_GET['bulan'].'-'.@$_GET['t'].' 23:59:59';

								$sch = "SELECT 	a.TimeStam, a.AccNo, c.Name,
												(SELECT Sum(Kredit) from [dbo].[ParkingTrans] where $where1 and TimeStam between '$ts1' and '$ts2' and Kredit is not null),
												(SELECT Sum(Kredit) from [dbo].[ParkingTrans] where $where2 and $where1 and TimeStam between '$ts1' and '$ts2' and Kredit is not null)
											from [dbo].[ParkingTrans] as a 
											inner join [dbo].[MemberCard] as b 
												on a.AccNo = b.CardNo 
											inner join [dbo].[MemberList] as c 
												on b.MemberID = c.MemberID
											where a.".$where1." and a.TimeStam between '$ts1' and '$ts2' and a.AccNo IS NOT NULL ORDER BY TimeStam ASC";
								//echo $sch;
								$qch = sqlsrv_query($conn, $sch);

								while( $dch = sqlsrv_fetch_array($qch) ) { 
									$ch_tot = $dch[3] - $dch[4];
									?>
									<tr>
										<td><?php echo $dch[0]->format('Y-m-d'); ?></td>
										<td><?php echo $dch[1]; ?></td>
										<td><?php echo $dch[2]; ?></td>
										<td style="text-align: right;"><?php echo number_format($dch[3], 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($dch[4], 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($ch_tot, 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($ch_tot * $dps[0], 2, ",", "."); ?></td>
									</tr>
									<?php
								}
							}
							else {
								for($x = 1; $x <= $lastdate; $x++) {
									$ts1 = $tahun.'-'.$bulan.'-'.$x.' 00:00:00';
									$ts2 = $tahun.'-'.$bulan.'-'.$x.' 23:59:59';
									$sch = "SELECT DISTINCT (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where $where1 and TimeStam between '$ts1' and '$ts2' and Kredit is not null),
															(SELECT Sum(Kredit) from [dbo].[ParkingTrans] where $where2 and TimeStam between '$ts1' and '$ts2' and Kredit is not null)
											from [dbo].[ParkingTrans] where TimeStam between '$ts1' and '$ts2'";
									$qch = sqlsrv_query($conn, $sch);
									$dch = sqlsrv_fetch_array($qch);
									$ch_tot = $dch[0] - $dch[1];

									if( $dch[0] != '' ) { ?>
										<tr>
											<td><a href="<?php echo '?ch='.@$_GET['ch'].'&det=1&tahun='.@$_GET['tahun'].'&bulan='.@$_GET['bulan'].'&t='.$x; ?>"><?php echo @$_GET['tahun'].'-'.@$_GET['bulan'].'-'.$x; ?></a></td>
											<td style="text-align: right;"><?php echo number_format($dch[0], 2, ",", "."); ?></td>
											<td style="text-align: right;"><?php echo number_format($dch[1], 2, ",", "."); ?></td>
											<td style="text-align: right;"><?php echo number_format($ch_tot, 2, ",", "."); ?></td>
											<td style="text-align: right;"><?php echo number_format($ch_tot * $dps[0], 2, ",", "."); ?></td>
										</tr>
										<?php
									}
									
								}
							}

						}
						else {
							// TOPP
							$sch1 = "SELECT DISTINCT	(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='TOPP' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where UserID='B0080' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where UserID='B0008' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='FEEVA'and KodeTransactionType='TOPP' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='FEEVA' and UserID='B0080' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='FEEVA' and UserID='B0008' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0)
										from [dbo].[ParkingTrans]";
							$qch1 = sqlsrv_query($conn, $sch1);
							$dch1 = sqlsrv_fetch_array($qch1);
							$ch1_tot = $dch1[0] - $dch1[3];
							$ch2_tot = $dch1[1] - $dch1[4];
							$ch3_tot = $dch1[2] - $dch1[5];
							?>
							<tr>
								<td><a href="?ch=1<?php echo '&bulan='.$bulan.'&tahun='.$tahun; ?>">EDC</a></td>
								<td style="text-align: right;"><?php echo number_format($dch1[0], 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[3], 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch1_tot, 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch1_tot * $dps[0], 2, ",", "."); ?></td>
							</tr>

							<tr>
								<td><a href="?ch=2<?php echo '&bulan='.$bulan.'&tahun='.$tahun; ?>">Sinar Mas</a></td>
								<td style="text-align: right;"><?php echo number_format($dch1[1], 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[4], 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch2_tot, 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch2_tot * $dps[0], 2, ",", "."); ?></td>
							</tr>

							<tr>
								<td><a href="?ch=3<?php echo '&bulan='.$bulan.'&tahun='.$tahun; ?>">VA BCA</a></td>
								<td style="text-align: right;"><?php echo number_format($dch1[2], 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[5], 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch3_tot, 2, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch3_tot * $dps[0], 2, ",", "."); ?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

		</div>
	</div>

<?php
	require('footer.php'); 
}
?>