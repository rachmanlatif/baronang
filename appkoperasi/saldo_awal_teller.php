<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$utgl   = "";
$usr1   = "";
$amn1   = "";
$href = "";

if(!empty($uledit)){
    $a   = "select * from dbo.CashierAssign where IDCashierAssign='$uledit'";
    $b  = sqlsrv_query($conn, $a);
    $c   = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if(count($c[0]) > 0){
        $utgl = $c[4]->format('Y-m-d');
        $usr1 = $c[0];
        $amn1 = $c[3];
        $href = "?edit=".$usr1;
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='saldo_awal_teller.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='saldo_awal_teller.php?page=".$_GET['page']."';</script>";
        }
    }
}
?>
<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Saldo Awal Teller'); ?></h2>
            <?php if(isset($_SESSION['error-type']) and isset($_SESSION['error-message']) and isset($_SESSION['error-time'])){ ?>
                  <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                      <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                          <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                          <?php echo $_SESSION['error-message']; ?>
                      </div>
                  <?php } ?>
              <?php } ?>

            <form class="form-horizontal" action="proc_saldo_awal_teller.php<?=$href?>" method = "POST">
                        <div class="input-field">
                            <input type="text" name="date" id="date" class="datepicker" value="<?php echo $utgl; ?>" autocomplete="off"></p></td>
                            <label><?php echo ('Assign Date'); ?></label>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="tusr">
                                <thead>
                                <tr>
                                    <th><?php echo lang('User ID'); ?></th>
                                    <th><?php echo lang('Amount'); ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                    <select class="browser-default" id="user">
                                        <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                                        <?php
                                        $julsql   = "select * from [dbo].[LoginList] where Status = 1";
                                        $julstmt = sqlsrv_query($conn, $julsql);
                                        while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                            <option value="<?php echo $rjulrow[0] ?>"><?php echo $rjulrow[1].' - '.$rjulrow[3]; ?></option>
                                        <?php } ?>
                                    </select>
                                    </td>
                                    <td><input type="text" id="amount" class="validate price" value="0"></td>
                                    <td><button type="button" class="btn btn-sm btn-success btn-addu"><?php echo lang('Tambah'); ?></button></td>
                                </tr>
                                </tbody>
                                <tfoot id="detailUser">
                                    <?php if (!empty($uledit)) { ?>
                                    <?php
                                    $no = 0;
                                    $julsql   = "select * from [dbo].[CashierAssignUser] where IDCashierAssign='$usr1'";
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){ ?>
                                    <tr>
                                        <td>
                                          <?php
                                          $userName = '';
                                          $sql = "select * from [dbo].[LoginList] where UserID='$rjulrow[2]'";
                                          $stmt = sqlsrv_query($conn, $sql);
                                          $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC);
                                          if($row != null){
                                              $userName = $row[1];
                                          }

                                          echo $userName;
                                          ?>
                                          <input type="hidden" name="idcashieruser[<?php echo $no; ?>]" class="validate" value="<?php echo $rjulrow[4]; ?>">
                                          <input type="hidden" name="user[<?php echo $no; ?>]" class="validate" value="<?php echo $rjulrow[2]; ?>">
                                        </td>
                                        <td><input type="text" name="amount[<?php echo $no; ?>]" class="validate" value="<?php echo round($rjulrow[1]); ?>"></td>
                                        <td>
                                          <a href="proc_saldo_awal_teller.php?delete=<?php echo $rjulrow[4]?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="ion-android-delete"></i></a>
                                        </td>
                                    </tr>
                                  <?php $no++; }
                                } ?>
                                </tfoot>
                            </table>
                        </div>

                        <div style="margin-top: 30px;">
                                <?php if(count($c[0]) > 0){ ?>
                                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                                <?php } else { ?>
                                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                                <?php } ?>
                        </div>

                        <div style="margin-top: 10px; margin-bottom: 30px;">
                            <?php if(count($c[0]) > 0){ ?>
                                <a href="saldo_awal_teller.php" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Batal'); ?></a>
                            <?php } ?>
                        </div>
            </form>

        <div class="box box-primary">
            <div class="box-body" style="margin-top: 30px;">
            <form action="" method="get">
                <div class="input-field">
                    <input type="text" name="cari" id="cari" class="datepicker" value="<?php echo "$_GET[cari]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Cari Berdasarkan Tanggal'); ?></label>
                </div>
                <button type="submit"  value = "cari" class="waves-effect waves-light btn-large primary-color width-100">Cari</button>
            </form>

            <?php
            if(isset($_GET['cari'])){
            $cari2 = $_GET['cari'];
            $cari3 = date_create($cari2);
            $cari = date_format($cari3, 'Y-m-d');

            echo "<b>Hasil pencarian : ".$cari."</b>";
            }
            ?>

            <h3 class="box-title" style="margin-top: 30px;"><?php echo lang('Daftar Teller'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><?php echo lang('Kode'); ?></th>
                            <th><?php echo lang('UserID'); ?></th>
                            <th><?php echo lang('Amount'); ?></th>
                            <th><?php echo lang('Tanggal Dibuat'); ?></th>
                            <th><?php echo lang('Assign date'); ?></th>
                            <th><?php echo lang('Aksi'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //count
                         if(isset($_GET['cari'])){
                            $cari2 = $_GET['cari'];
                            $cari3 = date_create($cari2);
                            $cari = date_format($cari3, 'Y-m-d');
                            $jmlulsql = "SELECT count (*) from (SELECT a.IDCashierAssign, b.UserID, b.Amount, a.TimeStamp, a.AssignDate, a.Type, b.IDCashierAssignUser, ROW_NUMBER() OVER (ORDER BY AssignDate Desc) as row FROM dbo.CashierAssign  a  inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where Type = '0' and AssignDate like '".$cari."' ) a";
                            } else {
                        $jmlulsql   = "SELECT count (*) from (SELECT a.IDCashierAssign, b.UserID, b.Amount, a.TimeStamp, a.AssignDate, a.Type, b.IDCashierAssignUser, ROW_NUMBER() OVER (ORDER BY AssignDate Desc) as row FROM dbo.CashierAssign  a  inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where Type = '0') a ";
                         }
                        //echo $jmlulsql;
                        $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        }
                        else{
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                        $dd = "SELECT * from (SELECT a.IDCashierAssign, b.UserID, b.Amount, a.TimeStamp, a.AssignDate, a.Type, b.IDCashierAssignUser, ROW_NUMBER() OVER (ORDER BY AssignDate Desc) as row FROM dbo.CashierAssign  a  inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where Type = '0') a where row between '$Posisi' and '$batas'";
                        // $dd = "SELECT * from (SELECT a.IDCashierAssign, b.UserID,  b.Amount, a.TimeStamp, a.AssignDate, a.Type, b.IDCashierAssignUser, ROW_NUMBER() OVER (ORDER BY AssignDate Desc) as row FROM dbo.CashierAssign a inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign) a where Type = 0 and row between '$posisi' and '$batas'";
                        if(isset($_GET['cari'])){
                            $cari2 = $_GET['cari'];
                            $cari3 = date_create($cari2);
                            $cari = date_format($cari3, 'Y-m-d');
                            $dd = "SELECT * from (SELECT a.IDCashierAssign, b.UserID, b.Amount, a.TimeStamp, a.AssignDate, a.Type, b.IDCashierAssignUser, ROW_NUMBER() OVER (ORDER BY AssignDate Desc) as row FROM dbo.CashierAssign  a  inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where Type = '0' and AssignDate like '".$cari."' ) a where row between '$Posisi' and '$batas'";
                            //echo $dd;
                            //$dd = "SELECT * from (SELECT a.IDCashierAssign, b.UserID,  b.Amount, a.TimeStamp, a.AssignDate, a.Type, b.IDCashierAssignUser, ROW_NUMBER() OVER (ORDER BY AssignDate Desc) as row FROM dbo.CashierAssign a inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign) a where Type = 0 and AssignDate like '%".$cari."%' ";
                        }
                            $ff = sqlsrv_query($conn, $dd);
                            //echo $dd;
                            while($gg = sqlsrv_fetch_array($ff, SQLSRV_FETCH_NUMERIC)){
                                $tanggal = $gg[3]->format('Y-m-d H:i:s');
                                $tanggal1 = $gg[4]->format('Y-m-d');
                                ?>
                                <tr>
                                    <td><?php echo $gg[0];?></td>
                                    <td><?php echo $gg[1];?></td>
                                    <td style="text-align: right"><?php echo number_format($gg[2]);?></td>
                                    <td><?php echo $tanggal;?></td>
                                    <td><?php echo $tanggal1;?></td>
                                    <td width="20%" style="padding: 3px">
                                        <div class="btn-group" style="padding-right: 15px">
                                            <a href="saldo_awal_teller.php?edit=<?=$gg[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="proc_saldo_awal_teller.php?delete=<?=$gg[6]?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="ion-android-delete"></i></a>
                                        </div>
                                    </td>
                                </tr>

                            <?php }
                            $jmlhalaman = ceil($jmlulrow[0]/$perpages); ?>
                        </tbody>
                    </table>
                </div>

                <div class="box-footer clearfix right">
                    <div style="text-align: center;">
                        <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>
                        <?php
                        $reload = "saldo_awal_teller.php?";
                        $page = intval($_GET["page"]);
                        $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                        if( $page == 0 ) $page = 1;

                        $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                        echo "<div>".paginate($reload, $page, $tpages)."</div>";
                        ?>
                    </div>
                </div>
        </div>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>


<script type="text/javascript">
    var tampung = [];

     $('.btn-addu').click(function(){
       var index = $('#detailUser tr').length;

        var user = $('#user option:selected').val();
        var amount = $('#amount').val();
        var date = $('#date').val();

        if(date == ''){
          alert('Assign date tidak boleh kosong');
          return false;
        }
        else if(user == '' || amount == '' || amount <= 0){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(user, tampung) !== -1){
            alert('Tidak dapat memilih user yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addteller.php",
                type : 'POST',
                dataType : 'json',
                data: { index: index, user: user, amount: amount, date: date},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.id);
                        $("#detailUser").append("<tr>" +
                            "<td><input type='hidden' name='user["+data.index+"]' value="+ data.id +">" + data.nama + "</td>" +
                            "<td><input type='hidden' name='amount["+data.index+"]' value="+ data.amount +">" + data.amount +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='ion-android-delete'></i></a></td>" +
                            "</tr>");

                            $('#user').val('');
                            $('#amount').val('');
                    }
                    else{
                        alert(data.message);
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });
</script>

<?php require('footer.php'); ?>
