<?php require('header.php');?>      
<?php require('sidebar-left-user.php');?>

<section class="content">

<div class="row">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content-header">
      <h1>
        Kartu Saya
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-5 connectedSortable">
          <div class="box box-info">
          <!-- Profile Image -->
            <div class="box-body box-profile">
			  
              <img class="img-responsive" src="../site/static/dist/img/card1.jpg" alt="Photo">
			  </a>
              <h3 class="profile-username text-center">Koperasi Baronang</h3>

              <p class="text-muted text-left">Informasi Akun</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Saldo</b> <a class="pull-right">143,504,544</a>
                </li>
                <li class="list-group-item">
                  <b>Loan</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Piutang</b> <a class="pull-right">13,287</a>
                </li>
				<li class="list-group-item">
                  <b>Tanggal Join</b> <a class="pull-right">2 Agustus 2017</a>
                </li>
              </ul>
            </div>
			<div class="box-body box-profile">
              <p class="text-muted text-left">Ringkasan Akun</h3>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Nomor Rekening</b> <a class="pull-right">127 124 1726</a>
                </li>
                <li class="list-group-item">
                  <b>Nomor Deposito</b> <a class="pull-right">123 462 7264</a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          <!-- /.box -->
        </div>
        </section>
		</div>
		</div>
		</section>

<?php require('content-footer.php');?>
<?php require('footer.php');?>