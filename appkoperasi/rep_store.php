<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Transaksi Toko</h3>
    </div>
    <div class="box-body">
        <?php if(!isset($_GET['sn'])){ ?>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Serial Number</th>
                        <th>KBA</th>
                        <th></th>
                    </tr>
                    <?php
                    $xx = "select* from [Gateway].[dbo].[EDCList] where KID = '$_SESSION[KID]'";
                    $yy = sqlsrv_query($conn, $xx);
                    while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><?php echo $zz[0]; ?></td>
                            <td>
                                <?php
                                $s = "select* from [dbo].[BankAccount] where KodeKoperasiBankAccount = '$zz[2]'";
                                $e = sqlsrv_query($conn, $s);
                                $r = sqlsrv_fetch_array($e, SQLSRV_FETCH_NUMERIC);
                                if($r != null){
                                    echo $r[3].' - '.$r[4];
                                }
                                ?>
                            </td>
                            <td><a href="?sn=<?php echo $zz[0]; ?>"><button type="button" class="btn btn-xs btn-primary">Lihat transaksi</button></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php } else { ?>
            <p>
                <a href="rep_store.php"><button type="button" class="btn btn-xs btn-primary">Kembali</button></a>
            </p>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Transaction Code</th>
                        <th>Transaction Date</th>
                        <th>Account Code</th>
                        <th>Debit</th>
                        <th>Credit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from Gateway.dbo.TransaksiPay where SN = '".$_GET['sn']."'";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeTrx,Tanggal,Debet desc) as row FROM [Gateway].[dbo].[TransaksiPay] where SN ='".$_GET['sn']."') a WHERE row between '$posisi' and '$batas'";
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><?php echo $ulrow[10]; ?></td>
                            <td><?php echo $ulrow[0]; ?></td>
                            <td><?php echo $ulrow[1]->format('Y-m-d H:i:s'); ?></td>
                            <td><?php echo $ulrow[2]; ?></td>
                            <td><?php echo number_format($ulrow[3]); ?></td>
                            <td><?php echo number_format($ulrow[4]); ?></td>
                        </tr>
                    <?php
                    }
                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>
            <div>
                <div class="box-footer clearfix">
                    <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
                    <?=$posisi." - ".$batas?>
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li class="paginate_button"><a href="rep_store.php">&laquo;</a></li>
                        <?php
                        for($ul = 1; $ul <= $jmlhalaman; $ul++){
                            if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                            echo '<li class="paginate_button '.$ulpageactive.'"><a href="rep_store.php?sn='.$_GET['sn'].'&page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                        }
                        ?>
                        <li class="paginate_button"><a href="rep_store.php?sn=<?php echo $_GET['sn'] ?>&page=<?=$jmlhalaman?>">&raquo;</a></li>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
