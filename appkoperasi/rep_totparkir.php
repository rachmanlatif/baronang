<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
     <div class="page-content">
         <div class="box-body">
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                <h2 class="uppercase"><?php echo lang('Detail Transaksi'); ?></h2> <br>

                <a href="rep_totpenerimaan.php?tgl1=<?php echo $_GET['tgl1'];?>&tgl2=<?php echo $_GET['tgl2'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 10px; margin-bottom: 20px;">Back</button></a>

                <a href="drep_totparkir.php?lok=<?php echo $_GET['lok'];?>&tgl1=<?php echo $_GET['tgl1'];?>&tgl2=<?php echo $_GET['tgl2'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 10px; margin-bottom: 20px;">Download To Excel</button></a>

                <table class="table table-bordered table-striped">
                    <tbody>
                        <?php 
                       
                        if($_GET['lok']){ ?>
                        <div class="table-responsive">
                            <?php
                            $lok = $_GET['lok'];
                            //echo $lok;
                            $disable = "readonly";
                            $tgl1 = $_GET['tgl1'];
                            $tgl2 = $_GET['tgl2'];
                            $tglh = date('Y/m/d', strtotime('+1 days', strtotime($tgl2)));
                            $xxx = "SELECT * FROM [dbo].[LocationMerchant] where LocationID='$lok'";
                            //echo $xxx;
                            $yyy = sqlsrv_query($conn,$xxx );
                            while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                 $asum = "SELECT sum(amount) FROM [dbo].[Translist] where AccountKredit = '$_GET[lok]' and TransactionType between 'MBRS' and 'PARK' and Date between '$tgl1' and '$tglh'";
                                 $prosum = sqlsrv_query($conn,$asum);
                                 $hassum = sqlsrv_fetch_array($prosum, SQLSRV_FETCH_NUMERIC);
                                 $sumn = $hassum[0] / 1000;

                            ?>

                            <tr>
                                <h5 class="box-title" align="left"><?php echo ('Data Parkir'); ?></h3>
                            </tr>

                            <div class="active">
                                    <label ><?php echo lang('Lokasi'); ?></label>
                                    <input type="text" name="lokasi" class="validate" id="" value="<?=$zzz[0]?> - <?=$zzz[2]?>" <?php echo $disable; ?>>   
                            </div>

                            <div class="active">
                                    <label ><?php echo lang('Total Point'); ?></label>
                                    <input type="text" name="point" class="validate" id="" value="<?= number_format($sumn)?> Point"<?php echo $disable; ?>>   
                            </div>
                            <?php } ?>

                            
                            <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th><?php echo lang('No Kartu'); ?></th>
                                    <th><?php echo lang('Type Kartu'); ?></th>
                                    <th><?php echo lang('Point'); ?></th>
                                </tr>
                            </thead>
                
                            <?php
                            //count
                            if ($_GET['lok']) {
                                
                                //echo $tglh;
                                $jmlulsql   = "select count(*) from dbo.TransList where AccountKredit='$_GET[lok]' and TransactionType between 'MBRS' and 'PARK' and date between '$tgl1' and '$tglh'";
                                //echo $jmlulsql;
                            }
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }


                            if ($_GET['lok']) {
                                // $aaa = "SELECT [TransactionNumber],[AccountNumber],[TimeStamp],[KodeTransactionType],[Description],[Message],[Debit],[Kredit],[UserID] FROM [dbo].[TimeDepositTrans]where AccountNumber='$_GET[acc]' order by TimeStamp asc ";

                                $aaa = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] where AccountKredit = '$_GET[lok]' and TransactionType between 'MBRS' and 'PARK' and Date between '$tgl1' and '$tglh') a  where row between '$posisi' and '$batas'";
                                //echo $aaa;
                            }
                            $bbb = sqlsrv_query($conn,$aaa );
                            //$total= 0;
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                                $point = $ccc[5] / 1000;
                            //echo $ccc[7];
                            
                            ?>
                                <tr>
                                    <td><?php echo $ccc[15]; ?></td>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[6]->format('Y-m-d H:i:s'); ?></td> 
                                    <td><?php echo $ccc[1]; ?></td>
                                    <td><?php echo $ccc[7]; ?></td>
                                    <td><?php echo number_format($point); ?></td>
                                </tr>
                                <?php
                                $jmlpage++; $iii0++; $iii2++; $iii5++; $iii6++; $iii7++;
                                }
                               $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                                ?>
                            <?php $nb++;} ?>
                            </table>
                        </div>
                    </tbody>

                </table>
                
            </div>
            <div class="box-footer clearfix right">
                        <div style="text-align: center;">
                            Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                            <?php
                            $lok = $_GET['lok'];
                            $tgl1 = $_GET['tgl1'];
                            
                            $tgl2 = $_GET['tgl2'];
                            $reload = "rep_totparkir.php?lok=$lok&tgl1=$tgl1&tgl2=$tgl2";
                            $page = intval($_GET["page"]);
                            $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                            if( $page == 0 ) $page = 1;
                                $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                ?>
                        </div>
                </div>
            
            </div>

        </div>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
 require('content-footer.php');?>

<?php require('footer.php');?>
