<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">
        <div class="box-body">
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                <h2 class="uppercase"><?php echo lang('Detail Transaksi Sesi Kasir'); ?></h2> <br>

                <a href="rep_session.php?page=<?php echo $_GET['pagem'];?>&tgl1=<?php echo $_GET['tgl1'];?>&tgl2=<?php echo $_GET['tgl2'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 10px; margin-bottom: 20px;">Back</button></a>

                <a href="drep_tsession.php?acc=<?php echo $_GET['lok'];?>&from=<?php echo $_GET['tgl1'];?>&to=<?php echo $_GET['tgl2'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 10px; margin-bottom: 20px;">Download To Excel</button></a>
                <?php
                $lok = $_GET['lok'];
                $tgl1 = $_GET['tgl1'];
                $tgl2 = $_GET['tgl2'];

                ?>
                <div class="box-header" align="center" style="margin-top: 30px;">
                    <tr>
                        <h3 class="" align="center"><?php echo ('Detail Transaksi Sesi Kasir'); ?></h3>
                    </tr>
                </div>

                <div class="box-header " align="left">
                    <tr>
                        <h5 class="box-title" align="left"><?php echo ('Akun : '); ?> <?php echo $lok; ?></h5  >
                    </tr>
                </div>

                <?php
                $disable = "readonly";
                $tgla= date('1990/01/01 00:00:00');
                $tglh = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($tgl1)));
                $tglt = date('Y/m/d h:i:s', strtotime('-1 days', strtotime($tgl1)));
                $tglk = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($tgl2)));
                $awal = 0;
                $snilaid = "select * from dbo.cashiersession where transno = '$lok'";
                //echo $snilaid;
                $pnilaid = sqlsrv_query($conn, $snilaid);
                while ($hnilaid = sqlsrv_fetch_array( $pnilaid, SQLSRV_FETCH_NUMERIC)){
                    
                $awal = $hnilaid[3];
                }
                $sdebit = "select sum(amount) from dbo.translist where AccountDebet = '$lok' and Date between '$tglt' and '$tglk'";
                //echo $sdebit;
                $pdebit = sqlsrv_query($conn, $sdebit);
                while ($hdebit = sqlsrv_fetch_array( $pdebit, SQLSRV_FETCH_NUMERIC)){
                    if ($hdebit != 0){
                        $hsdebit = $hdebit[0];    
                    } else {
                        $hsdebit = 0;    
                    }
                }

                $skredit = "select sum(amount) from dbo.translist where AccountKredit = '$lok' and Date between '$tglt' and '$tglk'";
                //echo $skredit;
                $pkredit = sqlsrv_query($conn, $skredit);
                while ($hkredit = sqlsrv_fetch_array( $pkredit, SQLSRV_FETCH_NUMERIC)){
                    if ($hkredit != 0){
                        $hskredit = $hkredit[0];
                    } else {
                        $hskredit = 0;    
                    }
                }


                ?>
                
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No Transaksi</th>
                            <th>Tanggal Transaksi</th>
                            <th><?php echo lang('Keterangan'); ?></th>
                            <th><?php echo lang('Debit'); ?></th>
                            <th><?php echo lang('Kredit'); ?></th>
                            <th><?php echo lang('Total'); ?></th>
                        </tr>
                    </thead>


                        <tr>
                            <th colspan="6"><?php echo lang('Saldo Awal'); ?></th>
                            <th style="text-align: right;"><span><?php echo number_format($awal,2); ?></span></th>
                        </tr>
                
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from (select *, ROW_NUMBER () over (Order by date asc) as row from dbo.translist where accountdebet = '$lok' or accountkredit = '$lok' and date between '$tglt' and '$tglk') a ";
                    //echo $jmlulsql;
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        } else {                                                
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                           

                    $aaa = "select * from (select *, ROW_NUMBER () over (Order by date asc) as row from dbo.translist where accountdebet = '$lok' and date between '$tglt' and '$tglk' or accountkredit = '$lok' and date between '$tglt' and '$tglk') a ";
                    //echo $aaa;
                    $bbb = sqlsrv_query($conn,$aaa );
                    $total= $awal;
                    $ctotal = $total;
                    //echo $total;
                    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                        if ($ccc != null){
                            if($ccc[1] == $lok ){
                                $total+=$ccc[5];
                                $ctotal=$total;               
                            } else {
                                $total-=$ccc[5];
                                $ctotal=$total;                
                            }
                        } else {
                            $ctotal = $total;   
                        }

                    ?>
                        <tr>
                            <td><?php echo $ccc[15]; ?></td>
                            <td><a href="rep_tsessionlap.php?pagem=<?php echo $_GET['page'];?>&lok=<?php echo $_GET['lok']; ?>&tgl1=<?php echo $_GET['tgl1']; ?>&tgl2=<?php echo $_GET['tgl2']; ?>&trans=<?php echo $ccc[0]; ?>"><?php echo $ccc[0]; ?></a></td>
                            <td><?php echo $ccc[6]->format('Y-m-d H:i:s'); ?></td>
                            <td><?php echo $ccc[9]; ?></td>
                            <td style="text-align: right;"><?php 
                                if ($lok == $ccc[1]){
                                    $ndebit = $ccc[5];   
                                } else {
                                    $ndebit = 0;                                    
                                } 
                                echo number_format($ndebit,2); ?></td>
                            <td style="text-align: right;"><?php 
                                if ($lok == $ccc[2]){
                                    $nkredit = $ccc[5];   
                                } else {
                                    $nkredit = 0;        
                                }
                                echo number_format($nkredit,2) ; ?></td>
                            <td style="text-align: right;"><?php echo number_format($ctotal,2); ?></td>
                        </tr>

                                
                    <?php
                    $jmlpage++; $iii0++; $iii2++; $iii5++; $iii6++; $iii7++; }
                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                        <tr>
                            <th colspan="4"><?php echo lang('Total Saldo Akhir'); ?></th>
                            <th style="text-align: right;"><?php echo number_format($hsdebit,2); ?></th>
                            <th style="text-align: right;"><?php echo number_format($hskredit,2); ?></th>
                            <th style="text-align: right;"><span><?php echo number_format($ctotal,2); ?></span></th>
                        </tr>
                </table>
            </div>
            <!-- <div class="box-footer clearfix right">
                        <div style="text-align: center;">
                            Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                            <?php
                            $lok = $_GET['lok'];
                            $tgl1 = $_GET['tgl1'];
                            $pagem = $_GET['pagem'];
                            $tgl2 = $_GET['tgl2'];
                            $reload = "rep_tedc.php?pagem=$pagem2&lok=$lok&tgl1=$tgl1&tgl2=$tgl2";
                            $page = intval($_GET["page"]);
                            $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                            if( $page == 0 ) $page = 1;
                                $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                ?>
                        </div>
                </div> -->
            
        </div>
    </div>
</div>



<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
 require('content-footer.php');?>

<?php require('footer.php');?>
                        