<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");

    $objPHPExcel->getProperties()->setTitle("Drep Neraca");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul

    $objWorkSheet->getStyle('B1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B1', 'Laporan Neraca Berdasarkan Rekening Akun');
    
    $objWorkSheet->getStyle('B2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B2', 'Nama Akun  : ');

    $objWorkSheet->getStyle('B3')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B3')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B3', 'Periode  : ');

    

   
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No');
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'No Akun');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'Nama Akun');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Nilai');



    if($_GET['acc']){
    $no = 1;
    $bln = 3 ;
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

        $objWorkSheet->getStyle('C'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("C".$bln,$bulan .'-'. $tahun);     

    $nama = 2;
    $x = "select * from dbo.AccountView where KodeAccount = '$_GET[acc]'";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){

        $objWorkSheet->getStyle('C'.$nama)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("C".$nama,$z[1]);   
    }          


    $row = 6;
    $from=$_GET['from'];
    $to=$_GET['to'];
    $a = "exec dbo.TampilDetailReportCOA '0','$_GET[acc]','$_GET[from]','$_GET[to]'";
    //echo $a;
    $tot=0;
    $b = sqlsrv_query($conn, $a);
    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){  
        
        $aa = "exec dbo.TampilDetailReportCOA '1','$c[0]','$_GET[from]','$_GET[to]'";
        //echo $aa;
        $bb = sqlsrv_query($conn, $aa);
        $total=0;

        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
        if($cc[2] > 0){
        $total+=$cc[2];
        $tot+=$cc[2];
        } else {
        $total+=$cc[2];
        $tot+=$cc[2];
        }

            $objWorkSheet->SetCellValue("A".$row,$no);
            $objWorkSheet->SetCellValueExplicit("B".$row, $cc[0]);
            $objWorkSheet->SetCellValue("C".$row, $cc[1]);
            $objWorkSheet->getStyle('D' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("D".$row, number_format($cc[2]), PHPExcel_Cell_DataType::TYPE_STRING);
    
            $no++;
            $row++;
        }
            $objWorkSheet->getStyle('A'.$row,'Saldo Akhir')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue('A'.$row,'Saldo Akhir');
            $objWorkSheet->getStyle('D'.$row, number_format($total))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("D".$row, number_format($total), PHPExcel_Cell_DataType::TYPE_STRING);
            
            $no++;
            $row++;
    }
            $objWorkSheet->getStyle('A5:D' .$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);  
            $objWorkSheet->getStyle('A'.$row,'Total Saldo Akhir Semua Akun')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue('A'.$row,'Total Saldo Akhir Semua Akun');
            $objWorkSheet->getStyle('D'.$row, number_format($tot))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("D".$row, number_format($tot), PHPExcel_Cell_DataType::TYPE_STRING); 

}
//exit;
    $objWorkSheet->setTitle('Drep Neraca');

    $fileName = 'Neraca'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
