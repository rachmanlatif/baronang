<?php
include "connect.php";

$json = array(
    'status'=>0,
);

if(isset($_POST['index']) and isset($_POST['id']) and isset($_POST['name']) and isset($_POST['ket']) and isset($_POST['type'])){
    $index = $_POST['index'];
    $id = $_POST['id'];
    $name = $_POST['name'];
    $ket = $_POST['ket'];
    $type = $_POST['type'];
    if($type == 'type'){
        $a = "select TOP 1 * from dbo.TypeAcc where KodeGroup='$id' order by KodeTipe desc";
        $b = sqlsrv_query($conn, $a);
        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
        if($c == null){
            $urut = 1;
            $code = $index.'.'.$urut.'.00.00.000';
        }
        else{
            $grepcode = substr($c[0],2,1);
            $urut = $grepcode+1;
            $code = $index.'.'.$urut.'.00.00.000';
        }

        $sql = "exec [dbo].[ProsesTypeAcc] '$code','$name','$id','$ket'";
        $query = sqlsrv_query($conn, $sql);
        if($query){
            $json['status'] = 1;
        }
    }

    if($type == 'detail'){
        $a = "select TOP 1 * from dbo.Account where KodeTipe='$id' and Header='$index' order by KodeAccount desc";
        $b = sqlsrv_query($conn, $a);
        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
        if($c == null){
            $add = 1;
            if($index == 0){
                $awal = substr($id,0,3);
                $urut = '0'.$add;
                $code = $awal.'.'.$urut.'.00.000';
            }

            if($index == 1){
                $awal = substr($id,0,6);
                $urut = '0'.$add;
                $code = $awal.'.'.$urut.'.000';
            }

            if($index == 2){
                $awal = substr($id,0,9);
                $urut = '00'.$add;
                $code = $awal.'.'.$urut;
            }
        }
        else{
            if($index == 0){
                $awal = substr($c[0],0,3);
                $grepcode = substr($c[0],4,2);
                $add = $grepcode+1;
                if($add < 10){
                    $urut = '0'.$add;
                }
                else{
                    $urut = $add;
                }
                $code = $awal.'.'.$urut.'.00.000';
            }

            if($index == 1){
                $awal = substr($c[0],0,6);
                $grepcode = substr($c[0],7,2);
                $add = $grepcode+1;
                if($add < 10){
                    $urut = '0'.$add;
                }
                else{
                    $urut = $add;
                }
                $code = $awal.'.'.$urut.'.000';
            }

            if($index == 2){
                $awal = substr($c[0],0,9);
                $grepcode = substr($c[0],10,3);
                $add = $grepcode+1;
                if($add < 10){
                    $urut = '00'.$add;
                }
                else if($add < 100){
                    $urut = '0'.$add;
                }
                else{
                    $urut = $add;
                }
                $code = $awal.'.'.$urut;
            }

        }

        //$sql = "exec [dbo].[ProsesAccManual] '$code','$name','$id',$index,'$ket',0";
        $sql = "exec [dbo].[ProsesAccount] '$code','$name','$id','$index','$ket',0,'$code','$name'";
        $query = sqlsrv_query($conn, $sql);
        if($query){
            $json['status'] = 1;
        }
        //$json['status'] = $sql;
    }
}

echo json_encode($json);

?>