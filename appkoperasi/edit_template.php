<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>



<div class="animated fadeinup delay-1">

<style>
    .tengah{
        text-align:center;
        }
    .kiri{
        text-align:left;
        }
    .kanan{
        text-align:right;
        }

    .tab4 { 
       margin-left: 40px; 
}

</style>


    <div class="page-content">
        <h3 class="box-title"><?php echo lang('Perubahan Template'); ?></h3>
        <?php 
        $id = @$_GET['id'];
        function getIsActive($id, $x, $y, $c) {
            $sql1 = "SELECT Top 1 Status from [dbo].[TemplateDetail_2] where IDTemplate='$id' and Account='$x' and State='$y'";
            $query1 = sqlsrv_query($c, $sql1);
            $data1 = sqlsrv_fetch_array($query1);

            return $data1[0];
        }

        //echo $id;
        $query = "select * from dbo.Template WHERE IDTemplate='$id'";
        $y = sqlsrv_query($conn, $query);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC) ){
            $qu = "select * from dbo.TemplateDetail_2 WHERE IDTemplate='$id'";
            $yz = sqlsrv_query($conn, $qu);
            $zd = sqlsrv_fetch_array($yz, SQLSRV_FETCH_NUMERIC);
        ?>

        <div class="form-inputs">
            <!-- ST=5 -->
            <form action="proc_edittemplate.php?st=<?php echo @$_GET['st']; ?>&set=<?php echo @$_GET['set']; ?>" target="_blank" method="POST">
                <input type="hidden" name="state" id="state" value="<?php echo @$_GET['st']; ?>">
                <input type="hidden" name="idtemp" id="idtemp" value="<?php echo $id; ?>">

                <h5 class="box-title"><?php echo lang('Template'); ?></h5>
                <div class="input-field">
                    <input type="text" name="nama" class="nama" id="nama" value="<?php echo $z[1] ?>">
                    <label for=""><?php echo lang('Nama'); ?></label>
                </div>
                <div class="input-field">
                        <select id="lap" name="lap" class="browser-default">
                            <option type="text" name="lap" id="lap" value="<?php echo $_GET['set']; ?>">- <?php echo lang('Pilih Laporan'); ?> -</option>
                            <?php
                            $bln=array(00=>"Neraca","Laba-Rugi");
                            //echo $bln;
                            $from='';
                            for ($bulan=00; $bulan<=1; $bulan++) { 
                                 if ($_GET['set'] == $bulan){
                                    $ck="selected";
                                 }   else {
                                    $ck="";
                                 }
                                echo "<option value='$bulan' $ck>$bln[$bulan]</option>";
                            }
                            ?>
                        </select>
                </div>
        
                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                </div>
                <div style="margin-top: 30px;">
                    <a href="list_template.php?&st=5&set=00" target="_blank"><button type="button" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('List Edit Template'); ?></button></a>
                </div>
        </div>

        
        <?php if($_GET['st'] == 5 ){ ?>
            <table class="table table-bordered">
                <tr>
                    <th colspan="2" width="50%"><?php echo lang('Aktiva'); ?></th>
                    <th colspan="2" width="50%"><?php echo lang('Pasiva'); ?></th>
                </tr>
                <tr>
                    <td colspan="2" width="50%">
                        <table class="table table-bordered table-striped" width="50%">
                            <tbody>
                                <?php
                                    $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                    $from = date('Y-m-01', strtotime($from1));
                                    $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                    $to = date('Y-m-d', strtotime('-1 days'));
                                    //echo $to;
                                    $to1 = date('m', strtotime($to));
                                    $to2 = date('Y', strtotime($to));
                                    $to4 = date('d', strtotime($to));
                                    $name = $_SESSION[Name];

                                    $x = "select * from dbo.GroupAcc where KodeGroup in('1.0.00.00.000')";
                                    $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('1.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2'";
                                    //echo $x;
                                    $bcd = sqlsrv_query($conn, $abc);
                                    while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)) {
                                        $total = $def[0];
                                    }
                                    $atotal = 0;
                                    $y = sqlsrv_query($conn, $x);
                                    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)) {
                                        $m=$z[0];
                                        $m1 = str_replace(".","",$m);
                                        $le = '';

                                        $mql = "SELECT Status, Level from TemplateDetail_2 where Account='$z[0]'";
                                        $mqr = sqlsrv_query($conn, $mql);
                                        $mdt = sqlsrv_fetch_array($mqr, SQLSRV_FETCH_NUMERIC);

                                        $sact1_ak = getIsActive($id, $z[0], '5', $conn);
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cek[]" id="<?php echo $m1; ?>" value = "<?php echo $m; ?>" <?php if($sact1_ak == 1) { echo 'checked'; } ?> >
                                                <label for="<?php echo $m1; ?>"><?php echo ($z[0]); ?></label>
                                                <input type="hidden" name="cek_status[]" id="<?php echo $m1.'-status'; ?>" value="<?php echo $mdt[0]; ?>" readonly>
                                                <input type="hidden" name="cek_level[]" id="<?php echo $m1.'-level'; ?>" value="<?php echo $mdt[1]; ?>" readonly>
                                            </td>
                                            <td><?php echo ($z[1]); ?></td>
                                            <td style="text-align: right;"><?php if ($z[0] != null){
                                                $amn = "select * from dbo.AccountBalance where KodeAccount = '$z[0]' and Tanggal = '$to'";
                                                $nma = sqlsrv_query($conn, $amn);
                                                $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                $man1 = $man[10];
                                                } else {
                                                    $man1 = 0;
                                                } 
                                                echo number_format($man1,2); ?>
                                            </td>
                                        </tr>

                                        <script type="text/javascript">
                                            $('#<?php echo $m1; ?>').click(function(){
                                                if($("#<?php echo $m1; ?>").is(':checked')) {
                                                    $('#<?php echo $m1.'-status'; ?>').val("1");
                                                    $('#<?php echo $m1.'-level'; ?>').val("1");
                                                }
                                                else {
                                                    $('#<?php echo $m1.'-status'; ?>').val("0");
                                                    $('#<?php echo $m1.'-level'; ?>').val("0");
                                                }
                                            });
                                        </script>
                                            
                                        <?php
                                        $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                        //echo $xxx;
                                        $yy = sqlsrv_query($conn, $xx);
                                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                            $n2=$zz[0];
                                            $n=$zz[0];
                                            //echo $n;
                                            $n1 = str_replace(".","",$n);
                                            $le1 = '';
                                            $xx1 = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' and KodeTipe in (select Account from TemplateDetail_2 where IDTemplate='$id') ORDER BY KodeTipe Asc";
                                            //echo $xx1;
                                            $values = array();
                                            $testarray = array();
                                            $yy1 = sqlsrv_query($conn, $xx1);
                                            while ($zz1 = sqlsrv_fetch_array($yy1, SQLSRV_FETCH_NUMERIC)){
                                                $values = array($zz1[0]);
                                                // var_dump($values);
                                                foreach ($values as $n) {
                                                   array_push($testarray,$n);
                                                    $n = str_replace(".","",$n);                       
                                                    if($n1 == $n ){
                                                        $le1 = 'checked';
                                                    } 
                                                }
                                            }

                                            $nql = "SELECT Status, Level from TemplateDetail_2 where Account='$zz[0]'";
                                            $nqr = sqlsrv_query($conn, $nql);
                                            $ndt = sqlsrv_fetch_array($nqr, SQLSRV_FETCH_NUMERIC);

                                            $sact2_ak = getIsActive($id, $zz[0], '5', $conn);
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="cek[]" id="<?php echo $n1; ?>" value = "<?php echo $n; ?>" <?php if($sact2_ak == 1) { echo 'checked'; } ?> >
                                                    <label for="<?php echo $n1; ?>">&emsp;<?php echo ($zz[0]); ?></label>
                                                    <input type="hidden" name="cek_status[]" id="<?php echo $n1.'-status'; ?>" value="<?php echo $ndt[0]; ?>" readonly>
                                                    <input type="hidden" name="cek_level[]" id="<?php echo $n1.'-level'; ?>" value="<?php echo $ndt[1]; ?>" readonly>
                                                </td>
                                                <td><?php echo ($zz[1]); ?><!-- <?php echo $n; ?> --></td>
                                                <td style="text-align: right;">
                                                    <?php 
                                                    if ($z[0] != null){
                                                        $amn = "select * from dbo.AccountBalance where KodeAccount = '$zz[0]' and Tanggal = '$to'";
                                                        $nma = sqlsrv_query($conn, $amn);
                                                        $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                        $man1 = $man[10];
                                                    } 
                                                    else {
                                                        $man1 = 0;
                                                    } 
                                                    echo number_format($man1,2); ?>
                                                </td>
                                            </tr>

                                            <script type="text/javascript">
                                                $('#<?php echo $n1; ?>').click(function(){
                                                    if($("#<?php echo $n1; ?>").is(':checked')) {
                                                        $('#<?php echo $n1.'-status'; ?>').val("1");
                                                        $('#<?php echo $n1.'-level'; ?>').val("2");
                                                    }
                                                    else {
                                                        $('#<?php echo $n1.'-status'; ?>').val("0");
                                                        $('#<?php echo $n1.'-level'; ?>').val("0");
                                                    }
                                                });
                                            </script>

                                            <?php
                                            $xxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and userName = '$name' ORDER BY KodeAccount Asc"; 
                                            //echo $xxx;
                                                
                                            $yyy = sqlsrv_query($conn, $xxx);           
                                            while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                $o=$zzz[0];
                                                $o2=$zzz[0];
                                                $o1 = str_replace(".","",$o);
                                                $lo1 = '';
                                                //echo $o1;
                                                $c=$b[0];
                                                $xxx1 = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and userName = '$name' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$id') ORDER BY KodeAccount Asc";
                                                //echo $xxx1;
                                                $values1 = array();
                                                $testarray1 = array();
                                                $yyy1 = sqlsrv_query($conn, $xxx1);
                                                while ($zzz1 = sqlsrv_fetch_array($yyy1, SQLSRV_FETCH_NUMERIC)){
                                                    $values1 = array($zzz1[0]);
                                                    //var_dump($values1);
                                                    foreach ($values1 as $o) {
                                                        array_push($testarray1,$o);
                                                        $o2 = str_replace(".","",$o);
                                                        if($o1 == $o ){
                                                            $lo1 = 'checked';
                                                        } 
                                                    }
                                                }

                                                $oql = "SELECT Status, Level from TemplateDetail_2 where Account='$zzz[0]'";
                                                $oqr = sqlsrv_query($conn, $oql);
                                                $odt = sqlsrv_fetch_array($oqr, SQLSRV_FETCH_NUMERIC);

                                                //echo $total2; 
                                                $sact3_ak = getIsActive($id, $zzz[0], '5', $conn);
                                                ?>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="cek[]" id="<?php echo $o1; ?>" value = "<?php echo $o; ?>"<?php if($sact3_ak == 1) { echo 'checked'; } ?>>
                                                        <label for="<?php echo $o1; ?>">&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></label>
                                                        <input type="hidden" name="cek_status[]" id="<?php echo $o1.'-status'; ?>" value="<?php echo $odt[0]; ?>" readonly>
                                                        <input type="hidden" name="cek_level[]" id="<?php echo $o1.'-level'; ?>" value="<?php echo $odt[1]; ?>" readonly>
                                                    </td>
                                                    <td><?php echo ($zzz[1]); ?><?php //echo $o1; ?></td>
                                                    <td style="text-align: right;">
                                                        <?php if ($z[0] != null){
                                                        $amn = "select * from dbo.AccountBalance where KodeAccount = '$zz[0]' and Tanggal = '$to'";
                                                        $nma = sqlsrv_query($conn, $amn);
                                                        $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                        $man1 = $man[10];
                                                        } else {
                                                            $man1 = 0;
                                                            } echo number_format($man1,2); ?>
                                                    </td>
                                                </tr>

                                                <script type="text/javascript">
                                                    $('#<?php echo $o1; ?>').click(function(){
                                                        if($("#<?php echo $o1; ?>").is(':checked')) {
                                                            $('#<?php echo $o1.'-status'; ?>').val("1");
                                                            $('#<?php echo $o1.'-level'; ?>').val("3");
                                                        }
                                                        else {
                                                            $('#<?php echo $o1.'-status'; ?>').val("0");
                                                            $('#<?php echo $o1.'-level'; ?>').val("0");
                                                        }
                                                    });
                                                </script>

                                                <?php
                                                $xxxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                //echo $xxxx;
                                                $yyyy = sqlsrv_query($conn, $xxxx);
                                                while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                    $p=$zzzz[0];
                                                    $p2=$zzzz[0];
                                                    $p1 = str_replace(".","",$p);
                                                    $lp1 = '';
                                                    //echo $o1;
                                                    $c=$b[0];
                                                    $xxxx1 = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 1 and userName = '$name' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$id') ORDER BY KodeAccount Asc";
                                                    //echo $xxxx1;
                                                    $values2 = array();
                                                    $testarray2 = array();
                                                    $yyyy1 = sqlsrv_query($conn, $xxxx1);
                                                    while ($zzzz1 = sqlsrv_fetch_array($yyyy1, SQLSRV_FETCH_NUMERIC)){
                                                        $values2 = array($zzzz1[0]);
                                                        //var_dump($values1);
                                                        foreach ($values2 as $p) {
                                                            array_push($testarray2,$p);
                                                            $p2 = str_replace(".","",$p);
                                                            if($p1 == $p ){
                                                                $lo1 = 'checked';
                                                            } 
                                                        }
                                                    }

                                                    $pql = "SELECT Status, Level from TemplateDetail_2 where Account='$zzzz[0]'";
                                                    $pqr = sqlsrv_query($conn, $pql);
                                                    $pdt = sqlsrv_fetch_array($pqr, SQLSRV_FETCH_NUMERIC);

                                                    $sact4_ak = getIsActive($id, $zzzz[0], '5', $conn);
                                                    ?>
                                                    <tr>        
                                                        <td>
                                                            <input type="checkbox" name="cek[]" id="<?php echo $p1; ?>" value = "<?php echo $p; ?>"<?php if($sact4_ak == 1) { echo 'checked'; } ?>>
                                                            <label for="<?php echo $p1; ?>">&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></label>
                                                            <input type="hidden" name="cek_status[]" id="<?php echo $p1.'-status'; ?>" value="<?php echo $pdt[0]; ?>" readonly>
                                                            <input type="hidden" name="cek_level[]" id="<?php echo $p1.'-level'; ?>" value="<?php echo $pdt[1]; ?>" readonly>
                                                        </td>
                                                        <td><?php echo ($zzzz[1]); ?><?php //echo $o1; ?></td>
                                                        <td style="text-align: right;"><?php if ($z[0] != null){
                                                            $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzz[0]' and Tanggal = '$to'";
                                                            $nma = sqlsrv_query($conn, $amn);
                                                            $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                            $man1 = $man[10];
                                                            } else {
                                                                $man1 = 0;
                                                            } echo number_format($man1,2); ?>
                                                        </td>
                                                    </tr>

                                                    <script type="text/javascript">
                                                        $('#<?php echo $p1; ?>').click(function(){
                                                            if($("#<?php echo $p1; ?>").is(':checked')) {
                                                                $('#<?php echo $p1.'-status'; ?>').val("1");
                                                                $('#<?php echo $p1.'-level'; ?>').val("4");
                                                            }
                                                            else {
                                                                $('#<?php echo $p1.'-status'; ?>').val("0");
                                                                $('#<?php echo $p1.'-level'; ?>').val("0");
                                                            }
                                                        });
                                                    </script>
                                                        
                                                    <?php
                                                    $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                    $from = date('Y-m-01', strtotime($from1));
                                                    $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                    $to = date('Y-m-d', strtotime('-1 days'));
                                                    $to1 = date('m', strtotime($to));
                                                    $to2 = date('Y', strtotime($to));
                                                    $name = $_SESSION[Name];
                                                    $xxxxx ="select * from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                            //echo $xxxxx;
                                                    $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                    while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  
                                                        $q=$zzzzz[0];
                                                        $q2=$zzzzz[0];
                                                        $q1 = str_replace(".","",$q);
                                                        $lq1 = '';
                                                        $xxxxx1 = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 1 and userName = '$name' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$id') ORDER BY KodeAccount Asc";
                                                        //echo $xxxx1;
                                                        $values3 = array();
                                                        $testarray3 = array();
                                                        $yyyyy1 = sqlsrv_query($conn, $xxxxx1);
                                                        while ($zzzzz1 = sqlsrv_fetch_array($yyyyy1, SQLSRV_FETCH_NUMERIC)){
                                                            $values3 = array($zzzzz1[0]);
                                                            //var_dump($values1);
                                                            foreach ($values3 as $q) {
                                                                array_push($testarray3,$q);
                                                                $q2 = str_replace(".","",$q);
                                                                if($q1 == $q ){
                                                                $lq1 = 'checked';
                                                                } 
                                                            }
                                                        }

                                                        $qql = "SELECT Status, Level from TemplateDetail_2 where Account='$zzzzz[0]'";
                                                        $qqr = sqlsrv_query($conn, $qql);
                                                        $qdt = sqlsrv_fetch_array($qqr, SQLSRV_FETCH_NUMERIC);

                                                        $sact5_ak = getIsActive($id, $zzzzz[0], '5', $conn);
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" name="cek[]" id="<?php echo $q1; ?>" value = "<?php echo $q; ?>" <?php if($sact5_ak == 1) { echo 'checked'; } ?>>
                                                                <label for="<?php echo $q1; ?>">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></label>
                                                                <input type="hidden" name="cek_status[]" id="<?php echo $q1.'-status'; ?>" value="<?php echo $qdt[0]; ?>" readonly>
                                                                <input type="hidden" name="cek_level[]" id="<?php echo $q1.'-level'; ?>" value="<?php echo $qdt[1]; ?>" readonly>
                                                            </td>
                                                            <td><?php echo ($zzzzz[1]); ?><?php echo $q1; ?></td>
                                                            <td style="text-align: right;"><?php if ($z[0] != null){
                                                                $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzzz[0]' and Tanggal = '$to'";
                                                                $nma = sqlsrv_query($conn, $amn);
                                                                $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                $man1 = $man[10];
                                                                } else {
                                                                    $man1 = 0;
                                                                } echo number_format($man1,2); ?>
                                                            </td>
                                                        </tr>

                                                        <script type="text/javascript">
                                                            $('#<?php echo $q1; ?>').click(function(){
                                                                if($("#<?php echo $q1; ?>").is(':checked')) {
                                                                    $('#<?php echo $q1.'-status'; ?>').val("1");
                                                                    $('#<?php echo $q1.'-level'; ?>').val("5");
                                                                }
                                                                else {
                                                                    $('#<?php echo $q1.'-status'; ?>').val("0");
                                                                    $('#<?php echo $q1.'-level'; ?>').val("0");
                                                                }
                                                            });
                                                        </script>

                                                    <?php } ?>
                                                <?php } ?>
                                            <?php  } ?>           
                                        <?php } ?>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </td>


                    <td colspan="2" width="50%">
                        <table class="table table-bordered table-striped" width="50%">
                            <tbody>
                                <?php
                                     $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                    $from = date('Y-m-01', strtotime($from1));
                                    $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                    $to = date('Y-m-d', strtotime('-1 days'));
                                    //echo $to;
                                    $to1 = date('m', strtotime($to));
                                    $to2 = date('Y', strtotime($to));
                                    $to4 = date('d', strtotime($to));
                                    $name = $_SESSION[Name];

                                    $x = "select* from dbo.GroupAcc where KodeGroup in('2.0.00.00.000','3.0.00.00.000')";
                                    $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('2.0.00.00.000','3.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2'";
                                    //echo $x;
                                    $bcd = sqlsrv_query($conn, $abc);
                                    while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                        $total1 = $def[0];
                                    }

                                    $y = sqlsrv_query($conn, $x);
                                    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                        $m=$z[0];
                                        $m1 = str_replace(".","",$m);
                                        $le = '';
                                        if($z != NULL){
                                            $le = 'checked';
                                        }

                                        $mql = "SELECT Status, Level from TemplateDetail_2 where Account='$z[0]'";
                                        $mqr = sqlsrv_query($conn, $mql);
                                        $mdt = sqlsrv_fetch_array($mqr, SQLSRV_FETCH_NUMERIC);

                                        $sact1_pa = getIsActive($id, $z[0], '5', $conn);
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cek[]" id="<?php echo $m1; ?>" value = "<?php echo $m; ?>" <?php if($sact1_pa == 1) { echo 'checked'; } ?>>
                                                <label for="<?php echo $m1; ?>"><?php echo ($z[0]); ?></label>
                                                <input type="hidden" name="cek_status[]" id="<?php echo $m1.'-status'; ?>" value="<?php echo $mdt[0]; ?>" readonly>
                                                <input type="hidden" name="cek_level[]" id="<?php echo $m1.'-level'; ?>" value="<?php echo $mdt[1]; ?>" readonly>
                                            </td>
                                            <td><?php echo ($z[1]); ?></td>
                                            <td style="text-align: right;">
                                                <?php if ($z[0] != null){
                                                $amn = "select * from dbo.AccountBalance where KodeAccount = '$z[0]' and Tanggal = '$to'";
                                                $nma = sqlsrv_query($conn, $amn);
                                                $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                $man1 = $man[10];
                                                } else {
                                                    $man1 = 0;
                                                } echo number_format($man1,2); ?>
                                            </td>
                                        </tr>

                                        <script type="text/javascript">
                                            $('#<?php echo $m1; ?>').click(function(){
                                                if($("#<?php echo $m1; ?>").is(':checked')) {
                                                    $('#<?php echo $m1.'-status'; ?>').val("1");
                                                    $('#<?php echo $m1.'-level'; ?>').val("1");
                                                }
                                                else {
                                                    $('#<?php echo $m1.'-status'; ?>').val("0");
                                                    $('#<?php echo $m1.'-level'; ?>').val("0");
                                                }
                                            });
                                        </script>
                                            
                                        <?php
                                        $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                        //echo $xxx;
                                        $yy = sqlsrv_query($conn, $xx);
                                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                            $n2=$zz[0];
                                            $n=$zz[0];
                                            //echo $n;
                                            $n1 = str_replace(".","",$n);
                                            $le1 = '';
                                            $xx1 = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' and KodeTipe in (select Account from TemplateDetail_2 where IDTemplate='$id') ORDER BY KodeTipe Asc";
                                            //echo $xx1;
                                            $values = array();
                                            $testarray = array();
                                            $yy1 = sqlsrv_query($conn, $xx1);
                                            while ($zz1 = sqlsrv_fetch_array($yy1, SQLSRV_FETCH_NUMERIC)){
                                                $values = array($zz1[0]);
                                                // var_dump($values);
                                                foreach ($values as $n) {
                                                   array_push($testarray,$n);
                                                    $n = str_replace(".","",$n);                       
                                                    if($n1 == $n ){
                                                        $le1 = 'checked';
                                                    } 
                                                }
                                            }

                                            $nql = "SELECT Status, Level from TemplateDetail_2 where Account='$z[0]'";
                                            $nqr = sqlsrv_query($conn, $nql);
                                            $ndt = sqlsrv_fetch_array($nqr, SQLSRV_FETCH_NUMERIC);

                                            $sact2_pa = getIsActive($id, $zz[0], '5', $conn);
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="cek[]" id="<?php echo $n1; ?>" value = "<?php echo $n; ?>" <?php if($sact2_pa == 1) { echo 'checked'; } ?>>
                                                    <label for="<?php echo $n1; ?>">&emsp;<?php echo ($zz[0]); ?></label>
                                                    <input type="hidden" name="cek_status[]" id="<?php echo $n1.'-status'; ?>" value="<?php echo $ndt[0]; ?>" readonly>
                                                    <input type="hidden" name="cek_level[]" id="<?php echo $n1.'-level'; ?>" value="<?php echo $ndt[1]; ?>" readonly>
                                                </td>
                                                <td><?php echo ($zz[1]); ?><!-- <?php echo $n; ?> --></td>
                                                <td style="text-align: right;">
                                                    <?php if ($z[0] != null){
                                                        $amn = "select * from dbo.AccountBalance where KodeAccount = '$zz[0]' and Tanggal = '$to'";
                                                        $nma = sqlsrv_query($conn, $amn);
                                                        $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                        $man1 = $man[10];
                                                        } else {
                                                            $man1 = 0;
                                                    } echo number_format($man1,2); ?>
                                                </td>
                                            </tr>

                                            <script type="text/javascript">
                                                $('#<?php echo $n1; ?>').click(function(){
                                                    if($("#<?php echo $n1; ?>").is(':checked')) {
                                                        $('#<?php echo $n1.'-status'; ?>').val("1");
                                                        $('#<?php echo $n1.'-level'; ?>').val("2");
                                                    }
                                                    else {
                                                        $('#<?php echo $n1.'-status'; ?>').val("0");
                                                        $('#<?php echo $n1.'-level'; ?>').val("0");
                                                    }
                                                });
                                            </script>

                                            <?php
                                            $xxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and userName = '$name' ORDER BY KodeAccount Asc"; 
                                            //echo $xxx;
                                                
                                            $yyy = sqlsrv_query($conn, $xxx);           
                                            while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                $o=$zzz[0];
                                                $o2=$zzz[0];
                                                $o1 = str_replace(".","",$o);
                                                $lo1 = '';
                                                //echo $o1;
                                                $c=$b[0];
                                                $xxx1 = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and userName = '$name' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$id') ORDER BY KodeAccount Asc";
                                                //echo $xxx1;
                                                $values1 = array();
                                                $testarray1 = array();
                                                $yyy1 = sqlsrv_query($conn, $xxx1);
                                                while ($zzz1 = sqlsrv_fetch_array($yyy1, SQLSRV_FETCH_NUMERIC)){
                                                    $values1 = array($zzz1[0]);
                                                    //var_dump($values1);
                                                    foreach ($values1 as $o) {
                                                        array_push($testarray1,$o);
                                                        $o2 = str_replace(".","",$o);
                                                        if($o1 == $o ){
                                                            $lo1 = 'checked';
                                                        } 
                                                    }
                                                }

                                                $oql = "SELECT Status, Level from TemplateDetail_2 where Account='$z[0]'";
                                                $oqr = sqlsrv_query($conn, $oql);
                                                $odt = sqlsrv_fetch_array($oqr, SQLSRV_FETCH_NUMERIC);

                                                //echo $total2; 
                                                $sact3_pa = getIsActive($id, $zzz[0], '5', $conn);
                                                ?>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="cek[]" id="<?php echo $o1; ?>" value = "<?php echo $o; ?>" <?php if($sact3_pa == 1) { echo 'checked'; } ?>>
                                                        <label for="<?php echo $o1; ?>">&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></label>
                                                        <input type="hidden" name="cek_status[]" id="<?php echo $o1.'-status'; ?>" value="<?php echo $odt[0]; ?>" readonly>
                                                        <input type="hidden" name="cek_level[]" id="<?php echo $o1.'-level'; ?>" value="<?php echo $odt[1]; ?>" readonly>
                                                    </td>
                                                    <td><?php echo ($zzz[1]); ?><!-- <?php echo $o1; ?> --></td>
                                                    <td style="text-align: right;"><?php if ($z[0] != null){
                                                        $amn = "select * from dbo.AccountBalance where KodeAccount = '$zz[0]' and Tanggal = '$to'";
                                                        $nma = sqlsrv_query($conn, $amn);
                                                        $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                        $man1 = $man[10];
                                                        } else {
                                                            $man1 = 0;
                                                        } echo number_format($man1,2); ?>
                                                    </td>
                                                </tr>

                                                <script type="text/javascript">
                                                    $('#<?php echo $o1; ?>').click(function(){
                                                        if($("#<?php echo $o1; ?>").is(':checked')) {
                                                            $('#<?php echo $o1.'-status'; ?>').val("1");
                                                            $('#<?php echo $o1.'-level'; ?>').val("3");
                                                        }
                                                        else {
                                                            $('#<?php echo $o1.'-status'; ?>').val("0");
                                                            $('#<?php echo $o1.'-level'; ?>').val("0");
                                                        }
                                                    });
                                                </script>

                                                <?php
                                                $xxxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                //echo $xxxx;
                                                $yyyy = sqlsrv_query($conn, $xxxx);
                                                while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                    $p=$zzzz[0];
                                                    $p2=$zzzz[0];
                                                    $p1 = str_replace(".","",$p);
                                                    $lp1 = '';
                                                    //echo $o1;
                                                    $c=$b[0];
                                                    $xxxx1 = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 1 and userName = '$name' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$id') ORDER BY KodeAccount Asc";
                                                    //echo $xxxx1;
                                                    $values2 = array();
                                                    $testarray2 = array();
                                                    $yyyy1 = sqlsrv_query($conn, $xxxx1);
                                                    while ($zzzz1 = sqlsrv_fetch_array($yyyy1, SQLSRV_FETCH_NUMERIC)){
                                                        $values2 = array($zzzz1[0]);
                                                        //var_dump($values1);
                                                        foreach ($values2 as $p) {
                                                            array_push($testarray2,$p);
                                                            $p2 = str_replace(".","",$p);
                                                            if($p1 == $p ){
                                                                $lo1 = 'checked';
                                                            } 
                                                        }
                                                    }

                                                    $pql = "SELECT Status, Level from TemplateDetail_2 where Account='$z[0]'";
                                                    $pqr = sqlsrv_query($conn, $pql);
                                                    $pdt = sqlsrv_fetch_array($pqr, SQLSRV_FETCH_NUMERIC);

                                                    $sact4_pa = getIsActive($id, $zzzz[0], '5', $conn);
                                                    ?>
                                                    <tr>        
                                                        <td>
                                                            <input type="checkbox" name="cek[]" id="<?php echo $p1; ?>" value = "<?php echo $p; ?>" <?php if($sact4_pa == 1) { echo 'checked'; } ?>>
                                                            <label for="<?php echo $p1; ?>">&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></label>
                                                            <input type="hidden" name="cek_status[]" id="<?php echo $p1.'-status'; ?>" value="<?php echo $pdt[0]; ?>" readonly>
                                                            <input type="hidden" name="cek_level[]" id="<?php echo $p1.'-level'; ?>" value="<?php echo $pdt[1]; ?>" readonly>
                                                        </td>
                                                        <td><?php echo ($zzzz[1]); ?></td>
                                                        <td style="text-align: right;">
                                                            <?php if ($z[0] != null){
                                                            $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzz[0]' and Tanggal = '$to'";
                                                            $nma = sqlsrv_query($conn, $amn);
                                                            $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                            $man1 = $man[10];
                                                            } else {
                                                                $man1 = 0;
                                                            } echo number_format($man1,2); ?>
                                                        </td>
                                                    </tr>

                                                    <script type="text/javascript">
                                                        $('#<?php echo $p1; ?>').click(function(){
                                                            if($("#<?php echo $p1; ?>").is(':checked')) {
                                                                $('#<?php echo $p1.'-status'; ?>').val("1");
                                                                $('#<?php echo $p1.'-level'; ?>').val("4");
                                                            }
                                                            else {
                                                                $('#<?php echo $p1.'-status'; ?>').val("0");
                                                                $('#<?php echo $p1.'-level'; ?>').val("0");
                                                            }
                                                        });
                                                    </script>
                                                        
                                                    <?php
                                                    $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                    $from = date('Y-m-01', strtotime($from1));
                                                    $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                    $to = date('Y-m-d', strtotime('-1 days'));
                                                    $to1 = date('m', strtotime($to));
                                                    $to2 = date('Y', strtotime($to));
                                                    $name = $_SESSION[Name];
                                                    $xxxxx ="select * from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                            //echo $xxxxx;
                                                    $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                    while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  
                                                        $q=$zzzzz[0];
                                                        $q2=$zzzzz[0];
                                                        $q1 = str_replace(".","",$q);
                                                        $lq1 = '';
                                                        $xxxxx1 = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 1 and userName = '$name' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$id') ORDER BY KodeAccount Asc";
                                                        //echo $xxxx1;
                                                        $values3 = array();
                                                        $testarray3 = array();
                                                        $yyyyy1 = sqlsrv_query($conn, $xxxxx1);
                                                        while ($zzzzz1 = sqlsrv_fetch_array($yyyyy1, SQLSRV_FETCH_NUMERIC)){
                                                            $values3 = array($zzzzz1[0]);
                                                            //var_dump($values1);
                                                            foreach ($values3 as $q) {
                                                                array_push($testarray3,$q);
                                                                $q2 = str_replace(".","",$q);
                                                                if($q1 == $q ){
                                                                    $lo1 = 'checked';
                                                                } 
                                                            }
                                                        }

                                                        $qql = "SELECT Status, Level from TemplateDetail_2 where Account='$z[0]'";
                                                        $qqr = sqlsrv_query($conn, $qql);
                                                        $qdt = sqlsrv_fetch_array($qqr, SQLSRV_FETCH_NUMERIC);

                                                        $sact5_pa = getIsActive($id, $zzzzz[0], '5', $conn);
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" name="cek[]" id="<?php echo $q1; ?>" value = "<?php echo $q; ?>" <?php if($sact5_pa == 1) { echo 'checked'; } ?>>
                                                                <label for="<?php echo $q1; ?>">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></label>
                                                                <input type="hidden" name="cek_status[]" id="<?php echo $q1.'-status'; ?>" value="<?php echo $qdt[0]; ?>" readonly>
                                                                <input type="hidden" name="cek_level[]" id="<?php echo $q1.'-level'; ?>" value="<?php echo $qdt[1]; ?>" readonly>
                                                            </td>
                                                            <td><?php echo ($zzzzz[1]); ?></td>
                                                            <td style="text-align: right;"><?php if ($z[0] != null){
                                                                $amn = "select * from dbo.AccountBalance where KodeAccount = '$zzzzz[0]' and Tanggal = '$to'";
                                                                $nma = sqlsrv_query($conn, $amn);
                                                                $man = sqlsrv_fetch_array($nma, SQLSRV_FETCH_NUMERIC);
                                                                $man1 = $man[10];
                                                                } else {
                                                                    $man1 = 0;
                                                                } echo number_format($man1,2); ?>
                                                            </td>
                                                        </tr>

                                                        <script type="text/javascript">
                                                            $('#<?php echo $q1; ?>').click(function(){
                                                                if($("#<?php echo $q1; ?>").is(':checked')) {
                                                                    $('#<?php echo $q1.'-status'; ?>').val("1");
                                                                    $('#<?php echo $q1.'-level'; ?>').val("5");
                                                                }
                                                                else {
                                                                    $('#<?php echo $q1.'-status'; ?>').val("0");
                                                                    $('#<?php echo $q1.'-level'; ?>').val("0");
                                                                }
                                                            });
                                                        </script>

                                                    <?php } ?>
                                                <?php } ?>
                                            <?php  } ?>           
                                        <?php } ?>
                                    <?php } ?>
                            </tbody>
                        </table>
                    </td>


                    <tr>
                        <th><?php echo lang('Total Aktiva'); ?></th>
                        <th><span class="right"><?php echo number_format($total,2); ?></span></th>
                        <th><?php echo lang('Total Pasiva'); ?></th>
                        <th><span class="right"><?php echo number_format($total1,2); ?></span></th>
                    </tr>
                </tr>
            </table>
        <?php } ?>

        <?php if($_GET['st'] == 4 ){ ?>
          <div class="table-responsive">
            <?php
            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
            $from = date('Y-m-01', strtotime($from1));
            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
            $to = date('Y-m-t', strtotime($to3));
            $to1 = date('m', strtotime($to3));
            $to2 = date('Y', strtotime($to3));
            $a = "exec dbo.ProsesGenerateLaporanRugiLaba '$from','$to','$_SESSION[UserID]''";
            //echo $a;
            $b = sqlsrv_query($conn, $a);
            ?>

            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Laporan Laba Rugi'); ?></h3>
                </tr>
            </div>

            <div class="box-header with-border" align="center">
                <tr>
                    <?php
                        $bulan = $_GET['bulan'];
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                    if ($bulan != 3) {
                                        if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        if($bulan !=12){
                                                                            Eror;
                                                                        } else {
                                                                           $bulan = 'Desember';
                                                                        }
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'OKtober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                        } else {
                                            $bulan = 'April';       
                                        }
                                    } else {
                                        $bulan = 'Maret';
                                    }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                    ?>
                    <h5 class="box-title" style="margin-bottom: 30px;"><?php echo "Periode : "; ?><?php echo $bulan," - "; ?><?php echo $_GET['tahun']; ?></h5>
                </tr>
            </div>

                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" width="50%"><?php echo lang('Laba Rugi'); ?></th>
                        </tr>
                        <tr>
                            <td colspan="2" width="50%">
                                <table class="table table-bordered table-striped" width="50%">
                                <tbody>
                             
                                    <?php
                                        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                        $from = date('Y-m-01', strtotime($from1));
                                        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                        $to = date('Y-m-t', strtotime($to3));
                                        $to1 = date('m', strtotime($to3));
                                        $to2 = date('Y', strtotime($to3));
                                        $name = $_SESSION[Name];
                                        $x = "SELECT * from dbo.GroupAcc where KodeGroup in('4.0.00.00.000', '5.0.00.00.000', '6.0.00.00.000') and KodeGroup in (select Account from TemplateDetail_2)";
                                        $atotal = 0;
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                            $m=$z[0];
                                            $m1 = str_replace(".","",$m); 

                                            $mql = "SELECT Status, Level from TemplateDetail_2 where Account='$z[0]'";
                                            $mqr = sqlsrv_query($conn, $mql);
                                            $mdt = sqlsrv_fetch_array($mqr, SQLSRV_FETCH_NUMERIC);

                                            $sact1 = getIsActive($id, $z[0], '4', $conn);
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" name="cek[]" id="<?php echo $m1; ?>" value ="<?php echo $m; ?>" <?php if($sact1 == 1) { echo 'checked'; } ?>>
                                                    <label for="<?php echo $m1; ?>"><?php echo ($z[0]); ?></label>
                                                    <input type="hidden" name="cek_status[]" id="<?php echo $m1.'-status'; ?>" value="<?php echo $mdt[0]; ?>" readonly>
                                                    <input type="hidden" name="cek_level[]" id="<?php echo $m1.'-level'; ?>" value="<?php echo $mdt[1]; ?>" readonly>
                                                </td>
                                                <td><?php echo ($z[1]); ?></td>
                                                <td></td>
                                            </tr>

                                            <script type="text/javascript">
                                                $('#<?php echo $m1; ?>').click(function(){
                                                    if($("#<?php echo $m1; ?>").is(':checked')) {
                                                        $('#<?php echo $m1.'-status'; ?>').val("1");
                                                        $('#<?php echo $m1.'-level'; ?>').val("1");
                                                    }
                                                    else {
                                                        $('#<?php echo $m1.'-status'; ?>').val("0");
                                                        $('#<?php echo $m1.'-level'; ?>').val("0");
                                                    }
                                                });
                                            </script>
                                            <?php
                                            $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' and KodeTipe in (select Account from TemplateDetail_2) ORDER BY KodeTipe Asc";
                                            //$xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                            $abc = "select SUM (NilaiHide) from dbo.LaporanRugiLabaViewNew where GroupID = '$z[0]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name'";
                                            $bcd = sqlsrv_query($conn, $abc);
                                            $b = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                            $dtotal1=$b[0];
                                            $atotal=$atotal+$dtotal1;
                                            $yy = sqlsrv_query($conn, $xx);
                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                                $n=$zz[0];
                                                $n1 = str_replace(".","",$n);

                                                $nql = "SELECT Status, Level from TemplateDetail_2 where Account='$zz[0]'";
                                                $nqr = sqlsrv_query($conn, $nql);
                                                $ndt = sqlsrv_fetch_array($nqr, SQLSRV_FETCH_NUMERIC);

                                                $sact2 = getIsActive($id, $zz[0], '4', $conn);
                                                ?>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="cek[]" id="<?php echo $n1; ?>" value="<?php echo $n; ?>" <?php if($sact2 == 1) { echo 'checked'; } ?>>
                                                        <label for="<?php echo $n1; ?>">&emsp;<?php echo ($zz[0]); ?></label>
                                                        <input type="hidden" name="cek_status[]" id="<?php echo $n1.'-status'; ?>" value="<?php echo $ndt[0]; ?>" readonly>
                                                        <input type="hidden" name="cek_level[]" id="<?php echo $n1.'-level'; ?>" value="<?php echo $ndt[1]; ?>" readonly>
                                                        
                                                    </td>
                                                    <td><?php echo ($zz[1]); ?></td>
                                                    <td></td>
                                                </tr>

                                                <script type="text/javascript">
                                                    $('#<?php echo $n1; ?>').click(function(){
                                                        if($("#<?php echo $n1; ?>").is(':checked')) {
                                                            $('#<?php echo $n1.'-status'; ?>').val("1");
                                                            $('#<?php echo $n1.'-level'; ?>').val("2");
                                                        }
                                                        else {
                                                            $('#<?php echo $n1.'-status'; ?>').val("0");
                                                            $('#<?php echo $n1.'-level'; ?>').val("0");
                                                        }
                                                    });
                                                </script>
                                                <?php
                                                //$xxx = "SELECT * from [dbo].[LaporanLabaRugiKeuangan] where TypeID = '$zz[0]' and header = 0 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount in (Select Account from TemplateDetail_2 ) ORDER BY KodeAccount Asc"; 
                                                $xxx = "SELECT * from [dbo].[LaporanLabaRugiKeuangan] where TypeID = '$zz[0]' and header = 0 and UserName = '$name' and KodeAccount in (Select Account from TemplateDetail_2 ) ORDER BY KodeAccount Asc"; 
                                                //echo $xxx.'<br>';
                                                $yyy = sqlsrv_query($conn, $xxx);
                                                while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                    $o=$zzz[0];
                                                    $o1 = str_replace(".","",$o);
                                                    $abc = "select SUM (NilaiHide) from dbo.LaporanLabaRugiKeuangan where TypeID = '$zzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%'";
                                                    //echo $abc;
                                                    $bcd = sqlsrv_query($conn, $abc);
                                                    $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                    $c=$b[0];
                                                    //echo $total2; 

                                                    $oql = "SELECT Status, Level from TemplateDetail_2 where Account='$zzz[0]'";
                                                    $oqr = sqlsrv_query($conn, $oql);
                                                    $odt = sqlsrv_fetch_array($oqr, SQLSRV_FETCH_NUMERIC);

                                                    $sact3 = getIsActive($id, $zzz[0], '4', $conn);
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="cek[]" id="<?php echo $o1; ?>" value="<?php echo $o; ?>" <?php if($sact3 == 1) { echo 'checked'; } ?>>
                                                            <label for="<?php echo $o1; ?>">&emsp;&emsp;&emsp;<?php echo ($zzz[0]); ?></label>
                                                            <input type="hidden" name="cek_status[]" id="<?php echo $o1.'-status'; ?>" value="<?php echo $odt[0]; ?>" readonly>
                                                            <input type="hidden" name="cek_level[]" id="<?php echo $o1.'-level'; ?>" value="<?php echo $odt[1]; ?>" readonly>
                                                        </td>
                                                        <td><?php echo ($zzz[1]); ?></td>
                                                        <td class="kanan"><?php echo number_format($c,2); ?></td>
                                                    </tr>

                                                    <script type="text/javascript">
                                                        $('#<?php echo $o1; ?>').click(function(){
                                                            if($("#<?php echo $o1; ?>").is(':checked')) {
                                                                $('#<?php echo $o1.'-status'; ?>').val("1");
                                                                $('#<?php echo $o1.'-level'; ?>').val("3");
                                                            }
                                                            else {
                                                                $('#<?php echo $o1.'-status'; ?>').val("0");
                                                                $('#<?php echo $o1.'-level'; ?>').val("0");
                                                            }
                                                        });
                                                    </script>
                    
                                                    <?php                                              
                                                    $xxxx = "SELECT * from [dbo].[LaporanLabaRugiKeuangan] where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount in (Select Account from TemplateDetail_2 )and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                    //echo $xxxx;
                                                    
                                                    $yyyy = sqlsrv_query($conn, $xxxx);
                                                    while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                        //echo 'OK';
                                                        $p=$zzzz[0];
                                                        $p1 = str_replace(".","",$p);
                                                        $abc = "select SUM (NilaiHide) from dbo.LaporanLabaRugiKeuangan where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%'";
                                                        //echo $abc;
                                                        $bcd = sqlsrv_query($conn, $abc);
                                                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                        $a=$b[0];
                                                        //echo $total3;

                                                        $pql = "SELECT Status, Level from TemplateDetail_2 where Account='$zzzz[0]'";
                                                        $pqr = sqlsrv_query($conn, $pql);
                                                        $pdt = sqlsrv_fetch_array($pqr, SQLSRV_FETCH_NUMERIC);

                                                        $sact4 = getIsActive($id, $zzzz[0], '4', $conn);
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" name="cek[]" id="<?php echo $p1; ?>" value = "<?php echo $p; ?>" <?php if($sact4 == 1) { echo 'checked'; } ?>>
                                                                <label for="<?php echo $p1; ?>">&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzz[0]); ?></label>
                                                                <input type="hidden" name="cek_status[]" id="<?php echo $p1.'-status'; ?>" value="<?php echo $pdt[0]; ?>" readonly>
                                                                <input type="hidden" name="cek_level[]" id="<?php echo $p1.'-level'; ?>" value="<?php echo $pdt[1]; ?>" readonly>
                                                            </td>
                                                            <td><?php echo ($zzzz[1]); ?></td>
                                                            <td class="kanan"><?php echo number_format($a,2); ?></td>
                                                        </tr>

                                                        <script type="text/javascript">
                                                            $('#<?php echo $p1; ?>').click(function(){
                                                                if($("#<?php echo $p1; ?>").is(':checked')) {
                                                                    $('#<?php echo $p1.'-status'; ?>').val("1");
                                                                    $('#<?php echo $p1.'-level'; ?>').val("4");
                                                                }
                                                                else {
                                                                    $('#<?php echo $p1.'-status'; ?>').val("0");
                                                                    $('#<?php echo $p1.'-level'; ?>').val("0");
                                                                }
                                                            });
                                                        </script>

                                                        <?php
                                                            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                            $from = date('Y-m-01', strtotime($from1));
                                                            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                                                            $to = date('Y-m-t', strtotime($to3));
                                                            $to1 = date('m', strtotime($to3));
                                                            $to2 = date('Y', strtotime($to3));
                                                            $name = $_SESSION[Name];
                                                            //$xxxxx ="select * from dbo.LaporanRugiLabaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount in (select Account from TemplateDetail_2 )and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                            $xxxxx ="SELECT * from [dbo].[LaporanLabaRugiKeuangan] where TypeID = '$zzzz[2]' and header =2 and UserName = '$name' and KodeAccount in (select Account from TemplateDetail_2 ) and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                            //echo $xxxxx;
                                                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){ 
                                                                $q=$zzzzz[0];
                                                                $q1 = str_replace(".","",$q);

                                                                $qql = "SELECT Status, Level from TemplateDetail_2 where Account='$zzzzz[0]'";
                                                                $qqr = sqlsrv_query($conn, $qql);
                                                                $qdt = sqlsrv_fetch_array($qqr, SQLSRV_FETCH_NUMERIC);

                                                                $sact5 = getIsActive($id, $zzzz[0], '4', $conn);
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <input type="checkbox" name="cek[]" id="<?php echo $q1; ?>" value="<?php echo $q; ?>" <?php if($sact5 == 1) { echo 'checked'; } ?>>
                                                                        <label for="<?php echo $q1; ?>">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php echo ($zzzzz[0]); ?></label>
                                                                        <input type="hidden" name="cek_status[]" id="<?php echo $q1.'-status'; ?>" value="<?php echo $qdt[0]; ?>" readonly>
                                                                        <input type="hidden" name="cek_level[]" id="<?php echo $q1.'-level'; ?>" value="<?php echo $qdt[1]; ?>" readonly>
                                                                    </td>
                                                                    <td><?php echo ($zzzzz[1]); ?></td>
                                                                    <td class="kanan"><?php echo ($zzzzz[8]); ?></td>
                                                                </tr>

                                                                <script type="text/javascript">
                                                                    $('#<?php echo $q1; ?>').click(function(){
                                                                        if($("#<?php echo $q1; ?>").is(':checked')) {
                                                                            $('#<?php echo $q1.'-status'; ?>').val("1");
                                                                            $('#<?php echo $q1.'-level'; ?>').val("5");
                                                                        }
                                                                        else {
                                                                            $('#<?php echo $q1.'-status'; ?>').val("0");
                                                                            $('#<?php echo $q1.'-level'; ?>').val("0");
                                                                        }
                                                                    });
                                                                </script>
                                                            <?php } ?>
                                                    <?php } ?>

                                                <?php } ?>
                                            <?php } ?>
                                        <?php $dtotal1+=$b[0];
                                        } 
                                        ?>
                                </tbody>
                                 </table>
                            </td>
                            </td>
                             <tr>
                                <th><?php echo ('Total Laba Rugi'); ?></th>
                                <th><span class="right"><?php echo number_format($atotal,2); ?></span></th>
                            </tr>
                        </tr>
                    </table>
        <?php } ?>

            </form>
        <?php } ?>  

    </div>

   
</div>


<!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });
</script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview1").shieldTreeView();
    });
</script>
<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview3").TreeView();
        collapsed: true,
        unique: true,
        persist: “location”
    });
</script>

<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

    $("#button1").click(function(){
        var level = $("#level").val();
        window.open("level_neraca.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&level="+level);
    }); 
    
    function arHide(setID) {
            if( $(setID).hasClass('hide') ) $(setID).removeClass('hide');
            else $(setID).addClass('hide');
        }


$(function() {

  $('input[type="checkbox"]').change(checkboxChanged);

  function checkboxChanged() {
    var $this = $(this),
        checked = $this.prop("checked"),
        container = $this.parent(),
        siblings = container.siblings();

    container.find('input[type="checkbox"]')
    .prop({
        indeterminate: false,
        checked: checked
    })
    .siblings('label')
    .removeClass('custom-checked custom-unchecked custom-indeterminate')
    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

    checkSiblings(container, checked);
  }

  function checkSiblings($el, checked) {
    var parent = $el.parent().parent(),
        all = true,
        indeterminate = false;

    $el.siblings().each(function() {
      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
    });

    if (all && checked) {
      parent.children('input[type="checkbox"]')
      .prop({
          indeterminate: false,
          checked: checked
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(checked ? 'custom-checked' : 'custom-unchecked');

      checkSiblings(parent, checked);
    } 
    else if (all && !checked) {
      indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

      parent.children('input[type="checkbox"]')
      .prop("checked", checked)
      .prop("indeterminate", indeterminate)
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

      checkSiblings(parent, checked);
    } 
    else {
      $el.parents("li").children('input[type="checkbox"]')
      .prop({
          indeterminate: true,
          checked: false
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass('custom-indeterminate');
    }
  }
});


</script>




<?php require('footer_new.php');?>
