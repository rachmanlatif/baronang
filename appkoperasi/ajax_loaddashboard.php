<?php
include "connect.php";

$json = array(
    'status'=>0,
    'point'=>'',
    'rp'=>'',
);

$month = date('m');
$yn = date('Y');
$day = date('d');
$dtf = date('Y-m-d');

$sql_line1 = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='TOPP' and [Date] between '$dtf 00:00:00' and '$dtf 23:59:59'";
$query1 = sqlsrv_query($conn, $sql_line1);
while($data1 = sqlsrv_fetch_array($query1)) {
 if ($data1[0] == null) {
   $point = 0; }
 else {$point =  $data1[0];}
}

$sql_rp = "SELECT ValueConversion from [dbo].[ParkingSetting] ";
$query_rp = sqlsrv_query($conn, $sql_rp);
while($data_rp = sqlsrv_fetch_array($query_rp)) {
$rp = $point * $data_rp[0];
}

$json['status'] = 1;
$json['point'] = number_format($point,0,",",".");
$json['rp'] = 'Rp. '.number_format($rp,0,",",".");

echo json_encode($json);
?>
