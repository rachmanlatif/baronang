<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php
        $qq = "select * from dbo.UserApprovalLoan where KodeKaryawan = '".$_SESSION['UserID']."' and KID = '".$_SESSION['KID']."'";
        $ww = sqlsrv_query($conn, $qq);
        $ee = sqlsrv_fetch_array( $ww, SQLSRV_FETCH_NUMERIC);
        if($ee != null){
        ?>
            <h2 class="uppercase"><?php echo lang('Persetujuan Pinjaman'); ?></h2>

            <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                    <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>--> --.
                    <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                    <?php echo $_SESSION['error-message']; ?>
                </div>
            <?php } ?>

                <div class="box box-primary">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th><?php echo lang('No App Pinjaman'); ?></th>
                                <th>Member ID</th>
                                <th><?php echo lang('Nama'); ?></th>
                                <th><?php echo lang('Produk'); ?></th>
                                <th><?php echo lang('Tanggal Permintaan'); ?></th>
                                <th><?php echo lang('Jumlah'); ?></th>
                                <th><?php echo lang('Jumlah yang disetujui'); ?></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            $aa   = "select * from [dbo].[LoanApplicationListView] where StatusComplete in(2,3) order by TimeStamp desc";
                            $bb  = sqlsrv_query($conn, $aa);
                            while($cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC)){
                                $amountapp = 0;
                                $x = "select MIN(Amount) from [dbo].[HistoriAppLoan] where KodeLoanAppNum = '".$cc[2]."'";
                                $y = sqlsrv_query($conn, $x);
                                $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                                if($z != null){
                                    $amountapp = $z[0];
                                }
                                ?>
                                <tr>
                                    <td><?= $no; ?></td>
                                    <td><?= $cc[2] ?></td>
                                    <td><?= $cc[0] ?></td>
                                    <td><?= $cc[1] ?></td>
                                    <td><?= $cc[6] ?></td>
                                    <td><?= date_format($cc[3],"Y-m-d H:i:s"); ?></td>
                                    <td style="text-align: right;x">
                                        <?= number_format($cc[7]) ?>
                                        <input type="hidden" id="amountreq<?php echo $cc[2]; ?>" value="<?php echo $cc[7]; ?>" readonly>
                                    </td>
                                    <td>
                                        <?php if($cc[9] == 3){ ?>
                                            <b><?php echo number_format($amountapp); ?></b> <?php echo lang('Menunggu persetujuan Peminta'); ?>
                                        <?php } else { ?>
                                            <input type="text" id="amount<?php echo $cc[2]; ?>" class="form-control price" value="<?php echo $amountapp; ?>">
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <?php if($cc[9] == 2){ ?>
                                            <button type="button" class="btn btn-success btn-sm btn-approve<?php echo $cc[2] ?>" title="Approve"><i class="fa fa-check"></i> <?php echo lang('Menyetujui'); ?></button>
                                        <?php } ?>
                                        <button type="button" class="btn btn-info btn-sm btn-user<?php echo $cc[0] ?>" title="Show detail user"><i class="ion-android-create"></i> </button>
                                        <button type="button" class="btn btn-info btn-sm btn-chat<?php echo $cc[2] ?>" data-toggle="modal" data-target="#myModalChat" title="Chat"><i class="fa fa-book"></i> Chat</button>
                                    </td>
                                </tr>

                                <script type="text/javascript">
                                    $('.btn-user<?php echo $cc[0] ?>').click(function(){
                                        $('#modal').click();
                                        $.ajax({
                                            url : "ajax_getmember.php",
                                            type : 'POST',
                                            data: { member: '<?php echo $cc[0] ?>'},
                                            success : function(data) {
                                                $(".modal-body").html(data);
                                            },
                                            error : function(){
                                                alert('Try again.');
                                            }
                                        });
                                    });

                                    $('.btn-chat<?php echo $cc[2] ?>').click(function(){
                                        $('#message').val('');

                                        $.ajax({
                                            url : "ajax_chatloan.php",
                                            type : 'POST',
                                            data: { loanappnum: '<?php echo $cc[2] ?>'},
                                            success : function(data) {
                                                $(".box-body-chat").html(data);
                                            },
                                            error : function(){
                                                alert('Try again.');
                                            }
                                        });
                                    });

                                    $('.btn-approve<?php echo $cc[2] ?>').click(function(){
                                        var amount = $('#amount<?php echo $cc[2] ?>').val();
                                        var amountreq = $('#amountreq<?php echo $cc[2] ?>').val();

                                        if(amount <= 0){
                                            alert('Amount approve tidak boleh 0');
                                            return false;
                                        }
                                        else if(amount > parseFloat(amountreq)){
                                            alert('Amount approve tidak boleh melebihi request');
                                            return false;
                                        }
                                        else{
                                            $.ajax({
                                                url : "procchatapp.php",
                                                type : 'POST',
                                                data: { loanappnum: '<?php echo $cc[2] ?>', amount: amount},
                                                success : function(data) {
                                                    window.location.href = 'loan_app.php';
                                                },
                                                error : function(){
                                                    alert('Try again.');
                                                }
                                            });
                                        }
                                    });
                                </script>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    $('.btn-send').click(function(){
                        var kode = $('#kode').val();
                        var msg = $('#message').val();

                        $.ajax({
                            url : "ajax_chatloan.php",
                            type : 'POST',
                            data: { loanappnum: kode, message: msg},
                            success : function(data) {
                                $(".box-body-chat").html(data);
                            },
                            error : function(){
                                alert('Try again.');
                            }
                        });
                    });
                </script>

        <?php } else { ?>
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                You are not user approval loan application
            </div>
        <?php } ?>

    </div>
</div>

<?php require('footer.php');?>
