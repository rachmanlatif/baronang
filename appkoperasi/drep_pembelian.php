<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Laporan Pembelian Produk Member");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
    

    // baris judul
    $objWorkSheet->getStyle('B1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('B1', 'Laporan Pembelian Produk Member');
    $objWorkSheet->getStyle('B2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B2', 'Periode :');
    $objWorkSheet->getStyle('B3')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B3')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B3', 'Lokasi :');
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A5', 'Jenis Kendaraan :');
    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A6', 'Total Pembelian :');
   
    


    $objWorkSheet->getStyle('A7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A7', 'No');
    $objWorkSheet->getStyle('B7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B7', 'No. Akun');
    $objWorkSheet->getStyle('C7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C7', 'Tanggal ');
    $objWorkSheet->getStyle('D7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D7', 'Valid Date');
    $objWorkSheet->getStyle('E7')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E7', 'Amount');  
 


    if($_GET['from'] and $_GET['to']){
    
    $no = 1;
    $bln = 2 ;
    $hanyatanggal = date('d', strtotime($_GET['from']));
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $tgl2 = date('d', strtotime($_GET['to']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('C'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("C".$bln,$hanyatanggal .' '.$bulan .' '. $tahun .' - '. $tgl2 .' '. $bulan2 .' '. $tahun2);   


    $k = 4;
    $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
           
        $objWorkSheet->getStyle('B'.$k)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$k,$z[0] .' - '. $z[2]);           

    }

    $lok = "select * from dbo.LocationMerchant where LocationID = '$_GET[akun]'";
    $proloc = sqlsrv_query($conn,$lok);
    $hasloc = sqlsrv_fetch_array($proloc, SQLSRV_FETCH_NUMERIC);


    $objWorkSheet->getStyle('C3')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("C3",$hasloc[2]);
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("B5",$_GET['kop']);
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("B6",number_format($_GET['total'],2), PHPExcel_Cell_DataType::TYPE_STRING);

    

    
    $row = 8;
    $rw = 8;
    
    $xy = "SELECT * FROM ( SELECT a.accountdebet, a.accountkredit, a.date, a.amount, b.validdate, b.vehicleID, c.name, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.memberlocation b on a.accountdebet = b.cardno join dbo.VehicleType c on b.VehicleID = c.vehicleID where date between '$_GET[from]' and '$_GET[to]' and a.accountKredit = '$_GET[akun]' and a.transactiontype = 'MBRS' and b.vehicleID = '$_GET[vehicle]') a";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
        //  // if($za[11] == 1){
        //  //    $member='Member';
        //  //    } else {
        //  //    $member='Non Member';
        //  //    }
        // $saldo = 0;
        //     if ($za != null){
        //         $snilaid = "select sum(amount) from dbo.translist where AccountDebet = '$za[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tgla' and '$tglk'";
        //         //echo $snilaid;
        //         $pnilaid = sqlsrv_query($conn, $snilaid);
        //         while ($hnilaid = sqlsrv_fetch_array( $pnilaid, SQLSRV_FETCH_NUMERIC)){
        //             if ($hnilaid != 0){
        //                 $hsnilaid = $hnilaid[0];    
        //             } else {
        //                 $hsnilaid = 0;    
        //             }
        //         }

        //         $snilaik = "select sum(amount) from dbo.translist where AccountKredit = '$za[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tgla' and '$tglk'";
        //         //echo $snilaik;
        //         $pnilaik = sqlsrv_query($conn, $snilaik);
        //         while ($hnilaik = sqlsrv_fetch_array( $pnilaik, SQLSRV_FETCH_NUMERIC)){
        //             if ($hnilaik != 0){
        //                 $hsnilaik = $hnilaik[0];    
        //             } else {
        //                 $hsnilaik = 0;    
        //             }
        //         }

        //         $sdebit = "select sum(amount) from dbo.translist where AccountDebet = '$za[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tglk' and '$tglh'";
        //         //echo $sdebit;
        //         $pdebit = sqlsrv_query($conn, $sdebit);
        //         while ($hdebit = sqlsrv_fetch_array( $pdebit, SQLSRV_FETCH_NUMERIC)){
        //             if ($hdebit != 0){
        //                 $hsdebit = $hdebit[0];    
        //             } else {
        //                 $hsdebit = 0;    
        //             }
        //         }
                                    

        //         $skredit = "select sum(amount) from dbo.translist where AccountKredit = '$za[0]' and transactiontype between 'TOPE' and 'TOPP'and Date between '$tglk' and '$tglh'";
        //         //echo $skredit;
        //         $pkredit = sqlsrv_query($conn, $skredit);
        //         while ($hkredit = sqlsrv_fetch_array( $pkredit, SQLSRV_FETCH_NUMERIC)){
        //             if ($hkredit != 0){
        //                 $hskredit = $hkredit[0];
        //             } else {
        //                 $hskredit = 0;    
        //             }
        //         }

        //     $saldo = ($hsnilaik - $hsnilaid) - $hsdebit + $hskredit;
        // } 


               
        $objWorkSheet->SetCellValueExplicit("A".$row,$za[7]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
        $objWorkSheet->SetCellValue("C".$row, $za[2]->format('Y-m-d H:i:s'));
        $objWorkSheet->SetCellValue("D".$row, $za[4]->format('Y-m-d'));
        $objWorkSheet->SetCellValue("E".$row, number_format($za[3],2), PHPExcel_Cell_DataType::TYPE_STRING);
        
        $row++;
        $rw++;
        }
 }
//exit;
    $objWorkSheet->setTitle('Drep Laporan Pembelian Produk');

    $fileName = 'LapBeliMember'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
