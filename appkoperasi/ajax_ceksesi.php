<?php
include "connect.php";

$json = array(
    'status'=>0,
    'message'=>'',
);

$tanggal = date('Y-m-d');
$q = "SELECT top 1 b.IDCashierAssignUser, a.AssignDate, b.UserID, b.Amount FROM dbo.CashierAssign a inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where a.Type in(0,1) and b.Status = 0 and b.UserID='$_SESSION[UserID]' and a.AssignDate='$tanggal'";
$w = sqlsrv_query($conn, $q);
$e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);
if($e != null){
    $x = "select* from [dbo].[CashierSession] where UserID = '$_SESSION[UserID]' and IDCashierAssignUser = '$e[0]' and Type in(0,1) and Tanggal = '$tanggal' and Status = 0";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    if($z == null){
        $json['status'] = 1;
    }
}

//kasir
$xx = "select * from [dbo].[CashierSession] where UserID = '$_SESSION[UserID]' and Type = 1 and Tanggal < '$tanggal' and Status = 0";
$yy = sqlsrv_query($conn, $xx);
$zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
if($zz != null){
    $json['status'] = 2;
    $_SESSION['sesikasir'] = $zz[0];
}

if(!isset($_SESSION['sesikasir'])){
    $xx = "select * from [dbo].[CashierSession] where UserID = '$_SESSION[UserID]' and Type = 1 and Tanggal = '$tanggal' and Status = 0";
    $yy = sqlsrv_query($conn, $xx);
    $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
    if($zz != null){
        $_SESSION['sesikasir'] = $zz[0];
    }
}

//teller
$xx = "select * from [dbo].[CashierSession] where UserID = '$_SESSION[UserID]' and Type = 0 and Tanggal < '$tanggal' and Status = 0";
$yy = sqlsrv_query($conn, $xx);
$zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
if($zz != null){
    $json['status'] = 3;
    $_SESSION['sesiteller'] = $zz[0];
}

if(!isset($_SESSION['sesiteller'])){
    $xx = "select * from [dbo].[CashierSession] where UserID = '$_SESSION[UserID]' and Type = 0 and Tanggal = '$tanggal' and Status = 0";
    $yy = sqlsrv_query($conn, $xx);
    $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
    if($zz != null){
        $_SESSION['sesiteller'] = $zz[0];
    }
}

echo json_encode($json);
?>
