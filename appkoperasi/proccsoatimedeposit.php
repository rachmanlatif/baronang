<?php
session_start();
error_reporting(0);

include "connect.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$bln			= $_GET['balance'];
$delete			= $_GET['delete'];

$member		= $_POST['member'];
$nama		= $_POST['nama'];
$timedep	= $_POST['timedep'];
$reg		= $_POST['reg'];
$amount		= $_POST['amount'];
$aro		= $_POST['aro'];
$itm		= $_POST['itm'];
$itaa		= $_POST['itaa'];

if($member == ""  || $timedep == "" || $reg == "" || $amount == "" || $aro == "" || $itm == "" || $itaa == ""){
    messageAlert('Harap isi seluruh kolom','info');
    header('Location: csoatimedeposit.php');
}
else{
    $a = "select * from [dbo].[MemberList] where MemberID='$member' and StatusMember = 1";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $x = "select * from [dbo].[RegularSavingAcc] where AccountNo='$reg' and Status in ('1','9')";
        $y = sqlsrv_query($conn, $x);
        $z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC);
        if($z != null){
            $q = "select * from [dbo].[TimeDepositType] where KodeTimeDepositType='$timedep'";
            $w = sqlsrv_query($conn, $q);
            $e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);
            if($e != null){

                if(doubleval($e[4]) <= str_replace(',','',$amount)){
                    $ppsql = "exec [dbo].[ProsesTimeDepositAccountBaru] '$member', '$timedep', '$aro', '$itm', '$itaa', '$amount', 1,'$reg','$_SESSION[UserID]','','$reg'";
                    $ppstmt = sqlsrv_query($conn, $ppsql);

                    if($ppstmt){
                        messageAlert('Success created Time Deposit','success');
                        header('Location: identity_cs.php');
                    }
                    else{
                        messageAlert('Failed create Time Deposit','danger');
                        header('Location: csoatimedeposit.php');
                    }
                }
                else{
                    messageAlert('Minimum amount is '.number_format($e[4]),'warning');
                    header('Location: csoatimedeposit.php');
                }
            }
            else{
                messageAlert('Time deposit account not found','warning');
                header('Location: csoatimedeposit.php');
            }
        }
        else{
            messageAlert('Regular saving account not found','warning');
            header('Location: csoatimedeposit.php');
        }
    }
    else{
        messageAlert('Member ID Not Active','warning');
        header('Location: csoatimedeposit.php');
    }
}
?>