<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>
    

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Laporan Pembelian Member'); ?></h2>                     
                              
            <form class="form-horizontal" action="" method = "GET">
                 <div class="input-field">
                    <?php
                        if(isset($_GET['bulan'])){
                            $from = $_GET['bulan'];
                        }
                        else{
                            $from = date('Y/m/d');
                        }
                        ?>
                        <select id="bulan" name="bulan" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['bulan']; ?>">- <?php echo lang('Pilih Bulan'); ?> -</option>
                            <?php
                            $bln=array(01=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
                            //echo $bln;
                            $from='';
                            for ($bulan=01; $bulan<=12; $bulan++) { 
                                 if ($_GET['bulan'] == $bulan){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$bulan' $ck>$bln[$bulan]</option>";
                            }
                            ?>
                        </select>
                </div>

                <div class="input-field">
                    <?php
                        if(isset($_GET['tahun'])){
                            $to = $_GET['tahun'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>

                        <select id="tahun" name="tahun" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['tahun']; ?>">- <?php echo lang('Pilih Tahun'); ?> -</option>
                            <?php
                            $now=date("Y");
                            for ($tahun=2017; $tahun<=$now; $tahun++) { 
                                 if ($_GET['tahun'] == $tahun){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$tahun' $ck>$tahun</option>"; 
                            }

                            ?>
                        </select>
                </div>

                <div class="input-field">
                    <select id="akun" name="akun" class="browser-default">
                        <option value=''>- <?php echo lang('Pilih Lokasi'); ?> -</option>
                            <?php
                            //$julsql   = "select * from [gateway].[dbo].[EDCList] where SUBSTRING(UserIDBaronang,0,6) = SUBSTRING('700080',0,6)";
                            $julsql   = "select distinct (LocationID) from [dbo].[MemberLocation] where status = '1' order by LocationID asc";
                            //echo $julsql1;
                            $julstmt = sqlsrv_query($conn, $julsql);

                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                $carmemb = "select * from dbo.LocationMerchant where LocationID = '$rjulrow[0]'";
                                $procmemb = sqlsrv_query($conn, $carmemb);
                                $hasilmemb  = sqlsrv_fetch_array( $procmemb, SQLSRV_FETCH_NUMERIC);

                               ?>
                            <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_GET['akun']){echo "selected";} ?>>&nbsp;<?=$hasilmemb[0];?>&nbsp;<?=$hasilmemb[2];?></option>
                            <?php } ?>
                    </select>
                </div>


                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_pembelian.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
            </form>

        <?php if(isset($_GET['bulan']) and isset($_GET['tahun']) and isset($_GET['akun'])){
            //echo $_POST['bulan'];
            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
            $from = date('Y-m-01 00:00:00.000', strtotime($from1));
            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
            $to = date('Y-m-t 23:59:59.999', strtotime($to3));
            $to1 = date('m', strtotime($to3));
            //echo $to1;
            $to2 = date('Y', strtotime($to3));
            $date = date('Y-m-d');
            //echo $date;
            ?>
            

            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Laporan List Pembelian Produk Member'); ?></h3>
                </tr>
            </div>
            <div class="box-header " align="Center">
                <tr>
                    <h3 class="box-title" align="center"><?php echo $_SESSION['NamaKoperasi']; ?></h3>
                </tr>
                </div>   


            <div class="box-header" align="Center">
                    <tr>
                        <?php
                            $bulan = date('m', strtotime($to3));
                            $tahun = date('Y', strtotime($to3));
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        $bulan = 'Desember';
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
    
                        <h3 class="box-title"><?php echo "Periode : ",$bulan,"  - ",$tahun," ",$bulan2," ",$tahun2; ?></h3>
                    </tr>
                <div class="box-header" align="center" >
                    <?php
                    $lok = "select * from dbo.LocationMerchant where LocationID = '$_GET[akun]'";
                    $proloc = sqlsrv_query($conn,$lok);
                    $hasloc = sqlsrv_fetch_array($proloc, SQLSRV_FETCH_NUMERIC);
                    ?>
                <tr>
                    <h3 class="" align="center"><?php echo "Lokasi : " ,$hasloc[2]; ?></h3>
                </tr>
                </div>


                <?php 

                $disable = "readonly";
                $nm = 1;
                $x = "select distinct(VehicleID) from dbo.MemberLocation where LocationID = '$_GET[akun]' order by VehicleID asc";
                    //echo $x;
                $y = sqlsrv_query($conn, $x);
                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                    
                    $cekedc  = "select * from dbo.VehicleType where VehicleID = '$z[0]'";
                    //echo $cekedc;
                    $proedc = sqlsrv_query($conn, $cekedc);
                    $hasedc = sqlsrv_fetch_array($proedc, SQLSRV_FETCH_NUMERIC);


                    $jumsum = "SELECT sum(amount) FROM ( SELECT a.accountdebet, a.accountkredit, a.date, a.amount, b.validdate, b.vehicleID, c.name, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.memberlocation b on a.accountdebet = b.cardno join dbo.VehicleType c on b.VehicleID = c.vehicleID where date between '$from' and '$to' and a.accountKredit = '$_GET[akun]' and a.transactiontype = 'MBRS' and b.vehicleID = '$hasedc[0]') a";
                    //echo $jumsum;
                    $pjumsum = sqlsrv_query($conn, $jumsum);
                    $hsjumsum = sqlsrv_fetch_array($pjumsum, SQLSRV_FETCH_NUMERIC);


                ?>
                <div class="box-header " align="left" style="margin-top: 30px;">
                <tr>
                    <h5 class="box-title" align="left"><?php echo ('Jenis Kendaraan : '); ?> <?php echo $nm; ?>. <?php echo $hasedc[1]; ?></h5  >
                </tr>
                <div class="active">
                    <label ><?php echo lang('Total Pembelian'); ?></label>
                    <input type="text" name="Totalbeli" class="validate" id="" value="<?php echo number_format($hsjumsum[0],2); ?>" <?php echo $disable; ?>>   
                </div>
                </div>
                <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                    <a href="drep_pembelian.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&akun=<?php echo $_GET['akun']; ?>&kop=<?php echo $hasedc[1];?>&vehicle=<?php echo $z[0]; ?>&total=<?php echo $hsjumsum[0]; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
                </div>    
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo 'No.'; ?></th>
                                    <th><?php echo 'No. Akun'; ?></th>
                                    <th><?php echo 'Tanggal '; ?></th>
                                    <th><?php echo 'validdate'; ?></th>
                                    <th><?php echo 'Amount'; ?></th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            //count
                            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $from = date('Y-m-01 00:00:00.000', strtotime($from1));
                            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $to = date('Y-m-t 23:59:59.999', strtotime($to3));
                            $akun = $_GET['akun'];
                            $jmlulsql   = "SELECT count(*) FROM ( SELECT a.accountdebet, a.accountkredit, a.date, a.amount, b.validdate, b.vehicleID, c.name, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.memberlocation b on a.accountdebet = b.cardno join dbo.VehicleType c on b.VehicleID = c.vehicleID where date between '$from' and '$to' and a.accountKredit = '$_GET[akun]' and a.transactiontype = 'MBRS' and b.vehicleID = '$hasedc[0]') a";
                            //echo $jmlulsql;
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }
                           


                                $a = "SELECT * FROM ( SELECT a.accountdebet, a.accountkredit, a.date, a.amount, b.validdate, b.vehicleID, c.name, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.memberlocation b on a.accountdebet = b.cardno join dbo.VehicleType c on b.VehicleID = c.vehicleID where date between '$from' and '$to' and a.accountKredit = '$_GET[akun]' and a.transactiontype = 'MBRS' and b.vehicleID = '$hasedc[0]') a WHERE row between '$posisi' and '$batas'";
                                //echo $a;
                                $b = sqlsrv_query($conn, $a);
                                //echo $b;
                               
                                
                                //var_dump($c);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                               
                                    ?>                 
                            <tr>   
                                <td><?php echo $c[7]; ?></a></td>
                                <td><?php echo $c[0]; ?></a></td>
                                <td><?php echo $c[2]->format('Y-m-d H:i:s'); ?></td>
                                <td><?php echo $c[4]->format('Y-m-d'); ?></td>
                                <td style="text-align: right;"><?php echo number_format($c[3],2) ; ?></a></td>
                                
                            </tr>
                            
                            <?php $no3++; $jmlhalaman = ceil($jmlulrow[0]/$perpages); } ?>
                            </tbody>
                        </table>
                        <div class="box-footer clearfix right">
                            <div style="text-align: center;">
                                Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                                <?php
                                $akun = $_GET['akun'];
                                $bulan = $_GET['bulan'];
                                $tahun = $_GET['tahun'];
                                $reload = "rep_pembelian.php?akun=$akun&bulan=$bulan&tahun=$tahun";
                                $page = intval($_GET["page"]);
                                $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                                if( $page == 0 ) $page = 1;

                                $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                ?>
                            </div>
                        </div>    
                    </div>
                </div>    
                          
                <?php $nm++;} ?>
                                        
            <!-- di dekat tanggal -->
            </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>


<script type="text/javascript">
    var tampung = [];

     $('.btn-addu').click(function(){
       var index = $('#detailUser tr').length;

        var user = $('#user option:selected').val();
        var amount = $('#amount').val();
        var date = $('#date').val();

        if(date == ''){
          alert('Assign date tidak boleh kosong');
          return false;
        }
        else if(user == '' || amount == '' || amount <= 0){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(user, tampung) !== -1){
            alert('Tidak dapat memilih user yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addteller.php",
                type : 'POST',
                dataType : 'json',
                data: { index: index, user: user, amount: amount, date: date},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.id);
                        $("#detailUser").append("<tr>" +
                            "<td><input type='hidden' name='user["+data.index+"]' value="+ data.id +">" + data.nama + "</td>" +
                            "<td><input type='hidden' name='amount["+data.index+"]' value="+ data.amount +">" + data.amount +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='ion-android-delete'></i></a></td>" +
                            "</tr>");

                            $('#user').val('');
                            $('#amount').val('');
                    }
                    else{
                        alert(data.message);
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });
</script>

<?php require('footer.php'); ?>
