<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";

$a = "select * from [dbo].[MemberList]";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);

$aa = "select * from [dbo].[RegularSavingType]";
$bb = sqlsrv_query($conn, $aa);
$cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);

if($c == null){
    messageAlert('Member belum di set. Tidak dapat mendownload template.','warning');
    header('Location: mreg_balance.php');
}
else if($cc == null){
    messageAlert('Regular saving type belum di set. Tidak dapat mendownload template.','warning');
    header('Location: mreg_balance.php');
}
else{

    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Data Upload RS Balance");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 3
    $objWorkSheet3 = $objPHPExcel->createSheet(1);
// baris judul
    $objWorkSheet3->SetCellValue('A1', 'No');
    $objWorkSheet3->SetCellValue('B1', 'Product Name');
    $objWorkSheet3->SetCellValue('C1', 'Interest Type');
    $objWorkSheet3->SetCellValue('D1', 'Interest Rate %');
    $objWorkSheet3->SetCellValue('E1', 'Skema Bunga');
    $objWorkSheet3->SetCellValue('F1', 'Minimum Balance');
    $objWorkSheet3->SetCellValue('G1', 'Maximum Balance');
    $objWorkSheet3->SetCellValue('H1', 'Admin Fee');
    $objWorkSheet3->SetCellValue('I1', 'Closing Fee');

    $no = 1;
    $row = 2;

    $x = "select * from [dbo].[RegularSavingType]";
    $y = sqlsrv_query($conn, $x);
    $type = '';
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        array_push($listCombo, $z[1]);

        $objWorkSheet3->SetCellValue("A".$row, $no);
        $objWorkSheet3->SetCellValue("B".$row, $z[1]);

        if($z[2] == 0){
            $type = 'Saldo harian';
        }
        else if($z[2] == 1){
            $type = 'Saldo rata-rata';
        }
        else{
            $type = 'Saldo terendah';
        }

        $objWorkSheet3->SetCellValue("C".$row, $type);

        $objWorkSheet3->SetCellValue("D".$row, $z[3]);
        $objWorkSheet3->SetCellValue("E".$row, $z[8]);
        $objWorkSheet3->SetCellValue("F".$row, number_format($z[4]));
        $objWorkSheet3->SetCellValue("G".$row, number_format($z[5]));
        $objWorkSheet3->SetCellValue("H".$row, number_format($z[6]));
        $objWorkSheet3->SetCellValue("I".$row, number_format($z[9]));

        $no++;
        $row++;
    }
    $objWorkSheet3->setTitle('Data Master Produk RS');

    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'Nama');
    $objWorkSheet->SetCellValue('B1', 'NIP');
    $objWorkSheet->SetCellValue('C1', 'KTP');
    $objWorkSheet->SetCellValue('D1', 'Email');
    $objWorkSheet->SetCellValue('E1', 'Telepon');
    $objWorkSheet->SetCellValue('F1', 'Nama Produk');
    $objWorkSheet->SetCellValue('G1', 'Nomor Rekening Lama');
    $objWorkSheet->SetCellValue('H1', 'Balance');

    //looping isi combo box
    $row = 2;
    for($no=1;$no<=100;$no++){
        $combo = $objWorkSheet->GetCell("F".$row)->getDataValidation();
        $combo->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
        $combo->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
        $combo->setAllowBlank(false);
        $combo->setShowInputMessage(true);
        $combo->setShowErrorMessage(true);
        $combo->setShowDropDown(true);
        $combo->setErrorTitle('Input error');
        $combo->setError('Value is not in list.');
        $combo->setPromptTitle('Pick from list');
        $combo->setPrompt('Please pick a value from the drop-down list.');
        $combo->setFormula1('"'.implode(',',$listCombo).'"');

        $row++;
    }

    $objWorkSheet->setTitle('Data Upload RS Balance');

//sheet 2
    $objWorkSheet2 = $objPHPExcel->createSheet(2);
// baris judul
    $objWorkSheet2->SetCellValue('A1', 'Member ID');
    $objWorkSheet2->SetCellValue('B1', 'No. KTP');
    $objWorkSheet2->SetCellValue('C1', 'NIP');
    $objWorkSheet2->SetCellValue('D1', 'Nama');
    $objWorkSheet2->SetCellValue('E1', 'Alamat');
    $objWorkSheet2->SetCellValue('F1', 'Telepon');
    $objWorkSheet2->SetCellValue('G1', 'Email');

    $no = 1;
    $row = 2;

    $x = "select * from [dbo].[MemberList] where StatusMember = 1 order by MemberID asc";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        $objWorkSheet2->SetCellValueExplicit("A".$row, $z[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValueExplicit("B".$row, $z[7], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("C".$row, $z[8]);
        $objWorkSheet2->SetCellValue("D".$row, $z[2]);
        $objWorkSheet2->SetCellValue("E".$row, $z[3]);
        $objWorkSheet2->SetCellValueExplicit("F".$row, $z[4], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("G".$row, $z[5]);

        $no++;
        $row++;
    }
    $objWorkSheet2->setTitle('Data Master Member');

    $fileName = 'templateRegularSavingBalance'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

    // download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;
}
?>