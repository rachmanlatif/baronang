<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="static/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=$NameUser?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <div></div>
        <ul class="sidebar-menu">
            <li class="header">MAIN MENU</li>
            <li class="<?=$master?> treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Master</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?=$linkgenset?>"><a href="mgeneralsetting.php"><i class="fa fa-circle-o"></i> General Settings</a></li>
                    <li class="<?=$linkulev?>"><a href="muserlevel.php"><i class="fa fa-circle-o"></i> User Level</a></li>
                    <li class="<?=$linkuacc?>"><a href="museraccount.php"><i class="fa fa-circle-o"></i> User Account</a></li>
                    <li class="<?=$linkbanklist?>"><a href="bank_list.php"><i class="fa fa-circle-o"></i> Bank List</a></li>
                    <li class="<?=$linkbankacc?>"><a href="bank_acc.php"><i class="fa fa-circle-o"></i> Bank Account</a></li>
                    <li class="<?=$linkpos?>"><a href="mposition.php"><i class="fa fa-circle-o"></i> Position</a></li>
                </ul>
            </li>

            <li class="<?=$product?> treeview">
                <a href="#">
                    <i class="fa fa-list"></i> <span>Product</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?=$linkbasavty?>"><a href="bas_sav_type.php"><i class="fa fa-circle-o"></i> Basic Saving Type</a></li>
                    <li class="<?=$linkregsavty?>"><a href="reg_sav_type.php"><i class="fa fa-circle-o"></i> Regular Saving Type</a></li>
                    <li class="<?=$linkdeptipe?>"><a href="time_dep_type.php"><i class="fa fa-circle-o"></i> Time Deposit Type</a></li>
                    <li class="<?=$linkloantype?>"><a href="loan_type.php"><i class="fa fa-circle-o"></i> Loan Type</a></li>
                </ul>
            </li>

            <li class="<?=$cuser?> treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Customer Service</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">

                    <li class="<?=$oa?>">
                        <a href="#"><i class="fa fa-circle-o"></i> Open Account
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?=$linkomemlist;?>"><a href="csoamemberlist.php"><i class="fa fa-circle-o"></i> Member List</a></li>
                            <li class="<?=$linkoregsav;?>"><a href="csoaregsaving.php"><i class="fa fa-circle-o"></i> Regular Saving</a></li>
                            <li class="<?=$linkotidep;?>"><a href="csoatimedeposit.php"><i class="fa fa-circle-o"></i> Time Deposit</a></li>
                            <li class="<?=$linkoloanapp;?>"><a href="csoaloanapp.php"><i class="fa fa-circle-o"></i> Loan Application</a></li>
                        </ul>
                    </li>

                    <li class="<?=$ca?>">
                        <a href="#"><i class="fa fa-circle-o"></i> Close Account
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?=$linkcscamb;?>"><a href="cscamember.php"><i class="fa fa-circle-o"></i> Member</a></li>
                            <li class="<?=$lincregsav;?>"><a href="cscaregsaving.php"><i class="fa fa-circle-o"></i> Regular Saving</a></li>
                            <li class="<?=$linkctidep;?>"><a href="cscatimedeposit.php"><i class="fa fa-circle-o"></i> Time Deposit</a></li>
                            <li class=""><a href="csloanclose.php"><i class="fa fa-circle-o"></i> Loan</a></li>
                        </ul>
                    </li>

                </ul>
            </li>

            <li class="<?=$cas?> treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i> <span>Cashier</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">

                    <li class="<?=$dep?>">
                        <a href="#"><i class="fa fa-circle-o"></i> Deposit
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?=$linkdbasav?>"><a href="dep_bas_sav.php"><i class="fa fa-circle-o"></i> Basic Saving</a></li>
                            <li class="<?=$linkdregsav?>"><a href="dep_reg_sav.php"><i class="fa fa-circle-o"></i> Regular Saving</a></li>
                            <li class="<?=$linkdloapay?>"><a href="dep_loan_pay.php"><i class="fa fa-circle-o"></i> Loan Payment</a></li>

                        </ul>
                    </li>

                    <li class="<?=$wd?>">
                        <a href="#"><i class="fa fa-circle-o"></i> Withdrawal
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?=$linkwdregsav?>"><a href="with_reg_sav.php"><i class="fa fa-circle-o"></i> Regular Saving</a></li>

                        </ul>
                    </li>

                </ul>
            </li>
            <li class="<?=$ln?> treeview">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Loan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?=$linkloanchat?>"><a href="loan_chat.php"><i class="fa fa-circle-o"></i> Loan Chat</a></li>
                    <li class="<?=$linkloanapp?>"><a href="loan_app.php"><i class="fa fa-circle-o"></i> Loan Approval</a></li>
                    <li class="<?=$linkloanrel?>"><a href="loan_rel.php"><i class="fa fa-circle-o"></i> Loan Release</a></li>
                </ul>
            </li>
            <li class="<?=$re?> treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i> <span>Report</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?=$linkrepmem?>"><a href="rep_list.php"><i class="fa fa-circle-o"></i> Report Member List </a></li>
                    <li class="<?=$linkrepbas?>"><a href="rep_bas.php"><i class="fa fa-circle-o"></i> Report Basic Saving </a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
