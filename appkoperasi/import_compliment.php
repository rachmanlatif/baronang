<?php
@session_start();
error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
?>

	<div class="animated fadeinup delay-1">
        <div class="page-content txt-black">

        	<h1 class="uppercase txt-black">Import Registrasi Compliment</h1><br>

        	<form action="" method="post" enctype="multipart/form-data">
                <div class="m-t-30">
        		
            		<div class="">
            			<span>Pilih Lokasi</span> : <br>
            			<select class="browser-default" name="loc">
            				<?php
            				$sloc = "SELECT * from [dbo].[LocationMerchant] order by Nama ASC";
            				$qloc = sqlsrv_query($conn, $sloc);
            				while($dloc = sqlsrv_fetch_array($qloc)) { ?>
            					<option value="<?php echo $dloc[0]; ?>"><?php echo $dloc[2]; ?></option>
            					<?php
            				}
            				?>
            			</select>
            		</div>

            		<div class="m-t-30">
            			<span>Exp Date</span> : <br>
            			<input type="date" name="expdate">
            		</div>

            		<div class="m-t-30">
            			<span>Vehicle Type</span> : <br>
            			<select class="browser-default" name="vhc">
            				<?php
            				$svhc = "SELECT * from [dbo].[VehicleType] order by Name ASC";
            				$qvhc = sqlsrv_query($conn, $svhc);
            				while($dvhc = sqlsrv_fetch_array($qvhc)) { ?>
            					<option value="<?php echo $dvhc[0]; ?>"><?php echo $dvhc[1]; ?></option>
            					<?php
            				}
            				?>
            			</select>
            		</div>

                    <div class="m-t-30">
                        <span>Upload File</span> <br>
                        <input type="file" name="fxcl" accept=".xls">
                    </div>

            	</div>

            	<div class="m-t-30">
            		<button name="ok" class="btn btn-large width-100 waves-effect waves-light primary-color">import</button>
            	</div>

            </form>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="4" style="text-align: center;">Format Excel</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <th>QR</th>
                            <th>Member ID</th>
                            <th>Nomor Kartu</th>
                            <th>User ID</th>
                        </tr>
                    </tbody>
                </table>
            </div>

            <?php
            if(isset($_POST['ok'])) {
                include "excel_reader2.php";

                $loc = @$_POST['loc'];
                $tgl = @$_POST['expdate'];
                $vhl = @$_POST['vhc'];

                $target = basename($_FILES['fxcl']['name']) ;
                move_uploaded_file($_FILES['fxcl']['tmp_name'], $target);
                chmod($_FILES['fxcl']['name'],0777);

                $data = new Spreadsheet_Excel_Reader($_FILES['fxcl']['name'],false);
                $jumlah_baris = $data->rowcount($sheet_index=0);
                // jumlah default data yang berhasil di import

                $sl1 = "SELECT RegistrasiComplimentID from [dbo].[RegistrasiCompliment] Order By RegistrasiComplimentID DESC";
                $ql1 = sqlsrv_query($conn, $sl1);
                $dl1 = sqlsrv_fetch_array($ql1);

                $gl1 = substr($dl1[0], -3);
                $gdl1 = (int)$gl1 + 1;
                $itdl1 = str_pad($gdl1, 3, '0', STR_PAD_LEFT);
                $idl1 = '700097REGCMP'.date('dm').$itdl1;

                $tgnow = date('Y-m-d H:i:s');
                $dr = date('Y-m-d').' 00:00:00';
                $jml = $jumlah_baris - 1;

                $s1 = "INSERT into [dbo].[RegistrasiCompliment](RegistrasiComplimentID, LocationID, VehicleID, ExpireDate, Quantity, DateRegister, DateReceipt) 
                        values('$idl1', '$loc', '$vhl', '$tgl', '$jml', '$tgnow', '$dr')";
                echo $s1.'<br><br>';
                //$q1 = sqlsrv_query($conn, $s1);

                $berhasil = 0;
                $sl2 = "SELECT AktivasiComplimentID from [dbo].[AktivasiCompliment] Order By AktivasiComplimentID DESC";
                $ql2 = sqlsrv_query($conn, $sl2);
                $dl2 = sqlsrv_fetch_array($ql2);

                $gl2 = substr($dl2[0], -6);
                //$gdl2 = (int)$gl2 + 1;
                //$itdl2 = str_pad($gdl2, 0, '0', STR_PAD_LEFT);
                //$idl2 = '700097CMT'.$itdl2;
                $idlr2 = '700097CMT'.date('dm');
                $iidl2 = 1;

                //$itdl2 = str_pad($iidl2, 3, '0', STR_PAD_LEFT);
                $iidl2 = (int)$gl2 + 1;

                for ($i=2; $i<=$jumlah_baris; $i++){
                    // menangkap data dan memasukkan ke variabel sesuai dengan kolumnya masing-masing
                    $qr = $data->val($i, 1);
                    if($qr != ""){
                        // input data ke database (table data_pegawai)
                        //mysqli_query($koneksi,"INSERT into data_pegawai values('','$nama','$alamat','$telepon')");
                       
                        $idl2 = $idlr2.$iidl2;
                        $s2 = "INSERT into [dbo].[AktivasiCompliment] values('$idl2', '$qr', '', '$idl1')";
                        //echo $i.'-1). '.$s2.'<br>';
                        $q2 = sqlsrv_query($conn, $s2);

                        $s3 = "INSERT into [dbo].[MemberLocation] values('".$data->val($i, 2)."', '$loc', 1, '$tgnow', '$tgl', '".$data->val($i, 4)."', '$vhl', '".$data->val($i, 3)."')";
                        //echo  $i.'-2). '.$s3.'<br>';
                        $q3 = sqlsrv_query($conn, $s3);

                        $s4 = "INSERT into [dbo].[MemberLocationMonth] values('".$data->val($i, 3)."', '".date('m', strtotime($tgl))."', 1, '$vhl', '$loc', '', '".date('Y', strtotime($tgl))."')";
                        //echo  $i.'-3). '.$s4.'<br><br>';
                        $q4 = sqlsrv_query($conn, $s4);

                        $berhasil++; $iidl2++;
                    }
                }
            }
            ?>

        </div>
    </div>

<?php require('footer.php'); ?>