<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
require_once 'lib/phpExcel/Classes/PHPExcel/IOFactory.php';
include "connect.php";

if(isset($_GET['delete'])){
    $a = "delete from dbo.GroupAcc";
    $b = sqlsrv_query($conn, $a);

    $c = "delete from dbo.TypeAcc";
    $d = sqlsrv_query($conn, $c);

    $e = "delete from dbo.Account";
    $f = sqlsrv_query($conn, $e);

    if($b and $d and $f){
        messageAlert('Berhasil menghapus seluruh data coa dari database','success');
        header('Location: coa.php');
    }
    else{
        messageAlert('Gagal menghapus seluruh data coa dari database','danger');
        header('Location: coa.php');
    }
}

if(isset($_FILES['filename'])) {
    $uploads_dir = 'uploads/excel/';
    $tmp_name = $_FILES["filename"]["tmp_name"];
    $path = $uploads_dir . basename($_FILES["filename"]["name"]);

    if(move_uploaded_file($tmp_name, $path)){
        //upload data from excel
        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getSheet(0);
        } catch(Exception $e) {
            messageAlert('Failed reading excel','warning');
            header('Location: coa.php');
        }

        $highestRow = $data->getHighestRow();
        $highestColumn = $data->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        if(!($highestRow -1) >0)
        {
            messageAlert('No data found in Sheet 0 or Sheet not available.','warning');
            header('Location: coa.php');
        }

        try {
            $filePath = "uploads/excel/";

            //cek header
            $rowHeader = $data->rangeToArray('A1:'.$highestColumn.'1',NULL,TRUE,FALSE);
            $hacc = $rowHeader[0][0];
            $hname = $rowHeader[0][5];
            $hcat = $rowHeader[0][6];

            //filter jika format tidak sesuai
            if($hacc != 'Account Code' or $hname != 'Account Name' or $hcat != 'Category Account'){
                messageAlert('Format template tidak sesuai','info');
                header('Location: coa.php');
            }

            //buat file excel jika ada yang gagal upload
            $objPHPExcel2 = new PHPExcel();
            $objPHPExcel2->getProperties()->setCreator("baronang");
            $objPHPExcel2->getProperties()->setTitle("Failed Upload Coa");

            // set autowidth
            for($col = 'A'; $col !== 'Z'; $col++) {
                $objPHPExcel2->getActiveSheet()
                    ->getColumnDimension($col)
                    ->setAutoSize(true);
            }

            //sheet 1
            $objWorkSheet2 = $objPHPExcel2->createSheet(0);
            // baris judul
            $objWorkSheet2->SetCellValue('A1', 'Account Code');
            $objWorkSheet2->mergeCells('A1:E1');

            $objWorkSheet2->SetCellValue('A2', 'Header 1');
            $objWorkSheet2->SetCellValue('B2', 'Header 2');
            $objWorkSheet2->SetCellValue('C2', 'Header 3');
            $objWorkSheet2->SetCellValue('D2', 'Header 4');
            $objWorkSheet2->SetCellValue('E2', 'Header 5');

            $objWorkSheet2->SetCellValue('F1', 'Account Name');
            $objWorkSheet2->mergeCells('F1:F2');

            $objWorkSheet2->SetCellValue('G1', 'Category Account');
            $objWorkSheet2->mergeCells('G1:G2');

            $no = 1;
            $baris = 3;
            //baca dari row 3
            $arr = array();

            for($row=3; $row<=$highestRow; ++$row) {

                $rowData = $data->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

                $header1 = $rowData[0][0];
                $header2 = $rowData[0][1];
                $header3 = $rowData[0][2];
                $header4 = $rowData[0][3];
                $header5 = $rowData[0][4];
                $name = $rowData[0][5];
                $category = $rowData[0][6];

                if($header1 != null and $header2 == null){
                    $a = "exec [dbo].[ProsesGroupAcc] '$header1','$name','',0";
                    $b = sqlsrv_query($conn, $a);
                }

                if($header1 != null and $header2 != null){
                    $a = "exec [dbo].[ProsesTypeAcc] '$header2','$name','$header1',''";
                    $b = sqlsrv_query($conn, $a);
                }

                if($header2 != null and $header3 != null){
                    $header = 0;
                    $sql = "select * from dbo.TypeAcc where NamaTipe='$category'";
                    $stmt = sqlsrv_query($conn, $sql);
                    $qwe = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                    if($qwe != null){
                        $a = "exec [dbo].[ProsesAccount] '$header3','$name','$qwe[0]',$header,'',1,'$header3','$name'";
                        $b = sqlsrv_query($conn, $a);
                    }
                }

                if($header3 != null and $header4 != null){
                    $header = 1;
                    $sql = "select * from dbo.TypeAcc where NamaTipe='$category'";
                    $stmt = sqlsrv_query($conn, $sql);
                    $qwe = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                    if($qwe != null){
                        $a = "exec [dbo].[ProsesAccount] '$header4','$name','$qwe[0]',$header,'',1,'$header4','$name'";
                        $b = sqlsrv_query($conn, $a);
                    }
                }

                if($header4 != null and $header5 != null){
                    $header = 2;
                    $sql = "select * from dbo.TypeAcc where NamaTipe='$category'";
                    $stmt = sqlsrv_query($conn, $sql);
                    $qwe = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                    if($qwe != null){
                        $a = "exec [dbo].[ProsesAccount] '$header5','$name','$qwe[0]',$header,'',1,'$header5','$name'";
                        $b = sqlsrv_query($conn, $a);
                    }
                }
            }

            if($arr[0] != ""){
                $objWorkSheet2->setTitle('Failed Upload Coa');

                $fileName = 'failedUploadCoa'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel2,'Excel5');

                // download ke client
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="'.$fileName.'"');
                $objWriter->save('php://output');

                return $filePath.'/'.$fileName;
            }
            else{
                messageAlert('Berhasil menyimpan ke database','success');
                header('Location: coa.php');
            }

        } catch(Exception $e) {
            messageAlert('Gagal upload excel','danger');
            header('Location: coa.php');
        }
    }
    else{
        messageAlert('Gagal upload file','danger');
        header('Location: coa.php');
    }
}

?>