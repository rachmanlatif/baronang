<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

 <div class="page-content">






<?php if(isset($_GET['trans']) ){ ?>
    <div>
        <a href="rep_tcardlist.php?pagem=<?php echo $_GET['pagem']; ?>&lok=<?php echo $_GET['lok']; ?>&tgl1=<?php echo $_GET['tgl1']; ?>&tgl2=<?php echo $_GET['tgl2']; ?>&trans=<?php echo $_GET['trans']; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back</button></a>

        <!-- <a href="drep_neracaHISAK.php?from=<?php echo $_GET['tgl1']; ?>&to=<?php echo $_GET['tgl2']; ?>&acc2=<?php echo $_GET['trx']; ?>&st=4"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Download To Excel</button></a>        -->
    </div>
     

    <div class="box-header" align="center" style="margin-top: 30px;">
        <tr>
            <h3 class="box-title" align="center"><?php echo lang('Detail History Transaksi'); ?></h3>
        </tr>
    </div>

    <div class="box-header " align="Center">
        <tr>
            <h3 class="box-title" align="center"><?php echo ('No. Transaksi : '); ?> <?php echo $_GET['trans']; ?></h3>
        </tr>
    </div>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th><?php echo lang('Kode Jurnal'); ?></th>
                <th><?php echo lang('Nomor Akun'); ?></th>
                <th><?php echo lang('Kode Akun'); ?></th>
                <th><?php echo lang('Nama Akun'); ?></th>
                <th><?php echo lang('Debit'); ?></th>
                <th><?php echo lang('Kredit'); ?></th>
                <th><?php echo lang('Deskripsi'); ?></th>
            </tr>
        </thead>

        <?php
        if ($_GET['type'] == 'TOPP' || $_GET['type'] == 'MBRS'){
            $tglh = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($_GET['tgl2'])));
            $tglk = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($_GET['tgl1'])));
            $trans = $_GET['trans'];
            $lok = $_GET['lok'];
            $aaa = "SELECT * FROM ( SELECT a.TransactionNumber, a.AccNo, b.AccountDebet, b.AccountKredit, b.debet, b.kredit, b.Amount, b.note, ROW_NUMBER() OVER (ORDER BY timestam asc) as row FROM [dbo].[parkingtrans] a inner join [dbo].[translist] b on a.TransactionNUmber = b.Transno where a.TransactionNumber = '$_GET[trans]'  and timestam between '$tglk' and '$tglh' ) a";
            //echo $aaa;
            $bbb = sqlsrv_query($conn, $aaa);
            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
            $ndebit = 0;
            $nkredit = 0;    
                if ($ccc[1] == $ccc[2]) {
                    $hakun = $ccc[4];
                    $ndebit = $ccc[6];
                    $nkredit = 0;
                } else {
                    $hakun = $ccc[5];
                    $nkredit = $ccc[6];
                    $ndebit = 0;
                                    
                }
            ?>
                <tr>
                    <td><?php echo $ccc[8]; ?></td>
                    <td><?php echo $ccc[0]; ?></td>
                    <td><?php echo $ccc[1]; ?></td>
                    <td><?php echo $hakun; ?></td>
                    <td><?php 
                        $sql   = "select * from dbo.Account where KodeAccount='$hakun'";
                        $stmt  = sqlsrv_query($conn, $sql);
                        $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                        $acc = $row[1];
                            echo $acc; ?></td>
                    <td class="kanan"><?php echo number_format($ndebit); ?></td>
                    <td class="kanan"><?php echo number_format($nkredit); ?></td>
                    <td><?php echo $ccc[7]; ?></td>
                </tr>
            <?php } ?>
            <?php } else {
    
            $tglh = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($_GET['tgl2'])));
            $tglk = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($_GET['tgl1'])));
            $trans = $_GET['trans'];
            $lok = $_GET['lok'];
            $aaa = "SELECT * FROM ( SELECT a.TransactionNumber, a.AccNo, b.AccountDebet, b.AccountKredit, b.debet, b.kredit, b.Amount, b.note,a.timestam, ROW_NUMBER() OVER (ORDER BY timestam asc) as row FROM [dbo].[parkingtrans] a inner join [dbo].[translist] b on a.TransactionNUmber = b.Transno where a.TransactionNumber = '$trans' and a.AccNo = '$lok' and timestam between '$tglk' and '$tglh' and a.AccNo = b.AccountDebet or a.TransactionNumber = '$trans' and a.AccNo = '$lok' and timestam between '$tglk' and '$tglh' and a.AccNo = b.AccountKredit) a";
            //echo $aaa;
            $bbb = sqlsrv_query($conn, $aaa);
            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
            $ndebit = 0;
            $nkredit = 0;    
                if ($ccc[1] == $ccc[2]) {
                    $hakun = $ccc[4];
                    $ndebit = $ccc[6];
                    $nkredit = 0;
                } else {
                    $hakun = $ccc[5];
                    $nkredit = $ccc[6];
                    $ndebit = 0;
                                    
                }
            
            
            ?>
                <tr>
                    <td><?php echo $ccc[9]; ?></td>
                    <td><?php echo $ccc[0]; ?></td>
                    <td><?php echo $ccc[1]; ?></td>
                    <td><?php echo $hakun; ?></td>
                    <td><?php 
                        $sql   = "select * from dbo.Account where KodeAccount='$hakun'";
                        $stmt  = sqlsrv_query($conn, $sql);
                        $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                        $acc = $row[1];
                            echo $acc; ?></td>
                    <td class="kanan"><?php echo number_format($ndebit); ?></td>
                    <td class="kanan"><?php echo number_format($nkredit); ?></td>
                    <td><?php echo $ccc[7]; ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
    <?php } ?>
    <?php } ?>


<?php require('footer_new.php');?>