<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php include "connectuser.php"; ?>

<?php include "connectinti.php"; ?>

	<div class="animated fadeinup delay-1">
		<div class="page-content">

			<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
					<div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
							<h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
							<?php echo $_SESSION['error-message']; ?>
					</div>
			<?php } ?>

			<form id="siden" action="" method="post">
				Scan atau Masukan nomor kartu
				<br>
				<input type="text" name="result-code" value="<?php echo $_REQUEST['barcode']; ?>" id="result-code" class="validate <?php echo $hide; ?>">
			</form>


			<div class="row">
				<div class="col s12">
					<button type="button" id="getdata" class="btn btn-large primary-color width-100 waves-effect waves-light">Lihat Data</button>
				</div>
			</div>
			<div class="row">
				<div class="col s12">
					<button type="button" id="ocam" class="btn btn-large primary-color width-100 waves-effect waves-light">Scan QR</button>
				</div>
			</div>

			<div class="box box-solid hide" id="form-camera">
	            <h3 class="uppercase">Scan QR Code/Barcode</h3><br>

	            <b>Pilih Kamera</b>
	            <select class="browser-default" id="camera-select"></select>
	            <br>
	            <div class="box-body">
	                <div class="row">
	                    <div class="col">
	                        <button title="Start Scan QR Barcode" class="waves-effect waves-light btn-large primary-color width-100" id="play" type="button">Start</button>
	                    </div>
	                    <div class="col">
	                        <button title="Stop streams" class="waves-effect waves-light btn-large primary-color width-100" id="stop" type="button">Stop</button>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="col-xs-12 col-sm-6">
	                        <div class="well" style="position: relative;display: inline-block;">
	                            <canvas id="webcodecam-canvas"></canvas>
	                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
	                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
	                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
	                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
	                        </div>
	                    </div>
	                    <div class="col-xs-12 col-sm-6">
	                        <div class="thumbnail" id="result">
	                            <img id="scanned-img" style="width: 100%;" src="">
	                            <div class="caption">
	                                <p class="text-center" id="scanned-QR"></p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>

	        <?php if($_SESSION['device'] == 0){ ?>
	            <script type="text/javascript">
	                $('#ocam').click(function(){
	                    Intent.openActivity("QRActivity","cekmember_parking.php");
	                  });
	            </script>
	        <?php } ?>

	        <?php if($_SESSION['device'] == 1){ ?>
	            <script type="text/javascript">
	                $('#ocam').click(function(){
	                    $('#result-code').val();
	                    $('#form-camera').removeClass('hide');
	                });

	                $('#stop').click(function(){
	                    $('#form-camera').addClass('hide');
	                });
	            </script>
	        <?php } ?>

			<div id="parkingdetil" class="">

			</div>

		</div>
	</div>

	<script type="text/javascript">
		$('#getdata').click(function() {
				var qr = $('#result-code').val();

				if(qr != ''){
					$.ajax({
	            url : "ajax_cekmember_release.php",
	            type : 'POST',
	            data: { qr: qr },
	            success : function(data) {
	            	$('#parkingdetil').html(data);
								$('#stop').click();
	            },
	            error : function(){
	                alert('Try again.');
	                return false;
	            }
	        });

				}
				else{
					alert('Scan QR Code');
				}
		});

		$('#result-code').keyup(function(){
            var uid = $('#result-code').val();
            if(uid.length == 16){
	            $.ajax({
	                url : "ajax_cekmember_release.php",
	                type : 'POST',
	                data: { qr: uid },
	                success : function(data) {
	                	//alert('BERHASIL');
	                	$('#parkingdetil').html(data);
	                },
	                error : function(){
	                    alert('Try again.');
	                    return false;
	                }
	            });
						}
        });
	</script>

	<?php require('footer.php');?>
