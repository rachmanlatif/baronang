<?php
include "connect.php";

$row		= @$_POST['row'];
$vehicle 	= @$_POST['vhcl'];
$qty 		= @$_POST['qty'];

$sav = "SELECT Name from [dbo].[VehicleType] where VehicleID='$vehicle'";
$qav = sqlsrv_query($conn, $sav);
$dav = sqlsrv_fetch_array($qav, SQLSRV_FETCH_NUMERIC);

?>

<div class="row" id="row-<?php echo $row; ?>">
	<div class="col s4">
		<input type="hidden" name="vhcl_add[]" id="vhcl_add" value="<?php echo $vehicle; ?>" readonly>
		<input type="text" name="vhcl_nameadd[]" id="vhcl_nameadd" value="<?php echo $dav[0]; ?>" readonly>
	</div>
	<div class="col s4">
		<input type="text" name="qty_add[]" id="qty_add" value="<?php echo $qty; ?>" readonly>
	</div>
	<div class="col s4">
		<button type="button" id="btndel_vehicle-<?php echo $row; ?>" class="btn btn-large width-100 primary-color waves-effect waves-light"><i class="ion-android-delete"></i></button>
	</div>

	<script type="text/javascript">
		$('#btndel_vehicle-<?php echo $row; ?>').click(function(){
			$('#row-<?php echo $row; ?>').remove(); 
		});
	</script>
</div>

