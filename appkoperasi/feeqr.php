<?php
// fee qr    
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

if(@$_GET['qrid']) {
	$btn_text = 'Update';
	$btn_name_id = 'btnupdate';

	$sql = "SELECT * from [PaymentGateway].[dbo].[MerchantParking] where UserIDBaronang='".@$_GET['qrid']."'";
	$query = sqlsrv_query($conn, $sql);
	$data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
}
else {
	$btn_text = 'Simpan';
	$btn_name_id = 'btnok';
}
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">
			
			<h2 class="uppercase txt-black">Fee QR Code</h2>

			<div class="form-inputs m-t-30">
				<form action="" method="post" class="m-t-30" enctype="multipart/form-data">

					<div class="input-field">
						<input type="text" name="result-code" id="result-code" class="validate" value="<?php if(@$_GET['qrid']) echo @$_GET['qrid']; ?>" readonly>
						<label for="result-code">QR Code</label>
					</div>

					<div class="input-field">
						<input type="number" name="fee" id="fee" class="validate" value="<?php if(@$_GET['qrid']) echo $data[2]; ?>">
						<label for="fee">(Rp.)</label>
					</div>

					<div class="row m-t-30">
						<div class="col s6">
							<button type="button" name="btnscanqr" id="ocam" class="btn btn-large primary-color width-100 waves-effect waves-light">Scan QR</button>
						</div>
						<div class="col s6">
							<button type="submit" name="<?php echo $btn_name_id; ?>" id="<?php echo $btn_name_id; ?>" class="btn btn-large primary-color width-100 waves-effect waves-light"><?php echo $btn_text; ?></button>
						</div>
					</div>

					<div class="box box-solid hide" id="form-camera">
                        <input type="text" name="txt_scanqr" id="txt_scanqr">

                        <div class=""><b>Pilih Kamera</b></div>
                        <select class="browser-default" id="camera-select"></select>
                        <br>
                        <div class="box-body">
                            <div class="row">
                                <div class="col">
                                    <button title="Start Scan QR Barcode" class="waves-effect waves-light btn-large primary-color width-100" id="play" type="button">Start</button>
                                </div>
                                <div class="col">
                                    <button title="Stop streams" class="waves-effect waves-light btn-large primary-color width-100" id="stop" type="button">Stop</button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="well" style="position: relative;display: inline-block;">
                                        <canvas id="webcodecam-canvas"></canvas>
                                        <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                                        <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                                        <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                                        <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6">
                                    <div class="thumbnail" id="result">
                                        <img style="width: 100%;" id="scanned-img" src="">
                                        <div class="caption">
                                            <p class="text-center" id="scanned-QR"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

				</form>

				<?php
				$ok = @$_POST['btnok'];
				if(isset($ok)) {
					$qr 	= @$_POST['result-code'];
					$fee 	= @$_POST['fee'];

					$sql = "INSERT into [PaymentGateway].[dbo].[MerchantParking] values('$qr','1','$fee','$KID')";
					$query = sqlsrv_query($conn, $sql);
					?>
					<script type="text/javascript">
						alert('Simpan Berhasil');
						window.location.href = "feeqr.php";
					</script>
					<?php
				}

				$update = @$_POST['btnupdate'];
				if(isset($update)) {
					$qr 	= @$_POST['result-code'];
					$fee 	= @$_POST['fee'];

					$sql = "UPDATE [PaymentGateway].[dbo].[MerchantParking] set Fee='$fee' where UserIDBaronang='$qr'"; 
					$query = sqlsrv_query($conn, $sql);
					?>
					<script type="text/javascript">
						alert('Update Berhasil');
						window.location.href = "feeqr.php";
					</script>
					<?php
				}
				?>
			</div>

			<div class="table-responsive m-t-30">
                <table class="table">
					<thead>
						<tr>
							<th>QR Code</th>
							<th>Status</th>
							<th style="text-align: center;">Fee</th>
							<th></th>
						</tr>
					</thead>

					<tbody>
						<?php
						$sql = "SELECT * from [PaymentGateway].[dbo].[MerchantParking]";
						$query = sqlsrv_query($conn, $sql);
						while ($data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) { 
							if($data[1] == '1') $status = 'Aktif';
							else 'Tidak Aktif'; 
							?>
							<tr>
								<td><?php echo $data[0]; ?></td>
								<td><?php echo $status; ?></td>
								<td style="text-align: right;"><?php echo number_format($data[2],2,',','.'); ?></td>
								<td>
									<a href="?qrid=<?php echo $data[0]; ?>" class="btn btn-large primary-color waves-effect waves-light"><i class="ion-android-create"></i></a>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

		</div>
	</div>

	<?php if($_SESSION['device'] == 0){ ?>
	    <script type="text/javascript">
	        $('#ocam').click(function(){
	            Intent.openActivity("QRActivity","identity_cs.php");
	          });
	    </script>
	<?php } ?>

	<?php if($_SESSION['device'] == 1){ ?>
	    <script type="text/javascript">
	        $('#ocam').click(function(){
	            $('#result-code').val();
	            $('#form-camera').removeClass('hide');
	        });

	        $('#stop').click(function(){
	            $('#form-camera').addClass('hide');
	        });
	    </script>
	<?php } ?>

	<script type="text/javascript">
		$('#result-code').keyup(function(){
            var uid = $('#result-code').val();
            if(uid.length == 16){
            	//$('#siden').submit();
            }

            $.ajax({
                url : "ajax_cekmember_parking.php",
                type : 'POST',
                data: { qr: uid },
                success : function(data) {
                	//alert('BERHASIL');
                	$('#parkingdetil').html(data);
                },
                error : function(){
                    alert('Try again.');
                    return false;
                }
            });
        });
	</script>

<?php require('footer.php'); ?>