<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Detail Transaksi Kendaraan Parkir");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    

    $objWorkSheet->getStyle('D1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D1', 'Detail Transaksi Kendaraan Parkir');
    $objWorkSheet->getStyle('D2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D2', 'Periode :');
    $objWorkSheet->getStyle('A3')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A3')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A3', 'Akun :');

    
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No');
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'No. Transaksi');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'Tanggal Transaksi');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Keterangan');
    $objWorkSheet->getStyle('E5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E5', 'Debit');
    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F5', 'Kredit'); 
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G5', 'Total');    



    if($_GET['acc'] and $_GET['from'] and $_GET['to']){
    $no = 1;
    $from= $_GET['from'];
    $to= $_GET['to'];
    $lok = $_GET['acc'];
    $bln = 2 ;
    $hanyatanggal = date('d', strtotime($_GET['from']));
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $tgl2 = date('d', strtotime($_GET['to']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('E'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("E".$bln,$hanyatanggal .' '.$bulan .' '. $tahun .' - '. $tgl2 .' '. $bulan2 .' '. $tahun2); 

    $objWorkSheet->getStyle('B3')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("B3",$_GET['acc']);

    $saldo = 6;
    $tgla= date('1990/01/01 00:00:00');
    $tglh = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($from)));
    $tglk = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($to)));
    $awal = 0;

    $snilaid = "select sum(amount) from dbo.translist where AccountDebet = '$lok' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tgla' and '$tglh'";
    //echo $snilaid;
    $pnilaid = sqlsrv_query($conn, $snilaid);
    while ($hnilaid = sqlsrv_fetch_array( $pnilaid, SQLSRV_FETCH_NUMERIC)){
        if ($hnilaid != 0){
            $hsnilaid = $hnilaid[0];    
        } else {
            $hsnilaid = 0;    
        }
    }

    $snilaik = "select sum(amount) from dbo.translist where AccountKredit = '$lok' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tgla' and '$tglh'";
    //echo $snilaik;
    $pnilaik = sqlsrv_query($conn, $snilaik);
    while ($hnilaik = sqlsrv_fetch_array( $pnilaik, SQLSRV_FETCH_NUMERIC)){
        if ($hnilaik != 0){
            $hsnilaik = $hnilaik[0];    
        } else {
            $hsnilaik = 0;    
        }                            
    }

        $awal = ($hsnilaik - $hsnilaid);


    

    $objWorkSheet->getStyle('A'.$saldo,'Saldo Awal')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A'.$saldo,'Saldo Awal');
    $objWorkSheet->getStyle('G'.$saldo, number_format($awal))->getFont()->setBold(true);
    $objWorkSheet->SetCellValue("G".$saldo, number_format($awal), PHPExcel_Cell_DataType::TYPE_STRING);

    $row = 7;

    $aaa = "select * from (select *, ROW_NUMBER () over (Order by date asc) as row from dbo.translist where accountdebet = '$lok' and date between '$tglh' and '$tglk' and transactiontype LIKE 'TOP%'  or accountkredit = '$lok' and date between '$tglh' and '$tglk' and transactiontype LIKE 'TOP%') a ";
    //echo $aaa;
    $bbb = sqlsrv_query($conn, $aaa);
    $total= $awal;
    $ctotal = $total;
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        if ($ccc != null){
            if($ccc[1] == $lok ){
                $total-=$ccc[5];
                $ctotal=$total;               
            } else {
                $total+=$ccc[5];
                $ctotal=$total;                
            }
        } else {
            $ctotal = $total;   
    }
     
        // $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
        // $bbbb = sqlsrv_query($conn, $aaaa);
        // $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
        // if($cccc != null){
        //     $abc = $cccc[1];
        // }
        // else{
        //     $abc = $ccc[3];
        // }

        if ($lok == $ccc[1]){
            $ndebit = $ccc[5];   
        } else {
            $ndebit = 0;                                    
        } 

        if ($lok == $ccc[2]){
            $nkredit = $ccc[5];   
        } else {
            $nkredit = 0;        
        }


            $objWorkSheet->SetCellValue("A".$row,$ccc[15]);
            $objWorkSheet->SetCellValue("A".$row,$ccc[0]);
            $objWorkSheet->SetCellValue("B".$row, $ccc[6]->format('Y-m-d H:i:s'));
            $objWorkSheet->SetCellValue("E".$row, number_format($ndebit), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->SetCellValue("F".$row, number_format($nkredit), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->SetCellValue("G".$row, number_format($ctotal), PHPExcel_Cell_DataType::TYPE_STRING);            

                $no++;
                $row++;
            
    }
            $objWorkSheet->getStyle('A5:G' .$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objWorkSheet->getStyle('A'.$row,'Saldo Akhir')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue('A'.$row,'Saldo Akhir');
            $objWorkSheet->getStyle('G'.$row, number_format($ctotal))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("G".$row, number_format($ctotal), PHPExcel_Cell_DataType::TYPE_STRING);
    }
//exit;
    $objWorkSheet->setTitle('Drep  Transaksi Kendaraan Parkir');

    $fileName = 'NerKendPar'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
