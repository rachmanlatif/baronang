<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];

$ul0 = "";
$ul1 = "";
$ul2 = 0;
$ul3 = 0;
$ul4 = "";
$ul5 = "";
$ul6 = "";
$ul7 = "";
$ul8 = 0;
$ulprocedit = "";
$uldisabled = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.JabatanView where KodeJabatan='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if($eulrow != null){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul4    = $eulrow[7];
        $ul5    = $eulrow[9];
        $ul6    = $eulrow[11];
        $ul7    = $eulrow[2];

        $qw = "select top 1 StatusLimit from dbo.GeneralSetting";
        $as = sqlsrv_query($conn, $qw);
        $zx = sqlsrv_fetch_array($as, SQLSRV_FETCH_NUMERIC);
        if($zx != null){
            if($zx[0] == 1){
                $ul2 = $eulrow[3];
                $ul3 = $eulrow[5];
            }
            else{
                $t = $eulrow[3]+$eulrow[5];
                $ul8 = $t;
            }
        }

        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='mposition.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='mposition.php?page=".$_GET['page']."';</script>";
        }
    }
}

$mpbspksql = "select * from [dbo].[BasicSavingTypeView] where Status='1'";  //simpanan pokok
$mpbswjsql = "select * from [dbo].[BasicSavingTypeView] where Status='0'";  //simpanan wajib
$mpbssksql = "select * from [dbo].[BasicSavingTypeView] where Status='2'";  //simpanan sukarela
$mpbsstmt = sqlsrv_query($conn, $mpbspksql);
$mpbswstmt = sqlsrv_query($conn, $mpbswjsql);
$mpbssstmt = sqlsrv_query($conn, $mpbssksql);
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Jabatan'); ?></h2>

            <form action="proc_mposition.php<?=$ulprocedit?>" method="POST" class="form-horizontal">

                <div class="input-field">
                    <input type="text" name="name" class="form-control" id="positionname" value="<?=$ul1?>">
                    <label for="positionname"><?php echo lang('Nama jabatan'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="description" class="form-control" id="Description" value="<?=$ul7?>">
                    <label for="Description"><?php echo lang('Keterangan'); ?></label>
                </div>

                <?php
                $qw = "select top 1 StatusLimit from dbo.GeneralSetting";
                $as = sqlsrv_query($conn, $qw);
                $zx = sqlsrv_fetch_array($as, SQLSRV_FETCH_NUMERIC);
                if($zx != null){
                    if($zx[0] == 1){
                        ?>
                        <div class="input-field">
                            <input type="text" name="belanja" class="form-control price" id="LimitBelanja" value="<?=$ul2?>">
                            <label for="LimitBelanja"><?php echo lang('Limit belanja'); ?></label>
                        </div>

                        <div class="input-field">
                            <input type="text" name="pinjaman" class="form-control price" id="LimitPinjaman" value="<?=$ul3?>">
                            <label for="LimitPinjaman"><?php echo lang('Limit pinjaman'); ?></label>
                        </div>

                    <?php }
                    else { ?>
                        <div class="input-field">
                            <input type="text" name="gabungan" class="form-control price" id="LimitGabungan" value="<?=$ul8?>">
                            <label for="LimitGabungan"><?php echo lang('Limit gabungan'); ?></label>
                            <?php echo lang('(Belanja+pinjaman)'); ?>
                        </div>
                    <?php }
                }?>
                *Limit dapat diganti melalui menu Pengaturan Umum

                <br><br>
                <label><?php echo lang('Simpanan pokok'); ?></label>
                <select name="pokok" class="browser-default">
                    <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    while ($mpbsrow = sqlsrv_fetch_array( $mpbsstmt, SQLSRV_FETCH_NUMERIC)) {
                        ?>
                        <option value="<?=$mpbsrow[0];?>" <?php if($mpbsrow[0]==$ul5){echo "selected";} ?>><?=$mpbsrow[1];?> - <?=$mpbsrow[3];?></option>
                        <?php
                    }
                    ?>
                </select><br>

                <label><?php echo lang('Simpanan wajib'); ?></label>
                <select name="wajib" class="browser-default">
                    <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    while ($mpbswrow = sqlsrv_fetch_array( $mpbswstmt, SQLSRV_FETCH_NUMERIC)) {
                        ?>
                        <option value="<?=$mpbswrow[0];?>" <?php if($mpbswrow[0]==$ul4){echo "selected";} ?>><?=$mpbswrow[1];?> - <?=$mpbswrow[3];?></option>
                        <?php
                    }
                    ?>
                </select><br>

                <label><?php echo lang('Simpanan sukarela'); ?></label>
                <select name="sukarela" class="browser-default">
                    <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    while ($mpbsswrow = sqlsrv_fetch_array( $mpbssstmt, SQLSRV_FETCH_NUMERIC)) {
                        ?>
                        <option value="<?=$mpbsswrow[0];?>" <?php if($mpbsswrow[0]==$ul6){echo "selected";} ?>><?=$mpbsswrow[1];?> - <?=$mpbsswrow[3];?></option>
                        <?php
                    }
                    ?>
                </select>

                <div style="margin-top: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                    <?php } else{ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                    <?php } ?>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <a href="mposition.php" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Batal'); ?></a>
                    <?php } ?>
                </div>

            </form>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Daftar jabatan'); ?></h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="text-center"><?php echo lang('No'); ?></th>
                            <th class="text-center"><?php echo lang('Kode jabatan'); ?></th>
                            <th class="text-center"><?php echo lang('Nama jabatan'); ?></th>
                            <th class="text-center"><?php echo lang('Keterangan'); ?></th>
                            <th class="text-center"><?php echo lang('Limit belanja'); ?></th>
                            <th class="text-center"><?php echo lang('Limit pinjaman'); ?></th>
                            <th class="text-center"><?php echo lang('Simpanan pokok'); ?></th>
                            <th class="text-center"><?php echo lang('Simpanan wajib'); ?></th>
                            <th class="text-center"><?php echo lang('Simpanan sukarela'); ?></th>
                            <th class="text-center"><?php echo lang('Aksi'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //count
                        $jmlulsql   = "select count(*) from dbo.JabatanView";
                        $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        }
                        else{
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeJabatan asc) as row FROM [dbo].[JabatanView]) a WHERE row between '$posisi' and '$batas'";

                        //$ulsql = "select * from [dbo].[UserLevel]";

                        $ulstmt = sqlsrv_query($conn, $ulsql);

                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                            $tmpbspksql = "select * from [dbo].[BasicSavingTypeView] where KodeBasicSavingType='$ulrow[9]'";  //simpanan pokok
                            $tmpbswjsql = "select * from [dbo].[BasicSavingTypeView] where KodeBasicSavingType='$ulrow[7]'";  //simpanan wajib
                            $tmpbssksql = "select * from [dbo].[BasicSavingTypeView] where KodeBasicSavingType='$ulrow[11]'";  //simpanan sukarela
                            $tmpbsstmt = sqlsrv_query($conn, $tmpbspksql);    //simpanan pokok
                            $tmpbswstmt = sqlsrv_query($conn, $tmpbswjsql);   //simpanan wajib
                            $tmpbssstmt = sqlsrv_query($conn, $tmpbssksql);   //simpanan sukarela
                            $tpkmprow = sqlsrv_fetch_array( $tmpbsstmt, SQLSRV_FETCH_NUMERIC);   //simpanan pokok
                            $twjmprow = sqlsrv_fetch_array( $tmpbswstmt, SQLSRV_FETCH_NUMERIC);     //simpanan wajib
                            $tskmprow = sqlsrv_fetch_array( $tmpbssstmt, SQLSRV_FETCH_NUMERIC);     //simpanan sukarela
                            ?>
                            <tr>
                                <td><?=$ulrow[13];?></td>
                                <td><?=$ulrow[0];?></td>
                                <td><?=$ulrow[1];?></td>
                                <td><?=$ulrow[2];?></td>
                                <td style="text-align: right;"><?=$ulrow[4];?></td>
                                <td style="text-align: right;"><?=$ulrow[6];?></td>
                                <td style="text-align: right;"><?=$tpkmprow[3];?></td>
                                <td style="text-align: right;"><?=$twjmprow[3];?></td>
                                <td style="text-align: right;"><?=$tskmprow[3];?></td>
                                <td width="100" style="padding: 3px">
                                    <div class="btn-group" style="padding-right: 15px">
                                        <a href="mposition.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                    </div>
                                    <!--
                            <div class="btn-group">
                              <a href="<?php /* proc_mposition.php?page=<?=$halaman?>&delete=<?=$ulrow[0]?> */?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
                            </div>
                            -->
                                </td>
                            </tr>
                        <?php
                        }

                        $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix right">
                    <div style="text-align: center;">
                        <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>
                        <?php
                        $reload = "mposition.php?";
                        $page = intval($_GET["page"]);
                        $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                        if( $page == 0 ) $page = 1;

                        $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                        echo "<div>".paginate($reload, $page, $tpages)."</div>";
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer_new.php');
?>
