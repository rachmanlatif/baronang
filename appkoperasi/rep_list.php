<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Daftar Anggota'); ?></h2><br><br>

        <div class="box-body">
            <a href="drep_list.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a><br><br>

            <form action="" method="get">
                <label>Cari</label>
                <input type="text" name="cari">

                <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                    <button type="submit"  value = "cari " class="waves-effect waves-light btn-large primary-color width-100">Cari</button>
                </div>
 
            </form>

            <?php 
            if(isset($_GET['cari'])){
            $cari = $_GET['cari'];
            echo "<b>Hasil pencarian : ".$cari."</b><br>";
            ?>
            <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                <a href="rep_list.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh All Data</button></a>
            </div>
            <?php } ?>
            
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive" id="example1" role="grid" aria-describedby="example1_info">
                <table class="table table-bordered table-striped" id="example1" role="grid" aria-describedby="example1_info" style="margin-top: 30px;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Member ID</th>
                        <th>OLD Member</th>
                        <th><?php echo lang('Nama'); ?></th>
                        <th><?php echo lang('Jabatan'); ?></th>
                        <th><?php echo lang('Alamat'); ?></th>
                        <th><?php echo lang('No. Telp'); ?></th>
                        <th>Email</th>
                        <th>KTP</th>
                        <th>NIP</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from dbo.MemberListView where KID='$_SESSION[KID]'";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    if(isset($_GET['cari'])){
                    $cari = $_GET['cari'];
                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[MemberList]) a WHERE KID='$_SESSION[KID]' and Name like '%".$cari."%' or MemberID like '%".$cari."%' or Addr like '%".$cari."%' or OldmemberID like '%".$cari."%'";
                        //echo $ulsql;
                    } else {
                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[MemberList]) a WHERE KID='$_SESSION[KID]' and row between '$posisi' and '$batas' ";



                    }
                    //echo $ulsql;
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        $jab = "Select * from dbo.jabatandetail where kodejabatan = '$ulrow[9]'";
                        $projab = sqlsrv_query($conn, $jab);
                        while ($hasjab = sqlsrv_fetch_array( $projab, SQLSRV_FETCH_NUMERIC)){

                    ?>
                        <tr>
                            <td><?php echo $ulrow[11]; ?></td>
                            <td align="right"><a href="rep_bas.php?acc=<?php echo $ulrow[1]; ?>&page=<?php echo $_GET['page']; ?>"><?php echo $ulrow[1]; ?></td>
                            <td><?php echo $ulrow[10]; ?></td>
                            <td><?php echo $ulrow[2]; ?></td>
                            <td><?php echo $hasjab[7]; ?></td>
                            <td><?php echo $ulrow[3]; ?></td>
                            <td><?php echo $ulrow[4]; ?></td>
                            <td><?php echo $ulrow[5]; ?></td>
                            <td><?php echo $ulrow[7]; ?></td>
                            <td><?php echo $ulrow[8]; ?></td>
                        </tr>
                    <?php } ?>    
                    <?php } ?>    
                    <?php
                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix right">
                <div style="text-align: center;">
                    Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                    <?php
                    $reload = "rep_list.php?";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages)."</div>";
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>



<?php require('footer_new.php');?>
