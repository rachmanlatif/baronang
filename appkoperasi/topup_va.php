<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h3 class="box-title"><?php echo lang('Upload Virtual Account'); ?></h3>

        <form action="uva.php" method="post" enctype="multipart/form-data">
            Pilih bank
            <br>
            <select name="bank" id="bank" class="browser-default">
                 <?php
                $sql = 'Select * from KoneksiKoperasiBaronang.dbo.BankList order by NamaBank asc';
                $query = sqlsrv_query($conn, $sql);
                while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                    <?php if($row[0] == 'B0080'){ ?>
                      <option value="<?php echo $row[0] ;?>" selected><?php echo $row[1]; ?> </option>
                    <?php } else { ?>
                      <option value="<?php echo $row[0] ;?>"><?php echo $row[1]; ?> </option>
                    <?php } ?>
                <?php } ?>
            </select>
            <br>
            <div class="box box-solid">
                <label class="active">Tanggal VA</label>
                <input type="date" class="validate" name="date" value="<?php echo date('Y-m-d', strtotime(date('Y-m-d').'- 1 day')); ?>">
            </div>
            <label class="validate control-label"><?php echo lang('Data Excel'); ?></label>
            <input type="file" name="filename" class="form-control" accept=".xls, .xlsx"  required="">

            <div style="margin-top: 30px;">
                <button type="submit" class="waves-effect waves-light btn-large primary-color width-100">Submit</button>
            </div>

        </form>

    </div>
</div>

<?php require('footer_new.php');?>
