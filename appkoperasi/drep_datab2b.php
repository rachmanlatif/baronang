<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Data B2B");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
    
    // baris judul
    $objWorkSheet->getStyle('E1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('E1', 'Data B2B');



    //$objWorkSheet->getActiveSheet()->margeCell('A5:B5');
    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A6', 'No.');  
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B6', 'No. Kartu');
    $objWorkSheet->getStyle('C6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C6', 'Tanggal Register');
    $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D6', 'Bulan Valid');
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E6', 'Tahun Valid');
    $objWorkSheet->getStyle('F6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F6', 'ID Lokasi');
    $objWorkSheet->getStyle('G6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G6', 'Nama Lokasi');
    $objWorkSheet->getStyle('H6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H6', 'Vehicle');
    $objWorkSheet->getStyle('I6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('I6', 'Nama');
    $objWorkSheet->getStyle('J6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('J6', 'Alamat');
    $objWorkSheet->getStyle('K6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('K6', 'No. Tlp');
    $objWorkSheet->getStyle('L6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('L6', 'Email');


if($_GET['akun']){

    $akun = $_GET['akun'];

    $row = 7;
    $rw = 7;
    
    $xy = "SELECT * from(select a.CardNo, b.DateRegister, (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidMonth, (select max(Year) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidYear, (select LocationID from dbo.LocationMerchant where LocationID= b.LocationID) as IDLocation, (select Nama from dbo.LocationMerchant where LocationID= b.LocationID) as Location, (select Name from dbo.VehicleType where VehicleID= b.VehicleID) as Vehicle, c.Name, c.Addr, c.Phone, c.email, row_number() over (ORDER BY b.Dateregister asc) as row from dbo.MemberCard a inner join dbo.MemberLocation b on a.CardNo = b.CardNo left join dbo.MemberList c on a.MemberID = c.MemberID left join dbo.AktivasiB2B d on a.CardNo = d.TipeQR where a.Status = 1 and a.CardNo not like '%100010001000%' and locationid = '$akun' and (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) = '12') a order by row asc";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
               
            $objWorkSheet->SetCellValueExplicit("A".$row,$za[11]);
            $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
            $objWorkSheet->SetCellValueExplicit("C".$row,$za[1]);
            $objWorkSheet->SetCellValueExplicit("D".$row,$za[2]);
            $objWorkSheet->SetCellValueExplicit("E".$row,$za[3]);
            $objWorkSheet->SetCellValueExplicit("F".$row,$za[4]);
            $objWorkSheet->SetCellValueExplicit("G".$row,$za[5]);
            $objWorkSheet->SetCellValueExplicit("H".$row,$za[6]);
            $objWorkSheet->SetCellValueExplicit("I".$row,$za[7]);
            $objWorkSheet->SetCellValueExplicit("J".$row,$za[8]);
            $objWorkSheet->SetCellValueExplicit("K".$row,$za[9]);
            $objWorkSheet->SetCellValueExplicit("L".$row,$za[10]);
        
        
        

        
        $row++;
        $rw++;
        }
} else {

    $row = 7;
    $rw = 7;
    
    $xy = "SELECT * from(select a.CardNo, b.DateRegister, (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidMonth, (select max(Year) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidYear, (select LocationID from dbo.LocationMerchant where LocationID= b.LocationID) as IDLocation, (select Nama from dbo.LocationMerchant where LocationID= b.LocationID) as Location, (select Name from dbo.VehicleType where VehicleID= b.VehicleID) as Vehicle, c.Name, c.Addr, c.Phone, c.email, row_number() over (ORDER BY b.Dateregister asc) as row from dbo.MemberCard a inner join dbo.MemberLocation b on a.CardNo = b.CardNo left join dbo.MemberList c on a.MemberID = c.MemberID left join dbo.AktivasiB2B d on a.CardNo = d.TipeQR where a.Status = 1 and a.CardNo not like '%100010001000%' and (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) = '12') a order by row asc";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
               
            $objWorkSheet->SetCellValueExplicit("A".$row,$za[11]);
            $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
            $objWorkSheet->SetCellValueExplicit("C".$row,$za[1]);
            $objWorkSheet->SetCellValueExplicit("D".$row,$za[2]);
            $objWorkSheet->SetCellValueExplicit("E".$row,$za[3]);
            $objWorkSheet->SetCellValueExplicit("F".$row,$za[4]);
            $objWorkSheet->SetCellValueExplicit("G".$row,$za[5]);
            $objWorkSheet->SetCellValueExplicit("H".$row,$za[6]);
            $objWorkSheet->SetCellValueExplicit("I".$row,$za[7]);
            $objWorkSheet->SetCellValueExplicit("J".$row,$za[8]);
            $objWorkSheet->SetCellValueExplicit("K".$row,$za[9]);
            $objWorkSheet->SetCellValueExplicit("L".$row,$za[10]);
        
        
        

        
        $row++;
        $rw++;
        }
}

//exit;
    $objWorkSheet->setTitle('Data B2B');

    $fileName = 'DataB2B'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
