<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep List EDC");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->getStyle('C1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('C1', 'Laporan List EDC');

    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C2', 'Periode :');                 

   
    


    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A4' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A4', 'No');
    $objWorkSheet->getStyle('B4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B4', 'No EDC');
    $objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('C4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C4', 'Debit');
    $objWorkSheet->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('D4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D4', 'Kredit');
    $objWorkSheet->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E4', 'Saldo');   
 


    if($_GET['from'] and $_GET['to']){
    
    $tanggaldari = $_GET['from'];
    $tanggalsampai = $_GET['to'];
    $tglh = date('Y/m/d', strtotime('+1 days', strtotime($tanggalsampai)));
    $from1= date('Y/m/d h:i:s', strtotime('-1 days', strtotime($tanggaldari)));
    $kid = $_SESSION[KID];
    $tgla= date('1990/01/01');
    $tglk = date('Y/m/d', strtotime('+1 days', strtotime($tanggaldari)));
    $tglh = date('Y/m/d', strtotime('+1 days', strtotime($tanggalsampai)));
    $no = 1;
    $bln = 2 ;
    $hanyatanggal = date('d', strtotime($_GET['from']));
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $tgl2 = date('d', strtotime($_GET['to']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('D'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("D".$bln,$hanyatanggal .' '.$bulan .' '. $tahun .' - '. $tgl2 .' '. $bulan2 .' '. $tahun2);   


    $k = 4;
    $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
           
        $objWorkSheet->getStyle('B'.$k)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$k,$z[0] .' - '. $z[2]);           

    }

    

    
    $row = 5;
    $rw = 5;
    
    $xy = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY SerialNumber asc) as row FROM [Gateway].[dbo].[EDCList] a where KID = '$kid' ) a";
    //echo $x;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
         // if($za[11] == 1){
         //    $member='Member';
         //    } else {
         //    $member='Non Member';
         //    }
        $saldo = 0;
            if ($za != null){
                $snilaid = "select sum(amount) from dbo.translist where AccountDebet = '$za[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tgla' and '$tglk'";
                //echo $snilaid;
                $pnilaid = sqlsrv_query($conn, $snilaid);
                while ($hnilaid = sqlsrv_fetch_array( $pnilaid, SQLSRV_FETCH_NUMERIC)){
                    if ($hnilaid != 0){
                        $hsnilaid = $hnilaid[0];    
                    } else {
                        $hsnilaid = 0;    
                    }
                }

                $snilaik = "select sum(amount) from dbo.translist where AccountKredit = '$za[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tgla' and '$tglk'";
                //echo $snilaik;
                $pnilaik = sqlsrv_query($conn, $snilaik);
                while ($hnilaik = sqlsrv_fetch_array( $pnilaik, SQLSRV_FETCH_NUMERIC)){
                    if ($hnilaik != 0){
                        $hsnilaik = $hnilaik[0];    
                    } else {
                        $hsnilaik = 0;    
                    }
                }

                $sdebit = "select sum(amount) from dbo.translist where AccountDebet = '$za[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tglk' and '$tglh'";
                //echo $sdebit;
                $pdebit = sqlsrv_query($conn, $sdebit);
                while ($hdebit = sqlsrv_fetch_array( $pdebit, SQLSRV_FETCH_NUMERIC)){
                    if ($hdebit != 0){
                        $hsdebit = $hdebit[0];    
                    } else {
                        $hsdebit = 0;    
                    }
                }
                                    

                $skredit = "select sum(amount) from dbo.translist where AccountKredit = '$za[0]' and transactiontype between 'TOPE' and 'TOPP'and Date between '$tglk' and '$tglh'";
                //echo $skredit;
                $pkredit = sqlsrv_query($conn, $skredit);
                while ($hkredit = sqlsrv_fetch_array( $pkredit, SQLSRV_FETCH_NUMERIC)){
                    if ($hkredit != 0){
                        $hskredit = $hkredit[0];
                    } else {
                        $hskredit = 0;    
                    }
                }

            $saldo = ($hsnilaik - $hsnilaid) - $hsdebit + $hskredit;
        } 


               
        $objWorkSheet->SetCellValueExplicit("A".$row,$za[9]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
        $objWorkSheet->SetCellValueExplicit("C".$row, number_format($hsdebit), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("D".$row, number_format($hskredit), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("E".$row, number_format($saldo), PHPExcel_Cell_DataType::TYPE_STRING);
        $row++;
        $rw++;
        }
 }
//exit;
    $objWorkSheet->setTitle('Drep Laporan EDC');

    $fileName = 'LapEDC'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
