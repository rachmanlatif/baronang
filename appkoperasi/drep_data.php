<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Laporan Data Member");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
    
    // baris judul
    $objWorkSheet->getStyle('F1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('F1', 'Laporan Data Member');
    $objWorkSheet->getStyle('F2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('F2', 'Periode :');
    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A4')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A4', 'Lokasi :');
    

    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A6', 'No');
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B6', 'No. Kartu');
    $objWorkSheet->getStyle('C6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C6', 'Tanggal Register');
    $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D6', 'Bulan Valid');
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E6', 'Tahun Valid');
    $objWorkSheet->getStyle('F6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F6', 'Type Mobil');
    $objWorkSheet->getStyle('G6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G6', 'Nama Pemilik');
    $objWorkSheet->getStyle('H6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H6', 'Alamat');
    $objWorkSheet->getStyle('I6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('I6', 'No. Tlp/HP');
    $objWorkSheet->getStyle('J6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('J6', 'Email');  

    if($_GET['from'] and $_GET['to'] and $_GET['akun']){
    
    $tgl1 = $_GET['from']; 
    $tgl2 = $_GET['to'];
    $akun = $_GET['akun'];
    $no = 1;
    $bln = 2 ;

    $hanyatanggal = date('d', strtotime($_GET['from']));
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $hanyatanggal1 = date('d', strtotime($_GET['to']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('G'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("G".$bln,$hanyatanggal .' '.$bulan .' '. $tahun .' - '. $hanyatanggal1 .' '. $bulan2 .' '. $tahun2);   


    // $k = 4;
    // $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
    // //echo $x;
    // $y = sqlsrv_query($conn, $x);
    // while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
           
    //     $objWorkSheet->getStyle('F'.$k)->getFont()->setBold(true);
    //     $objWorkSheet->SetCellValue("F".$k,$z[0] .' - '. $z[2]);           

    // }

    $lok = "select * from dbo.LocationMerchant where LocationID = '$_GET[akun]'";
    $proloc = sqlsrv_query($conn,$lok);
    $hasloc = sqlsrv_fetch_array($proloc, SQLSRV_FETCH_NUMERIC);

    $objWorkSheet->getStyle('B4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("B4",$hasloc[2]);    
    
    $row = 7;
    $rw = 7; 
    $xy = "SELECT * from (select a.CardNo, b.DateRegister, (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidMonth,(select max(Year) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidYear, (select LocationID from dbo.LocationMerchant where LocationID= b.LocationID) as IDLocation,(select Nama from dbo.LocationMerchant where LocationID= b.LocationID) as Location, (select Name from dbo.VehicleType where VehicleID= b.VehicleID) as Vehicle, c.Name, c.Addr, c.Phone, c.email, ROW_NUMBER() OVER (ORDER BY b.DateRegister asc) as row from dbo.MemberCard a inner join dbo.MemberLocation b on a.CardNo = b.CardNo  left join dbo.MemberList c on a.MemberID = c.MemberID where a.Status = 1 and a.CardNo not like '%100010001000%' and b.LocationID = '$akun' and b.DateRegister between '$tgl1' and '$tgl2') a order by row asc";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
    
        $objWorkSheet->getStyle('A7' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objWorkSheet->SetCellValueExplicit("A".$row,$za[11]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
        $objWorkSheet->SetCellValue("C".$row, $za[1]->format('Y-m-d H:i:s'));
        $objWorkSheet->SetCellValueExplicit("D".$row,$za[2]);
        $objWorkSheet->SetCellValueExplicit("E".$row,$za[3]);
        $objWorkSheet->SetCellValueExplicit("F".$row,$za[6]);
        $objWorkSheet->SetCellValueExplicit("G".$row,$za[7]);
        $objWorkSheet->SetCellValueExplicit("H".$row,$za[8]);
        $objWorkSheet->SetCellValueExplicit("I".$row,$za[9]);
        $objWorkSheet->SetCellValueExplicit("J".$row,$za[10]);
        
        $row++;
        $rw++;
        }
 }
//exit;
    $objWorkSheet->setTitle('Drep Laporan Data Member');

    $fileName = 'LapDatMember'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
