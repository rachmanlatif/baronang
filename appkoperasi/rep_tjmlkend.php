    <?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">
        <div class="box-body">
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                <h2 class="uppercase"><?php echo lang('Detail Transaksi'); ?></h2> <br>

                <a href="rep_jmlkend.php?tgl1=<?php echo $_GET['tgl1'];?>&tgl2=<?php echo $_GET['tgl2'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 10px; margin-bottom: 20px;">Back</button></a>

                <!-- <a href="drep_totparkir.php?lok=<?php echo $_GET['lok'];?>&tgl1=<?php echo $_GET['tgl1'];?>&tgl2=<?php echo $_GET['tgl2'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 10px; margin-bottom: 20px;">Download To Excel</button></a> -->

                <?php 
                $lok = $_GET['lok'];
                $from= $_GET['tgl1'];
                $to= $_GET['tgl2'];
                $tglh = date('Y/m/d', strtotime('+1 days', strtotime($to)));
                $nm = 1;
                $sim = "SELECT distinct a.VehicleType, v.name as Nama, v.VehicleID from dbo.ParkingList a inner join dbo.VehicleType v on a.VehicleType = v.Code where LocationIn = '$lok' and TimeIn between '$from' and '$tglh' order by v.VehicleID asc";
                //echo $sim;
                $pan = sqlsrv_query($conn, $sim);
                while($nan = sqlsrv_fetch_array( $pan, SQLSRV_FETCH_NUMERIC)){
                    $no1 = 1;
                ?>
                
                <div>
                    <h4><?php echo $nm; ?>. <?php echo $nan[1]; ?></h4>
                </div>
                    
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo ('No'); ?></th>    
                                    <th><?php echo ('Tanggal Masuk'); ?></th>
                                    <th><?php echo ('Tanggal Keluar'); ?></th> 
                                    <th><?php echo ('Nomor Kartu'); ?></th> 
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php
                                //count
                                $jmlulsql   = "select count(*) from dbo.ParkingList where LocationIn='$lok' and TimeIn between '$from' and '$tglh' and VehicleType = '$nan[0]'";
                                //echo $jmlulsql;
                                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                                //pagging
                                $perpages   = 10;
                                $halaman    = $_GET['page'];
                                    if(empty($halaman)){
                                        $posisi  = 0;
                                        $batas   = $perpages;
                                        $halaman = 1;
                                    } else {
                                        $posisi  = (($perpages * $halaman) - 10) + 1;
                                        $batas   = ($perpages * $halaman);
                                    }

                                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY TransID asc) as row from dbo.ParkingList where LocationIn='$lok' and TimeIn between '$from' and '$tglh' and VehicleType = '$nan[0]') a  where row between '$posisi' and '$batas'";
                                //echo $ulsql;
                                $ulstmt = sqlsrv_query($conn, $ulsql);
                                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                                    //var_dump($ulrow);
                                    if ($ulrow[4] == null ) {
                                        $row2 = 'Kendaraan Masih Dilokasi Parkir';
                                    } else {
                                        $row2 = $ulrow[4] ->format('Y-m-d H:i:s');
                                    }
                                   ?>
                                    <tr>
                                        <td><?php echo $ulrow[12]; ?></td>
                                        <td align="right"><?php echo $ulrow[3]->format('Y-m-d H:i:s'); ?></td>
                                        <td><?php echo $row2; ?></td>
                                        <td><?php echo $ulrow[2]; ?></td>
                                    </tr>
                                <?php $no1++; $jmlhalaman = ceil($jmlulrow[0]/$perpages); } ?>
                            </tbody>
                        </table>
                        
                        <div class="box-footer clearfix right">
                            <div style="text-align: center;">
                                Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                                <?php
                                $ind2 = $nan[0];
                                $lok = $_GET['lok'];
                                $from= $_GET['tgl1'];
                                $to= $_GET['tgl2'];
                                $reload = "rep_tjmlkend.php?lok=$lok&tgl1=$from&tgl2=$to";
                                $page = intval($_GET["page"]);
                                $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                                if( $page == 0 ) $page = 1;
                                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                    echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                    ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <?php $nm++;} ?>        
            </div>
        </div>
    </div>
</div>
                                                               

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
 require('content-footer.php');?>

<?php require('footer.php');?>
