<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";

$a = "select * from [dbo].[MemberList]";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
if($c == null){
    messageAlert('Member belum di set. Tidak dapat mendownload template.','warning');
    header('Location: mbas_balance.php');
}
else{

    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Data Upload BS Balance");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'Nama');
    $objWorkSheet->SetCellValue('B1', 'NIP');
    $objWorkSheet->SetCellValue('C1', 'KTP');
    $objWorkSheet->SetCellValue('D1', 'Email');
    $objWorkSheet->SetCellValue('E1', 'Telepon');
    $objWorkSheet->SetCellValue('F2', 'Pay Amount');
    $objWorkSheet->SetCellValue('G2', 'Tunggakan');
    $objWorkSheet->SetCellValue('H2', 'Pay Amount');
    $objWorkSheet->SetCellValue('I2', 'Tunggakan');
    $objWorkSheet->SetCellValue('J2', 'Pay Amount');
    $objWorkSheet->SetCellValue('K2', 'Tunggakan');

    $objWorkSheet->SetCellValue('F1', 'Simpanan Pokok');
    $objWorkSheet->SetCellValue('H1', 'Simpanan Wajib');
    $objWorkSheet->SetCellValue('J1', 'Simpanan Sukarela');

    $objWorkSheet->mergeCells('A1:A2');
    $objWorkSheet->mergeCells('B1:B2');
    $objWorkSheet->mergeCells('C1:C2');
    $objWorkSheet->mergeCells('D1:D2');
    $objWorkSheet->mergeCells('E1:E2');

    $objWorkSheet->mergeCells('F1:G1');
    $objWorkSheet->mergeCells('H1:I1');
    $objWorkSheet->mergeCells('J1:K1');

    $no = 1;
    $row = 3;

    $x = "select * from [dbo].[MemberList] where StatusMember = 1";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        $objWorkSheet->SetCellValue("A".$row, $z[2], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("B".$row, $z[8]);
        $objWorkSheet->SetCellValueExplicit("C".$row, $z[7], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("D".$row, $z[5]);
        $objWorkSheet->SetCellValueExplicit("E".$row, $z[4], PHPExcel_Cell_DataType::TYPE_STRING);

        $no++;
        $row++;
    }

    $objWorkSheet->setTitle('Data Upload BS Balance');

//sheet 2
    $objWorkSheet2 = $objPHPExcel->createSheet(1);
// baris judul
    $objWorkSheet2->SetCellValue('A1', 'Member ID');
    $objWorkSheet2->SetCellValue('B1', 'No. KTP');
    $objWorkSheet2->SetCellValue('C1', 'NIP');
    $objWorkSheet2->SetCellValue('D1', 'Nama');
    $objWorkSheet2->SetCellValue('E1', 'Alamat');
    $objWorkSheet2->SetCellValue('F1', 'Telepon');
    $objWorkSheet2->SetCellValue('G1', 'Email');

    $no = 1;
    $row = 2;

    $x = "select * from [dbo].[MemberList] where StatusMember = 1 order by MemberID asc";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        $objWorkSheet2->SetCellValueExplicit("A".$row, $z[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValueExplicit("B".$row, $z[7], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("C".$row, $z[8]);
        $objWorkSheet2->SetCellValue("D".$row, $z[2]);
        $objWorkSheet2->SetCellValue("E".$row, $z[3]);
        $objWorkSheet2->SetCellValueExplicit("F".$row, $z[4], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("G".$row, $z[5]);

        $no++;
        $row++;
    }

    $objWorkSheet2->setTitle('Data Master Member');

    $fileName = 'templateBasicSavingBalance'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

    // download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;
}
?>