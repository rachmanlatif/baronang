<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

            <div class="notification notification-warning notification-dismissible">
                <!--<button type="button" class="close" data-dismiss="notification" aria-hidden="true">×</button>-->
                <h4>Warning</h4>
                You are not authorize for this menu!
            </div>

    </div>
</div>

    <!-- </div> End of Main Contents  -->

    </div> <!-- End of Page Content -->

    </div> <!-- End of Page Container -->


    <script src="js/jquery-2.1.0.min.js"></script>
    <script src="js/jquery.swipebox.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/masonry.min.js"></script>
    <script src="js/chart.min.js"></script>
    <script src="js/functions.js"></script>
</body>
</html>