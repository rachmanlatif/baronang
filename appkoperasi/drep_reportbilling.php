<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";

$kid = @$_GET['id'];
$bulan = @$_GET['bulan'];
$ldbulan = @$_GET['ldbulan'];
$tahun = @$_GET['tahun'];

$from = $tahun.'-'.$bulan.'-1';
$to = $tahun.'-'.$bulan.'-'.$ldbulan;

$tgl = date("Y-m-d");
$uidm = $_SESSION['UserID'];

if($bulan == '01') $fbulan = 'Januari';
else if($bulan == '02') $fbulan = 'Februari';
else if($bulan == '03') $fbulan = 'Maret';
else if($bulan == '04') $fbulan = 'April';
else if($bulan == '05') $fbulan = 'Mei';
else if($bulan == '06') $fbulan = 'Juni';
else if($bulan == '07') $fbulan = 'Juli';
else if($bulan == '08') $fbulan = 'Agustus';
else if($bulan == '09') $fbulan = 'September';
else if($bulan == '10') $fbulan = 'Oktober';
else if($bulan == '11') $fbulan = 'November';
else if($bulan == '12')  $fbulan = 'Desember';


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Baronang");
    $objPHPExcel->getProperties()->setTitle("Report Billing");

    // set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
    
    // baris judul   
    $objWorkSheet->getStyle('A1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A1')->getFont()->setSize(18);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
    $objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A1', 'Billing '.$fbulan.' '.$tahun );


    $objWorkSheet->getStyle('A3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
    $objWorkSheet->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A3', 'NIP');

    $objWorkSheet->getStyle('B3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $objWorkSheet->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B3', 'No. Anggota Lama');

    $objWorkSheet->getStyle('C3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
    $objWorkSheet->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C3', 'Nama');

    $objWorkSheet->getStyle('D3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    $objWorkSheet->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D3', 'SP (Rp.)');

    $objWorkSheet->getStyle('E3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $objWorkSheet->getStyle('E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E3', 'SW (Rp.)');

    $objWorkSheet->getStyle('F3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $objWorkSheet->getStyle('F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F3', 'SS(Rp.)');

    $objWorkSheet->getStyle('G3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(22);
    $objWorkSheet->getStyle('G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G3', 'Angsuran Pokok (Rp.)');

    $objWorkSheet->getStyle('H3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(22);
    $objWorkSheet->getStyle('H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H3', 'Angsuran Bunga (Rp.)');

    $objWorkSheet->getStyle('I3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
    $objWorkSheet->getStyle('I3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('I3', 'Total (Rp.)');

    $sqla = "SELECT MemberID, NIP, OldMemberID, Name from [dbo].[MemberList] where KID='$kid' ORDER BY MemberID ASC";
    $querya = sqlsrv_query($conn, $sqla);
    $cr = 4;
    $nosave = 1;
    while( $data1 = sqlsrv_fetch_array($querya) ) {
        $total = 0;
        $mid = $data1[0];
        $nip = $data1[1];
        $oldmid = $data1[2];
        $billid = $_SESSION['KID'].'BILL'.$uidm.date('y', strtotime($tahun)).$bulan.'-'.str_pad($nosave, 3, '0', STR_PAD_LEFT);

        $objWorkSheet->getStyle('A'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objWorkSheet->SetCellValue('A'.$cr, $data1[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->getStyle('B'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objWorkSheet->SetCellValue('B'.$cr, $data1[2], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->getStyle('C'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objWorkSheet->SetCellValue('C'.$cr, $data1[3], PHPExcel_Cell_DataType::TYPE_STRING);

        $sqlb1 = "SELECT KodeBasicSavingType from [dbo].[BasicSavingBalance] where MemberID = '$mid'";
        $queryb1 = sqlsrv_query($conn, $sqlb1);
        while( $data2 = sqlsrv_fetch_array($queryb1) ) {
            $kbst = $data2[0];

            $sqlc = "SELECT Status from [dbo].[BasicSavingType] where KodeBasicSavingType = '$kbst'";
            $queryc = sqlsrv_query($conn, $sqlc);
            $data3 = sqlsrv_fetch_array($queryc);

            $sqld = "SELECT BillingAmmount from [dbo].[BasicSavingTransaction] where KodeBasicSavingType = '$kbst' and TimeStam between '$from' and '$to'";
            $queryd = sqlsrv_query($conn, $sqld);
            $data4 = sqlsrv_fetch_array($queryd);

            if($data3[0] == 0) {
                $sp = $data4[0];
                $total = intval($total) + intval($sp);
            }
            else if($data3[0] == 1) {
                $sw = $data4[0];
                $total = intval($total) + intval($sw);
            }
            else if($data3[0] == 2) {
                $ss = $data4[0];
                $total = intval($total) + intval($ss);
            }
        }

        $sqlb2 = "SELECT SUM(Pokok) as Pokok, SUM(Bunga) as Bunga from [dbo].[LoanApplicationList] a 
                  INNER JOIN [dbo].[LogLoanSchedule] b ON a.LoanAppNum = b.LoanNum
                  where MemberID = '$mid' and Tanggal between '$from' and '$to' ";
        $queryb2 = sqlsrv_query($conn, $sqlb2);
        $data2 = sqlsrv_fetch_array($queryb2);

        $pokok = $data2[0];
        $total = intval($total) + intval($pokok);

        $bunga = $data2[1];
        $total = intval($total) + intval($bunga);

        $objWorkSheet->getStyle('D'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue('D'.$cr, number_format($sp,2,',','.'), PHPExcel_Cell_DataType::TYPE_STRING);

        $objWorkSheet->getStyle('E'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue('E'.$cr, number_format($sw,2,',','.'), PHPExcel_Cell_DataType::TYPE_STRING);

        $objWorkSheet->getStyle('F'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue('F'.$cr, number_format($ss,2,',','.'), PHPExcel_Cell_DataType::TYPE_STRING);

        $objWorkSheet->getStyle('G'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue('G'.$cr, number_format($pokok,2,',','.'), PHPExcel_Cell_DataType::TYPE_STRING);

        $objWorkSheet->getStyle('H'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue('H'.$cr, number_format($bunga,2,',','.'), PHPExcel_Cell_DataType::TYPE_STRING);

        $objWorkSheet->getStyle('I'.$cr)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue('I'.$cr, number_format($total,2,',','.'), PHPExcel_Cell_DataType::TYPE_STRING);
        

        // simpan ke tabel
        $sql = "INSERT into [dbo].[ReportBilling](BillingID, UserIDMaker, UserIDBayar, Tanggal, Bulan, Tahun, NIP, NoAnggotaLama, SP, SW, SS, AngsuranPokok, AngsuranBunga, Total)
                values('$billid', '$uidm', '-', '$tgl','$bulan','$tahun','$nip','$oldmid','$sp','$sw','$ss','$pokok','$bunga','$total')";
        //echo $sql.'<br><br>';
        $query = sqlsrv_query($conn, $sql);

        $cr++;
        $nosave++;
    }

    //exit;
    $objWorkSheet->setTitle('Billing '.$fbulan.' '.$tahun);

    $fileName = 'Billing '.$fbulan.' '.$tahun.'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');


    // download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;
?>
