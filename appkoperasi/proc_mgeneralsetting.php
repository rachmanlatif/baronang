<?php
session_start();
error_reporting(0);

include "connect.php";

$BS				= $_POST['BS'];
$RSI			= $_POST['RSI'];
$DCT			= $_POST['DCT'];
$CRD			= $_POST['CRD'];
$CRW			= $_POST['CRW'];
$CRM			= $_POST['CRM'];
$isRealTime		= $_POST['isRealTime'];
$CDM			= $_POST['CDM'];
$isRealDay		= $_POST['isRealDay'];
$isInterestTax	= $_POST['isInterestTax'];
$TaxRate		= $_POST['TaxRate'];
$MinimumTax		= $_POST['MinimumTax'];
$isAkun         = $_POST['isAkun'];
$isLimit         = $_POST['isLimit'];

if($BS == "" || $RSI == "" || $DCT == "" || $CRD == "" || $CRW == "" || $CRM == "" || $isRealTime == ""|| $CDM == "" || $isRealDay == "" || $isInterestTax == "" || $TaxRate == "" || $MinimumTax == "" || $isAkun == ""){
    messageAlert(lang('Harap isi seluruh kolom'),'info');
    header('Location: mgeneralsetting.php');
}
else{
	$gssql = "select * from [dbo].[GeneralSetting] where KID='$_SESSION[KID]'";
	$gsstmt = sqlsrv_query($conn, $gssql);
	$gsrow = sqlsrv_fetch_array( $gsstmt, SQLSRV_FETCH_NUMERIC);

	if(empty($gsrow[0])){ //insert
        $PRBS = '';
        $PRL = '';
        $PRSC = '';
        if($_POST['template'] != ''){
            $mpbssksql = "select * from [dbo].[TemplateGeneralSeting] where NamaTemplate = '$_POST[template]'";  //simpanan sukarela
            $mpbsstmt = sqlsrv_query($conn, $mpbssksql);
            $mpbsrow = sqlsrv_fetch_array( $mpbsstmt, SQLSRV_FETCH_NUMERIC);
            $PRSC = $mpbsrow[3];
            $PRBS = $mpbsrow[1];
            $PRL = $mpbsrow[2];
        }
		$pgstsql = "exec [dbo].[ProsesGeneralSetting] '$_SESSION[KID]', '$PRBS', '$PRL', '$PRSC', '$BS', '$RSI', '$DCT', '$CRD', '$CRW', '$CRM', '$isRealTime', '$CDM', '$isRealDay', '$isInterestTax', '$TaxRate', '$MinimumTax','$isAkun','$isLimit'";
		$pgsstmt = sqlsrv_query($conn, $pgstsql);

		if($pgsstmt){
            messageAlert(lang('Berhasil menyimpan kedatabase'),'success');
            header('Location: mgeneralsetting.php');
		}
		else{
            messageAlert(lang('Gagal menyimpan kedatabase'),'danger');
            header('Location: mgeneralsetting.php');
		}
	}
	else{
        if($_POST['template'] != ''){
            $mpbssksql = "select * from [dbo].[TemplateGeneralSeting] where NamaTemplate = '$_POST[template]'";  //simpanan sukarela
            $mpbsstmt = sqlsrv_query($conn, $mpbssksql);
            $mpbsrow = sqlsrv_fetch_array( $mpbsstmt, SQLSRV_FETCH_NUMERIC);
            $PRBS=$mpbsrow[1];
            $PRL = $mpbsrow[2];
            $PRSC = $mpbsrow[3];
        }

        $pgsutsql = "update [dbo].[GeneralSetting] set PRBS='$PRBS', PRL='$PRL', PRSC='$PRSC', BS='$BS', RSI='$RSI', DCT='$DCT', CRD='$CRD', CRW='$CRW', CRM='$CRM', isRealTime='$isRealTime', CDM='$CDM', isRealDay='$isRealDay', isInterestTax='$isInterestTax', TaxRate='$TaxRate', MinimumTax='$MinimumTax', isAkun='$isAkun', StatusLimit='$isLimit' where KID='$_SESSION[KID]'";
		$pgsustmt = sqlsrv_query($conn, $pgsutsql);
		if($pgsustmt){
            messageAlert(lang('Berhasil memperbaharui data kedatabase'),'success');
            header('Location: mgeneralsetting.php');
		}
		else{
            messageAlert(lang('Gagal memperbaharui data kedatabase'),'danger');
            header('Location: mgeneralsetting.php');
		}
	}
}

?>