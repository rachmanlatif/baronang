/* Shield UI 1.7.36 Trial Version | Copyright 2013-2018 Shield UI Ltd. | http://www.shieldui.com/eula */
function a(b) {
    this.ok = !1, "#" == b.charAt(0) && (b = b.substr(1, 6)), b = b.replace(/ /g, ""), b = b.toLowerCase();
    var c = {
        aliceblue: "f0f8ff",
        antiquewhite: "faebd7",
        aqua: "00ffff",
        aquamarine: "7fffd4",
        azure: "f0ffff",
        beige: "f5f5dc",
        bisque: "ffe4c4",
        black: "000000",
        blanchedalmond: "ffebcd",
        blue: "0000ff",
        blueviolet: "8a2be2",
        brown: "a52a2a",
        burlywood: "deb887",
        cadetblue: "5f9ea0",
        chartreuse: "7fff00",
        chocolate: "d2691e",
        coral: "ff7f50",
        cornflowerblue: "6495ed",
        cornsilk: "fff8dc",
        crimson: "dc143c",
        cyan: "00ffff",
        darkblue: "00008b",
        darkcyan: "008b8b",
        darkgoldenrod: "b8860b",
        darkgray: "a9a9a9",
        darkgreen: "006400",
        darkkhaki: "bdb76b",
        darkmagenta: "8b008b",
        darkolivegreen: "556b2f",
        darkorange: "ff8c00",
        darkorchid: "9932cc",
        darkred: "8b0000",
        darksalmon: "e9967a",
        darkseagreen: "8fbc8f",
        darkslateblue: "483d8b",
        darkslategray: "2f4f4f",
        darkturquoise: "00ced1",
        darkviolet: "9400d3",
        deeppink: "ff1493",
        deepskyblue: "00bfff",
        dimgray: "696969",
        dodgerblue: "1e90ff",
        feldspar: "d19275",
        firebrick: "b22222",
        floralwhite: "fffaf0",
        forestgreen: "228b22",
        fuchsia: "ff00ff",
        gainsboro: "dcdcdc",
        ghostwhite: "f8f8ff",
        gold: "ffd700",
        goldenrod: "daa520",
        gray: "808080",
        green: "008000",
        greenyellow: "adff2f",
        honeydew: "f0fff0",
        hotpink: "ff69b4",
        indianred: "cd5c5c",
        indigo: "4b0082",
        ivory: "fffff0",
        khaki: "f0e68c",
        lavender: "e6e6fa",
        lavenderblush: "fff0f5",
        lawngreen: "7cfc00",
        lemonchiffon: "fffacd",
        lightblue: "add8e6",
        lightcoral: "f08080",
        lightcyan: "e0ffff",
        lightgoldenrodyellow: "fafad2",
        lightgrey: "d3d3d3",
        lightgreen: "90ee90",
        lightpink: "ffb6c1",
        lightsalmon: "ffa07a",
        lightseagreen: "20b2aa",
        lightskyblue: "87cefa",
        lightslateblue: "8470ff",
        lightslategray: "778899",
        lightsteelblue: "b0c4de",
        lightyellow: "ffffe0",
        lime: "00ff00",
        limegreen: "32cd32",
        linen: "faf0e6",
        magenta: "ff00ff",
        maroon: "800000",
        mediumaquamarine: "66cdaa",
        mediumblue: "0000cd",
        mediumorchid: "ba55d3",
        mediumpurple: "9370d8",
        mediumseagreen: "3cb371",
        mediumslateblue: "7b68ee",
        mediumspringgreen: "00fa9a",
        mediumturquoise: "48d1cc",
        mediumvioletred: "c71585",
        midnightblue: "191970",
        mintcream: "f5fffa",
        mistyrose: "ffe4e1",
        moccasin: "ffe4b5",
        navajowhite: "ffdead",
        navy: "000080",
        oldlace: "fdf5e6",
        olive: "808000",
        olivedrab: "6b8e23",
        orange: "ffa500",
        orangered: "ff4500",
        orchid: "da70d6",
        palegoldenrod: "eee8aa",
        palegreen: "98fb98",
        paleturquoise: "afeeee",
        palevioletred: "d87093",
        papayawhip: "ffefd5",
        peachpuff: "ffdab9",
        peru: "cd853f",
        pink: "ffc0cb",
        plum: "dda0dd",
        powderblue: "b0e0e6",
        purple: "800080",
        red: "ff0000",
        rosybrown: "bc8f8f",
        royalblue: "4169e1",
        saddlebrown: "8b4513",
        salmon: "fa8072",
        sandybrown: "f4a460",
        seagreen: "2e8b57",
        seashell: "fff5ee",
        sienna: "a0522d",
        silver: "c0c0c0",
        skyblue: "87ceeb",
        slateblue: "6a5acd",
        slategray: "708090",
        snow: "fffafa",
        springgreen: "00ff7f",
        steelblue: "4682b4",
        tan: "d2b48c",
        teal: "008080",
        thistle: "d8bfd8",
        tomato: "ff6347",
        turquoise: "40e0d0",
        violet: "ee82ee",
        violetred: "d02090",
        wheat: "f5deb3",
        white: "ffffff",
        whitesmoke: "f5f5f5",
        yellow: "ffff00",
        yellowgreen: "9acd32"
    };
    for (var d in c) b == d && (b = c[d]);
    for (var e = [{
        re: /^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/,
        example: ["rgb(123, 234, 45)", "rgb(255,234,245)"],
        process: function(a) {
            return [parseInt(a[1]), parseInt(a[2]), parseInt(a[3])]
        }
    }, {
        re: /^(\w{2})(\w{2})(\w{2})$/,
        example: ["#00ff00", "336699"],
        process: function(a) {
            return [parseInt(a[1], 16), parseInt(a[2], 16), parseInt(a[3], 16)]
        }
    }, {
        re: /^(\w{1})(\w{1})(\w{1})$/,
        example: ["#fb0", "f0f"],
        process: function(a) {
            return [parseInt(a[1] + a[1], 16), parseInt(a[2] + a[2], 16), parseInt(a[3] + a[3], 16)]
        }
    }], f = 0; f < e.length; f++) {
        var g = e[f].re,
            h = e[f].process,
            i = g.exec(b);
        i && (channels = h(i), this.r = channels[0], this.g = channels[1], this.b = channels[2], this.ok = !0)
    }
    this.r = this.r < 0 || isNaN(this.r) ? 0 : this.r > 255 ? 255 : this.r, this.g = this.g < 0 || isNaN(this.g) ? 0 : this.g > 255 ? 255 : this.g, this.b = this.b < 0 || isNaN(this.b) ? 0 : this.b > 255 ? 255 : this.b, this.toRGB = function() {
        return "rgb(" + this.r + ", " + this.g + ", " + this.b + ")"
    }, this.toHex = function() {
        var a = this.r.toString(16),
            b = this.g.toString(16),
            c = this.b.toString(16);
        return 1 == a.length && (a = "0" + a), 1 == b.length && (b = "0" + b), 1 == c.length && (c = "0" + c), "#" + a + b + c
    }, this.getHelpXML = function() {
        for (var b = new Array, d = 0; d < e.length; d++)
            for (var f = e[d].example, g = 0; g < f.length; g++) b[b.length] = f[g];
        for (var h in c) b[b.length] = h;
        var i = document.createElement("ul");
        i.setAttribute("id", "rgbcolor-examples");
        for (var d = 0; d < b.length; d++) try {
            var j = document.createElement("li"),
                k = new a(b[d]),
                l = document.createElement("div");
            l.style.cssText = "margin: 3px; border: 1px solid black; background:" + k.toHex() + "; color:" + k.toHex(), l.appendChild(document.createTextNode("test"));
            var m = document.createTextNode(" " + b[d] + " -> " + k.toRGB() + " -> " + k.toHex());
            j.appendChild(l), j.appendChild(m), i.appendChild(j)
        } catch (n) {}
        return i
    }
}! function(a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a : a(jQuery)
}(
    function(a) {
    function b(b) {
        var g = b || window.event,
            h = i.call(arguments, 1),
            j = 0,
            l = 0,
            m = 0,
            n = 0,
            o = 0,
            p = 0;
        if (b = a.event.fix(g), b.type = "mousewheel", "detail" in g && (m = g.detail * -1), "wheelDelta" in g && (m = g.wheelDelta), "wheelDeltaY" in g && (m = g.wheelDeltaY), "wheelDeltaX" in g && (l = g.wheelDeltaX * -1), "axis" in g && g.axis === g.HORIZONTAL_AXIS && (l = m * -1, m = 0), j = 0 === m ? l : m, "deltaY" in g && (m = g.deltaY * -1, j = m), "deltaX" in g && (l = g.deltaX, 0 === m && (j = l * -1)), 0 !== m || 0 !== l) {
            if (1 === g.deltaMode) {
                var q = a.data(this, "mousewheel-line-height");
                j *= q, m *= q, l *= q
            } else if (2 === g.deltaMode) {
                var r = a.data(this, "mousewheel-page-height");
                j *= r, m *= r, l *= r
            }
            if (n = Math.max(Math.abs(m), Math.abs(l)), (!f || n < f) && (f = n, d(g, n) && (f /= 40)), d(g, n) && (j /= 40, l /= 40, m /= 40), j = Math[j >= 1 ? "floor" : "ceil"](j / f), l = Math[l >= 1 ? "floor" : "ceil"](l / f), m = Math[m >= 1 ? "floor" : "ceil"](m / f), k.settings.normalizeOffset && this.getBoundingClientRect) {
                var s = this.getBoundingClientRect();
                o = b.clientX - s.left, p = b.clientY - s.top
            }
            return b.deltaX = l, b.deltaY = m, b.deltaFactor = f, b.offsetX = o, b.offsetY = p, b.deltaMode = 0, h.unshift(b, j, l, m), e && clearTimeout(e), e = setTimeout(c, 200), (a.event.dispatch || a.event.handle).apply(this, h)
        }
    }

    function c() {
        f = null
    }

    function d(a, b) {
        return k.settings.adjustOldDeltas && "mousewheel" === a.type && b % 120 === 0
    }
    var e, f, g = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
        h = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
        i = Array.prototype.slice;
    if (a.event.fixHooks)
        for (var j = g.length; j;) a.event.fixHooks[g[--j]] = a.event.mouseHooks;
    var k = a.event.special.mousewheel = {
        version: "3.1.12",
        setup: function() {
            if (this.addEventListener)
                for (var c = h.length; c;) this.addEventListener(h[--c], b, !1);
            else this.onmousewheel = b;
            a.data(this, "mousewheel-line-height", k.getLineHeight(this)), a.data(this, "mousewheel-page-height", k.getPageHeight(this))
        },
        teardown: function() {
            if (this.removeEventListener)
                for (var c = h.length; c;) this.removeEventListener(h[--c], b, !1);
            else this.onmousewheel = null;
            a.removeData(this, "mousewheel-line-height"), a.removeData(this, "mousewheel-page-height")
        },
        getLineHeight: function(b) {
            var c = a(b),
                d = c["offsetParent" in a.fn ? "offsetParent" : "parent"]();
            return d.length || (d = a("body")), parseInt(d.css("fontSize"), 10) || parseInt(c.css("fontSize"), 10) || 16
        },
        getPageHeight: function(b) {
            return a(b).height()
        },
        settings: {
            adjustOldDeltas: !0,
            normalizeOffset: !0
        }
    };
    a.fn.extend({
        mousewheel: function(a) {
            return a ? this.bind("mousewheel", a) : this.trigger("mousewheel")
        },
        unmousewheel: function(a) {
            return this.unbind("mousewheel", a)
        }
    })
}),
    function(a, b) {
        var c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z;
        c = function(a) {
            return new c.prototype.init(a)
        }, "undefined" != typeof require && "undefined" != typeof exports && "undefined" != typeof module ? module.exports = c : a.Globalize = c, c.cultures = {}, c.prototype = {
            constructor: c,
            init: function(a) {
                return this.cultures = c.cultures, this.cultureSelector = a, this
            }
        }, c.prototype.init.prototype = c.prototype, c.cultures["default"] = {
            name: "en",
            englishName: "English",
            nativeName: "English",
            isRTL: !1,
            language: "en",
            numberFormat: {
                pattern: ["-n"],
                decimals: 2,
                ",": ",",
                ".": ".",
                groupSizes: [3],
                "+": "+",
                "-": "-",
                NaN: "NaN",
                negativeInfinity: "-Infinity",
                positiveInfinity: "Infinity",
                percent: {
                    pattern: ["-n %", "n %"],
                    decimals: 2,
                    groupSizes: [3],
                    ",": ",",
                    ".": ".",
                    symbol: "%"
                },
                currency: {
                    pattern: ["($n)", "$n"],
                    decimals: 2,
                    groupSizes: [3],
                    ",": ",",
                    ".": ".",
                    symbol: "$"
                }
            },
            calendars: {
                standard: {
                    name: "Gregorian_USEnglish",
                    "/": "/",
                    ":": ":",
                    firstDay: 0,
                    days: {
                        names: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                        namesAbbr: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                        namesShort: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
                    },
                    months: {
                        names: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
                        namesAbbr: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""]
                    },
                    AM: ["AM", "am", "AM"],
                    PM: ["PM", "pm", "PM"],
                    eras: [{
                        name: "A.D.",
                        start: null,
                        offset: 0
                    }],
                    twoDigitYearMax: 2029,
                    patterns: {
                        d: "M/d/yyyy",
                        D: "dddd, MMMM dd, yyyy",
                        t: "h:mm tt",
                        T: "h:mm:ss tt",
                        f: "dddd, MMMM dd, yyyy h:mm tt",
                        F: "dddd, MMMM dd, yyyy h:mm:ss tt",
                        M: "MMMM dd",
                        Y: "yyyy MMMM",
                        S: "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
                    }
                }
            },
            messages: {}
        }, c.cultures["default"].calendar = c.cultures["default"].calendars.standard, c.cultures.en = c.cultures["default"], c.cultureSelector = "en", d = /^0x[a-f0-9]+$/i, e = /^[+\-]?infinity$/i, f = /^[+\-]?\d*\.?\d*(e[+\-]?\d+)?$/, g = /^\s+|\s+$/g, h = function(a, b) {
            if (a.indexOf) return a.indexOf(b);
            for (var c = 0, d = a.length; c < d; c++)
                if (a[c] === b) return c;
            return -1
        }, i = function(a, b) {
            return a.substr(a.length - b.length) === b
        }, j = function() {
            var a, c, d, e, f, g, h = arguments[0] || {},
                i = 1,
                n = arguments.length,
                o = !1;
            for ("boolean" == typeof h && (o = h, h = arguments[1] || {}, i = 2), "object" == typeof h || l(h) || (h = {}); i < n; i++)
                if (null != (a = arguments[i]))
                    for (c in a) d = h[c], e = a[c], h !== e && (o && e && (m(e) || (f = k(e))) ? (f ? (f = !1, g = d && k(d) ? d : []) : g = d && m(d) ? d : {}, h[c] = j(o, g, e)) : e !== b && (h[c] = e));
            return h
        }, k = Array.isArray || function(a) {
            return "[object Array]" === Object.prototype.toString.call(a)
        }, l = function(a) {
            return "[object Function]" === Object.prototype.toString.call(a)
        }, m = function(a) {
            return "[object Object]" === Object.prototype.toString.call(a)
        }, n = function(a, b) {
            return 0 === a.indexOf(b)
        }, o = function(a) {
            return (a + "").replace(g, "")
        }, p = function(a) {
            return isNaN(a) ? NaN : Math[a < 0 ? "ceil" : "floor"](a)
        }, q = function(a, b, c) {
            var d;
            for (d = a.length; d < b; d += 1) a = c ? "0" + a : a + "0";
            return a
        }, r = function(a, b) {
            for (var c = 0, d = !1, e = 0, f = a.length; e < f; e++) {
                var g = a.charAt(e);
                switch (g) {
                    case "'":
                        d ? b.push("'") : c++, d = !1;
                        break;
                    case "\\":
                        d && b.push("\\"), d = !d;
                        break;
                    default:
                        b.push(g), d = !1
                }
            }
            return c
        }, s = function(a, b) {
            b = b || "F";
            var c, d = a.patterns,
                e = b.length;
            if (1 === e) {
                if (c = d[b], !c) throw "Invalid date format string '" + b + "'.";
                b = c
            } else 2 === e && "%" === b.charAt(0) && (b = b.charAt(1));
            return b
        }, t = function(a, b, c) {
            function d(a, b) {
                var c, d = a + "";
                return b > 1 && d.length < b ? (c = u[b - 2] + d, c.substr(c.length - b, b)) : c = d
            }

            function e() {
                return o || p ? o : (o = y.test(b), p = !0, o)
            }

            function f(a, b) {
                if (q) return q[b];
                switch (b) {
                    case 0:
                        return a.getFullYear();
                    case 1:
                        return a.getMonth();
                    case 2:
                        return a.getDate();
                    default:
                        throw "Invalid part value " + b
                }
            }
            var g, h = c.calendar,
                i = h.convert;
            if (!b || !b.length || "i" === b) {
                if (c && c.name.length)
                    if (i) g = t(a, h.patterns.F, c);
                    else {
                        var j = new Date(a.getTime()),
                            k = w(a, h.eras);
                        j.setFullYear(x(a, h, k)), g = j.toLocaleString()
                    }
                else g = a.toString();
                return g
            }
            var l = h.eras,
                m = "s" === b;
            b = s(h, b), g = [];
            var n, o, p, q, u = ["0", "00", "000"],
                y = /([^d]|^)(d|dd)([^d]|$)/g,
                z = 0,
                A = v();
            for (!m && i && (q = i.fromGregorian(a));;) {
                var B = A.lastIndex,
                    C = A.exec(b),
                    D = b.slice(B, C ? C.index : b.length);
                if (z += r(D, g), !C) break;
                if (z % 2) g.push(C[0]);
                else {
                    var E = C[0],
                        F = E.length;
                    switch (E) {
                        case "ddd":
                        case "dddd":
                            var G = 3 === F ? h.days.namesAbbr : h.days.names;
                            g.push(G[a.getDay()]);
                            break;
                        case "d":
                        case "dd":
                            o = !0, g.push(d(f(a, 2), F));
                            break;
                        case "MMM":
                        case "MMMM":
                            var H = f(a, 1);
                            g.push(h.monthsGenitive && e() ? h.monthsGenitive[3 === F ? "namesAbbr" : "names"][H] : h.months[3 === F ? "namesAbbr" : "names"][H]);
                            break;
                        case "M":
                        case "MM":
                            g.push(d(f(a, 1) + 1, F));
                            break;
                        case "y":
                        case "yy":
                        case "yyyy":
                            H = q ? q[0] : x(a, h, w(a, l), m), F < 4 && (H %= 100), g.push(d(H, F));
                            break;
                        case "h":
                        case "hh":
                            n = a.getHours() % 12, 0 === n && (n = 12), g.push(d(n, F));
                            break;
                        case "H":
                        case "HH":
                            g.push(d(a.getHours(), F));
                            break;
                        case "m":
                        case "mm":
                            g.push(d(a.getMinutes(), F));
                            break;
                        case "s":
                        case "ss":
                            g.push(d(a.getSeconds(), F));
                            break;
                        case "t":
                        case "tt":
                            H = a.getHours() < 12 ? h.AM ? h.AM[0] : " " : h.PM ? h.PM[0] : " ", g.push(1 === F ? H.charAt(0) : H);
                            break;
                        case "f":
                        case "ff":
                        case "fff":
                            g.push(d(a.getMilliseconds(), 3).substr(0, F));
                            break;
                        case "z":
                        case "zz":
                            n = a.getTimezoneOffset() / 60, g.push((n <= 0 ? "+" : "-") + d(Math.floor(Math.abs(n)), F));
                            break;
                        case "zzz":
                            n = a.getTimezoneOffset() / 60, g.push((n <= 0 ? "+" : "-") + d(Math.floor(Math.abs(n)), 2) + ":" + d(Math.abs(a.getTimezoneOffset() % 60), 2));
                            break;
                        case "g":
                        case "gg":
                            h.eras && g.push(h.eras[w(a, l)].name);
                            break;
                        case "/":
                            g.push(h["/"]);
                            break;
                        default:
                            throw "Invalid date format pattern '" + E + "'."
                    }
                }
            }
            return g.join("")
        },
            function() {
                var a;
                a = function(a, b, c) {
                    var d = c.groupSizes,
                        e = d[0],
                        f = 1,
                        g = Math.pow(10, b),
                        h = Math.round(a * g) / g;
                    isFinite(h) || (h = a), a = h;
                    var i = a + "",
                        j = "",
                        k = i.split(/e/i),
                        l = k.length > 1 ? parseInt(k[1], 10) : 0;
                    i = k[0], k = i.split("."), i = k[0], j = k.length > 1 ? k[1] : "";
                    l > 0 ? (j = q(j, l, !1), i += j.slice(0, l), j = j.substr(l)) : l < 0 && (l = -l, i = q(i, l + 1, !0), j = i.slice(-l, i.length) + j, i = i.slice(0, -l)), j = b > 0 ? c["."] + (j.length > b ? j.slice(0, b) : q(j, b)) : "";
                    for (var m = i.length - 1, n = c[","], o = ""; m >= 0;) {
                        if (0 === e || e > m) return i.slice(0, m + 1) + (o.length ? n + o + j : j);
                        o = i.slice(m - e + 1, m + 1) + (o.length ? n + o : ""), m -= e, f < d.length && (e = d[f], f++)
                    }
                    return i.slice(0, m + 1) + n + o + j
                }, u = function(b, c, d) {
                    if (!isFinite(b)) return b === 1 / 0 ? d.numberFormat.positiveInfinity : b === -(1 / 0) ? d.numberFormat.negativeInfinity : d.numberFormat.NaN;
                    if (!c || "i" === c) return d.name.length ? b.toLocaleString() : b.toString();
                    c = c || "D";
                    var e, f = d.numberFormat,
                        g = Math.abs(b),
                        h = -1;
                    c.length > 1 && (h = parseInt(c.slice(1), 10));
                    var i, j = c.charAt(0).toUpperCase();
                    switch (j) {
                        case "D":
                            e = "n", g = p(g), h !== -1 && (g = q("" + g, h, !0)), b < 0 && (g = "-" + g);
                            break;
                        case "N":
                            i = f;
                        case "C":
                            i = i || f.currency;
                        case "P":
                            i = i || f.percent, e = b < 0 ? i.pattern[0] : i.pattern[1] || "n", h === -1 && (h = i.decimals), g = a(g * ("P" === j ? 100 : 1), h, i);
                            break;
                        default:
                            throw "Bad number format specifier: " + j
                    }
                    for (var k = /n|\$|-|%/g, l = "";;) {
                        var m = k.lastIndex,
                            n = k.exec(e);
                        if (l += e.slice(m, n ? n.index : e.length), !n) break;
                        switch (n[0]) {
                            case "n":
                                l += g;
                                break;
                            case "$":
                                l += f.currency.symbol;
                                break;
                            case "-":
                                /[1-9]/.test(g) && (l += f["-"]);
                                break;
                            case "%":
                                l += f.percent.symbol
                        }
                    }
                    return l
                }
            }(), v = function() {
            return /\/|dddd|ddd|dd|d|MMMM|MMM|MM|M|yyyy|yy|y|hh|h|HH|H|mm|m|ss|s|tt|t|fff|ff|f|zzz|zz|z|gg|g/g
        }, w = function(a, b) {
            if (!b) return 0;
            for (var c, d = a.getTime(), e = 0, f = b.length; e < f; e++)
                if (c = b[e].start, null === c || d >= c) return e;
            return 0
        }, x = function(a, b, c, d) {
            var e = a.getFullYear();
            return !d && b.eras && (e -= b.eras[c].offset), e
        },
            function() {
                var a, b, c, d, e, f, g;
                a = function(a, b) {
                    if (b < 100) {
                        var c = new Date,
                            d = w(c),
                            e = x(c, a, d),
                            f = a.twoDigitYearMax;
                        f = "string" == typeof f ? (new Date).getFullYear() % 100 + parseInt(f, 10) : f, b += e - e % 100, b > f && (b -= 100)
                    }
                    return b
                }, b = function(a, b, c) {
                    var d, e = a.days,
                        i = a._upperDays;
                    return i || (a._upperDays = i = [g(e.names), g(e.namesAbbr), g(e.namesShort)]), b = f(b), c ? (d = h(i[1], b), d === -1 && (d = h(i[2], b))) : d = h(i[0], b), d
                }, c = function(a, b, c) {
                    var d = a.months,
                        e = a.monthsGenitive || a.months,
                        i = a._upperMonths,
                        j = a._upperMonthsGen;
                    i || (a._upperMonths = i = [g(d.names), g(d.namesAbbr)], a._upperMonthsGen = j = [g(e.names), g(e.namesAbbr)]), b = f(b);
                    var k = h(c ? i[1] : i[0], b);
                    return k < 0 && (k = h(c ? j[1] : j[0], b)), k
                }, d = function(a, b) {
                    var c = a._parseRegExp;
                    if (c) {
                        var d = c[b];
                        if (d) return d
                    } else a._parseRegExp = c = {};
                    for (var e, f = s(a, b).replace(/([\^\$\.\*\+\?\|\[\]\(\)\{\}])/g, "\\\\$1"), g = ["^"], h = [], i = 0, j = 0, k = v(); null !== (e = k.exec(f));) {
                        var l = f.slice(i, e.index);
                        if (i = k.lastIndex, j += r(l, g), j % 2) g.push(e[0]);
                        else {
                            var m, n = e[0],
                                o = n.length;
                            switch (n) {
                                case "dddd":
                                case "ddd":
                                case "MMMM":
                                case "MMM":
                                case "gg":
                                case "g":
                                    m = "(\\D+)";
                                    break;
                                case "tt":
                                case "t":
                                    m = "(\\D*)";
                                    break;
                                case "yyyy":
                                case "fff":
                                case "ff":
                                case "f":
                                    m = "(\\d{" + o + "})";
                                    break;
                                case "dd":
                                case "d":
                                case "MM":
                                case "M":
                                case "yy":
                                case "y":
                                case "HH":
                                case "H":
                                case "hh":
                                case "h":
                                case "mm":
                                case "m":
                                case "ss":
                                case "s":
                                    m = "(\\d\\d?)";
                                    break;
                                case "zzz":
                                    m = "([+-]?\\d\\d?:\\d{2})";
                                    break;
                                case "zz":
                                case "z":
                                    m = "([+-]?\\d\\d?)";
                                    break;
                                case "/":
                                    m = "(\\/)";
                                    break;
                                default:
                                    throw "Invalid date format pattern '" + n + "'."
                            }
                            m && g.push(m), h.push(e[0])
                        }
                    }
                    r(f.slice(i), g), g.push("$");
                    var p = g.join("").replace(/\s+/g, "\\s+"),
                        q = {
                            regExp: p,
                            groups: h
                        };
                    return c[b] = q
                }, e = function(a, b, c) {
                    return a < b || a > c
                }, f = function(a) {
                    return a.split("Â ").join(" ").toUpperCase()
                }, g = function(a) {
                    for (var b = [], c = 0, d = a.length; c < d; c++) b[c] = f(a[c]);
                    return b
                }, y = function(f, g, h) {
                    f = o(f);
                    var i = h.calendar,
                        j = d(i, g),
                        k = new RegExp(j.regExp).exec(f);
                    if (null === k) return null;
                    for (var l, m = j.groups, p = null, q = null, r = null, s = null, t = null, u = 0, v = 0, w = 0, x = 0, y = null, z = !1, A = 0, B = m.length; A < B; A++) {
                        var C = k[A + 1];
                        if (C) {
                            var D = m[A],
                                E = D.length,
                                F = parseInt(C, 10);
                            switch (D) {
                                case "dd":
                                case "d":
                                    if (s = F, e(s, 1, 31)) return null;
                                    break;
                                case "MMM":
                                case "MMMM":
                                    if (r = c(i, C, 3 === E), e(r, 0, 11)) return null;
                                    break;
                                case "M":
                                case "MM":
                                    if (r = F - 1, e(r, 0, 11)) return null;
                                    break;
                                case "y":
                                case "yy":
                                case "yyyy":
                                    if (q = E < 4 ? a(i, F) : F, e(q, 0, 9999)) return null;
                                    break;
                                case "h":
                                case "hh":
                                    if (u = F, 12 === u && (u = 0), e(u, 0, 11)) return null;
                                    break;
                                case "H":
                                case "HH":
                                    if (u = F, e(u, 0, 23)) return null;
                                    break;
                                case "m":
                                case "mm":
                                    if (v = F, e(v, 0, 59)) return null;
                                    break;
                                case "s":
                                case "ss":
                                    if (w = F, e(w, 0, 59)) return null;
                                    break;
                                case "tt":
                                case "t":
                                    if (z = i.PM && (C === i.PM[0] || C === i.PM[1] || C === i.PM[2]), !z && (!i.AM || C !== i.AM[0] && C !== i.AM[1] && C !== i.AM[2])) return null;
                                    break;
                                case "f":
                                case "ff":
                                case "fff":
                                    if (x = F * Math.pow(10, 3 - E), e(x, 0, 999)) return null;
                                    break;
                                case "ddd":
                                case "dddd":
                                    if (t = b(i, C, 3 === E), e(t, 0, 6)) return null;
                                    break;
                                case "zzz":
                                    var G = C.split(/:/);
                                    if (2 !== G.length) return null;
                                    if (l = parseInt(G[0], 10), e(l, -12, 13)) return null;
                                    var H = parseInt(G[1], 10);
                                    if (e(H, 0, 59)) return null;
                                    y = 60 * l + (n(C, "-") ? -H : H);
                                    break;
                                case "z":
                                case "zz":
                                    if (l = F, e(l, -12, 13)) return null;
                                    y = 60 * l;
                                    break;
                                case "g":
                                case "gg":
                                    var I = C;
                                    if (!I || !i.eras) return null;
                                    I = o(I.toLowerCase());
                                    for (var J = 0, K = i.eras.length; J < K; J++)
                                        if (I === i.eras[J].name.toLowerCase()) {
                                            p = J;
                                            break
                                        }
                                    if (null === p) return null
                            }
                        }
                    }
                    var L, M = new Date,
                        N = i.convert;
                    if (L = N ? N.fromGregorian(M)[0] : M.getFullYear(), null === q ? q = L : i.eras && (q += i.eras[p || 0].offset), null === r && (r = 0), null === s && (s = 1), N) {
                        if (M = N.toGregorian(q, r, s), null === M) return null
                    } else {
                        if (M.setFullYear(q, r, s), M.getDate() !== s) return null;
                        if (null !== t && M.getDay() !== t) return null
                    }
                    if (z && u < 12 && (u += 12), M.setHours(u, v, w, x), null !== y) {
                        var O = M.getMinutes() - (y + M.getTimezoneOffset());
                        M.setHours(M.getHours() + parseInt(O / 60, 10), O % 60)
                    }
                    return M
                }
            }(), z = function(a, b, c) {
            var d, e = b["-"],
                f = b["+"];
            switch (c) {
                case "n -":
                    e = " " + e, f = " " + f;
                case "n-":
                    i(a, e) ? d = ["-", a.substr(0, a.length - e.length)] : i(a, f) && (d = ["+", a.substr(0, a.length - f.length)]);
                    break;
                case "- n":
                    e += " ", f += " ";
                case "-n":
                    n(a, e) ? d = ["-", a.substr(e.length)] : n(a, f) && (d = ["+", a.substr(f.length)]);
                    break;
                case "(n)":
                    n(a, "(") && i(a, ")") && (d = ["-", a.substr(1, a.length - 2)])
            }
            return d || ["", a]
        }, c.prototype.findClosestCulture = function(a) {
            return c.findClosestCulture.call(this, a)
        }, c.prototype.format = function(a, b, d) {
            return c.format.call(this, a, b, d)
        }, c.prototype.localize = function(a, b) {
            return c.localize.call(this, a, b)
        }, c.prototype.parseInt = function(a, b, d) {
            return c.parseInt.call(this, a, b, d)
        }, c.prototype.parseFloat = function(a, b, d) {
            return c.parseFloat.call(this, a, b, d)
        }, c.prototype.culture = function(a) {
            return c.culture.call(this, a)
        }, c.addCultureInfo = function(a, b, c) {
            var d = {},
                e = !1;
            "string" != typeof a ? (c = a, a = this.culture().name, d = this.cultures[a]) : "string" != typeof b ? (c = b, e = null == this.cultures[a], d = this.cultures[a] || this.cultures["default"]) : (e = !0, d = this.cultures[b]), this.cultures[a] = j(!0, {}, d, c), e && (this.cultures[a].calendar = this.cultures[a].calendars.standard)
        }, c.findClosestCulture = function(a) {
            var b;
            if (!a) return this.findClosestCulture(this.cultureSelector) || this.cultures["default"];
            if ("string" == typeof a && (a = a.split(",")), k(a)) {
                var c, d, e = this.cultures,
                    f = a,
                    g = f.length,
                    h = [];
                for (d = 0; d < g; d++) {
                    a = o(f[d]);
                    var i, j = a.split(";");
                    c = o(j[0]), 1 === j.length ? i = 1 : (a = o(j[1]), 0 === a.indexOf("q=") ? (a = a.substr(2), i = parseFloat(a), i = isNaN(i) ? 0 : i) : i = 1), h.push({
                        lang: c,
                        pri: i
                    })
                }
                for (h.sort(function(a, b) {
                    return a.pri < b.pri ? 1 : a.pri > b.pri ? -1 : 0
                }), d = 0; d < g; d++)
                    if (c = h[d].lang, b = e[c]) return b;
                for (d = 0; d < g; d++)
                    for (c = h[d].lang;;) {
                        var l = c.lastIndexOf("-");
                        if (l === -1) break;
                        if (c = c.substr(0, l), b = e[c]) return b
                    }
                for (d = 0; d < g; d++) {
                    c = h[d].lang;
                    for (var m in e) {
                        var n = e[m];
                        if (n.language == c) return n
                    }
                }
            } else if ("object" == typeof a) return a;
            return b || null
        }, c.format = function(a, b, c) {
            var d = this.findClosestCulture(c);
            return a instanceof Date ? a = t(a, b, d) : "number" == typeof a && (a = u(a, b, d)), a
        }, c.localize = function(a, b) {
            return this.findClosestCulture(b).messages[a] || this.cultures["default"].messages[a]
        }, c.parseDate = function(a, b, c) {
            c = this.findClosestCulture(c);
            var d, e, f;
            if (b) {
                if ("string" == typeof b && (b = [b]), b.length)
                    for (var g = 0, h = b.length; g < h; g++) {
                        var i = b[g];
                        if (i && (d = y(a, i, c))) break
                    }
            } else {
                f = c.calendar.patterns;
                for (e in f)
                    if (d = y(a, f[e], c)) break
            }
            return d || null
        }, c.parseInt = function(a, b, d) {
            return p(c.parseFloat(a, b, d))
        }, c.parseFloat = function(a, b, c) {
            "number" != typeof b && (c = b, b = 10);
            var g = this.findClosestCulture(c),
                h = NaN,
                i = g.numberFormat;
            if (a.indexOf(g.numberFormat.currency.symbol) > -1 && (a = a.replace(g.numberFormat.currency.symbol, ""), a = a.replace(g.numberFormat.currency["."], g.numberFormat["."])), a.indexOf(g.numberFormat.percent.symbol) > -1 && (a = a.replace(g.numberFormat.percent.symbol, "")), a = a.replace(/ /g, ""), e.test(a)) h = parseFloat(a);
            else if (!b && d.test(a)) h = parseInt(a, 16);
            else {
                var j = z(a, i, i.pattern[0]),
                    k = j[0],
                    l = j[1];
                "" === k && "(n)" !== i.pattern[0] && (j = z(a, i, "(n)"), k = j[0], l = j[1]), "" === k && "-n" !== i.pattern[0] && (j = z(a, i, "-n"), k = j[0], l = j[1]), k = k || "+";
                var m, n, o = l.indexOf("e");
                o < 0 && (o = l.indexOf("E")), o < 0 ? (n = l, m = null) : (n = l.substr(0, o), m = l.substr(o + 1));
                var p, q, r = i["."],
                    s = n.indexOf(r);
                s < 0 ? (p = n, q = null) : (p = n.substr(0, s), q = n.substr(s + r.length));
                var t = i[","];
                p = p.split(t).join("");
                var u = t.replace(/\u00A0/g, " ");
                t !== u && (p = p.split(u).join(""));
                var v = k + p;
                if (null !== q && (v += "." + q), null !== m) {
                    var w = z(m, i, "-n");
                    v += "e" + (w[0] || "+") + w[1]
                }
                f.test(v) && (h = parseFloat(v))
            }
            return h
        }, c.culture = function(a) {
            return "undefined" != typeof a && (this.cultureSelector = a), this.findClosestCulture(a) || this.cultures["default"]
        }
    }(this),
    function(a, b, c) {
        function d(a) {
            var b, c = "";
            a = Math.abs(a);
            do b = a % 26, c = String.fromCharCode(b + 97) + c, a = (a - b) / 26; while (a > 0);
            return c
        }

        function e() {
            return D + d(Q++)
        }

        function f() {
            return R++
        }

        function g() {
            var a = [8, 9, "a", "b"],
                b = function() {
                    return (65536 * (1 + Math.random()) | 0).toString(16).substring(1)
                };
            return b() + b() + "-" + b() + "-4" + b().substr(0, 3) + "-" + a[Math.floor(4 * Math.random())] + b().substr(0, 3) + "-" + b() + b() + b()
        }

        function h(a, b) {
            return typeof Globalize === H && typeof Globalize.format === H ? Globalize.parseDate(a, b) : new Date(a)
        }

        function i(a) {
            var b;
            return b = _.defined(a) && typeof Globalize === H && typeof Globalize.parseFloat === H ? Globalize.parseFloat(a) : Number(a), isNaN(b) ? null : b
        }

        function j(a) {
            if (!_.defined(a) || "" === a) return "";
            var b = /\{([\.\d\w\:\-\/\' \[\]]+)\}/g,
                c = arguments,
                d = c && c.length > 1 && typeof c[1] === F,
                e = typeof Globalize === H && typeof Globalize.format === H;
            return a.replace(b, function(a, b) {
                var f, g, h = b.indexOf(":");
                if (h > 0) {
                    var i = b;
                    b = i.substring(0, h), g = i.substring(h + 1)
                }
                return f = d ? /^\d+$/.test(b) ? c[parseInt(b, 10) + 1] : r(c[1], b) : c[parseInt(b, 10) + 1], g && e && (f = Globalize.format(f, g)), f
            })
        }

        function k(a) {
            if (_.func(a)) {
                var b = [].slice.call(arguments);
                return b.shift(), a.apply(this, b)
            }
            return j.apply(this, arguments)
        }

        function l() {
            return typeof Globalize === H && Globalize.cultures && Globalize.cultures[Globalize.cultureSelector] && Globalize.cultures[Globalize.cultureSelector].calendar ? Globalize.cultures[Globalize.cultureSelector].calendar : {
                days: {
                    names: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                    namesAbbr: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                    namesShort: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
                },
                firstDay: 0,
                months: {
                    names: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ""],
                    namesAbbr: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", ""]
                }
            }
        }

        function m() {
            return typeof Globalize === H && Globalize.cultures && Globalize.cultures[Globalize.cultureSelector] && Globalize.cultures[Globalize.cultureSelector].numberFormat ? Globalize.cultures[Globalize.cultureSelector].numberFormat.currency : {
                pattern: ["($n)", "$n"],
                decimals: 2,
                groupSizes: [3],
                ",": ",",
                ".": ".",
                symbol: "$"
            }
        }

        function n(a, d) {
            if (d = d !== c ? d : x.dieOnError) throw new Error(a);
            b.console && (b.console.error ? console.error(a) : console.log(a))
        }

        function o(a) {
            var b = Object.prototype.toString.call(a);
            return null === a ? "null" : a === c ? "undefined" : b.substr(8, b.length - 9).toLowerCase()
        }

        function p(b) {
            var d, e, f, g, h, i = Array.apply(null, arguments),
                j = _.array(b) && b.length && !z(b, function(a) {
                    return !_.func(a)
                }).length,
                k = i[1],
                l = function(a, b) {
                    return a instanceof b
                };
            if (!j) return y.apply(a, i);
            for (_.object(k) || _.array(k) || (k = {}), e = 2; e < i.length; e++)
                if (d = i[e])
                    for (f in d) d.hasOwnProperty(f) && (h = d[f], g = k[f], h && z(b, A(l, null, h)).length ? k[f] = h : _.object(h) || _.array(h) ? (_.object(g) || (g = k[f] = _.array(h) ? [] : {}), k[f] = p(b, g, h)) : _.date(h) ? k[f] = new Date(h.getTime()) : h !== c && (k[f] = h));
            return k
        }

        function q(a) {
            var b, c = [];
            for (b in a) a.hasOwnProperty(b) && c.push(b);
            return c
        }

        function r(a, b) {
            var c;
            if (!_.string(b)) throw new Error("shield.get: parameter 'path' must be a string.");
            if (b = t(b), c = T[b]) return c(a);
            try {
                c = new Function("a", "try{return a" + b + "}catch(e){return arguments[1];}")
            } catch (d) {
                throw new Error("shield.get: invalid 'path' parameter")
            }
            return T[b] = c, c(a)
        }

        function s(a, b, c) {
            var d;
            if (!_.string(b)) throw new Error("shield.set: parameter 'path' must be a string");
            if (b = t(b), d = U[b]) return d(a, c);
            try {
                d = new Function("obj,val", "obj" + b + "=val;")
            } catch (e) {
                throw new Error("shield.set: invalid 'path' parameter")
            }
            return U[b] = d, d(a, c), a
        }

        function t(a) {
            var b, c, d, e, f, g = a.split(".");
            for (c = 0, d = g.length; c < d; c++) e = g[c], b = e.indexOf("["), e && (b < 0 ? (f = e.indexOf("'") < 0 ? "'" : '"', e = g[c] = "[" + f + e + f + "]") : b > 0 && (e = g[c] = "." + e));
            return g.join("")
        }

        function u(b, c) {
            var d, e, f = [],
                g = a(b).length,
                h = c ? E + "-" + c : E;
            for (e = 0; e < g; e++) d = a(a(b)[e]).data(h), d && f.push(d);
            return f
        }

        function v(b, d) {
            x.ui[b] = d, a.fn[D + b] = function(e) {
                var f, g = this;
                return typeof e === G ? (f = [].slice.call(arguments, 1), this.each(function() {
                    var d, h, i = a(this).data(E);
                    if (!i) throw new Error(k("shield: cannot call method '{0}' on uninitialized {1}.", e, b));
                    if (d = i[e], typeof d !== H) throw new Error(k("shield: cannot find method '{0}' of {1}", e, b));
                    if (h = d.apply(i, f), h !== c) return g = h, !1
                }), g) : this.each(function() {
                    var c = new d(this, e);
                    a(this).data(E, c), a(this).data(E + "-" + b, c)
                })
            }
        }

        function w(a) {
            a ? C.onselectstart == w.handler && (C.onselectstart = w.onselectstart, C.ondragstart = w.ondragstart) : C.onselectstart != w.handler && (w.onselectstart = C.onselectstart, w.ondragstart = C.ondragstart, C.onselectstart = C.ondragstart = w.handler)
        }
        var x = b.shield = b.shield || {},
            y = a.extend,
            z = a.grep,
            A = a.proxy,
            B = navigator.userAgent,
            C = document,
            D = "shield",
            E = "shieldWidget",
            F = "object",
            G = "string",
            H = "function",
            I = "array",
            J = "number",
            K = "date",
            L = "boolean",
            M = "null",
            N = "undefined",
            O = "sui-vc-top",
            P = function() {},
            Q = 100,
            R = 1e3,
            S = {},
            T = {},
            U = {},
            V = function() {},
            W = {
                SVG_NS: "http://www.w3.org/2000/svg",
                XHTML_NS: "http://www.w3.org/1999/xhtml",
                KeyCode: {
                    BACK: 8,
                    TAB: 9,
                    ENTER: 13,
                    CTRL: 17,
                    ESC: 27,
                    SPACE: 32,
                    PAGEUP: 33,
                    PAGEDOWN: 34,
                    END: 35,
                    HOME: 36,
                    LEFT: 37,
                    UP: 38,
                    RIGHT: 39,
                    DOWN: 40,
                    DEL: 46
                }
            };
        V.extend = function(a) {
            var b, c, d, e, f = this,
                g = a && a.init ? a.init : function() {
                    f.apply(this, arguments)
                },
                h = function() {};
            h.prototype = f.prototype, b = g.fn = g.prototype = new h;
            for (c in a) a.hasOwnProperty(c) && (d = a[c], e = o(d), d && typeof d === F ? b[c] = y(!0, e === I ? [] : {}, f.prototype[c], d) : b[c] = d);
            return b.constructor = g, g.extend = f.extend, g
        }, Date.prototype.toISOString || ! function() {
            function a(a) {
                var b = String(a);
                return 1 === b.length && (b = "0" + b), b
            }
            Date.prototype.toISOString = function() {
                return this.getUTCFullYear() + "-" + a(this.getUTCMonth() + 1) + "-" + a(this.getUTCDate()) + "T" + a(this.getUTCHours()) + ":" + a(this.getUTCMinutes()) + ":" + a(this.getUTCSeconds()) + "." + String((this.getUTCMilliseconds() / 1e3).toFixed(3)).slice(2, 5) + "Z"
            }
        }();
        var X = V.extend({
                init: function(a) {
                    var b, c = this,
                        d = (a || {}).events;
                    c.events = {}, a = a || {};
                    for (b in d) typeof d[b] === H && c.on(b, d[b])
                },
                on: function(a, b, c) {
                    var d, e, f, g, h, i = this,
                        j = i.events,
                        k = aa.array(a),
                        l = _.func(b);
                    if (_.object(a)) {
                        k = [];
                        for (h in a) a.hasOwnProperty(h) && k.push(h);
                        l = !1, b = a
                    }
                    for (f = 0, g = k.length; f < g; f++) a = k[f], d = i._eventType(a), e = l ? b : b[a], _.func(e) && (c && (e = i._one(a, e)), (j[d] || (j[d] = [])).push({
                        name: a,
                        handler: e
                    }));
                    return i
                },
                _one: function(a, b) {
                    var c = this,
                        d = function() {
                            c.off(a, d), b.apply(this, arguments)
                        };
                    return d
                },
                _eventType: function(a) {
                    var b = (a += "").indexOf(".");
                    return b > -1 ? a.substring(0, b) : a
                },
                _eventNameMatch: function(b, c) {
                    var d, e, f, g, h = this,
                        i = h._eventType(b),
                        j = h._eventType(c);
                    if (b += "", c += "", (i === j || !j) && (d = b.split("."), d.shift(), d = z(d, function(a) {
                        return _.string(a) && a.length > 0
                    }), e = c.split("."), j && e.shift(), e = z(e, function(a) {
                        return _.string(a) && a.length > 0
                    }), d && e)) {
                        for (f = 0, g = 0; g < e.length; g++) a.inArray(e[g], d) > -1 && f++;
                        return f >= e.length
                    }
                    return !1
                },
                one: function(a, b) {
                    this.on(a, b, !0)
                },
                off: function(a, b) {
                    var d, e, f, g, h, i, j = this,
                        k = j.events,
                        l = aa.array(a),
                        m = _.func(b);
                    if (_.object(a)) {
                        l = [];
                        for (i in a) a.hasOwnProperty(i) && l.push(i);
                        m = !1, b = a
                    }
                    if (_.string(a) && 0 === a.indexOf(".")) {
                        for (d in k)
                            if (k.hasOwnProperty(d)) {
                                for (e = k[d] || [], f = m ? b : b || c, h = e.length - 1; h >= 0; h--) !j._eventNameMatch(e[h].name, a) || _.defined(f) && e[h].handler !== f || e.splice(h, 1);
                                e.length || delete k[d]
                            }
                    } else
                        for (g = 0; g < l.length; g++) {
                            for (a = l[g], d = j._eventType(a), e = k[d] || [], f = m ? b : (b || {})[a], h = e.length - 1; h >= 0; h--) !j._eventNameMatch(e[h].name, a) || _.defined(f) && e[h].handler !== f || e.splice(h, 1);
                            e.length || delete k[d]
                        }
                    return j
                },
                trigger: function(a, b) {
                    var c, d, e = this,
                        f = e._eventType(a),
                        g = (e.events[f] || []).slice();
                    for (c = 0, d = g.length; c < d; c++) g[c].handler.apply(e, [].slice.call(arguments, 1));
                    return b
                },
                destroy: function() {
                    this.events = {}
                }
            }),
            Y = V.extend({
                init: function(a) {
                    var b = !1,
                        c = !1;
                    y(this, {
                        timeStamp: (new Date).getTime(),
                        isDefaultPrevented: function() {
                            return b
                        },
                        isPropagationStopped: function() {
                            return c
                        },
                        preventDefault: function() {
                            b = !0
                        },
                        stopPropagation: function() {
                            c = !0
                        }
                    }, a)
                }
            }),
            Z = X.extend({
                init: function(b, c) {
                    var d = this,
                        e = d.constructor;
                    c = c || {}, d.element = a(b), d.initialOptions = c, d.options = y(!0, {}, e.defaults, (e.themes || {})[c.theme], c), d._iid = f(), X.fn.init.call(d, c)
                },
                getInstanceId: function() {
                    return this._iid
                },
                refresh: function(a) {
                    this.refreshWithElement(this.element, a)
                },
                refreshWithElement: function(a, b) {
                    var c = this,
                        d = p([V], c.options, b);
                    c.destroy(), c.init(a, d)
                },
                hide: function() {
                    a(this.element).hide()
                },
                show: function() {
                    a(this.element).show()
                },
                isVisible: function() {
                    return a(this.element).is(":visible")
                },
                visible: function() {
                    var a = this,
                        b = [].slice.call(arguments);
                    return b.length > 0 ? void(b[0] ? a.show() : a.hide()) : a.isVisible()
                },
                focus: function() {
                    a(this.element).focus()
                },
                trigger: function(b, c, d) {
                    var e, f = this;
                    return c = _.event(c) ? {
                        domEvent: c
                    } : c, !c || !c.domEvent || c.domEvent instanceof a.Event || (c.domEvent = a.Event(c.domEvent)), e = new Y(y({
                        type: b,
                        target: f
                    }, c)), X.fn.trigger.call(f, b, e), typeof d !== H || e.isDefaultPrevented() || d.call(f, e), e
                }
            }),
            $ = X.extend({
                options: {
                    total: 0,
                    pageBuffer: 2,
                    createContainer: P,
                    getItems: P,
                    eventNS: ".shieldVirtualized",
                    skipRender: !1
                },
                init: function(b, c) {
                    var d = this;
                    c = d.options = y({}, d.options, c), d.element = a(b), X.fn.init.call(d, c), c.skipRender || d.render()
                },
                _elements: function() {
                    var b, c, d = this,
                        e = d.element,
                        f = d.options;
                    b = d.wrapper = e.off(f.eventNS).empty().css({
                        overflow: "auto",
                        position: "relative"
                    }).on("scroll" + f.eventNS, A(d.scroll, d)).append('<div class="sui-virtualized"/>').find(".sui-virtualized").css({
                            position: "relative",
                            overflow: "visible"
                        }), c = d.container = a(f.createContainer(b)), b.children().css({
                        position: "absolute",
                        top: 0
                    }), d._positionedContainer = b.children().first()
                },
                _dimensions: function() {
                    var a = this,
                        b = a.options,
                        c = b.total,
                        d = b.itemHeight,
                        e = Math.min(c * d, x.support.maxElementHeight),
                        f = a.element.height(),
                        g = Math.ceil(f / d),
                        h = g * d,
                        i = e - f;
                    return {
                        total: c,
                        itemHeight: d,
                        totalHeight: e,
                        viewportHeight: f,
                        itemsPerPage: g,
                        pageHeight: h,
                        totalScrollableHeight: i
                    }
                },
                render: function() {
                    var a, b = this,
                        c = b.element,
                        d = b.options;
                    b._elements(), b.prevScroll = 0, a = b.dimensions = b._dimensions(), c.find(".sui-virtualized").height(a.totalHeight), b._renderItems(0, Math.min(d.total, (d.pageBuffer + 1) * a.itemsPerPage))
                },
                _renderItems: function(a, b, c) {
                    var d = this;
                    d.options.getItems(a, b, function(a, b) {
                        var e, f = a.length;
                        for (b = !_.defined(b) || !!b, b && d.container.empty(), e = 0; e < f; e++) d.container.append(a[e]);
                        c && c()
                    })
                },
                scroll: function() {
                    var a, b, c, d = this,
                        e = d.options,
                        f = e.pageBuffer,
                        g = d.dimensions,
                        h = d.element.scrollTop(),
                        i = d.prevScroll,
                        j = h - i,
                        k = h / g.totalScrollableHeight,
                        l = d._positionedContainer,
                        m = l.data(O) || 0,
                        n = h - k * (g.pageHeight - g.viewportHeight),
                        o = j > 0 && n - m > (f / 4 + 1) * g.pageHeight,
                        p = j < 0 && n - m <= f / 4 * g.pageHeight;
                    (p || o) && (b = Math.min(g.total, Math.floor(k * g.total - k * g.itemsPerPage)), a = Math.max(0, b - f / 2 * g.itemsPerPage), c = Math.min(g.total, a + (f + 1) * g.itemsPerPage), n = Math.max(0, n - (b - a) * g.itemHeight), d._renderItems(a, c, function() {
                        l.css("top", n), l.data(O, n)
                    })), d.prevScroll = h
                },
                scrollTop: function(a) {
                    var b = this,
                        c = b.element;
                    return _.defined(a) ? void c.scrollTop(a) : c.scrollTop()
                },
                destroy: function() {
                    var a = this,
                        b = a.options;
                    a.element.off(b.eventNS), a.element = null, b.createContainer = P, b.getItems = P, X.fn.destroy.call(a)
                }
            });
        a.fn.swidget = function(a) {
            var b = u(this, a);
            return b.length ? b.length > 1 ? b : b[0] : null
        }, a.fn.swidgets = function(a) {
            return u(this, a)
        };
        var _ = {
                string: function(a) {
                    return o(a) === G
                },
                number: function(a) {
                    return o(a) === J
                },
                integer: function(a) {
                    return o(a) === J && /^[\+\-]?\d+$/.test(a + "")
                },
                "float": function(a) {
                    return o(a) === J && /^[\+\-]?\d+\.\d+$/.test(a + "")
                },
                func: function(a) {
                    return o(a) === H
                },
                object: function(a) {
                    return o(a) === F
                },
                array: function(a) {
                    return o(a) === I
                },
                date: function(a) {
                    return o(a) === K
                },
                "boolean": function(a) {
                    return o(a) === L
                },
                "null": function(a) {
                    return o(a) === M
                },
                undefined: function(a) {
                    return o(a) === N
                },
                defined: function(a) {
                    return a !== c && null !== a
                },
                event: function(a) {
                    return typeof b.Event == H && a instanceof b.Event || a && a.altKey !== c
                }
            },
            aa = {
                "int": function(a, b) {
                    return parseInt(a, b || 10)
                },
                "float": function(a) {
                    return parseFloat(a)
                },
                number: function(a) {
                    return i(a + "")
                },
                array: function(a) {
                    return a instanceof Array ? a : a !== c ? [a] : []
                },
                string: function(a) {
                    return a + ""
                },
                key: function(b) {
                    var c, d, e, f, g = o(b);
                    switch (g) {
                        case M:
                        case N:
                            return g;
                        case F:
                            return c = q(b).sort(), d = [], a.each(c, function(a, c) {
                                d.push(c + ":" + aa.key(b[c]))
                            }), "{" + d.join(",") + "}";
                        case I:
                            for (d = "[", e = 0, f = b.length; e < f; e++) d += aa.key(b[e]), e < f - 1 && (d += ",");
                            return d += "]";
                        case K:
                            return b.toISOString();
                        default:
                            return b + ""
                    }
                }
            };
        ! function() {
            S.stableSort = function() {
                var a = "abcdefghijklmnopqrstuvwxyz";
                return "xyzvwtursopqmnklhijfgdeabc" == a.split("").sort(function(b, c) {
                    return ~~(a.indexOf(c) / 2.3) - ~~(a.indexOf(b) / 2.3)
                }).join("")
            }(), S.scrollbar = function() {
                var a, b = C.createElement("div");
                return b.style.cssText = "overflow:scroll;overflow-x:hidden;zoom:1;clear:both", b.innerHTML = "&nbsp;", C.body.appendChild(b), a = b.offsetWidth - b.scrollWidth, C.body.removeChild(b), a
            }, S.isRtl = function(b) {
                return a(b).closest(".sui-rtl").length > 0
            }, S.transitions = function() {
                var a, b = (C.body || C.documentElement).style,
                    c = "Transition",
                    d = ["Moz", "webkit", "Webkit", "Khtml", "O", "ms"];
                if (_.string(b[c.toLowerCase()])) return !0;
                for (a = 0; a < d.length; a++)
                    if (_.string(b[d[a] + c])) return !0;
                return !1
            }(), S.hasScrollbarY = function(b) {
                var c = a(b).get(0);
                return c.scrollHeight > c.clientHeight
            }, S.hasScrollbarX = function(b) {
                var c = a(b).get(0);
                return c.scrollWidth > c.clientWidth
            }
        }(),
            function() {
                x.rAF = function(a) {
                    return (b.requestAnimationFrame || b.webkitRequestAnimationFrame || b.mozRequestAnimationFrame || function(a) {
                        return this.setTimeout(a, 1e3 / 60)
                    }).call(b, a)
                }, x.cAF = function(a) {
                    return (b.cancelAnimationFrame || b.clearTimeout)(a)
                }
            }(),
            function() {
                var a = /MSIE/i.test(B),
                    b = /Trident/i.test(B),
                    c = /Firefox/i.test(B),
                    d = /Opera/i.test(B),
                    e = /Safari/i.test(B),
                    f = /Chrome/i.test(B),
                    g = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(B);
                S.browser = {
                    ie: a || b,
                    firefox: c,
                    opera: d,
                    safari: !f && e,
                    chrome: f,
                    mobile: g,
                    version: a ? parseInt(B.substr(B.indexOf("MSIE ") + 5), 10) : b ? parseInt(B.substr(B.indexOf("rv:") + 3), 10) : c ? parseInt(B.substr(B.indexOf("Firefox/") + 8), 10) : d ? parseInt(B.substr(B.indexOf("Version/") + 8), 10) : f ? parseInt(B.substr(B.indexOf("Chrome/") + 7), 10) : e ? parseInt(B.substr(B.indexOf("Version/") + 8), 10) : 0
                }
            }(), a(function() {
            for (var b, c = 1e6, d = 1e9, e = a('<div style="display:none;"/>').appendTo(C.body), f = c;;) {
                if (b = f + c, e.css("height", b), b > d || e.height() !== b) break;
                f = b
            }
            e.remove(), S.maxElementHeight = f
        }), w.handler = function() {
            return !1
        }, y(x, {
            Class: V,
            Dispatcher: X,
            Event: Y,
            Constants: W,
            format: k,
            formatString: j,
            parseDate: h,
            getCalendarInfo: l,
            getCurrencyInfo: m,
            error: n,
            dieOnError: !0,
            iid: f,
            strid: e,
            guid: g,
            support: S,
            extend: p,
            selection: w,
            type: o,
            is: _,
            to: aa,
            keys: q,
            get: r,
            set: s,
            ui: y({}, x.ui || {}, {
                Widget: Z,
                VirtualizedContainer: $,
                plugin: v
            })
        })
    }(jQuery, this),
    function(a, b, c, d) {
        function e(a, b) {
            var c, d, e = [];
            if (a.map) return a.map(b);
            for (c = 0, d = a.length; c < d; c++) e[c] = b(a[c], c, a);
            return e
        }

        function f(a, b, c, e) {
            var f = B(a, b);
            return f === d ? c && (f = u.def(c, e, !0)) : f = u.convert(f, c), f
        }
        var g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v = b.Class,
            w = b.Dispatcher,
            x = Math.min,
            y = Math.max,
            z = b.is,
            A = b.to,
            B = b.get,
            C = b.set,
            D = (b.support, b.type),
            E = function() {},
            F = a.extend,
            G = a.proxy,
            H = a.each,
            I = a.grep,
            J = a.inArray,
            K = a.Deferred,
            L = "function",
            M = "array",
            N = "object",
            O = "string",
            P = "change",
            Q = "error",
            R = "start",
            S = "complete",
            T = "save",
            U = "afterset",
            V = "insert",
            W = "remove";
        g = v.extend({
            init: function(a) {
                this.data = a
            },
            read: function(a, b, c, d) {
                return b(this.data, !1, d)
            },
            modify: function(a, b, c, d) {
                b(d)
            }
        }), h = v.extend({
            init: function(a, b) {
                this.options = a, this.cache = b
            },
            read: function(b, c, e, f) {
                var g, h = this.options,
                    i = h.read,
                    j = this.cache;
                return z.func(i) ? void i(b, c, e, f) : (i = z.string(i) ? {
                    url: i
                } : F(!0, {}, i), z.func(i.data) && (b = i.data(b, f)), g = j.get(b), void(g !== d ? c(g, !0, f) : (i.data = b, i.error = e, i.success = function(a) {
                    j.set(b, a), c(a, !1, f)
                }, a.ajax(i))))
            },
            modify: function(b, c, d, f) {
                var g, h, i, j, k, l = this.options,
                    m = l.modify || {},
                    n = ["create", "update", "remove"],
                    o = ["added", "edited", "removed"],
                    p = [],
                    q = function(a) {
                        return a.data
                    };
                if (z.func(m)) return void m(b, c, d, f);
                for (k = 0; k < n.length; k++) i = n[k], j = o[k], h = m[i], b[j].length ? (z.string(h) && (h = {
                    url: h,
                    type: "POST"
                }), z.func(h) ? (g = p[k] = new K, h(b[j], G(g.resolve, g), G(g.reject, g), f)) : z.object(h) ? (h = F(!0, {}, h), z.func(h.data) ? h.data = h.data(b[j], f) : (h.data = {}, h.data[j] = e(b[j], q)), g = p[k] = a.ajax(h)) : (g = p[k] = new K, g.resolve())) : (g = p[k] = new K, g.resolve());
                a.when.apply(a, p).then(c, d)
            }
        }), i = v.extend({
            init: function() {
                this.values = {}
            },
            get: function(a) {
                return this.values[A.key(a)]
            },
            set: function(a, b) {
                return this.values[A.key(a)] = b, b
            },
            remove: function(a) {
                var b = A.key(a),
                    c = this.values[b];
                return delete this.values[b], c
            },
            clear: function() {
                this.values = []
            }
        }), i.noop = {
            get: E,
            set: E
        }, k = v.extend({
            init: function(a, b) {
                z.object(a) ? a = A.array(a) : z.string(a) && (a = [{
                    path: a,
                    desc: !!b
                }]), this._expr = a
            },
            build: function() {
                var a = this._expr,
                    b = k.cache;
                return z.func(a) ? function(a) {
                    return function(b, c) {
                        var d = a(b.item, c.item);
                        return 0 !== d ? d : b.index - c.index
                    }
                }(a) : z.array(a) ? b.get(a) || b.set(a, function(a) {
                    return function(b, c) {
                        var d, e, f, g, h, i, j, k;
                        for (g = 0, h = a.length; g < h; g++)
                            if (i = b.item, j = c.item, d = a[g] || {}, e = d.path, f = d.desc ? -1 : 1, z.string(e) ? (i = B(i, e), j = B(j, e)) : z.func(e) && (i = e(i), j = e(j)), z.date(i) && (i = i.getTime()), z.date(j) && (j = j.getTime()), i !== j && (null != i || null != j)) {
                                if (null == i) return -1 * f;
                                if (null == j) return 1 * f;
                                if (z.number(i) && z.number(j) || (i = i.toString(), j = j.toString()), i.localeCompare) {
                                    if (k = i.localeCompare(j), 0 === k) continue;
                                    return k * f
                                }
                                if (i > j) return 1 * f;
                                if (i < j) return -1 * f
                            }
                        return b.index - c.index
                    }
                }([].concat(a))) : void 0
            }
        }), k.cache = new i, l = v.extend({
            init: function(a) {
                this._expr = a || []
            },
            build: function() {
                var a = this,
                    b = l.cache,
                    c = a._expr,
                    d = a._func || (a._func = b.get(c));
                return d || (d = a._buildRecursive(c, !0), d && b.set(c, d)), d
            },
            _single: function(a) {
                var b;
                if (a) {
                    if (z.func(a.filter)) return a.filter;
                    if (b = l.filters[l.filterAliases[a.filter]]) return function(a, b, c, d) {
                        return function(e) {
                            return b(B(e, a), c, d)
                        }
                    }(a.path, b, a.value, a.sensitive)
                }
            },
            _multiple: function(a, b) {
                return function(c) {
                    var d, e = !0,
                        f = a.length;
                    for (d = 0; d < f; d++)
                        if (e = a[d](c), b) {
                            if (!e) break
                        } else if (e) break;
                    return e
                }
            },
            _buildRecursive: function(a, b) {
                var c, d, e = this,
                    f = [];
                if (z.array(a)) {
                    for (c = 0, d = a.length; c < d; c++) f.push(e._buildRecursive(a[c]));
                    return f.length > 1 ? e._multiple(f, b) : f[0]
                }
                return a.and || a.or ? e._buildRecursive(a.and || a.or, !!a.and) : e._single(a)
            }
        }), l.cache = new i, l.normalize = {
            equatable: function(a, b, c) {
                return z.date(a) ? {
                    a: a.getTime(),
                    b: new Date(b).getTime()
                } : l.normalize.string(a, b, c)
            },
            string: function(a, b, c) {
                return {
                    a: c ? a + "" : (a + "").toLowerCase(),
                    b: c ? b + "" : (b + "").toLowerCase()
                }
            },
            scalar: function(a, b) {
                return z.date(a) ? {
                    a: a.getTime(),
                    b: new Date(b).getTime()
                } : isNaN(parseFloat(a)) ? {
                    a: a,
                    b: b
                } : {
                    a: parseFloat(a),
                    b: parseFloat(b)
                }
            }
        }, l.filters = {
            eq: function(a, b, c) {
                var d = l.normalize.equatable(a, b, c);
                return d.a === d.b
            },
            neq: function(a, b, c) {
                var d = l.normalize.equatable(a, b, c);
                return d.a !== d.b
            },
            con: function(a, b, c) {
                var d = l.normalize.string(a, b, c);
                return d.a.indexOf(d.b) > -1
            },
            notcon: function(a, b, c) {
                var d = l.normalize.string(a, b, c);
                return d.a.indexOf(d.b) < 0
            },
            starts: function(a, b, c) {
                var d = l.normalize.string(a, b, c);
                return 0 === d.a.indexOf(d.b)
            },
            ends: function(a, b, c) {
                var d = l.normalize.string(a, b, c);
                return d.a.indexOf(d.b) === d.a.length - d.b.length
            },
            gt: function(a, b) {
                var c = l.normalize.scalar(a, b);
                return c.a > c.b
            },
            lt: function(a, b) {
                var c = l.normalize.scalar(a, b);
                return c.a < c.b
            },
            gte: function(a, b) {
                var c = l.normalize.scalar(a, b);
                return c.a >= c.b
            },
            lte: function(a, b) {
                var c = l.normalize.scalar(a, b);
                return c.a <= c.b
            },
            isnull: function(a) {
                return null == a
            },
            notnull: function(a) {
                return null != a
            }
        }, l.filterAliases = {
            eq: "eq",
            equal: "eq",
            equals: "eq",
            "==": "eq",
            neq: "neq",
            ne: "neq",
            doesnotequal: "neq",
            notequal: "neq",
            notequals: "neq",
            "!=": "neq",
            con: "con",
            contains: "con",
            notcon: "notcon",
            doesnotcontain: "notcon",
            notcontains: "notcon",
            starts: "starts",
            startswith: "starts",
            ends: "ends",
            endswith: "ends",
            gt: "gt",
            greaterthan: "gt",
            ">": "gt",
            lt: "lt",
            lessthan: "lt",
            "<": "lt",
            gte: "gte",
            ge: "gte",
            greaterthanorequal: "gte",
            ">=": "gte",
            lte: "lte",
            le: "lte",
            lessthanorequal: "lte",
            "<=": "lte",
            isnull: "isnull",
            "null": "isnull",
            notnull: "notnull",
            isnotnull: "notnull"
        }, m = v.extend({
            init: function() {},
            build: function(a) {
                var b = this,
                    c = m.cache,
                    d = c.get(a);
                return d || (d = b._build(a), d && c.set(a, d)), d
            },
            _build: function(a) {
                if (!a) return null;
                var b = a.field,
                    c = a.aggregate,
                    g = a.type || Number,
                    h = function(a) {
                        return a === d ? u.def(g) : u.convert(a, g)
                    };
                return z.func(c) ? c : "count" == c ? function(a) {
                    return a.length
                } : "sum" == c ? function(a) {
                    var c, d, e = 0,
                        i = a.length;
                    for (c = 0; c < i; c++) d = f(a[c], b, g), e += g == Date && z.date(d) ? d.getTime() : d;
                    return h(e)
                } : "average" == c ? function(a) {
                    var c, d, e = 0,
                        i = a.length;
                    for (c = 0; c < i; c++) d = f(a[c], b, g), e += g == Date && z.date(d) ? d.getTime() : d;
                    return h(e / i)
                } : "min" == c ? function(a) {
                    return h(x.apply(null, e(a, function(a) {
                        return f(a, b, g)
                    })))
                } : "max" == c ? function(a) {
                    return h(y.apply(null, e(a, function(a) {
                        return f(a, b, g)
                    })))
                } : d
            }
        }), m.cache = new i, j = v.extend({
            init: function(a, b, c, d, f) {
                var g = this;
                g.data = a, g.total = null != b ? b : a ? a.length : 0, g.aggregates = c, g.groups = d, g.indices = z.array(f) ? f.slice(0) : e(a || [], function(a, b) {
                    return b
                })
            },
            filter: function(a) {
                var b, c, d, e = this,
                    f = new l(a).build(),
                    g = e.data,
                    h = [],
                    i = [];
                if (f) {
                    for (b = 0, c = g.length; b < c; b++) d = g[b], f(d) && (h.push(d), i.push(e.indices[b]));
                    e.indices = i
                } else h = g.slice(0);
                return new j(h, h.length, e.aggregates, e.groups, e.indices)
            },
            aggregate: function(a) {
                var b, c, d, e, f = this,
                    g = new m,
                    h = f.data;
                for (z.array(a) || (a = [a]), f.aggregates = [], b = 0; b < a.length; b++) c = a[b], d = g.build(c), d && (e = d(h, c)), f.aggregates.push(F({}, c, {
                    value: e
                }));
                return new j(h.slice(0), f.total, f.aggregates, f.groups, f.indices)
            },
            aggregateGroups: function(a) {
                var b = this,
                    c = b.data.slice(0),
                    d = new j(b._getInnerMostItems(c), null, null, null, null).aggregate(a);
                return new j(c, b.total, d.aggregates, b.groups, b.indices)
            },
            _getInnerMostItems: function(a) {
                var b = [];
                return H(this._getInnerMostGroups(a), function(a, c) {
                    b = b.concat(c.items)
                }), b
            },
            _getInnerMostGroups: function(a) {
                var b = this,
                    c = [];
                return H(a || [], function(a, d) {
                    c = c.concat(b._hasInnerGroups(d) ? b._getInnerMostGroups(d.items) : d)
                }), c
            },
            _hasInnerGroups: function(a) {
                return !!(a && a.items && a.items.length > 0) && (a.items[0].field && z.array(a.items[0].items))
            },
            group: function(a) {
                var b, c = this,
                    d = c.data.slice(0);
                return z.array(a) || (a = [a]), a.length > 0 && (b = c._groupData(a, d, c.indices), d = b[0], c.indices = b[1]), new j(d, c.total, c.aggregates, c.groups, c.indices)
            },
            _groupData: function(a, b, c) {
                var d, e, f, g, h, i = this,
                    l = [],
                    m = [],
                    n = a.slice(0),
                    o = n.shift(),
                    p = o.field,
                    q = o.aggregate,
                    r = o.order,
                    s = {};
                if (H(b, function(a, b) {
                    e = B(b, p), f = s[e], z.defined(f) ? l[f].items.push({
                        item: b,
                        index: c[a]
                    }) : (s[e] = l.length, l.push(F({}, o, {
                        value: e,
                        items: [{
                            item: b,
                            index: c[a]
                        }]
                    })))
                }), r && (d = new k("value", "desc" == r).build())) {
                    for (g = 0; g < l.length; g++) l[g] = {
                        item: l[g],
                        index: g
                    };
                    for (l.sort(d), g = 0; g < l.length; g++) l[g] = l[g].item
                }
                for (g = 0; g < l.length; g++)
                    for (h = 0; h < l[g].items.length; h++) m.push(l[g].items[h].index), l[g].items[h] = l[g].items[h].item;
                if (q && H(l, function(a, b) {
                    l[a].aggregate = new j(b.items).aggregate(q).aggregates
                }), n && n.length > 0) {
                    var t = 0,
                        u = [];
                    for (g = 0; g < l.length; g++) {
                        var v = l[g],
                            w = m.slice(t, t + (v.items ? v.items.length : 0)),
                            x = i._groupData(n, v.items, w);
                        l[g].items = x[0], u = u.concat(x[1]), t += v.items ? v.items.length : 0
                    }
                    m = u
                }
                return [l, m]
            },
            sort: function(a, b) {
                var c, d = this,
                    e = new k(a, b).build(),
                    f = d.data.slice(0),
                    g = [],
                    h = f ? f.length : 0,
                    i = d.indices;
                if (e) {
                    for (c = 0; c < h; c++) g.push({
                        index: i[c],
                        item: f[c]
                    });
                    for (g.sort(e), c = 0; c < h; c++) f[c] = g[c].item, d.indices[c] = g[c].index
                }
                return new j(f, d.total, d.aggregates, d.groups, d.indices)
            },
            sortGroups: function(a, b) {
                var c, d = this,
                    e = d.data.slice(0),
                    f = [],
                    g = 0;
                return c = function(e) {
                    if (d._hasInnerGroups(e)) H(e.items, function(a, b) {
                        c(b)
                    });
                    else {
                        var h = new j(e.items, null, null, null, d.indices.slice(g, g + (e.items ? e.items.length : 0))).sort(a, b);
                        e.items = h.data, f = f.concat(h.indices), g += e.items ? e.items.length : 0
                    }
                }, H(e, function(a, b) {
                    c(b)
                }), new j(e, d.total, d.aggregates, d.groups, f)
            },
            _sliceGroups: function(a, b, c, d) {
                var e, f = this,
                    g = 0,
                    h = [],
                    i = [],
                    j = z.defined(d);
                return 0 !== c || j ? (e = function(a) {
                    if (f._hasInnerGroups(a)) {
                        var h = [],
                            k = 0;
                        return H(a.items, function(a, b) {
                            var c = e(b);
                            c > 0 && (h.push(b), k += c)
                        }), a.items = h, k
                    }
                    var l, m, n = g,
                        o = a.items.length,
                        p = n + o - 1;
                    return j ? d - 1 >= p ? c <= p && (l = y(0, c - n), m = o) : d - 1 >= n && c <= p && (l = y(0, c - n), m = x(o, d - n)) : c <= n ? (l = 0, m = o) : c >= n && c <= p && (l = y(0, c - n), m = o), z.defined(l) && z.defined(m) && m > l ? (a.items = a.items.slice(l, m), i = i.concat(b.slice(n + l, n + m))) : a.items = [], g += o, a.items.length
                }, H(a, function(a, b) {
                    var c = e(b);
                    c > 0 && h.push(b)
                }), [h, i]) : [a.slice(0), b.slice(0)]
            },
            skip: function(a) {
                var b = this;
                return new j(b.data.slice(a), b.total, b.aggregates, b.groups, b.indices.slice(a))
            },
            skipGroups: function(a) {
                var b = this,
                    c = b._sliceGroups(b.data, b.indices, a);
                return new j(c[0], b.total, b.aggregates, b.groups, c[1])
            },
            take: function(a) {
                var b = this;
                return new j(b.data.slice(0, a), b.total, b.aggregates, b.groups, b.indices.slice(0, a))
            },
            takeGroups: function(a) {
                var b = this,
                    c = b._sliceGroups(b.data, b.indices, 0, a);
                return new j(c[0], b.total, b.aggregates, b.groups, c[1])
            }
        }), j.create = function(a, b, c, d, e) {
            b = b || {};
            var f = new j(a, c, d, e),
                g = b.remoteOperations || [],
                h = g.join(" "),
                i = h.indexOf("group") > -1;
            return b.group || i ? (b.group ? (b.filter && (f = f.filter(b.filter)), f = f.group(b.group)) : f.data = f.groups, b.aggregate && (f = f.aggregateGroups(b.aggregate)), b.sort && (f = f.sortGroups(b.sort)), b.skip && (f = f.skipGroups(b.skip)), b.take && (f = f.takeGroups(b.take))) : (b.filter && (f = f.filter(b.filter)), b.aggregate && (f = f.aggregate(b.aggregate)), b.sort && (f = f.sort(b.sort)), b.skip && (f = f.skip(b.skip)), b.take && (f = f.take(b.take))), f
        }, n = w.extend({
            init: function(a) {
                var b = this,
                    c = b.options = z.array(a) ? {
                        data: a
                    } : a || {},
                    d = c.schema || {},
                    e = n.schemas[d.type || "json"];
                b.data = null, b.filter = c.filter, b.sort = c.sort, b.skip = c.skip, b.take = c.take, b.group = c.group, b.aggregate = c.aggregate, b.schema = new e(d), b.remote = c.remote, b.cache = new i, b._recursive = !!c.recursive, w.fn.init.call(b, c)
            },
            isRecursive: function() {
                return this._recursive
            },
            trigger: b.ui.Widget.fn.trigger,
            _client: function() {
                var a = this,
                    b = a.remote;
                return z.object(b) ? new h(b, b.cache ? a.cache : i.noop) : new g(a.options.data)
            },
            _params: function() {
                var a, b = this,
                    c = ["filter", "aggregate", "group", "sort", "skip", "take"],
                    d = b.remote,
                    e = b._remoteOperations().join(" "),
                    f = {
                        local: {},
                        remote: {}
                    };
                return H(c, function(c, g) {
                    a = b[g], null != a && (d && e.indexOf(g) > -1 ? f.remote[g] = a : f.local[g] = a)
                }), f
            },
            _remoteOperations: function() {
                var a = this.remote;
                return a && a.read ? a.read.operations || a.operations || [] : []
            },
            _success: function(a, b, c, d, e) {
                var f = this,
                    g = f.schema,
                    h = g.process(c);
                f.data = h.data, f._pTotal = h.total, f._pAggregates = h.aggregates, f._pGroups = h.groups, f._createView(h.data, b), f.trigger(S), f.trigger(P, {
                    fromCache: !!d,
                    extra: e
                }), a.resolve(f.view, !!d)
            },
            _createView: function(a, b) {
                var c = this,
                    d = j.create(a, F({}, b || c._params().local, {
                        remoteOperations: c._remoteOperations()
                    }), c._pTotal, c._pAggregates, c._pGroups);
                c.view = d.data, c.total = d.total, c._indices = d.indices, c.aggregates = d.aggregates
            },
            _error: function(a, b, c, d, e) {
                var f = this;
                a.reject(d), b && f.trigger(S), f.trigger(Q, {
                    errorType: "transport",
                    error: d,
                    operation: c,
                    extra: e
                })
            },
            read: function(a) {
                var b = this,
                    c = new K,
                    d = b._params(),
                    e = b.trigger(R, {
                        params: d,
                        extra: a
                    });
                return e.isDefaultPrevented() ? c.resolve() : (b.cancel(), b._client().read(d.remote, G(b._success, b, c, d.local), G(b._error, b, c, !0, "read"), a)), c.promise()
            },
            _ensureTracker: function() {
                var a = this,
                    b = a.tracker,
                    c = a.data;
                if (!b) {
                    if (!c || !z.array(c)) throw new Error("shield.DataSource: cannot modify when no data array is available.");
                    b = a.tracker = new t({
                        data: c,
                        model: a.schema.model,
                        events: {
                            change: function(b) {
                                a._createView(a.data), b && b.afterset || a.trigger(P)
                            },
                            error: function(b) {
                                a.trigger(Q, {
                                    errorType: "tracker",
                                    path: b ? b.path : "undefined path",
                                    value: b ? b.value : "undefined value",
                                    error: b ? b.reason : "undefined error",
                                    model: b ? b.target : "undefined target model"
                                })
                            }
                        }
                    }), a.data = b.data
                }
            },
            getDataIndex: function(a) {
                return this._indices[a]
            },
            add: function(a) {
                return this._ensureTracker(), this.tracker.add(a)
            },
            insert: function(a, b) {
                return this._ensureTracker(), this.tracker.insert(a, b)
            },
            remove: function(a) {
                return this._ensureTracker(), this.tracker.remove(a)
            },
            removeAt: function(a) {
                return this._ensureTracker(), this.tracker.removeAt(a)
            },
            edit: function(a) {
                return this._ensureTracker(), this.tracker.edit(a)
            },
            insertView: function(a, b) {
                return this.insert(this._indices[a], b)
            },
            removeAtView: function(a) {
                return this.removeAt(this._indices[a])
            },
            editView: function(a) {
                return this.edit(this._indices[a])
            },
            save: function(a, b) {
                var c, d, e, f, g = this,
                    h = g.tracker,
                    i = h ? h.changes : {
                        added: [],
                        edited: [],
                        removed: []
                    },
                    j = h && g.trigger(T, {
                        changes: i
                    }),
                    k = new K;
                if (a = !z.defined(a) || !!a, j && !j.isDefaultPrevented()) {
                    for (c = g.data = h.original, d = h.data, c.length = 0, e = 0; e < d.length; e++) c[e] = d[e];
                    g.options.data && !z.func(g.options.data) && k.done(G(g._syncLocalData, g)), g._client().modify(i, G(k.resolve, k), G(g._error, g, k, !1, "save"), b), h.destroy(), h = g.tracker = null
                } else k.resolve(i);
                return a && (f = function() {
                    g._createView(g.data), g.trigger(P, {
                        extra: b
                    })
                }, k.then(f, f)), k.promise()
            },
            _syncLocalData: function() {
                var a, b = this,
                    c = b.schema,
                    d = (c.options.fields, b.data),
                    e = [],
                    f = z.array(c.getReverseDataFirstItem(b.options.data));
                for (a = 0; a < d.length; a++) e[a] = f ? [] : {}, c.reverseFields(d[a], e[a]);
                c.reverseData(e, b.options.data)
            },
            cancel: function() {
                var a = this,
                    b = a.tracker,
                    c = b && (b.changes.added.length || b.changes.edited.length || b.changes.removed.length);
                b && (a.data = b.original, b.destroy(), b = a.tracker = null, c && (a._createView(a.data), a.trigger(P)))
            },
            destroy: function() {
                var a, b = this,
                    c = ["options", "data", "total", "aggregates", "filter", "sort", "aggregate", "group", "skip", "take", "schema", "remote", "view", "cache"];
                for (b.cancel(), b.cache && b.cache.clear(), a = 0; a < c.length; a++) delete b[c[a]];
                w.fn.destroy.call(b)
            }
        }), n.create = function(a, b) {
            return a instanceof n ? a : new n(z.array(a) ? F({
                data: a
            }, b) : F({}, a, b))
        }, p = v.extend({
            init: function(a) {
                this.options = a
            },
            parse: function(b) {
                var c = this.options.parse;
                if (z.func(c)) return c(b);
                if (z.string(b)) try {
                    b = a.parseJSON(b)
                } catch (d) {}
                return b
            },
            data: function(a) {
                var b = this.options.data;
                return z.func(b) ? b(a) : z.string(b) ? B(a, b) : a
            },
            aggregates: function(a) {
                var b = this.options.aggregates;
                return z.func(b) ? b(a) : z.string(b) ? B(a, b) : d
            },
            groups: function(a) {
                var b = this.options.groups;
                return z.func(b) ? b(a) : z.string(b) ? B(a, b) : d
            },
            reverseData: function(a, b) {
                var c, d = this.options.data;
                if (z.func(d)) d(a, b);
                else if (z.string(d)) C(b, d, a);
                else if (z.array(b))
                    for (b.length = 0, c = 0; c < a.length; c++) b[c] = a[c]
            },
            getReverseDataFirstItem: function(a) {
                var b = this.data(a);
                return z.array(b) && b.length > 0 ? b[0] : d
            },
            total: function(a, b) {
                var c = this.options.total;
                return a = a || [], b = b || [], z.func(c) ? c(a) : z.string(c) ? B(a, c) : b.length
            },
            fields: function(a) {
                var b = this,
                    c = b.options.fields,
                    d = b.model = u.define(c);
                return c ? e(a, function(a) {
                    return p.mapFields(a, d.fn.fields)
                }) : a
            },
            reverseFields: function() {
                var a, b, c, d = this,
                    e = d.options.fields,
                    f = [].slice.call(arguments),
                    g = f[0],
                    h = f.length <= 1,
                    i = h ? g : f[1];
                if (z.defined(e))
                    for (c in e) e.hasOwnProperty(c) && (a = e[c], b = g[c], z.string(a.path) ? C(i, a.path, b) : z.func(a.path) ? a.path(g, b) : i[c] = b, h && delete i[c]);
                else if (!h && z.object(g))
                    for (c in g) g.hasOwnProperty(c) && (i[c] = g[c])
            },
            process: function(a) {
                var b = this,
                    c = b.parse(a),
                    d = b.aggregates(a),
                    e = b.groups(a),
                    f = b.fields(b.data(c));
                return {
                    data: f,
                    aggregates: d,
                    groups: e,
                    total: b.total(c, f)
                }
            }
        }), p.mapFields = function(a, b) {
            var c, e, f, g = {};
            a = a || {};
            for (c in b) b.hasOwnProperty(c) && (e = b[c], z.string(e.path) ? (f = B(a, e.path), f === d && (f = B(a, c))) : f = z.func(e.path) ? e.path(a) : a[c], f === d ? e.type && (f = u.def(e.type, e.def, e.nullable)) : f = u.convert(f, e.type), g[c] = f);
            return g
        }, q = p.extend({
            parse: function(b) {
                var c = this,
                    d = c.options;
                if (z.func(d.parse)) b = d.parse(b);
                else if (z.string(b)) try {
                    b = a.parseXML(b)
                } catch (e) {}
                return b && 9 === b.nodeType && (b = c._json(c._root(b))), b
            },
            _root: function(a) {
                var b, c, d = a.childNodes;
                for (b = 0, c = d.length; b < c; b++)
                    if (1 === d[b].nodeType) return d[b];
                return null
            },
            _json: function(a) {
                var b, c, d, e, f = {},
                    g = {},
                    h = "";
                for (d = 0, e = a.attributes.length; d < e; d++) b = a.attributes[d], f["_" + b.nodeName] = b.nodeValue;
                for (d = 0, e = a.childNodes.length; d < e; d++) switch (b = a.childNodes[d], c = b.nodeName, b.nodeType) {
                    case 1:
                        g[c] ? (z.array(f[c]) || (f[c] = [f[c]]), f[c].push(this._json(b))) : (f[c] = this._json(b), g[c] = !0);
                        break;
                    case 3:
                        h += b.nodeValue;
                        break;
                    case 4:
                        f._cdata = b.nodeValue
                }
                return h = h.replace(/^\s+(.*)\s+$/gim, "$1"), h && (f._text = h), f
            }
        }), r = p.extend({
            parse: function(b) {
                var c, d = this.options,
                    e = d.parse,
                    f = a(b),
                    g = [],
                    h = [];
                if (z.func(e)) return e(b);
                if (d.result) return d.result;
                if (!f[0] || "table" !== f[0].tagName.toLowerCase()) return b;
                f.eq(0).find("thead th").each(function() {
                    h.push(a(this).text())
                }).end().find("tbody tr").each(function() {
                        c = {}, a(this).children().each(function(b) {
                            c[h[b]] = a(this).text()
                        }), g.push(c)
                    });
                return d.result = g, g
            }
        }), s = p.extend({
            parse: function(b) {
                var c = this.options,
                    d = c.parse,
                    e = a(b),
                    f = [];
                return z.func(d) ? d(b) : c.result ? c.result : e[0] && "select" === e[0].tagName.toLowerCase() ? (e.find("option").each(function(b) {
                    var c = a(this);
                    f.push({
                        value: c.attr("value"),
                        text: c.text(),
                        selected: c.is(":selected")
                    })
                }), c.result = f, f) : b
            }
        }), n.schemas = {
            json: p,
            xml: q,
            table: r,
            select: s
        }, t = w.extend({
            init: function(a) {
                var b = this;
                b.original = a.data, b.data = Array.apply(null, a.data), b.model = a.model, b.changes = {
                    added: [],
                    edited: [],
                    removed: []
                }, w.fn.init.call(this, a)
            },
            _model: function(a) {
                var b = this,
                    c = new b.model(a);
                return c.on(P, G(b.trigger, b, P)), c.on(Q, G(b.trigger, b, Q)), c.on(U, G(b.trigger, b, P, {
                    afterset: !0
                })), c
            },
            add: function(a) {
                return this.insert(this.data.length, a)
            },
            insert: function(a, b) {
                var c, d = this,
                    e = d.data,
                    f = d.changes;
                if (a < 0 || a > e.length) throw new Error("shield.DataSource: invalid item index.");
                return c = d._model(b), f.added.push(c), e.splice(a, 0, c.data), d.trigger(P, {
                    operation: V,
                    index: a,
                    model: c
                }), c
            },
            edit: function(a) {
                var b, c = this,
                    d = c.data,
                    e = c.changes;
                if (isNaN(a) || a < 0 || a >= d.length) throw new Error("shield.DataSource: invalid item index.");
                return (b = I(e.added.concat(e.edited), function(b) {
                    return b.data === d[a]
                })[0]) ? b : (b = c._model(d[a]), e.edited.push(b), d[a] = b.data, b)
            },
            remove: function(a) {
                var b = this,
                    c = b.changes,
                    d = I(c.added.concat(c.edited), function(b) {
                        return b === a
                    })[0],
                    e = -1;
                if (a instanceof u ? d && (e = J(d.data, b.data)) : e = J(a, b.data), e > -1) return b.removeAt(e)
            },
            removeAt: function(a) {
                var b = this,
                    c = b.data,
                    d = b.changes,
                    e = I(d.added.concat(d.edited), function(b) {
                        return b.data === c[a]
                    })[0];
                if (a < 0 || a > c.length) throw new Error("shield.DataSource: invalid item index.");
                return e ? e.destroy() : e = new b.model(c[a]), d.removed.push(e), c.splice(a, 1), b.trigger(P, {
                    operation: W,
                    index: a,
                    model: e
                }), e
            },
            destroy: function() {
                var a, b = this,
                    c = b.changes,
                    d = c.added.concat(c.edited).concat(c.removed);
                for (a = 0; a < d.length; a++) d[a].destroy();
                c.added.length = c.edited.length = c.removed.length = 0, b.data = b.original = null, w.fn.destroy.call(b)
            }
        }), u = w.extend({
            init: function(a, b) {
                var c = this;
                w.fn.init.call(c, b), c.fields = F(!0, {}, c.constructor.prototype.fields), c.data = F(!0, {}, a)
            },
            trigger: b.ui.Widget.fn.trigger,
            get: function(a) {
                return B(this.data, a)
            },
            set: function(a, b) {
                var c = this;
                b = c.validate(a, b), b !== d && (C(c.data, a, b), c.trigger(U))
            },
            validate: function(a, b) {
                var c, e, f = this,
                    g = B(f.fields, a);
                if (!g) return b;
                if (D(g.validator) === L) c = g.validator;
                else if (D(g.type.validate) === L) c = g.type.validate;
                else {
                    var h = g.type.toString().split("(")[0].split(" ")[1].toLowerCase();
                    c = u.validators[h]
                }
                if (c) {
                    if (e = c(b), e === d) return void f.trigger(Q, {
                        errorType: "validation",
                        path: a,
                        value: b,
                        error: "validation error"
                    });
                    b = e
                }
                return null === b && g.nullable === !1 ? void f.trigger(Q, {
                    errorType: "validation",
                    path: a,
                    value: b,
                    error: "null value not allowed"
                }) : b
            }
        }), u.define = function(a) {
            return u.extend({
                fields: u.normalize(F(!0, {}, a))
            })
        }, u.normalize = function(a) {
            var b, c, d;
            a = a || {};
            for (b in a) a.hasOwnProperty(b) && (c = a[b], d = D(c), d === L ? a[b] = {
                type: c
            } : d === M ? a[b] = {
                type: c
            } : d === O && (a[b] = {
                path: c
            }));
            return a
        }, u.def = function(a, b, c) {
            var e;
            return b !== d ? D(b) === L ? b() : b : c ? null : a === String ? "" : a === Number ? 0 : a === Date ? new Date : a !== Boolean && (a === Array ? [] : a === Object ? {} : (e = D(a), e === M ? [] : e === N ? {} : e === L ? a() : null))
        }, u.convert = function(a, b) {
            var c;
            return null == a ? a : b === String ? a.toString() : b === Number ? (c = parseFloat(a), isNaN(c) ? a : c) : b === Date ? (c = new Date(a), isNaN(c.getTime()) ? a : c) : b === Boolean ? z.string(a) ? !/^(false|0)$/i.test(a) : !!a : a
        }, u.validators = {
            string: function(a) {
                return null == a ? null : a.toString()
            },
            number: function(a) {
                return null === a ? null : isNaN(+a) ? d : +a
            },
            date: function(a) {
                return null === a ? null : a instanceof Date ? a : (a = new Date(a), isNaN(a.getTime()) ? d : a)
            },
            "boolean": function(a) {
                return null === a ? null : null == a ? d : !!a
            },
            array: function(a) {
                return null === a ? null : D(a) === M ? a : d
            },
            object: function(a) {
                return null === a ? null : D(a) === N ? a : d
            }
        }, o = n.extend({
            init: function(a) {
                F(a, {
                    recursive: !0
                }), n.fn.init.call(this, a)
            }
        }), F(b, {
            map: e,
            DataSource: n,
            RecursiveDataSource: o,
            DataQuery: j,
            Model: u
        })
    }(jQuery, shield, this);

! function(a, b, c, d) {
    var e, f, g = b.ui.Widget,
        h = (b.Class, b.DataSource),
        i = b.RecursiveDataSource,
        j = (b.ui.Position, b.Constants.KeyCode),
        k = b.strid,
        l = document,
        m = a.proxy,
        n = a.each,
        o = (a.extend, b.error),
        p = b.format,
        q = b.is.defined,
        r = b.is["boolean"],
        s = (b.is.integer, b.is.func),
        t = b.is.array,
        u = b.is.object,
        v = b.is.string,
        w = b.support.browser,
        x = "role",
        y = "aria-expanded",
        z = "click",
        A = "dbl" + z,
        B = "focus",
        C = "blur",
        D = "change",
        E = "keydown",
        F = "mousedown",
        G = "tabindex",
        H = "disabled",
        I = "true",
        J = "false",
        K = "up",
        L = "down",
        M = "left",
        N = "right",
        O = "checked",
        P = "li",
        Q = "ul",
        R = "visibility",
        S = "hidden",
        T = "visible",
        U = "sui-treeview-list-item",
        V = "sui-treeview-list-item-idx",
        W = "sui-treeview-list-item-parent",
        X = "sui-treeview-list-item-ds",
        Y = "sui-treeview-list-items-ds",
        Z = "sui-treeview-list-item-checked",
        $ = "sui-treeview-list-item-loaded",
        _ = "sui-treeview-item-list-expanded",
        aa = "sui-treeview-item-toggle-loading",
        ba = "sui-treeview-item-selected",
        ca = "sui-treeview-item-active",
        da = "sui-treeview-item-disabled",
        ea = "sui-treeview-item";
    e = {
        cls: d,
        width: d,
        minWidth: d,
        height: d,
        dataSource: d,
        readDataSource: !0,
        animation: {
            enabled: !0,
            expandDuration: 200,
            collapseDuration: 150
        },
        checkboxes: {
            enabled: !1,
            children: !1,
            template: d
        },
        dragDrop: !1,
        dragDropScope: d,
        textTemplate: "{text}",
        hrefTemplate: "{href}",
        expandedTemplate: "{expanded}",
        checkedTemplate: "{checked}",
        disabledTemplate: "{disabled}",
        iconUrlTemplate: "{iconUrl}",
        iconClsTemplate: "{iconCls}",
        hasChildrenTemplate: function(a) {
            return a && (a.items && a.items.length > 0 || a.hasChildren)
        },
        events: {}
    }, f = g.extend({
        init: function() {
            g.fn.init.apply(this, arguments);
            var b, c, d, e, f, i, j = this,
                k = a(j.element),
                l = j.options,
                p = l.dieOnError,
                r = l.dataSource,
                s = l.cls;
            if (j._original = b = a(j.element), j._tagname = e = b.prop("tagName").toLowerCase(), i = j._eventNS = ".shieldTreeView" + j.getInstanceId(), j._wrapper = d = b.wrap("<div/>").parent(), d.addClass("sui-treeview" + (s ? " " + s : "")), b.hide(), j.element = k = a('<ul class="sui-treeview-list"/>').on(B + i, m(j._focus, j)).on(C + i, m(j._blur, j)).on(E + i, m(j._keydown, j)), b.after(k), n(["width", "minWidth", "height", "maxHeight"], function(a, b) {
                q(l[b]) && d.css(b, l[b])
            }), c = b.attr(G), k.attr(G, q(c) ? c : "0"), r) j.dataSource = h.create(r);
            else {
                if (e != Q) return j.destroy(), void o("shieldTreeView: No dataSource or underlying UL element found.", p);
                f = function(b) {
                    var c = [];
                    return a(b).children(P).each(function() {
                        var b, d = a(this),
                            e = d.children(Q).length > 0,
                            g = {
                                cls: d.attr("data-class"),
                                href: d.attr("data-href") ? d.attr("data-href") : d.children().not(Q).first().attr("href"),
                                disabled: d.attr("data-disabled") ? d.attr("data-disabled") : q(d.attr(H)) && null !== d.attr(H),
                                expanded: d.attr("data-expanded"),
                                checked: d.attr("data-checked"),
                                iconUrl: d.attr("data-icon-url"),
                                iconCls: d.attr("data-icon-cls")
                            };
                        e ? (b = d.clone(), b.children(Q).remove(), g.text = b.html()) : g.text = d.html(), e && (g.items = f(d.children(Q)[0])), c.push(g)
                    }), c
                }, j.dataSource = h.create({
                    data: b,
                    schema: {
                        parse: f
                    }
                })
            }
            j.dataSource.on(D + i, m(j._dsChange, j)), k.attr(x, "tree"), l.readDataSource && j.dataSource.read()
        },
        refresh: function(a) {
            this.refreshWithElement(this._original, a)
        },
        _dsChange: function() {
            var a = this,
                b = a.element,
                c = a.dataSource;
            a._renderItems(c, b, null, !0, !0, function() {
                a.trigger("dataBound", {
                    dataSource: c,
                    element: null
                })
            })
        },
        _destroyItems: function(b) {
            var c = this;
            a(b).children("ul.sui-treeview-item-list").each(function() {
                c._destroyItems(a(this))
            }), a(b).children(".sui-treeview-item-content").find(".sui-treeview-item-text").each(function() {
                a(this).swidget("Draggable") && a(this).swidget("Draggable").destroy(), a(this).swidget("Droppable") && a(this).swidget("Droppable").destroy()
            }), a(b).children(".sui-treeview-item-content").find(".sui-checkbox").each(function() {
                a(this).swidget("CheckBox") && a(this).swidget("CheckBox").destroy()
            });
            var d = a(b).data(Y);
            d && d.off(c._eventNS), a(b).removeData(), a(b).empty()
        },
        _renderItems: function(a, b, c, d, e, f) {
            var g, h, i, j = this,
                k = j.options,
                l = k.checkboxes,
                m = a.view,
                n = (m || []).length,
                o = [],
                p = [];
            for (j._destroyItems(b), i = 0; i < n; i++) g = j._renderItem(m[i], c, a, i), h = g.listItem, h.appendTo(b), g.itemExpanded && o.push(h), g.itemChecked && p.push(h);
            if (s(f) && f(), d)
                for (i = 0; i < o.length; i++) j._expand(o[i], !1, e);
            if (l.enabled && l.children)
                for (i = 0; i < p.length; i++) j._updateChecked(p[i])
        },
        _renderItem: function(c, e, f, g) {
            var h, i, j, n, o, t, u, w, B, C, D, E = this,
                G = E.options,
                H = G.checkboxes,
                I = H.template,
                K = E.getInstanceId(),
                L = G.dragDropScope,
                M = function() {
                    return a(this.element).clone().appendTo(l.body)
                },
                N = e ? e.data(U) : null,
                O = c.cls,
                P = E._hasChildren(c),
                Q = p.call(E, G.textTemplate, c),
                T = k();
            if (h = p(G.disabledTemplate, c), h = !!(r(h) && h || "true" === h || "1" === h || 1 === h), i = p(G.expandedTemplate, c), i = !!(r(i) && i || "true" === i || "1" === i || 1 === i), j = p(G.checkedTemplate, c), j = !!(r(j) && j || "true" === j || "1" === j || 1 === j), n = p(G.hrefTemplate, c), v(n) && "undefined" !== n || (n = d), o = p(G.iconUrlTemplate, c), v(o) && "undefined" !== o || (o = d), t = p(G.iconClsTemplate, c), v(t) && "undefined" !== t || (t = d), u = a('<li class="' + ea + " sui-unselectable" + (h ? " " + da : "") + (q(O) ? " " + O : "") + '"/>').data(U, c).data(V, g).data(W, e).data(X, f), D = a('<span class="sui-treeview-item-text" id="' + T + '">' + (n ? '<a href="' + n + '">' : "") + Q + (n ? "</a>" : "") + "</span>").on(F, m(E._itemClick, E, u)).on(A, m(E._itemDblClick, E, u)), w = a('<span class="sui-treeview-item-toggle"/>').on(z, m(E._toggleItem, E, u)).append(a('<span class="sui-treeview-item-toggle-collapsed"/>')), P || w.css(R, S), u.attr(x, "treeitem").attr("aria-describedby", T), P && u.attr(y, J), H.enabled)
                if (u.data(Z, !!j), B = a('<span class="sui-treeview-item-check"/>'), v(I)) B.html(p(I, {
                    item: c,
                    parent: N
                }));
                else if (s(I)) I(B, c, N);
                else {
                    var Y = a('<input type="checkbox"' + (j ? ' checked="checked"' : "") + "/>").appendTo(B);
                    b.ui.CheckBox ? a(Y).shieldCheckBox({
                        enabled: !h,
                        enableThreeStates: !!H.children,
                        events: {
                            click: m(E._checkboxClick, E, u)
                        }
                    }) : Y.on(z, m(E._checkboxClick, E, u))
                }
            return (o || t) && (C = a('<span class="sui-treeview-item-icon"/>'), o ? C.append('<img src="' + o + '"/>') : C.addClass(t)), u.append(a('<div class="sui-treeview-item-content"/>').append(w, B, D.prepend(C))), u.append(a('<ul class="sui-treeview-list sui-treeview-item-list"/>').attr(x, "group").hide()), G.dragDrop && !h && (D.shieldDraggable({
                scope: q(L) ? L : "suiTv" + K,
                dragCls: "sui-treeview-item-text-dragging",
                helper: M,
                events: {
                    start: m(E._dragStart, E, u),
                    stop: m(E._dragStop, E, u)
                }
            }), D.shieldDroppable({
                scope: q(L) ? L : "suiTv" + K,
                hoverCls: "sui-treeview-item-text-dropover",
                tolerance: {
                    x: "touch",
                    y: "intersect"
                },
                events: {
                    over: m(E._dropOver, E, u),
                    out: m(E._dropOut, E, u),
                    drop: m(E._drop, E, u)
                }
            })), {
                listItem: u,
                itemExpanded: i,
                itemChecked: j,
                itemDisabled: h
            }
        },
        _isValidDragDrop: function(b, c) {
            if (a(b).hasClass("sui-treeview-item-text")) {
                var d = a(b).parent().parent().parent().parent().children(".sui-treeview-item-content").first().children(".sui-treeview-item-text").first();
                return d && d.length > 0 && c[0] === d[0] ? c[0] !== d[0] : !(a(b).parent().parent().find(c[0]).length > 0)
            }
            return !1
        },
        _dragStart: function(a, b) {
            this.trigger("dragStart", {
                draggable: b.target.element,
                element: a,
                domEvent: b
            })
        },
        _dragStop: function(a, b) {
            this.trigger("dragStop", {
                draggable: b.target.element,
                element: a,
                domEvent: b
            }), b.preventDefault()
        },
        _dropOver: function(b, c) {
            var d, e = c.draggable,
                f = c.droppable,
                g = this._isValidDragDrop(e, f),
                h = {
                    draggable: e,
                    droppable: f,
                    element: b,
                    valid: g,
                    domEvent: c
                };
            g && (h.sourceNode = a(e).closest("." + ea)), h.targetNode = a(f).closest("." + ea), d = this.trigger("droppableOver", h), !d.isDefaultPrevented() && d.valid || c.preventDefault()
        },
        _dropOut: function(b, c) {
            var d, e = c.draggable,
                f = c.droppable,
                g = this._isValidDragDrop(e, f),
                h = {
                    draggable: e,
                    droppable: f,
                    element: b,
                    valid: g,
                    domEvent: c
                };
            g && (h.sourceNode = a(e).closest("." + ea)), h.targetNode = a(f).closest("." + ea), d = this.trigger("droppableOut", h)
        },
        _drop: function(b, c) {
            var d, e = this,
                f = c.draggable,
                g = c.droppable,
                h = e._isValidDragDrop(f, g),
                i = {
                    draggable: f,
                    droppable: g,
                    element: b,
                    cancelled: !0,
                    skipAnimation: !!h,
                    valid: h,
                    domEvent: c
                };
            h && (i.sourceNode = a(f).closest("." + ea)), i.targetNode = a(g).closest("." + ea), d = e.trigger("drop", i), c.cancelled = d.cancelled, c.skipAnimation = d.skipAnimation, d.isDefaultPrevented() && c.preventDefault()
        },
        _hasChildren: function(a) {
            var b = p(this.options.hasChildrenTemplate, a);
            return r(b) && b || "true" === b || "1" === b || 1 === b
        },
        _isLoaded: function(b) {
            return a(b).data($) === !0
        },
        _isExpanded: function(a) {
            return a.hasClass(_)
        },
        _isFullyExpanded: function(a) {
            var b = this,
                c = b.getParent(a);
            return c ? b._isExpanded(a) && b._isFullyExpanded(c) : b._isExpanded(a)
        },
        _isDisabled: function(b) {
            return a(b).hasClass(da)
        },
        _isSelected: function(b) {
            return a(b).hasClass(ba)
        },
        _isActive: function(b) {
            return a(b).hasClass(ca)
        },
        _isFocused: function() {
            return a(this.element).hasClass("sui-treeview-focus")
        },
        _focus: function(b) {
            var c = this,
                d = c.element;
            w.ie && l.activeElement !== a(d)[0] && a(d).focus(), c._blurTimeout && (clearTimeout(c._blurTimeout), c._blurTimeout = null), c._isFocused() || (a(c.element).addClass("sui-treeview-focus"), c.trigger(B))
        },
        _blur: function(b) {
            var c = this;
            c._isFocused() && (c._blurTimeout && clearTimeout(c._blurTimeout), c._blurTimeout = setTimeout(function() {
                a(c.element).removeClass("sui-treeview-focus"), c.trigger(C)
            }, 100))
        },
        _selectItem: function(a, b, c) {
            var d, e = this;
            e._isSelected(a) || c && (d = e.trigger("select", {
                element: a,
                item: a.data(U)
            }), d.isDefaultPrevented()) || (e._selectedItem && e._selectedItem.removeClass(ba), e._selectedItem = a, a.addClass(ba), b && e._activateItem(a, c))
        },
        _activateItem: function(a, b) {
            var c = this;
            c._isActive(a) || (c._activeItem && c._activeItem.removeClass(ca), c._activeItem = a, a.addClass(ca), b && c.trigger(D, {
                element: a,
                item: a.data(U)
            }))
        },
        _itemClick: function(a, b) {
            var c = this;
            w.ie && setTimeout(m(c._focus, c)), c._selectItem(a, !0, !0)
        },
        _itemDblClick: function(a, b) {
            var c = this;
            c._selectItem(a, !0, !0), c._toggleItem(a, b)
        },
        _getCheckBox: function(a) {
            return a.children(".sui-treeview-item-content").children(".sui-treeview-item-check").find('input[type="checkbox"]').first()
        },
        _checkboxClick: function(a) {
            var b = this;
            w.ie && setTimeout(m(b._focus, b)), b._toggleCheck(a, !0)
        },
        _toggleCheck: function(a, b) {
            this._setChecked(a, !a.data(Z), b)
        },
        _setChecked: function(b, c, d) {
            var e = this,
                f = e.options.checkboxes.children,
                g = e._getCheckBox(b);
            g && g.length > 0 && (a(g).swidget("CheckBox") ? (g = a(g).swidget("CheckBox"), g.checked(c)) : c ? g.attr(O, O) : g.removeAttr(O), b.data(Z, c), f && e._updateChecked(b), d && e.trigger("check", {
                element: b,
                item: b.data(U),
                checked: c
            }))
        },
        _updateChecked: function(a) {
            var b = this,
                c = a.data(Z);
            b._updateCheckedChildren(a, c), b._updateCheckedParents(a, c)
        },
        _updateCheckedChildren: function(b, c) {
            var d = this;
            a(b).children("ul.sui-treeview-item-list").first().children(P).each(function() {
                var b = a(this),
                    e = d._getCheckBox(b);
                a(e).swidget("CheckBox") ? a(e).swidget("CheckBox").checked(c) : c ? e.attr(O, O) : e.removeAttr(O), b.data(Z, c), d._updateCheckedChildren(b, c)
            })
        },
        _updateCheckedParents: function(b, c) {
            var e, f = this,
                g = f.getParent(b),
                h = g ? g.data(Z) : d,
                i = c;
            g && h !== !1 && (a(b).parent().children(P).each(function() {
                if (a(this).data(Z) !== c) return i = null, !0
            }), e = f._getCheckBox(g), a(e).swidget("CheckBox") ? a(e).swidget("CheckBox").checked(i) : i ? e.attr(O, O) : e.removeAttr(O), g.data(Z, i), f._updateCheckedParents(g, c))
        },
        _toggleItem: function(a, b) {
            var c = this;
            w.ie && setTimeout(m(c._focus, c)), c._isDisabled(a) || (c._isExpanded(a) ? c._collapse(a, !0) : c._expand(a, !0, !1))
        },
        _getToggleEl: function(b) {
            return a(b).children(".sui-treeview-item-content").first().children(".sui-treeview-item-toggle").first()
        },
        _expand: function(b, c, d, e) {
            var f, g, h, i = this,
                j = i.options.animation,
                k = b.children("ul.sui-treeview-item-list").first();
            if (k && !(a(k).length <= 0) && (i.hasChildren(b) || !(k.children(P).length <= 0))) return i._isExpanded(b) ? void(s(e) && e.call(i)) : void(c && (h = i.trigger("expand", {
                element: b,
                item: b.data(U)
            }), h.isDefaultPrevented()) || (k.stop(!0, !0), k.hide(), f = i._getToggleEl(b), g = function() {
                k.slideDown(j.enabled && !d ? j.expandDuration : 0, function() {
                    b.addClass(_).attr(y, I), f.css(R, T), a(f).find("span").addClass("sui-treeview-item-toggle-expanded").removeClass("sui-treeview-item-toggle-collapsed"), s(e) && e.call(i)
                })
            }, i._isLoaded(b) ? g() : i._loadItem(b, d, g)))
        },
        _loadItem: function(b, c, d) {
            var e, f = this,
                g = b.data(U),
                j = b.children("ul.sui-treeview-item-list").first(),
                k = b.data(X),
                l = f._getToggleEl(b),
                m = g.items || g.children;
            !j || a(j).length <= 0 || (t(m) || u(m) || m instanceof h ? (a(l).addClass(aa), e = h.create(m), j.data(Y, e), e.on(D + f._eventNS, function(g) {
                a(l).removeClass(aa), b.data($, !0), f._renderItems(this, j, b, !0, c, function() {
                    f.trigger("dataBound", {
                        dataSource: e,
                        element: b
                    }), s(d) && d.call(f)
                })
            }), e.read({
                parent: g
            })) : k && k instanceof i ? (a(l).addClass(aa), e = new i(k.options), j.data(Y, e), e.on(D + f._eventNS, function(g) {
                a(l).removeClass(aa), b.data($, !0), f._renderItems(this, j, b, !0, c, function() {
                    f.trigger("dataBound", {
                        dataSource: e,
                        element: b
                    }), s(d) && d.call(f)
                })
            }), e.read({
                parent: g
            })) : (b.data($, !0), s(d) && d.call(f)))
        },
        _expandPathSegment: function(a, b, c, d, e) {
            var f, g, h = this,
                i = a.length;
            return d > i ? void(s(e) && e.call(h)) : (f = [].slice.call(a).splice(0, d), g = h.getByPath(f), void(h._isExpanded(g) ? h._expandPathSegment(a, b, c, d + 1, e) : h._expand(g, b, c, function() {
                h._expandPathSegment(a, b, c, d + 1, e)
            })))
        },
        _expandPath: function(a, b, c, d) {
            this._expandPathSegment(a, b, c, 1, d)
        },
        _collapse: function(b, c) {
            var d, e, f = this,
                g = f.options.animation,
                h = b.children("ul.sui-treeview-item-list").first();
            !f._isExpanded(b) || !h || a(h).length <= 0 || c && (e = f.trigger("collapse", {
                element: b,
                item: b.data(U)
            }), e.isDefaultPrevented()) || (d = f._getToggleEl(b), h.stop(!0, !0), h.slideUp(g.enabled ? g.collapseDuration : 0, function() {
                b.removeClass(_).attr(y, J), a(d).find("span").removeClass("sui-treeview-item-toggle-expanded").addClass("sui-treeview-item-toggle-collapsed")
            }))
        },
        _keydown: function(a) {
            var b = this,
                c = !0;
            switch (a.keyCode) {
                case j.UP:
                    b._move(K, a);
                    break;
                case j.DOWN:
                    b._move(L, a);
                    break;
                case j.LEFT:
                    b._move(M, a);
                    break;
                case j.RIGHT:
                    b._move(N, a);
                    break;
                case j.SPACE:
                    b._selectedItem && b._toggleCheck(b._selectedItem, !0);
                    break;
                case j.ENTER:
                    b._selectedItem && b._activateItem(b._selectedItem, !0);
                    break;
                default:
                    c = !1
            }
            c && a.preventDefault()
        },
        _getExpandedItems: function(b) {
            var c = this;
            return c.element.children("." + ea).add(c.element.find("li.sui-treeview-item-list-expanded > ul.sui-treeview-list > li." + ea)).filter(function() {
                var b = c.getParent(a(this));
                return !b || c._isFullyExpanded(b)
            }).not("." + da)
        },
        _getNextItem: function(b) {
            var c = this._getExpandedItems(),
                d = a(c).length,
                e = a(c).index(b);
            if (e < d - 1) return a(a(c).get(e + 1))
        },
        _getPrevItem: function(b) {
            var c = this._getExpandedItems(),
                d = a(c).index(b);
            if (d > 0) return a(a(c).get(d - 1))
        },
        _getPrevParentItem: function(b) {
            var c = this,
                d = c.getParent(b);
            if (d && a(d).length > 0) return c._isDisabled(d) ? c._getPrevParentItem(d) : d
        },
        _move: function(b, c) {
            var d, e, f = this,
                g = f._selectedItem;
            f.getParent(a(g));
            if (q(g))
                if (b === K) e = f._getPrevItem(g);
                else if (b === L) e = f._getNextItem(g);
                else if (b === M) {
                    if (f._isExpanded(g)) return void f._collapse(g, !0, !1);
                    e = f._getPrevParentItem(g)
                } else {
                    if (!f._isExpanded(g)) return void f._expand(g, !0, !1);
                    d = a(g).children("ul.sui-treeview-list").first(), d.length > 0 && (e = a(d).children("li:not(." + da + ")").first())
                } else e = a(f.element).find(P).first();
            q(e) && a(e).length > 0 && f._selectItem(e, !1, !0)
        },
        _getByPath: function(b, c) {
            var d = this,
                e = b.shift(),
                f = a(c.children(P)[e]);
            return b.length > 0 ? d._getByPath(b, f.children(Q)) : a(c.children(P)[e])
        },
        _getListItem: function(b) {
            return a(t(b) ? this.getByPath(b, this.element) : b)
        },
        getByPath: function(b) {
            return this._getByPath([].slice.call(b), a(this.element))
        },
        isLoaded: function(b) {
            return a(this._getListItem(b)).data($) === !0
        },
        hasChildren: function(a) {
            return this._hasChildren(this.getItem(a))
        },
        getItem: function(b) {
            return a(this._getListItem(b)).data(U)
        },
        getDataSource: function(b) {
            return a(this._getListItem(b)).data(X)
        },
        getChildrenDataSource: function(b) {
            var c = this,
                e = c._getListItem(b);
            return c.hasChildren(c.getItem(e)) && c.isLoaded(e) ? a(e).children("ul.sui-treeview-item-list").first().data(Y) : d
        },
        getParent: function(b) {
            return a(this._getListItem(b)).data(W)
        },
        getDataSourceIndex: function(b) {
            return a(this._getListItem(b)).data(V)
        },
        _getIndex: function(b) {
            var c = this._getListItem(b);
            return a(c).index()
        },
        getPath: function(b) {
            var c = this,
                d = a(b),
                e = [];
            do e.unshift(c._getIndex(d)), d = c.getParent(d); while (d && d.length > 0);
            return e
        },
        updateIndetermined: function(b) {
            var c = this,
                d = c._getListItem(b);
            q(d) ? c._updateChecked(a(d)) : a(c.element).children(P).each(function() {
                c._updateChecked(a(this))
            })
        },
        expanded: function() {
            var a = this,
                b = [].slice.call(arguments),
                c = r(b[0]) ? b.shift() : d,
                e = b[0],
                f = a._getListItem(e);
            return q(c) ? void(c ? t(e) ? a._expandPath([].slice.call(e), !1, !1, b[1]) : a._expand(f, !1, !1, b[1]) : a._collapse(f, !1)) : a._isExpanded(f)
        },
        selected: function() {
            var a, b = this,
                c = [].slice.call(arguments);
            return c.length > 0 ? (a = b._getListItem(c[0]), void(a && a.length > 0 && b._selectItem(a, !0, !1))) : b._selectedItem
        },
        enabled: function() {
            var b = this,
                c = [].slice.call(arguments),
                e = r(c[0]) ? c.shift() : d,
                f = b._getListItem(c[0]);
            if (f && !(a(f).length <= 0)) return q(e) ? void(e ? f.removeClass(da) : f.addClass(da)) : !b._isDisabled(f)
        },
        checked: function() {
            var b = this,
                c = [].slice.call(arguments),
                e = r(c[0]) ? c.shift() : d,
                f = b._getListItem(c[0]);
            if (f && !(a(f).length <= 0)) return q(e) ? void b._setChecked(f, e, !1) : f.data(Z)
        },
        _insertedItem: function(b, c) {
            return b instanceof a ? a(b) : t(b) ? this._getListItem(b) : u(b) ? this._renderItem(b, c).listItem : void 0
        },
        _beforeRemoveChild: function(b) {
            var c = this;
            b && b.children(Q).children(P).length <= 1 && (a(c._getToggleEl(b)).css(R, S), b.removeClass(_))
        },
        _refreshParent: function(b) {
            var c = a(b).parents(P).first();
            a(b).data(W, c && c.length > 0 ? c : null)
        },
        load: function(a, b) {
            var c = this,
                d = c._getListItem(a);
            d && d.length > 0 && c._loadItem(d, !1, b)
        },
        append: function() {
            var b, c, d, e = this,
                f = [].slice.call(arguments),
                g = f[0],
                h = t(g) || g instanceof a,
                i = f[1],
                j = f[2];
            if (q(i)) {
                if (c = e._getListItem(i), !(c && a(c).length > 0)) return;
                d = a(c).children(Q).first()
            } else c = null, d = e.element;
            b = e._insertedItem(g, c), b && (h && e._beforeRemoveChild(e.getParent(b)), !c || e._isExpanded(c) ? (b.appendTo(d), e._refreshParent(b), s(j) && j.call(e, b)) : e.hasChildren(c) ? e._isExpanded(c) ? (b.appendTo(d), e._refreshParent(b), s(j) && j.call(e, b)) : e._expand(c, !1, !1, function() {
                b.appendTo(d), e._refreshParent(b), s(j) && j.call(e, b)
            }) : e._isExpanded(c) ? (b.appendTo(d), e._refreshParent(b), s(j) && j.call(e, b)) : (a(e._getToggleEl(c)).css(R, T), c.removeClass(_), b.appendTo(d), e._refreshParent(b), e._expand(c, !1, !1, function() {
                s(j) && j.call(e, b)
            })))
        },
        insertBefore: function() {
            var b, c = this,
                d = [].slice.call(arguments),
                e = d[0],
                f = t(e) || e instanceof a,
                g = c._getListItem(d[1]);
            if (g && g.length > 0 && (b = c._insertedItem(e, c.getParent(g)))) return f && c._beforeRemoveChild(c.getParent(b)), b.insertBefore(g), c._refreshParent(b), b
        },
        insertAfter: function() {
            var b, c = this,
                d = [].slice.call(arguments),
                e = d[0],
                f = t(e) || e instanceof a,
                g = c._getListItem(d[1]);
            if (g && g.length > 0 && (b = c._insertedItem(e, c.getParent(g)))) return f && c._beforeRemoveChild(c.getParent(b)), b.insertAfter(g), c._refreshParent(b), b
        },
        remove: function(b) {
            var c = this,
                d = t(b) || b instanceof a,
                e = c._getListItem(b);
            c.getParent(e);
            return !!(e && e.length > 0) && (c._destroyItems(a(e).children(Q).first()), d && c._beforeRemoveChild(c.getParent(e)), e.remove(), !0)
        },
        destroy: function() {
            var b = this,
                c = b.element,
                d = b._eventNS;
            b.dataSource && b.dataSource.off(D + d), b._destroyItems(c), b._blurTimeout && (clearTimeout(b._blurTimeout), b._blurTimeout = null), a(c).off(d).remove(), b._original.unwrap(), b._original.show(), b._original = b._wrapper = b._selectedItem = b._activeItem = null, g.fn.destroy.call(b)
        }
    }), f.defaults = e, b.ui.plugin("TreeView", f)
}(jQuery, shield, this);
