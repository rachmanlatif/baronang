<?php
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
?>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<div class="form-inputs">

				<?php if($_GET['mobile'] == 'popup') {
						$link = '?mobile=popup'; ?>
						<h1>Pop Up Mobile</h1>
				<?php } ?>

				<?php if($_GET['mobile'] == 'inbox') {
						$link = '?mobile=inbox'; ?>
						<h1>Inbox Mobile</h1>
				<?php } ?>
				<form action="<?php echo $link; ?>" method="post" enctype="multipart/form-data">

					<?php
					if($_GET['mobile'] == 'popup') { ?>
						<?php
						$image = '';
						$a = "select top 1 * from [dbo].[PopUpMobile] where Status = 1";
						$b = sqlsrv_query($conn, $a);
						$c = sqlsrv_fetch_array($b, SQLSRV_FETCH_ASSOC);
						if($c != null){
								$image = $c['Image'];
						?>
						<div class="input-field">
								<span>Image yang sudah di upload</span> : <br>
								<img src="<?php echo $image; ?>" style="width: 20%;height: 20%;margin:0 auto;">
						</div>
						<?php } ?>

						<div class="input-field">
							<label for="nama">Nama</label>
							<input type="text" name="nama" id="nama">
						</div>

						<span>Deskripsi</span> : <br>
						<textarea name="deskripsi" id="deskripsi"></textarea>

						<div class="input-field">
							<input type="text" name="link" id="link">
							<label for="link">Link</label>
						</div>

						<div class="input-field">
							<span>Ambil Gambar</span><br>
							<input type="file" name="image" id="image">
						</div>

						<?php
					}

					if($_GET['mobile'] == 'inbox') { ?>

						<div class="input-field">
							<label for="judul">Judul</label>
							<input type="text" name="judul" id="judul">
						</div>

						<span>Deskripsi</span><br>
						<textarea name="isi" id="isi"></textarea>

						<div class="input-field">
							<span>Ambil Gambar</span><br>
							<input type="file" name="image" id="image">
						</div>

						<?php
					}

					if( isset($_GET['mobile']) ) { ?>
						<button type="submit" name="btnok" id="btnok" class="btn btn-large primary-color width-100 waves-effect waves-light m-t-30">Submit</button>
						<?php
					}
					?>

				</form>
				<?php if($_GET['mobile'] == 'inbox') { ?>
					<br>
					<span class="lead">Daftar Inbox</span>
					<table class="table table-bordered table-striped">
							<tr>
									<th>Judul</th>
									<th>Isi</th>
									<th>Image</th>
									<th>Tanggal Publish</th>
									<th>Status</th>
							</tr>
							<?php
							$imagein = '';
							$xx = "select* from [dbo].[InboxMobile]";
							$yy = sqlsrv_query($conn, $xx);
							while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_ASSOC)){
									$imagein = $zz['Image'];

									$status = 'Tidak aktif';
									if($zz['Status'] == 1){
										$status = 'Aktif';
									}
									?>
									<tr>
											<td><?php echo $zz['Judul']; ?></td>
											<td><?php echo $zz['Isi']; ?></td>
											<td><img src="<?php echo $imagein; ?>" style="width: 80%;height: 50%;margin: 0 auto;"></td>
											<td><?php echo $zz['CreatedDate']->format('Y-m-d H:i:s'); ?></td>
											<td><?php echo $status; ?></td>
									</tr>
							<?php } ?>
					</table>
				<?php } ?>
			</div>

			<?php
			$ok = $_POST['btnok'];
			if(isset($ok)) {
				if($_GET['mobile'] == 'popup') {
					$nama = $_POST['nama'];
					$desk = $_POST['deskripsi'];
					$link = $_POST['link'];

					$src  = $_FILES['image']['tmp_name'];

					$linkimg = '';
					if($src != ''){
						$linkimg  = 'http://'.$_SERVER['HTTP_HOST'].'/appkoperasi/img/popupmobile/'.$_FILES['image']['name'];
					}

					$img  = 'img/popupmobile/'.$_FILES['image']['name'];

					$tgl = date('Y-m-d h:m:s');

					$sql2 = "update [dbo].[PopUpMobile] set Status = 0";
					$query = sqlsrv_query($conn, $sql2);

					$sql = "INSERT into [dbo].[PopUpMobile](Name, Description, Link, Image, Status, CreatedDate) values('$nama', '$desk', '$link', '$linkimg', 1, '$tgl')";
					$query = sqlsrv_query($conn, $sql);
					if($query) {
						move_uploaded_file($src, $img);
						?>
						<script type="text/javascript">
							alert('Berhasil Menambahkan Data PopUp!');
							window.location.href= "?mobile=popup"
						</script>
						<?php
					}
				}

				if($_GET['mobile'] == 'inbox') {
					$judul = $_POST['judul'];
					$isi = $_POST['isi'];

					$src  = $_FILES['image']['tmp_name'];

					$linkimg = '';
					if($src != ''){
						$linkimg  = 'http://'.$_SERVER['HTTP_HOST'].'/appkoperasi/img/popupmobile/'.$_FILES['image']['name'];
					}

					$img  = 'img/popupmobile/'.$_FILES['image']['name'];

					$tgl = date('Y-m-d h:m:s');

					$sql = "INSERT into [dbo].[InboxMobile](Judul, Isi, Image, Status, CreatedDate) values('$judul', '$isi', '$linkimg', 1, '$tgl')";
					$query = sqlsrv_query($conn, $sql);
					if($query) {
						move_uploaded_file($src, $img);
						?>
						<script type="text/javascript">
							alert('Berhasil Menambahkan Data Inbox!');
							window.location.href= "?mobile=inbox"
						</script>
						<?php
					}
				}

			}
			?>

		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#deskripsi').summernote();
			$('#isi').summernote();
		});
	</script>
<?php require('footer.php'); ?>
