<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
require_once 'lib/phpExcel/Classes/PHPExcel/IOFactory.php';
include "connect.php";

if(isset($_FILES['filename'])) {
    $uploads_dir = 'uploads/excel/';
    $tmp_name = $_FILES["filename"]["tmp_name"];
    $path = $uploads_dir . basename($_FILES["filename"]["name"]);

    if(move_uploaded_file($tmp_name, $path)){
        //upload data from excel
        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getSheet(0);
        } catch(Exception $e) {
            messageAlert('Failed reading excel','warning');
            header('Location: mreg_balance.php');
        }

        $highestRow = $data->getHighestRow();
        $highestColumn = $data->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        if(!($highestRow -1) >0)
        {
            messageAlert('No data found in Sheet 0 or Sheet not available.','warning');
            header('Location: mreg_balance.php');
        }

        try {
            $filePath = "uploads/excel/";

            //cek header
            $rowHeader = $data->rangeToArray('A1:'.$highestColumn.'1',NULL,TRUE,FALSE);
            $hname = $rowHeader[0][0];
            $hnip = $rowHeader[0][1];
            $hktp = $rowHeader[0][2];
            $hemail = $rowHeader[0][3];
            $htelp = $rowHeader[0][4];
            $hprod = $rowHeader[0][5];
            $hnorek = $rowHeader[0][6];
            $hbalance = $rowHeader[0][7];

            //filter jika format tidak sesuai
            if($hname != 'Nama' or $hnip != 'NIP' or $hktp != 'KTP' or $hemail != 'Email' or $htelp != 'Telepon' or $hprod != 'Nama Produk' or $hnorek != 'Nomor Rekening Lama' or $hbalance != 'Balance'){
                messageAlert('Format template tidak sesuai','info');
                header('Location: mreg_balance.php');
            }

            //buat file excel jika ada yang gagal upload
            $objPHPExcel2 = new PHPExcel();
            $objPHPExcel2->getProperties()->setCreator("baronang");
            $objPHPExcel2->getProperties()->setTitle("Failed Upload RS Balance");

            // set autowidth
            for($col = 'A'; $col !== 'Z'; $col++) {
                $objPHPExcel2->getActiveSheet()
                    ->getColumnDimension($col)
                    ->setAutoSize(true);
            }

            //sheet 1
            $objWorkSheet2 = $objPHPExcel2->createSheet(0);
            // baris judul
            $objWorkSheet2->SetCellValue('A1', 'Nama');
            $objWorkSheet2->SetCellValue('B1', 'NIP');
            $objWorkSheet2->SetCellValue('C1', 'KTP');
            $objWorkSheet2->SetCellValue('D1', 'Email');
            $objWorkSheet2->SetCellValue('E1', 'Telepon');
            $objWorkSheet2->SetCellValue('F1', 'Nama Produk');
            $objWorkSheet2->SetCellValue('G1', 'Nomor Rekening Lama');
            $objWorkSheet2->SetCellValue('H1', 'Balance');

            $baris = 2;
            //baca dari row 2
            $arr = array();
            for($row=2; $row<=$highestRow; ++$row) {

                $rowData = $data->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

                $nama = $rowData[0][0];
                $nip = $rowData[0][1];
                $ktp = number_format($rowData[0][2], 0, '.', '');
                $email = $rowData[0][3];
                $telp = number_format($rowData[0][4], 0, '.', '');
                $produk = $rowData[0][5];
                $norek = $rowData[0][6];
                $balance = number_format($rowData[0][7], 0, '.', '');

                if(is_numeric($balance)){
                    //cek posisi
                    $aaa = "select * from [dbo].[RegularSavingType] where RegularSavingTypeDesc='$produk'";
                    $bbb = sqlsrv_query($conn, $aaa);
                    $ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC);
                    if($ccc != null){
                        //save
                        $ppsql = "exec [dbo].[ProsesImportRegSavingAcc] '$nip','$ktp','$telp','$email','$ccc[1]','$norek','$balance','$_SESSION[UserID]'";
                        $ppstmt = sqlsrv_query($conn, $ppsql);
                        if(!$ppstmt){
                            //push yang gagal
                            array_push($arr, $nama);

                            //data ditolak
                            $objWorkSheet2->SetCellValue("A".$baris, $nama);
                            $objWorkSheet2->SetCellValue("B".$baris, $nip);
                            $objWorkSheet2->SetCellValueExplicit("C".$baris, $ktp, PHPExcel_Cell_DataType::TYPE_STRING);
                            $objWorkSheet2->SetCellValue("D".$baris, $email);
                            $objWorkSheet2->SetCellValueExplicit("E".$baris, $telp, PHPExcel_Cell_DataType::TYPE_STRING);
                            $objWorkSheet2->SetCellValue("F".$baris, $produk);
                            $objWorkSheet2->SetCellValue("G".$baris, $norek);
                            $objWorkSheet2->SetCellValue("H".$baris, $balance);

                            $baris++;
                        }
                    }
                }
                else{
                    //push yang gagal
                    array_push($arr, $nama);

                    //data ditolak
                    $objWorkSheet2->SetCellValue("A".$baris, $nama);
                    $objWorkSheet2->SetCellValue("B".$baris, $nip);
                    $objWorkSheet2->SetCellValueExplicit("C".$baris, $ktp, PHPExcel_Cell_DataType::TYPE_STRING);
                    $objWorkSheet2->SetCellValue("D".$baris, $email);
                    $objWorkSheet2->SetCellValueExplicit("E".$baris, $telp, PHPExcel_Cell_DataType::TYPE_STRING);
                    $objWorkSheet2->SetCellValue("F".$baris, $produk);
                    $objWorkSheet2->SetCellValue("G".$baris, $norek);
                    $objWorkSheet2->SetCellValue("H".$baris, $balance);

                    $baris++;
                }
            }

            if(!empty($arr) and $arr[0] != ""){
                $objWorkSheet2->setTitle('Failed Upload BS Balance');

                $fileName = 'failedUploadRSBalance'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel2,'Excel5');

                // download ke client
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="'.$fileName.'"');
                $objWriter->save('php://output');

                return $filePath.'/'.$fileName;
            }
            else{
                messageAlert('Berhasil menyimpan ke database','success');
                header('Location: mreg_balance.php');
            }

        } catch(Exception $e) {
            messageAlert('Gagal upload excel','danger');
            header('Location: mreg_balance.php');
        }
    }
    else{
        messageAlert('Gagal upload file','danger');
        header('Location: mreg_balance.php');
    }
}

?>