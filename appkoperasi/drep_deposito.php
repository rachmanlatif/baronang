<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Daftar Deposito");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    

    $objWorkSheet->getStyle('B1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B1', 'Daftar Deposito');


    $objWorkSheet->getStyle('B2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B2', 'Jenis Deposito');
    
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No');
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'No. Akun');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'Nama');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Tanggal Buka');
    $objWorkSheet->getStyle('E5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E5', 'Tanggal Maturity');
    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F5', 'Tanggal Tutup');
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G5', 'Suku Bunga (%)');  
    $objWorkSheet->getStyle('H5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('H5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H5', 'Saldo');
    $objWorkSheet->getStyle('I5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('I5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('I5', 'Status');




    if(isset($_GET['id'])){

    $a = "select * from [dbo].[TimeDepositTypeView] where KodeTimeDepositType='$_GET[id]'";
    //echo $a;
    $b =  sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);


    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("C2",$c[1]);
    

    $row = 6;
    $ind = $_GET['id'];
    $aaa = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[TimeDepositReport] where KodeTimeDepositType = '$ind') a";
    //echo $aaa;
    $bbb = sqlsrv_query($conn, $aaa);
    $total= $awal;
    $ctotal = $total;
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        
        $open = $ccc[7]->format('Y-m-d H:i:s');
        $maturity = $ccc[8]->format('Y-m-d H:i:s');
        $close = '';
        $tglaw = '1900-01-01 00:00:00';
        $tgltes = $ccc[9]->format('Y-m-d H:i:s');
        
        if($tgltes == $tglaw || $tgltes = ''){
            $close = '';
        } else {
            $close = $ccc[9]->format('Y-m-d H:i:s');  
        }      
                                               

            $objWorkSheet->SetCellValue("A".$row,$ccc[11]);
            $objWorkSheet->SetCellValue("B".$row, $ccc[2]);
            $objWorkSheet->SetCellValue("C".$row, $ccc[1]);
            $objWorkSheet->SetCellValue("D".$row,$open);
            $objWorkSheet->SetCellValue("E".$row,$maturity);
            $objWorkSheet->SetCellValue("F".$row,$close);
            $objWorkSheet->getStyle('G' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("G".$row,$ccc[5].'%', PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('H' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("H".$row, $ccc[6], PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->SetCellValue("I".$row, $ccc[10]);

                

        $no++;
        $row++;
        }              
        $objWorkSheet->getStyle('A5:I' .$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
  
    }
//exit;
    $objWorkSheet->setTitle('Drep Daftar Deposito');

    $fileName = 'DafDeposito'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
