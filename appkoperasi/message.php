<?php require('blank-header.php');?>

<div class="wrapper">
    <div class="col-sm-12 col-xs-12">
        <br>
        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>
    </div>
    <div class="col-sm-6 col-xs-12">
        <a href="login.php"><button type="button" class="btn btn-primary btn-block btn-flat">Login ke Baronang.com</button></a>
    </div>
    <div class="col-sm-6">

    </div>
</div>

<?php require('blank-footer.php');?>