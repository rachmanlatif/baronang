<?php
	session_start();
	error_reporting(0);

	require('qrcode/qrlib.php');

	//barcode
	include('barcode128/BarcodeGenerator.php');
	include('barcode128/BarcodeGeneratorPNG.php');
	include('barcode128/BarcodeGeneratorSVG.php');
	include('barcode128/BarcodeGeneratorJPG.php');
	include('barcode128/BarcodeGeneratorHTML.php');

	function saveQRImage_HASIL($templ, $qr, $gud, $glr, $fud, $flr, $fs, $pre_sav) {


		$temp_format = substr($templ, -4);
		if($temp_format == '.jpg') {
			$dest = imagecreatefromjpeg('qrtemplate/'.$templ);
		}
		else if($temp_format == '.jpeg') {
			$dest = imagecreatefromjpeg('qrtemplate/'.$templ);
		}
		else if($temp_format == '.png') {
			$dest = imagecreatefrompng('qrtemplate/'.$templ);
		}

		$cor = imagecolorallocate($dest, 255, 255, 255);
		$font = 'fonts/seguisb.ttf';

		$qr_bformat = substr($qr, -4);
		$qr_fformat = substr($qr, 0, 2);
		if($qr_bformat == '.jpg') {
			if($qr_fformat == 'qr') {
				$fqr = get_string_between($qr, 'qr', '.jpg');
			}
			else {
				$fqr = split ("\.", $qr);
				$fqr = $fqr[0];
			}
			$src = imagecreatefromjpeg('temp/qr'.$qr);
		}
		else if($qr_bformat == '.jpeg') {
			if($qr_fformat == 'qr') {
				$fqr = get_string_between($qr, 'qr', '.jpeg');
			}
			else {
				$fqr = split ("\.", $qr);
				$fqr = $fqr[0];
			}
			$src = imagecreatefromjpeg('temp/qr'.$qr);
		}
		else if($qr_bformat == '.png') {
			if($qr_fformat == 'qr') {
				$fqr = get_string_between($qr, 'qr', '.png');
			}
			else {
				$fqr = split ("\.", $qr);
				$fqr = $fqr[0];
			}
			$src = imagecreatefrompng('temp/qr'.$qr);
		}
		//$fqr = $fqr[0];

		imagettftext($dest, $fs, 0, $flr, $fud, $cor, $font, $fqr);
		imagecopymerge($dest, $src, $glr, $gud, 0, 0, 425, 425, 100);

		//header('Content-Type: image/jpeg');
		//imagejpeg($dest);
		$hqr = 'hasil'.$qr;
		imagepng($dest,'qrpreview/Sky Parking Kuning New/8 Mei 2019/'.$hqr);

		if($pre_sav == 'save') {
			$sql = "INSERT into KoperasiNew.dbo.qrcode values('$hqr', '$templ','$gud','$glr','$fud','$flr','$fs')";
			//echo $sql.'<br>';
			$query = sqlsrv_query($conn, $sql);
		}
		else {
			?>
			<script type="text/javascript">
				window.location.href = "qr_adjust.php?back=<?php echo $templ; ?>&qr=<?php echo $qr; ?>&hqr=<?php echo $hqr; ?>&gud=<?php echo $gud; ?>&glr=<?php echo $glr; ?>&fud=<?php echo $fud; ?>&flr=<?php echo $flr; ?>&fs=<?php echo $fs; ?>";
			</script>
			<?php
		}
	}

	function saveQRImage($qr) {
		$sqr = substr($qr,0,4);

		$qrtitle = '';
		$template = '';
		$template = 'bag2.jpg';
		$qrtitle = 'Bag QR'.$qr;

		$dest = imagecreatefromjpeg('template/'.$template);
		$cor = imagecolorallocate($dest, 255, 255, 255);
		$font = 'fonts/seguisb.ttf';

		imagettftext($dest, 61, 0, 280,1470, $cor, $font, $qr);
		$src = imagecreatefrompng('temp/qr'.$qr.'.png');

		imagecopymerge($dest, $src, 180, 430, 0, 0, 100, 100, 100);

		//header('Content-Type: image/jpeg');
		//imagejpeg($dest);
		imagejpeg($dest,'qrhasil/qr'.$qr.'.jpg');
	}

	function QRCode($kode, $cp, $matrixPointSize) {
		//QR code

		$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
		$PNG_WEB_DIR = 'temp/';

		$errorCorrectionLevel = 'H';
		//$matrixPointSize = 500;

		//$kode = '1000100010001004';
		$filename = $PNG_WEB_DIR.'qr'.$kode.'.png';
		if (!file_exists($filename)){
			QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
		}

		$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
		$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));

		saveQRImage($kode);
		saveQRImage_HASIL('Sky Parking Card_Model 2 Depan.jpg',$kode.'.png',262,130,765,162,31,'save');
		//$templ, $qr, $gud, $glr, $fud, $flr, $fs, $pre_sa
	}


	function getRandomQR($t, $j, $m) {
		$kid = $_SESSION['KID'];
		include "connect.php";

		$date = date('Y-m-d H:i:s');
		for($x = 0; $x < $j; $x++) {
			$aa = "select dbo.getKodeMasterCard()";
			$bb = sqlsrv_query($conn, $aa);
			$cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
			if($cc != null){
				$kode =	$cc[0];

				$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
				$iv = openssl_random_pseudo_bytes($ivlen);
				$ciphertext_raw = openssl_encrypt($kode, $cipher, '70007000', $options=OPENSSL_RAW_DATA, $iv);
				$hmac = hash_hmac('sha256', $ciphertext_raw, '70007000', $as_binary=true);
				$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

				$sql123 = "INSERT into dbo.MasterCard(CardNo, Barcode, DateCreate, Status) values('$kode','$ciphertext','$date','0')";
				//echo $sql123.'<br>';
				$query123 = sqlsrv_query($conn, $sql123);
				QRCode($kode, $ciphertext, $m);
			}
		}
	}

	$tgl = @$_POST['tgl'];
	$jml = @$_POST['jml'];

	if( empty($jml) || is_null($jml) || $jml == '' || $jml > 50 ) { ?>
		<script type="text/javascript">
			alert('Jumlah tidak boleh kosong atau lebih dari 50');
			window.location.href = "qrcode_gen.php";
		</script>
		<?php
	}
	else {
		if( !is_dir('qrhasil') ) mkdir('qrhasil');
		getRandomQR($tgl, $jml, 7);
		//echo 'OK';
		?>
		<script type="text/javascript">
			//window.location.href = "qrcode_gen.php?dw=ok";
			window.location.href = "qrcode_gen.php?"
		</script>
		<?php
	}
?>
