<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Buku Besar'); ?></h2>

            <form class="form-horizontal" action="" method = "POST">
                <div class="input-field">
                    <input type="text" name="tgl1" id="tgl1" class="datepicker" value="<?php echo "$_POST[tgl1]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Awal'); ?></label>
                </div>
                
                <div class="input-field">
                    <input type="text" name="tgl2" id="tgl2" class="datepicker" value="<?php echo "$_POST[tgl2]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Akhir'); ?></label>
                </div>

                <div class="input-field">
                    <?php
                    if(isset($_POST['template'])){
                    $to = $_POST['template'];
                    }
                    else{
                    $to = date('Y/m/d');
                    }
                    ?>


                    <select id="akun" name="akun" class="browser-default">
                        <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                            <?php
                            $julsql   = "select * from [dbo].[Account] where Header ='2' order by KodeAccount asc";
                            // $julsql1   = "select * from [dbo].[LaporanRugiLabaViewNew] where Header ='2' and Username = '$nameuser'";
                            echo $julsq;
                            $julstmt = sqlsrv_query($conn, $julsql);

                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                               ?>
                            <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_POST['akun']){echo "selected";} ?>>&nbsp;<?=$rjulrow[0];?>&nbsp;<?=$rjulrow[1];?></option>
                            <?php } ?>
                    </select>
                </div>

                <div class="input-field">
                    <?php
                    if(isset($_POST['template2'])){
                        $to = $_POST['template2'];
                    }
                    else{
                        $to = date('Y/m/d');
                    }
                    ?>


                    <select id="akun2" name="akun2" class="browser-default">
                        <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                        <?php
                        $julsql   = "select * from [dbo].[Account] where Header ='2' order by KodeAccount asc";
                         // $julsql   = "select * from [dbo].[LaporanRugiLabaViewNew] where Header ='2' and Username = '$nameuser'";
                        echo $julsql;
                        $julstmt = sqlsrv_query($conn, $julsql);
                        while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                           ?>
                        <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_POST['akun2']){echo "selected";} ?>>&nbsp;<?=$rjulrow[0];?>&nbsp;<?=$rjulrow[1];?></option>
                        <?php } ?>
                    </select>
                </div>
                       


                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="buku_besar.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
            </form>

        <?php if($_POST['akun']){
            $tgl1 = $_POST['tgl1'];
            $tanggaldari = date('Y-m-d', strtotime($tgl1));
            //echo $tgl1;
            $tgl2 = $_POST['tgl2'];
            //echo $tgl2;
            $tanggalsampai = date('Y-m-d', strtotime($tgl2));
            $akun = $_POST['akun'];
            $akun2 = $_POST['akun2'];
            //echo $akun;
            ?>
            <div>
                <a href="drep_neracaHIS.php?from=<?php echo $tanggaldari; ?>&to=<?php echo $tanggalsampai; ?>&acc3=<?php echo $_POST['akun']; ?>&st=5"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
            </div>

            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Laporan Buku Besar'); ?></h3>
                </tr>
            </div>
            <div class="box-header " align="Center">
                <tr>
                    <h3 class="box-title" align="center"><?php echo $_SESSION['NamaKoperasi']; ?></h3>
                </tr>
                </div>   


            <div class="box-header" align="Center">
                    <tr>
                        <?php
                            $bulan = date('m', strtotime($_POST['tgl1']));
                            $tahun = date('Y', strtotime($_POST['tgl1']));
                            $bulan2 = date('m', strtotime($_POST['tgl2']));
                            $tahun2 = date('Y', strtotime($_POST['tgl2']));
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        $bulan = 'Desember';
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
                        <?php
                        if ($bulan2 != 1) {
                                if ($bulan2 != 2 ) {
                                   if ($bulan2 != 3) {
                                       if ($bulan2 != 4) {
                                            if ($bulan2 !=5) {
                                                if ($bulan2 !=6) {
                                                    if ($bulan2 !=7) {
                                                        if ($bulan2 !=8) {
                                                            if ($bulan2 !=9) {
                                                                if ($bulan2 !=10) {
                                                                    if ($bulan2 !=11) {
                                                                        $bulan2 = 'Desember';
                                                                    } else {
                                                                        $bulan2 = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan2 = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan2 = 'September';
                                                            }
                                                        } else {
                                                            $bulan2 = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan2 = 'Juli';
                                                    }
                                                } else {
                                                    $bulan2 = 'Juni';
                                                }
                                            } else {
                                              $bulan2 = 'Mei';  
                                            }
                                       } else {
                                            $bulan2 = 'April';       
                                       }
                                   } else {
                                    $bulan2 = 'Maret';
                                   }
                                } else {
                                    $bulan2 = 'Februari';    
                                }                               
                            } else {
                                $bulan2 = 'Januari';
                            }
                        ?>
                        <h3 class="box-title"><?php echo "Periode : ",$bulan," ",$tahun, " - ", $bulan2," ",$tahun2; ?></h3>
                    </tr>

                <?php 
                $nm = 1;
                $x = "select * from dbo.AccountView where header = 2 and KodeAccount between '$akun' and '$akun2' order by KodeAccount asc";
                //echo $x;
                $y = sqlsrv_query($conn, $x);
                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                    

                ?>
                <div class="box-header " align="left">
                <tr>
                    <h5 class="box-title" align="left"><?php echo ('Akun : '); ?> <?php echo $z[1]; ?></h5  >
                </tr>
                </div>    
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo 'Kode Transaksi'; ?></th>
                                    <th><?php echo 'No. Rekening'; ?></th>
                                    <th><?php echo 'Tanggal Transaksi'; ?></th>
                                    <th><?php echo 'Deskripsi'; ?></th>
                                    <th><?php echo 'Debit'; ?></th>
                                    <th><?php echo 'Kredit'; ?></th>
                                    <th><?php echo 'Total'; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $from= date('Y/01/d', strtotime('-50 years', strtotime($_POST['tgl1'])));
                            //echo $from;
                            $from1= date('Y/m/d', strtotime('-1 month', strtotime($_POST['tgl1'])));
                            $to = date('Y/m/t', strtotime($from1));
                            $from2= date('Y/m/d 00:00:00.000', strtotime('-1 days', strtotime($_POST['tgl1'])));
                            //echo $to;
                           
                            //$a = "SELECT * FROM ( SELECT a.KodeJurnal,a.KodeAccount,a.Debet,a.Kredit,a.Keterangan,b.Tanggal, ROW_NUMBER() OVER (ORDER BY b.Tanggal asc) as row FROM [dbo].[JurnalDetail] a inner join [dbo].[JurnalHeader] b on a.KodeJurnal = b.KodeJurnal where a.KodeAccount = '$z[0]' and b.tanggal between '$from' and '$to') a ";
                            
                            //$a = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where Debet = '$z[0]' and CONVERT(date,Date) between '$from' and '$to' or Kredit = '$z[0]' and CONVERT(date,Date) between '$from' and '$to') a";
                           
                            
                            $a = "select * from dbo.AccountBalance where KodeAccount ='$z[0]' and Header = '5'and Tanggal = '$from2'";
                            //echo $a;
                            $b = sqlsrv_query($conn, $a);
                            
                            //$btotal = 0;
                            $ck = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                                //var_dump($ck);



                            if($ck == null){
                                $nilai = 0;
                            } else {
                                $nilai = $ck[10];
                            }
                                
                                
                             // //var_dump($c);
                             //        if($c[3] == $z[0] ){
                             //            $total3-=$c[5];
                             //            $ctotal2=$total3;               
                             //        }else{
                             //            $total3+=$c[5];
                             //            $ctotal2=$total3;
                             //        }
                             //        echo $total3;
                            ?>       
                             <tr>
                                <th colspan="6"><?php echo lang('Saldo Awal'); ?></th>
                                <th style="text-align: right;"><span><?php echo number_format($nilai,2); ?></span></th>
                            </tr>
                            
                    <?php
                    $fromtes = $_POST['tgl1'];
                    $from = date('Y/m/d 00:00:00.000', strtotime($fromtes));

                    $totes = $_POST['tgl2'];
                    $to = date('Y/m/d 23:59:59.998', strtotime($totes));
                    
                    //$a = "select * from dbo.Translist where date between '$from' and '$to'";
                    $a = "SELECT * FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$z[0]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Debet = '$z[0]' and a.Date between '$from' and '$to' or a.Kredit = '$z[0]' and a.Date between '$from' and '$to') a";
                    //echo $a;
                    $b = sqlsrv_query($conn, $a);
                    $total=$ck[10];
                    //echo $total;
                    $ctotal=$total;
                    //echo $ctotal;
                    //echo $total;
                    //$btotal = 0;
                    
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){


                        if ($z[4] == '1.1.00.00.000'){
                       
                         if($c[15] == 0) {
                            if($c[3] != $z[0] ){
                                $total-=$c[5];
                                $ctotal=$total;               
                            }else{
                                $total+=$c[5];
                                $ctotal=$total; 
                            }
                            
                        } else if ($c[15] == 1) {
                            //var_dump($c);
                            if($c[3] == $z[0] ){
                                $total+=$c[5];
                                $ctotal=$total;               
                            }else{
                                $total-=$c[5];
                                $ctotal=$total;
                                
                            }
                        }
                    } else {
                        if($c[15] == 0) {
                            if($c[3] == $z[0] ){
                                $total+=$c[5];
                                $ctotal=$total;               
                            }else{
                                $total-=$c[5];
                                $ctotal=$total; 
                            }
                            
                        } else if ($c[15] == 1) {
                            //var_dump($c);
                            if($c[3] == $z[0] ){
                                $total-=$c[5];
                                $ctotal=$total;               
                            }else{
                                $total+=$c[5];
                                $ctotal=$total;
                                
                            }
                        }
                    }
                            ?>                 
                    <tr>
                        <td><a href="buku_besarlap.php?trx=<?php echo $c[0]; ?>&tgl1=<?php echo $from; ?>&tgl2=<?php echo $to; ?>&st=5" target="_blank"><?php echo $c[0]; ?></a></td>
                        <td><?php  if ($z[0] == $c[3]){
                                    $akunr = $c[1];
                                    } else {
                                    $akunr = $c[2];
                                        } 
                                    echo $akunr; ?></td>
                        <td><?php echo $c[6]->format('Y-m-d H:i:s'); ?></td>
                        <td><?php echo $c[9]; ?></td>
                        <td style="text-align: right;"><?php if ($z[0] == $c[3]){
                                    $amountr = $c[5];
                                    } else {
                                    $amountr = 0;
                                        }
                                    echo number_format($amountr,2); ; ?></td>
                        <td style="text-align: right;"><?php if ($z[0] == $c[4]){
                                    $amountr = $c[5];
                                    } else {
                                    $amountr = 0;
                                        }
                                    echo number_format($amountr,2); ; ?></td>
                        <td style="text-align: right;"><?php echo number_format($total,2); ?></td>

                    </tr>
                     <?php } ?>
                     <?php

                        $fromtes = $_POST['tgl1'];
                        $from = date('Y/m/d 00:00:00.000', strtotime($fromtes));
                        $totes = $_POST['tgl2'];
                        $to = date('Y/m/d 23:59:59.998', strtotime($totes));
                        //$a = "select * from dbo.Translist where date between '$from' and '$to'";
                        $a = "SELECT * FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$z[0]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Debet = '$z[0]' and a.Date between '$from' and '$to' or a.Kredit = '$z[0]' and a.Date between '$from' and '$to') a";
                          //echo $a;
                        $b = sqlsrv_query($conn, $a);
                        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                            if($c[15] == 0) {
                                $amountdebit = "SELECT sum (amount) FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$z[0]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Kredit = '$z[0]' and a.Date between '$from' and '$to') a";
                                //echo $amountdebit;
                                $prosesdebit = sqlsrv_query($conn, $amountdebit);
                                $hasilDebit1 = sqlsrv_fetch_array($prosesdebit, SQLSRV_FETCH_NUMERIC); 
                                $hasilDebit = $hasilDebit1[0];

                                $amountKredit = "SELECT sum (amount) FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$z[0]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Debet = '$z[0]' and a.Date between '$from' and '$to') a";
                                //echo $amountKredit;
                                $prosesKredit = sqlsrv_query($conn, $amountKredit);
                                $hasilKredit1 = sqlsrv_fetch_array($prosesKredit, SQLSRV_FETCH_NUMERIC); 
                                $hasilKredit = $hasilKredit1[0];

                                } else if ($c[15] == 1) {
                                $amountdebit = "SELECT sum (amount) FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$z[0]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Kredit = '$z[0]' and a.Date between '$from' and '$to') a";
                                //echo $amountdebit;
                                $prosesdebit = sqlsrv_query($conn, $amountdebit);
                                $hasilDebit1 = sqlsrv_fetch_array($prosesdebit, SQLSRV_FETCH_NUMERIC); 
                                $hasilDebit = $hasilDebit1[0];

                                $amountKredit = "SELECT sum (amount) FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$z[0]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Debet = '$z[0]' and a.Date between '$from' and '$to') a";
                                //echo $amountKredit;
                                $prosesKredit = sqlsrv_query($conn, $amountKredit);
                                $hasilKredit1 = sqlsrv_fetch_array($prosesKredit, SQLSRV_FETCH_NUMERIC); 
                                $hasilKredit = $hasilKredit1[0];
                                
                            }
                        ?>
                             <tr>
                                <th colspan="4"><?php echo lang('Total Saldo Akhir'); ?></th>
                                <th style="text-align: right;"><?php echo number_format($hasilKredit,2); ?></th>
                                <th style="text-align: right;"><?php echo number_format($hasilDebit,2); ?></th>
                                <th style="text-align: right;"><span><?php echo number_format($ctotal,2); ?></span></th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>    
                              
                <?php } ?>

                    
            <!-- div dekat tanggal -->
            </div>
            </div>
        <?php } ?>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>


<script type="text/javascript">
    var tampung = [];

     $('.btn-addu').click(function(){
       var index = $('#detailUser tr').length;

        var user = $('#user option:selected').val();
        var amount = $('#amount').val();
        var date = $('#date').val();

        if(date == ''){
          alert('Assign date tidak boleh kosong');
          return false;
        }
        else if(user == '' || amount == '' || amount <= 0){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(user, tampung) !== -1){
            alert('Tidak dapat memilih user yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addteller.php",
                type : 'POST',
                dataType : 'json',
                data: { index: index, user: user, amount: amount, date: date},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.id);
                        $("#detailUser").append("<tr>" +
                            "<td><input type='hidden' name='user["+data.index+"]' value="+ data.id +">" + data.nama + "</td>" +
                            "<td><input type='hidden' name='amount["+data.index+"]' value="+ data.amount +">" + data.amount +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='ion-android-delete'></i></a></td>" +
                            "</tr>");

                            $('#user').val('');
                            $('#amount').val('');
                    }
                    else{
                        alert(data.message);
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });
</script>

<?php require('footer.php'); ?>
