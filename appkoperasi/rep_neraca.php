<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">

<style>
    .tengah{
        text-align:center;
        }
    .kiri{
        text-align:left;
        }
    .kanan{
        text-align:right;
        }

    ul, #tree_aktiva, #tree_pasiva {
        list-style-type: none;
        border-bottom: #3px solid #000;
    }

    #tree_aktiva, #tree_pasiva {
        margin: 0;
        padding: 0;
    }

    .caret {
        border-bottom: #1px solid #000;
        cursor: pointer;
        -webkit-user-select: none; /* Safari 3.1+ */
        -moz-user-select: none; /* Firefox 2+ */
        -ms-user-select: none; /* IE 10+ */
        user-select: none;
    }

    .caret::before {
        content: "\25B6";
        color: black;
        display: inline-block;
        margin-right: 6px;
    }

    .caret-down::before {
        -ms-transform: rotate(90deg); /* IE 9 */
        -webkit-transform: rotate(90deg); /* Safari */'
        transform: rotate(90deg);  
    }

    .nested {
      display: none;
      margin-left: 5%;
      margin-top: 5px;
    }

    .active {
      display: block;
    }
</style>

    <div class="page-content">

        <h3 class="box-title"><?php echo lang('Neraca'); ?></h3>

        <div class="form-inputs">
         <?php if(!isset($_GET['acc']) and !isset($_GET['acc1']) and !isset($_GET['acc2']) and !isset($_GET['acc3']) and !isset($_GET['acc4']) and !isset($_GET['acc5']) and !isset($_GET['acc6'])){ ?>
            <form action="" method="POST">
                <form>
                <h5 class="box-title"><?php echo lang('Periode dan Template'); ?></h5>
                <div class="input-field">

                    <?php
                        if(isset($_POST['bulan'])){
                            $from = $_POST['bulan'];
                        }
                        else{
                            $from = date('Y/m/d');
                        }
                        ?>


                        <select id="bulan" name="bulan" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['bulan']; ?>">- <?php echo lang('Pilih Bulan'); ?> -</option>
                            <?php
                            $bln=array(01=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
                            echo $bln;
                            $from='';
                            for ($bulan=01; $bulan<=12; $bulan++) { 
                                 if ($_POST['bulan'] == $bulan){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$bulan' $ck>$bln[$bulan]</option>";


                            }
                            ?>
                        </select>

                </div>
                <div class="input-field">
                    <?php
                        if(isset($_POST['tahun'])){
                            $to = $_POST['tahun'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>

                        <select id="tahun" name="tahun" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['tahun']; ?>">- <?php echo lang('Pilih Tahun'); ?> -</option>
                            <?php
                            $now=date("Y");
                            for ($tahun=2017; $tahun<=$now; $tahun++) { 
                                 if ($_POST['tahun'] == $tahun){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$tahun' $ck>$tahun</option>"; 
                            }

                            ?>
                        </select>
                </div>

                <div class="input-field">
                    <?php
                        if(isset($_POST['template'])){
                            $to = $_POST['template'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>

                        <select id="template" name="template" class="browser-default">
                            <option value=''>- <?php echo lang('Pilih Template'); ?> -</option>
                            <?php
                            $julsql   = "select * from [dbo].[Template] where Tipe = 0";
                            echo $julsql;
                            $julstmt = sqlsrv_query($conn, $julsql);
                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){ ?>
                                <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_POST['template']){echo "selected";} ?>><?=$rjulrow[1];?></option>
                            <?php } ?>
                        </select>

                </div>


                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_neraca.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
                </form>
            </form>
        </div>
        <?php } ?>

        <?php if(isset($_POST['bulan']) and isset($_POST['tahun']) and isset($_POST['template']) and !isset($_POST['acc'])){
                    $from1 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                    $from = date('Y-m-01', strtotime($from1));
                    $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                    $to = date('Y-m-t', strtotime($to3));
                    $to1 = date('m', strtotime($to3));
                    $to2 = date('Y', strtotime($to3));
                    $a = "exec dbo.ProsesGenerateLaporanNeraca '$to','$_SESSION[Name]'";
                    //echo $a;
                    

                    //echo $a;
                    $b = sqlsrv_query($conn, $a);
                    ?>

                    <div class="row m-l-0">
                        <!--<div class="col">
                             <a onclick="printDiv()" href="#"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-example" onclick="arHide('#modal-example')"> <?php echo lang('Print'); ?></button></a>
                        </div> -->
                        <div class="col">
                            <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-confirm" onclick="arHide('#modal-confirm')"> <?php echo lang('Download Template'); ?></button>
                        </div>
                        <div class="col">
                            <a href="rep_template.php?&st=5&set=00" target="_blank"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="delete"> <?php echo lang('Template'); ?></button>
                        </div>

                    </div>


                    <div class="hide" id="modal-confirm">
                        <h3 class="uppercase"><?php echo lang('Konfirmasi'); ?></h3>
                        <?php echo lang('Sudah Yakin Download Template ?'); ?><br><br>
                        <div class="row m-l-0">
                            <div class="col">
                                <a href="dneraca.php?bulan=<?php echo $_POST['bulan']; ?>&tahun=<?php echo $_POST['tahun']; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" target="_blank"><?php echo lang('Ya'); ?></button></a>
                            </div>
                           
                            <div class="col">
                                <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" onclick="arHide('#modal-confirm')"><?php echo lang('Tidak'); ?></button>
                            </div>
                        </div>
                    </div>

                    
                    <!--<div method="POST" action="">
                    <select id="level" name="level" class="browser-default" style="margin-top: 30px;">
                        <option value="">-Pilih Level-</option>
                        <option value="0">-0-</option>
                        <option value="1">-1-</option>
                        <option value="2">-2-</option>
                    </select>
                    <?php
                    if(isset($_GET['submit'])){
                        if(empty($_GET['level']))
                        {
                            $level= "Eror";
                        } else {
                            $level=$_GET['level'];
                        }
                    } 
                    ?>


                        <div style="margin-top: 20px;">
                            <button type="submit" class="waves-effect waves-light btn-large primary-color width-100" name="submit1" id="button1" value="submit"><?php echo lang('Pilih'); ?></button>
                        </div>
                    </div>-->

                <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">

                <div class="box-header" align="center">
                    <tr>
                        <h3 class="" style="margin-top: 30px;"><?php echo ('Laporan Neraca'); ?></h3>
                    </tr>
                    <tr>
                        <h3 class="" ><?php echo $_SESSION['NamaKoperasi']; ?></h3>
                    </tr>
                </div>
                <div class="box-header with-border" align="center">
                    <tr>
                        <?php
                            $bulan = $_POST['bulan'];
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        if($bulan !=12){
                                                                            Eror;
                                                                        } else {
                                                                           $bulan = 'Desember';
                                                                        }
                                                                    } else {
                                                                        $bulan = 'November';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
                        <h3 class="box-title" style="margin-bottom: 30px;"><?php echo "Periode : "; ?></h3>
                        <h3 class="box-title" style="margin-bottom: 30px;"><?php echo $bulan," - "; ?></h3>
                        <h3 class="box-title" style="margin-bottom: 30px;"><?php echo $_POST['tahun']; ?></h3>
                    </tr>
                    
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2" width="50%"><?php echo lang('Aktiva'); ?></th>
                            <th colspan="2" width="50%"><?php echo lang('Pasiva'); ?></th>
                        </tr>

                        <tr>
                            <?php /*
                            <td colspan="2" width="50%">
                                <table class="table table-bordered table-striped" width="50%">
                                    <tbody>
                                        <ul id="treeview" style="display: block;">
                                            <?php
                                            if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                $to = date('Y-m-d', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('d', strtotime($to));    
                                            } else {
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                $to = date('Y-m-t', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('t', strtotime($to));
                                            }
                                        
                                            //echo $to4;

                                            $name = $_SESSION[Name];
                                            $x = "select * from dbo.AccountBalance where KodeAccount in ('1.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('1.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                            //echo $x;
                                            $bcd = sqlsrv_query($conn, $abc);
                                            while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                $total = $def[0];
                                            }
                                    
                                            $y = sqlsrv_query($conn, $x);
                                            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){ ?>
                                                <!-- sub 1 -->
                                                <li data-icon-cls="fa fa-folder" ><?php echo $z[1].' - '.$z[2]; ?>
                                                    <span class="right"><?php echo number_format($z[10],2);?></span>
                
                                                    <ul id="treeview" style="display: block;">
                                                        <?php
                                                        $xx = "select * from dbo.AccountBalance where KodeGroup ='$z[1]' and Header = '2' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                        $yy = sqlsrv_query($conn, $xx);
                                                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                            <!-- sub 2 -->
                                                            <li data-icon-cls="fa fa-folder"><?php echo $zz[1].' - '.$zz[2]; ?>
                                                                <span class="right"><?php echo '  ' ?><?php echo number_format($zz[10],2);?></span>

                                                                <ul id="treeview" style="display: block;">
                                                                    <?php
                                                                    $xxx = "select * from dbo.AccountBalance where KodeTipe ='$zz[1]' and Header = '3' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                    //echo $xxx;
                                                                    $yyy = sqlsrv_query($conn, $xxx);
                                                                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                        <li data-icon-cls="fa fa-folder"><?php echo $zzz[1].' - '.$zzz[2]; ?>
                                                                            <span class="right"><?php echo '  ' ?><?php echo number_format($zzz[10],2);?></span>

                                                                            <ul id="ttreeview1" style="display: block;">
                                                                                <?php
                                                                                $xxxx = "select * from dbo.AccountBalance where KodeTipe ='$zzz[4]' and Header = '4' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzz[1],0,6)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                                //echo $xxxx;
                                                                                $yyyy = sqlsrv_query($conn, $xxxx);
                                                                                while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                                    <li data-icon-cls="fa fa-folder"><?php echo $zzzz[1].' - '.$zzzz[2]; ?>
                                                                                        <span class="right"><?php echo number_format($zzzz[10],2);?></span>

                                                                                        <ul class="treesh">
                                                                                            <?php
                                                                                            if($_POST['bulan'] == date('m')and $_POST['tahun'] == date('Y')){
                                                                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                                                                $to = date('Y-m-d', strtotime($to3));
                                                                                                $to1 = date('m', strtotime($to3));
                                                                                                $to2 = date('Y', strtotime($to3));
                                                                                                $to4 = date('d', strtotime($to));    
                                                                                            } else {
                                                                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                                                                $to = date('Y-m-t', strtotime($to3));
                                                                                                $to1 = date('m', strtotime($to3));
                                                                                                $to2 = date('Y', strtotime($to3));
                                                                                                $to4 = date('t', strtotime($to));
                                                                                            }
                                                                                            $name = $_SESSION[Name];
                                                                                            $abcde = 11111;
                                                                                            $xxxxx ="select * from dbo.AccountBalance where KodeTipe ='$zzzz[4]' and Header = '5'and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzzz[1],0,9)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                                            //echo $xxxxx;
                                                                                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                                                <li  data-icon-cls="fa fa-folder"><a href="rep_neracalap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[1]; ?>&st=5" target="_blank"><?php echo $zzzzz[1].' - '.$zzzzz[2]; ?></a>
                                                                                                    <span class="right"><?php echo number_format($zzzzz[10],2);?></span>
                                                                                                </li>
                                                                                            <?php } ?>
                                                                                        </ul>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </li>
                                                        <?php } ?>        
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </tbody>
                                </table>
                            </td>

                            <td colspan="2">
                                <table class="table table-bordered table-striped" width="50%">
                                    <tbody>
                                        <ul id="treeview1" style="display: block;">
                                            <?php
                                                if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                    $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                    $to = date('Y-m-d', strtotime($to3));
                                                    $to1 = date('m', strtotime($to3));
                                                    $to2 = date('Y', strtotime($to3));
                                                    $to4 = date('d', strtotime($to));    
                                                } else {
                                                    $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                    $to = date('Y-m-t', strtotime($to3));
                                                    $to1 = date('m', strtotime($to3));
                                                    $to2 = date('Y', strtotime($to3));
                                                    $to4 = date('t', strtotime($to));
                                                }
                                                $name = $_SESSION[Name];
                                                $btotal = 1;
                                                $x = "select * from dbo.AccountBalance where KodeAccount in ('2.0.00.00.000','3.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                //echo $x;
                                                $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('2.0.00.00.000', '3.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                //echo $abc;
                                                $bcd = sqlsrv_query($conn, $abc);
                                                while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                    $total1 = $def[0];
                                                }
                                                $y = sqlsrv_query($conn, $x);
                                                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){ ?>
                                                    <li data-icon-cls="fa fa-folder"><?php echo $z[1].' - '.$z[2];?>
                                                        <span class="right"><?php echo number_format($z[10],2);?></span>
                        
                                                        <ul id="treeview1" style="display: block;">
                                                        <?php
                                                        $xx = "select * from dbo.AccountBalance where KodeGroup ='$z[1]' and Header = '2' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                        //echo $xx;
                                                        $bcd = sqlsrv_query($conn, $abc);
                                                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                        $yy = sqlsrv_query($conn, $xx);
                                                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                        <li data-icon-cls="fa fa-folder"><?php echo $zz[1].' - '.$zz[2]; ?>
                                                            <span class="right"><?php echo number_format($zz[10],2);?></span>

                                                            <ul id="treeview3" style="display: block;">
                                                                <?php
                                                                $xxx = "select * from dbo.AccountBalance where KodeTipe ='$zz[1]' and Header = '3' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                //echo $xxx;
                                                                $yyy = sqlsrv_query($conn, $xxx);
                                                                while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                <li data-icon-cls="fa fa-folder"><?php echo $zzz[1].' - '.$zzz[2]; ?>
                                                                    <span class="right"><?php echo number_format($zzz[10],2);?></span>

                                                                    <ul id="treeview1" style="display: block;">
                                                                        <?php
                                                                        $xxxx = "select * from dbo.AccountBalance where KodeTipe ='$zzz[4]' and Header = '4' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzz[1],0,6)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                        //echo $xxxx;
                                                                        $yyyy = sqlsrv_query($conn, $xxxx);
                                                                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)) { ?>
                                                                            <li data-icon-cls="fa fa-folder"><?php echo $zzzz[1].' - '.$zzzz[2]; ?>
                                                                                <span class="right"><?php echo number_format($zzzz[10],2);?></span>

                                                                                <ul class="treesh">
                                                                                    <?php
                                                                                    if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                                                        $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                                                        $to = date('Y-m-d', strtotime($to3));
                                                                                        $to1 = date('m', strtotime($to3));
                                                                                        $to2 = date('Y', strtotime($to3));
                                                                                        $to4 = date('d', strtotime($to));    
                                                                                    } else {
                                                                                        $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                                                        $to = date('Y-m-t', strtotime($to3));
                                                                                        $to1 = date('m', strtotime($to3));
                                                                                        $to2 = date('Y', strtotime($to3));
                                                                                        $to4 = date('t', strtotime($to));
                                                                                    }
                                                                                    $name = $_SESSION[Name];
                                                                                    $abcde = 11111;
                                                                                    $xxxxx ="select * from dbo.AccountBalance where KodeTipe ='$zzzz[4]' and Header = '5'and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzzz[1],0,9)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                                    //echo $xxxxx;
                                                                                    $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                                    while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                                        <li  data-icon-cls="fa fa-folder"><a href="rep_neracalap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[1]; ?>&st=5" target="_blank"><?php echo $zzzzz[1].' - '.$zzzzz[2]; ?></a>
                                                                                            <span class="right"><?php echo number_format($zzzzz[10],2);?></span>
                                                                                        </li>
                                                                                    <?php } ?>
                                                                                </ul>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </li>
                                                            <?php  } ?>
                                                            </ul>
                                                        </li>
                                                        <?php  } ?>        
                                                        </ul>
                                                    </li>
                                            <?php } ?>
                                        </ul>
                                    </tbody>
                                </table>
                            </td>
                            
                            <tr>
                                <th><?php echo lang('Total Aktiva'); ?></th>
                                <th><span class="right"><?php echo number_format($total,2); ?></span></th>
                                <th><?php echo lang('Total Pasiva'); ?></th>
                                <th><span class="right"><?php echo number_format($total1,2); ?></span></th>
                            </tr>
                            */ ?>

                            <?php
                            function getIsActive($x, $y, $c) {
                                $sql1 = "SELECT Status from [dbo].[TemplateDetail_2] where IDTemplate='$x' and Account='$y' and State='5'";
                                //echo $sql1;
                                $query1 = sqlsrv_query($c, $sql1);
                                $data1 = sqlsrv_fetch_array($query1);

                                return $data1[0];
                            }
                            ?>

                            <tr>
                                <td colspan="2">
                                    <div class="m-t-10 demo">
                                        <ul id="tree_aktiva">
                                            <?php
                                            if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                $to = date('Y-m-d', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('d', strtotime($to));    
                                            } else {
                                                $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                $to = date('Y-m-t', strtotime($to3));
                                                $to1 = date('m', strtotime($to3));
                                                $to2 = date('Y', strtotime($to3));
                                                $to4 = date('t', strtotime($to));
                                            }
                                                
                                                //echo $to4;

                                                $name = $_SESSION[Name];
                                                $x = "select * from dbo.AccountBalance where KodeAccount in ('1.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('1.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                //echo $x;
                                                $bcd = sqlsrv_query($conn, $abc);
                                                while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                    $total = $def[0];
                                                }
                                        
                                                $g1 = 1;
                                                $y = sqlsrv_query($conn, $x);
                                                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){ 
                                                    $sact1 = getIsActive($_POST['template'], $z[1], $conn);
                                                    //echo 'sact1 : '.$sact1.'<br>';
                                                    ?>
                                                    <!-- sub 1 -->
                                                    <li><span class="caret <?php if($sact1 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'lvl1-ta-'.$g1; ?>"> <?php echo $z[1].' - '.$z[2]; ?> </span> <div class="right" style="font-weight: bold;" id="<?php echo 'lvl1-ta-lbl-'.$g1; ?>"><?php echo number_format($z[10],2);?></div>
                                                        <ul class="nested" id="tra-lvl1-<?php echo $g1; ?>">
                                                            <?php
                                                            $xx = "select * from dbo.AccountBalance where KodeGroup ='$z[1]' and Header = '2' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                            $yy = sqlsrv_query($conn, $xx);
                                                            $g2 = 2;
                                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                                                $sact2 = getIsActive($_POST['template'], $zz[1], $conn);
                                                                //echo 'sact2 : '.$sact2.'<br>';
                                                            ?>
                                                            <!-- sub 2 -->
                                                                <li><span class="caret <?php if($sact2 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'lvl2-ta-'.$g1.'-'.$g2; ?>"><?php echo $zz[1].' - '.$zz[2]; ?></span> <div class="right" style="font-weight: bold;" id="<?php echo 'lvl2-ta-lbl-'.$g1.'-'.$g2; ?>"><?php echo ' '.number_format($zz[10],2);?></div>
                                                                    <ul class="nested" id="tra-lvl2-<?php echo $g1.'-'.$g2; ?>">
                                                                        <?php
                                                                        $xxx = "select * from dbo.AccountBalance where KodeTipe ='$zz[1]' and Header = '3' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                        //echo $xxx;
                                                                        $yyy = sqlsrv_query($conn, $xxx);
                                                                        $g3 = 3;
                                                                        while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                                            $sact3 = getIsActive($_POST['template'], $zzz[1], $conn);
                                                                        ?>
                                                                        <li><span class="caret <?php if($sact3 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'lvl3-ta-'.$g1.'-'.$g2.'-'.$g3; ?>"><?php echo $zzz[1].' - '.$zzz[2]; ?></span> <div class="right" style="font-weight: bold;" id="<?php echo 'lvl3-ta-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>"><?php echo ' '.number_format($zzz[10],2);?></div>
                                                                            <ul class="nested" id="tra-lvl3-<?php echo $g1.'-'.$g2.'-'.$g3; ?>">
                                                                                <?php
                                                                                $xxxx = "select * from dbo.AccountBalance where KodeTipe ='$zzz[4]' and Header = '4' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzz[1],0,6)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                                                //echo $xxxx;
                                                                                $yyyy = sqlsrv_query($conn, $xxxx);
                                                                                $g4 = 4;
                                                                                while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                                                    $sact4 = getIsActive($_POST['template'], $zzzz[1], $conn);
                                                                                ?>
                                                                                <li><span class="caret <?php if($sact4 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'lvl4-ta-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>"><?php echo $zzzz[1] .' - '.$zzzz[2]; ?></span> <div class="right" style="font-weight: bold;" id="<?php echo 'lvl4-ta-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>"><?php echo number_format($zzzz[10],2);?></div>
                                                                                    <ul class="nested" id="tra-lvl4-<?php echo $g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>">
                                                                                        <?php
                                                                                        if($_POST['bulan'] == date('m')and $_POST['tahun'] == date('Y')){
                                                                                            $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                                                            $to = date('Y-m-d', strtotime($to3));
                                                                                            $to1 = date('m', strtotime($to3));
                                                                                            $to2 = date('Y', strtotime($to3));
                                                                                            $to4 = date('d', strtotime($to));    
                                                                                        } else {
                                                                                            $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                                                            $to = date('Y-m-t', strtotime($to3));
                                                                                            $to1 = date('m', strtotime($to3));
                                                                                            $to2 = date('Y', strtotime($to3));
                                                                                            $to4 = date('t', strtotime($to));
                                                                                        }
                                                                                        $name = $_SESSION[Name];
                                                                                        $abcde = 11111;
                                                                                        $xxxxx ="select * from dbo.AccountBalance where KodeTipe ='$zzzz[4]' and Header = '5'and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzzz[1],0,9)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                                        //echo $xxxxx;
                                                                                        $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                                        $g5 = 5;
                                                                                        while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                                            <li class="">
                                                                                                <a href="rep_neracalap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[1]; ?>&st=5" target="_blank"><?php echo $zzzzz[1].' - '.$zzzzz[2]; ?></a>
                                                                                                <div class="right" style="font-weight: bold;"><?php echo number_format($zzzzz[10],2);?></div>
                                                                                            </li>
                                                                                        <?php  } ?>
                                                                                    </ul>
                                                                                </li>
                                                                                <script type="text/javascript">
                                                                                    // Level 4
                                                                                    $('ul#tree_aktiva li <?php echo 'span#lvl4-ta-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').click(function(){
                                                                                        if(  $('<?php echo '#lvl4-ta-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').hasClass('hide') ) {
                                                                                            $('<?php echo '#lvl4-ta-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').removeClass('hide');
                                                                                        }
                                                                                        else {
                                                                                            $('<?php echo '#lvl4-ta-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').addClass('hide');
                                                                                        }
                                                                                    });

                                                                                    if( $('ul#tree_aktiva li <?php echo 'span#lvl4-ta-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').hasClass('caret-down') ) {
                                                                                        $('ul#tree_aktiva ul#tra-lvl4-<?php echo $g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').addClass('active');
                                                                                        $('<?php echo '#lvl4-ta-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').addClass('hide');
                                                                                    }
                                                                                </script>
                                                                                <?php $g4++; } ?>
                                                                            </ul>
                                                                        </li>
                                                                        <script type="text/javascript">
                                                                            // Level 3
                                                                            $('ul#tree_aktiva li <?php echo 'span#lvl3-ta-'.$g1.'-'.$g2.'-'.$g3; ?>').click(function(){
                                                                                if(  $('<?php echo '#lvl3-ta-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>').hasClass('hide') ) {
                                                                                    $('<?php echo '#lvl3-ta-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>').removeClass('hide');
                                                                                }
                                                                                else {
                                                                                    $('<?php echo '#lvl3-ta-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>').addClass('hide');
                                                                                }
                                                                            });

                                                                            if( $('ul#tree_aktiva li <?php echo 'span#lvl3-ta-'.$g1.'-'.$g2.'-'.$g3; ?>').hasClass('caret-down') ) {
                                                                                $('ul#tree_aktiva ul#tra-lvl3-<?php echo $g1.'-'.$g2.'-'.$g3; ?>').addClass('active');
                                                                                $('<?php echo '#lvl3-ta-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>').addClass('hide');
                                                                            }
                                                                        </script>
                                                                    <?php $g3++; } ?>
                                                                    </ul>
                                                                </li>

                                                                <script type="text/javascript">
                                                                    // Level 2
                                                                    $('ul#tree_aktiva li <?php echo 'span#lvl2-ta-'.$g1.'-'.$g2; ?>').click(function(){
                                                                        if(  $('<?php echo '#lvl2-ta-lbl-'.$g1.'-'.$g2; ?>').hasClass('hide') ) {
                                                                            $('<?php echo '#lvl2-ta-lbl-'.$g1.'-'.$g2; ?>').removeClass('hide');
                                                                        }
                                                                        else {
                                                                            $('<?php echo '#lvl2-ta-lbl-'.$g1.'-'.$g2; ?>').addClass('hide');
                                                                        }
                                                                    });

                                                                    if( $('ul#tree_aktiva li <?php echo 'span#lvl2-ta-'.$g1.'-'.$g2; ?>').hasClass('caret-down') ) {
                                                                        $('ul#tree_aktiva ul#tra-lvl2-<?php echo $g1.'-'.$g2; ?>').addClass('active');
                                                                        $('<?php echo '#lvl2-ta-lbl-'.$g1.'-'.$g2; ?>').addClass('hide');
                                                                    }
                                                                </script>
                                                            <?php $g2++; } ?>        
                                                        </ul>
                                                    </li>

                                                    <script type="text/javascript">
                                                        // Level 1
                                                        $('ul#tree_aktiva li <?php echo 'span#lvl1-ta-'.$g1; ?>').click(function(){
                                                            if(  $('<?php echo '#lvl1-ta-lbl-'.$g1; ?>').hasClass('hide') ) {
                                                                $('<?php echo '#lvl1-ta-lbl-'.$g1; ?>').removeClass('hide');
                                                            }
                                                            else {
                                                                $('<?php echo '#lvl1-ta-lbl-'.$g1; ?>').addClass('hide');
                                                            }
                                                        });

                                                        if( $('ul#tree_aktiva li <?php echo 'span#lvl1-ta-'.$g1; ?>').hasClass('caret-down') ) {
                                                            $('ul#tree_aktiva ul#tra-lvl1-<?php echo $g1; ?>').addClass('active');
                                                            $('<?php echo '#lvl1-ta-lbl-'.$g1; ?>').addClass('hide');
                                                        }
                                                    </script>
                                            <?php $g1++; } ?>
                                        </ul>
                                    </div>
                                </td>

                                <td colspan="2">
                                    <div class="m-t-10 demo" >
                                        <ul id="tree_pasiva">
                                            <?php
                                                if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                    $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                    $to = date('Y-m-d', strtotime($to3));
                                                    $to1 = date('m', strtotime($to3));
                                                    $to2 = date('Y', strtotime($to3));
                                                    $to4 = date('d', strtotime($to));    
                                                } else {
                                                    $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                    $to = date('Y-m-t', strtotime($to3));
                                                    $to1 = date('m', strtotime($to3));
                                                    $to2 = date('Y', strtotime($to3));
                                                    $to4 = date('t', strtotime($to));
                                                }
                                                $name = $_SESSION[Name];
                                                $btotal = 1;
                                                $x = "select * from dbo.AccountBalance where KodeAccount in ('2.0.00.00.000','3.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                //echo $x;
                                                $abc = "select SUM (Balance) from dbo.AccountBalance where KodeAccount in ('2.0.00.00.000', '3.0.00.00.000') and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                //echo $abc;
                                                $bcd = sqlsrv_query($conn, $abc);
                                                while($def = sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC)){
                                                    $total1 = $def[0];
                                                }
                                                $y = sqlsrv_query($conn, $x);
                                                $g1 = 1;
                                                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){ 
                                                    $sact1 = getIsActive($_POST['template'], $z[1], $conn);
                                                ?>
                                                <li><span class="caret <?php if($sact1 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'lvl1-tp-'.$g1; ?>"><?php echo $z[1].' - '.$z[2];?></span>  <div class="right" style="font-weight: bold;" id="<?php echo 'lvl1-tp-lbl-'.$g1; ?>"><?php echo number_format($z[10],2);?></div>
                                                    <ul class="nested" id="trp-lvl1-<?php echo $g1; ?>">
                                                    <?php
                                                   
                                                    $xx = "select * from dbo.AccountBalance where KodeGroup ='$z[1]' and Header = '2' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                    //echo $xx;
                                                    $bcd = sqlsrv_query($conn, $abc);
                                                    $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                                                    $yy = sqlsrv_query($conn, $xx);
                                                    $g2 = 2;
                                                    while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){ 
                                                        $sact2 = getIsActive($_POST['template'], $zz[1], $conn);
                                                    ?>
                                                    <li><span class="caret <?php if($sact2 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'lvl2-tp-'.$g1.'-'.$g2; ?>"><?php echo $zz[1].' - '.$zz[2]; ?></span> <div class="right" style="font-weight: bold;" id="<?php echo 'lvl2-tp-lbl-'.$g1.'-'.$g2; ?>"><?php echo number_format($zz[10],2);?></div>
                                                        <ul class="nested" id="trp-lvl2-<?php echo $g1.'-'.$g2; ?>">
                                                            <?php
                                                            $xxx = "select * from dbo.AccountBalance where KodeTipe ='$zz[1]' and Header = '3' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')"; 
                                                            //echo $xxx;
                                                            $yyy = sqlsrv_query($conn, $xxx);
                                                            $g3 = 3;
                                                            while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)) { 
                                                                $sact3 = getIsActive($_POST['template'], $zzz[1], $conn);
                                                            ?>
                                                            <li><span class="caret <?php if($sact3 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'lvl3-tp-'.$g1.'-'.$g2.'-'.$g3; ?>"><?php echo $zzz[1].' - '.$zzz[2]; ?></span> <div class="right" style="font-weight: bold;" id="<?php echo 'lvl3-tp-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>"><?php echo number_format($zzz[10],2);?></div>
                                                                <ul class="nested" id="trp-lvl3-<?php echo $g1.'-'.$g2.'-'.$g3; ?>">
                                                                <?php
                                                                $xxxx = "select * from dbo.AccountBalance where KodeTipe ='$zzz[4]' and Header = '4' and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzz[1],0,6)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                //echo $xxxx;
                                                                $yyyy = sqlsrv_query($conn, $xxxx);
                                                                $g4 = 4;
                                                                while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){ 
                                                                    $sact4 = getIsActive($_POST['template'], $zzzz[1], $conn);
                                                                ?>
                                                                <li><span class="caret <?php if($sact4 == 1) { echo 'caret-down'; } ?>" id="<?php echo 'lvl4-tp-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>"><?php echo $zzzz[1].' - '.$zzzz[2]; ?></span> <div class="right" style="font-weight: bold;" id="<?php echo 'lvl4-tp-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>"><?php echo number_format($zzzz[10],2);?></div>
                                                                    <ul class="nested" id="trp-lvl4-<?php echo $g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>">
                                                                        <?php
                                                                        if($_POST['bulan'] == date('m') and $_POST['tahun'] == date('Y')){
                                                                            $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/'.date('d', strtotime("-1 day"));
                                                                            $to = date('Y-m-d', strtotime($to3));
                                                                            $to1 = date('m', strtotime($to3));
                                                                            $to2 = date('Y', strtotime($to3));
                                                                            $to4 = date('d', strtotime($to));    
                                                                        } else {
                                                                            $to3 = $_POST['tahun'].'/'.$_POST['bulan'].'/01';
                                                                            $to = date('Y-m-t', strtotime($to3));
                                                                            $to1 = date('m', strtotime($to3));
                                                                            $to2 = date('Y', strtotime($to3));
                                                                            $to4 = date('t', strtotime($to));
                                                                        }
                                                                        $name = $_SESSION[Name];
                                                                        $abcde = 11111;
                                                                        $xxxxx ="select * from dbo.AccountBalance where KodeTipe ='$zzzz[4]' and Header = '5'and month(Tanggal) = '$to1' and DAY(Tanggal) = '$to4' and Year(tanggal) = '$to2' and KodeAccount like '%".substr($zzzz[1],0,9)."%' and KodeAccount in (select Account from TemplateDetail_2 where IDTemplate='$_POST[template]')";
                                                                        //echo $xxxxx;
                                                                        $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                        while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){ ?>
                                                                            <li>
                                                                                <a href="rep_neracalap.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&acc3=<?php echo $zzzzz[1]; ?>&st=5" target="_blank">
                                                                                    <?php echo $zzzzz[1].' - '.$zzzzz[2]; ?>
                                                                                </a> 
                                                                                <div class="right" style="font-weight: bold;"><?php echo number_format($zzzzz[10],2);?></div>
                                                                            </li>
                                                                        <?php  } ?>
                                                                    </ul>
                                                                </li>

                                                                <script type="text/javascript">
                                                                    // Level 4
                                                                    $('ul#tree_pasiva li <?php echo 'span#lvl4-tp-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').click(function(){
                                                                        if(  $('<?php echo '#lvl4-tp-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').hasClass('hide') ) {
                                                                            $('<?php echo '#lvl4-tp-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').removeClass('hide');
                                                                        }
                                                                        else {
                                                                            $('<?php echo '#lvl4-tp-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').addClass('hide');
                                                                        }
                                                                    });

                                                                    if( $('ul#tree_pasiva li <?php echo 'span#lvl4-tp-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').hasClass('caret-down') ) {
                                                                        $('ul#tree_pasiva ul#trp-lvl4-<?php echo $g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').addClass('active');
                                                                        $('<?php echo '#lvl4-tp-lbl-'.$g1.'-'.$g2.'-'.$g3.'-'.$g4; ?>').addClass('hide');
                                                                    }
                                                                </script>
                                                                <?php $g4++; } ?>
                                                                </ul>
                                                            </li>

                                                            <script type="text/javascript">
                                                                // Level 3
                                                                $('ul#tree_pasiva li <?php echo 'span#lvl3-tp-'.$g1.'-'.$g2.'-'.$g3; ?>').click(function(){
                                                                    if(  $('<?php echo '#lvl3-tp-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>').hasClass('hide') ) {
                                                                        $('<?php echo '#lvl3-tp-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>').removeClass('hide');
                                                                    }
                                                                    else {
                                                                        $('<?php echo '#lvl3-tp-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>').addClass('hide');
                                                                    }
                                                                });

                                                                if( $('ul#tree_pasiva li <?php echo 'span#lvl3-tp-'.$g1.'-'.$g2.'-'.$g3; ?>').hasClass('caret-down') ) {
                                                                    $('ul#tree_pasiva ul#trp-lvl3-<?php echo $g1.'-'.$g2.'-'.$g3; ?>').addClass('active');
                                                                    $('<?php echo '#lvl3-tp-lbl-'.$g1.'-'.$g2.'-'.$g3; ?>').addClass('hide');
                                                                }
                                                            </script>
                                                        <?php $g3++; } ?>
                                                        </ul>
                                                    </li>

                                                        <script type="text/javascript">
                                                            // Level 2
                                                            $('ul#tree_pasiva li <?php echo 'span#lvl2-tp-'.$g1.'-'.$g2; ?>').click(function(){
                                                                if(  $('<?php echo '#lvl2-tp-lbl-'.$g1.'-'.$g2; ?>').hasClass('hide') ) {
                                                                    $('<?php echo '#lvl2-tp-lbl-'.$g1.'-'.$g2; ?>').removeClass('hide');
                                                                }
                                                                else {
                                                                    $('<?php echo '#lvl2-tp-lbl-'.$g1.'-'.$g2; ?>').addClass('hide');
                                                                }
                                                            });

                                                            if( $('ul#tree_pasiva li <?php echo 'span#lvl2-tp-'.$g1.'-'.$g2; ?>').hasClass('caret-down') ) {
                                                                $('ul#tree_pasiva ul#trp-lvl2-<?php echo $g1.'-'.$g2; ?>').addClass('active');
                                                                $('<?php echo '#lvl2-tp-lbl-'.$g1.'-'.$g2; ?>').addClass('hide');
                                                            }
                                                        </script>
                                                    <?php $g2++; } ?>        
                                                    </ul>
                                                </li>
                                                <script type="text/javascript">
                                                    // Level 1
                                                    $('ul#tree_pasiva li <?php echo 'span#lvl1-tp-'.$g1; ?>').click(function(){
                                                        if(  $('<?php echo '#lvl1-tp-lbl-'.$g1; ?>').hasClass('hide') ) {
                                                            $('<?php echo '#lvl1-tp-lbl-'.$g1; ?>').removeClass('hide');
                                                        }
                                                        else {
                                                            $('<?php echo '#lvl1-tp-lbl-'.$g1; ?>').addClass('hide');
                                                        }
                                                    });

                                                    if( $('ul#tree_pasiva li <?php echo 'span#lvl1-tp-'.$g1; ?>').hasClass('caret-down') ) {
                                                        $('ul#tree_pasiva ul#trp-lvl1-<?php echo $g1; ?>').addClass('active');
                                                        $('<?php echo '#lvl1-tp-lbl-'.$g1; ?>').addClass('hide');
                                                    }
                                                </script>
                                         <?php $g1++; } ?>
                                        </ul>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <th><?php echo lang('Total Aktiva'); ?></th>
                                <th><span class="right"><?php echo number_format($total,2); ?></span></th>
                                <th><?php echo lang('Total Pasiva'); ?></th>
                                <th><span class="right"><?php echo number_format($total1,2); ?></span></th>
                            </tr>
                        </tr>
                    </table>
                </div> 

        <?php } ?>
    </div>

   
</div>


    <!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <?php /*
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>
    */ ?>
<script>
    var toggler = document.getElementsByClassName("caret");
    var i;

    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function() {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }

    /*
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });

    jQuery(function ($) {
        $("#treeview1").shieldTreeView();
    });
    
    jQuery(function ($) {
        $("#treeview3").TreeView();
        collapsed: true,
        unique: true,
        persist: “location”
    });
    */
    
</script>

<script type="text/javascript">
    
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}


    $("#button1").click(function(){
        var level = $("#level").val();
        window.open("level_neraca.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&level="+level);
    }); 
    
    function arHide(setID) {
            if( $(setID).hasClass('hide') ) $(setID).removeClass('hide');
            else $(setID).addClass('hide');
        }


$(function() {

  $('input[type="checkbox"]').change(checkboxChanged);

  function checkboxChanged() {
    var $this = $(this),
        checked = $this.prop("checked"),
        container = $this.parent(),
        siblings = container.siblings();

    container.find('input[type="checkbox"]')
    .prop({
        indeterminate: false,
        checked: checked
    })
    .siblings('label')
    .removeClass('custom-checked custom-unchecked custom-indeterminate')
    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

    checkSiblings(container, checked);
  }

  function checkSiblings($el, checked) {
    var parent = $el.parent().parent(),
        all = true,
        indeterminate = false;

    $el.siblings().each(function() {
      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
    });

    if (all && checked) {
      parent.children('input[type="checkbox"]')
      .prop({
          indeterminate: false,
          checked: checked
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(checked ? 'custom-checked' : 'custom-unchecked');

      checkSiblings(parent, checked);
    } 
    else if (all && !checked) {
      indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;

      parent.children('input[type="checkbox"]')
      .prop("checked", checked)
      .prop("indeterminate", indeterminate)
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

      checkSiblings(parent, checked);
    } 
    else {
      $el.parents("li").children('input[type="checkbox"]')
      .prop({
          indeterminate: true,
          checked: false
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass('custom-indeterminate');
    }
  }
});


</script>

<?php require('footer_new.php');?>
