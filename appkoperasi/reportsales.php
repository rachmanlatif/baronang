<?php
@session_start();
error_reporting(0);

if(isset($_GET['download'])) {

    $filePath = "uploads/excel/";

    require('connect.php');
    require('lib/phpexcel/Classes/PHPExcel.php');

    $today = @$_GET['tgl'];

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Baronang");
    $objPHPExcel->getProperties()->setTitle("Membership");

    // set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);

    $sloc = "SELECT LocationID, Nama from [dbo].[LocationMerchant] where LocationID='".@$_GET['loc']."'";
    $qloc = sqlsrv_query($conn, $sloc);
    $dloc = sqlsrv_fetch_array($qloc);

    // baris judul
    $objWorkSheet->getStyle('A1')->getFont()->setSize(15);
    $objWorkSheet->getStyle('A1')->getFont()->setBold(true);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1');
    $objWorkSheet->SetCellValue('A1', 'Sales Report '.date('d F Y', strtotime($today)));
    $objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:I4');
    $objWorkSheet->SetCellValue('A4', 'Sky Parking @ '.$dloc[1]);
    $objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);


    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:I5');
    $objWorkSheet->SetCellValue('A5', date('d F Y', strtotime(@$_GET['tgl'])));
    $objWorkSheet->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

    $objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A6', 'ID');

    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('B6', 'Status');

    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    $objWorkSheet->getStyle('C6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C6', 'Kendaraan');

    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
    $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D6', 'Guest Name');

    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E6', 'Transaction Date');

    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
    $objWorkSheet->getStyle('F6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('F6', 'Booking Source');

    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
    $objWorkSheet->getStyle('G6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('G6', 'Total Receivable');
    $objWorkSheet->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(4);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $objWorkSheet->getStyle('H6')->getFont()->setBold(true);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('H6:I6');
    $objWorkSheet->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H6', 'Currency');

    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
    $objWorkSheet->getStyle('J6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('J6', 'Payment Method');

    $row = 7;
    $sql = "SELECT DISTINCT TOP 100 b.CardNo as No_Kartu, 
                                    (SELECT Count(AccNo) from [dbo].[ParkingTrans] where KodeTransactionType='TOPP' and AccNo = b.CardNo and TimeStam between '$_GET[tgl] 00:00:00' and '$_GET[tgl] 23:59:59') as Count_Kartu,
                                    f.Name as Kendaraan,
                                    d.Name as Nama,
                                    c.TimeStam as Efective_Date,
                                    (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='TOPP' and AccNo = b.CardNo and TimeStam between '$_GET[tgl] 00:00:00' and '$_GET[tgl] 23:59:59') as Total_Receivable
            from Gateway.dbo.EDCList as a 
            inner join [dbo].[MemberCard] as b 
                on a.MemberID = b.MemberID
            inner join [dbo].[ParkingTrans] as c 
                on b.CardNo = c.AccNo 
            inner join [dbo].[MemberList] as d 
                on b.MemberID = d.MemberID
            inner join [dbo].[MemberLocation] as e 
                on c.AccNo = e.CardNo 
            inner join [dbo].[VehicleType] as f 
                on e.VehicleID = f.VehicleID
            where b.MemberID <> '' and e.LocationID='$_GET[loc]' and c.TimeStam between '$_GET[tgl] 00:00:00' and '$_GET[tgl] 23:59:59'
            order by c.TimeStam ASC";
    $que = sqlsrv_query($conn, $sql);
    while($data = sqlsrv_fetch_array($que)) {

        $objWorkSheet->SetCellValue('A'.$row, $data[0]);
        
        if($data[1] > 1) { $objWorkSheet->SetCellValue('B'.$row, 'Renewal'); }
        else { $objWorkSheet->SetCellValue('B'.$row, 'New'); }

        $objWorkSheet->SetCellValue('C'.$row, $data[2]);
        $objWorkSheet->SetCellValue('D'.$row, $data[3]);
        $objWorkSheet->SetCellValue('E'.$row, $data[4]->format('H:m:s'));
        $objWorkSheet->SetCellValue('F'.$row, 'EDC (ALL)');
        $objWorkSheet->SetCellValue('G'.$row, number_format($data[5], 2, ",", "."));
        $objWorkSheet->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        $objWorkSheet->SetCellValue('H'.$row, 'Rp. ');
        if($data[5] == 0) {
            $objWorkSheet->SetCellValue('I'.$row, number_format('0', 2, ",", "."));
        }
        else {
            $objWorkSheet->SetCellValue('I'.$row, number_format($data[5] * 1000, 2, ",", "."));
        }
        $objWorkSheet->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        $objWorkSheet->SetCellValue('J'.$row, 'Pay At Cashier');

        $row++;
    }


    //exit;
    $objWorkSheet->setTitle('Report Sales '.date('Y-m-d', strtotime($today)) );

    $fileName = 'Report Sales '.date('d F Y', strtotime($today)).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');


    // download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

}
else {
    require('header.php');
    require('sidebar-right.php');
    require('sidebar-left.php');
    ?>

    	<div class="animated fadeinup delay-1">
            <div class="page-content txt-black">

                <h1 class="uppercase">Sales Report</h1><br>

                <?php
                if(isset($_GET['sales'])) { 
                    $sloc = "SELECT LocationID, Nama from [dbo].[LocationMerchant] where LocationID='".@$_GET['loc']."'";
                    $qloc = sqlsrv_query($conn, $sloc);
                    $dloc = sqlsrv_fetch_array($qloc);
                    ?>
                    <div class="uppercase m-t-30">
                        <a href="reportsales.php" class="btn btn-large primary-color waves-effect waves-light" style="padding: 15px; margin-right: 5px;"><i class="ion-android-arrow-back"></i></a>
                        <a href="?download=1&sales=1&loc=<?php echo @$_GET['loc']; ?>&tgl=<?php echo @$_GET['tgl']; ?>" class="btn btn-large primary-color waves-effect waves-light" style="padding: 15px; margin-left: 5px;">Download</a><br><br>
                        <?php 
                            echo 'Sky Parking @ '.$dloc[1].'<br>';
                            echo date('d F Y', strtotime(@$_GET['tgl']));
                        ?>
                    </div>
                	<div class="table-responsive">
                		<table class="table width-100">
                			<thead>
                				<tr>
                					<th>ID</th>
                					<th>Status</th>
                					<th>Kendaraan</th>
                					<th>Guest Name</th>
                					<th>Transaction Date</th>
                					<th>Booking Source</th>
                					<th>Total Receivable</th>
                					<th colspan="2">Currency</th>
                					<th>Payment Method</th>
                				</tr>
                			</thead>

                			<tbody>
                				<?php
                				$sql = "SELECT DISTINCT TOP 100 b.CardNo as No_Kartu, 
                                                                (SELECT Count(AccNo) from [dbo].[ParkingTrans] where KodeTransactionType='TOPP' and AccNo = b.CardNo and TimeStam between '$_GET[tgl] 00:00:00' and '$_GET[tgl] 23:59:59') as Count_Kartu,
                                                                f.Name as Kendaraan,
                                                                d.Name as Nama,
                                                                c.TimeStam as Efective_Date,
                                                                (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='TOPP' and AccNo = b.CardNo and TimeStam between '$_GET[tgl] 00:00:00' and '$_GET[tgl] 23:59:59') as Total_Receivable
                                        from Gateway.dbo.EDCList as a 
                                        inner join [dbo].[MemberCard] as b 
                                            on a.MemberID = b.MemberID
                                        inner join [dbo].[ParkingTrans] as c 
                                            on b.CardNo = c.AccNo 
                                        inner join [dbo].[MemberList] as d 
                                            on b.MemberID = d.MemberID
                                        inner join [dbo].[MemberLocation] as e 
                                            on c.AccNo = e.CardNo 
                                        inner join [dbo].[VehicleType] as f 
                                            on e.VehicleID = f.VehicleID
                                        where b.MemberID <> '' and e.LocationID='$_GET[loc]' and c.TimeStam between '$_GET[tgl] 00:00:00' and '$_GET[tgl] 23:59:59'
                                        order by c.TimeStam ASC";
                                $que = sqlsrv_query($conn, $sql);
                                while($data = sqlsrv_fetch_array($que)) { ?>
                                    <tr>
                                        <td><?php echo $data[0]; ?></td>
                                        <td>
                                            <?php 
                                            if($data[1] > 1) { echo 'Renewal'; }
                                            else { echo 'New'; }
                                            ?>
                                        </td>
                                        <td><?php echo $data[2]; ?></td>
                                        <td><?php echo $data[3]; ?></td>
                                        <td><?php echo $data[4]->format('H:m:s'); ?></td>
                                        <td>EDC (ALL)</td>
                                        <td style="text-align: right;"><?php echo number_format($data[5], 2, ",", "."); ?></td>
                                        <td>Rp. </td>
                                        <td style="text-align: right;">
                                            <?php 
                                            if($data[5] == 0) {
                                                echo number_format('0', 2, ",", ".");
                                            }
                                            else {
                                                echo number_format($data[5] * 1000, 2, ",", ".");
                                            }
                                            ?>  
                                        </td>
                                        <td>Pay At Cashier</td>
                                    </tr>
                                    <?php
                                }
                				?>
                			</tbody>
                		</table>
                	</div>
                <?php
                }
                else { ?>
                    <div class="form-input">
                        
                        <div class="row">
                            
                            <div class="col s12">
                                <b>Pilih Lokasi</b> : <br>
                                <select class="browser-default" id="lokasi">
                                    <?php
                                    $sloc = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
                                    $qloc = sqlsrv_query($conn, $sloc);
                                    while ($dloc = sqlsrv_fetch_array($qloc)) { ?>
                                        <option value="<?php echo $dloc[0]; ?>"><?php echo $dloc[1]; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="col s6">
                                <b>Tanggal</b> : <br>
                                <input type="date" id="tgl" class="validate">
                            </div>

                        </div>

                        <div class="m-t-30">
                            <button id="btn_ok" class="btn btn-large primary-color waves effect waves-light width-100">OK</button>
                        </div>

                        <script type="text/javascript">
                            $('#btn_ok').click(function(){
                                var loc = $('#lokasi :selected').val();
                                var tgl = $('#tgl').val();
                                window.location.href = "?sales=1&loc="+loc+"&tgl="+tgl;
                            });
                        </script>

                    </div>
                    <?php
                }
                ?>
            </div>
    	</div>

    <?php 
    require('footer.php'); 
}
?>