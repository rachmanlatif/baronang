<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
require_once 'lib/phpExcel/Classes/PHPExcel/IOFactory.php';
include "connect.php";

if(isset($_FILES['filename'])) {
    $uploads_dir = 'uploads/excel/';
    $tmp_name = $_FILES["filename"]["tmp_name"];
    $path = $uploads_dir . basename($_FILES["filename"]["name"]);

    if(move_uploaded_file($tmp_name, $path)){
        //upload data from excel
        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getSheet(0);
        } catch(Exception $e) {
            messageAlert('Failed reading excel','warning');
            header('Location: upmemberlist.php');
        }

        $highestRow = $data->getHighestRow();
        $highestColumn = $data->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        if(!($highestRow -1) >0)
        {
            messageAlert('No data found in Sheet 0 or Sheet not available.','warning');
            header('Location: upmemberlist.php');
        }

        try {
            $filePath = "uploads/excel/";

            //cek header
            $rowHeader = $data->rangeToArray('A1:'.$highestColumn.'1',NULL,TRUE,FALSE);
            $hno = $rowHeader[0][0];
            $hmemberid = $rowHeader[0][1];
            $hktp = $rowHeader[0][2];
            $hnip = $rowHeader[0][3];
            $hname = $rowHeader[0][4];
            $haddress = $rowHeader[0][5];
            $htelp = $rowHeader[0][6];
            $hemail = $rowHeader[0][7];
            $hkjab = $rowHeader[0][8];

            //filter jika format tidak sesuai
            if($hno != 'No' or $hmemberid != 'Member ID' or $hktp != 'No. KTP' or $hnip != 'NIP' or $hname != 'Nama' or $haddress != 'Alamat' or $htelp != 'Telepon' or $hemail != 'Email' or $hkjab != 'Kode Jabatan'){
                messageAlert('Format template tidak sesuai','info');
                header('Location: upmemberlist.php');
            }

            //buat file excel jika ada yang gagal upload
            $objPHPExcel2 = new PHPExcel();
            $objPHPExcel2->getProperties()->setCreator("baronang");
            $objPHPExcel2->getProperties()->setTitle("Failed Upload Member");

            // set autowidth
            for($col = 'A'; $col !== 'Z'; $col++) {
                $objPHPExcel2->getActiveSheet()
                    ->getColumnDimension($col)
                    ->setAutoSize(true);
            }

            //sheet 1
            $objWorkSheet2 = $objPHPExcel2->createSheet(0);
            // baris judul
            $objWorkSheet2->SetCellValue('A1', 'No');
            $objWorkSheet2->SetCellValue('B1', 'Member ID');
            $objWorkSheet2->SetCellValue('C1', 'No. KTP');
            $objWorkSheet2->SetCellValue('D1', 'NIP');
            $objWorkSheet2->SetCellValue('E1', 'Nama');
            $objWorkSheet2->SetCellValue('F1', 'Alamat');
            $objWorkSheet2->SetCellValue('G1', 'Telepon');
            $objWorkSheet2->SetCellValue('H1', 'Email');
            $objWorkSheet2->SetCellValue('I1', 'Kode Jabatan');

            $no = 1;
            $baris = 2;
            //baca dari row 2
            $arr = array();
            for($row=2; $row<=$highestRow; ++$row) {

                $rowData = $data->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

                $memberid = $rowData[0][1];
                $ktp = number_format($rowData[0][2], 0, '.', '');
                $nip = $rowData[0][3];
                $nama = $rowData[0][4];
                $address = $rowData[0][5];
                $telp = number_format($rowData[0][6], 0, '.', '');
                $email = $rowData[0][7];
                $position = $rowData[0][8];

                //cek posisi
                $aaa = "select * from [dbo].[JabatanView] where NamaJabatan='$position'";
                $bbb = sqlsrv_query($conn, $aaa);
                $ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC);
                if(is_numeric($telp) and $ccc != null){
                    $psqlx = "select [dbo].[getKodeMember]('$_SESSION[KID]')";
                    $pstmtx = sqlsrv_query($conn, $psqlx);
                    $prowx = sqlsrv_fetch_array( $pstmtx, SQLSRV_FETCH_NUMERIC);

                    $ppsql = "exec [dbo].[ProsesMemberList] '$_SESSION[KID]', '$prowx[0]', '$nama', '$address', '0$telp', '$email', '1', '$ktp', '$nip', '$ccc[0]','$memberid'";
                    $ppstmt = sqlsrv_query($conn, $ppsql);

                    //create default
                    $eulsql   = "select top 1 * from dbo.RegularSavingType order by RegularSavingType asc";
                    $eulstmt  = sqlsrv_query($conn, $eulsql);
                    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
                    if($eulrow != null){
                        $ppsql = "exec [dbo].[ProsesRegularSavingAcc] '$_SESSION[KID]', '$prowx[0]', '$eulrow[0]', 0, '', 9";
                        $ppstmt = sqlsrv_query($conn, $ppsql);
                    }
                }
                else{
                    //push yang gagal
                    array_push($arr, $position);

                    //data ditolak
                    $objWorkSheet2->SetCellValue("A".$baris, $no);
                    $objWorkSheet2->SetCellValue("B".$baris, $memberid);
                    $objWorkSheet2->SetCellValueExplicit("C".$baris, $ktp, PHPExcel_Cell_DataType::TYPE_STRING);
                    $objWorkSheet2->SetCellValue("D".$baris, $nip);
                    $objWorkSheet2->SetCellValue("E".$baris, $nama);
                    $objWorkSheet2->SetCellValue("F".$baris, $address);
                    $objWorkSheet2->SetCellValueExplicit("G".$baris, $telp, PHPExcel_Cell_DataType::TYPE_STRING);
                    $objWorkSheet2->SetCellValue("H".$baris, $email);
                    $objWorkSheet2->SetCellValue("I".$baris, $position);

                    $no++;
                    $baris++;
                }
            }

            if($arr[0] != ""){
                $objWorkSheet2->setTitle('Failed Upload Member');

                $fileName = 'failedUploadMember'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel2,'Excel5');

                // download ke client
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="'.$fileName.'"');
                $objWriter->save('php://output');

                return $filePath.'/'.$fileName;
            }
            else{
                messageAlert('Berhasil menyimpan ke database','success');
                header('Location: upmemberlist.php');
            }

        } catch(Exception $e) {
            messageAlert('Gagal upload excel','danger');
            header('Location: upmemberlist.php');
        }
    }
    else{
        messageAlert('Gagal upload file','danger');
        header('Location: upmemberlist.php');
    }
}

?>