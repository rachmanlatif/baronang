<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

    <!-- Main Content -->

    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">


    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <!-- Mulai coding disini -->
            <h2 class="uppercase">Dashboard</h2>

            <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                    <?php echo $_SESSION['error-message']; ?>
                </div>
            <?php } ?>

            <?php if(isset($_SESSION['sesiteller'])){ ?>
            <div class="c-widget">
                <div class="c-widget-figure primary-color">
                    <i class="ion-android-mail"></i>
                </div>
                <div class="c-widget-body">
                    <p class="m-0">Sesi Teller Aktif</p>
                    <p class="small m-0"><?php echo $_SESSION['sesiteller']; ?></p>
                </div>
            </div>
          <?php } ?>

          <?php if(isset($_SESSION['sesikasir'])){ ?>
            <div class="c-widget">
                <div class="c-widget-figure primary-color">
                    <i class="ion-android-mail"></i>
                </div>
                <div class="c-widget-body">
                    <p class="m-0">Sesi Kasir Aktif</p>
                    <p class="small m-0"><?php echo $_SESSION['sesikasir']; ?></p>
                </div>
            </div>
          <?php } ?>

            <!-- Akhir baris coding -->
        </div>

    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="point"></h3>
              <p>Points</p>
              <p id="rp"></p>
            </div>
         </div>
        </div>
        <!-- ./col -->
         </div>
          <!-- ./row -->
      </section>
    </div> <!-- End of Main Contents -->

    <script type="text/javascript">
    var interval2 = 1000; //3detik

    function doAjaxDashboard(){
        $.ajax({
            url : "ajax_loaddashboard.php",
            type : 'POST',
            dataType : 'json',
            data: { },
            success : function(data) {
                if(data.status == 1){
                    $('#point').html(data.point);
                    $('#rp').html(data.rp);
                }
            },
            complete: function (data) {
                // Schedule the next
                setTimeout(doAjaxDashboard, interval2);
            }
        });
    }

    setTimeout(doAjaxDashboard, interval2);
    </script>

<?php require('footer.php');?>
