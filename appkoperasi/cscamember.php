<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-sm-8">
        <div class="box box-primary">
            <form action="procmemberclose.php" id="mpost" method="post">
            <div class="box-header with-border">
                <h3 class="box-title">Close Member</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="csoaregsavmember" class="col-sm-4 control-label" style="text-align: left;">Member ID</label>
                                <div class="col-sm-8">
                                    <input type="number" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?php echo $_SESSION['RequestMember']; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="nama" class="col-sm-3 control-label" style="text-align: left;">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo $_SESSION['RequestMemberName']; ?>" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-flat btn-block btn-primary btn-cek" data-toggle="modal" data-target=".bs-example-modal-lg">Cek</button>
                    </div>
                    <div class="col-sm-4">

                    </div>
                    <div class="col-sm-4">
                        <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Detail Member</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger pull-right btn-close">Close Member</button>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $('.btn-cek').click(function(){
        var mid = $('#csoaregsavmember').val();

        if(mid == ''){
            alert('Member ID harus diisi');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_cekbalance.php",
                type : 'POST',
                data: {mid: mid},
                success : function(data) {
                    $(".modal-body").html(data);
                },
                error : function(){
                    alert('Silahkan coba lagi.');
                }
            });
        }
    });

    $('.btn-close').click(function(){
        if(confirm('Apakah anda yakin menutup Member ini ?') == true){
            $('#mpost').submit();
        }
    });
</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>