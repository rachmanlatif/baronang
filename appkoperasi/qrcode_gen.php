<?php
//@session_start();
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

if(@$_GET['dw']) {
	$dw = @$_GET['dw'];
}

$kid = $_SESSION['KID'];

function rmdir_recursive($dir) {
    foreach(scandir($dir) as $file) {
        if ('.' === $file || '..' === $file) continue;
        if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
        else unlink("$dir/$file");
    }
    rmdir($dir);
}
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<h2 class="uppercase txt-black">QR Code Generator</h2>

			<div class="form-inputs m-t-30">
				<div  class="input-field" style="text-align: center;">
					<button type="button" name="btn_preview" id="btn_preview" class="btn primary-color waves-effect waves-light">Preview</button>
				</div>

				<div class="m-t-10 hide" id="preview_img">
					<img src="temp/qr7000251810155947.png" style="width: 300px; height: 300px; display: block; margin: 0 auto;">
				</div>

				<form action="proc_qrcode.php" method="post" class="m-t-30">
					<input type="hidden" name="tgl" id="tgl" value="<?php echo date('Y-m-d h:i:s'); ?>">
					<div class="input-field">
						<input type="number" name="jml" id="jml">
						<label for="jml">Jumlah (Min: 1, Max: 50)</label>
					</div>

					<button type="submit" name="btnok" id="btnok" class="btn btn-large primary-color width-100 waves-effect waves-light" >Generate</button>
				</form>

				<?php
				if(@$_GET['dw']) { ?>
					<div style="margin-top: 50px;">
						<form action="" method="post">
							<span class="uppercase center m-t-30">*mendownload qr code akan menghapus semua data di penyimpanan server</span>
							<button type="submit" name="btn_download" id="btn_download" class="btn btn-large primary-color width-100 waves-effect waves-light m-t-10">Download QR Code</button>
						</form>
					</div>
					<?php
					$download = @$_POST['btn_download'];
					if(isset($download)) {


						$zipfile = 'zipfile.zip';
						$zip = new ZipArchive();
						$zip->open($zipfile, ZipArchive::CREATE|ZipArchive::OVERWRITE);

						$rootPath = realpath('qrhasil');

						$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);

						foreach ($files as $name => $file) {
						    // Skip directories (they would be added automatically)
						    if (!$file->isDir())
						    {
						        // Get real and relative path for current file
						        $filePath = $file->getRealPath();
						        $relativePath = substr($filePath, strlen($rootPath) + 1);

						        // Add current file to archive
						        $zip->addFile($filePath, $relativePath);
						    }
						}


						/*
						$zip->addFile('zipfile.zip', 'ajax_acc.php');
						$zip->addFile('zipfile.zip', 'bank_list.php');
						$zip->addFile('zipfile.zip', 'coa.php');
						$zip->addFile('zipfile.zip', 'saldoawal.php');
						*/
						$zip->close();
						?>
						<script type="text/javascript">
							window.location.href = "zipfile.zip";
						</script>
						<?php
						rmdir_recursive('qrhasil');
					}
				}
				?>

				<script type="text/javascript">
					$('#btn_preview').click(function(){
						if( $('#preview_img').hasClass('hide') ) {
							$('#preview_img').removeClass('hide');
						}
						else {
							$('#preview_img').addClass('hide');
						}
					});
				</script>

			</div>

		</div>
	</div>

<?php require('footer.php');?>
