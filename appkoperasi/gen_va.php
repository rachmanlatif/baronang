<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php include "connectuser.php"; ?>

<?php include "connectinti.php"; ?>

	<div class="animated fadeinup delay-1">
		<div class="page-content">

			<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
					<div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
							<!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
							<h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
							<?php echo $_SESSION['error-message']; ?>
					</div>
			<?php } ?>

			<form action="procgen_va.php" method="post">
        Pilih bank
        <br>
        <select name="bank" id="bank" class="browser-default">
             <?php
            $sql = 'Select * from KoneksiKoperasiBaronang.dbo.BankList order by NamaBank asc';
            $query = sqlsrv_query($conn, $sql);
            while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                <?php if($row[0] == 'B0080'){ ?>
                  <option value="<?php echo $row[0] ;?>" selected><?php echo $row[1]; ?> </option>
                <?php } else { ?>
                  <option value="<?php echo $row[0] ;?>"><?php echo $row[1]; ?> </option>
                <?php } ?>
            <?php } ?>
        </select>
        <br>
				Masukan jumlah yang akan digenerate (max 1000)
				<br>
				<input type="number" name="generate" id="result-code" class="validate">
        <div class="row">
  				<div class="col s12">
  					<button type="submit" id="getdata" class="btn btn-large primary-color width-100 waves-effect waves-light">Generate</button>
  				</div>
  			</div>
			</form>
			<hr>
			<div class="box box-primary">
					<div class="box-body">
							<div class="table-responsive">
									<table class="table table-bordered table-striped">
									<thead>
									<tr>
											<th>No</th>
											<th>Generate Date</th>
											<th>Filename</th>
											<th>QTY Generate</th>
											<th></th>
									</tr>
									</thead>
									<tbody>
									<?php
									$no = 1;
									$aa   = "select * from [dbo].[VaMemberHeader] order by GenerateDate desc";
									$bb  = sqlsrv_query($conn, $aa);
									while($cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC)){
											?>
											<tr>
													<td><?= $no; ?></td>
													<td><?= date_format($cc[1],"Y-m-d H:i:s"); ?></td>
													<td><?= $cc[2] ?></td>
													<td><?= $cc[5] ?></td>
													<td>
														 <a href="<?php echo $cc[6]; ?>"><button type="button" class="btn btn-success btn-sm">Download</button></a>
													</td>
											</tr>
											<?php $no++; } ?>
									</tbody>
							</table>
							</div>
					</div>
			</div>

		</div>
	</div>

	<?php require('footer.php');?>
