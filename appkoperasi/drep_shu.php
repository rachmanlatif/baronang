<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Rekap SHU Anggota");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
    // baris judul
   
    $objWorkSheet->getStyle('D1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('D1', 'Rekap SHU Per Anggota');
    $objWorkSheet->getStyle('D2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D2', 'Lokasi :');
    $objWorkSheet->getStyle('D3')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D3')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D3', 'Periode :');

   
    


    //$objWorkSheet->margeCells('A5:B5');
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No. Anggota');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'Nama');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Simpanan');
    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F5', 'Pokok Pinjaman');
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G5', 'Bunga Pinjaman');
    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A6', 'New');  
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B6', 'Old');
    $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D6', 'Simpanan Pokok');
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E6', 'Simpanan Wajib');


    if($_GET['tahun'] and $_GET['koperasi'] and $_GET['kid']){
    
    $no = 1;
    $kop = 2;
    $bln = 3;
    $tahun = $_GET['tahun'];
    $koperasi = $_GET['koperasi'];
    $kid = $_GET['kid'];

    $from1 = $_GET['tahun'].'/01'.'/01';
    $from = date('Y-m-01 00:00:00.000', strtotime($from1));

    $to3 = $_GET['tahun'].'/12'.'/01';
    $to = date('Y-m-t 23:59:59.999', strtotime($to3));
   

    $objWorkSheet->getStyle('E'.$kop)->getFont()->setBold(true);
    $objWorkSheet->SetCellValue("E".$kop,$koperasi);   
    $objWorkSheet->getStyle('E'.$bln)->getFont()->setBold(true);
    $objWorkSheet->SetCellValue("E".$bln,$tahun);   



    // $k = 4;
    // $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
    // //echo $x;
    // $y = sqlsrv_query($conn, $x);
    // while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
           
    //     $objWorkSheet->getStyle('B'.$k)->getFont()->setBold(true);
    //     $objWorkSheet->SetCellValue("B".$k,$z[0] .' - '. $z[2]);           

    // }

    

    $row = 7;
    $rw = 7;
    
    $xy = "SELECT distinct userid, kid, kodemember FROM ( SELECT a.userid, a.date, b.KID, b.Kodemember, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join paymentgateway.dbo.usermemberkoperasi b on a.userid = b.userid where  kid = '$kid' and transactiontype = 'BSCP' and (accountkredit like '______101%' or accountkredit like '______102%') or  kid = '$kid' and transactiontype = 'LNCP' ) a where a.date between '$from' and '$to'";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
        $member = "select * from dbo.memberlist where memberID = $za[2] and kid = $za[1]";
        $pmember = sqlsrv_query($conn, $member);
        $hmember = sqlsrv_fetch_array($pmember, SQLSRV_FETCH_NUMERIC);


        //simpanan pokok
        $sp = "SELECT sum (amount) FROM ( SELECT a.accountkredit, a.date, a.transactiontype, a.amount, b.MemberID, b.KodeBasicSavingtype, b.paymentbalance, c.OldMemberID, c.name, d.status, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.basicsavingbalance b on a.accountKredit = b.Accno join dbo.memberlist c on b.MemberID = c.MemberID join dbo.BasicSavingType d on b.Kodebasicsavingtype = d.kodebasicsavingtype where transactiontype = 'BSCP' and d.status = '1' and b.memberID = '$za[2]' ) a where a.date between '$from' and '$to'";
        $psp = sqlsrv_query($conn, $sp);
        $hsp = sqlsrv_fetch_array($psp, SQLSRV_FETCH_NUMERIC);


        //Simpanan Wajib
        $sw = "SELECT sum (amount) FROM ( SELECT a.accountkredit, a.date, a.transactiontype, a.amount, b.MemberID, b.KodeBasicSavingtype, b.paymentbalance, c.OldMemberID, c.name, d.status, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.basicsavingbalance b on a.accountKredit = b.Accno join dbo.memberlist c on b.MemberID = c.MemberID join dbo.BasicSavingType d on b.Kodebasicsavingtype = d.kodebasicsavingtype where transactiontype = 'BSCP' and d.status = '0' and b.memberID = '$za[2]' ) a where a.date between '$from' and '$to'";
        $psw = sqlsrv_query($conn, $sw);
        $hsw = sqlsrv_fetch_array($psw, SQLSRV_FETCH_NUMERIC);

        //Pinjaman Pokok
        $pokokp = "SELECT sum(amount) from dbo.translist where transactiontype = 'LNCP' and userid = '$za[0]' and note like 'pembayaran pokok%' and date between '$from' and '$to'";
        $ppokokp = sqlsrv_query($conn, $pokokp);
        $hpokokp = sqlsrv_fetch_array($ppokokp, SQLSRV_FETCH_NUMERIC);
        //echo $pokok;


        //Bunga Pinjaman
        $bungap = "SELECT sum(amount) from dbo.translist where transactiontype = 'LNCP' and userid = '$za[0]' and note like 'pembayaran bunga%' and date between '$from' and '$to'";
        $pbungap = sqlsrv_query($conn, $bungap);
        $hbungap = sqlsrv_fetch_array($pbungap, SQLSRV_FETCH_NUMERIC);
        
               
        $objWorkSheet->SetCellValueExplicit("A".$row,$za[2]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$hmember[10]);
        $objWorkSheet->SetCellValueExplicit("C".$row,$hmember[2]);
        $objWorkSheet->getStyle('D' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("D".$row, number_format($hsp[0],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->getStyle('E' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("E".$row, number_format($hsw[0],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->getStyle('F' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("F".$row, number_format($hpokokp[0],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->getStyle('G' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("G".$row, number_format($hbungap[0],2), PHPExcel_Cell_DataType::TYPE_STRING);
        
        
        $row++;
        $rw++;
        }
 }
//exit;
    $objWorkSheet->setTitle('Drep Rekap SHU Anggota');

    $fileName = 'RekapSHU'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
