<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    echo $_SESSION['RequestMember'];
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-sm-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Open Regular Saving</h3>
            </div>
            <form action="proc_csoaregsaving.php" method="POST" class="form-horizontal">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="csoaregsavmember" class="col-sm-6 control-label" style="text-align: left;">Member ID</label>
                                    <div class="col-sm-6">
                                        <input type="number" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?php echo $_SESSION['RequestMember']; ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nama" class="col-sm-3 control-label" style="text-align: left;">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo $_SESSION['RequestMemberName']; ?>" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align: left;">Regular Saving Type</label>
                                    <div class="col-sm-9">
                                        <select name="reg" class="form-control">
                                            <option>--- Select One ---</option>
                                            <?php
                                            $julsql   = "select * from [dbo].[RegularSavingTypeView] order by RegularSavingType";
                                            $julstmt = sqlsrv_query($conn, $julsql);
                                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                                ?>
                                                <option value="<?=$rjulrow[0];?>"><?=$rjulrow[0];?> <?=$rjulrow[1];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama" class="col-sm-3 control-label" style="text-align: left;">Amount</label>
                                    <div class="col-sm-5">
                                        <input type="text" name="amount" class="form-control price" id="amount" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-flat btn-block btn-primary pull-right">Save</button>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                            <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
