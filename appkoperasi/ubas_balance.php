<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
require_once 'lib/phpExcel/Classes/PHPExcel/IOFactory.php';
include "connect.php";

if(isset($_FILES['filename'])) {
    $uploads_dir = 'uploads/excel/';
    $tmp_name = $_FILES["filename"]["tmp_name"];
    $path = $uploads_dir . basename($_FILES["filename"]["name"]);

    if(move_uploaded_file($tmp_name, $path)){
        //upload data from excel
        try{
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getSheet(0);
        } catch(Exception $e) {
            messageAlert('Failed reading excel','warning');
            header('Location: mbas_balance.php');
        }

        $highestRow = $data->getHighestRow();
        $highestColumn = $data->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        if(!($highestRow -1) >0)
        {
            messageAlert('No data found in Sheet 0 or Sheet not available.','warning');
            header('Location: mbas_balance.php');
        }

        try {
            $filePath = "uploads/excel/";

            //cek header
            $rowHeader = $data->rangeToArray('A1:'.$highestColumn.'1',NULL,TRUE,FALSE);
            $hname = $rowHeader[0][0];
            $hnip = $rowHeader[0][1];
            $hktp = $rowHeader[0][2];
            $hemail = $rowHeader[0][3];
            $htelp = $rowHeader[0][4];
            $hsp = $rowHeader[0][5];
            $hsw = $rowHeader[0][7];
            $hss = $rowHeader[0][9];

            //filter jika format tidak sesuai
            if($hname != 'Nama' or $hnip != 'NIP' or $hktp != 'KTP' or $hemail != 'Email' or $htelp != 'Telepon' or $hsp != 'Simpanan Pokok' or $hsw != 'Simpanan Wajib' or $hss != 'Simpanan Sukarela'){
                messageAlert('Format template tidak sesuai','info');
                header('Location: mbas_balance.php');
            }

            //buat file excel jika ada yang gagal upload
            $objPHPExcel2 = new PHPExcel();
            $objPHPExcel2->getProperties()->setCreator("baronang");
            $objPHPExcel2->getProperties()->setTitle("Failed Upload BS Balance");

            // set autowidth
            for($col = 'A'; $col !== 'Z'; $col++) {
                $objPHPExcel2->getActiveSheet()
                    ->getColumnDimension($col)
                    ->setAutoSize(true);
            }

            //sheet 1
            $objWorkSheet2 = $objPHPExcel2->createSheet(0);
            // baris judul
            $objWorkSheet2->SetCellValue('A1', 'Nama');
            $objWorkSheet2->SetCellValue('B1', 'NIP');
            $objWorkSheet2->SetCellValue('C1', 'KTP');
            $objWorkSheet2->SetCellValue('D1', 'Email');
            $objWorkSheet2->SetCellValue('E1', 'Telepon');
            $objWorkSheet2->SetCellValue('F2', 'Pay Amount');
            $objWorkSheet2->SetCellValue('G2', 'Tunggakan');
            $objWorkSheet2->SetCellValue('H2', 'Pay Amount');
            $objWorkSheet2->SetCellValue('I2', 'Tunggakan');
            $objWorkSheet2->SetCellValue('J2', 'Pay Amount');
            $objWorkSheet2->SetCellValue('K2', 'Tunggakan');

            $objWorkSheet2->SetCellValue('F1', 'Simpanan Pokok');
            $objWorkSheet2->SetCellValue('H1', 'Simpanan Wajib');
            $objWorkSheet2->SetCellValue('J1', 'Simpanan Sukarela');

            $objWorkSheet2->mergeCells('A1:A2');
            $objWorkSheet2->mergeCells('B1:B2');
            $objWorkSheet2->mergeCells('C1:C2');
            $objWorkSheet2->mergeCells('D1:D2');
            $objWorkSheet2->mergeCells('E1:E2');

            $objWorkSheet2->mergeCells('F1:G1');
            $objWorkSheet2->mergeCells('H1:I1');
            $objWorkSheet2->mergeCells('J1:K1');

            $baris = 3;
            //baca dari row 3
            $arr = array();
            for($row=3; $row<=$highestRow; ++$row) {

                $rowData = $data->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);

                $nama = $rowData[0][0];
                $nip = $rowData[0][1];
                $ktp = number_format($rowData[0][2], 0, '.', '');
                $email = $rowData[0][3];
                $telp = number_format($rowData[0][4], 0, '.', '');
                //pokok 1
                $ppay = number_format($rowData[0][5], 0, '.', '');
                $ptung = number_format($rowData[0][6], 0, '.', '');
                //wajib 0
                $wpay = number_format($rowData[0][7], 0, '.', '');
                $wtung = number_format($rowData[0][8], 0, '.', '');
                //sukarela 2
                $spay = number_format($rowData[0][9], 0, '.', '');
                $stung = number_format($rowData[0][10], 0, '.', '');

                if(is_numeric($ppay) and is_numeric($ptung) and is_numeric($wpay) and is_numeric($wtung) and is_numeric($spay) and is_numeric($stung)){
                    //save pokok
                    $ppsql1 = "exec [dbo].[ProsesImportBasicSaving] '$_SESSION[KID]','$nip','$ktp','$telp','$email','$ptung','$ppay',1,'$_SESSION[UserID]'";
                    $ppstmt1 = sqlsrv_query($conn, $ppsql1);
                    //echo $ppsql1;
                    //save wajib
                    $ppsql0 = "exec [dbo].[ProsesImportBasicSaving] '$_SESSION[KID]','$nip','$ktp','$telp','$email','$wtung','$wpay',0,'$_SESSION[UserID]'";
                    $ppstmt0 = sqlsrv_query($conn, $ppsql0);

                    //save sukarela
                    $ppsql2 = "exec [dbo].[ProsesImportBasicSaving] '$_SESSION[KID]','$nip','$ktp','$telp','$email','$stung','$spay',2,'$_SESSION[UserID]'";
                    $ppstmt2 = sqlsrv_query($conn, $ppsql2);

                    if(!$ppstmt1 or !$ppstmt0 or !$ppstmt2){
                        //push yang gagal
                        array_push($arr, $nama);

                        //data ditolak
                        $objWorkSheet2->SetCellValue("A".$baris, $nama);
                        $objWorkSheet2->SetCellValue("B".$baris, $nip);
                        $objWorkSheet2->SetCellValueExplicit("C".$baris, $ktp, PHPExcel_Cell_DataType::TYPE_STRING);
                        $objWorkSheet2->SetCellValue("D".$baris, $email);
                        $objWorkSheet2->SetCellValueExplicit("E".$baris, $telp, PHPExcel_Cell_DataType::TYPE_STRING);
                        $objWorkSheet2->SetCellValue("F".$baris, $ppay);
                        $objWorkSheet2->SetCellValue("G".$baris, $ptung);
                        $objWorkSheet2->SetCellValue("H".$baris, $wpay);
                        $objWorkSheet2->SetCellValue("I".$baris, $wtung);
                        $objWorkSheet2->SetCellValue("J".$baris, $spay);
                        $objWorkSheet2->SetCellValue("K".$baris, $stung);

                        $baris++;
                    }
                }
                else{
                    //push yang gagal
                    array_push($arr, $nama);

                    //data ditolak
                    $objWorkSheet2->SetCellValue("A".$baris, $nama);
                    $objWorkSheet2->SetCellValue("B".$baris, $nip);
                    $objWorkSheet2->SetCellValueExplicit("C".$baris, $ktp, PHPExcel_Cell_DataType::TYPE_STRING);
                    $objWorkSheet2->SetCellValue("D".$baris, $email);
                    $objWorkSheet2->SetCellValueExplicit("E".$baris, $telp, PHPExcel_Cell_DataType::TYPE_STRING);
                    $objWorkSheet2->SetCellValue("F".$baris, $ppay);
                    $objWorkSheet2->SetCellValue("G".$baris, $ptung);
                    $objWorkSheet2->SetCellValue("H".$baris, $wpay);
                    $objWorkSheet2->SetCellValue("I".$baris, $wtung);
                    $objWorkSheet2->SetCellValue("J".$baris, $spay);
                    $objWorkSheet2->SetCellValue("K".$baris, $stung);

                    $baris++;
                }
            }

            if(!empty($arr) and $arr[0] != ""){
                $objWorkSheet2->setTitle('Failed Upload BS Balance');

                $fileName = 'failedUploadBSBalance'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel2,'Excel5');

                // download ke client
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="'.$fileName.'"');
                $objWriter->save('php://output');

                return $filePath.'/'.$fileName;
            }
            else{
                messageAlert('Berhasil menyimpan ke database','success');
                header('Location: mbas_balance.php');
            }

        } catch(Exception $e) {
            messageAlert('Gagal upload excel','danger');
            header('Location: mbas_balance.php');
        }
    }
    else{
        messageAlert('Gagal upload file','danger');
        header('Location: mbas_balance.php');
    }
}
 //exit();
?>