<?php
error_reporting(0);
include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";

$filePath = "uploads/excel/";

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("baronang");
$objPHPExcel->getProperties()->setTitle("Data Upload COA");

// set autowidth
for($col = 'A'; $col !== 'Z'; $col++) {
    $objPHPExcel->getActiveSheet()
        ->getColumnDimension($col)
        ->setAutoSize(true);
}

//sheet 1
$objWorkSheet2 = $objPHPExcel->createSheet(0);
// baris judul
$objWorkSheet2->SetCellValue('A1', 'Account Code');
$objWorkSheet2->mergeCells('A1:E1');

$objWorkSheet2->SetCellValue('A2', 'Header 1');
$objWorkSheet2->SetCellValue('B2', 'Header 2');
$objWorkSheet2->SetCellValue('C2', 'Header 3');
$objWorkSheet2->SetCellValue('D2', 'Header 4');
$objWorkSheet2->SetCellValue('E2', 'Header 5');

$objWorkSheet2->SetCellValue('F1', 'Account Name');
$objWorkSheet2->mergeCells('F1:F2');

$objWorkSheet2->SetCellValue('G1', 'Category Account');
$objWorkSheet2->mergeCells('G1:G2');

if(isset($_GET['example'])){
    $objWorkSheet2->SetCellValue('A3', '1.0.00.00.000');
    $objWorkSheet2->SetCellValue('F3', 'Aktiva');
    $objWorkSheet2->SetCellValue('G3', 'Aktiva');
    $objWorkSheet2->SetCellValue('A4', '1.0.00.00.000');
    $objWorkSheet2->SetCellValue('B4', '1.1.00.00.000');
    $objWorkSheet2->SetCellValue('F4', 'Aktiva Lancar');
    $objWorkSheet2->SetCellValue('G4', 'Aktiva Lancar');
    $objWorkSheet2->SetCellValue('B5', '1.1.00.00.000');
    $objWorkSheet2->SetCellValue('C5', '1.1.01.00.000');
    $objWorkSheet2->SetCellValue('F5', 'Kas & Bank');
    $objWorkSheet2->SetCellValue('G5', 'Aktiva Lancar');
    $objWorkSheet2->SetCellValue('C6', '1.1.01.00.000');
    $objWorkSheet2->SetCellValue('D6', '1.1.01.01.000');
    $objWorkSheet2->SetCellValue('F6', 'Kas');
    $objWorkSheet2->SetCellValue('G6', 'Aktiva Lancar');
    $objWorkSheet2->SetCellValue('D7', '1.1.01.01.000');
    $objWorkSheet2->SetCellValue('E7', '1.1.01.01.001');
    $objWorkSheet2->SetCellValue('F7', 'Kas');
    $objWorkSheet2->SetCellValue('G7', 'Aktiva Lancar');

    $objWorkSheet2->SetCellValue('C8', '1.1.01.00.000');
    $objWorkSheet2->SetCellValue('D8', '1.1.01.02.000');
    $objWorkSheet2->SetCellValue('F8', 'Bank');
    $objWorkSheet2->SetCellValue('G8', 'Aktiva Lancar');
    $objWorkSheet2->SetCellValue('D9', '1.1.01.02.000');
    $objWorkSheet2->SetCellValue('E9', '1.1.01.02.001');
    $objWorkSheet2->SetCellValue('F9', 'Bank Mandiri');
    $objWorkSheet2->SetCellValue('G9', 'Aktiva Lancar');
}

$objWorkSheet2->setTitle('Data Master Coa');

$fileName = 'templateCoa'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$fileName.'"');
$objWriter->save('php://output');

return $filePath.'/'.$fileName;
?>