<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Daftar Pinjaman");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    

    $objWorkSheet->getStyle('B1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B1', 'Daftar Pinjaman');


    $objWorkSheet->getStyle('B2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('B2', 'Jenis Pinjaman');
    
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No');                
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'No. Pinjaman');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', ' Tipe Jatuh Tempo');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Pinjaman Awal');
    $objWorkSheet->getStyle('E5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E5', 'Pinjaman Disetujui');
    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F5', 'Tanggal Buka');
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G5', 'Tanggal Tutup');
    $objWorkSheet->getStyle('H5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('H5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H5', 'Sisa Angsuran');
    $objWorkSheet->getStyle('I5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('I5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('I5', 'Saldo');
    $objWorkSheet->getStyle('J5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('J5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('J5', 'Status');
    



    if(isset($_GET['id'])){ 

    $a = "select * from [dbo].[LoanTypeViewNew] where KodeLoanType='$_GET[id]'";
    //echo $a;
    $b =  sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);


    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("C2",$c[1]);
    

    $row = 6;
    $ind = $_GET['id'];
    $aaa = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY OpenDate asc) as row FROM [dbo].[LoanReportBalance] where KodeLoanType = '$ind') a ";
    //echo $aaa;
    $bbb = sqlsrv_query($conn, $aaa);
    $total= $awal;
    $ctotal = $total;
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        $ammoun = "select * from dbo.loanrelease where KodeAppNum = '$ccc[0]'";
        $pammoun = sqlsrv_query($conn, $ammoun);
        $hammoun  = sqlsrv_fetch_array( $pammoun, SQLSRV_FETCH_NUMERIC);

        $loan   = "select * from [dbo].[LoanTypeViewNew] where KodeLoanType ='$ccc[8]'";
        $type = sqlsrv_query($conn, $loan);
        $new  = sqlsrv_fetch_array( $type, SQLSRV_FETCH_NUMERIC);
        $nw = $new[9];

        $loanlist = "select * from dbo.loanlist where loanappnumber = '$ccc[0]'";
        $ploanlist = sqlsrv_query($conn, $loanlist);
        $hloanlist = sqlsrv_fetch_array($ploanlist, SQLSRV_FETCH_NUMERIC);
        $hsloanlist = $hloanlist[8];

        $loantrans = "select count(*) from dbo.Loantransaction where loannumber = '$ccc[0]'";
        //echo $loantrans;
        $ploantrans = sqlsrv_query($conn, $loantrans);
        $hloantrans = sqlsrv_fetch_array($ploantrans, SQLSRV_FETCH_NUMERIC);
        //var_dump($hloantrans);
        //$hsloantrans = $hloantrans[0];
        
        //echo $hsloanlist;
        //echo $hsloantrans;

        $sisaangsuran = $hsloanlist - $hloantrans[0];
        //echo $sisaangsuran;
        
        $open = $ccc[2]->format('Y-m-d H:i:s');
        $close = '';
            if($ccc[3] != ''){
                $close = $ccc[3]->format('Y-m-d H:i:s');
            }
                                               

            $objWorkSheet->SetCellValue("A".$row,$ccc[9]);
            $objWorkSheet->SetCellValue("B".$row, $ccc[0]);
            $objWorkSheet->SetCellValue("C".$row,$nw);
            $objWorkSheet->getStyle('D' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("D".$row,$ccc[1], PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('E' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("E".$row,number_format($hammoun[3],2), PHPExcel_Cell_DataType::TYPE_STRING); 
            $objWorkSheet->SetCellValue("F".$row,$open); 
            $objWorkSheet->SetCellValue("G".$row,$close);
            $objWorkSheet->getStyle('H' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("H".$row, $sisaangsuran , PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('I' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("I".$row, $ccc[5] , PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->SetCellValue("J".$row, $ccc[6]);


        $no++;
        $row++;
        }              
        $objWorkSheet->getStyle('A5:J' .$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
  
    }
//exit;
    $objWorkSheet->setTitle('Drep Daftar Pinjaman');

    $fileName = 'DafPinjaman'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
