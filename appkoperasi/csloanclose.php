<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<script language="javascript" type="text/javascript">
function showCompany(ind) {
document.frm.submit();
}
</script>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Tutup Pinjaman'); ?></h2> <br>
            <form class="form-horizontal" action="" method = "post" name="frm" id="frm">
            <div class="box-body">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="csoaregsavmember" style="text-align: left;">Member ID</label>
                        <input type="number" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?php echo $_SESSION['RequestMember']; ?>" readonly>
                                
                        <label for="nama" style="text-align: left;">Name</label>
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo $_SESSION['RequestMemberName']; ?>" disabled>        
                    </div>
                </div>
                <div class="col-sm-12">
                        <div class="row">
                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <?php
                                    $julsql   = "select * from [dbo].[LoanReportBalance] where MemberID='$_SESSION[RequestMember]'";
                                    //echo $julsql;
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    //     $loan = "select * from dbo.LoanList where LoanAppNumber = '$rjulrow[0]' and LoanAppNumber not in (select LoanAppNum from dbo.LoanClose)";
                                    //     echo $loan;
                                    //     $loan1 = sqlsrv_query($conn, $loan);
                                    //     $loan2 = sqlsrv_fetch_array( $loan1, SQLSRV_FETCH_NUMERIC);
                                     }
                                    ?>
                                <label ><?php echo lang('Pilih Pinjaman'); ?></label>
                                 <select id="ind" name="ind" class="browser-default" value="" onChange="showCompany(this.value);">
                                    <option value=''> <?php echo ('Semua Pinjaman'); ?> </option>
                                    <?php
                                    $julsql   = "select * from [dbo].[LoanReportBalance] where MemberID = '$_SESSION[RequestMember]' and LoanNumber not in (select LoanAppNum from dbo.LoanClose)";
                                    //echo $julsql;
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                        // $loan = "select * from dbo.LoanList where LoanAppNumber = '$rjulrow[0]' and LoanAppNumber not in (select LoanAppNum from dbo.LoanClose)";
                                        // $loan1 = sqlsrv_query($conn, $loan);
                                        // $loan2 = sqlsrv_fetch_array( $loan1, SQLSRV_FETCH_NUMERIC);
                                    ?>
                                    <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_POST['ind']){echo "selected";} ?>><?=$rjulrow[0];?></option>
                                    <?php } ?>
                                </select>
                            
                             
                                <?php
                                if ($_POST['ind']){
                                $acc = $_POST['ind'];
                                $a = "select * from [dbo].[LoanReportBalance] where LoanNumber='$acc'";
                                //echo $a;
                                $b =  sqlsrv_query($conn, $a);
                                while ($c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC)) {
                                    $xyz = "SELECT * FROM [dbo].[MemberListView] WHERE MemberID='$c[7]'";
                                    //echo $xyz;
                                    $yza = sqlsrv_query($conn,$xyz); 
                                    $zay = sqlsrv_fetch_array($yza, SQLSRV_FETCH_NUMERIC);

                                    $bun = "select * from dbo.LoanTypeViewNew where KodeLoanType = '$c[8]'";
                                    $nga = sqlsrv_query($conn,$bun); 
                                    $loan = sqlsrv_fetch_array($nga, SQLSRV_FETCH_NUMERIC);

                                
                                ?>
                                <div class="Active" style="padding-top: 30px">
                                    <label><?php echo ('No Rekening'); ?></label>
                                    <input type="hidden" name="" id="" value="<?php echo $c[0]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo $c[0]; ?>" readonly>
                                
                                </div>

                                <div class="Active">
                                    <label><?php echo ('Nama'); ?></label>
                                    <input type="hidden" name="" id="" value="<?php echo $zay[2]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo $zay[2]; ?>" readonly>
                                    
                                </div>

                                <div class="Active">
                                    <label><?php echo ('Saldo'); ?></label>
                                    <input type="hidden" name="" id="" value="<?php echo $c[5]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo $c[5]; ?>" readonly>
                                    
                                </div>

                                <div class="Active">
                                    <label><?php echo ('Biaya Pokok Pinalti(%)'); ?></label>
                                    <input type="hidden" name="" id="" value="<?php echo $loan[11]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo ($loan[11]); ?>" readonly>
                                    
                                </div>

                                <div class="Active">
                                    <label><?php echo ('Biaya Bunga Pinalti(%) '); ?></label>
                                    <input type="hidden" name="" id="" value="<?php echo $loan[12]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo ($loan[12]); ?>" readonly>
                                    
                                </div>

                                

                                <?php } ?>
                </div>
            </div>
            </form>
            <form action="procloanclose.php" method="post" class="form-horizontal">
             <?php 
                if ($_POST['member'] and $_POST['ind']){
                    $acc = $_POST['ind'];
                                $a = "select * from [dbo].[LoanReportBalance] where LoanNumber='$acc'";
                                //echo $a;
                                $b =  sqlsrv_query($conn, $a);
                                while ($c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC)) {
                                    $bun = "select * from dbo.LoanTypeViewNew where KodeLoanType = '$c[8]'";
                                    $nga = sqlsrv_query($conn,$bun); 
                                    $loan = sqlsrv_fetch_array($nga, SQLSRV_FETCH_NUMERIC);
                                
                                ?>

                    <div>
                        <input type="hidden" name="member" id="member" value="<?php echo $_POST['member']; ?>" readonly>
                        <input type="hidden" name="acc" id="acc" value="<?php echo $c[0]; ?>" readonly>
                        <input type="hidden" name="BPP" id="BPP" value="<?php echo $loan[11]; ?>" readonly>
                        <input type="hidden" name="BPB" id="BPB" value="<?php echo $loan[12]; ?>" readonly>
                    </div> 
                    
                    <div class="Active">
                            <label><?php echo ('Biaya Pinalti'); ?></label>
                            <input type="hidden" name="pinalty" id="pinalty" value="0" >
                            <input type="text" name="pinalty" class="validate" id="pinalty" value="0">
                                    
                    </div>
                    <?php } ?>  
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No Transaksi</th>
                                            <th>Tanggal Transaksi</th>
                                            <th><?php echo lang('Tipe Transaksi'); ?></th>
                                            <th><?php echo lang('Debit'); ?></th>
                                            <th><?php echo lang('Kredit'); ?></th>
                                        </tr>
                                    </thead>
                        
                                    <?php
                                        $aaa = "SELECT Top 5* FROM ( SELECT [TransactionNumber],[LoanNumber],[TimeStamp],[KodeTransactionType],[Description],[InvCount],[PrincipalPayment],[InterestPayment],[UserID], ROW_NUMBER() OVER (ORDER BY TimeStamp Desc) as row FROM [dbo].[LoanTransView] where LoanNumber = '$_POST[ind]') a";
                                        //echo $aaa;

                                    $bbb = sqlsrv_query($conn,$aaa );
                                    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                
                                    ?>
                                        <tr>
                                            <td><?php echo $ccc[9]; ?></td>
                                            <td><?php echo $ccc[0]; ?></td>
                                            <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                            <td>
                                                <?php
                                                $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                                $bbbb = sqlsrv_query($conn, $aaaa);
                                                $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                                if($cccc != null){
                                                    echo $cccc[1];
                                                }
                                                else{
                                                    echo $ccc[3];
                                                }
                                                ?>
                                            </td>
                                            <td style="text-align: right;"><?php echo number_format($ccc[6],2); ?></td>
                                            <td style="text-align: right;"><?php echo number_format($ccc[7],2) ; ?></td>
                                        </tr>
                                    <?php } ?>    
                                    <?php } ?>
                            </table>
                        </div>
                    </div>
                       
                       
                </div>  
                        <div class="box-footer">
                                <button type="submit" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Close</button>

                                <a href="identity_cs.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back</button></a>
                        </div>
                    <?php } ?>
            </form> 
    </div>
</div>



<?php require('content-footer.php');?>

<?php require('footer.php');?>
