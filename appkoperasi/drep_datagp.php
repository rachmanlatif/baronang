<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Data GP");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
    
    // baris judul
    $objWorkSheet->getStyle('E1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('E1', 'Data GP');



    //$objWorkSheet->getActiveSheet()->margeCell('A5:B5');
    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A6', 'No.');  
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B6', 'No. Kartu');
    $objWorkSheet->getStyle('C6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C6', 'Nama');
    $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D6', 'Alamat');
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E6', 'ID Lokasi');
    $objWorkSheet->getStyle('F6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F6', 'Nama Lokasi');
    $objWorkSheet->getStyle('G6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G6', 'Status');
    $objWorkSheet->getStyle('H6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H6', 'ID');
    $objWorkSheet->getStyle('I6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('I6', 'Vehicle ID');
    $objWorkSheet->getStyle('J6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('J6', 'Nama Vehicle');


if($_GET['akun']){

     $akun = $_GET['akun'];

    $row = 7;
    $rw = 7;
    
    $xy = "select * from (select a.CardNo, c.Name, c.addr, a.locationID, (select Nama from dbo.LocationMerchant where LocationID= a.LocationID) as NamaLokasi, a.status, a.ID, a.VehicleID, (select Name from dbo.VehicleType where VehicleID= a.VehicleID) as Vehicle, row_number() over (order by a.cardno asc) as row from dbo.memberlocationgp a inner join dbo.Membercard b on a.CardNo = b.CardNo  left join dbo.MemberList c on b.MemberID = c.MemberID where a.Status = 1  and a.CardNo not like '%100010001000%' and LocationID = '$akun') a";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
               
            $objWorkSheet->SetCellValueExplicit("A".$row,$za[9]);
            $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
            $objWorkSheet->SetCellValueExplicit("C".$row,$za[1]);
            $objWorkSheet->SetCellValueExplicit("D".$row,$za[2]);
            $objWorkSheet->SetCellValueExplicit("E".$row,$za[3]);
            $objWorkSheet->SetCellValueExplicit("F".$row,$za[4]);
            $objWorkSheet->SetCellValueExplicit("G".$row,$za[5]);
            $objWorkSheet->SetCellValueExplicit("H".$row,$za[6]);
            $objWorkSheet->SetCellValueExplicit("I".$row,$za[7]);
            $objWorkSheet->SetCellValueExplicit("J".$row,$za[8]);
            
        
        
        

        
        $row++;
        $rw++;
        }
} else {
    $akun = $_GET['akun'];

    $row = 7;
    $rw = 7;
    
    $xy = "select * from (select a.CardNo, c.Name, c.addr, a.locationID, (select Nama from dbo.LocationMerchant where LocationID= a.LocationID) as NamaLokasi, a.status, a.ID, a.VehicleID, (select Name from dbo.VehicleType where VehicleID= a.VehicleID) as Vehicle, row_number() over (order by a.cardno asc) as row from dbo.memberlocationgp a inner join dbo.Membercard b on a.CardNo = b.CardNo  left join dbo.MemberList c on b.MemberID = c.MemberID where a.Status = 1  and a.CardNo not like '%100010001000%') a";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
               
            $objWorkSheet->SetCellValueExplicit("A".$row,$za[9]);
            $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
            $objWorkSheet->SetCellValueExplicit("C".$row,$za[1]);
            $objWorkSheet->SetCellValueExplicit("D".$row,$za[2]);
            $objWorkSheet->SetCellValueExplicit("E".$row,$za[3]);
            $objWorkSheet->SetCellValueExplicit("F".$row,$za[4]);
            $objWorkSheet->SetCellValueExplicit("G".$row,$za[5]);
            $objWorkSheet->SetCellValueExplicit("H".$row,$za[6]);
            $objWorkSheet->SetCellValueExplicit("I".$row,$za[7]);
            $objWorkSheet->SetCellValueExplicit("J".$row,$za[8]);
            
        
        
        

        
        $row++;
        $rw++;
        }

}

//exit;
    $objWorkSheet->setTitle('Data GP');

    $fileName = 'DataGP'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
