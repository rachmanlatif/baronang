<?php require('header.php');?>
    <div class="row header-merchant">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <img class="img-responsive" src="enter.png" style="height: 50px;" />
        </div>
        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 unselected_text">
            <span class="" id="name-merchant">Merchant Name</span>
        </div>
    </div>
    <div class="row trc">
        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"><input type="text" name="calculated" class="calculated" id="calculated" readonly/></div>
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><button name="hpsNilai" class="btn-primary inhpsNilai" id="inhpsNilai"><span class="glyphicon glyphicon-arrow-left"></span></button></div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 colSwitch">
            <table class="table table-bordered" id="table_calc">
                <tr>
                    <td><button name="c1" id="c1" class="inbtn btn-primary">1</button></td>
                    <td><button name="c2" id="c2" class="inbtn btn-primary">2</button></td>
                    <td><button name="c3" id="c3" class="inbtn btn-primary">3</button></td>
                </tr>
                <tr>
                    <td><button name="c4" id="c4" class="inbtn btn-primary">4</button></td>
                    <td><button name="c5" id="c5" class="inbtn btn-primary">5</button></td>
                    <td><button name="c6" id="c6" class="inbtn btn-primary">6</button></td>
                </tr>
                <tr>
                    <td><button name="c7" id="c7" class="inbtn btn-primary">7</button></td>
                    <td><button name="c8" id="c8" class="inbtn btn-primary">8</button></td>
                    <td><button name="c9" id="c9" class="inbtn btn-primary">9</button></td>
                </tr>
                <tr>
                    <td><button name="cswitch" id="cswitch" class="inbtn btn-primary" onclick="btnSwitch()">Switch</button></td>
                    <td><button name="c0" id="c0" class="inbtn btn-primary">0</button></td>
                    <td><button name="cent" id="cent" class="inbtn btn-primary"><img src="enter.png" height="20" width="20"/></button></td>
                </tr>
            </table>
        </div>
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
    </div>

    <script type="text/javascript">
        function btnSwitch() {
            window.location.href = "number.php";
        }
        $(document).ready(function(){
            $('#c1').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '1');
            });
            $('#c2').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '2');
            });
            $('#c3').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '3');
            });
            $('#c4').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '4');
            });
            $('#c5').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '5');
            });
            $('#c6').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '6');
            });
            $('#c7').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '7');
            });
            $('#c8').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '8');
            });
            $('#c9').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '9');
            });
            $('#c0').click(function(){
                var text = $('#calculated').val();
                $('#calculated').val(text + '0');
            });
            $('#inhpsNilai').click(function(){
                $('#calculated').val($('#calculated').val().slice(0, -1));
            });
            $('#calculated').on('change',function(){
                console.log('Change event.');
                var val = $('#calculated').val();
                $('#calculated').text( val !== '' ? val : '(empty)' );
            });
            $('#calculated').change(function(){
                console.log('Second change event...');
            });
            $('#calculated').number( true, 0 );
        });
    </script>
    <br>
<?php require('footer.php');?>