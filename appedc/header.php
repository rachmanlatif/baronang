<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baronang App</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- ICON -->
    <link href="static/images/Logo-fish-icon.png" rel="shortcut icon">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="static/plugins/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="static/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="calc_style.css">

    <!-- jQuery 2.1.4 -->
    <script src="static/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="static/plugins/jquery-number/jquery.number.js"></script>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed">
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <a href="#" class="pull-left" style="padding: 8px;" onclick="window.location.href = '../appedc/'">
                <span class="logo-mini"><img width="160" height="30" src ="images/Baronang Logo-03.png"></span>
            </a>

            <a href="#" class="pull-right setting" style="padding: 8px;" onclick="window.location.href = 'setting.php'">
                <span class="glyphicon glyphicon-th-list"></span>
            </a>
        </nav>
    </header>

    <div class="content-wrapper" style="">
        <br>
