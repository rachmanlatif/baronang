<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">

            <h3 class="uppercase"><?php echo $lang->lang('Buka e-Money', $conn); ?></h3> <br>

            <div class="box-body">
                <form id="procpin" action="procemoney.php" method="POST" class="form-horizontal">
                    <div class="">

                        <div class="">
                            <?php echo $lang->lang('Produk e-money', $conn); ?><br>
                            <div class="table-responsive">

                                    <?php
                                    $a = "exec dbo.emoneyTypeView '$_SESSION[KID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        ?>
										<a class="modal-trigger" href="#bs-example-modal-<?php echo $c[0]; ?>">
											<input type="radio" name="acc" id="acc-<?php echo $c[0]; ?>" value="<?php echo $c[0]; ?>">
											<label for="acc-<?php echo $c[0]; ?>"><?php echo $c[1]; ?></label> <br>
										</a>
                                    <?php } ?>

                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="13" readonly>
                            </div>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Akun Tabungan', $conn); ?><br>
                            <div class="col-sm-9">
                                <select name="rsacc" class="browser-default">
                                    <option value=""><?php echo $lang->lang('Pilih', $conn); ?></option>
                                    <?php
                                    $a = "exec dbo.ListRegularSavingBal '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <option value="<?php echo $c[2]; ?>"><?php echo $c[2].' - '.$c[4].' , Rp. '.$c[6]; ?></option>
                                    <?php } ?>
                                </select>
                                <?php echo $lang->lang('e-Money akan menggunakan saldo akun Regular Saving yang dipilih', $conn); ?>
                            </div>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah Saldo', $conn); ?><br>
                            <input type="number" name="amount" class="validate price" id="amount" value="0">
                        </div>

                        <div class="m-t-30 text-center">
                            <h3>Masukan PIN anda</h3> <br>

                            <input id="pin" name="pin" type="password" class="validate text-center" style="font-size: 2em;background: white;" readonly>

							<div class="row">
							    <div class="col s4" style="padding: 20px;"><button type="button" id="n1" class="btn btn-lg primary-color" style="border-radius: 40px;">1</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n2" class="btn btn-lg primary-color" style="border-radius: 40px;">2</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n3" class="btn btn-lg primary-color" style="border-radius: 40px;">3</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n4" class="btn btn-lg primary-color" style="border-radius: 40px;">4</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n5" class="btn btn-lg primary-color" style="border-radius: 40px;">5</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n6" class="btn btn-lg primary-color" style="border-radius: 40px;">6</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n7" class="btn btn-lg primary-color" style="border-radius: 40px;">7</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n8" class="btn btn-lg primary-color" style="border-radius: 40px;">8</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n9" class="btn btn-lg primary-color" style="border-radius: 40px;">9</button></div>
								<div class="col s4" style="padding: 20px;"></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="n0" class="btn btn-lg primary-color" style="border-radius: 40px;">0</button></div>
								<div class="col s4" style="padding: 20px;"><button type="button" id="nb" class="btn btn-lg primary-color" style="border-radius: 40px;"><i class="ion-android-arrow-back"></i></button></div>
							</div>
                        </div>

                        <div class="m-t-30">
                            <a href="proccanceltrans.php?barcode=<?php echo $_GET['barcode']; ?>"><button type="button" class="btn primary-color btn-block"><i class="ion-android-cancel"></i> Batalkan Transaksi</button></a>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
	
	<?php
    $a = "exec dbo.emoneyTypeView '$_SESSION[KID]'";
    $b = sqlsrv_query($conn, $a);
    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
    ?>
        <div class="modal" id="bs-example-modal-<?php echo $c[0]; ?>">
            <div class="modal-content">
                <h4><?php echo $lang->lang('e-Money', $conn); ?> <?php echo $c[1]; ?></h4>
                
				<a href="#"><?php echo $lang->lang('Max Limit ', $conn); ?><span class="right"><?php echo number_format($c[2]); ?></span></a> <br>
                <a href="#"><?php echo $lang->lang('Fee Top Up', $conn); ?><span class="right"><?php echo number_format($c[3]); ?> </span></a> <br>
                <a href="#"><?php echo $lang->lang('Harga e-Money', $conn); ?><span class="right"><?php echo number_format($c[4]); ?></span></a>      
            </div>
			<div class="modal-footer">
				<button type="button" class="btn primary-color right modal-close" id="btn-<?php echo $c[0]; ?>"><?php echo $lang->lang('OK', $conn); ?></button>
			</div>
        </div>
		
		<script>
		$('#btn-<?php echo $c[0]; ?>').click(function () {
			$('#acc-<?php echo $c[0]; ?>').prop('checked', true);
		});
		</script>
		
    <?php } ?>

    <script type="text/javascript">
        function cekDigit(){
            var a = $('#pin').val();
            if(a.length == 6){
                $('#procpin').submit();
            }

            if(a.length > 6){
                $('#pin').val($('#pin').val().slice(0, -1));
            }
        }

        $('#n1').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '1');

            cekDigit();
        });
        $('#n2').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '2');

            cekDigit();
        });
        $('#n3').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '3');

            cekDigit();
        });
        $('#n4').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '4');

            cekDigit();
        });
        $('#n5').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '5');

            cekDigit();
        });
        $('#n6').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '6');

            cekDigit();
        });
        $('#n7').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '7');

            cekDigit();
        });
        $('#n8').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '8');

            cekDigit();
        });
        $('#n9').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '9');

            cekDigit();
        });
        $('#n0').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '0');

            cekDigit();
        });
        $('#nb').click(function(){
            $('#pin').val($('#pin').val().slice(0, -1));
        });
    </script>

<?php require('footer-login.php');?>
<?php require('footer_new.php');?>
