<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                </div>
            </div>
        </div>
    </div>

<?php
if($_POST['kid'] == '' || $_POST['mid'] == '' || $_POST['acc'] == '' || $_POST['pin'] == '' || $_POST['jenis'] == '' || $_POST['amount'] == '' || $_POST['amount'] <= 0){
    echo "<script language='javascript'>history.go(-1);</script>";
}
else{

    $kid = $_POST['kid'];
    $mid = $_POST['mid'];
    $acc = $_POST['acc'];
    $rsacc = $_POST['rsacc'];
    $amount = $_POST['amount'];
    $jenis = $_POST['jenis'];
    $pin = md5($_POST['pin']);

    $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$_SESSION[UserID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        if ($pin == $c[12])
        {
            $x = "exec [dbo].[RegularSavingAccSearch] '$_SESSION[KID]', '$_SESSION[MemberID]','$rsacc'";
            $y = sqlsrv_query($conn, $x);
            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
            if($z != null){
                $amacc = str_replace(',','',number_format($z[5],0));

                $q = "exec [dbo].[RegularSavingTypeSearch] '$_SESSION[KID]', '$z[3]'";
                $w = sqlsrv_query($conn, $q);
                $e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);

                $min = $e[4];
                $max = $e[5];
                if($amacc >= $amount){
                    if($amacc >= $min+$amount){
                        $sql = "exec [PaymentGateway].[dbo].[ProsesemoneyOpen] '$kid','$_SESSION[UserID]','$mid','$acc',$amount,'$rsacc',$jenis";
                        $exec = sqlsrv_query($conn, $sql);
                        if($exec){
                            messageAlert('Berhasil membuat e-Money. Silahkan melakukan link kartu e-Money');
                            echo "<script language='javascript'>document.location='notif.php';</script>";
                        }
                        else{
                            echo "<script>System.showToast('Gagal membuka e-Money');history.go(-2);</script>";
                        }
                    }
                    else{
                        echo "<script>System.showToast('Transaksi gagal. Minimum saldo tabungan adalah ".number_format($min)."');history.go(-2);</script>";
                    }
                }
                else{
                    echo "<script>System.showToast('Saldo tabungan tidak mencukupi. Transaksi tidak dapat dilakukan');history.go(-2);</script>";
                }
            }
            else{
                echo "<script>System.showToast('Akun tabungan tidak ditemukan');history.go(-2);</script>";
            }
        }
        else
        {
            echo "<script>System.showToast('PIN tidak benar. Transaksi tidak dapat dilakukan');history.go(-2);</script>";
        }
    }
    else{
        echo "<script language='javascript'>document.location='login.php';</script>";
    }
}
?>

<?php require('footer.php');?>