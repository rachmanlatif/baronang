<?php require('header-register.php');?>

<?php
$date = date('Y-m-d H:i:s');
$q = "select top 1 * from [Gateway].[dbo].[RequestPay] where UserID = '$_SESSION[UserID]' and Status = 0 and ExpiredDate >= '$date' order by ExpiredDate asc";
$w = sqlsrv_query($conn, $q);
$e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);
if($e != null){
    $x = "select* from [KoneksiKoperasiBaronang].[dbo].[ListKoperasi] where KID = '$e[0]' and Status = 1";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    if($z != null){ ?>

        <div class="register-box-body">
            <p class="login-box-msg">
                <label>Konfirmasi Pembayaran <?php echo $z[1]; ?></label>
            </p>
            <form id="procpos" action="procpos.php" method="post">
                <input type="hidden" name="sn" value="<?php echo $e[7]; ?>" readonly>
                <input type="hidden" name="kid" value="<?php echo $e[0]; ?>" readonly>
                <input type="hidden" name="acc" value="<?php echo $e[8]; ?>" readonly>
                <input type="hidden" name="amount" value="<?php echo $e[3]; ?>" readonly>

                <table class="table table-bordered table-striped">
                    <tr>
                        <td>Jumlah bayar</td>
                        <td>
                            <?php echo number_format($e[3]); ?>
                        </td>
                        <td>
                            <a href="procpos.php?sn=<?php echo $e[7]; ?>&delete"><button type="button" class="btn btn-primary btn-block btn-flat">Reject</button></a>
                        </td>
                    </tr>
                </table>

                <div class="col-sm-12">
                    <div class="box box-solid">
                        <div class="box-header text-center">
                            <h3 class="box-title">Masukan PIN anda</h3>
                        </div>
                        <div class="box-body text-center">
                            <input id="pin" name="pin" type="password" class="form-control text-center" style="font-size: 2em;background: white;" readonly>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n1" class="btn btn-lg btn-default" style="border-radius: 40px;">1</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n2" class="btn btn-lg btn-default" style="border-radius: 40px;">2</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n3" class="btn btn-lg btn-default" style="border-radius: 40px;">3</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n4" class="btn btn-lg btn-default" style="border-radius: 40px;">4</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n5" class="btn btn-lg btn-default" style="border-radius: 40px;">5</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n6" class="btn btn-lg btn-default" style="border-radius: 40px;">6</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n7" class="btn btn-lg btn-default" style="border-radius: 40px;">7</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n8" class="btn btn-lg btn-default" style="border-radius: 40px;">8</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n9" class="btn btn-lg btn-default" style="border-radius: 40px;">9</button></div>
                            <div class="col-xs-4" style="padding: 20px;"></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n0" class="btn btn-lg btn-default" style="border-radius: 40px;">0</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="nb" class="btn btn-lg btn-default" style="border-radius: 40px;"><i class="fa fa-chevron-left"></i></button></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <?php } } ?>

    <script type="text/javascript">
        function cekDigit(){
            var a = $('#pin').val();
            if(a.length == 6){
                $('#procpos').submit();
            }

            if(a.length > 6){
                $('#pin').val($('#pin').val().slice(0, -1));
            }
        }

        $('#n1').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '1');

            cekDigit();
        });
        $('#n2').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '2');

            cekDigit();
        });
        $('#n3').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '3');

            cekDigit();
        });
        $('#n4').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '4');

            cekDigit();
        });
        $('#n5').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '5');

            cekDigit();
        });
        $('#n6').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '6');

            cekDigit();
        });
        $('#n7').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '7');

            cekDigit();
        });
        $('#n8').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '8');

            cekDigit();
        });
        $('#n9').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '9');

            cekDigit();
        });
        $('#n0').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '0');

            cekDigit();
        });
        $('#nb').click(function(){
            $('#pin').val($('#pin').val().slice(0, -1));
        });
    </script>

<?php require('footer-login.php');?>