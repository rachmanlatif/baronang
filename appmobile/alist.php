<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

$amount = 0;
if(isset($_GET['amount'])){
    $amount = $_GET['amount'];
}
?>

    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Account Number Transfer</h3>
            </div>
            <div class="box-body">
                <form action="proclist.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Regular Saving Account</label>
                            <div class="col-sm-9">
                                <input type="number" name="acc" id="result-code" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-flat btn-block btn-success">Add</button>
                                <button type="button" id="ocam" class="btn btn-flat btn-block btn-primary"><i class="fa fa-camera"></i> Scan QR Code</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="box hide" id="form-camera">
            <div class="box-body">
                <div class="form-group">
                    <div class="input-group input-group">
                        <select class="form-control" id="camera-select"></select>
                            <span class="input-group-btn">
                            <button class="btn btn-default hide" id="decode-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
                            <button class="btn btn-info disabled hide" id="grab-img" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
                            <button class="btn btn-success" id="play" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-camera"></span></button>
                            <button class="btn btn-warning hide" id="pause" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-pause"></span></button>
                            <button class="btn btn-danger" id="stop" type="button" data-toggle="tooltip"><span class="glyphicon glyphicon-stop"></span></button>
                            </span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <canvas id="webcodecam-canvas" style="width: 100%;"></canvas>
                        <div class="thumbnail" id="result">
                            <div class="caption">
                                <p class="text-center" id="scanned-QR"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('#ocam').click(function(){
            $('#result-code').val();
            $('#form-camera').removeClass('hide');
        });
    </script>

<?php require('footer.php');?>