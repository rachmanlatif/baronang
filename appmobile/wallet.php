<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<div class="animated fadeinup delay-1">
    <div class="">

        <div class="col m-t-30" style="text-align: center;">
            <a class="modal-trigger" href="#myCard"><img src="images/kartu1.png" class="img-responsive"></a>
            <?php
            //QR code
            include('qrcode/qrlib.php');
            $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
            $PNG_WEB_DIR = 'temp/';

            $errorCorrectionLevel = 'H';
            $matrixPointSize = 5;
            $kode = $_SESSION['UserID'];
            $filename = $PNG_WEB_DIR.'qr'.md5($kode.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
            if (!file_exists($filename)){
                QRcode::png($kode, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
            }

            //barcode
            include('barcode128/BarcodeGenerator.php');
            include('barcode128/BarcodeGeneratorPNG.php');
            include('barcode128/BarcodeGeneratorSVG.php');
            include('barcode128/BarcodeGeneratorJPG.php');
            include('barcode128/BarcodeGeneratorHTML.php');
            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
            $resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
            ?>
            <br><img src="<?php echo $filename; ?>" style="width: 15%;top: 55%;right: 8%;position: absolute;">

            <span class="lead" style="position: absolute;top: 45%;left: 8%;color: white;"><?php echo $_SESSION['NamaUser']; ?></span>
            <span class="lead" style="position: absolute;top: 58%;left: 8%;color: white;"><?php echo $_SESSION['UserID']; ?></span>
            <img src="<?php echo $resource ?>" style="position: absolute;top: 70%;left: 8%;width: 40%;background: white;padding: 3px;">
        </div>

        <div class="">

            <?php
            $arr = array();
            $xx = "select* from [dbo].[ListKoperasiView] where UserID = '$_SESSION[UserID]'";
            $yy = sqlsrv_query($conn, $xx);
            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                array_push($arr, $zz[0]);

                $poi = "select* from $zz[3].[dbo].[Profil]";
                $lkj = sqlsrv_query($conn, $poi);
                $mnb = sqlsrv_fetch_array($lkj, SQLSRV_FETCH_NUMERIC);

                $cvb = "select* from $zz[3].[dbo].[CardDesign]";
                $dfg = sqlsrv_query($conn, $cvb);
                $ert = sqlsrv_fetch_array($dfg, SQLSRV_FETCH_NUMERIC);
                if($ert != null){
                    $imgcard = '../koperasi/'.$ert[0];
                    $fontcolor = $ert[1];
                }
                else{
                    $imgcard = '../koperasi/card/card.png';
                    $fontcolor = '#000000';
                }
                ?>
                <div class="col" style="text-align: center;">
                    <a href="connectkop.php?idkoperasiwallet=<?php echo $zz[0]; ?>">
                        <img class="img-responsive" style="border-radius: 10px;" src="<?php echo $imgcard; ?>" alt="Photo">
                        <?php
                        $code = $zz[0].' '.$zz[8];
                        $filename = $PNG_TEMP_DIR.'qr'.md5($code.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                        if (!file_exists($filename)){
                            QRcode::png($code, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                        }
                        ?>
                        <img src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" style="width: 15%;bottom: 10%;right: 8%;position: absolute;">
                        <span class="lead" style="position: absolute;top: 7%;right: 8%;color: <?php echo $fontcolor; ?>;">
                                <b>
                                    <?php
                                    $word = explode(' ', $mnb[2]);
                                    echo ucwords($word[0].' '.$word[1]);
                                    ?>
                                </b>
                            </span>
                        <span class="lead" style="position: absolute;top: 65%;left: 8%;color: <?php echo $fontcolor; ?>;"><?php echo $_SESSION['NamaUser']; ?></span>
                        <span class="lead" style="position: absolute;top: 80%;left: 8%;color: <?php echo $fontcolor; ?>;"><?php echo $zz[0].' '.$zz[8]; ?></span>
                        <?php if($mnb[6] != ''){ ?>
                            <img src="<?php echo '../koperasi/'.$mnb[6]; ?>" style="width: 15%;top: 7%;left: 8%;position: absolute;">
                        <?php } ?>
                    </a>
                </div>

                <div class="col"> <br> </div>
                <?php
            }

            if($arr == null){ ?>
                <div class="col" style="text-align: center;">
                    <span>
                        <?php echo $lang->lang('Anda harus menghubungkan akun ini ke koperasi atau pendaftaran sebagai member baru di koperasi', $conn); ?>
                    </span>
                </div> <?php
            } ?>

        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal" id="myCard" style="background: transparent; box-shadow: none;">
    <img src="images/kartu.png" style="height: 100%; width:auto;" class="modal-close">
    <img class="" src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" style="width: 30%;bottom: 6%;left: 13%;position: absolute;">
    <div class="" style="position: absolute;left: -14%;top: 22%;transform: rotate(90deg);">
        <span class="lead" style="color: white;font-size: 1.6em;"><?php echo $_SESSION['NamaUser']; ?></span>
        <br>
        <span class="lead" style="color: white;font-size: 1.6em;"><?php echo $_SESSION['UserID']; ?></span>
        <br>
        <img src="<?php echo $resource ?>" style="width: 80%;background: white; padding: 3px;">
    </div>
</div>

<?php require('footer_new.php');?>
