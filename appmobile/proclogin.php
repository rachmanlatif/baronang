<?php include('header-login.php');?>

<div style="text-align: center;margin-top: 45%;">
    <img width="50" height="50" src="images/loading.gif">
</div>

<?php
$email = $_POST['email'];
$password = $_POST['password'];

$pass = md5($password);
$a = "select* from [dbo].[UserPaymentGateway] where email = '$email' and Pass = '$pass'";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);

if($email == "" || $password == ""){
    //jika belum diisi
    echo "<script language='javascript'>System.showToast('Harap isi seluruh kolom');document.location='login.php';</script>";
}
else if (count($c[0]) > 0){
    //jika da datanya cek
    if($c[7] == 0){
        //jika status = 0/belum konfirmasi email
        echo "<script language='javascript'>System.showToast('Anda belum melakukan konfirmasi email');document.location='login.php';</script>";
    }
    else if($c[7] == 1){
        //jika sudah konfirmasi
        //filter hanya hp/tablet
        $tablet_browser = 0;
        $mobile_browser = 0;

        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $tablet_browser++;
        }

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
            $mobile_browser++;
        }

        if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
            $mobile_browser++;
        }

        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');

        if (in_array($mobile_ua,$mobile_agents)) {
            $mobile_browser++;
        }

        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
                $tablet_browser++;
            }
        }

        if ($tablet_browser <= 0 and $mobile_browser <= 0) {
            // if not phone/tablet
            messageAlert('Anda tidak dapat melakukan login melalui perangkat ini');
            header('location: login.php');
        }
        else {
            $session_key = md5(time());
            $session_expired = 60*15 + time(); //15 menit
            $mobileId = md5($_SERVER['HTTP_USER_AGENT']);

            //add session
            $_SESSION['UserID'] =  $c[0];
            $_SESSION['NamaUser'] =  $c[1];
            $_SESSION['Email'] =  $c[2];
            $_SESSION['tipe'] =  $c[17];
            $_SESSION['session_expired'] = $session_expired;
            $_SESSION['session_key'] = $session_key;
            $_SESSION['device'] = $mobileId;

            $upload_dir = "upload_pic"; 				// The directory for the images to be saved in
            $upload_path = $upload_dir."/";				// The path to where the image will be saved
            $large_image_prefix = "profile_"; 			// The prefix name to large image
            $large_image_prefix2 = "resize_"; 			// The prefix name to large image
            $large_image_name = $large_image_prefix.$c[0];     // New name of the large image (append the timestamp to the filename)
            $large_image_name2 = $large_image_prefix2.$c[0];     // New name of the large image (append the timestamp to the filename)

            //Image Locations
            $large_image_location1 = $upload_path.$large_image_name.'.jpg';
            $large_image_location2 = $upload_path.$large_image_name.'.png';
            $large_image_location3 = $upload_path.$large_image_name.'.gif';

            $large_image_location11 = $upload_path.$large_image_name2.'.jpg';
            $large_image_location22 = $upload_path.$large_image_name2.'.png';
            $large_image_location33 = $upload_path.$large_image_name2.'.gif';

            if(file_exists($large_image_location3) or file_exists($large_image_location33)){
                $_SESSION['user_file_ext'] = '.gif';
            }
            else if(file_exists($large_image_location2) or file_exists($large_image_location22)){
                $_SESSION['user_file_ext'] = '.png';
            }
            else{
                $_SESSION['user_file_ext'] = '.jpg';
            }

            if($mobileId <> $c[14] and $c[14] <> null and $c[14] <> ''){
                //jika dideteksi telah login dari perangkat lain
                echo "<script language='javascript'>document.location='deteksi.php';</script>";
            }
            else{ ?>
                <script type="text/javascript">
                    $.ajax({
                        url : "ajax_ceknotif.php",
                        type : 'POST',
                        dataType : 'json',
                        data: { },
                        success : function(data) {
                            if(data.status == 1){
                                Notification.push('Approval',data.message);
                            }
                        }
                    });
                </script>
            <?php
                //update data
                $aa = "update [dbo].[UserPaymentGateway] set SessionKey='".$session_key."', SessionExpired='".$session_expired."', MobileID='".$mobileId."', IsLogin=1  where email = '$email'";
                $bb = sqlsrv_query($conn, $aa);

                $_SESSION['url'] = 'profile.php';
                echo "<script language='javascript'>document.location='profile.php';</script>";
            }
        }
    }
    else{
        echo "<script language='javascript'>System.showToast('Login gagal');document.location='login.php';</script>";
    }
}
else{
    echo "<script language='javascript'>System.showToast('Email/Password salah');document.location='login.php';</script>";
}

?>

<?php require('footer.php');?>
