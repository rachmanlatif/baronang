<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
$x = "select* from [dbo].[UserPaymentGateway] where email = '$_SESSION[Email]'";
$y = sqlsrv_query($conn, $x);
$z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <div class="center m-0">
            <center>
                <?php
                $upload_dir = "upload_pic"; 				// The directory for the images to be saved in
                $upload_path = $upload_dir."/";				// The path to where the image will be saved
                $large_image_prefix = "profile_"; 			// The prefix name to large image
                $large_image_name = $large_image_prefix.$_SESSION['UserID'];     // New name of the large image (append the timestamp to the filename)

                //Image Locations
                $large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];

                if(file_exists($large_image_location)){?>
                    <img style="width: 100px; height: 100px;" class="profile-user-img img-responsive img-circle" src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" alt="User Avatar">
                <?php } else { ?>
                    <img style="width: 100px; height: 100px;" class="profile-user-img img-responsive img-circle" src="static/images/No-Image-Icon.png" alt="User Avatar">
                <?php } ?>

            </center>

            <div class="m-20">
                <div class="">
                    <a href="profile_edit.php"><h5 class="widget-user-desc"><?php echo $lang->lang('Ubah profil', $conn); ?></h5></a>
                </div>
            </div>
        </div>

        <div class="p-t-20 m-30">
            <ul class="tabs">
                <li class="tab">
                    <a class="active" href="#basic"> <?php echo $lang->lang('Info', $conn); ?> </a>

                </li>
                <li class="tab">
                    <a href="#premium"> <?php echo $lang->lang('Baronang Premium', $conn); ?> </a>

                </li>
            </ul>
        </div>

        <div id="basic">
            <div class="container">
                <div class="single-news animated fadeinright delay-3">
                    <b><?php echo $lang->lang('Nama', $conn); ?></b> <a class="right"><?php echo $z[1]; ?></a>
                </div>

                <div class="single-news animated fadeinright delay-3">
                    <b><?php echo $lang->lang('KTP', $conn); ?></b> <a class="right"><?php echo $z[9]; ?></a>
                </div>

                <div class="single-news animated fadeinright delay-3">
                    <b><?php echo $lang->lang('Telepon/HP', $conn); ?></b> <a class="right"><?php echo $z[3]; ?></a>
                </div>

                <div class="single-news animated fadeinright delay-3">
                    <b><?php echo $lang->lang('Email', $conn); ?></b> <a class="right"><?php echo $z[2]; ?></a>
                </div>

                <div class="single-news animated fadeinright delay-3">
                    <b><?php echo $lang->lang('Alamat', $conn); ?></b> <a class="right"><?php echo $z[4]; ?></a>
                </div>
            </div>
        </div>

        <div id="premium">
            <div class="container">
                <div class="single-news animated fadeinright delay-3">
                    <a href="#"><?php echo $lang->lang('Upgrade ke Baronang Premium agar bisa melihat kolom ini', $conn); ?></a>
                </div>
            </div>
        </div>

    </div>
</div>

<?php require('footer_new.php');?>