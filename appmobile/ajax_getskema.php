<?php
session_start();
include "connect.inc";

$a = "exec dbo.RegularSavingTypeSearch '$_SESSION[KID]','$_POST[acc]'";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);

if($c[2] == 0){
    $type = 'Saldo harian';
}
else if($c[2] == 1){
    $type = 'Saldo rata-rata';
}
else if($c[2] == 2){
    $type = 'Saldo terendah';
}
else{
    $type = '';
}
?>
<h4>Tabungan - <?php echo $c[1]; ?></h4>
<div>
    <div class="">
        <!--
        <ul class="tabs">
            <li class="tab"><a class="active" href="#tab_1">Info</a></li>
            <li class="tab"><a href="#tab_2">Skema Bunga</a></li>
        </ul>
        -->

        <div id="tab_1">
            <b>Info</b> <br>
            <a href="#">Tipe Bunga <span class="right"><?php echo $type; ?></span></a><br>
            <a href="#">Suku Bunga % <span class="right"><?php echo $c[3]; ?>%</span></a><br>
            <a href="#">Saldo Minimum <span class="right"><?php echo number_format($c[4]); ?></span></a><br>
            <a href="#">Penarikan Maksimum <span class="right"><?php echo number_format($c[5]); ?></span></a><br>
            <a href="#">Biaya Admin <span class="right"><?php echo number_format($c[6]); ?></span></a><br>
            <a href="#">Biaya Penutupan <span class="right"><?php echo number_format($c[9]); ?></span></a><br>
        </div>

        <div class="m-t-20" id="tab_2">
            <b>Skema Bunga</b> <br>
            <div class="table-responsive">
                <table class="table">
                    <?php
                    $zxc = "select * from $_SESSION[Kop].dbo.SkemaBunga where RegularSavingType = '$_POST[acc]' order by Urutan asc";
                    $asd = sqlsrv_query($conn, $zxc);
                    while($qwe = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <th><?php echo number_format($qwe[2]).' - '.number_format($qwe[3]); ?></th>
                            <td><?php echo $qwe[4]; ?>%</td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>

    </div>
</div>