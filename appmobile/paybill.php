<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_GET['acc']) or $_GET['acc'] == ''){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}

?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <h3 class="uppercase"><?php echo $lang->lang('Bayar billing', $conn); ?></h3> <br>

            <div class="form-inputs">
                <form action="procauth.php" method="POST" class="form-horizontal">

                    <div class="col-sm-12">

                        <div class="">
                            <div class="col-sm-9">
                                <input type="hidden" name="acc" value="<?php echo $_GET['acc']; ?>" readonly>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="17" readonly>
                            </div>
                        </div>

                        <div class="">
                            <?php echo $lang->lang('Pilih Akun Tabungan', $conn); ?><br>
                            <div class="table-responsive">
                                    <?php
                                    $a = "exec [dbo].[ListRegularSavingBal] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                        <input type="radio" name="regacc" class="minimal" id="regacc-<?php echo $c[2]; ?>" value="<?php echo $c[2]; ?>">
                                        <label for="regacc-<?php echo $c[2]; ?>">
                                            <b><?php echo $c[2]; ?></b> - <?php echo $c[4]; ?> <br>
                                            <span class="">Rp. <?php echo number_format($c[5]); ?></span>
                                        </label>
                                        <br><br>
                                    <?php } ?>
                            </div>
                        </div>

                        <?php
                        $jumlah = 0;
                        $xx = "exec dbo.ListBillSearch '$_SESSION[KID]','$_GET[acc]'";
                        $yy = sqlsrv_query($conn, $xx);
                        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                        if($zz != null){
                            $jumlah = $zz[6]; //amount+admin+denda
                        }
                        ?>

                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah Tagihan', $conn); ?><br>
                            <input type="text" class="validate price" value="<?php echo number_format($jumlah); ?>" readonly>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah Bayar', $conn); ?> <br>
                            <input type="text" name="amount" class="validate price" value="0">
                        </div>

                        <div class="m-t-30">
                            <button type="submit" class="btn btn-block primary-color"><?php echo $lang->lang('Bayar', $conn); ?></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

<?php require('footer_new.php');?>
