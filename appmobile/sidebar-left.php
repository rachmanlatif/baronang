<ul id="slide-out-left" class="m-t-50 side-nav collapsible">
    <li>
        <div class="sidenav-header primary-color">
            <div class="nav-avatar">
                <?php
                $upload_dir = "upload_pic"; 				// The directory for the images to be saved in
                $upload_path = $upload_dir."/";				// The path to where the image will be saved
                $large_image_prefix = "profile_"; 			// The prefix name to large image
                $large_image_name = $large_image_prefix.$_SESSION['UserID'];     // New name of the large image (append the timestamp to the filename)

                //Image Locations
                $large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];

                if(file_exists($large_image_location)){?>
                    <img class="circle avatar" src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" alt="User Avatar">
                <?php } else { ?>
                    <img class="circle avatar" src="static/images/No-Image-Icon.png" alt="User Avatar">
                <?php } ?>
                <div class="avatar-body">
                    <?php
                    $x = "select * from [dbo].[UserPaymentGateway] where email = '$_SESSION[Email]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    ?>
                    <h5 style="color: #fff;"><?php echo $z[1]; ?></h5>
                </div>
            </div>
        </div>
    </li>

    <?php if(isset($_SESSION['KID'])){ ?>
        <li>
            <a href="balance.php" class="no-child">
                <i class="ion-ios-home"></i>
                <?php
                $word = explode(' ', $_SESSION['KoperasiName']);
                echo ucwords($word[0].' '.$word[1]);
                ?>
            </a>
        </li>
        <li>
            <a href="profile.php" class="no-child">
                <i class="ion-android-person"></i> <?php echo $lang->lang('Profil', $conn); ?>
            </a>
        </li>
        <li>
            <a href="wallet.php" class="no-child">
                <i class="ion-cash"></i> <?php echo $lang->lang('Dompet', $conn); ?>
            </a>
        </li>
    <?php } else { ?>
        <li>
            <a href="profile.php" class="no-child">
                <i class="ion-android-person"></i> <?php echo $lang->lang('Profil', $conn); ?>
            </a>
        </li>
        <li>
            <a href="wallet.php" class="no-child">
                <i class="ion-cash"></i> <?php echo $lang->lang('Dompet', $conn); ?>
            </a>
        </li>

    <?php } ?>

    <li>
        <a href="logout.php" class="no-child">
            <i class="ion-log-out"></i> <?php echo $lang->lang('Keluar'); ?>
        </a>
    </li>
</ul>
<!-- End of Sidebars -->

<!-- Page Content -->
<div id="content" class="page">

    <!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <div class="open-left" id="open-left" data-activates="slide-out-left">
            <i class="ion-android-menu"></i>
        </div>

        <span class="title">Baronang</span>

        <div class="open-right">
            <a href="setting.php">
                <i class="ion-gear-a"></i>
            </a>
        </div>

        <div class="open-right owntooltip">
            <a href="approval.php">
                <i class="ion-ios-bell"></i>
                <?php if($m > 0){ ?>
                    <span><?php echo $m; ?></span>
                <?php } ?>
            </a>
        </div>

        <div class="open-right owntooltip">
            <?php
            $date = date('Y-m-d H:i:s');
            if(isset($_SESSION['KID'])){
                //listtrx
                $tanggal = date('Y-m-d');
                $a = "select count(*) from [dbo].[UserTokenTransaksi] where KodeBaronangPay in ('$_SESSION[UserID]','$_SESSION[MemberID]') and StatusTrx = 0 and KID = '$_SESSION[KID]'";
                $b = sqlsrv_query($conn, $a);
                $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                $j = $c[0];

                //approval
                $x = "select* from [dbo].[UserMemberKoperasi] where KID = '$_SESSION[KID]' and UserID = '$_SESSION[UserID]'";
                $y = sqlsrv_query($conn, $x);
                $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                if($z != null){
                    $kode = $z[2];
                }
                else{
                    $kode = $z[1];
                }
                $a = "select count(*) from [KoneksiKoperasiBaronang].[dbo].[RequestPage] where KodeUser = '$kode' and Status = '0' and TanggalExpired >= '$date' or KodeUser = '$_SESSION[UserID]' and Status = '0' and TanggalExpired >= '$date'";
                ?>
                <a href="listtrx.php">
                    <i class="ion-android-apps"></i>
                    <span id="txttip">
                        <?php if($j > 0){ ?>
                            <b><?php echo $j; ?></b>
                        <?php } ?>
                     </span>
                </a>
                <?php
            }
            else {
                $a = "select count(*) from [KoneksiKoperasiBaronang].[dbo].[RequestPage] where KodeUser = '$_SESSION[UserID]' and Status = '0' and TanggalExpired >= '$date' and Link in('identity_cs.php','identity_cas.php')";
            }

            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
            $m = $c[0];
            ?>
        </div>

    </div>
