<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <?php
                if(isset($_GET['app'])){
                    $link = 'app-request.php';
                }
                else{
                    $link = 'procprofile.php';
                }
            ?>
            <form id="procpin" action="<?php echo $link; ?>" method="POST">
                <?php if(isset($_GET['app'])){ ?>
                    <input type="hidden" name="id" value="<?php echo $_GET['app']; ?>" readonly>
                <?php } ?>

                <?php if(isset($_POST['name'])){ ?>
                    <input type="hidden" name="name" value="<?php echo $_POST['name']; ?>" readonly>
                <?php } ?>
                <?php if(isset($_POST['phone'])){ ?>
                    <input type="hidden" name="phone" value="<?php echo $_POST['phone']; ?>" readonly>
                <?php } ?>
                <?php if(isset($_POST['address'])){ ?>
                    <input type="hidden" name="address" value="<?php echo $_POST['address']; ?>" readonly>
                <?php } ?>

                <div class="m-t-10">
                    <h3 class="uppercase">Masukan PIN anda</h3>
                </div>
                <div class="">
                    <input id="pin" name="pin" type="password" class="form-control text-center" style="font-size: 2em;background: white;" readonly>
                </div>
                <div class="row">
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n1" class="btn btn-lg primary-color" style="border-radius: 40px;">1</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n2" class="btn btn-lg primary-color" style="border-radius: 40px;">2</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n3" class="btn btn-lg primary-color" style="border-radius: 40px;">3</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n4" class="btn btn-lg primary-color" style="border-radius: 40px;">4</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n5" class="btn btn-lg primary-color" style="border-radius: 40px;">5</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n6" class="btn btn-lg primary-color" style="border-radius: 40px;">6</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n7" class="btn btn-lg primary-color" style="border-radius: 40px;">7</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n8" class="btn btn-lg primary-color" style="border-radius: 40px;">8</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n9" class="btn btn-lg primary-color" style="border-radius: 40px;">9</button></div>
                    <div class="col s4" style="padding: 20px;"></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="n0" class="btn btn-lg primary-color" style="border-radius: 40px;">0</button></div>
                    <div class="col s4" style="padding: 20px;"><button type="button" id="nb" class="btn btn-lg primary-color" style="border-radius: 40px;"><i class="ion-android-arrow-back"></i></button></div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        function cekDigit(){
            var a = $('#pin').val();
            if(a.length == 6){
                $('#procpin').submit();
            }

            if(a.length > 6){
                $('#pin').val($('#pin').val().slice(0, -1));
            }
        }

        $('#n1').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '1');

            cekDigit();
        });
        $('#n2').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '2');

            cekDigit();
        });
        $('#n3').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '3');

            cekDigit();
        });
        $('#n4').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '4');

            cekDigit();
        });
        $('#n5').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '5');

            cekDigit();
        });
        $('#n6').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '6');

            cekDigit();
        });
        $('#n7').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '7');

            cekDigit();
        });
        $('#n8').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '8');

            cekDigit();
        });
        $('#n9').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '9');

            cekDigit();
        });
        $('#n0').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '0');

            cekDigit();
        });
        $('#nb').click(function(){
            $('#pin').val($('#pin').val().slice(0, -1));
        });
    </script>

<?php require('footer_new.php');?>