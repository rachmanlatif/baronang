<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_SESSION['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}

$_SESSION['url'] = 'report.php?type=bas&id='.$_SESSION['acc'];
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <h3 class="uppercase"><?php echo $lang->lang('Bayar Simpanan', $conn); ?></h3> <br>

            <div class="form-inputs">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="">

                        <div class="input-field">
                            <?php echo $lang->lang('Deposit Account', $conn); ?><br>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $acc = '';
                                    $name = '';
                                    $amount = 0;
                                    //pokok
                                    $a = "exec dbo.BasicSavingPokokSearchBilling '$_SESSION[KID]','$_SESSION[acc]'";
                                    $b = sqlsrv_query($conn, $a);
                                    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                                    if($c == null){
                                        //wajib
                                        $aa = "exec dbo.BasicSavingWajibSearchBilling '$_SESSION[KID]','$_SESSION[acc]'";
                                        $bb = sqlsrv_query($conn, $aa);
                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                        if($cc == null){
                                            //sukarela
                                            $aaa = "exec dbo.BasicSavingSukarelaSearchBilling '$_SESSION[KID]','$_SESSION[acc]'";
                                            $bbb = sqlsrv_query($conn, $aaa);
                                            $ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC);
                                            if($ccc == null){
                                                echo "<script language='javascript'>document.location='balance.php';</script>";
                                            }
                                            else{
                                                $acc = $_SESSION['acc'];
                                                $name = 'Simpanan Sukarela';
                                                $amount = 0;
                                            }
                                        }
                                        else{
                                            $acc = $_SESSION['acc'];
                                            $name = 'Simpanan Wajib';
                                            $amount = $c[5];
                                        }
                                    }
                                    else{
                                        $acc = $_SESSION['acc'];
                                        $name = 'Simpanan Pokok';
                                        $amount = $c[3];
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="acc" value="<?php echo $acc; ?>" readonly>
                                            <?php echo $acc.' - '.$name; ?>
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="0" readonly>
                            </div>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah', $conn); ?> <br>
                            <input type="text" name="amount" class="validate price" id="amount" value="<?php echo number_format($amount); ?>">
                        </div>

                        <div class="m-t-30">
                            <button type="submit" class="btn btn-block primary-color"><?php echo $lang->lang('Bayar', $conn); ?></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $('#rpo').click(function(){
        $('#apo').removeClass('hide');
        $('#apo').prop('disabled', false);
        $('#awa').addClass('hide');
        $('#awa').prop('disabled', true);
        $('#asu').addClass('hide');
        $('#asu').prop('disabled', true);
    });

    $('#rwa').click(function(){
        $('#awa').removeClass('hide');
        $('#awa').prop('disabled', false);
        $('#apo').addClass('hide');
        $('#apo').prop('disabled', true);
        $('#asu').addClass('hide');
        $('#asu').prop('disabled', true);
    });

    $('#rsu').click(function(){
        $('#asu').removeClass('hide');
        $('#asu').prop('disabled', false);
        $('#apo').addClass('hide');
        $('#apo').prop('disabled', true);
        $('#awa').addClass('hide');
        $('#awa').prop('disabled', true);
    });
</script>

<?php require('footer_new.php');?>