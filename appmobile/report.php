<?php require('header_new.php');?>
<?php require('toolbar.php');?>

<?php
if(!isset($_GET['type']) or !isset($_GET['id'])){
    echo "<script language='javascript'>System.showToast('Gagal memuat halaman');document.location='balance.php';</script>";
}

$_SESSION['type'] = $_GET['type'];
$_SESSION['acc'] = $_GET['id'];

$_SESSION['url'] = 'balance.php';
?>

<div class="animated fadeinup delay-1" style="margin-top: 50px;">
    <div class="page-content">

    <div class="col s12">
        <div class="box box-solid">
            <div class="box-body">
                <?php
                $acc = '';
                $tab = '';
                $balance = 0;
                if($_GET['type'] == 'lim'){
                    $x = "exec dbo.ListLimitBelanjaBalance '$_SESSION[KID]','$_GET[id]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    if($z != null){
                        $acc = $z[3];
                        $tab = 'Limit Belanja';
                        $balance = number_format($z[4]);
                    }
                    else{
                        echo "<script language='javascript'>document.location='balance.php';</script>";
                    }
                }
                else if($_GET['type'] == 'bas'){
                    $x = "exec dbo.ListBasicSavingBalance '$_SESSION[KID]','$_GET[id]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    if($z != null){
                        $acc = $z[5];
                        $tab = $z[4];
                        $balance = number_format($z[7]);
                    }
                    else{
                        echo "<script language='javascript'>document.location='balance.php';</script>";
                    }
                }
                else if($_GET['type'] == 'reg'){
                    $x = "exec dbo.RegularSavingAccSearchTransfer '$_SESSION[KID]','$_GET[id]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    if($z != null){
                        $acc = $z[2];
                        $tab = $z[4];
                        $balance = number_format($z[5]);
                    }
                    else{
                        echo "<script language='javascript'>document.location='balance.php';</script>";
                    }
                }
                else if($_GET['type'] == 'time'){
                    $x = "exec dbo.ListTimeDepositBalance '$_SESSION[KID]','$_GET[id]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    if($z != null){
                        $acc = $z[7];
                        $tab = $z[9];
                        $balance = number_format($z[14]);
                    }
                    else{
                        echo "<script language='javascript'>document.location='balance.php';</script>";
                    }
                }
                else if($_GET['type'] == 'loan'){
                    $x = "exec dbo.LoanApplicationReleaseSearch '$_SESSION[KID]','$_GET[id]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    if($z != null){
                        $xx = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$z[4]'";
                        $yy = sqlsrv_query($conn, $xx);
                        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                        if($zz != null){
                            $tab = $zz[1];
                        }

                        $acc = $z[1];
                        //cek yang sudah bayar
                        $aa = "exec dbo.LoanPaidAll '$_SESSION[KID]','$z[1]'";
                        $bb = sqlsrv_query($conn, $aa);
                        $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
                        if($cc != null){
                            $sisa = $z[5] - ($cc[1]+$cc[2]);
                            $balance = number_format($sisa);
                        }
                        else{
                            $balance = number_format($z[5]);
                        }
                    }
                    else{
                        echo "<script language='javascript'>document.location='balance.php';</script>";
                    }
                }
                else if($_GET['type'] == 'emon'){
                    $x = "exec dbo.ListemoneyAccBalance '$_SESSION[KID]','$_GET[id]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    if($z != null){
                        $acc = $z[0];
                        $tab = $z[3];
                        $balance = number_format($z[4]);

                        //card
                        $cardno = '';
                        $xx = "exec dbo.ListemoneyCard '$_SESSION[KID]','$acc'";
                        $yy = sqlsrv_query($conn, $xx);
                        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                        if($zz != null){
                            $cardno = $zz[4];
                        }
                    }
                    else{
                        echo "<script language='javascript'>document.location='balance.php';</script>";
                    }
                }
                else if($_GET['type'] == 'bill'){
                    $x = "exec dbo.ListBillSearch '$_SESSION[KID]','$_GET[id]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    if($z != null){
                        $acc = $z[0];
                        $tab = $z[4];
                        $totalBill = $z[6]; //amount+admin+denda
                        $balance = number_format($totalBill);
                    }
                    else{
                        echo "<script language='javascript'>document.location='balance.php';</script>";
                    }
                }
                else if($_GET['type'] == 'edc'){
                    $x = "select* from Gateway.dbo.EDCList where SerialNumber = '$_GET[id]' and KID = '$_SESSION[KID]' and MemberID = '$_SESSION[MemberID]' and UserIDBaronang = '$_SESSION[UserID]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                    if($z != null){
                        $acc = 'Serial Number<br><b>'.$z[0].'</b>';

                        $a = "exec dbo.RegularSavingAccSearchTransfer '$_SESSION[KID]','$z[2]'";
                        $b = sqlsrv_query($conn, $a);
                        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                        if($c != null){
                            $tab = $c[2].' '.$c[4];
                            $balance = number_format($c[5]);
                        }
                        else{
                            $tab = $z[2];
                            $balance = '';
                        }
                    }
                    else{
                        echo "<script language='javascript'>document.location='balance.php';</script>";
                    }
                }
                else {
                    echo "<script language='javascript'>document.location='balance.php';</script>";
                }

                include('qrcode/qrlib.php');
                $errorCorrectionLevel = 'H';
                $matrixPointSize = 5;
                $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
                $PNG_WEB_DIR = 'temp/';
                $code = $_GET['id'];

                $filename = $PNG_WEB_DIR.'qr'.md5($code.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                if (!file_exists($filename)){
                    QRcode::png($code, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                }

                ?>

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td><?php echo $acc; ?></td>
                            <td rowspan="3">
                                <div class="right text-center">
                                    <a class="modal-trigger" href="#myBarcode"><img src="<?php echo $filename; ?>"></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo $tab; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $balance; ?></td>
                        </tr>
                        <?php if($_GET['type'] == 'emon'){ ?>
                            <tr>
                                <th colspan="2">
                                    <?php
                                    if($cardno <> ''){
                                        echo 'No. Kartu: '.$cardno;
                                    }
                                    else{
                                        echo 'Kartu belum terhubung dengan akun';
                                    }
                                    ?>
                                </th>
                            </tr>
                        <?php } ?>
                    </table>
                </div>

                <br>
                <div class="row">
                <?php if($_GET['type'] == 'lim'){ ?>

                <?php } ?>
                <?php if($_GET['type'] == 'bas'){ ?>
                    <div class="col s12">
                        <a href="dbasic_saving.php?acc=<?php echo $_GET['id']; ?>"><button type="button" id="btn-dbas" class="btn primary-color width-100"><?php echo $lang->lang('Bayar Simpanan', $conn); ?></button></a>
                    </div>
                <?php } ?>
                <?php if($_GET['type'] == 'reg'){ ?>
                    <div class="col s12 m-t-10">
                        <a href="dregular_saving.php?acc=<?php echo $_GET['id']; ?>"><button type="button" class="btn primary-color width-100"><?php echo $lang->lang('Setoran', $conn); ?></button></a>
                    </div>
                    <div class="col s12 m-t-10">
                        <button type="button" class="btn primary-color width-100" onclick="callScanner()"><?php echo $lang->lang('Transfer', $conn); ?></button>
                    </div>
                    <div class="col s12 m-t-10">
                        <a href="wregular_saving.php?acc=<?php echo $_GET['id']; ?>"><button type="button" class="btn primary-color width-100"><?php echo $lang->lang('Penarikan', $conn); ?></button></a>
                    </div>
                <?php } ?>
                <?php if($_GET['type'] == 'time'){ ?>

                <?php } ?>
                <?php if($_GET['type'] == 'loan'){ ?>
                    <div class="col s6">
                        <a href="dloan.php?acc=<?php echo $_GET['id']; ?>"><button type="button" class="btn primary-color"><?php echo $lang->lang('Bayar Pinjaman', $conn); ?></button></a>
                    </div>
                    <div class="col s6">
                        <a href="oloansimulasi.php?acc=<?php echo $_GET['id']; ?>"><button type="button" class="btn primary-color"><?php echo $lang->lang('Simulasi Cicilan', $conn); ?></button></a>
                    </div>
                <?php } ?>
                <?php if($_GET['type'] == 'emon'){ ?>
                    <div class="col s6">
                        <a href="topemoney.php?acc=<?php echo $_GET['id']; ?>"><button type="button" class="btn primary-color"><?php echo $lang->lang('Top Up', $conn); ?></button></a>
                    </div>
                    <?php if($_GET['type'] == 'emon' and $cardno <> ''){ ?>
                        <div class="col s6">
                            <a href="blockemoney.php?acc=<?php echo $_GET['id']; ?>"><button type="button" class="btn primary-color"><?php echo $lang->lang('Blokir Kartu', $conn); ?></button></a>
                        </div>
                  <?php } ?>
                <?php } ?>
                <?php if($_GET['type'] == 'bill'){ ?>
                    <div class="col s12">
                        <a href="paybill.php?acc=<?php echo $_GET['id']; ?>"><button type="button" class="btn primary-color"><?php echo $lang->lang('Bayar', $conn); ?></button></a>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col s12">
        <div class="">
            <div class="table-responsive">
                <table class="table">
                    <tbody id="result">

                    </tbody>
                </table>
            </div>

                <div class="box-footer" id="load" class="hide">
                    <div id="loading" class="text-center"></div>
                    <button type="button" id="btn-load" class="btn btn-block primary-color"><?php echo $lang->lang('Lihat Lebih', $conn); ?></button>
                </div>
                <input type="hidden" id="halaman" value="1" readonly>

                <div class="input-field">
                    <div class="col s8">
                        <input type="text" id="date" class="validate daterange-btn" readonly>
                    </div>
                    <div class="col s4">
                        <button type="button" id="btn-select" class="btn btn-block primary-color"><?php echo $lang->lang('Pilih', $conn); ?></button>
                    </div>
                </div>

        </div>
    </div>

    </div>
</div>

<!-- Modal -->
<div class="modal" id="myBarcode">
    <div class="modal-content">
        <center>
            <img src="<?php echo $filename; ?>" style="width: 60%;" class="modal-close">
        </center>
        <table class="table text-center">
            <tr>
                <th>
                    <span class="lead">
                        <?php echo $acc; ?><br>
                        <?php echo $tab; ?><br><br>
                    </span>
                    <b>
                        <?php
                        if($_GET['type'] == 'emon'){
                            echo $cardno.'<br>';
                        }
                        ?>
                    </b>
                    <?php echo $balance; ?>
                </th>
            </tr>
        </table>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn primary-color modal-close"><i class="ion-android-close"></i> Tutup</button>
    </div>
</div>

    <script type="text/javascript">
        function callScanner() {
            Intent.openActivity("QRActivity","tregular_saving.php?acc=<?php echo $_GET['id']; ?>");
        }

        $( document ).ready(function() {
            $('#btn-select').click();
        });

        $('#btn-load').click(function(){
            $('#btn-select').click();
        });

        $('#btn-select').click(function(){
            var date = $('#date').val();
            var halaman = $('#halaman').val();

            var perpage = 5;
            var batas = perpage;
            if(parseInt(halaman) > 1){
                var batas = (parseInt(perpage) * parseInt(halaman));
            }

            $.ajax({
                url : "ajax_report.php",
                dataType : 'json',
                type : 'POST',
                data: { date: date, type: '<?php echo $_GET['type']; ?>', id: '<?php echo $_GET['id']; ?>', batas: batas, halaman: halaman},
                beforeSend: function(){
                    $('#loading').html('<i class="fa fa-rotate-right fa-spin">');
                },
                success : function(responseJson) {
                    $("#load").removeClass('hide');
                    $("#result").html(responseJson.content);
                    $("#halaman").val(responseJson.halaman);
                },
                error : function(){
                    System.showToast('Silahkan coba lagi.');
                },
                complete: function(){
                    $('#loading').html('');
                }
            });
        });
    </script>

<?php require('footer_new.php');?>
