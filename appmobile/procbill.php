<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                </div>
            </div>
        </div>
    </div>

<?php
require_once 'lib/googleLib/GoogleAuthenticator.php';

if($_POST['pin'] == '' || $_POST['acc'] == '' || $_POST['regacc'] == '' || $_POST['amount'] == '' || $_POST['amount'] <= 0){
    echo "<script language='javascript'>document.location='paybill.php';</script>";
}
else{
    $acc = $_POST['acc'];
    $regacc = $_POST['regacc'];
    $amount = $_POST['amount'];

    $pin = md5($_POST['pin']);

    $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$_SESSION[UserID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        if ($pin == $c[12])
        {
            $xx = "exec dbo.ListBillSearch '$_SESSION[KID]','$acc'";
            $yy = sqlsrv_query($conn, $xx);
            $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
            if($zz != null){
                $jumlah = $zz[6]; //amount+admin+denda

                $x = "exec [dbo].[RegularSavingAccSearch] '$_SESSION[KID]','$_SESSION[MemberID]','$regacc'";
                $y = sqlsrv_query($conn, $x);
                $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                if($z != null){
                    $amacc = str_replace(',','',number_format($z[5],0));
                    $nominal = str_replace(',','',number_format($amount,0));

                    if($nominal == $jumlah){
                        if($amacc > $nominal){
                            $sql = "exec [dbo].[ProsesPaymentBilling] '$_SESSION[KID]','$acc','$regacc','$amount','$_SESSION[UserID]'";
                            $exec = sqlsrv_query($conn, $sql);
                            if($exec){
                                messageAlert('Pembayaran billing berhasil dilakukan');
                                echo "<script language='javascript'>document.location='notif.php';</script>";
                            }
                            else{
                                echo "<script>System.showToast('Gagal melakukan pembayaran');document.location='paybill.php';</script>";
                            }
                        }
                        else{
                            echo "<script language='javascript'>System.showToast('Saldo tidak mencukupi. Transaksi tidak dapat dilakukan');document.location='paybill.php';</script>";
                        }
                    }
                    else{
                        echo "<script language='javascript'>System.showToast('Jumlah bayar harus sesuai dengan jumlah tagihan. Transaksi tidak dapat dilakukan');document.location='paybill.php';</script>";
                    }
                }
                else{
                    echo "<script>System.showToast('Akun tabungan tidak ditemukan');document.location='paybill.php';</script>";
                }
            }
            else{
                echo "<script>System.showToast('Billing tidak ditemukan');document.location='paybill.php';</script>";
            }
        }
        else
        {
            echo "<script>System.showToast('PIN tidak benar. Transaksi tidak dapat dilakukan');document.location='paybill.php';</script>";
        }
    }
    else{
        echo "<script language='javascript'>document.location='login.php';</script>";
    }
}

?>

<?php require('footer.php');?>
