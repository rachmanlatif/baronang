<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="m-t-50">
                <div class="center" style="text-align: center;">
                    <?php
                    $upload_dir = "upload_pic"; 				// The directory for the images to be saved in
                    $upload_path = $upload_dir."/";				// The path to where the image will be saved
                    $large_image_prefix = "resize_"; 			// The prefix name to large image
                    $profile_image_prefix = "profile_"; 			// The prefix name to large image
                    $large_image_name = $large_image_prefix.$_SESSION['UserID'];     // New name of the large image (append the timestamp to the filename)
                    $profile_image_name = $profile_image_prefix.$_SESSION['UserID'];     // New name of the large image (append the timestamp to the filename)

                    //Image Locations
                    $large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];
                    $profile_image_location = $upload_path.$profile_image_name.$_SESSION['user_file_ext'];

                    if(file_exists($large_image_location)){?>
                        <form id="form-crop" action="procprofilephoto.php" method="post">
                            <center>
                                <img style="height: 100px; width: 100px;" class="img-responsive" id="image" src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>"/>
                                <br>
                                <input type="text" class="hide" id="input" name="content" readonly>
                            </center>

                            <a class="btn btn-sm btn-danger btn-flat btn-block" href="procprofilephoto.php?a=delete&t=<?php echo $large_image_name.$_SESSION['user_file_ext']; ?>">
                                <?php echo $lang->lang('Hapus', $conn); ?>
                            </a>
                            <button class="btn btn-sm btn-primary btn-flat btn-block" id="button" type="button" name="save"><?php echo $lang->lang('Simpan', $conn); ?></button>
                        </form>
                    <?php } else { ?>
                        <center>
                            <div class="widget-user-image">
                                <?php if(file_exists($profile_image_location)){?>
                                    <img style="height: 100px; width: 100px;" class="profile-user-img img-responsive img-circle" src="<?php echo $upload_path.$profile_image_name.$_SESSION['user_file_ext'];?>" alt="User Avatar">
                                <?php } else { ?>
                                    <img style="height: 100px; width: 100px;" class="profile-user-img img-responsive img-circle" src="static/images/No-Image-Icon.png" alt="User Avatar">
                                <?php } ?>
                            </div>
                        </center>

                        <div class="m-t-30 center" style="text-align: center;">
                            <form enctype="multipart/form-data" action="procprofilephoto.php" method="post">
                                <input type="file" name="image" accept="image/*">
                                <br>
                                <input type="submit" class="m-t-10 btn primary-color btn-block" name="upload" value="<?php echo $lang->lang('Unggah', $conn); ?>" />
                            </form>
                        </div>
                    <?php } ?>
                </div>
            </div>

    </div>
</div>

    <script type="text/javascript">
        (function () {
            function getRoundedCanvas(sourceCanvas) {
                var canvas = document.createElement('canvas');
                var context = canvas.getContext('2d');
                var width = sourceCanvas.width;
                var height = sourceCanvas.height;

                canvas.width = width;
                canvas.height = height;

                context.imageSmoothingEnabled = true;
                context.drawImage(sourceCanvas, 0, 0, width, height);
                context.globalCompositeOperation = 'destination-in';
                context.beginPath();
                context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
                context.fill();

                return canvas;
            }

            window.addEventListener('DOMContentLoaded', function () {
                var image = document.getElementById('image');
                var button = document.getElementById('button');
                var input = document.getElementById('input');

                var croppable = false;
                var cropper = new Cropper(image, {
                    aspectRatio: 1,
                    viewMode: 1,
                    ready: function () {
                        croppable = true;
                    }
                });

                button.onclick = function () {
                    var croppedCanvas;
                    var roundedCanvas;
                    var roundedImage;

                    if (!croppable) {
                        return;
                    }

                    // Crop
                    croppedCanvas = cropper.getCroppedCanvas();

                    // Round
                    roundedCanvas = getRoundedCanvas(croppedCanvas);

                    //Create
                    input.value = roundedCanvas.toDataURL();

                    // Save
                    $('#form-crop').submit();
                };

            });

        })();
    </script>

<?php require('footer_new.php');?>