<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_GET['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <h3 class="uppercase"><?php echo $lang->lang('Top UP e-Money', $conn); ?></h3> <br>

            <div class="form-inputs">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="">

                        <div class="">
                            <div class="">
							    <input type="hidden" name="acc" value="<?php echo $_GET['acc']; ?>" readonly>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="14" readonly>
                            </div>
                        </div>

                        <div class="">
                            <?php echo $lang->lang('Pilih Akun Tabungan', $conn); ?> <br>
                            <div class="table-responsive">
                                    <?php
                                    $a = "exec [dbo].[ListRegularSavingBal] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                        <input type="radio" name="regacc" class="minimal" id="regacc-<?php echo $c[2]; ?>" value="<?php echo $c[2]; ?>">
                                        <label for="regacc-<?php echo $c[2]; ?>">
                                            <b><?php echo $c[2]; ?></b> - <?php echo $c[4]; ?> <br>
                                            <span class="">Rp. <?php echo number_format($c[5]); ?></span>
                                        </label>
                                        <br><br>
                                    <?php } ?>
                            </div>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah Top UP', $conn); ?> <br>
                            <input type="number" name="amount" class="validate" value="0">
                        </div>

                        <div class="m-t-30">
                            <button type="submit" class="btn btn-block primary-color"><?php echo $lang->lang('Simpan', $conn); ?></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

<?php require('footer_new.php');?>
