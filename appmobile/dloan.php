<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

$amount = 0;
if(isset($_GET['amount'])){
    $amount = $_GET['amount'];
}

if(!isset($_SESSION['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}
else{
    //cek tagihan
    $i = 0;
    $p = 0;
    $t = 0;
    $a = "exec dbo.Tagihan '$_SESSION[KID]','$_SESSION[MemberID]','$_SESSION[acc]'";
    $b = sqlsrv_query($conn, $a);
    while($c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC)){
        //cek yang sudah bayar
        $ii = 0;
        $pp = 0;
        $aa = "exec dbo.LoanPaid '$_SESSION[KID]','$_SESSION[acc]','$c[0]'";
        $bb = sqlsrv_query($conn, $aa);
        $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
        if($cc != null){
            $ii=$cc[1];
            $pp=$cc[2];
        }

        $i+=$c[9]-$ii;
        $p+=$c[8]-$pp;
    }

    $t = $i+$p;

    $info = '';
    $disabled = '';
    $readonly = '';
    if($t <= 0){
        $disabled = 'disabled';
        $readonly = 'readonly';

        $info = 'Belum ada tagihan terbaru saat ini';
        echo "<script>System.showToast('Belum ada tagihan terbaru saat ini');</script>";
    }
}

$_SESSION['url'] = 'report.php?type=loan&id='.$_SESSION['acc'];

?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <h3 class="uppercase"><?php echo $lang->lang('Pembayaran Pinjaman', $conn); ?></h3> <br>

            <div class="form-inputs">
                <form id="dloan" action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">

                        <div class="input-field">
                            <?php echo $lang->lang('Akun Pinjaman', $conn); ?> <br>
                            <div class="">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $a = "exec dbo.LoanApplicationSearch '$_SESSION[KID]','$_SESSION[acc]'";
                                    $b = sqlsrv_query($conn, $a);
                                    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                                    if($c != null){
                                        $aa = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$c[4]'";
                                        $bb = sqlsrv_query($conn, $aa);
                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" class="acc" name="acc" value="<?php echo $c[1]; ?>">
                                                <?php echo $c[1].' - '.$cc[1]; ?>
                                            </td>
                                        </tr>

                                    <?php
                                    }
                                    else{
                                        echo "<script language='javascript'>document.location='balance.php';</script>";
                                    }
                                    ?>
                                </table>
                                <span class="text-danger"><?php echo $info; ?></span>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="12" readonly>
                            </div>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah Bunga', $conn); ?> <br>
                            <input type="text" class="validate price" id="interest" value="<?php echo $i; ?>" readonly>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah Pokok', $conn); ?> <br>
                            <input type="text" class="validate price" id="principal" value="<?php echo $p; ?>" readonly>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Total', $conn); ?><br>
                            <input type="text" class="validate price" id="total" value="<?php echo $t; ?>" readonly>
                        </div>

                        <hr>
                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah Pembayaran', $conn); ?> <br>
                            <input type="number" name="amount" id="amount" class="validate price" value="<?php echo $amount; ?>" <?php echo $readonly; ?>>
                        </div>

                        <div class="m-t-30">
                            <button type="submit" class="btn btn-block primary-color" <?php echo $disabled; ?>><?php echo $lang->lang('Bayar', $conn); ?></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('.btn-save').click(function(){
            var total = $('#total').val();
            var amount = $('#amount').val();

            if(parseInt(amount) > parseInt(total)){
                System.showToast('Jumlah harus lebih kecil dari '+ total);
                return false;
            }
            else{
                $('#dloan').submit();
            }
        });

        $('.acc').click(function(){
            var acc = $("input[name='acc']:checked").val();
            $("#interest").val(0);
            $("#principal").val(0);
            $("#total").val(0);

            $.ajax({
                url : "ajax_gettagihan.php",
                type : 'POST',
                dataType: 'json',
                data: { acc: acc},
                success : function(data) {
                    if(data.status == 1){
                        $("#interest").val(data.interest);
                        $("#principal").val(data.principal);
                        $("#total").val(data.total);

                        if(data.total > 0){
                            $("#btn-save").prop('disabled', false);
                            $("#amount").prop('readonly', false);
                        }
                    }
                    else{
                        System.showToast(data.message);
                        return false;
                    }
                },
                error : function(){
                    System.showToast('Coba lagi');
                }
            });
        });
    </script>
<?php require('footer_new.php');?>