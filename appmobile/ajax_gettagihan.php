<?php
session_start();
include "connect.inc";

$acc = $_POST['acc'];

$json = array(
    'status'=>0,
    'message'=>'',
    'interest'=>0,
    'principal'=>0,
    'total'=>0,
);

$i = 0;
$p = 0;
$t = 0;
$a = "exec dbo.Tagihan '$_SESSION[KID]','$_SESSION[MemberID]','$acc'";
$b = sqlsrv_query($conn, $a);
while($c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC)){
    //cek yang sudah bayar
    $ii = 0;
    $pp = 0;
    $aa = "exec dbo.LoanPaid '$_SESSION[KID]','$acc','$c[0]'";
    $bb = sqlsrv_query($conn, $aa);
    $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
    if($cc != null){
        $ii=$cc[1];
        $pp=$cc[2];
    }

    $i+=$c[9]-$ii;
    $p+=$c[8]-$pp;
}

$t = $i+$p;

if($t > 0){
    $json['status'] = 1;
    $json['interest'] = $i;
    $json['principal'] = $p;
    $json['total'] = $t;
}
else{
    $json['message'] = 'Belum ada tagihan terbaru saat ini';
}

echo json_encode($json);
?>