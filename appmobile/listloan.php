<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h3 class="uppercase"><?php echo $lang->lang('Riwayat Pengajuan Pinjaman', $conn); ?></h3> <br>

        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th><?php echo $lang->lang('Nomor', $conn); ?></th>
                        <th><?php echo $lang->lang('Produk', $conn); ?></th>
                        <th><?php echo $lang->lang('Status', $conn); ?></th>
                        <th><?php echo $lang->lang('Jumlah', $conn); ?></th>
                        <th><?php echo $lang->lang('Tanggal Kadaluarsa', $conn); ?></th>
                        <th></th>
                    </tr>
                    <?php
                    $a = "exec dbo.LoanApplicationSearchMember '$_SESSION[KID]','$_SESSION[MemberID]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        if($c[8]->format('Y-m-d H:i:s') >= date('Y-m-d H;i:s') and $c[7] == 0 or $c[7] <> 0){
                            $type = '';
                            $x = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$c[4]'";
                            $y = sqlsrv_query($conn, $x);
                            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                            if($z != null){
                                $type = $z[1];
                            }

                            $status = '';
                            if($c[7] == 0){
                                $status = 'Belum Lengkap';
                            }
                            else if($c[7] == 1){
                                $status = 'Lengkap';
                            }
                            else if($c[7] == 2){
                                $status = 'Diverifikasi';
                            }
                            else if($c[7] == 3){
                                $status = 'Menunggu Persetujuan';
                            }
                            else if($c[7] == 4){
                                $status = 'Disetujui';
                            }
                            else if($c[7] == 5){
                                $status = 'Ditolak';
                            }
                            else if($c[7] == 6){
                                $status = 'Pinjaman Dicairkan';
                            }
                            else{
                                $status = 'Undefined';
                            }

                            $due = '';
                            if($c[7] == 0){
                                $due = $c[8]->format('Y-m-d H:i:s');
                            }
                            ?>
                            <tr>
                                <td><?php echo $c[1]; ?></td>
                                <td><?php echo $type; ?></td>
                                <td><?php echo $status; ?></td>
                                <td><?php echo number_format($c[5]); ?></td>
                                <td><?php echo $due; ?></td>
                                <td>
                                    <?php if($c[7] == 0){ ?>
                                        <a href="eloan.php?id=<?php echo $c[1]; ?>"><button type="button" class="btn btn-sm btn-primary btn flat"><i class="fa fa-circle-o"></i> </button></a>
                                    <?php } ?>
                                </td>
                            </tr>
                    <?php }
                    } ?>
                </table>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>