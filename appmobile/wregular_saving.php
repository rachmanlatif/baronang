<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_SESSION['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}

$_SESSION['url'] = 'report.php?type=reg&id='.$_SESSION['acc'];

?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <h3 class="uppercase"><?php echo $lang->lang('Penarikan Tabungan', $conn); ?></h3> <br>

            <div class="form-inputs">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">

                        <div class="">
                            <?php echo $lang->lang('Akun Tabungan', $conn); ?> <br>
                            <div class="">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $a = "exec dbo.RegularSavingBalanceAcc '$_SESSION[KID]','$_SESSION[acc]'";
                                    $b = sqlsrv_query($conn, $a);
                                    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                                    if($c == null){
                                        echo "<script language='javascript'>document.location='balance.php';</script>";
                                    }
                                    else{ ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="acc" value="<?php echo $c[2]; ?>">
                                                <?php echo $c[2].' - '.$c[4]; ?>
                                            </td>
                                            <td>
                                                <?php echo 'Rp. '.$c[6]; ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="2" readonly>
                            </div>
                        </div>

                        <div class="input-field">
                            <?php echo $lang->lang('Jumlah Penarikan', $conn); ?> <br>
                            <input type="text" name="amount" class="validate price" id="amount" value="0">
                        </div>

                        <div class="m-t-30">
                            <button type="submit" class="btn btn-block primary-color"><?php echo $lang->lang('Simpan', $conn); ?></button>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>

<?php require('footer_new.php');?>