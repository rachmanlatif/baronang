<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
$x = "select* from [dbo].[UserPaymentGateway] where email = '$_SESSION[Email]'";
$y = sqlsrv_query($conn, $x);
$z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <div class="m-t-50" style="text-align: center;">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body box-profile">
                <center>
                    <?php
                    $upload_dir = "upload_pic"; 				// The directory for the images to be saved in
                    $upload_path = $upload_dir."/";				// The path to where the image will be saved
                    $large_image_prefix = "profile_"; 			// The prefix name to large image
                    $large_image_name = $large_image_prefix.$_SESSION['UserID'];     // New name of the large image (append the timestamp to the filename)

                    //Image Locations
                    $large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];

                    if(file_exists($large_image_location)){?>
                        <img style="height: 100px; width: 100px;" class="profile-user-img img-responsive img-circle" src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" alt="User Avatar">
                    <?php } else { ?>
                        <img style="height: 100px; width: 100px;" class="profile-user-img img-responsive img-circle" src="static/images/No-Image-Icon.png" alt="User Avatar">
                    <?php } ?>
                </center>
            </div>

            <div class="m-t-10">
                <a href="changephoto.php"><?php echo $lang->lang('Ubah foto profil', $conn); ?></a>
            </div>
        </div>

        <div class="m-t-50">

            <br><h3><u><?php echo $lang->lang('Info', $conn); ?></u></h3>

            <div class="form-inputs">
                <form action="pin.php" method="post">

                    <div class="input-field">
                        <b><?php echo $lang->lang('Nama', $conn); ?></b><br>
                        <input type="text" name="name" class="validate" id="tname" value="<?php echo $z[1]; ?>">
                    </div>

                    <div class="input-field">
                        <b><?php echo $lang->lang('KTP', $conn); ?></b>
                        <div class="validate bg-gray-light">
                            <?php echo $z[9]; ?>
                        </div>
                    </div><br>

                    <div class="input-field">
                        <b><?php echo $lang->lang('Telepon/HP', $conn); ?></b><br>
                        <input type="number" name="phone" class="validate" id="tphone" value="<?php echo $z[3]; ?>">
                    </div><br>

                    <div class="input-field">
                        <b><?php echo $lang->lang('Email', $conn); ?></b>
                        <div class="validate bg-gray-light">
                            <?php echo $z[2]; ?>
                        </div>
                    </div><br>

                    <div class="input-field">
                        <b><?php echo $lang->lang('Alamat', $conn); ?></b><br>
                        <input type="text" name="address" class="validate" id="taddress" value="<?php echo $z[4]; ?>">
                    </div><br>

                    <div class="input-field">
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo $lang->lang('Simpan', $conn); ?></button>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>

<?php require('footer_new.php');?>