<?php require('header_new.php');?>

<?php
$_SESSION['url'] = 'login.php';
?>

<div id="content" class="primary-color login">
    <div class="login-form animated delay-2 primary-color" style="color: #fff; margin-top: 0px; margin-right: -7px; margin-left: -7px; margin-bottom: 10px;">

        <div id="toolbar" class="primary-color">
            <a class="open-left" href="javascript:history.back()">
                <i class="ion-android-arrow-back"></i>
            </a>
            <span class="title">Baronang</span>
        </div>

        <div class="login-form animated fadeinup delay-2 primary-color" style="color: #fff; margin-top: 0px; margin-right: -7px; margin-left: -7px; margin-bottom: 10px;">
            <h2 class="uppercase" style="color: #fff;"><?php echo $lang->lang('Daftar', $conn); ?></h2> <br>

                <form action="procregister.php" method="post">

                    <div class="input-field">
                        <b><?php echo $lang->lang('Tipe Account', $conn); ?></b><br>

                        <input type="radio" name="tipeacc" id="tipeacc0" value="0">
                        <label for="tipeacc0"><?php echo $lang->lang('Personal', $conn); ?></label>

                        <input type="radio" name="tipeacc" id="tipeacc1" value="1">
                        <label for="tipeacc1"><?php echo $lang->lang('Corporate', $conn); ?></label>
                    </div>

                    <div class="input-field">
                        <label for="email"><?php echo $lang->lang('Email', $conn); ?></label>
                        <input type="email" name="email" id="email" class="validate">
                        <!--<span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>-->
                    </div>

                    <div class="input-field">
                        <label for="ktp"><?php echo $lang->lang('KTP', $conn); ?></label>
                        <input type="number" name="ktp" id="ktp" class="validate">
                        <!--<span class="fa fa-address-card-o form-control-feedback" aria-hidden="true"></span>-->
                    </div>

                    <div class="input-field">
                        <label for="name"><?php echo $lang->lang('Nama', $conn); ?></label>
                        <input type="text" name="name" id="name" class="validate">
                        <!--<span class="fa fa-file-o form-control-feedback" aria-hidden="true"></span>-->
                    </div>

                    <div class="input-field">
                        <label for="telp"><?php echo $lang->lang('Telepon/HP', $conn); ?></label>
                        <input type="number" name="telp" id="telp" class="validate">
                        <!--<span class="fa fa-phone form-control-feedback" aria-hidden="true"></span>-->
                    </div>

                    <div class="input-field">
                        <label for="address"><?php echo $lang->lang('Alamat', $conn); ?></label>
                        <input type="text" name="address" id="address" class="validate">
                        <!--<span class="fa fa-globe form-control-feedback" aria-hidden="true"></span>-->
                    </div>

                    <br><b><?php echo $lang->lang('Keamanan Akun', $conn); ?></b><br>

                    <div class="input-field">
                        <label for="password"><?php echo $lang->lang('Kata Sandi', $conn); ?></label>
                        <input type="password" id="password" name="password" class="validate">
                        <!--<span class="fa fa-eye form-control-feedback eye1" aria-hidden="true"></span>
                        <span class="fa fa-eye-slash form-control-feedback eye11 hide" aria-hidden="true"></span>-->
                    </div>

                    <div class="input-field">
                        <label for="repassword"><?php echo $lang->lang('Ulangi Password', $conn); ?></label>
                        <input type="password" id="repassword" name="repassword" class="validate" onkeyup="validatePassword()">
                        <!--<span class="fa fa-eye form-control-feedback eye2" aria-hidden="true"></span>
                        <span class="fa fa-eye-slash form-control-feedback eye22 hide" aria-hidden="true"></span>-->
                        <?php echo $lang->lang('Masukan Ulang Password', $conn); ?>
                    </div>

                    <div class="input-field">
                        <label for="pin"><?php echo $lang->lang('PIN', $conn); ?></label>
                        <input type="number" id="pin" name="pin" class="validate">
                        <!--<span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>-->
                        <?php echo $lang->lang('Harus 6 angka', $conn); ?>
                    </div>

                    <div class="input-field">
                        <label for="repin"><?php echo $lang->lang('Ulangi PIN', $conn); ?></label>
                        <input type="number" id="repin" name="repin" class="validate" onkeyup="validatePin()">
                        <!--<span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>-->
                        <?php echo $lang->lang('Masukan Ulang PIN', $conn); ?>
                    </div>

                    <div style="margin-top: 30px;">
                        <button type="submit" class="waves-effect waves-light width-100 m-b-20 animated bouncein delay-4" style="background: #00c0ef; border-radius: 50px; color: #fff; padding: 15px; border: none;"><?php echo $lang->lang('Daftar', $conn); ?></button>
                    </div>

                    <div class="input-field" style="margin-top: 10px;">
                        <?php echo $lang->lang('Dengan mendaftar', $conn); ?>, <?php echo $lang->lang('Saya menyetujui', $conn); ?><br>
                        <a id="btn-eula"><?php echo $lang->lang('Syarat ketentuan', $conn); ?></a> <?php echo $lang->lang('Serta', $conn); ?> <a id="btn-privacy"><?php echo $lang->lang('Kebijakan privasi', $conn); ?></a>
                    </div>

            </form>

            <div style="margin-top: 10px;">
                <button type="button" id="btn-login" class="waves-effect waves-light width-100 m-b-20 animated bouncein delay-4" style="background: #fff; border-radius: 50px; color: #000; padding: 15px; border: none;"><?php echo $lang->lang('Masuk', $conn); ?></button>
            </div>
        </div>

    </div>
</div>

    <script type="text/javascript">
        $('#btn-login').click(function(){
            Intent.openActivity('LoginActivity','login.php');
        });

        $('#btn-eula').click(function(){
            Intent.openActivity('LoginActivity','eula.php');
        });

        $('#btn-privacy').click(function(){
            Intent.openActivity('LoginActivity','privacy-pl.php');
        });

        $('.eye1').click(function(){
            $("#password").prop('type','text');
            $('.eye1').hide();
            $('.eye11').removeClass('hide');
        });

        $('.eye11').click(function(){
            $("#password").prop('type','password');
            $('.eye11').addClass('hide');
            $('.eye1').show();
        });

        $('.eye2').click(function(){
            $("#repassword").prop('type','text');
            $('.eye2').hide();
            $('.eye22').removeClass('hide');
        });

        $('.eye22').click(function(){
            $("#repassword").prop('type','password');
            $('.eye22').addClass('hide');
            $('.eye2').show();
        });

    </script>

<?php require('footer_new.php');?>