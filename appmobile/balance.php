<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if(isset($_SESSION['KID'])){
            unset($_SESSION['type']);
            unset($_SESSION['acc']);

            $_SESSION['url'] = 'balance.php';

            $x = "select* from [dbo].[UserMemberKoperasi] where UserID = '$_SESSION[UserID]' and KID = '$_SESSION[KID]'";
            $y = sqlsrv_query($conn, $x);
            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
            ?>

                <div class="" style="margin-top: 30px;">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="">

                        <div class="" style="text-align: center;">
                            <img class="img-responsive" style="border-radius: 10px;" src="<?php echo $_SESSION['CardImage']; ?>" data-toggle="modal" data-target="#myCard">
                            <?php
                            //set it to writable location, a place for temp generated PNG files
                            $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
                            $PNG_WEB_DIR = 'temp/';

                            $code = $_SESSION['UserID'];
                            $errorCorrectionLevel = 'H';
                            $matrixPointSize = 5;
                            $filename = $PNG_TEMP_DIR.'qr'.md5($code.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                            ?>
                            <img src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" style="width: 15%;top: 38%;right: 8%;position: absolute;">
                            <span class="lead" style="position: absolute;top: 20%;right: 8%;color: <?php echo $_SESSION['CardFontColor']; ?>;">
                                <b>
                                    <?php
                                    $word = explode(' ', $_SESSION['NickName']);
                                    echo ucwords($word[0].' '.$word[1]);
                                    ?>
                                </b>
                            </span>
                            <span class="lead" style="position: absolute;top: 38%;left: 8%;color: <?php echo $_SESSION['CardFontColor']; ?>;"><?php echo $_SESSION['NamaUser']; ?></span>
                            <span class="lead" style="position: absolute;top: 43%;left: 8%;color: <?php echo $_SESSION['CardFontColor']; ?>;"><?php echo $_SESSION['KID'].' '.$_SESSION['MemberID']; ?></span>
                            <?php if($_SESSION['Logo'] != ''){ ?>
                                <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 20%;left: 8%;position: absolute;">
                            <?php } ?>
                        </div>

                    </div>
                </div>

                <br>
                <span><?php echo $lang->lang('Untuk melakukan transaksi, pilih akun dibawah ini', $conn); ?></span>

                <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                    <?php if ($_SESSION['tipe'] ==0) { ?>
                    <li>
                        <div class="collapsible-header">
                            <i class="ion-android-add right"></i>
                            <span style="color: #0a3177; font-weight: bold;"><?php echo $lang->lang('Limit Belanja', $conn); ?></span>
                        </div>
                        <div class="collapsible-body">
                            <table class="table">
                                <?php
                                $a = "exec [dbo].[ListLimitBelanjaBal] '$z[0]','$z[2]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><a href="report.php?type=lim&id=<?php echo $c[3]; ?>"><b><?php echo $c[3]; ?></b><span class="pull-right">Rp. <?php echo number_format($c[4]); ?></span></a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header">
                            <i class="ion-android-add right"></i>
                            <span style="color: #0a3177; font-weight: bold;""><?php echo $lang->lang('Simpanan', $conn); ?></span>
                        </div>
                        <div class="collapsible-body">
                            <table class="table">
                                <?php
                                $a = "exec [dbo].[ListBasicSavingBal] '$z[0]','$z[2]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><a href="report.php?type=bas&id=<?php echo $c[5]; ?>"><b><?php echo $c[5]; ?></b> <?php echo $c[4]; ?><span class="pull-right">Rp. <?php echo number_format($c[7]); ?></span></a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </li>
                    <?php } ?>

                    <li>
                        <div class="collapsible-header">
                            <i class="ion-android-add right"></i>
                            <span style="color: #0a3177; font-weight: bold;""><?php echo $lang->lang('Tabungan', $conn); ?></span>
                        </div>
                        <div class="collapsible-body">
                            <table class="table">
                                <?php
                                $a = "exec [dbo].[ListRegularSavingBal] '$z[0]','$z[2]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><a href="report.php?type=reg&id=<?php echo $c[2]; ?>"><b><?php echo $c[2]; ?></b> <?php echo $c[4]; ?><span class="pull-right">Rp. <?php echo number_format($c[5]); ?></span></a></td>
                                    </tr>
                                <?php } ?>
                            </table>

                            <div class="m-t-10 center" style="margin-bottom: 10px;">
                                <a href="oregular_saving.php"><button type="button" class="btn primary-color"><?php echo $lang->lang('Buka Akun Tabungan', $conn); ?></button></a>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header">
                            <i class="ion-android-add right"></i>
                            <span style="color: #0a3177; font-weight: bold;"><?php echo $lang->lang('Deposito', $conn); ?></span>
                        </div>
                        <div class="collapsible-body">
                            <table class="table">
                                <?php
                                $a = "exec [dbo].[ListTimeDepositBal] '$z[0]','$z[2]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><a href="report.php?type=time&id=<?php echo $c[7]; ?>"><b><?php echo $c[7]; ?></b> <?php echo $c[9]; ?><span class="pull-right">Rp. <?php echo number_format($c[14]); ?></span></a></td>
                                    </tr>
                                <?php } ?>
                            </table>

                            <div class="m-t-10 center" style="margin-bottom: 10px;">
                                <a href="otime_deposit.php"><button type="button" class="btn primary-color"><?php echo $lang->lang('Buka Akun Deposito', $conn); ?></button></a>
                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header">
                            <i class="ion-android-add right"></i>
                            <span style="color: #0a3177; font-weight: bold;"><?php echo $lang->lang('Pinjaman', $conn); ?></span>
                        </div>
                        <div class="collapsible-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="listloan.php"><button type="button" class="btn primary-color btn-block"><?php echo $lang->lang('Riwayat Pengajuan Pinjaman', $conn); ?></button></a>
                                    </td>
                                </tr>
                                <?php
                                $a = "exec dbo.LoanApplicationSearchMemberRelease '$_SESSION[KID]','$_SESSION[MemberID]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    $type = '';
                                    $x = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$c[4]'";
                                    $y = sqlsrv_query($conn, $x);
                                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                                    if($z != null){
                                        $type = $z[1];
                                    }

                                    //cek yang sudah bayar
                                    $aa = "exec dbo.LoanPaidAll '$_SESSION[KID]','$c[1]'";
                                    $bb = sqlsrv_query($conn, $aa);
                                    $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
                                    if($cc != null){
                                        $sisa = $c[5] - ($cc[1]+$cc[2]);
                                    }
                                    else{
                                        $sisa = $c[5];
                                    }
                                    ?>
                                    <tr>
                                        <td><a href="report.php?type=loan&id=<?php echo $c[1]; ?>"><b><?php echo $c[1]; ?></b> <?php echo $type; ?><span class="pull-right">Rp. <?php echo number_format($sisa); ?></span></a></td>
                                    </tr>
                                <?php } ?>
                            </table>

                            <div class="m-t-10 center" style="margin-bottom: 10px;">
                                <a href="oloan.php"><button type="button" class="btn primary-color"><?php echo $lang->lang('Buka Pengajuan Pinjaman', $conn); ?></button></a>
                            </div>

                        </div>
                    </li>

                    <li>
                        <div class="collapsible-header">
                            <i class="ion-android-add right"></i>
                            <span style="color: #0a3177; font-weight: bold;"><?php echo $lang->lang('e-Money', $conn); ?></span>
                        </div>
                        <div class="collapsible-body">
                            <table class="table">
                                <?php
                                $a = "exec [dbo].[ListemoneyAcc] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><a href="report.php?type=emon&id=<?php echo $c[0]; ?>"><b><?php echo $c[0]; ?></b><span class="pull-right">Rp. <?php echo number_format($c[4]); ?></span></a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </li>

                    <!-- <li>
                        <div class="collapsible-header">
                            <i class="ion-android-add right"></i>
                            <span style="color: #0a3177; font-weight: bold;"><?php echo $lang->lang('Billing', $conn); ?></span>
                        </div>
                        <div class="collapsible-body">
                            <table class="table">
                                <?php
                                $amount = 0;
                                $admin = 0;
                                $denda = 0;
                                $totalBill = 0;
                                $a = "exec [dbo].[ListBillMember] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    $amount = $c[6];
                                    $admin = $c[7];
                                    $denda = $c[8];
                                    $totalBill = $amount;
                                    ?>
                                    <tr>
                                        <td><a href="report.php?type=bill&id=<?php echo $c[0]; ?>"><b><?php echo $c[0].' '.$c[4]; ?></b><span class="pull-right">Rp. <?php echo number_format($totalBill); ?></span></a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </li> -->

                    <?php if ($_SESSION['tipe'] == 1) { ?>
                    <li>
                        <div class="collapsible-header">
                            <i class="ion-android-add right"></i>
                            <span style="color: #0a3177; font-weight: bold;"><?php echo $lang->lang('EDC', $conn); ?></span>
                        </div>
                        <div class="collapsible-body">
                            <a class="right" href="oedc.php"><button type="button" class="btn primary-color"><?php echo $lang->lang('Buka EDC', $conn); ?></button></a>

                            <table class="table">
                                <?php
                                $a = "select* from Gateway.dbo.EDCList where KID = '$_SESSION[KID]' and MemberID = '$_SESSION[MemberID]' and UserIDBaronang = '$_SESSION[UserID]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><a href="report.php?type=edc&id=<?php echo $c[0]; ?>"><b><?php echo $c[0]; ?></b></a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </li>
                    <?php } ?>
                </ul>

            <!--
            <div class="m-t-30">
                <a href="oedc.php"><button type="button" class="btn primary-color"><?php echo $lang->lang('oedc', $conn); ?></button></a> <br>
                <a href="oemoney.php"><button type="button" class="btn primary-color"><?php echo $lang->lang('oemoney', $conn); ?></button></a> <br>
                <a href="oloansimulasi.php"><button type="button" class="btn primary-color"><?php echo $lang->lang('oloansimulasi', $conn); ?></button></a>
            </div>
			-->


        <?php
        }
        else{
            echo "<script language='javascript'>document.location='logout.php';</script>";
        }
        ?>

    </div>
</div>

<?php require('footer_new.php');?>
