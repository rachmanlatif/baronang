<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h4 style="color: #0a3177">
            <b><?php echo $lang->lang('Keamanan Akun', $conn); ?></b>
        </h4>

        <div class="single-news animated fadeinright delay-2">
            <a href="changepass.php">
                <?php echo $lang->lang('Ubah Password', $conn); ?>
            </a>
        </div>

        <div class="single-news animated fadeinright delay-2">
            <a href="changepin.php">
                <?php echo $lang->lang('Ubah PIN', $conn); ?>
            </a>
        </div>

        <h4 class="m-t-20 text-bold" style="color: #0a3177">
            <?php echo $lang->lang('Tentang Baronang', $conn); ?>
        </h4>

        <div class="single-news animated fadeinright delay-2">
            <a href="#">
                <?php echo $lang->lang('Profil', $conn); ?>
            </a>
        </div>

        <div class="single-news animated fadeinright delay-2">
            <a href="#">
                <?php echo $lang->lang('Saran', $conn); ?>
            </a>
        </div>

        <div class="single-news animated fadeinright delay-2">
            <a href="kontak.php">
                <?php echo $lang->lang('Kontak', $conn); ?>
            </a>
        </div>

        <h4 class="m-t-20" style="color: #0a3177">
            <b><?php echo $lang->lang('Bahasa', $conn); ?></b>
        </h4>

        <div class="single-news animated fadeinright delay-2">
            <?php
            $y = "select * from [dbo].[LanguageList]";
            $n = sqlsrv_query($conns, $y);
            while($m = sqlsrv_fetch_array($n, SQLSRV_FETCH_NUMERIC)){
                $disabled = '';
                if($_SESSION['Language'] == $m[0]){
                    $disabled = 'disabled';
                }
                ?>
                <a href="setting.php?lang=<?php echo $m[0]; ?>" class="btn btn-xs primary-color" <?php echo $disabled; ?>><?php echo $m[0]; ?></a>
            <?php } ?>
        </div>

        <div class="single-news animated fadeinright delay-2">
            <h6>
                <?php echo $lang->lang('Versi', $conn); ?> 1.2.0
            </h6>
        </div>

        <div class="single-news animated fadeinright delay-2">
            <button type="button" class="btn btn-block primary-color" id="btn-logout">
                <?php echo $lang->lang('Ganti akun', $conn); ?>
            </button>
        </div>

    </div>
</div>

    <script type="text/javascript">
    $('#btn-logout').click(function(){
        Intent.openActivity('LogoutActivity','logout.php');
    });
    </script>

<?php require('footer_new.php');?>
