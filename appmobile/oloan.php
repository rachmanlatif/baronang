<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>
    <div class="animated fadeinup delay-1">
        <div class="page-content">
            
            <h3 class="uppercase"><?php echo $lang->lang('Buka Pengajuan Pinjaman', $conn); ?></h3>
            
            <div class="form-inputs">
                <form action="procloan.php" id="sloan" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="">
                        
                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Produk Pinjaman', $conn); ?></label><br>
                            <div class="m-t-10 table-responsive">
                                    <?php
                                    //table KoperasiNew.LoanType
                                    $cp = '';
                                    $a = "exec dbo.LoanTypeView '$_SESSION[KID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        $itype = '';
                                        if($c[2] == '01'){
                                            $itype = 'Flat';
                                        }
                                        else if($c[2] == '02'){
                                            $itype = 'Efektif';
                                        }
                                        else if($c[2] == '03'){
                                            $itype = 'Annuitas';
                                        }

                                        $mat = '';
                                        if($c[6] == '01'){
                                            $mat = 'Bulanan';
                                        }
                                        else if($c[6] == '02'){
                                            $mat = 'Mingguan';
                                        }
                                        else if($c[6] == '03'){
                                            $mat = 'Harian';
                                        }
                                        ?>
                                        <a class="modal-trigger" href="#bs-example-modal-<?php echo $c[0]; ?>">
                                            <input type="radio" name="acc" class="acc" id="acc-<?php echo $c[0]; ?>" value="<?php echo $c[0]; ?>" >
                                            <label for="acc-<?php echo $c[0]; ?>"><?php echo $c[1]; ?></label>
                                        </a>
                                        <br>
                                        
                                    <?php } ?>

                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="10" readonly>
                                <input type="hidden" name="max" id="max" value="0" readonly>
                                <input type="hidden" id="min" value="0" readonly>

                                <input type="hidden" name="minpen" id="minpen" value="0" readonly>
                                <input type="hidden" name="mindoc" id="mindoc" value="0" readonly>

                                <input type="hidden" name="tempo" class="price" id="tempo" value="0" readonly>
                            </div>
                        </div> <br>
                        
                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Jumlah Minimum', $conn); ?></label><br>
                            <div class="m-t-10" style="color: #000;">
                                <div class="min"></div>
                            </div>
                        </div> <br>
                        
                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Jumlah Maksimum', $conn); ?></label><br>
                            <div class="m-t-10">
                                <div class="max"></div>
                            </div>
                        </div> <br>
                        
                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Sisa Batas Pinjaman', $conn); ?></label><br>
                            <div class="m-t-10">
                                <span class="last text-bold">0</span>
                                dari
                                <span class="limit">0</span>
                            </div>
                        </div> <br>

                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Lama Angsuran ', $conn); ?><span id="contracttime"></span></label>
                            <input type="text" name="contractperiod" id="contractperiod" class="validate" value="0">
                        </div>
                        
                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Jumlah', $conn); ?></label><br><br>
                            
                            <input type="number" name="amount" id="amount" class="validate" value="0" required=""><br>
                            <a class="modal-trigger" href="#bs-example-modal-simulasi"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" id="btn-simulasi">Lihat Simulasi</button></a>
                        </div> <br>
                        
                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Akun Tabungan', $conn); ?></label><br>
                            <select name="rsacc" class="browser-default m-t-10">
                                <option value=""><?php echo $lang->lang('Pilih', $conn); ?></option>
                                <?php
                                $a = "exec dbo.ListRegularSavingBal '$_SESSION[KID]','$_SESSION[MemberID]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <option value="<?php echo $c[2]; ?>"><?php echo $c[2].' - '.$c[4].' , Rp. '.$c[6]; ?></option>
                                <?php } ?>
                            </select>
                            <?php echo $lang->lang('Pinjaman akan dicairkan ke akun tabungan', $conn); ?>
                        </div> <br>
                        
                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Penjamin', $conn); ?></label><br>
                            <div class="m-t-10">
                                <div id="dpen">
                                    <?php echo $lang->lang('Pilih produk pinjaman', $conn); ?>
                                </div>
                            </div>
                        </div> <br>
                        
                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Dokumen', $conn); ?></label><br>
                            <div class="m-t-10">
                                <div id="ddoc">
                                    <?php echo $lang->lang('Pilih produk pinjaman', $conn); ?>
                                </div>
                            </div>
                        </div> <br>
                        
                        <div class="m-t-30">
                            <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo $lang->lang('Simpan', $conn); ?></button>
                        </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" id="bs-example-modal-simulasi" style="overflow: scroll;">
        <div >
            <div class="modal-content">
                <h4 class="left"><?php echo $lang->lang('Simulasi Pinjaman', $conn); ?>
                <span><i class="ion-android-close right modal-close"></i></span>
                </h4>
                <div class="modal-body-loan">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn primary-color modal-close"><?php echo $lang->lang('Tutup', $conn); ?></button>
            </div>
        </div>
    </div>

    <?php
    $a = "exec dbo.LoanTypeView '$_SESSION[KID]'";
    $b = sqlsrv_query($conn, $a);
    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)) { ?>

        <div class="modal" id="bs-example-modal-<?php echo $c[0]; ?>">
                <div class="modal-content">
                    <h4>Pinjaman - <?php echo $c[1]; ?></h4>

                    <div class="modal-body">
                        <a href="#"><?php echo $lang->lang('Tipe Bunga', $conn); ?><span class="right"><?php echo $itype; ?></span></a> <br>
                        <a href="#"><?php echo $lang->lang('Suku Bunga %', $conn); ?><span class="right"><?php echo $c[3]; ?>%</span></a> <br>
                        <a href="#"><?php echo $lang->lang('Masa Kontrak', $conn); ?><span class="right"><?php echo $c[7]; ?> month</span></a> <br>
                        <a href="#"><?php echo $lang->lang('Jumlah Minimum', $conn); ?><span class="right"><?php echo number_format($c[4]); ?></span></a> <br>
                        <a href="#"><?php echo $lang->lang('Jumlah Maximum', $conn); ?><span class="right"><?php echo number_format($c[5]); ?></span></a> <br>
                        <a href="#"><?php echo $lang->lang('Maturity', $conn); ?> <span class="right"><?php echo $mat; ?></span></a> <br>
                        <a href="#"><?php echo $lang->lang('Pokok Penalti %', $conn); ?><span class="right"><?php echo $c[8]; ?>%</span></a> <br>
                        <a href="#"><?php echo $lang->lang('Bunga Penalti %', $conn); ?><span class="right"><?php echo $c[9]; ?>%</span></a>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn primary-color right modal-close" id="btn-<?php echo $c[0]; ?>"><?php echo $lang->lang('OK', $conn); ?></button>
                </div>
            </div>
        </div>

        <script>
            $('#btn-<?php echo $c[0]; ?>').click(function () {
                $('#acc-<?php echo $c[0]; ?>').prop('checked', true);

                var acc = $("input[name='acc']:checked").val();

                $.ajax({
                    url : "ajax_doc.php",
                    type : 'POST',
                    data: { acc: acc},
                    success : function(data) {
                        $("#ddoc").html(data);
                    },
                    error : function(){
                        System.showToast('Coba lagi');
                    }
                });

                $.ajax({
                    url : "ajax_penjamin.php",
                    type : 'POST',
                    data: { acc: acc},
                    success : function(data) {
                        $("#dpen").html(data);
                    },
                    error : function(){
                        System.showToast('Coba lagi');
                    }
                });

                $.ajax({
                    url : "ajax_getloan.php",
                    type : 'POST',
                    dataType: 'json',
                    data: { acc: acc},
                    success : function(data) {
                        if(data.status == 1){
                            $("#btn-simulasi").removeClass('hide');

                            $("#min").val(data.min);
                            $("#max").val(data.last);
                            $(".max").html(data.maxloan);
                            $(".min").html(data.minloan);
                            $(".limit").html(data.limitf);
                            $(".last").html(data.lastf);

                            $("#minpen").val(data.minpen);
                            $("#mindoc").val(data.mindoc);
                            $("#tempo").val(data.tempo);
                            $('#contractperiod').val(data.contractperiod);
                            $('#contracttime').html('(Max '+data.contractperiod+'x)');
                        }
                        else{
                            System.showToast(data.message);
                            return false;
                        }
                    },
                    error : function(){
                        System.showToast('Coba lagi');
                    }
                });
            });

            $('#acc-<?php echo $c[0]; ?>').click(function(){

            });
        </script>

        <?php
    }
    ?>

    <script type="text/javascript">
        //var rows = document.getElementById("membertable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;

        $('#btn-simulasi').click(function(){
            var acc = $("input[name='acc']:checked").val();
            var amount = $('#amount').val();

            $.ajax({
                url : "ajax_getsimulasi.php",
                type : 'POST',
                data: { acc: acc, amount: amount},
                success : function(data) {
                    $(".modal-body-loan").html(data);
                },
                error : function(){
                    System.showToast('Coba lagi');
                }
            });
        });
    </script>

<?php require('footer_new.php');?>