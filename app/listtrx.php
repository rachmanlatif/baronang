<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

<div class="col-sm-12">
    <div class="box box-solid">
        <div class="box-header">
            <h3 class="box-title"><?php echo $lang->lang('Transaksi tertunda', $conn); ?></h3>
        </div>
        <div class="box-body">
            <?php echo $lang->lang('Transaksi akan dilanjutkan di kasir', $conn); ?> <?php echo $lang->lang('dengan menunjukan', $conn); ?> <b><?php echo $lang->lang('Barcode', $conn); ?></b>
            <table class="table table-bordered table-striped table-hover">
                <?php
                $arr = array();
                $no = 1;
                $tanggal = date('Y-m-d');
                $a = "select* from [dbo].[UserTokenTransaksi] where KodeBaronangPay in ('$_SESSION[UserID]','$_SESSION[MemberID]') and StatusTrx = 0 and KID = '$_SESSION[KID]'";
                $b = sqlsrv_query($conn, $a);
                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                    array_push($arr, $c[0]);

                    $x = "select top 1 * from [dbo].[ListKoperasiView] where UserID = '$_SESSION[UserID]' and KID = '$c[1]'";
                    $y = sqlsrv_query($conn, $x);
                    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);

                    if($c[5] == 0){
                        $type = 'Pembayaran Simpanan';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 1){
                        $type = 'Setoran Tabungan';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 2){
                        $type = 'Penarikan Tabungan';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 3){
                        $type = 'Pembayaran Pokok Pinjaman';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 4){
                        $type = 'Pembayaran Bunga Pinjaman';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 5){
                        $type = 'Deposito';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 6){
                        $type = 'Tutup Tabungan';
                        $amount = '-';
                    }
                    else if($c[5] == 7){
                        $type = 'Tutup Pinjaman';
                        $amount = '-';
                    }
                    else if($c[5] == 8){
                        $type = 'Tutup Deposito';
                        $amount = '-';
                    }
                    else if($c[5] == 9){
                        $type = 'Buka Tabungan';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 10){
                        $type = 'Buka Pinjaman';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 11){
                        $type = 'Buka Deposito';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 12){
                        $type = 'Pembayaran Pinjaman';
                        $amount = number_format($c[6]);
                    }
                    else if($c[5] == 13){
                        $type = 'Open e-Money';
                        $amount = number_format($c[6]);
                    }
                    else{
                        $type = '';
                        $amount = '-';
                    }
                    ?>
                    <tr>
                        <td>
                            No. Akun: <?php echo $c[3]; ?>
                            <br>
                            Jumlah: <?php echo $amount; ?>
                        </td>
                        <td><?php echo $type; ?></td>
                        <td>
                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#myModal<?php echo $no; ?>"><i class="fa fa-barcode"></i> </a>
                        </td>
                    </tr>

                    <div class="modal fade" id="myModal<?php echo $no; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document" style="margin-top: 50%;">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="text-center">
                                        <div class="barcode<?php echo $no; ?>"></div>
                                        <span class="lead"><?php echo $c[0]; ?></span>
                                        <br>
                                        <br>
                                        <a href="proccanceltrans.php?barcode=<?php echo $c[0]; ?>"><button type="button" class="btn btn-xs btn-default"><i class="fa fa-times"></i> Batalkan Transaksi</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $.ajax({
                            url : "ajax_getbarcode.php",
                            type : 'POST',
                            data: { number: '<?php echo $c[0]; ?>'},
                            success : function(data) {
                                $(".barcode<?php echo $no; ?>").html(data);
                            },
                            error : function(){
                                System.showToast('Coba lagi');
                            }
                        });
                    </script>

                    <?php $no++; } ?>

                    <?php if($arr == null){ ?>
                    <tr>
                        <td colspan="4" class="text-center">
                            <span class="lead"><?php echo $lang->lang('Kosong', $conn); ?></span>
                        </td>
                    </tr>
                    <?php } ?>
            </table>
        </div>
    </div>
</div>

<?php require('footer.php');?>
