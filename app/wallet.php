<?php require('header.php');?>

<div class="col-sm-12">
    <div class="box box-solid">
        <div class="box-body">
            <div class="col-xs-12 text-center">
                <img src="images/kartu1.png" class="img-responsive" data-toggle="modal" data-target="#myCard">
                <?php
                //QR code
                include('qrcode/qrlib.php');
                $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
                $PNG_WEB_DIR = 'temp/';

                $errorCorrectionLevel = 'H';
                $matrixPointSize = 5;
                $kode = $_SESSION['UserID'];
                $filename = $PNG_WEB_DIR.'qr'.md5($kode.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                if (!file_exists($filename)){
                    QRcode::png($kode, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                }

                //barcode
                include('barcode128/BarcodeGenerator.php');
                include('barcode128/BarcodeGeneratorPNG.php');
                include('barcode128/BarcodeGeneratorSVG.php');
                include('barcode128/BarcodeGeneratorJPG.php');
                include('barcode128/BarcodeGeneratorHTML.php');
                $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                $resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
                ?>
                <img src="<?php echo $filename; ?>" style="width: 15%;bottom: 10%;right: 8%;position: absolute;">
                <span class="lead" style="position: absolute;bottom: 22%;left: 8%;color: white;"><?php echo $_SESSION['NamaUser']; ?></span>
                <span class="lead" style="position: absolute;bottom: 12%;left: 8%;color: white;"><?php echo $_SESSION['UserID']; ?></span>
                <img src="<?php echo $resource ?>" style="position: absolute;bottom: 10%;left: 8%;width: 40%;background: white;padding: 3px;">
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myCard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="images/kartu.png" class="img-responsive" data-dismiss="modal">
                    <img class="card" src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" style="width: 30%;bottom: 6%;left: 10%;position: absolute;">
                    <div class="card" style="position: absolute;left: -14%;top: 22%;">
                        <span class="lead" style="color: white;font-size: 1.6em;"><?php echo $_SESSION['NamaUser']; ?></span>
                        <br>
                        <span class="lead" style="color: white;font-size: 1.6em;"><?php echo $_SESSION['UserID']; ?></span>
                        <br>
                        <img src="<?php echo $resource ?>" style="width: 80%;background: white;padding: 3px;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-solid">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="box-body">

            <?php
            $arr = array();
            $xx = "select* from [dbo].[ListKoperasiView] where UserID = '$_SESSION[UserID]'";
            $yy = sqlsrv_query($conn, $xx);
            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                array_push($arr, $zz[0]);

                $poi = "select* from $zz[3].[dbo].[Profil]";
                $lkj = sqlsrv_query($conn, $poi);
                $mnb = sqlsrv_fetch_array($lkj, SQLSRV_FETCH_NUMERIC);

                $cvb = "select* from $zz[3].[dbo].[CardDesign]";
                $dfg = sqlsrv_query($conn, $cvb);
                $ert = sqlsrv_fetch_array($dfg, SQLSRV_FETCH_NUMERIC);
                if($ert != null){
                    $imgcard = '../koperasi/'.$ert[0];
                    $fontcolor = $ert[1];
                }
                else{
                    $imgcard = '../koperasi/card/card.png';
                    $fontcolor = '#000000';
                }
                ?>
                <div class="col-xs-12 text-center">
                    <a href="connectkop.php?idkoperasiwallet=<?php echo $zz[0]; ?>">
                        <img class="img-responsive" style="border-radius: 10px;" src="<?php echo $imgcard; ?>" alt="Photo">
                        <?php
                        $code = $zz[0].' '.$zz[8];
                        $filename = $PNG_TEMP_DIR.'qr'.md5($code.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                        if (!file_exists($filename)){
                            QRcode::png($code, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                        }
                        ?>
                        <img src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" style="width: 15%;bottom: 10%;right: 8%;position: absolute;">
                        <span class="lead" style="position: absolute;top: 12%;right: 8%;color: <?php echo $fontcolor; ?>;">
                            <b>
                                <?php
                                $word = explode(' ', $mnb[2]);
                                echo ucwords($word[0].' '.$word[1]);
                                ?>
                            </b>
                        </span>
                        <span class="lead" style="position: absolute;bottom: 5%;left: 8%;color: <?php echo $fontcolor; ?>;"><?php echo $_SESSION['NamaUser']; ?></span>
                        <span class="lead" style="position: absolute;bottom: -4%;left: 8%;color: <?php echo $fontcolor; ?>;"><?php echo $zz[0].' '.$zz[8]; ?></span>
                        <?php if($mnb[6] != ''){ ?>
                            <img src="<?php echo '../koperasi/'.$mnb[6]; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                        <?php } ?>
                    </a>
                </div>
                <div class="col-xs-12">
                    <br>
                </div>
            <?php } ?>

            <?php if($arr == null){ ?>
                <div class="col-sm-12 text-center">
                    <span class="lead">
                        <?php echo $lang->lang('Anda harus menghubungkan akun ini ke koperasi atau pendaftaran sebagai member baru di koperasi', $conn); ?>
                    </span>
                </div>
            <?php } ?>

        </div>
    </div>

<?php require('footer.php');?>
