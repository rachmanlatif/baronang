<?php
session_start();
include "connect.inc";

$acc = $_POST['acc'];

$json = array(
    'status'=>0,
    'message'=>'',
    'minloan'=>0,
    'min'=>0,
    'maxloan'=>0,
    'last'=>0,
    'limit'=>0,
    'lastf'=>0,
    'limitf'=>0,
    'minpen'=>0,
    'mindoc'=>0,
    'tempo'=>0,
    'contractperiod'=>0,
);

$a = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$acc'";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
if($c != null){
    $json['status'] = 1;

    $aa = "select * from $_SESSION[Kop].dbo.MemberView where KID = '$_SESSION[KID]' and MemberID = '$_SESSION[MemberID]'";
    $bb = sqlsrv_query($conn, $aa);
    $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
    if($cc != null){
        $qw = "select StatusLimit from $_SESSION[Kop].dbo.GeneralSetting";
        $as = sqlsrv_query($conn, $qw);
        $zx = sqlsrv_fetch_array( $as, SQLSRV_FETCH_NUMERIC);
        if($zx != null){
            if($zx[0] == 1){
                $json['limit'] = $cc[18];
                $json['limitf'] = $cc[19];
                $json['last'] = $cc[14];
                $json['lastf'] = $cc[15];
            }
            else{
                $json['limit'] = $cc[18]+$cc[16];
                $json['limitf'] = number_format($cc[18]+$cc[16]);
                $json['last'] = $cc[20];
                $json['lastf'] = number_format($cc[20]);
            }
        }
    }

    $json['min'] = $c[4];
    $json['minloan'] = number_format($c[4]);
    $json['maxloan'] = number_format($c[5]);
    $json['minpen'] = $c[14];
    $json['mindoc'] = $c[16];
    $json['tempo'] = $c[11];
    $json['contractperiod'] = $c[7];
}
else{
    $json['status'] = 2;
    $json['message'] = 'Not found!.';
}

echo json_encode($json);
?>