<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_SESSION['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}

$_SESSION['url'] = 'report.php?type=bas&id='.$_SESSION['acc'];
?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Bayar Simpanan', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Deposit Account', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $acc = '';
                                    $name = '';
                                    $amount = 0;
                                    $cek = '';
                                    //pokok
                                    $a = "exec dbo.BasicSavingPokokSearchBilling '$_SESSION[KID]','$_SESSION[acc]'";
                                    $b = sqlsrv_query($conn, $a);
                                    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                                    if($c == null){
                                        //wajib
                                        $aa = "exec dbo.BasicSavingWajibSearchBilling '$_SESSION[KID]','$_SESSION[acc]'";
                                        $bb = sqlsrv_query($conn, $aa);
                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                        if($cc == null){
                                            //sukarela
                                            $aaa = "exec dbo.BasicSavingSukarelaSearchBilling '$_SESSION[KID]','$_SESSION[acc]'";
                                            $bbb = sqlsrv_query($conn, $aaa);
                                            $ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC);
                                            if($ccc == null){
                                                echo "<script language='javascript'>document.location='balance.php';</script>";
                                            }
                                            else{
                                                $acc = $_SESSION['acc'];
                                                $name = 'Simpanan Sukarela';
                                                $amount = 0;
                                                $cek = 's';
                                            }
                                        }
                                        else{
                                            $acc = $_SESSION['acc'];
                                            $name = 'Simpanan Wajib';
                                            $amount = $cc[5];
                                        }
                                    }
                                    else{
                                        $acc = $_SESSION['acc'];
                                        $name = 'Simpanan Pokok';
                                        $amount = $c[3];
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="acc" value="<?php echo $acc; ?>" readonly>
                                            <?php echo $acc.' - '.$name; ?>
                                        </td>
                                    </tr>
                                </table>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="0" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="number" name="amount" class="form-control" placeholder="" value="<?php echo round($amount); ?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <?php
                                $disabled = '';
                                $kata = '';
                                if($amount <= 0 and $cek == ''){
                                    $disabled = 'disabled';
                                    $kata = 'Tidak ada simpanan yang harus dibayar';
                                }
                                ?>
                                <b><?php echo $kata; ?></b>
                                <button type="submit" class="btn btn-flat btn-block btn-success" <?php echo $disabled; ?>><?php echo $lang->lang('Bayar', $conn); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script type="text/javascript">
    $('#rpo').click(function(){
        $('#apo').removeClass('hide');
        $('#apo').prop('disabled', false);
        $('#awa').addClass('hide');
        $('#awa').prop('disabled', true);
        $('#asu').addClass('hide');
        $('#asu').prop('disabled', true);
    });

    $('#rwa').click(function(){
        $('#awa').removeClass('hide');
        $('#awa').prop('disabled', false);
        $('#apo').addClass('hide');
        $('#apo').prop('disabled', true);
        $('#asu').addClass('hide');
        $('#asu').prop('disabled', true);
    });

    $('#rsu').click(function(){
        $('#asu').removeClass('hide');
        $('#asu').prop('disabled', false);
        $('#apo').addClass('hide');
        $('#apo').prop('disabled', true);
        $('#awa').addClass('hide');
        $('#awa').prop('disabled', true);
    });
</script>

<?php require('footer.php');?>
