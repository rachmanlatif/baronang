<?php require('header.php');?>

<?php
$x = "select* from [dbo].[UserPaymentGateway] where email = '$_SESSION[Email]'";
$y = sqlsrv_query($conn, $x);
$z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
?>
    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body box-profile">
                <div class="widget-user-image">
                    <?php
                    $upload_dir = "upload_pic"; 				// The directory for the images to be saved in
                    $upload_path = $upload_dir."/";				// The path to where the image will be saved
                    $large_image_prefix = "profile_"; 			// The prefix name to large image
                    $large_image_name = $large_image_prefix.$_SESSION['UserID'];     // New name of the large image (append the timestamp to the filename)

                    //Image Locations
                    $large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];

                    if(file_exists($large_image_location)){?>
                        <img class="profile-user-img img-responsive img-circle" src="<?php echo $upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" alt="User Avatar">
                    <?php } else { ?>
                        <img class="profile-user-img img-responsive img-circle" src="static/images/No-Image-Icon.png" alt="User Avatar">
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">

                </div>
                <div class="col-sm-4">
                    <div class="description-block">
                        <a href="changephoto.php"><?php echo $lang->lang('Ubah foto profil', $conn); ?></a>
                    </div>
                </div>
                <div class="col-sm-4">

                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#basic" data-toggle="tab"><?php echo $lang->lang('Info', $conn); ?></a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="basic">
                    <div class="box-body">
                        <form action="pin.php" method="post">
                            <table class="table">
                                <tr>
                                    <td>
                                        <b><?php echo $lang->lang('Nama', $conn); ?></b>
                                        <input type="text" name="name" class="form-control" value="<?php echo $z[1]; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><?php echo $lang->lang('KTP', $conn); ?></b>
                                        <div class="form-control bg-gray-light">
                                            <?php echo $z[9]; ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><?php echo $lang->lang('Telepon/HP', $conn); ?></b>
                                        <input type="number" name="phone" class="form-control" value="<?php echo $z[3]; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><?php echo $lang->lang('Email', $conn); ?></b>
                                        <div class="form-control bg-gray-light">
                                            <?php echo $z[2]; ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><?php echo $lang->lang('Alamat', $conn); ?></b>
                                        <input type="text" name="address" class="form-control" value="<?php echo $z[4]; ?>">
                                    </td>
                                </tr>
                            </table>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-block btn-primary"><?php echo $lang->lang('Simpan', $conn); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>

<?php require('footer.php');?>