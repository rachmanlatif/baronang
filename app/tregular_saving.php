<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_SESSION['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}

$amount = 0;
if(isset($_GET['amount'])){
    $amount = $_GET['amount'];
}

$barcode = '';
if(isset($_POST['barcode'])){
    $barcode = $_POST['barcode'];
}

$_SESSION['url'] = 'report.php?type=reg&id='.$_SESSION['acc'];

?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Transfer Antar Akun</h3>
            </div>
            <div class="box-body">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                    <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                    <input type="hidden" name="jenis" value="15" readonly>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Akun Tabungan</label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $a = "exec dbo.RegularSavingBalanceAcc '$_SESSION[KID]','$_SESSION[acc]'";
                                    $b = sqlsrv_query($conn, $a);
                                    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                                    if($c == null){
                                        echo "<script language='javascript'>document.location='balance.php';</script>";
                                    }
                                    else{ ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" name="acc" value="<?php echo $c[2]; ?>">
                                                <?php echo $c[2].' - '.$c[4]; ?>
                                            </td>
                                            <td><?php echo 'Rp. '.$c[6]; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Transfer Ke</label>
                            <div class="col-sm-9">
                                <?php if(isset($_POST['barcode'])){ ?>
                                    <input type="number" class="form-control" name="acctrans" value="<?php echo $barcode; ?>">

                                    Masukan akun tabungan tujuan transfer<br>
                                    <input type="checkbox" name="fav" value="1"> Tambahkan akun kedalam daftar transfer
                                <?php } ?>

                                <?php if(!isset($_POST['barcode'])){ ?>
                                    <select class="form-control" name="acctrans">
                                        <option value="">- Pilih -</option>
                                        <?php
                                        $x = "exec dbo.ListRegTransferListSearchMember '$_SESSION[KID]','$_SESSION[MemberID]'";
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                            <option value="<?php echo $z[1]; ?>"><?php echo $z[1].' - '.$z[2]; ?></option>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Jumlah Transfer</label>
                            <div class="col-sm-9">
                                <input type="text" name="amount" class="form-control price" placeholder="" value="<?php echo $amount; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Note</label>
                            <div class="col-sm-9">
                                <input type="text" name="note" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-flat btn-block btn-success">Transfer</button>
                                <?php if(!isset($_POST['barcode'])){ ?>
                                    <button type="button" class="btn btn-flat btn-block btn-info" onclick="callScanner()"><i class="fa fa-camera"></i> Scan Barcode</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function callScanner() {
            Intent.openActivity("QRActivity","tregular_saving.php");
        }
    </script>

<?php require('footer.php');?>