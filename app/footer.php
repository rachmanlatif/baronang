    <br>
    </div>
</div>

    <script type="text/javascript">
        var interval = 1000; //1detik

        function doAjax(){
            $.ajax({
                url : "ajax_cekpos.php",
                type : 'POST',
                dataType : 'json',
                data: { },
                success : function(data) {
                    if(data.status == 1){
                        window.location.href = 'paymentpos.php';
                    }
                },
                complete: function (data) {
                    // Schedule the next
                    setTimeout(doAjax, interval);
                }
            });
        }

        function cekEmoney(){
            $.ajax({
                url : "ajax_cekemoney.php",
                type : 'POST',
                dataType : 'json',
                data: { },
                success : function(data) {
                    if(data.status == 1){
                        window.location.href = 'oemoney.php?barcode=' + data.barcode;
                    }
                }
            });
        }

        setTimeout(doAjax, interval);
        setTimeout(cekEmoney, interval);
    </script>

<!-- Bootstrap 3.3.5 -->
<script src="static/bootstrap/js/bootstrap.min.js"></script>
<!-- PACE -->
<script src="static/plugins/pace/pace.min.js"></script>
<!-- Select2 -->
<script src="static/plugins/select2/select2.full.min.js"></script>
<!-- Auto number format -->
<script src="static/plugins/jquery-number/jquery.number.js"></script>
    <!-- InputMask -->
<script src="static/plugins/input-mask/jquery.inputmask.js"></script>
<script src="static/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="static/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="static/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap date picker -->
<script src="static/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap time picker -->
<script src="static/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="static/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="static/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="static/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="static/dist/js/demo.js"></script>
<!-- QR Barcode Reader -->
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/qrcodelib.js"></script>
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/webcodecamjquery.js"></script>
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/mainjquery.js"></script>
<!-- Cropper -->
<script src="static/plugins/cropper/cropper.js"></script>

<!-- Page script -->
<?php require('content-footer.php');?>

</body>
</html>
