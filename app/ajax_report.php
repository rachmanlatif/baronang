<?php
if(!isset($_POST['type']) or !isset($_POST['id']) or !isset($_POST['date']) or !isset($_POST['batas']) or !isset($_POST['halaman'])){
    echo "<script language='javascript'>System.showToast('Harap masukan dengan benar');document.location='report.php';</script>";
}
else{
    session_start();
    include "connect.inc";

    $json = array(
        'status'=>0,
        'content'=>'',
        'halaman'=>'',
    );

    $exp = explode('-', $_POST['date']);
    $startdate = $exp[0];
    $enddate = date('Y-m-d H:i:s', strtotime($exp[1] . ' +1 day'));

    $arr = array();
    ?>

    <?php if($_POST['type'] == 'lim'){ ?>
        <?php
        $x = "select TOP $_POST[batas] * from [Gateway].[dbo].[TransaksiPay] where KodeAcc = '$_POST[id]' and Tanggal >= '$startdate' and Tanggal <= '$enddate' order by Tanggal desc";
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            $amount = 0;
            if($z[4] > 0){
                $amount = number_format($z[4]);
            }

            $content ="<tr>
                <td>".$z[1]->format('d/m')."</td>
                <td>
                    ".$z[0]."
                </td>
                <td>
                    <span class='pull-right'>".$amount."</span>
                </td>
            </tr>";

            array_push($arr, $content);
        }
        ?>
    <?php } ?>

    <?php if($_POST['type'] == 'bas'){ ?>
        <?php
        $x = "select TOP $_POST[batas] * from [$_SESSION[Kop]].[dbo].[BasicSavingTransaction] where AccNo = '$_POST[id]' and TimeStam >= '$startdate' and TimeStam <= '$enddate' order by TimeStam desc";
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            $type = '';
            $a = "select * from [$_SESSION[Kop]].[dbo].[TransactionType] where KodeTransactionType = '$z[4]'";
            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
            if($c != null){
                $type = $c[1];
            }

            $amount = 0;
            if($z[7] > 0){
                $amount = number_format($z[7]);
            }

            $content ="<tr>
                <td>".$z[3]->format('d/m')."</td>
                <td>
                    ".$z[1]."<br>
                    ".$type."
                </td>
                <td>
                    <span class='pull-right'>".$amount."</span>
                </td>
            </tr>";

            array_push($arr, $content);
        }
        ?>
    <?php } ?>

    <?php if($_POST['type'] == 'emon'){ ?>
        <?php
        $x = "select TOP $_POST[batas] * from [$_SESSION[Kop]].[dbo].[eMoneyTrans] where AccNo = '$_POST[id]' and TimeStam >= '$startdate' and TimeStam <= '$enddate' order by TimeStam desc";
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            $type = $z[5];

            $amount = 0;
            $st = '';
            if($z[6] > 0){
                $amount = number_format($z[6]);
                $st = 'DB';
            }
            else{
                $amount = number_format($z[7]);
                $st = 'CR';
            }

            $content ="<tr>
                <td>
                    ".$z[2]->format('d/m')."<br>
                    ".$st."
                </td>
                <td>
                    ".$z[0]."<br>
                    ".$type."
                </td>
                <td>
                    <span class='pull-right'>".$amount."</span>
                </td>
            </tr>";

            array_push($arr, $content);
        }
        ?>
    <?php } ?>

    <?php if($_POST['type'] == 'bill'){ ?>
        <?php
        $x = "select TOP $_POST[batas] * from [$_SESSION[Kop]].[dbo].[TransaksiBill] where Kode = '$_POST[id]' and Tanggal >= '$startdate' and Tanggal <= '$enddate' order by Tanggal desc";
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            $type = '';

            $amount = 0;
            $st = '';
            if($z[4] > 0){
                $amount = number_format($z[4]);
                $st = 'DB';
            }
            else{
                $amount = number_format($z[5]);
                $st = 'CR';
            }

            $content ="<tr>
                <td>
                    ".$z[1]->format('d/m')."<br>
                    ".$st."
                </td>
                <td>
                    ".$z[0]."<br>
                    ".$type."
                </td>
                <td>
                    <span class='pull-right'>".$amount."</span>
                </td>
            </tr>";

            array_push($arr, $content);
        }
        ?>
    <?php } ?>

    <?php if($_POST['type'] == 'reg'){ ?>
        <?php
        $x = "select TOP $_POST[batas] * from [$_SESSION[Kop]].[dbo].[RegularSavingTrans] where AccNumber = '$_POST[id]' and TimeStam >= '$startdate' and TimeStam <= '$enddate' order by TimeStam desc";
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            $type = $z[5];

            $amount = 0;
            $st = '';
            if($z[7] > 0){
                $amount = number_format($z[7]);
                $st = 'DB';
            }
            else{
                $amount = number_format($z[8]);
                $st = 'CR';
            }

            $content ="<tr>
                <td>
                    ".$z[3]->format('d/m')."<br>
                    ".$st."
                </td>
                <td>
                    ".$z[1]."<br>
                    ".$type."
                </td>
                <td>
                    <span class='pull-right'>".$amount."</span>
                </td>
            </tr>";

            array_push($arr, $content);
        }
        ?>
    <?php } ?>

    <?php if($_POST['type'] == 'time'){ ?>
        <?php
        $x = "select TOP $_POST[batas] * from [$_SESSION[Kop]].[dbo].[TimeDepositTrans] where AccountNumber = '$_POST[id]' and TimeStam >= '$startdate' and TimeStam <= '$enddate' order by TimeStam desc";
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            $type = '';
            $a = "select * from [$_SESSION[Kop]].[dbo].[TransactionType] where KodeTransactionType = '$z[3]'";
            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
            if($c != null){
                $type = $c[1];
            }

            $amount = 0;
            $st = '';
            if($z[6] > 0){
                $amount = number_format($z[6]);
                $st = 'DB';
            }
            else{
                $amount = number_format($z[7]);
                $st = 'CR';
            }

            $content ="<tr>
                <td>
                    ".$z[2]->format('d/m')."<br>
                    ".$st."
                </td>
                <td>
                    ".$z[0]."<br>
                    ".$type."
                </td>
                <td>
                    <span class='pull-right'>".$amount."</span>
                </td>
            </tr>";

            array_push($arr, $content);
        }
        ?>
    <?php } ?>

    <?php if($_POST['type'] == 'loan'){ ?>
        <?php
        $x = "select TOP $_POST[batas] * from [$_SESSION[Kop]].[dbo].[LoanTransaction] where LoanNumber = '$_POST[id]' and TimeStamp >= '$startdate' and TimeStamp <= '$enddate' order by TimeStamp desc";
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            $type = '';
            $a = "select * from [$_SESSION[Kop]].[dbo].[TransactionType] where KodeTransactionType = '$z[3]'";
            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
            if($c != null){
                $type = $c[1];
            }

            $amountpr = 0;
            $amountint = 0;
            if($z[3] == 'LNCP'){
                if($z[8] > 0){
                    $amountpr = number_format($z[8],2).' PR';
                }

                if($z[9] > 0){
                    $amountint = number_format($z[9],2).' INT';
                }
            }
            else{
                if($z[6] > 0){
                    $amountpr = number_format($z[6],2).' PR';
                }

                if($z[7] > 0){
                    $amountint = number_format($z[7],2).' INT';
                }
            }

            $content ="<tr>
                <td>".$z[1]->format('d/m')."</td>
                <td>
                    ".$z[0]."<br>
                    ".$type."
                </td>
                <td>
                    <span class='pull-right'>".$amountpr."</span><br>
                    <span class='pull-right'>".$amountint."</span>
                </td>
            </tr>";

            array_push($arr, $content);
        }
        ?>
    <?php } ?>

    <?php if($_POST['type'] == 'edc'){ ?>
        <?php
        $x = "select TOP $_POST[batas] * from [$_SESSION[Kop]].[dbo].[eMoneyTrans] where AccNo = '$_POST[id]' and TimeStam >= '$startdate' and TimeStam <= '$enddate' order by TimeStam desc";
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            $type = '';
            $a = "select * from [$_SESSION[Kop]].[dbo].[TransactionType] where KodeTransactionType = '$z[3]'";
            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
            if($c != null){
                $type = $c[1];
            }

            $amount = 0;
            $st = '';
            if($z[6] > 0){
                $amount = number_format($z[6]);
                $st = 'DB';
            }
            else{
                $amount = number_format($z[7]);
                $st = 'CR';
            }

            $content ="<tr>
                <td>
                    ".$z[2]->format('d/m')."<br>
                    ".$st."
                </td>
                <td>
                    ".$z[0]."<br>
                    ".$type."
                </td>
                <td>
                    <span class='pull-right'>".$amount."</span>
                </td>
            </tr>";

            array_push($arr, $content);
        }
        ?>
      <?php } ?>

    <?php
    $json['status'] = 1;
    $json['content'] = $arr;
    $json['halaman'] = $_POST['halaman']+1;
    echo json_encode($json);
    ?>

<?php } ?>
