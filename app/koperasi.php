<?php require('header.php');?>

<?php
if(!isset($_GET['id'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

$x = "select* from [dbo].[UserMemberKoperasi] where UserID = '$_SESSION[UserID]' and KID = '$_GET[id]'";
$y = sqlsrv_query($conn, $x);
$z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
?>

<div class="col-sm-12">
<h3>Choose Your Account</h3>
</div>

<?php if(isset($_GET['t']) and $_GET['t'] == 'bs' or !isset($_GET['t'])){ ?>
    <div class="col-sm-6">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green-gradient">
                <h3 class="widget-user-username">Basic Saving Balance</h3>
                <h5 class="widget-user-desc">List Account</h5>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <?php
                    $a = "exec [dbo].[ListBasicSavingBal] '$z[0]','$z[2]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <li><a href="dbasic_saving.php?kid=<?php echo $_GET['id'] ?>&mid=<?php echo $z[2]; ?>&t=<?php echo $c[4]; ?>"><b><?php echo $c[5]; ?></b> <?php echo $c[4]; ?><span class="pull-right badge bg-green">Rp. <?php echo number_format($c[7]); ?></span></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <!-- /.widget-user -->
    </div>
<?php } ?>

<?php if(isset($_GET['t']) and $_GET['t'] == 'rs' or !isset($_GET['t'])){ ?>
    <div class="col-sm-6">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-gradient">
                <h3 class="widget-user-username">Regular Saving Balance</h3>
                <h5 class="widget-user-desc">List Account</h5>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <?php
                    $a = "exec [dbo].[ListRegularSavingBal] '$z[0]','$z[2]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <li><a href="dregular_saving.php?kid=<?php echo $_GET['id'] ?>&mid=<?php echo $z[2]; ?>"><b><?php echo $c[2]; ?></b> <?php echo $c[4]; ?><span class="pull-right badge bg-aqua">Rp. <?php echo number_format($c[5]); ?></span></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <!-- /.widget-user -->
    </div>
<?php } ?>

<?php if(isset($_GET['t']) and $_GET['t'] == 'td' or !isset($_GET['t'])){ ?>
    <div class="col-sm-12">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue-gradient">
                <h3 class="widget-user-username">Time Deposit</h3>
                <h5 class="widget-user-desc">List Account</h5>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <?php
                    $a = "exec [dbo].[ListTimeDepositBal] '$z[0]','$z[2]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <li><a href="#"><b><?php echo $c[7]; ?></b> <?php echo $c[9]; ?><span class="pull-right badge bg-blue">Rp. <?php echo number_format($c[14]); ?></span></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <!-- /.widget-user -->
    </div>
<?php } ?>

<?php require('footer.php');?>