<?php require('header-login.php');?>

<?php
$_SESSION['url'] = 'login.php';
?>
    <span style="text-align:center;position: absolute;top: 15%;">
        <img id="imgup" src ="images/logo_vertical.png" style="width: 40%;" align="middle">
    </span>

    <div class="footer2">
        <div class="register-box-body" style="background: #0a3177;">
            <p class="login-box-msg" style="color: white;">
                <label><?php echo $lang->lang('Lupa PIN', $conn); ?></label>
            </p>
            <form action="procforgotpin.php" method="post" autocomplete="off">
                <div class="form-group has-feedback">
                    <input id="email" type="email" name="email" class="form-control" value="<?php echo $_SESSION['Email']; ?>" placeholder="Email" style="background: transparent;color: white;">
                    <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true" style="color: white;"></span>
                </div>
                <div class="form-group has-feedback">
                    <div class="input-group">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" style="background: transparent;color: white;">
                        <span class="input-group-btn" style="color: white;">
                            <div id="eye1" class="btn"><i class="fa fa-eye"></i></div>
                            <div id="eye2" class="btn hide"><i class="fa fa-eye-slash"></i></div>
                        </span>
                    </div>
                </div>
                <div class="form-group has-feedback" style="color: white;">
                    <?php echo $lang->lang('Harap masukan email dengan benar. Konfirmasi perubahan PIN akan dikirim melalui email.', $conn); ?>
                </div>
                <br>
                <button type="submit" class="btn btn-lg btn-info btn-flat btn-block btn-login" style="border-radius: 30px;"><?php echo $lang->lang('Kirim', $conn); ?></button>
                <br>
                <a href="login.php"><button type="button" class="btn btn-lg btn-default btn-flat btn-block" style="border-radius: 30px;"><?php echo $lang->lang('Batal', $conn); ?></button></a>
            </form>
        </div><!-- /.login-box-body -->
    </div>

    <script type="text/javascript">
        $( "#email" ).focus(function() {
            $("#imgup").hide();
        });

        $( "#email" ).blur(function() {
            $("#imgup").show();
        });
    </script>
<?php require('footer-login.php');?>