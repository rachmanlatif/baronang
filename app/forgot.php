<?php require('header-login.php');?>

<?php
$_SESSION['url'] = 'login.php';
?>
    <span style="text-align:center;position: absolute;top: 15%;">
        <img id="imgup" src ="images/logo_vertical.png" style="width: 40%;" align="middle">
    </span>

    <div class="footer2">
        <div class="register-box-body" style="background: #0a3177;">
            <p class="login-box-msg" style="color: white;">
                <label><?php echo $lang->lang('Lupa Password', $conn); ?></label>
            </p>
            <form action="procforgot.php" method="post" autocomplete="off">
                <div class="form-group has-feedback">
                    <input id="email" type="email" name="email" class="form-control" value="<?php echo $_SESSION['Email']; ?>" placeholder="Email" style="background: transparent;color: white;">
                    <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true" style="color: white;"></span>
                </div>
                <div class="form-group has-feedback" style="color: white;">
                    <?php echo $lang->lang('Harap masukan email dengan benar. Konfirmasi perubahan password akan dikirim melalui email.', $conn); ?>
                </div>
                <br>
                <br>
                <br>
                <button type="submit" class="btn btn-lg btn-primary btn-info btn-block btn-login" style="border-radius: 30px;"><?php echo $lang->lang('Kirim', $conn); ?></button>
                <br>
                <a href="login.php"><button type="button" class="btn btn-lg btn-default btn-flat btn-block" style="border-radius: 30px;"><?php echo $lang->lang('Batal', $conn); ?></button></a>
            </form>
        </div><!-- /.login-box-body -->
    </div>

    <script type="text/javascript">
        $( "#email" ).focus(function() {
            $("#imgup").hide();
        });

        $( "#email" ).blur(function() {
            $("#imgup").show();
        });
    </script>
<?php require('footer-login.php');?>