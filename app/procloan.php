<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                    <br>
                    Sedang memproses pengajuan
                </div>
            </div>
        </div>
    </div>

<?php
$kid = $_POST['kid'];
$member = $_POST['mid'];
$acc = $_POST['acc'];
$rsacc = $_POST['rsacc'];
$amount = str_replace(',','',$_POST['amount']);

$minpen = $_POST['minpen'];
$mindoc = $_POST['mindoc'];
$tempo = $_POST['tempo'];
$contractperiod = $_POST['contractperiod'];

$max = $_POST['max'];

$q = "exec dbo.getKodeLoanAppNumber '$kid','$member','$acc'";
$w = sqlsrv_query($conn, $q);
$e = sqlsrv_fetch_array( $w, SQLSRV_FETCH_NUMERIC);
$code = $e[0];

$a = "exec dbo.ProsesLoadSimulasi '$kid','$code','$acc','$amount'";
$b = sqlsrv_query($conn, $a);

$cicilan = 0;
$er = "exec dbo.SimulasiLoanSearch '$kid','$code'";
$df = sqlsrv_query($conn, $er);
$cv = sqlsrv_fetch_array($df, SQLSRV_FETCH_NUMERIC);
if($cv != null){
    $cicilan = $cv[6];
}

if($acc == "" || $rsacc == "" || $amount <= 0 || !is_numeric($amount)){
    echo "<script language='javascript'>System.showToast('Harap isi seluruh kolom ');document.location='oloan.php';</script>";
}
else{
    //save doc
    if(isset($_FILES['filename'])){
        $uploads_dir = 'uploads/';
        foreach ($_FILES["filename"]["error"] as $key => $error) {
            $tmp_name = $_FILES["filename"]["tmp_name"][$key];
            $path = $uploads_dir . basename($_FILES["filename"]["name"][$key]);

            $name = $_POST['name'][$key];

            if($tmp_name != '' or $tmp_name != null){
                if(move_uploaded_file($tmp_name, $path)){
                    $s = "exec dbo.ProsesLoanDocUpload '$kid','$code','$name','$path',''";
                    $v = sqlsrv_query($conn, $s);
                    if(!$v){
                        echo "<script language='javascript'>System.showToast('Gagal menyimpan file');document.location='oloan.php';</script>";
                    }
                }
                else{
                    echo "<script language='javascript'>System.showToast('Gagal mengupload file');document.location='oloan.php';</script>";
                }
            }
        }
    }

    //save penjamin
    if(isset($_POST['uid'])){
        foreach($_POST['uid'] as $u){
            $a = "select * from [dbo].[UserMemberKoperasi] where KID='$_SESSION[KID]' and UserID='$u'";
            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
            if($c != null){
                $p = "exec dbo.ProsesLoanPenjamin '$kid','$code','$c[2]',0,'',1";
                $o = sqlsrv_query($conn, $p);
                if($o){
                    //create approval penjamin
                    $exp = date('Y-m-d H:i:s', strtotime("+".$tempo." days"));
                    $kata = 'User ID '.$_SESSION['UserID'].' - '.$_SESSION['NamaUser'].' menambahkan anda sebagai penjamin. Setujui ?';
                    $a = "exec [dbo].[ProsesRequestPage] '$_SESSION[KID]','$u','$_SESSION[UserID]','oloan.php',0,'$kata','$code','0','','$exp'";
                    $b = sqlsrv_query($conns, $a);
                }
                else{
                    echo "<script language='javascript'>System.showToast('Gagal menyimpan penjamin');document.location='oloan.php';</script>";
                }
            }
        }
    }

    //get loan type
    $qwe = "exec dbo.LoanTypeSearch '$kid','$acc'";
    $asd = sqlsrv_query($conn, $qwe);
    $zxc = sqlsrv_fetch_array( $asd, SQLSRV_FETCH_NUMERIC);
    if($zxc != null){
        if($zxc[7] >= $contractperiod){
            if($zxc[4] < $amount){
                if($zxc[5] > $amount){
                    $qw = "select StatusLimit from $_SESSION[Kop].dbo.GeneralSetting";
                    $as = sqlsrv_query($conn, $qw);
                    $zx = sqlsrv_fetch_array( $as, SQLSRV_FETCH_NUMERIC);
                    if($zx != null){
                        if($zx[0] == 1 and $amount > $max){
                            echo '<script language="javascript">System.showToast("Jumlah pengajuan pinjaman harus lebih kecil dari '.number_format($max).'");document.location="oloan.php";</script>';
                        }
                        else if($zx[0] == 2 and $max < $cicilan){
                            echo '<script language="javascript">System.showToast("Jumlah cicilan anda melebihi limit '.number_format($max).'");document.location="oloan.php";</script>';
                        }
                        else{
                            $status = 0;
                            $msg = 'Berhasil membuka akun pinjaman. Pinjaman menunggu disetujui penjamin atau kelengkapan dokumen';

                            if($zxc[13] == 0 and $zxc[15] == 0){
                                $status = 1;
                                $msg = 'Berhasil membuka akun pinjaman. Pinjaman akan diproses lebih lanjut';
                            }

                            $sql = "exec dbo.ProsesOpenLoan '$kid','$member','$code','$acc','$amount','$_SESSION[UserID]','$status','$rsacc','$contractperiod'";
                            $stmt = sqlsrv_query($conn, $sql);
                            if($stmt){
                                messageAlert($msg);
                                echo "<script language='javascript'>document.location='notif.php';</script>";
                            }
                            else{
                                echo "<script language='javascript'>System.showToast('Gagal membuat pengajuan pinjaman');document.location='oloan.php';</script>";
                            }
                        }
                    }
                    else{
                        echo "<script language='javascript'>System.showToast('Gagal membuat pengajuan pinjaman. Limit tidak tersedia');document.location='oloan.php';</script>";
                    }
                }
                else{
                    echo '<script language="javascript">System.showToast("Maximum jumlah pengajuan pinjaman adalah '.number_format($zxc[5]).'");document.location="oloan.php";</script>';
                }
            }
            else{
                echo '<script language="javascript">System.showToast("Minimum jumlah pengajuan pinjaman adalah '.number_format($zxc[4]).'");document.location="oloan.php";</script>';
            }
        }
        else{
            echo '<script language="javascript">System.showToast("Maximum lama angsuran tidak boleh melebihi '.number_format($zxc[7]).'x");document.location="oloan.php";</script>';
        }
    }
    else{
        echo "<script language='javascript'>System.showToast('Gagal membuat pengajuan pinjaman');document.location='oloan.php';</script>";
    }
}
?>

<?php require('footer.php');?>
