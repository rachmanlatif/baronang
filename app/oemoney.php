<?php require('header-register.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Buka e-Money', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form id="procpin" action="procemoney.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Produk e-money', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $a = "exec dbo.emoneyTypeView '$_SESSION[KID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td><input type="radio" name="acc" value="<?php echo $c[0]; ?>" data-toggle="modal" data-target=".bs-example-modal-<?php echo $c[0]; ?>"></td>
                                            <td><?php echo $c[1]; ?></td>
                                        </tr>

                                        <div class="modal fade bs-example-modal-<?php echo $c[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel"><?php echo $lang->lang('e-Money', $conn); ?> <?php echo $c[1]; ?></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <ul class="nav nav-stacked">
                                                            <li><a href="#"><?php echo $lang->lang('Max Limit ', $conn); ?><span class="pull-right"><?php echo number_format($c[2]); ?></span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Fee Top Up', $conn); ?><span class="pull-right"><?php echo number_format($c[3]); ?> </span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Harga e-Money', $conn); ?><span class="pull-right"><?php echo number_format($c[4]); ?></span></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $lang->lang('Tutup', $conn); ?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </table>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="13" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Akun Tabungan', $conn); ?></label>
                            <div class="col-sm-9">
                                <select name="rsacc" class="form-control">
                                    <option value=""><?php echo $lang->lang('Pilih', $conn); ?></option>
                                    <?php
                                    $a = "exec dbo.ListRegularSavingBal '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <option value="<?php echo $c[2]; ?>"><?php echo $c[2].' - '.$c[4].' , Rp. '.$c[6]; ?></option>
                                    <?php } ?>
                                </select>
                                <?php echo $lang->lang('e-Money akan menggunakan saldo akun Regular Saving yang dipilih', $conn); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Saldo', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="number" name="amount" class="form-control price" placeholder="" value="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="box-header text-center">
                                <h3 class="box-title">Masukan PIN anda</h3>
                            </div>
                            <div class="col-xs-12">
                                <input id="pin" name="pin" type="password" class="form-control text-center" style="font-size: 2em;background: white;" readonly>
                            </div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n1" class="btn btn-lg btn-default" style="border-radius: 40px;">1</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n2" class="btn btn-lg btn-default" style="border-radius: 40px;">2</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n3" class="btn btn-lg btn-default" style="border-radius: 40px;">3</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n4" class="btn btn-lg btn-default" style="border-radius: 40px;">4</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n5" class="btn btn-lg btn-default" style="border-radius: 40px;">5</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n6" class="btn btn-lg btn-default" style="border-radius: 40px;">6</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n7" class="btn btn-lg btn-default" style="border-radius: 40px;">7</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n8" class="btn btn-lg btn-default" style="border-radius: 40px;">8</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n9" class="btn btn-lg btn-default" style="border-radius: 40px;">9</button></div>
                            <div class="col-xs-4" style="padding: 20px;"></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n0" class="btn btn-lg btn-default" style="border-radius: 40px;">0</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="nb" class="btn btn-lg btn-default" style="border-radius: 40px;"><i class="fa fa-chevron-left"></i></button></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 col-xs-12">
                                <a href="proccanceltrans.php?barcode=<?php echo $_GET['barcode']; ?>"><button type="button" class="btn btn-xs btn-default btn-flat btn-block"><i class="fa fa-times"></i> Batalkan Transaksi</button></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function cekDigit(){
            var a = $('#pin').val();
            if(a.length == 6){
                $('#procpin').submit();
            }

            if(a.length > 6){
                $('#pin').val($('#pin').val().slice(0, -1));
            }
        }

        $('#n1').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '1');

            cekDigit();
        });
        $('#n2').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '2');

            cekDigit();
        });
        $('#n3').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '3');

            cekDigit();
        });
        $('#n4').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '4');

            cekDigit();
        });
        $('#n5').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '5');

            cekDigit();
        });
        $('#n6').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '6');

            cekDigit();
        });
        $('#n7').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '7');

            cekDigit();
        });
        $('#n8').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '8');

            cekDigit();
        });
        $('#n9').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '9');

            cekDigit();
        });
        $('#n0').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '0');

            cekDigit();
        });
        $('#nb').click(function(){
            $('#pin').val($('#pin').val().slice(0, -1));
        });
    </script>

<?php require('footer-login.php');?>
