<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Buka Akun Deposito', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Produk Deposito', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $a = "exec dbo.TimeDepositTypeView '$_SESSION[KID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td><input type="radio" name="acc" value="<?php echo $c[0]; ?>" data-toggle="modal" data-target=".bs-example-modal-<?php echo $c[0]; ?>"></td>
                                            <td><?php echo $c[1]; ?></td>
                                        </tr>

                                        <div class="modal fade bs-example-modal-<?php echo $c[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel"><?php echo $lang->lang('Deposito', $conn); ?> <?php echo $c[1]; ?></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <ul class="nav nav-stacked">
                                                            <li><a href="#"><?php echo $lang->lang('Suku Bunga %', $conn); ?><span class="pull-right"><?php echo $c[2]; ?>%</span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Masa Kontrak', $conn); ?><span class="pull-right"><?php echo $c[3]; ?> <?php echo $lang->lang('Bulan', $conn); ?></span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Jumlah Minimum', $conn); ?><span class="pull-right"><?php echo number_format($c[4]); ?></span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Biaya Penalti', $conn); ?><span class="pull-right"><?php echo number_format($c[5]); ?></span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Bunga Penalti %', $conn); ?><span class="pull-right"><?php echo $c[6]; ?></span></a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $lang->lang('Tutup', $conn); ?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </table>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="11" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Akun Tabungan', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table">
                                    <?php
                                    $a = "exec [dbo].[ListRegularSavingBal] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="radio" name="regacc" class="minimal" id="regacc" value="<?php echo $c[2]; ?>">
                                            <b><?php echo $c[2]; ?></b> - <?php echo $c[4]; ?><span class="pull-right badge bg-aqua">Rp. <?php echo number_format($c[5]); ?></span>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="aro" class="col-sm-3 control-label" style="text-align: left;">ARO</label>
                            <div class="col-sm-9">
                                <label style="padding-right: 20px">
                                    <input type="radio" name="aro" class="minimal" id="aro" value="1">
                                    <?php echo $lang->lang('Ya', $conn); ?>
                                </label>
                                <label>
                                    <input type="radio" name="aro" class="minimal" id="aro" value="0" checked>
                                    <?php echo $lang->lang('Tidak', $conn); ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="itm" class="col-sm-3 control-label" style="text-align: left;"><?php echo $lang->lang('Bunga Ditransfer Bulanan', $conn); ?></label>
                            <div class="col-sm-9">
                                <label style="padding-right: 20px">
                                    <input type="radio" name="itm" class="minimal" id="itm" value="1">
                                    <?php echo $lang->lang('Ya', $conn); ?>
                                </label>
                                <label>
                                    <input type="radio" name="itm" class="minimal" id="itm" value="0" checked>
                                    <?php echo $lang->lang('Tidak', $conn); ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="itaa" class="col-sm-3 control-label" style="text-align: left;"><?php echo $lang->lang('Bunga Ditransfer Saat ARO', $conn); ?></label>
                            <div class="col-sm-9">
                                <label style="padding-right: 20px">
                                    <input type="radio" name="itaa" class="minimal" id="itaa" value="1">
                                    <?php echo $lang->lang('Ya', $conn); ?>
                                </label>
                                <label>
                                    <input type="radio" name="itaa" class="minimal" id="itaa" value="0" checked>
                                    <?php echo $lang->lang('Tidak', $conn); ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="text" name="amount" class="form-control price" placeholder="" value="0">
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-flat btn-block btn-success"><?php echo $lang->lang('Simpan', $conn); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php require('footer.php');?>