<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Buka Akun Tabungan', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Produk Tabungan', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $a = "exec dbo.RegularSavingTypeView '$_SESSION[KID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td><input type="radio" name="acc" class="acc" value="<?php echo $c[0]; ?>" data-toggle="modal" data-target=".bs-example-modal"></td>
                                            <td><?php echo $c[1]; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="9" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Setoran Awal', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="text" name="amount" class="form-control price" placeholder="" value="0">
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-flat btn-block btn-success"><?php echo $lang->lang('Simpan', $conn); ?></button>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('.acc').click(function(){
            var acc = $("input[name='acc']:checked").val();

            $.ajax({
                url : "ajax_getskema.php",
                type : 'POST',
                data: { acc: acc},
                success : function(data) {
                    $(".modal-content").html(data);
                },
                error : function(){
                    System.showToast('Coba lagi');
                }
            });
        });

    </script>
<?php require('footer.php');?>