<?php require('header-register.php');?>

<?php
$_SESSION['url'] = 'login.php';
?>
    <div class="register-box-body">
        <p class="login-box-msg">
            <label><?php echo $lang->lang('Daftar', $conn); ?></label>
        </p>
			<form action="procregister.php" method="post">
			<div class="form-group has-feedback">
                <b><?php echo $lang->lang('Tipe Account', $conn); ?></b>			
				<div class="radio">
					<label><input type="radio" name="tipeacc" value="0"><?php echo $lang->lang('Personal', $conn); ?></label>
				</div>
			<div class="radio">
				<label><input type="radio" name="tipeacc" value="1"><?php echo $lang->lang('Corporate', $conn); ?></label>
			</div>
			</div>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="<?php echo $lang->lang('Email', $conn); ?>">
                <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="number" name="ktp" class="form-control" placeholder="<?php echo $lang->lang('KTP', $conn); ?>">
                <span class="fa fa-address-card-o form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="name" class="form-control" placeholder="<?php echo $lang->lang('Nama', $conn); ?>">
                <span class="fa fa-file-o form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="number" name="telp" class="form-control" placeholder="<?php echo $lang->lang('Telepon/HP', $conn); ?>">
                <span class="fa fa-phone form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="address" class="form-control" placeholder="<?php echo $lang->lang('Alamat', $conn); ?>">
                <span class="fa fa-globe form-control-feedback" aria-hidden="true"></span>
            </div>

            <b><?php echo $lang->lang('Keamanan Akun', $conn); ?></b>
            <br>
            <div class="form-group has-feedback">
                <input type="password" id="password" name="password" class="form-control" placeholder="<?php echo $lang->lang('Kata Sandi', $conn); ?>">
                <span class="fa fa-eye form-control-feedback eye1" aria-hidden="true"></span>
                <span class="fa fa-eye-slash form-control-feedback eye11 hide" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="repassword" name="repassword" class="form-control" placeholder="<?php echo $lang->lang('Ulangi Password', $conn); ?>" onkeyup="validatePassword()">
                <span class="fa fa-eye form-control-feedback eye2" aria-hidden="true"></span>
                <span class="fa fa-eye-slash form-control-feedback eye22 hide" aria-hidden="true"></span>
                <?php echo $lang->lang('Masukan Ulang Password', $conn); ?>
            </div>
            <div class="form-group has-feedback">
                <input type="number" id="pin" name="pin" class="form-control" placeholder="<?php echo $lang->lang('PIN', $conn); ?>">
                <span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
                <?php echo $lang->lang('Harus 6 angka', $conn); ?>
            </div>
            <div class="form-group has-feedback">
                <input type="number" id="repin" name="repin" class="form-control" placeholder="<?php echo $lang->lang('Ulangi PIN', $conn); ?>" onkeyup="validatePin()">
                <span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
                <?php echo $lang->lang('Masukan Ulang PIN', $conn); ?>
            </div>
            <div class="box-footer">
                <div class="row">
                    <button type="submit" class="btn btn-lg btn-primary btn-flat btn-block btn-register" style="border-radius: 30px;"><?php echo $lang->lang('Daftar', $conn); ?></button>
                </div>
            </div>
            <div class="form-group has-feedback text-center">
                <?php echo $lang->lang('Dengan mendaftar', $conn); ?>, <?php echo $lang->lang('Saya menyetujui', $conn); ?><br>
                <a id="btn-eula"><?php echo $lang->lang('Syarat ketentuan', $conn); ?></a> <?php echo $lang->lang('Serta', $conn); ?> <a id="btn-privacy"><?php echo $lang->lang('Kebijakan privasi', $conn); ?></a>
            </div>
        </form>
        <button type="button" id="btn-login" class="btn btn-lg btn-info btn-flat btn-block" style="border-radius: 30px;"><?php echo $lang->lang('Masuk', $conn); ?></button>
    </div><!-- /.login-box-body -->

    <script type="text/javascript">
        $('#btn-login').click(function(){
            Intent.openActivity('LoginActivity','login.php');
        });

        $('#btn-eula').click(function(){
            Intent.openActivity('LoginActivity','eula.php');
        });

        $('#btn-privacy').click(function(){
            Intent.openActivity('LoginActivity','privacy-pl.php');
        });

        $('.eye1').click(function(){
            $("#password").prop('type','text');
            $('.eye1').hide();
            $('.eye11').removeClass('hide');
        });

        $('.eye11').click(function(){
            $("#password").prop('type','password');
            $('.eye11').addClass('hide');
            $('.eye1').show();
        });

        $('.eye2').click(function(){
            $("#repassword").prop('type','text');
            $('.eye2').hide();
            $('.eye22').removeClass('hide');
        });

        $('.eye22').click(function(){
            $("#repassword").prop('type','password');
            $('.eye22').addClass('hide');
            $('.eye2').show();
        });

    </script>

<?php require('footer-login.php');?>