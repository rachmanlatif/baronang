<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Buka EDC', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Akun Tabungan', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table">
                                    <?php
                                    $a = "exec [dbo].[ListRegularSavingBal] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="radio" name="acc" class="minimal" id="regacc" value="<?php echo $c[2]; ?>">
                                            <b><?php echo $c[2]; ?></b> - <?php echo $c[4]; ?><span class="pull-right badge bg-aqua">Rp. <?php echo number_format($c[5]); ?></span>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                            <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                            <input type="hidden" name="amount" value="1" readonly>
                            <input type="hidden" name="jenis" value="16" readonly>
                        </div>
                        <div class="form-group">
                            <label for="aro" class="col-sm-3 control-label" style="text-align: left;">Info</label>
                            <div class="col-sm-9">
                                Akun tabungan yang dipilih akan dgunakan sebagai pendapatan setiap transaksi yang dilakukan malalui Baronang EDC
                                <br>
                                <br>
                                Download Baronang EDC di PlayStore untuk dapat menggunakan EDC
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-flat btn-block btn-success"><?php echo $lang->lang('Simpan', $conn); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php require('footer.php');?>