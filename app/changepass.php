<?php require('header.php');?>

    <div class="col-sm-12">
        <form action="procchange.php" method="post">
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">Ubah Password</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <input type="password" class="form-control" name="old" placeholder="Password Lama">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="new" placeholder="Password Baru">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="retype" placeholder="Ulangi Password Baru">
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-block btn-primary">Ubah</button>
                </div>
            </div>
        </form>
    </div>

<?php require('footer.php');?>