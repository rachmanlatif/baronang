<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                </div>
            </div>
        </div>
    </div>

<?php
require_once 'lib/googleLib/GoogleAuthenticator.php';

if($_POST['kid'] == '' || $_POST['mid'] == '' || $_POST['jenis'] == '' || $_POST['amount'] == '' || $_POST['amount'] <= 0){
    echo "<script language='javascript'>history.go(-1);</script>";
}
else{

    $kid = $_POST['kid'];
    $mid = $_POST['mid'];
    $acc = $_POST['acc'];
    $amount = $_POST['amount'];
    $jenis = $_POST['jenis'];

    $regacc = $_POST['regacc'];
    //$gac = $_POST['gac'];
    $pin = md5($_POST['pin']);

    $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$_SESSION[UserID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $ga = new GoogleAuthenticator();
        //$checkResult = $ga->verifyCode($c[11], $gac, 2);    // 2 = 2*30sec clock tolerance
        if ($pin == $c[12])
        {
            //$_SESSION['googleCode'] = $gac;

            $x = "exec [dbo].[RegularSavingAccSearch] '$kid','$_SESSION[MemberID]','$regacc'";
            $y = sqlsrv_query($conn, $x);
            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
            if($z != null){
                $q = "exec [dbo].[RegularSavingTypeSearch] '$kid','$z[3]'";
                $w = sqlsrv_query($conn, $q);
                $e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);

                $balance = $z[5] - $e[4];
                if($balance >= $amount){
                    $p = "exec [dbo].[emoneySearchTransfer] '$kid','$_SESSION[acc]'";
                    $l = sqlsrv_query($conn, $p);
                    $m = sqlsrv_fetch_array($l, SQLSRV_FETCH_NUMERIC);
                    if($m != null){
                      $pp = "exec [dbo].[emoneyTypeSearch] '$kid','$m[2]'";
                      $ll = sqlsrv_query($conn, $pp);
                      $mm = sqlsrv_fetch_array($ll, SQLSRV_FETCH_NUMERIC);
                        $aktual = $m[4]+$amount;
                        if($mm[2] >= $aktual){
                            $nominal = str_replace(',','',number_format($amount,0));

              							$sql = "exec [dbo].[Prosesemoneytopup] '$kid', '$_SESSION[acc]',$nominal,'$_SESSION[UserID]','$regacc'";
                            $exec = sqlsrv_query($conn, $sql);
                            if($exec){
                                messageAlert('Berhasil melakukan top up');
                                echo "<script language='javascript'>document.location='notif.php';</script>";
                            }
                            else{
                                echo "<script>System.showToast('Gagal melakukan top up');history.go(-2);</script>";
                            }
                        }
                        else{
                            $min = number_format($mm[2]);
                            echo "<script>System.showToast('Maximum saldo e-Money adalah ".$min."');history.go(-2);</script>";
                        }
                    }
                    else{
                        echo "<script>System.showToast('Gagal melakukan transaksi');history.go(-2);</script>";
                    }
                }
                else{
                    echo "<script>System.showToast('Saldo tabungan tidak mencukupi');history.go(-2);</script>";
                }
            }
            else{
                echo "<script>System.showToast('Akun tabungan tidak ditemukan');history.go(-2);</script>";
            }
        }
        else
        {
            echo "<script>System.showToast('PIN tidak benar. Transaksi tidak dapat dilakukan');history.go(-2);</script>";
        }
    }
    else{
        echo "<script language='javascript'>document.location='login.php';</script>";
    }
}
?>

<?php require('footer.php');?>
