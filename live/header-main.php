<?php
session_start();
error_reporting(0);
include("conf/conf.inc");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baronang</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Google -->
    <meta name="google-site-verification" content="myMQsdKAV6knfGnFRzsZgh1DGvZ8VNDsX65hf60JEm4" />
    <!-- ICON -->
    <link href="static/images/Logo-fish-icon.png" rel="shortcut icon">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="static/plugins/morris/morris.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="static/plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="static/plugins/daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="static/plugins/iCheck/all.css">
    <!-- Bootstrap date Picker -->
    <link rel="stylesheet" href="static/plugins/datepicker/datepicker3.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="static/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="static/plugins/select2/select2.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">

    <!-- jQuery 2.1.4 -->
    <script src="static/plugins/jQuery/jQuery-2.1.4.min.js"></script>

</head>
<body class="hold-transition skin-blue layout-top-nav fixed">
<?php
if(isset($_SESSION['error-time'])){
    if($_SESSION['error-time'] <= time()){
        unset($_SESSION['error-time']);
        unset($_SESSION['error-type']);
        unset($_SESSION['error-message']);
    }
}
?>
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top" style="padding-left: 10%;background: #0a3177;">
            <a href="#">
                <img src ="static/images/Logo_White.png" class="img-responsive">
            </a>

            <div class="navbar-custom-menu">

            </div>
        </nav>
    </header>

    <div class="content-wrapper">
        <section class="content">
