<?php require('header.php');?>

<?php include("connect.inc");?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary col-md-6">
            <div class="box-header with-border">
                <h3 class="box-title">Registration</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>User ID Baronang Pay</th>
                                <th>Cooperate ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Status</th>
                            </tr>
                            <?php
                            $a = "select* from [dbo].[UserRegister]";
                            $b = sqlsrv_query($conn, $a);
                            while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?php echo $c[0]; ?></td>
                                <td><?php echo $c[4]; ?></td>
                                <td><?php echo $c[5]; ?></td>
                                <td><?php echo $c[2]; ?></td>
                                <td><?php echo $c[3]; ?></td>
                                <td><?php echo $c[6]; ?></td>
                                <td>
                                    <?php
                                    if($c[9] == 0){
                                        echo 'Tidak Aktif';
                                    }
                                    else{
                                        echo 'Aktif';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>