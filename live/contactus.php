<?php require('header-login.php');?>

<div class="content-wrapper">
<section class="content">
<div class="row" >
<section class="col-lg-6 connectedSortable">

	<div class="col-sm-12" style="padding-left: 50px">
		<label class="col-sm-12 control-label" style="font-size: 30px; color: #375ce4;">Hubungi Kami!</label>
		<label class="col-sm-12 control-label" style="font-size: 15px;">Plaza Sentra Indoraya Blok A No.3 Jl. Imam Bonjol</label>
		<label class="col-sm-12 control-label" style="font-size: 15px;">Phone :(021) 5533507</label>
		<label class="col-sm-12 control-label" style="font-size: 15px;">Fax   :(021) 5533508</label>
		<label class="col-sm-12 control-label" style="font-size: 15px;">Email :info@baronang.com</label>		
	</div>

<div class="col-sm-12" style="padding-left: 50px">
<br>
		<label class="col-sm-12 control-label" style="font-size: 25px; color: #375ce4;">Atau tinggalkan pesan saja!</label>
		<div class="form-group">
                  <label for="Nama" class="col-sm-12 control-label" >Nama</label>

                  <div class="col-sm-8">
                    <input type="email" class="form-control input-lg input-lg" id="Nama" placeholder="Nama">
                  </div>
        </div>
		<div class="form-group">
                  <label for="email" class="col-sm-12 control-label">E-mail</label>

                  <div class="col-sm-8">
                    <input type="email" class="form-control input-lg input-lg" id="email" placeholder="E-mail">
                  </div>
        </div>
		<div class="form-group">
                  <label for="pesan" class="col-sm-12 control-label">Pesan</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" placeholder="Pesan"></textarea>
                  </div>
        </div>


                <div class="col-sm-3">
                    <button type="button" class="btn btn-block btn-primary btn-sm">Kirim</button>
                </div>
		
</div>
</section>

<section class="col-lg-6 connectedSortable">
<div id="googleMap" style="width:100%;height:400px;"></div>

<script>
function myMap() {
var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
};
var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCP5zm1KYZBUQsm28_d86PspIheF4QXHH0&callback=myMap"></script>






</section>

</div>
</section>
</div>

<?php require('footer-login.php');?>