<?php require('header-login.php');?>

<?php
if($_SESSION['Username'] != ''){
    echo "<script language='javascript'>document.location='dashboard.php';</script>";
}
?>
<div class="login-box">
    <div class="login-logo">
        <a href="" class="logo">
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src = "static/images/Logo_Black.png"></span>
        </a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form action="cek_login.php" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="name" class="form-control" placeholder="Username">
                <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-flat btn-primary btn-block">Sign In</button>
            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<?php require('footer-login.php');?>
