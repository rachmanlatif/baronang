<?php
session_start();
include("conf/conf.inc");
include("lib/encrDecr/ed.php");

$u = decr($uid, $key);
$p = decr($pwd, $key);
$d = decr($databaseName, $key);
$s = decr($serverName, $key);

$connectionInfo = array( "UID"=>$u,
    "PWD"=>$p,
    "Database"=>$d);

$conn = sqlsrv_connect($s, $connectionInfo);

function messageAlert($message, $type){
    $_SESSION['error-message'] = $message;
    $_SESSION['error-type'] = $type;
    $_SESSION['error-time'] = time()+5;
}

?>