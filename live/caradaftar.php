<?php require('header-login.php');?>

    <div class="row">
    <section class="content">
    <div class="row" style="background-image: url(static/images/bg.png); -webkit-background-size:cover ;background-size:cover ;margin-top: 40px;">
            <section class="col-lg-12 connectedSortable">
                <div class="row">
                    <section id="menu">
                        <div class="form-group">
                            <br>
                            <br>
                            <div class="col-sm-12 col-xs-12 text-center">
                                <label class="col-sm-12 control-label" style="font-size: 30px; color: white;">Bingung cara pendaftaran?</label>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                    </section>


                    <div style="padding-bottom: 150px;padding-top: 50px">
                        <div class="col-sm-6 col-xs-6" >
                            <a href="#baronangapp">
                                <img class="img-responsive" src="static/images/baronangapp.png" width="200px" alt="Photo" style="margin-left: auto; margin-right: auto; display:block;">
                            </a>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <a href="#baronangweb">
                                <img class="img-responsive" src="static/images/baronangweb.png" width="200px" alt="Photo" style="margin-left: auto; margin-right: auto; display:block;">
                            </a>
                        </div>
                    </div>
                </div>

            </section>
    </div>
    <div class="row">
    <div class="row">
    <section id="baronangapp">
        <img class="img-responsive" src="static/images/baronangapp.png" alt="Photo" style="margin-left: 50px; margin-top:50px;width: 200px;">
        <div class="row">
            <div class="box-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false" style="text-align: justify;">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="5" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="6" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <section class="col-lg-1 col-xs-1 connectedSortable">
                            </section>
                            <section class="col-lg-7 col-xs-12 connectedSortable">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">1. Buka applikasi Baronang App di Handphone Android Anda!</label>
                            </section>
                            <section class="col-lg-4 col-xs-4 connectedSortable">
                                <img class="img-responsive" src="static/images/howto1.jpg" width="250px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                            </section>
                            <div class="carousel-caption">
                                First Slide
                            </div>
                        </div>
                        <div class="item">
                            <section class="col-lg-1 col-xs-1 connectedSortable">
                            </section>
                            <section class="col-lg-7 col-xs-5 connectedSortable">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">2. Pilih Daftar</label>
                            </section>
                            <section class="col-lg-4 col-xs-4 connectedSortable">
                                <img class="img-responsive" src="static/images/howto2.jpg" width="250px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                            </section>
                            <div class="carousel-caption">
                                Second Slide
                            </div>
                        </div>
                        <div class="item">
                            <section class="col-lg-1 col-xs-1 connectedSortable">
                            </section>
                            <section class="col-lg-4 col-xs-4 connectedSortable">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px;">3. Isilah beberapa data penting yang di minta untuk pendaftaran akun Baronang App seperti contoh di samping ini.
                                    Setelah selesai, pilih pilihan daftar!</label>
                            </section>
                            <section class="col-lg-3 col-xs-3 connectedSortable">
                                <img class="img-responsive" src="static/images/howto3.png" width="250px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                            </section>
                            <section class="col-lg-3 col-xs-3 connectedSortable">
                                <img class="img-responsive" src="static/images/howto4.png" width="250px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                            </section>
                            <div class="carousel-caption">
                                Third Slide
                            </div>
                        </div>
                        <div class="item">
                            <section class="col-lg-1 connectedSortable">
                            </section>
                            <section class="col-lg-7 connectedSortable">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">4. Jika pendaftaran berhasil, Baronang App akan menunjukkan tampilan layar seperti di samping ini.
                                    Dan mohon cek E-mail Anda untuk konfirmasi E-mail yang Anda daftarkan kepada Baronang App!</label>
                            </section>
                            <section class="col-lg-4 connectedSortable">
                                <img class="img-responsive" src="static/images/howto5.jpg" width="250px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                            </section>
                            <div class="carousel-caption">
                                Fourth Slide
                            </div>
                        </div>
                        <div class="item">
                            <section class="col-lg-1 connectedSortable">
                            </section>
                            <section class="col-lg-4 connectedSortable">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">5. Buka E-mail Anda dan pilih konfirmasi pada E-mail yang sudah dikirimkan ke E-mail Anda</label>
                            </section>
                            <section class="col-lg-7 connectedSortable">
                                <img class="img-responsive" src="static/images/howto6.jpg" width="700px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                                <img class="img-responsive" src="static/images/howto7.jpg" width="700px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                            </section>
                            <div class="carousel-caption">
                                Fifth Slide
                            </div>
                        </div>
                        <div class="item">
                            <section class="col-lg-1 connectedSortable">
                            </section>
                            <section class="col-lg-7 connectedSortable">
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">6. Setelah konfirmasi E-mail Anda, Anda siap untuk
                                    login ke Baronang App! Silahkan masukkan E-mail dan Password yang telah terdaftar di halaman awal Baronang App!</label>
                            </section>
                            <section class="col-lg-4 connectedSortable">
                                <img class="img-responsive" src="static/images/howto8.png" width="250px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                            </section>
                            <div class="carousel-caption">
                                Sixth Slide
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <div class="row">
    <section id="baronangweb">
    <img class="img-responsive" src="static/images/baronangweb.png" width="200px" alt="Photo" style="margin-left: 50px; margin-top:50px;margin-right: 50px">
    <div class="box-body">
        <div id="carousel-example-generic2" class="carousel slide" data-ride="carousel" data-interval="false" style="text-align: justify;">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic2" data-slide-to="1" class="active"></li>
                <li data-target="#carousel-example-generic2" data-slide-to="2" class=""></li>
                <li data-target="#carousel-example-generic2" data-slide-to="3" class=""></li>
                <li data-target="#carousel-example-generic2" data-slide-to="4" class=""></li>
                <li data-target="#carousel-example-generic2" data-slide-to="5" class=""></li>
                <li data-target="#carousel-example-generic2" data-slide-to="6" class=""></li>
                <li data-target="#carousel-example-generic2" data-slide-to="7" class=""></li>
                <li data-target="#carousel-example-generic2" data-slide-to="8" class=""></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <section class="col-lg-1 connectedSortable">
                    </section>
                    <section class="col-lg-4 connectedSortable">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">1. Anda membutuhkan Nomor anggota Baronang App sebelum Anda dapat daftar untuk Baronang Web!
                            Nomor Anggota dapat di dapat dengan contoh di samping ini:</label>
                    </section>
                    <section class="col-lg-3 connectedSortable">
                        <img class="img-responsive" src="static/images/howto9.jpg" width="250px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                    </section>
                    <section class="col-lg-3 connectedSortable">
                        <img class="img-responsive" src="static/images/howto10.jpg" width="250px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                    </section>
                    <div class="carousel-caption">
                        First Slide
                    </div>
                </div>
                <div class="item">
                    <section class="col-lg-1 connectedSortable">
                    </section>
                    <section class="col-lg-4 connectedSortable">
                        <br>
                        <br>
                        <br>
                        <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">2. Bukalah web browser dan pergi ke halaman web www.baronang.com untuk melanjutkan!</label>
                    </section>
                    <section class="col-lg-6 connectedSortable">
                        <img class="img-responsive" src="static/images/howto11.jpg" width="700px" alt="Photo" style="margin-left: 50px; margin-top:50px;margin-bottom:50px;">
                    </section>
                    <div class="carousel-caption">
                        Second Slide
                    </div>
                </div>
                <div class="item">
                    <section class="col-lg-1 connectedSortable">
                    </section>
                    <section class="col-lg-4 connectedSortable">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">3. Klik pada pilihan registrasi koperasi seperti contoh di samping ini:</label>
                    </section>
                    <section class="col-lg-7 connectedSortable">
                        <img class="img-responsive" src="static/images/howto12.jpg" width="700px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                    </section>
                    <div class="carousel-caption">
                        Third Slide
                    </div>
                </div>
                <div class="item">
                    <section class="col-lg-1 connectedSortable">
                    </section>
                    <section class="col-lg-6 connectedSortable">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">4. Isilah beberapa data penting yang di minta untuk pendaftaran akun Baronang Web seperti contoh di samping ini.
                            Masukkan juga nomor Anggota Baronang App yang sudah di dapat di petunjuk nomor 1 diatas. Setelah selesai, pilih pilihan daftar!</label>
                    </section>
                    <section class="col-lg-4 connectedSortable">
                        <img class="img-responsive" src="static/images/howto13.jpg" width="275px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                    </section>
                    <div class="carousel-caption">
                        Fourth Slide
                    </div>
                </div>
                <div class="item">
                    <section class="col-lg-1 connectedSortable">
                    </section>
                    <section class="col-lg-5 connectedSortable">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">5. Jika pendaftaran berhasil, Anda akan melihat gambar seperti disamping ini! Lalu cek kembali
                            E-mail yang terdaftar di Baronang Web! </label>
                    </section>
                    <section class="col-lg-5 connectedSortable">
                        <img class="img-responsive" src="static/images/howto14.png" width="350" alt="Photo" style="margin-left: 50px; margin-top:50px; margin-bottom:50px;">
                    </section>
                    <div class="carousel-caption">
                        Fifth Slide
                    </div>
                </div>
                <div class="item">
                    <section class="col-lg-1 connectedSortable">
                    </section>
                    <section class="col-lg-4 connectedSortable">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">6. Buka E-mail Anda dan pilih konfirmasi pada E-mail yang sudah dikirimkan ke E-mail Anda</label>
                    </section>
                    <section class="col-lg-7 connectedSortable">
                        <img class="img-responsive" src="static/images/howto15.jpg" width="700px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                        <img class="img-responsive" src="static/images/howto16.jpg" width="700px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                    </section>
                    <div class="carousel-caption">
                        Sixth Slide
                    </div>
                </div>
                <div class="item">
                    <section class="col-lg-1 connectedSortable">
                    </section>
                    <section class="col-lg-5 connectedSortable">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">7. Setelah Anda klik konfirmasi, Anda akan di kirimkan lagi E-mail dengan isi Kode Koperasi yang
                            dibutuhkan untuk login Baronang Web</label>
                    </section>
                    <section class="col-lg-5 connectedSortable">
                        <img class="img-responsive" src="static/images/howto17.jpg" width="400px" alt="Photo" style="margin-left: 50px; margin-top:50px; margin-bottom:50px;">
                    </section>
                    <div class="carousel-caption">
                        Seventh Slide
                    </div>
                </div>
                <div class="item">
                    <section class="col-lg-1 connectedSortable">
                    </section>
                    <section class="col-lg-6 connectedSortable">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <label class="col-sm-12 control-label" style="font-size: 20px; color: #375ce4;padding-left: 50px">8. Selamat! Anda sudah dapat login kedalam Baronang Web! Silahkan masukkan Kode Koperasi, E-mail dan Password yang anda gunakan!</label>
                    </section>
                    <section class="col-lg-4 connectedSortable">
                        <img class="img-responsive" src="static/images/howto18.png" width="275px" alt="Photo" style="margin-left: 50px; margin-top:50px;">
                    </section>
                    <div class="carousel-caption">
                        Eight Slide
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic2" data-slide="prev">
                <span class="fa fa-angle-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic2" data-slide="next">
                <span class="fa fa-angle-right"></span>
            </a>
        </div>
    </div>
    </section>
    </div>
    </div>
    </section>
    </div>

<?php require('footer-login.php');?>