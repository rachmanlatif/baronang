<?php require('header-login.php');?>

    <div class="row">
    <section class="content">
        <div class="row" style="background: white;">
            <section class="col-lg-5">
                <div class="row">
                    <div class="form-group">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="col-sm-12 col-xs-12">
                            <label class="col-sm-14 control-label" style="font-size: 30px; color: #375ce4;padding-left: 50px">Download Baronang App Sekarang
                                dan dapatkan manfaatnya! </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 col-xs-12">
                            <label class="col-sm-14 control-label" style="font-size: 15px; color: black;padding-left: 50px">Baronang App merupakan aplikasi yang membantu Anda
                                mengakses rekening koperasi Anda. </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-xs-4" style="padding-left: 65px">
                            <br>
                            <a href="https://play.google.com/store/apps/details?id=com.baronang.koperasibaronang&hl=en" target="_blank">
                                <img class="img-responsive" src="static/images/googleplay.png" alt="Photo">
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <section class="col-lg-7 col-xs-12">
                <br>
                <br>
                <br>
                <br>
                <div class="col-sm-9 col-xs-9">
                    <img class="img-responsive" src="static/images/laptop.png" alt="Photo">
                </div>
                <div class="col-sm-3 col-xs-3">
                    <img class="img-responsive" src="static/images/phone4.png" alt="Photo">
                </div>
                <br>
                <br>
                <br>
                <br>
            </section>
        </div>

        <div class="row" style="background-image: url(static/images/bg.png); -webkit-background-size:cover ;background-size:cover ;">
            <div class="box-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
                        <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <br>
                            <br>
                            <section class="col-lg-3 col-xs-3 connectedSortable">

                            </section>
                            <section class="col-lg-3 col-xs-3 connectedSortable">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-7 ">
                                            <img class="img-responsive" src="static/images/phone5.png" alt="Photo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </section>
                            <section class="col-lg-6 col-xs-6 connectedSortable">
                                <div class="row" style="margin-top:100px; margin-bottom: 100px;">
                                    <label class="col-sm-9 col-xs-12 control-label" style="text-align: left; font-size: 22px; color: white;">1.	Anggota dapat melihat dan mengelola Simpanan, Pinjaman dan transaksi lainnya di aplikasi Baronang.</label>
                                </div>
                            </section>
                            <div class="carousel-caption">
                                First Slide
                            </div>
                        </div>
                        <div class="item">
                            <br>
                            <br>
                            <section class="col-lg-3 col-xs-3 connectedSortable">

                            </section>
                            <section class="col-lg-3 col-xs-3 connectedSortable">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <img class="img-responsive" src="static/images/phone6.png" alt="Photo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </section>
                            <section class="col-lg-6 col-xs-6 connectedSortable">
                                <div class="row" style="margin-top:100px; margin-bottom: 100px;">
                                    <label class="col-sm-9 control-label" style="text-align: left; font-size: 22px; color: white;">2. Pembukaan rekening, Pengajuan pinjaman dan  Aktifitas keuangan lainnya dapat dilakukan secara online</label>
                                </div>
                            </section>

                            <div class="carousel-caption">
                                Second Slide
                            </div>
                        </div>
                        <div class="item">
                            <br>
                            <br>
                            <section class="col-lg-3 col-xs-3 connectedSortable">

                            </section>
                            <section class="col-lg-3 col-xs-3 connectedSortable">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <img class="img-responsive" src="static/images/phone7.png" alt="Photo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </section>
                            <section class="col-lg-6 col-xs-6 connectedSortable">
                                <div class="row" style="margin-top:100px; margin-bottom: 100px;">
                                    <label class="col-sm-9 control-label" style="text-align: left; font-size: 22px; color: white;">3. Fungsi pembayaran dan pembelian online (Listrik, telepon, air, dll)*</label>
                                    <label class="col-sm-9 control-label" style="text-align: left; font-size: 15px; color: white;">*Dalam development</label>
                                </div>
                            </section>

                            <div class="carousel-caption">
                                Third Slide
                            </div>
                        </div>

                        <div class="item">
                            <br>
                            <br>
                            <section class="col-lg-3 col-xs-3 connectedSortable">

                            </section>
                            <section class="col-lg-3 col-xs-3 connectedSortable">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-7">
                                            <img class="img-responsive" src="static/images/phone4.png" alt="Photo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </section>
                            <section class="col-lg-6 col-xs-6 connectedSortable">
                                <div class="row" style="margin-top:100px; margin-bottom: 100px;">
                                    <label class="col-sm-9 control-label" style="text-align: left; font-size: 22px; color: white;">4. Tampilan yang interaktif dan menarik.</label>
                                </div>
                            </section>

                            <div class="carousel-caption">
                                Fourth Slide
                            </div>
                        </div>

                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="row" style="background-image: url(static/images/bgwhite.png); -webkit-background-size:cover ;background-size:cover ;">
            <div class="box-body">
                <div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic2" data-slide-to="1" class="active"></li>
                        <li data-target="#carousel-example-generic2" data-slide-to="2" class=""></li>
                        <li data-target="#carousel-example-generic2" data-slide-to="3" class=""></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <br>
                            <br>
                            <section class="col-lg-1 col-xs-1 connectedSortable">

                            </section>

                            <section class="col-lg-4 col-xs-4 connectedSortable">
                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <label class="col-sm-9 col-xs-12 control-label" style="text-align: left; font-size: 22px; color: #375ce4;">1. Digitalisasi data secara online dan real time untuk:</label>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <ul style="text-align: left; font-size: 15px; color: #375ce4;">
                                        <li>Simpanan wajib dan simpanan pokok</li>
                                        <li>Tabungan dan tabungan berjangka</li>
                                        <li>Pinjaman </li>
                                        <li>Kredit belanja </li>
                                        <li>Laporan keuangan </li>
                                    </ul>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                            </section>

                            <section class="col-lg-6 col-xs-6 connectedSortable">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <img class="img-responsive" src="static/images/laptop1.png" alt="Photo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </section>

                            <div class="carousel-caption">
                                First Slide
                            </div>
                        </div>
                        <div class="item">
                            <br>
                            <br>
                            <section class="col-lg-1 col-xs-1 connectedSortable">

                            </section>

                            <section class="col-lg-4 col-xs-6 connectedSortable">
                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <label class="col-sm-9 col-xs-12 control-label" style="text-align: left; font-size: 22px; color: #375ce4;">2. Penghitungan SHU secara otomatis*</label>
                                    <label class="col-sm-9 col-xs-12 control-label" style="text-align: left; font-size: 15px; color: #375ce4;">Baronang di program untuk dapat dengan mudah menyajikan laporan dengan otomatis.</label>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                            </section>

                            <section class="col-lg-6 col-xs-5 connectedSortable">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-12 ">
                                            <img class="img-responsive" src="static/images/laptop4.png" alt="Photo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </section>

                            <div class="carousel-caption">
                                Second Slide
                            </div>
                        </div>
                        <div class="item">
                            <br>
                            <br>
                            <section class="col-lg-1 col-xs-1 connectedSortable">

                            </section>

                            <section class="col-lg-4 col-xs-6 connectedSortable">
                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <label class="col-sm-9 col-xs-12 control-label" style="text-align: left; font-size: 22px; color: #375ce4;">3. Laporan penagihan tersajikan secara otomatis.</label>
                                    <label class="col-sm-9 col-xs-12 control-label" style="text-align: left; font-size: 15px; color: #375ce4;">Baronang di program untuk dapat dengan mudah menyajikan laporan dengan otomatis.</label>
                                </div>
                                <br>
                                <br>
                                <br>
                                <br>
                            </section>

                            <section class="col-lg-6 col-xs-5 connectedSortable">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-12 ">
                                            <img class="img-responsive" src="static/images/laptop3.png" alt="Photo">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                            </section>

                            <div class="carousel-caption">
                                Third Slide
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic2" data-slide="prev">
                        <span class="fa fa-angle-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic2" data-slide="next">
                        <span class="fa fa-angle-right"></span>
                    </a>
                </div>
            </div>
        </div>

    </section>
    </div>

<?php require('footer-login.php');?>