<?php require('header.php');?>

<?php include("connect.inc");?>

<div class="row">
    <div class="col-sm-12">
        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <?php if(isset($_GET['add'])){ ?>
            <?php
            $x = "select* from [dbo].[ListKoperasi] where KID = '$_GET[add]' and Status = 1";
            $y = sqlsrv_query($conn, $x);
            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
            if($z == null){
                $_SESSION['error-message'] = 'Koperasi tidak ditemukan';
                $_SESSION['error-type'] = 'warning';
                $_SESSION['error-time'] = time()+5;
                echo "<script>window.location.href='edc.php'</script>";
            }
            else{
            ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah EDC</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="procedc.php?kid=<?php echo $_GET['add']; ?>" method="post">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="text-align: left;">KBA</label>
                                    <div class="col-sm-6">
                                        <select name="kba" class="form-control">
                                            <?php
                                            $sql = "select* from $z[3].[dbo].[BankAccount] where KID = '$_GET[add]'";
                                            $exec = sqlsrv_query($conn, $sql);
                                            while($row = sqlsrv_fetch_array($exec, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                                <option value="<?php echo $row[1]; ?>"><?php echo $row[3].' - '.$row[4]; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="lead">Daftar EDC</span>
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Serial Number</th>
                                    <th>KBA</th>
                                    <th>Status</th>
                                    <th>Private Key</th>
                                    <th></th>
                                </tr>
                                <?php
                                $xx = "select* from [Gateway].[dbo].[EDCList] where KID = '$_GET[add]'";
                                $yy = sqlsrv_query($conn, $xx);
                                while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                    <tr>
                                        <td><?php echo $zz[0]; ?></td>
                                        <td><?php echo $zz[2]; ?></td>
                                        <td><?php echo $zz[3]; ?></td>
                                        <td><?php echo $zz[4]; ?></td>
                                        <td>
                                            <a href="procedc.php?send=<?php echo $zz[0]; ?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-reply"></i> Send Email</button></a>
                                            <a href="procedc.php?change=<?php echo $zz[0]; ?>" onclick="return confirm('Apakah anda yakin mengubah private key ?')"><button type="button" class="btn btn-info btn-xs"><i class="fa fa-random"></i> Change Key</button></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
        } ?>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Daftar Koperasi</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>User ID Baronang Pay</th>
                                <th>Cooperate ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            <?php
                            $a = "select* from [dbo].[UserRegister]";
                            $b = sqlsrv_query($conn, $a);
                            while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?php echo $c[0]; ?></td>
                                <td><?php echo $c[4]; ?></td>
                                <td><?php echo $c[5]; ?></td>
                                <td><?php echo $c[2]; ?></td>
                                <td><?php echo $c[3]; ?></td>
                                <td><?php echo $c[6]; ?></td>
                                <td>
                                    <?php
                                    if($c[9] == 0){
                                        echo 'Tidak Aktif';
                                    }
                                    else{
                                        echo 'Aktif';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="?add=<?php echo $c[4] ?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add</button> </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>