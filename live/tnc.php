<?php require('header-login.php');?>

<div class="content-wrapper">
<section class="content">
<div class="row" >
<div class="content-wrapper">
<div class="content-header">
          <h1  style="font-size: 35px; color: #375ce4;padding-left: 30px;">
            Terms & Condition Baronang
			
          </h1>
		  <h3 class="page-header" style="font-size: 18px; padding-left: 30px;">
            Syarat & ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh Baronang.
          </h3>
</div>
<div class="content body" style="font-size: 17px; padding-left: 50px;padding-right: 50px" align="justify" >
  <h2 style="color: #375ce4;">Selamat datang di Baronang App!</h2>
  
  <p  >
    Terima Kasih Anda telah mengunduh Baronang App. Baronang App adalah layanan aplikasi sebagaimana dimaksud dalam Syarat dan Ketentuan ini. 
	Pada saat anda mengunduh Aplikasi, Anda mengakui dan menyetujui bahwa Anda telah membaca dengan teliti, memahami, menerima dan menyetujui 
	seluruh Syarat dan Ketentuan yang akan berlaku sebagai perjanjian antara Anda dengan PT Baronang Asia Pasifik, 
	beserta rekanan PT Baronang Asia Pasifik yang tergabung dalam Layanan Aplikasi ini.
  </p>
  <p  >
  MOHON ANDA MEMBACA DAN MEMAHAMI SYARAT DAN KETENTUAN KAMI DENGAN SEKSAMA SEBELUM MENGUNDUH APLIKASI ATAU MENGGUNAKAN 
  LAYANAN KAMI UNTUK PERTAMA KALI-NYA.
  </p>
  <p  >
  Layanan yang tersedia untuk Anda pada Baronang App sangat-lah beragam, sehingga kami harus 
  mencantumkan atau memberlakukan Syarat dan Ketentuan tambahan untuk keperluan Layanan tertentu, 
  dimana persyaratan tambahan tersebut merupakan bagian dari Layanan kami 
  dan akan tunduk dan terikat dengan Syarat dan Ketentuan ini.
  </p>
  <p  >
  Kami berhak untuk setiap waktu mengubah, menambah, mengurangi, mengganti, menyesuaikan, dan/atau memodifikasi 
  Syarat dan Ketentuan (baik sebagian ataupun seluruhnya). Untuk itu Anda diwajibkan untuk dari waktu ke waktu membaca 
  Syarat dan Ketentuan yang kami sediakan dalam website tersebut diatas atau melalui Aplikasi.
  </p>
  <p>
  Syarat dan Ketentuan ini berlaku terhadap Anda maupun atas Transaksi atau atas penggunaan Layanan oleh atau melalui Akun Anda dimanapun 
  Anda berada, baik di wilayah Republik Indonesia maupun di wilayah Negara lainnya.
  </p>
  <ul>
  <li><a href="#definisi">Definisi </a></li>
  <li><a href="#layanan">Layanan Baronang </a></li>
  <li><a href="#panduan">Panduan Singkat </a></li>
  <li><a href="#akun">Pendaftaran Akun</a></li>
  <li><a href="#kebijakan">Kebijakan Privasi </li>
  <li><a href="#pengumpulan">Pengumpulan Informasi Personal </a></li>
  <li><a href="#pengungkapan">Pengungkapan kepada Pihak Ketiga </a></li>
  <li><a href="#keamanan">Keamanan Informasi Pribadi </a></li>
  <li><a href="#perubahan">Perubahan Dalam Kebijakan Privasi </a></li>
  <li><a href="#kewajiban">Kewajiban Pernyataan Dan Jaminan </a></li>
  <li><a href="#pembatasan">Pembatasan Tanggung Jawab </a></li>
  <li><a href="#hak">Hak Kekayaan Intelektual</a></li>
  <li><a href="#masaberlaku">Masa Berlaku Dan PengAakhiran</a></li>
  <li><a href="#hukum">Hukum Yang Berlaku Dan Penyelesaian Perselisihan</a></li>
  <li><a href="#ketentuanlain">Ketentuan Lain </a></li>
  
  </ul>
  
  
  
  <section id="definisi">
  <h2 class="page-header" style="color: #375ce4;">Definisi</h2>
  <ul  >
    <li><b>“Baronang App”</b> adalah aplikasi ini yang Anda unduh (download) sehingga Anda masuk pada Laman ini atau Syarat dan Ketentuan ini, yang saat ini dikenal dengan merek, nama, logo dan/atau tanda yang dikenal dengan <b>“BARONANG”</b>atau merek, nama, logo dan/atau tanda lainnya.</li>
    <li><b>“Akun”</b> atau <b>“Akun Anda”</b> berarti identifikasi khusus yang dibuat di BARONANG berdasarkan permintaan pendaftaran Anda.</li>
	<li><b>“Data”</b> berarti adalah setiap data, informasi dan/atau keterangan dalam bentuk apapun yang dari waktu ke waktu (termasuk pada saat Anda mengunduh (download) Aplikasi) Anda sampaikan kepada Kami/Penyedia Layanan atau yang Anda cantumkan atau sampaikan dalam, pada atau melalui Aplikasi.</li>
	<li><b>“Kami”</b> adalah PT Baronang Asia Pasifik.</li>
	<li><b>“Layanan”</b> berarti setiap layanan, program, jasa, produk, fitur, sistem, fasilitas dan/atau jasa yang disediakan dan/atau ditawarkan dalam atau melalui Aplikasi.</li>
	<li><b>“Layanan Pelanggan"</b> adalah fungsi customer service center untuk nasabah yang dapat dihubungi lewat email.</li>
	<li><b>“BARONANG”</b> adalah sistem elektronik (platform) yang dibuat oleh PT Baronang Asia Pasifik.</li>
	<li><b>“Syarat dan Ketentuan”</b> berarti Syarat dan Ketentuan ini berikut setiap perubahan, penambahan, penggantian, penyesuaian dan/atau modifikasinya yang dibuat dari waktu ke waktu.</li>
	<li><b>“Transaksi”</b> berarti segala transaksi, kegiatan, aktivitas dan/atau aksi yang dilakukan dalam atau melalui Aplikasi, Akun dan/atau Security Code termasuk penggunaan Layanan atau fitur-fitur tertentu dalam Layanan atau Aplikasi.</li>
	<li><b>"BAP”</b> adalah PT Baronang Asia Pasifik, suatu perseroan terbatas yang didirikan berdasarkan hukum Negara Republik Indonesia. </li>
	
  </ul>
  </section>
  <section id="layanan">
  <h2 class="page-header" style="color: #375ce4;">Layanan Baronang</h2>
  <ol  >
    <li>BARONANG adalah uang elektronik yang diselenggarakan oleh Baronang yang telah terdaftar dan diawasi oleh Bank Indonesia, memiliki fungsi yang sama dengan uang tunai sebagai alat pembayaran yang sah, di mana nilainya setara dengan nilai uang tunai yang disetorkan terlebih dahulu ke rekening BARONANG dan uang yang disetorkan bukanlah bersifat simpanan sebagaimana diatur dalam peraturan perundang-undangan perbankan. Oleh karenanya, BARONANG tidak memberikan bunga serta tidak dijamin oleh Lembaga Penjamin Simpanan. </li>
	<li>Jenis Layanan BARONANG terbagi atas:
	<ul>
	<li>Baronang Standard adalah jenis layanan Baronang yang data identitas pemegangnya tidak terdaftar dan tidak tercatat di Baronang; yang dapat dipergunakan untuk fasilitas layanan sebagai berikut:
	<ul>
	<li>Isi Saldo (Cash In/Top Up) </li>
	<li>Pembayaran Transaksi </li>
	<li>Pembayaran Tagihan </li>
	<li>Fasilitas lain berdasarkan persetujuan Bank Indonesia </li>
	</ul>
	</li>
	<li>Baronang Premium adalah jenis layanan Baronang yang data identitas pemegangnya terdaftar dan tercatat di Baronang; yang dapat dipergunakan untuk fasilitas layanan sebagai berikut:
	<ul>
	<li>Isi Saldo (Cash In/Top Up) </li>
	<li>Pembayaran Transaksi </li>
	<li>Pembayaran Tagihan </li>
	<li>Transfer Dana</li>
	<li>Tarik Tunai </li>
	<li>Penyaluran program bantuan pemerintah kepada masyarakat: dan/atau </li>
	<li>Fasilitas lain berdasarkan persetujuan Bank Indonesia  </li>
	</ul>
	</li>
	</li>
  </ol>	
  </section>
  <section id="panduan">
  <h2 class="page-header" style="color: #375ce4;">Panduan Singkat</h2>
  <ul  >
    <li>Baronang App merupakan aplikasi perangkat lunak dimana seluruh instruksi yang Anda lakukan akan berasal dari mobile aplikasi atau online.</li>
    <li>Baronang App dapat dioperasikan melalui HP Android (OS 4.2 ke atas) melalui Google Play Store.</li>
    <li>Anda akan diminta untuk melakukan otorisasi instruksi dengan menggunakan berbagai jenis informasi keamanan yang berbeda (misalnya Security Code, nama pengguna, password) jika dibutuhkan.</li>
    <li>Setelah melakukan otorisasi, BARONANG akan melaksanakan instruksi sesuai perintah Anda, Anda diwajibkan untuk memastikan tidak memberitahukan informasi keamanan Anda kepada pihak lain.</li>
    <li>BARONANG memastikan kerahasiaan dan keamanan informasi diri yang Anda berikan terjaga dengan baik. Penggunaan data Anda akan kami lakukan sesuai dengan ketentuan yang berlaku.</li>
    <li>Anda dapat menanyakan atau memberi masukkan kepada kami melalui Contact Center Layanan Pengguna BARONANG pada alamat email sebagai berikut [info@Baronang.id].</li>
  </ul>	
  <p  >
  Baronang App menawarkan 2 (dua) jenis klasifikasi pelanggan dengan jenis layanan BARONANG atau fitur-fitur 
  layanan yang berbeda. Klasifikasi pelanggan tersebut adalah:
  </p>
  <ol  >
    <li><b>Baronang App</b></li>
	<p  >
  Baronang App adalah aplikasi untuk nasabah koperasi yang memungkinkan Anda dapat menikmati fasilitas yang disiapkan.
	</p>
	<li><b>Baronang Web</b></li>
	<p  >
  Baronang Web adalah aplikasi untuk management koperasi untuk mempermudah penggunaan sistem koperasi.
	</p>
  </ol>
  </section>
  
 <section id="Akun">
 <h2 class="page-header" style="color: #375ce4;">Pendaftaran Akun</h2>  
  <ul>
  <li><b>Baronang App</b>
  <ol>
  <li>Untuk dapat menggunakan layanan Baronang App, Anda harus terlebih dahulu melakukan pendaftaran akun dengan melalui Baronang App. </li>
  <li>Anda harus mengisi semua data yang dibutuhkan untuk setiap transaksi secara benar dan lengkap. </li>
  <li>Akun yang sudah terdaftar pada Baronang hanya akan aktif sebagai akun Baronang Standard setelah selesai pendaftaran. </li>
  <li>Pemegang akun yang sudah mendaftarkan data identitas dan memberikan dokumen identitas kepada Baronang akan aktif sebagai akun Baronang Premium apabila telah lulus uji tuntas oleh BAP. </li>
  <li>Pemegang akun Baronang Standard dapat melakukan upgrade menjadi akun Baronang Premium dengan cara mendaftarkan data pribadi dan memberikan dokumen identitas Pemegang BARONANG kepada BAP sebagaimana dipersyaratkan berdasarkan peraturan perundang-undangan yang berlaku dan lulus proses uji tuntas yang dilakukan oleh BAP.</li>
  <li>Baronang berhak atas pertimbangannya sendiri karena alasan apapun, untuk melakukan pembatalan dan/atau penolakan proses upgrade dari Baronang Standard menjadi Baronang Premium.</li>
  <li>Pemegang Akun Baronang membebaskan Baronang dari segala tuntutan dalam bentuk apapun dari pihak ketiga manapun termasuk suami/istri/ahli waris Pemegang sehubungan dengan proses pembatalan upgrade dari Baronang Standard menjadi Baronang Premium. </li>
  </ol>
  </li>
  <li><b>Baronang Web</b>
  <ol>
  <li>Untuk dapat menggunakan layanan Baronang Web, Anda harus terlebih dahulu melakukan pendaftaran akun dengan melalui website Baronang di www.baronang.com.</li>
  </ol>
  </li>
  </ul>
  </section>
  
  <section id="kebijakan">
  <h2 class="page-header" style="color: #375ce4;">Kebijakan Privasi</h2>  
  <p>
  BARONANG menghormati hal-hal yang berkenaan dengan perlindungan data dan privasi Anda.
  </p>
  <p  >
  Oleh karena itu, Kami hanya akan menggunakan nama Anda dan 
  data lainnya yang berhubungan dengan Anda sesuai dengan kebijakan privasi ini.
  </p> 
  <p  >
  Kami hanya mengumpulkan Data yang penting bagi Kami dan Kami hanya akan mengumpulkan Data yang dibutuhkan untuk segala sesuatu yang berhubungan dengan Anda. Kami hanya akan menyimpan Data Anda selama dibutuhkan untuk memenuhi kewajiban hukum atau selama Data tersebut berhubungan dengan tujuan-tujuan yang ada saat Data dikumpulkan. Kebijakan privasi Kami mengikuti kebijakan perundang-undangan yang berlaku untuk menjaga kerahasiaan data kecuali kami diwajibkan oleh hukum yang berlaku untuk membuka data kepada pihak ketiga yang memiliki kewenangan 
  seperti pemerintah atau lembaga pemerintah lainnya, hanya jika ada perintah yang sah.
  </p> 
  </section>
  <section id="pengumpulan">
  <h2 class="page-header" style="color: #375ce4;">Pengumpulan Informasi Personal</h2>  
  <p  >
  Data pribadi adalah data yang dapat digunakan untuk mengenali atau menghubungi seseorang. Saat Anda membuat Akun pengguna BARONANG, Anda akan diminta untuk 
  memberikan Data pribadi yang kami butuhkan termasuk namun tidak terbatas pada:
  <ol>
  <li>Nama Lengkap </li>
  <li>Alamat Surel (E-mail) </li>
  <li>Nomor Telepon Genggam </li>
  <li>Tanggal Lahir </li>
  <li>Nomor Kartu Tanda Penduduk </li>
  </ol>
  </p>
  
  <p  >
  Data yang kami peroleh langsung dari Anda melalui pengisian di Baronang App. Data yang dikumpulkan ketika Anda menggunakan Baronang App yang mungkin Kami gunakan untuk mengidentifikasi Anda dan mengesahkan pengguna Baronang App dengan tujuan pencegahan kehilangan dan penipuan.
  </p> 
  <p  >
  Lebih dari itu, Kami akan menggunakan Data yang Anda berikan untuk urusan administrasi Akun Anda dengan Kami untuk keperluan verifikasi dan mengelola transaksi yang berhubungan dengan Baronang App, melakukan riset mengenai data demografis pengguna Baronang App, mengembangkan Baronang App, mengirimkan Anda informasi yang Kami anggap berguna untuk Anda termasuk informasi tentang layanan dari Kami setelah Anda memberikan persetujuan kepada Kami bahwa Anda tidak keberatan dihubungi mengenai layanan Kami.
  </p> 
  <p  >
  Kami dapat menggunakan informasi pribadi Anda, termasuk tanggal lahir untuk memverifikasi identitas, membantu pengguna, menentukan Layanan yang tepat untuk Anda dan bermanfaat kepada Anda serta untuk memahami bagian yang paling menarik dari Baronang App.
  </p> 
  <p  >
  Transaksi yang Anda lakukan melalui Baronang App diproses oleh PT Baronang Asia Pasifik. Anda harus memberikan Data yang akurat dan tidak menyesatkan kepada Kami. Anda harus memperbaharui dan memberitahukan kepada Kami apabila ada perubahan Data yang terkait dengan Anda. Rincian transaksi Anda akan Kami simpan untuk alasan keamanan Kami. Anda dapat mengakses informasi tersebut dengan cara melakukan masuk ke Akun Anda melalui Baronang App.
  </p> 
  <p  >
  Anda tidak akan membiarkan pihak ketiga untuk dapat mengakses Data pribadi Anda. Kami tidak bertanggung jawab atas penyalahgunaan password dan/atau PIN. Anda wajib memberitahukan kepada Kami apabila Anda yakin bahwa password dan/atau PIN Anda disalahgunakan oleh pihak lain.
  </p>
  </section>
  
  <section id="pembuktian">
  <h2 class="page-header" style="color: #375ce4;">Pembuktian</h2> 
  <ol>
  <li>Setiap instruksi transaksi dari Nasabah yang tersimpan pada pusat data Baronang dalam bentuk apapun, termasuk namun tidak terbatas pada catatan,tape/cartridge, printout komputer/perangkat, komunikasi yang dikirimkan secara elektronik antara Baronang dan Nasabah, merupakan alat bukti yang sah, kecuali Nasabah dapat membuktikan sebaliknya.</li>
  <li>Nasabah menyetujui semua komunikasi dan instruksi dari Nasabah yang diterima oleh Baronang merupakan alat bukti yang sah meskipun tidak dibuat dokumen tertulis ataupun dikeluarkan dokumen yang ditandatangani. </li>
  </ol>
  
  </section>
  <section id="pengungkapan">
  <h2 class="page-header" style="color: #375ce4;">Pengungkapan kepada Pihak Ketiga</h2> 
  
  <p>
  Terkadang Baronang App dapat memberikan informasi pribadi tertentu kepada merchant strategis yang bekerja sama dengan Baronang App untuk menyediakan Layanan. Baronang App tidak akan menyewakan, menjual atau menyebarluaskan Data pribadi Anda kepada orang lain atau perusahaan-perusahaan yang tidak berafiliasi dengan Baronang App.
  </p>
  <p  >
  Kami dapat membagikan informasi pribadi kepada perusahaan induk, afiliasi-afiliasi dan anak perusahaan afiliasi, dan perusahaan-perusahaan yang sudah bekerjasama dengan Kami melalui perjanjian kerahasiaan Kami dan ketika Kami memiliki izin dan persetujuan dari Anda atau keadaan-keadaan yang berkaitan dengan hal-hal berikut ini:
  <ol>
  <li>Baronang App menyediakan informasi kepada rekanan yang Anda percayakan bekerja untuk dan atas nama Baronang App berdasarkan perjanjian-perjanjian kerahasiaan. Perusahaan-perusahaan ini dapat menggunakan identitas pribadi Anda untuk membantu Baronang App berkomunikasi dengan Anda tentang penawaran-penawaran dari Baronang App dan rekanan-rekanan Layanan Kami. Akan tetapi, perusahaan-perusahaan ini tidak berhak untuk memakai informasi dan/atau Data pribadi Anda. </li>
  <li>Kami merespons panggilan dari pengadilan, permintaan pengadilan atau proses hukum atau untuk menerapkan hak-hak hukum Kami atau mempertahankan hak Kami terhadap tuntutan hukum.</li>
  <li>Kami yakin bahwa ini sangatlah penting untuk membagi informasi dan/atau Data dalam rangka penyelidikan, pencegahan, atau pengambilan tindakan terhadap kegiatan yang tidak sah, dugaan penipuan, situasi-situasi yang dapat mengakibatkan keselamatan fisik dari orang lain, pelanggaran atas penggunaan Baronang App dan/atau kewajiban-kewajiban hukum lainnya. </li>
  <li>Baronang App bekerjasama dengan vendor, rekanan-rekanan, dan penyedia jasa lainnya di industri dan kategori bisnis yang berbeda. KEMAMPUAN UNTUK MENGUBAH DAN MENGHAPUS AKUN INFORMASI ANDA Anda dapat mengubah Data pada Akun BARONANG Anda setiap saat. </li>
  </ol>  
  </p>
  </section>
  <section id="keamanan">
  <h2 class="page-header" style="color: #375ce4;">Keamanan Informasi Pribadi</h2>
  <p  >
  Kami memastikan bahwa informasi dan/atau Data yang dikumpulkan akan disimpan dengan aman. Kami menyimpan informasi dan/atau Data pribadi Anda selama diperlukan untuk memenuhi tujuan yang dijelaskan dalam kebijakan privasi ini.
  </p>
  </section>
  <section id="kewajiban">
  <h2 class="page-header" style="color: #375ce4;">Used ID, Password, PIN, dan Kewajiban Nasabah</h2>
  <ol>
  <li>Nasabah wajib mengamankan User ID, PASSWORD dan PIN dengan cara: 
  <ul>
  <li>Tidak memberitahukan User ID, PASSWORD dan PIN kepada orang lain untuk mendapatkan hadiah atau tujuan apapun lainnya termasuk kepada anggota keluarga atau sahabat, kecuali untuk melakukan transaksi-transaksi tertentu yang mengharuskan Nasabah untuk memberitahukan User ID milik Nasabah, antara lain untuk transaksi pembelian barang atau jasa secara online. </li>
  <li>Tidak menuliskan User ID, PASSWORD dan PIN pada meja atau terminal atau menyimpannya dalam bentuk tertulis atau pada aplikasi komputer/perangkat atau sarana penyimpanan lainnya yang memungkinkan User ID dan PIN diketahui oleh orang lain. </li>
  <li>Menggunakan User ID, PASSWORD dan PIN dengan hati-hati agar tidak terlihat oleh orang lain. </li>
  <li>Tidak menggunakan PIN yang diberikan oleh orang lain atau yang mudah diterka seperti tanggal lahir atau kombinasinya, nomor telepon dan lain-lain. </li>
  </ul>
  </li>
  <li>Nasabah diberikan kebebasan untuk membuat PIN-nya sendiri pada saat registrasi  </li>
  <li>Nasabah dapat melakukan inquiry (pengecekan) User ID-nya sendiri melalui menu Lupa User ID di Baronang App, apabila Nasabah tidak mengingat User ID-nya. </li>
  <li>Nasabah bertanggung jawab sepenuhnya terhadap semua instruksi transaksi yang terjadi berdasarkan penggunaan User ID, PASSWORD dan PIN yang dimiliki oleh Nasabah, kecuali Nasabah dapat membuktikan sebaliknya. </li>
  <li>Nasabah wajib untuk segera memberitahukan kepada BAP apabila User ID, PASSWORD dan atau PIN Nasabah telah diketahui orang lain atau berpotensi disalahgunakan. Segala instruksi transaksi berdasarkan penggunaan User ID, PASSWORD dan PIN yang terjadi sebelum pejabat yang berwenang dari BAP menerima secara tertulis laporan tersebut merupakan tanggung jawab sepenuhnya dari Nasabah. </li>
  
  
  </ol>
  </section>
  <section id="email">
  <h2 class="page-header" style="color: #375ce4;">Electronic Mail (E-mail)</h2>
  <ol>
  <li>Alamat E-Mail yang didaftarkan oleh Nasabah pada saat login pertama kali di Baronang App digunakan oleh BAP untuk mengirim informasi transaksi yang telah dilakukan oleh Nasabah melalui Baronang App. </li>
  <li>BAP hanya mengirimkan informasi ke alamat E-Mail yang telah dikonfirmasikan kebenarannya oleh Nasabah kepada BAP dan BAP tidak bertanggung jawab atas kebenaran alamat E-Mail tersebut maupun pengiriman data dan informasi yang ditujukan ke alamat E-Mail tersebut. </li>
  <li>BAP tidak menjamin keamanan informasi atau data yang dikirim kepada BAP melalui E-Mail yang tidak terdapat pada menu Baronang App, yang tidak dalam format yang aman yang disetujui atau ditentukan oleh BAP. </li>
  </ol>
  </section>
  
  
  <section id="perubahan">
  <h2 class="page-header" style="color: #375ce4;">Perubahan Dalam Kebijakan Privasi</h2>
  <p  >
  Baronang App memiliki hak untuk mengubah kebijakan privasi Kami sesuai dengan Syarat & Ketentuan BARONANG dari waktu ke waktu.
  </p>
  </section>
  <section id="kewajiban">
  <h2 class="page-header" style="color: #375ce4;">Kewajiban Pernyataan dan Jaminan</h2>
  <ol  >
  <li>Anda hanya dapat mengakses atau menggunakan Aplikasi, Layanan, dan/atau Sistem (a) sesuai dengan Syarat dan Ketentuan ini, (b) untuk tujuan yang sah, dan (c) tidak digunakan untuk tujuan atau tindakan penipuan, pelanggaran hukum, kriminal maupun tindakan, aktifitas, perbuatan atau tujuan lain yang melanggar atau bertentangan dengan hukum, peraturan perundang-undangan yang berlaku maupun hak atau kepentingan pihak manapun. Anda bertanggung jawab penuh untuk memeriksa dan memastikan bahwa Anda telah mengunduh (download) software yang benar untuk perangkat Anda. Kami tidak bertanggung jawab jika Anda tidak memiliki perangkat yang kompatibel dengan Sistem atau Aplikasi atau jika Anda telah mengunduh (download) versi software yang salah untuk perangkat Anda. </li>
  <li>Anda dilarang untuk menggunakan Layanan, Aplikasi dan/atau Akun atau melakukan Transaksi: (a) untuk tujuan, kegiatan, aktifitas atau aksi yang melanggar hukum atau melanggar hak atau kepentingan (termasuk Hak Kekayaan Intelektual atau hak privasi milik pihak manapun); (b) yang memiliki materi atau unsur yang berbahaya atau yang merugikan pihak manapun; (c) yang mengandung virus software, worm, trojan horses atau kode komputer berbahaya lainnya, file, script, agen atau program; dan (d) yang mengganggu integritas atau kinerja Sistem dan/atau Aplikasi.</li>
  <li>Anda dilarang untuk melakukan tindakan apapun termasuk dalam atau melalui Aplikasi atau Akun serta dilarang untuk melakukan Transaksi yang dapat merusak atau mengganggu reputasi Kami atau Penyedia Layanan.</li>
  <li>Anda dengan ini secara tegas menyetujui serta menyatakan dan menjamin bahwa:
  <ul>
  <li>Anda adalah individu yang secara hukum cakap untuk melakukan tindakan hukum berdasarkan hukum negara Republik Indonesia termasuk untuk mengikatkan diri dalam Syarat dan Ketentuan ini. Jika Anda tidak memenuhi syarat ini, Kami berhak untuk sewaktu-waktu untuk men-nonaktifkan atau mengakhiri penggunaan Akun, Layanan dan/atau Aplikasi, baik untuk sementara waktu maupun untuk seterusnya; </li>
  <li>Anda memiliki hak, wewenang dan kapasitas untuk menggunakan Layanan dan Akun serta untuk melaksanakan seluruh Syarat dan Ketentuan; </li>
  <li>jika Anda melakukan pendaftaran atau mengunduh Aplikasi atas nama suatu badan hukum, persekutuan perdata atau pihak lain, Anda dengan ini menyatakan dan menjamin bahwa Anda memiliki kapasitas, hak dan wewenang yang sah untuk bertindak untuk dan atas nama badan hukum, persekutuan perdata atau pihak lain tersebut termasuk tetapi tidak terbatas pada mengikat badan hukum, persekutuan perdata atau pihak lain tersebut untuk tunduk pada seluruh isi Syarat dan Ketentuan; </li>
  <li>Anda telah membaca seluruh isi Syarat dan Ketentuan serta telah mengerti dan memahami seluruh isi Syarat dan Ketentuan ini dan karenanya mengikatkan diri secara sukarela untuk tunduk dengan serta melaksanakan seluruh Syarat dan Ketentuan ini; </li>
  <li>Anda menyatakan dan menjamin bahwa dana yang dipergunakan dalam rangka transaksi bukan dana yang berasal dari tindak pidana yang dilarang berdasarkan peraturan perundang-undangan yang berlaku di Republik Indonesia, pembukaan rekening ini tidak dimaksudkan dan/atau ditujukan dalam rangka upaya melakukan tidak pidana pencucian uang sesuai dengan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia, transaksi tidak dilakukan untuk maksud mengelabui, mengaburkan, atau menghindari pelaporan kepada Pusat Pelaporan Dan Analisa Transaksi Keuangan (PPATK) berdasarkan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia, dan Anda bertanggung jawab sepenuhnya serta melepaskan BAP dari segala tuntutan, klaim, atau ganti rugi dalam bentuk apapun, apabila Anda melakukan tindak pidana pencucian uang berdasarkan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia; </li>
  <li>Seluruh Data baik yang telah Anda sampaikan atau cantumkan maupun yang akan Anda sampaikan atau cantumkan baik langsung maupun tidak langsung di kemudian hari atau dari waktu ke waktu adalah benar, lengkap, akurat terkini dan tidak menyesatkan serta tidak melanggar hak (termasuk tetapi tidak terbatas pada hak kekayaan intelektual) atau kepentingan pihak manapun. Penyampaian Data oleh Anda kepada Kami atau pada atau melalui Aplikasi atau Sistem tidak bertentangan dengan hukum yang berlaku serta tidak melanggar akta, perjanjian, kontrak, kesepakatan atau dokumen lain dimana Anda merupakan pihak atau dimana Anda atau aset Anda terikat; </li>
  <li>Layanan, Aplikasi maupun Akun akan digunakan untuk kepentingan Anda sendiri atau untuk kepentingan badan hukum, badan usaha, persekutuan perdata atau pihak lain yang secara sah Anda wakili sebagaimana dimaksud huruf c di atas; </li>
  <li>Anda tidak akan memberikan hak, wewenang dan/atau kuasa dalam bentuk apapun dan dalam kondisi apapun kepada orang atau pihak lain untuk menggunakan Data, Akun dan/atau Security Code, dan Anda karena alasan apapun dan dalam kondisi apapun tidak akan dan dilarang untuk mengalihkan Akun kepada orang atau pihak manapun; dan </li>
  <li>dalam atau pada saat menggunakan Layanan, Aplikasi dan/atau Akun, Anda setuju untuk mematuhi dan melaksanakan seluruh ketentuan hukum dan peraturan perundang-undangan yang berlaku termasuk hukum dan peraturan perundang-undangan di negara asal Anda maupun di negara atau kota dimana Anda berada. </li>
  
  </ul>
  </li>
  <li>Dengan melaksanakan transaksi melalui Aplikasi Mobile BARONANG, Anda memahami bahwa seluruh komunikasi dan instruksi dari Anda yang diterima oleh Kami akan diperlakukan sebagai bukti solid meskipun tidak dibuat dalam bentuk dokumen tertulis atau diterbitkan dalam bentuk dokumen yang ditandatangani, dan, dengan demikian, Anda setuju untuk mengganti rugi dan melepaskan Kami dan rekanan-rekanan Kami dari segala kerugian, tanggung jawab, tuntutan dan pengeluaran (termasuk biaya litigasi) yang dapat muncul terkait dengan eksekusi dari instruksi Anda.</li>
  </ol>
  </section>
  <section id="pembatasan">
  <h2 class="page-header" style="color: #375ce4;">Pembatasan Tanggung Jawab</h2>
  
  <ol  >
  <li>Layanan disediakan dalam kondisi “as is” atau “apa adanya”. Kami (dalam beberapa fitur) tidak memberikan pernyataan atau jaminan dalam bentuk apapun atas reliabilitas, keamanan, ketepatan waktu, kualitas, kesesuaian, ketersediaan, akurasi dan/atau kelengkapan Layanan, Aplikasi dan/atau Sistem. Kami tidak memberikan pernyataan atau jaminan dalam bentuk apapun bahwa:
  <ul>
  <li>penggunaan Layanan, Aplikasi, Akun dan/atau Sistem (atau bagian daripadanya) akan aman, tepat waktu, tidak terganggu atau bebas dari kesalahan, gangguan, virus atau hambatan lain atau komponen berbahaya lainnya; </li>
  <li>Layanan, Aplikasi, Akun dan/atau Sistem dapat tetap beroperasi atau digunakan bersamaan dengan atau dengan kombinasi perangkat (baik hardware maupun software) atau sistem pihak lain yang tidak kami sediakan atau miliki untuk pengoperasian Aplikasi; </li>
  <li>penyediaan Layanan maupun Aplikasi atau kelancaran ber-Transaksi atau penggunaan Akun akan memenuhi persyaratan atau harapan Anda; </li>
  <li>setiap data Transaksi yang Kami simpan atau tersimpan dalam Sistem harus akurat atau terpercaya; </li>
  <li>kualitas Layanan atau Aplikasi akan memenuhi persyaratan atau harapan Anda; dan </li>
  <li>tidak akan ada kesalahan, gangguan atau cacat dari Sistem, Layanan dan/atau Aplikasi. </li>
  </ul>

  </li>
  <li>Anda mengetahui dan setuju bahwa Kami berhak untuk memblokir dan/atau menutup Akun dan rekening dan/atau layanan/fasilitas apabila: a.) Kami memahami dan memiliki alasan yang memadai untuk menyatakan bahwa telah terjadi atau akan terjadi manipulasi keuangan atau perbankan atau kriminal yang terkait dengan Akun atau Rekening dan/atau layanan/fasilitas Pengguna BARONANG; b.) Pemegang atau Nasabah memberikan data yang tidak valid/tidak lengkap kepada Kami; c.) Terdapat permintaan tertulis dari instansi Kepolisian, Kejaksaan, Pengadilan, Pusat Pelaporan dan Analisis Transaksi Keuangan (PPATK), Kantor Pajak atau lembaga berwenang lainnya sesuai dengan hukum dan perundangan yang berlaku atau untuk memenuhi kewajiban/utang yang belum diselesaikan oleh Pemegang atau Nasabah.</li>
  <li>Anda dengan ini mengetahui bahwa Layanan, Aplikasi, Akun dan/atau Sistem mungkin atau dapat mengalami, terdapat atau terjadi pembatasan, keterlambatan, dan/atau masalah lain termasuk yang disebabkan karena atau sehubungan dengan (a) ketidaktersediaan atau terbatasnya jaringan (termasuk jaringan internet) dan/atau penggunaan atau (b) tidak tersedianya, terganggunya, atau tidak berfungsinya fitur tertentu pada perangkat yang Anda gunakan. Kami tidak bertanggung jawab atas segala keterlambatan, terhambatnya, tidak suksesnya, terganggunya atau gagalnya suatu Transaksi yang disebabkan karena hal tersebut di atas. </li>
  <li>Anda dengan ini mengetahui bahwa terdapat kemungkinan (a) Sistem atau Aplikasi (atau bagian manapun dari Sistem atau Aplikasi) tidak stabil, terganggu, terhenti, tidak berjalan dengan baik, tidak berjalan dengan sempurna dan/atau memiliki beberapa bug, dan/atau (b) Layanan (atau fitur-futur atau bagian-bagian tertentu) dapat berubah, tidak tersedia, dan atas terjadinya hal tersebut Anda setuju untuk tidak mengajukan Klaim kepada Kami.</li>
  <li>Kami dalam kondisi apapun tidak bertanggung jawab atas segala Klaim dari pihak manapun termasuk Anda maupun atas kerugian Anda serta pihak manapun yang terjadi sebagai akibat dari atau sehubungan dengan:
  <ul>
  <li>kehilangan Data; </li>
  <li>kehilangan pendapatan, keuntungan atau pemasukan lainnya; </li>
  <li>kehilangan, kerusakan atau cedera yang timbul dari, atau sehubungan dengan penggunaan Anda atas Aplikasi, Layanan dan/atau Akun atau atas ketidakmampuan atau kesalahan Anda dalam menggunakan Layanan, Aplikasi dan/atau Akun; atau </li>
  <li>tuntutan maupun gugatan yang dialami oleh Anda yang mungkin timbul sebagai akibat penyampaian informasi dari Anda yang tidak lengkap atau akibat tidak dilaksanakannya instruksi Anda, antara lain pembatalan, perubahan instruksi (untuk instruksi yang belum dijalankan) yang disampaikan kepada BAP, kecuali jika kerugian tersebut terjadi akibat kesalahan yang disengaja atau kelalaian oleh BAP </li>
  </ul>
  </li>
  <li>Anda dengan ini setuju dan mengikatkan diri untuk membebaskan Kami dari segala Klaim dalam bentuk apapun dan dimanapun dalam hal Kami tidak dapat melaksanakan, melanjutkan atau meneruskan Transaksi maupun perintah atau instruksi dari Anda melalui Aplikasi atau Akun, baik sebagian maupun seluruhnya, yang disebabkan karena kejadian-kejadian atau hal-hal di luar kekuasaan atau kemampuan Kami termasuk namun tidak terbatas pada (a) segala gangguan virus komputer, (b) sistem trojan horses, (c) komponen atau sistem yang dapat membahayakan serta mengganggu Aplikasi, Layanan dan/atau Akun, (d) layanan atau jasa Internet Service Provider atau jasa atau layanan pihak ketiga lainnya yang tersedia dalam Layanan, dan/atau (e) bencana alam, perang, huru-hara, keadaan peralatan, sistem atau transmisi yang tidak berfungsi, gangguan listrik, gangguan telekomunikasi, kebijakan pemerintah, kegagalan sistem perbankan dan/atau keuangan serta kejadian-kejadian atau sebab-sebab lain diluar kekuasaan atau kemampuan Kami. </li>
  <li>Anda dengan ini setuju dan mengikatkan diri untuk membebaskan Kami dari setiap dan seluruh Klaim dalam bentuk apapun, dari pihak manapun dan dimanapun yang diajukan, timbul atau terjadi sehubungan dengan atau sebagai akibat dari: 
  
  <ul>
  <li>penggunaan Data oleh Kami berdasarkan Syarat dan Ketentuan ini atau berdasarkan persetujuan, pengakuan, wewenang, kuasa dan/atau hak yang Anda berikan baik secara langsung maupun tidak langsung kepada Kami dalam Syarat dan Ketentuan ini; </li>
  <li>pemberian Data baik secara langsung maupun tidak langsung oleh Anda kepada Kami atau dalam atau melalui Aplikasi ini yang Anda lakukan secara (i) melanggar atau melawan hukum atau peraturan perundang-undangan yang berlaku, (ii) melanggar hak (termasuk hak kekayaan intelektual) dari atau milik orang atau pihak manapun, atau (iii) melanggar kontrak, kerjasama, kesepakatan, akta, pernyataan, penetapan, keputusan dan/atau dokumen apapun dimana Anda merupakan pihak atau dimana Anda atau aset Anda terikat; </li>
  <li>penggunaan Aplikasi, Akun dan/atau Layanan (i) secara tidak sah, (ii) melanggar hukum yang berlaku, (iii) melanggar Syarat dan Ketentuan ini, dan/atau (iv) untuk tindakan atau tujuan penipuan, kriminal, tindakan tidak sah atau tindakan pelanggaran hukum lainnya. </li>
  </ul>
  </li>
  <li>Anda dengan ini setuju dan mengikatkan diri untuk mengganti seluruh kerugian yang Kami alami dan mengganti seluruh biaya, ongkos, beban dan pengeluaran yang telah atau mungkin akan Kami keluarkan atau bayarkan sehubungan dengan atau sebagai akibat dari Klaim sebagaimana dimaksud butir 6, 8 dan 9 di atas (termasuk tetapi tidak terbatas pada biaya jasa hukum yang kami bayarkan atau keluarkan untuk melakukan pembelaan atau tindakan lain yang diperlukan terkait dengan Klaim tersebut). </li>
  <li>Anda dengan ini setuju bahwa Kami akan dibebaskan dari segala tuntutan, jika Kami tidak dapat melaksanakan instruksi dari Anda, baik sebagian maupun sepenuhnya yang disebabkan oleh kejadian atau sebab yang berada di luar kendali atau kemampuan Kami, meliputi tetapi tidak terbatas pada bencana alam, peperangan, kerusuhan, kondisi perangkat keras, kegagalan sistem infrastruktur elektronik atau transmisi, gangguan daya, gangguan telekomunikasi, kegagalan sistem kliring atau hal lainnya yang ditetapkan oleh Bank Indonesia atau lembaga berwenang lainnya. </li>
  <li>Setelah kejadian yang menyebabkan BAP dan/atau rekanannya tidak dapat melaksanakan instruksi dari Pemegang atau Nasabah berakhir, maka BAP dan/atau rekanannya akan melanjutkan kembali instruksi tersebut dalam kurun waktu sesuai dengan ketentuan dari Bank Indonesia dan/atau Otoritas Jasa Keuangan. </li>
  </ol>
  </section>
  <section id="hak">
  <h2 class="page-header" style="color: #375ce4;">Hak Kekayaan Intelektual</h2>
  <ol  >
  <li>Seluruh Sistem dan Aplikasi termasuk tetapi tidak terbatas pada seluruh: (a) layout, desain dan tampilan Aplikasi yang terdapat dalam atau ditampilkan pada media Aplikasi Anda; (b) logo, foto, gambar, nama, merek, kata, huruf-huruf, angka-angka, tulisan, dan susunan warna yang terdapat dalam Aplikasi; dan (c) kombinasi dari unsur-unsur sebagaimana dimaksud dalam huruf (a) dan (b), sepenuhnya merupakan Hak Kekayaan Intelektual milik Kami dan tidak ada pihak lain yang turut memiliki hak atas Aplikasi maupun atas layout, desain dan tampilan Aplikasi. </li>
  <li>Anda dilarang untuk dengan cara apapun dan dalam kondisi apapun menggunakan Hak Kekayaan Intelektual milik kami sebagaimana dimaksud dalam butir 1 di atas, tanpa persetujuan tertulis terlebih dahulu dari Kami. </li>
  </ol>
  </section>
  <section id="masaberlaku">
  <h2 class="page-header" style="color: #375ce4;">Masa Berlaku Dan Pengakhiran</h2>
  <ol  >
  <li>Perjanjian ini berlaku selama Anda belum melakukan penutupan Akun dan uang elektronik pada BARONANG Premier Anda atau Layanan BARONANG. </li>
  <li>Kami berhak untuk (i) tidak melaksanakan perintah dari Anda, (ii) tidak melanjutkan atau tidak meneruskan atau membatasi Transaksi, (iii) menghentikan, menangguhkan atau men-nonaktifkan penggunaan Akun, Layanan atau Aplikasi, dan/atau (iv) menghentikan atau menangguhkan keanggotaan Anda sebagai pengguna Aplikasi, dimana untuk masing-masing kondisi tersebut di atas baik untuk sementara waktu, untuk jangka waktu tertentu atau untuk seterusnya berdasarkan pertimbangan atau keputusan Kami yang Kami anggap baik, apabila terjadinya satu atau lebih kondisi di bawah ini: 
  <ul>
  <li>berdasarkan penilaian, keputusan dan/atau pertimbangan Kami sendiri, Kami mengetahui atau menduga telah atau akan terjadi atau dilakukan penggunaan Akun, Security Code, Aplikasi dan/atau Layanan untuk tujuan penipuan, tujuan kriminal, aksi kejahatan dan/atau untuk tujuan, alasan, aksi atau aktifitas lainnya yang melanggar hukum atau peraturan perundang-undangan negara Republik Indonesia maupun negara lain; </li>
  <li>berdasarkan penilaian, keputusan dan/atau pertimbangan Kami sendiri (a) Kami mengetahui atau menduga terjadi Transaksi yang tidak wajar, dan/atau (b) jumlah Transaksi dengan menggunakan atau melalui Akun dan/atau Security Code dalam satu hari melebihi jumlah atau tingkat kewajaran Transaksi, masing-masing sebagaimana ditentukan oleh Kami; </li>
  <li>berdasarkan penilaian, keputusan dan/atau pertimbangan Kami sendiri, Anda telah melanggar satu atau lebih Syarat dan Ketentuan; </li>
  <li>sebagian atau seluruh Data tidak benar, tidak lengkap, palsu, fiktif dan/atau menyesatkan; </li>
  <li>alamat email atau telepon yang Anda cantumkan atau gunakan untuk penggunaan Aplikasi atau Layanan atau untuk melakukan Transaksi telah terblokir; </li>
  <li>Anda belum melengkapi proses atau persyaratan sebagaimana diatur dalam ketentuan hukum dan/atau peraturan perundang-undangan yang berlaku; </li>
  <li>Kami sedang melakukan pembaharuan, pemeliharaan, perubahan, up-grade, penyesuaian, penggantian dan/atau tindakan lain atas Aplikasi, Layanan dan/atau Sistem (atau bagian manapun daripadanya) dan untuk itu Kami tidak berkewajiban mempertanggungjawabkannya kepada siapapun; dan/atau </li>
  <li>Anda meninggal dunia atau perusahaan, badan usaha, badan hukum atau persekutuan perdata yang Anda wakili dinyatakan atau dimohonkan pailit atau likuidasi atau berada dalam kondisi pailit atau likuidasi. </li>
  </ul>
  </li>
  <li>Anda dengan ini sepakat untuk mengenyampingkan ketentuan Pasal 1266 ayat (2) dan (3) Kitab Undang-undang Hukum Perdata sehingga Syarat dan Ketentuan, Aplikasi dan/atau Layanan dapat diakhiri (baik sebagian maupun seluruhnya, baik sementara waktu maupun seterusnya) sesuai dengan Syarat dan Ketentuan tanpa diperlukan adanya keputusan atau penetapan dari hakim pengadilan. </li>
  <li>Jika Akun atau Rekening telah berakhir atau ditutup atau jika Layanan atau Aplikasi sudah berakhir atau diakhiri oleh Kami:
  <ul>
  <li>Jika pengakhiran Akun, Layanan atau Aplikasi disebabkan karena pelanggaran yang Anda lakukan atas satu atau lebih Syarat dan Ketentuan, maka sisa saldo pada Akun seluruhnya akan menjadi hak BAP dan Anda setuju dan mengikatkan untuk tidak meminta pengembalian, pembayaran, penggantian dan/atau kompensasi dalam bentuk apapun kepada BAP. </li>
  </ul>
  </li>
  <li>Anda sepakat dan mengikatkan diri untuk tidak melakukan tindakan apapun yang dapat membatasi, menghambat dan/atau mengurangi satu atau lebih hak dan/atau wewenang Kami berdasarkan Syarat dan Ketentuan ini maupun hukum yang berlaku. </li>
  </ol>
  </section>
  <section id="hukum">
  <h2 class="page-header" style="color: #375ce4;">Hukum Yang Berlaku Dan Penyelesaian Perselisihan</h2>
  <ol  >
  <li>Syarat dan Ketentuan ini diatur dan ditafsirkan berdasarkan hukum Negara Republik Indonesia. </li>
  <li>Segala perselisihan atau pertentangan yang timbul sehubungan dengan atau terkait dengan hal-hal yang diatur dalam Syarat dan Ketentuan (maupun bagian daripadanya) termasuk perselisihan yang disebabkan karena adanya atau dilakukannya perbuatan melawan hukum atau pelanggaran atas satu atau lebih Syarat dan Ketentuan ini <b>(“Perselisihan”)</b> wajib diselesaikan dengan cara sebagai berikut: 
  <ol>
  <li>Salah satu pihak baik Anda atau Kami <b>(“Pihak Pertama”)</b> wajib menyampaikan pemberitahuan tertulis kepada pihak lainnya <b>(“Pihak Kedua”)</b> atas telah terjadinya Perselisihan <b>(“Pemberitahuan Perselisihan”)</b>. Perselisihan wajib diselesaikan secara musyawarah mufakat dalam waktu paling lambat 90 (sembilan puluh) hari kalender sejak tanggal Pemberitahuan Perselisihan Perselisihan <b>(“Periode Penyelesaian Musyawarah”)</b>; </li>
  <li>jika Perselisihan tidak dapat diselesaikan secara musyawarah mufakat sampai dengan berakhirnya Periode Penyelesaian Musyawarah, Pihak Pertama dan Pihak Kedua wajib untuk bersama-sama menunjuk pihak ketiga <b>(“Mediator”)</b> sebagai mediator untuk menyelesaikan Perselisihan dan penunjukan tersebut wajib dituangkan dalam bentuk tertulis yang ditandatangani bersama oleh Pihak Pertama dan Pihak Kedua. </li>
  <li>proses mediasi oleh Mediator khusus untuk sengketa Para Pihak dibidang Perbankan, diselesaikan penyelesaiannya oleh Para Pihak kepada <b>LAPSPI</b> yaitu <i>Lembaga Alternatif Penyelesaian Sengketa Perbankan Indonesia</i> sebagaimana diatur dalam Peraturan Otoritas Jasa Keuangan Nomor 1/POJK.07/2014 tentang Perlindungan Konsumen Sektor Jasa Keuangan, yang diundangkan tanggal 23 Januari 2014 (Lembaran Negara Republik Indonesia Tahun 2014 Nomor 12, Tambahan Lembaran Negara Republik Indonesia Nomor 5499) beserta perubahannya apabila ada. </li>
  <li>Anda wajib untuk menanggung seluruh biaya, ongkos dan pengeluaran yang telah atau mungkin Kami keluarkan atau bayarkan dalam rangka penyelesaian Perselisihan (termasuk tetapi tidak terbatas pada biaya, ongkos dan pengeluaran untuk menghadiri sidang arbitrase serta untuk mempersiapkan dan mengajukan segala pembelaan, gugatan, tuntutan, informasi, bukti, keterangan dan/atau dokumen apapun dalam rangka pelaksanaan atau selama proses sidang arbitrase) sampai dengan adanya putusan arbitrase yang final dan mengikat; </li>
  <li>Kecuali disyaratkan berdasarkan hukum yang berlaku atau diminta berdasarkan permintaan, keputusan atau penetapan resmi yang diterbitkan, dikeluarkan atau dibuat oleh pengadilan atau instansi pemerintah yang berwenang, selama proses penyelesaian Perselisihan sebagaimana diatur di atas sampai dengan adanya keputusan yang sah, final dan mengikat Pihak Pertama dan Pihak Kedua, maka Pihak Pertama dan Pihak Kedua wajib untuk merahasiakan segala informasi terkait dengan Perselisihan maupun proses penyelesaiannya dan karenanya dilarang untuk dengan cara apapun menginformasikan, memberitahukan atau mengumumkan kepada pihak manapun adanya Perselisihan tersebut maupun proses penyelesaiannya termasuk tetapi tidak terbatas melalui media massa (koran, televisi atau media lainnya) dan/atau media sosial. Jika Anda melanggar ketentuan butir (f) ini, Anda dengan ini mengetahui dan setuju bahwa seluruh atau sebagian hak Anda untuk menggunakan Layanan, Aplikasi, Akun dan/atau PIN dapat sewaktu-waktu diakhiri atau dinon-aktifkan oleh Kami baik untuk sementara waktu maupun untuk seterusnya. </li>
  </ol>
  </li>
  </ol>
  </section>
  <section id="ketentuanlain">
  <h2 class="page-header" style="color: #375ce4;">Ketentuan Lain</h2>
  <ol>
  <li>Untuk setiap masalah yang berkaitan dengan Transaksi, Layanan dan/atau Akun, Anda dapat meminta bantuan pada atau dari Contact Center atau Layanan Pengguna BARONANG Kami pada alamat email sebagai berikut [info@Baronang.id] dengan wajib menyertakan bukti pendukung yang dapat Kami terima untuk penyampaian pengaduan tertulis tersebut. </li>
  <li>Seluruh persetujuan, kuasa, wewenang dan/atau hak yang Anda berikan kepada Kami dalam Syarat dan Ketentuan ini tidak dapat berakhir karena alasan apapun termasuk karena alasan-alasan sebagaimana dimaksud dalam Pasal 1813, 1814, dan 1816 Kitab Undang-Undang Hukum Perdata selama Anda masih menggunakan Layanan atau Aplikasi atau masih menggunakan dan/atau memiliki Akun. </li>
  <li>Anda dengan ini membebaskan Kami dari seluruh Klaim sehubungan dengan pelaksanaan segala tindakan Kami berdasarkan kuasa, wewenang dan/atau hak yang Anda berikan kepada Kami dalam atau berdasarkan Syarat dan Ketentuan maupun pelaksanaan hak atau wewenang Kami berdasarkan Syarat dan Ketentuan. </li>
  </ol>
  </section>  
</div>




</div>
</div>
</section>

  </div>

<?php require('footer-login.php');?>