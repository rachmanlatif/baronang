<?php
require("lib/PHPMailer_5.2.0/class.phpmailer.php");
include("connect.inc");

if(isset($_GET['confirm']) and isset($_GET['tgl'])){
    $x = "select* from [dbo].[TrainingRequest] where Tanggal = '$_GET[tgl]' and EmailKoperasi = '$_GET[confirm]'";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        $xx = "select* from [dbo].[ScheduleTraining] where Tanggal = '$z[2]'";
        $yy = sqlsrv_query($conn, $xx);
        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);

        $sql = "update dbo.TrainingRequest set Status=1 where Tanggal='".$z[2]."' and EmailKoperasi='".$z[0]."'";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            $detail = '';
            $a = "select* from [dbo].[TrainingRequestDetail] where Tanggal = '$z[2]' and EmailKoperasi='$z[0]'";
            $b = sqlsrv_query($conn, $a);
            while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                $detail .= $c[0].' - '.$c[1].'<br>';
            }

            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->Host = "baronang.com";  // specify main and backup server
            $mail->SMTPAuth = true;     // turn on SMTP authentication
            $mail->Username = "konfirmasi@baronang.com";  // SMTP username
            $mail->Password = "alva7000"; // SMTP password
            $mail->Port = 587;
            $mail->SMTPDebug = 1;

            $mail->From = "konfirmasi@baronang.com";
            $mail->FromName = "Baronang Mail Service";
            $mail->AddAddress($z[0]);
            $mail->AddReplyTo("konfirmasi@baronang.com", "Baronang Konfirmasi");
            $mail->IsHTML(true);// set email format to HTML

            $mail->Subject = "Konfirmasi Pendaftaran Training";
            $mail->Body =
            "Hi! ".$z[1].",<br><br>
            Selamat! Koperasi anda telah diterima untuk training pada tanggal ".$zz[2]->format('Y-m-d')." jam ".$zz[0]." - ".$zz[1].".<br>
            Dengan detail peserta:<br>
            ".$detail."
            <br>
            <br>
            Alamat:
            <br>
            Plaza Sentra Indoraya Blok A No. 3
            <br>
            Jl. Imam Bonjol 88 Karawaci, Tangerang
            <br>
            <br>
            <br>
            Salam,<br>
            <br>
            <br>
            Baronang";
            if(!$mail->Send())
            {
                echo "Message could not be sent.";
                echo "Mailer Error: " . $mail->ErrorInfo;
            }
            else{
                messageAlert('Berhasil konfirmasi','success');
                header('Location: mtraining.php');
            }
        }
        else{
            messageAlert('Gagal konfirmasi','danger');
            header('Location: mtraining.php');
        }
    }
    else{
        messageAlert('Request tidak ditemukan','danger');
        header('Location: mtraining.php');
    }

}
else if(isset($_GET['del']) and isset($_GET['tgl'])){
    $x = "select* from [dbo].[TrainingRequest] where Tanggal = '$_GET[tgl]' and EmailKoperasi = '$_GET[del]'";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        $xx = "select* from [dbo].[ScheduleTraining] where Tanggal = '$z[2]'";
        $yy = sqlsrv_query($conn, $xx);
        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);

        $a = "select count(*) from [dbo].[TrainingRequestDetail] where Tanggal = '$z[2]' and EmailKoperasi = '$z[0]'";
        $b = sqlsrv_query($conn, $a);
        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
        $jml = $c[0];
        //tambah jumlah kursi
        $sql = "update dbo.ScheduleTraining set Sisa=Sisa+'$jml' where Tanggal='".$z[2]."'";
        $stmt = sqlsrv_query($conn, $sql);

        $sql = "update dbo.TrainingRequest set Status=2 where Tanggal='".$z[2]."' and EmailKoperasi='".$z[0]."'";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            $detail = '';
            $a = "select* from [dbo].[TrainingRequestDetail] where Tanggal = '$z[2]' and EmailKoperasi='$z[0]'";
            $b = sqlsrv_query($conn, $a);
            while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                $detail .= $c[0].' - '.$c[1].'<br>';
            }

            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->Host = "baronang.com";  // specify main and backup server
            $mail->SMTPAuth = true;     // turn on SMTP authentication
            $mail->Username = "konfirmasi@baronang.com";  // SMTP username
            $mail->Password = "alva7000"; // SMTP password
            $mail->Port = 587;
            $mail->SMTPDebug = 1;

            $mail->From = "konfirmasi@baronang.com";
            $mail->FromName = "Baronang Mail Service";
            $mail->AddAddress($z[0]);
            $mail->AddReplyTo("konfirmasi@baronang.com", "Baronang Konfirmasi");
            $mail->IsHTML(true);// set email format to HTML

            $mail->Subject = "Konfirmasi Pendaftaran Training";
            $mail->Body =
                "Hi! ".$z[1].",<br><br>
            Mohon maaf, koperasi anda tidak dapat kami terima untuk training pada tanggal ".$zz[2]->format('Y-m-d')." jam ".$zz[0]." - ".$zz[1].".<br>
            Dengan detail peserta:<br>
            ".$detail."
            <br>
            <br>
            <br>
            <br>
            Salam,<br>
            <br>
            <br>
            Baronang";
            if(!$mail->Send())
            {
                echo "Message could not be sent.";
                echo "Mailer Error: " . $mail->ErrorInfo;
            }
            else{
                messageAlert('Berhasil menolak koperasi','success');
                header('Location: mtraining.php');
            }
        }
        else{
            messageAlert('Gagal konfirmasi','danger');
            header('Location: mtraining.php');
        }
    }
    else{
        messageAlert('Request tidak ditemukan','danger');
        header('Location: mtraining.php');
    }

}
else if(isset($_POST['namekop']) or isset($_POST['email']) or isset($_POST['jadwal'])){
    $jml = count($_POST['name']);

    $sql = "select * from dbo.ScheduleTraining where Tanggal = '$_POST[jadwal]'";
    $stmt = sqlsrv_query($conn, $sql);
    $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC);
    if($row[4] < $jml){
        messageAlert('Jumlah kursi tidak mencukupi','info');
        header('Location: training.php');
    }
    else{
        $sql = "exec [dbo].[ProsesTrainingRequest] '$_POST[email]', '$_POST[namekop]','$_POST[jadwal]'";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            foreach($_POST['name'] as $index=>$name){
                $n = $_POST['name'][$index];
                $t = $_POST['telp'][$index];

                $detail .= $n.' - '.$t.'<br>';

                $sql = "exec [dbo].[ProsesTrainingRequestDetail] '$n','$t','$_POST[email]','$_POST[jadwal]'";
                $stmt = sqlsrv_query($conn, $sql);
            }

            //kurangin jumlah kursi
            $sql = "update dbo.ScheduleTraining set Sisa=Sisa-'$jml' where Tanggal='".$_POST['jadwal']."'";
            $stmt = sqlsrv_query($conn, $sql);

            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->Host = "baronang.com";  // specify main and backup server
            $mail->SMTPAuth = true;     // turn on SMTP authentication
            $mail->Username = "konfirmasi@baronang.com";  // SMTP username
            $mail->Password = "alva7000"; // SMTP password
            $mail->Port = 587;
            $mail->SMTPDebug = 1;

            $mail->From = "konfirmasi@baronang.com";
            $mail->FromName = "Baronang Mail Service";
            $mail->AddAddress($_POST['email']);
            $mail->AddReplyTo("konfirmasi@baronang.com", "Baronang Konfirmasi");
            $mail->IsHTML(true);// set email format to HTML

            $mail->Subject = "Konfirmasi Pendaftaran Training";
            $mail->Body =
            "Hi! ".$_POST['namekop'].",<br><br>
            Selamat! Anda telah berhasil mendaftarkan koperasi anda untuk jadwal training.<br>
            Dengan detail peserta:<br>
            ".$detail."
            <br>
            <br>
            Kami akan memverifikasi data yang anda berikan dan menginformasikan lagi ke email anda.<br>
            <br>
            <br>
            <br>
            <br>
            Salam,<br>
            <br>
            <br>
            Baronang";
            if(!$mail->Send())
            {
                echo "Message could not be sent.";
                echo "Mailer Error: " . $mail->ErrorInfo;
            }
            else{
                messageAlert('Berhasil menyimpan data training. Kami akan mengkonfirmasi ke email anda apabila telah disetujui.','success');
                header('Location: training.php');
            }
        }
        else{
            messageAlert('Gagal menyimpan data training','danger');
            header('Location: training.php');
        }
    }
}
else{
    messageAlert('Invalid request data','warning');
    header('Location: training.php');
}

?>