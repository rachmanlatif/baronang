<?php require('header.php');?>

<?php include("connectgateway.inc");?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary col-md-6">
                <div class="box-header with-border">
                    <h3 class="box-title">User Baronang Pay</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">

                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>User ID Baronang Pay</th>
                                    <th>Email</th>
                                    <th>No. KTP</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th>Balance</th>
                                </tr>
                                <?php
                                $a = "select* from [dbo].[UserPaymentGateway]";
                                $b = sqlsrv_query($conngw, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><?php echo $c[0]; ?></td>
                                        <td><?php echo $c[2]; ?></td>
                                        <td><?php echo $c[9]; ?></td>
                                        <td><?php echo $c[1]; ?></td>
                                        <td><?php echo $c[3]; ?></td>
                                        <td><?php echo $c[4]; ?></td>
                                        <td>
                                            <?php
                                            if($c[9] == 0){
                                                echo 'Tidak Aktif';
                                            }
                                            else{
                                                echo 'Aktif';
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo number_format($c[6]); ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require('footer.php');?>