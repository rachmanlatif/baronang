<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="static/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $_SESSION['Username']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <div></div>
        <ul class="sidebar-menu">
            <li class="header">MAIN MENU</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Master</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=""><a href="mregister.php"><i class="fa fa-circle-o"></i> Registration</a></li>
                    <li class=""><a href="muserpay.php"><i class="fa fa-circle-o"></i> User Baronang Pay</a></li>
                    <li class=""><a href="bank_list.php"><i class="fa fa-circle-o"></i> Bank List</a></li>
                    <li class=""><a href="wizard.php"><i class="fa fa-circle-o"></i> Wizard</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Content</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=""><a href="suggestion.php"><i class="fa fa-circle-o"></i> Suggestion</a></li>
                    <li class=""><a href="about.php"><i class="fa fa-circle-o"></i> About</a></li>
                    <li class=""><a href="contact.php"><i class="fa fa-circle-o"></i> Contact</a></li>
                    <li class=""><a href="eula.php"><i class="fa fa-circle-o"></i> EULA</a></li>
                    <li class=""><a href="privacy-pl.php"><i class="fa fa-circle-o"></i> Privacy Policy</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-tasks"></i> <span>Transaction</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class=""><a href="edc.php"><i class="fa fa-circle-o"></i> EDC</a></li>
                    <li class=""><a href="mtraining.php"><i class="fa fa-circle-o"></i> Training List</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
